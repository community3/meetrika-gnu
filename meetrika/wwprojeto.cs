/*
               File: WWProjeto
        Description:  Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:45:24.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwprojeto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwprojeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwprojeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavProjeto_tipocontagem1 = new GXCombobox();
         cmbavProjeto_tecnicacontagem1 = new GXCombobox();
         cmbavProjeto_status1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavProjeto_tipocontagem2 = new GXCombobox();
         cmbavProjeto_tecnicacontagem2 = new GXCombobox();
         cmbavProjeto_status2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavProjeto_tipocontagem3 = new GXCombobox();
         cmbavProjeto_tecnicacontagem3 = new GXCombobox();
         cmbavProjeto_status3 = new GXCombobox();
         chkProjeto_Incremental = new GXCheckbox();
         cmbProjeto_Status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_85 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_85_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_85_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV31Projeto_Sigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
               AV17Projeto_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Projeto_Nome1", AV17Projeto_Nome1);
               AV32Projeto_TipoContagem1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Projeto_TipoContagem1", AV32Projeto_TipoContagem1);
               AV33Projeto_TecnicaContagem1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
               AV34Projeto_Status1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV35Projeto_Sigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
               AV21Projeto_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Nome2", AV21Projeto_Nome2);
               AV36Projeto_TipoContagem2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Projeto_TipoContagem2", AV36Projeto_TipoContagem2);
               AV37Projeto_TecnicaContagem2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
               AV38Projeto_Status2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV39Projeto_Sigla3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
               AV25Projeto_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Projeto_Nome3", AV25Projeto_Nome3);
               AV40Projeto_TipoContagem3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Projeto_TipoContagem3", AV40Projeto_TipoContagem3);
               AV41Projeto_TecnicaContagem3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
               AV42Projeto_Status3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV47TFProjeto_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProjeto_Nome", AV47TFProjeto_Nome);
               AV48TFProjeto_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProjeto_Nome_Sel", AV48TFProjeto_Nome_Sel);
               AV51TFProjeto_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProjeto_Sigla", AV51TFProjeto_Sigla);
               AV52TFProjeto_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProjeto_Sigla_Sel", AV52TFProjeto_Sigla_Sel);
               AV55TFProjeto_Prazo = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0)));
               AV56TFProjeto_Prazo_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0)));
               AV59TFProjeto_Custo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFProjeto_Custo", StringUtil.LTrim( StringUtil.Str( AV59TFProjeto_Custo, 12, 2)));
               AV60TFProjeto_Custo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFProjeto_Custo_To", StringUtil.LTrim( StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2)));
               AV63TFProjeto_Incremental_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFProjeto_Incremental_Sel", StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0));
               AV66TFProjeto_Esforco = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFProjeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0)));
               AV67TFProjeto_Esforco_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFProjeto_Esforco_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0)));
               AV49ddo_Projeto_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Projeto_NomeTitleControlIdToReplace", AV49ddo_Projeto_NomeTitleControlIdToReplace);
               AV53ddo_Projeto_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Projeto_SiglaTitleControlIdToReplace", AV53ddo_Projeto_SiglaTitleControlIdToReplace);
               AV57ddo_Projeto_PrazoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Projeto_PrazoTitleControlIdToReplace", AV57ddo_Projeto_PrazoTitleControlIdToReplace);
               AV61ddo_Projeto_CustoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Projeto_CustoTitleControlIdToReplace", AV61ddo_Projeto_CustoTitleControlIdToReplace);
               AV64ddo_Projeto_IncrementalTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_Projeto_IncrementalTitleControlIdToReplace", AV64ddo_Projeto_IncrementalTitleControlIdToReplace);
               AV68ddo_Projeto_EsforcoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Projeto_EsforcoTitleControlIdToReplace", AV68ddo_Projeto_EsforcoTitleControlIdToReplace);
               AV72ddo_Projeto_StatusTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Projeto_StatusTitleControlIdToReplace", AV72ddo_Projeto_StatusTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV71TFProjeto_Status_Sels);
               AV114Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADF2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDF2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117452492");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwprojeto.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA1", StringUtil.RTrim( AV31Projeto_Sigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_NOME1", StringUtil.RTrim( AV17Projeto_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TIPOCONTAGEM1", StringUtil.RTrim( AV32Projeto_TipoContagem1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM1", StringUtil.RTrim( AV33Projeto_TecnicaContagem1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS1", StringUtil.RTrim( AV34Projeto_Status1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA2", StringUtil.RTrim( AV35Projeto_Sigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_NOME2", StringUtil.RTrim( AV21Projeto_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TIPOCONTAGEM2", StringUtil.RTrim( AV36Projeto_TipoContagem2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM2", StringUtil.RTrim( AV37Projeto_TecnicaContagem2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS2", StringUtil.RTrim( AV38Projeto_Status2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA3", StringUtil.RTrim( AV39Projeto_Sigla3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_NOME3", StringUtil.RTrim( AV25Projeto_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TIPOCONTAGEM3", StringUtil.RTrim( AV40Projeto_TipoContagem3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM3", StringUtil.RTrim( AV41Projeto_TecnicaContagem3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS3", StringUtil.RTrim( AV42Projeto_Status3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_NOME", StringUtil.RTrim( AV47TFProjeto_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_NOME_SEL", StringUtil.RTrim( AV48TFProjeto_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_SIGLA", StringUtil.RTrim( AV51TFProjeto_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_SIGLA_SEL", StringUtil.RTrim( AV52TFProjeto_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFProjeto_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_PRAZO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFProjeto_Prazo_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_CUSTO", StringUtil.LTrim( StringUtil.NToC( AV59TFProjeto_Custo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_CUSTO_TO", StringUtil.LTrim( StringUtil.NToC( AV60TFProjeto_Custo_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_INCREMENTAL_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_ESFORCO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFProjeto_Esforco), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETO_ESFORCO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67TFProjeto_Esforco_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_85", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_85), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV73DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV73DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETO_NOMETITLEFILTERDATA", AV46Projeto_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETO_NOMETITLEFILTERDATA", AV46Projeto_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETO_SIGLATITLEFILTERDATA", AV50Projeto_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETO_SIGLATITLEFILTERDATA", AV50Projeto_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETO_PRAZOTITLEFILTERDATA", AV54Projeto_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETO_PRAZOTITLEFILTERDATA", AV54Projeto_PrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETO_CUSTOTITLEFILTERDATA", AV58Projeto_CustoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETO_CUSTOTITLEFILTERDATA", AV58Projeto_CustoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETO_INCREMENTALTITLEFILTERDATA", AV62Projeto_IncrementalTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETO_INCREMENTALTITLEFILTERDATA", AV62Projeto_IncrementalTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETO_ESFORCOTITLEFILTERDATA", AV65Projeto_EsforcoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETO_ESFORCOTITLEFILTERDATA", AV65Projeto_EsforcoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETO_STATUSTITLEFILTERDATA", AV69Projeto_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETO_STATUSTITLEFILTERDATA", AV69Projeto_StatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFPROJETO_STATUS_SELS", AV71TFProjeto_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFPROJETO_STATUS_SELS", AV71TFProjeto_Status_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV114Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Caption", StringUtil.RTrim( Ddo_projeto_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Tooltip", StringUtil.RTrim( Ddo_projeto_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Cls", StringUtil.RTrim( Ddo_projeto_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_projeto_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_projeto_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_projeto_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projeto_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_projeto_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_projeto_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_projeto_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_projeto_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Filtertype", StringUtil.RTrim( Ddo_projeto_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_projeto_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_projeto_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Datalisttype", StringUtil.RTrim( Ddo_projeto_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Datalistproc", StringUtil.RTrim( Ddo_projeto_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_projeto_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Sortasc", StringUtil.RTrim( Ddo_projeto_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Sortdsc", StringUtil.RTrim( Ddo_projeto_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Loadingdata", StringUtil.RTrim( Ddo_projeto_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_projeto_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_projeto_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_projeto_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Caption", StringUtil.RTrim( Ddo_projeto_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_projeto_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Cls", StringUtil.RTrim( Ddo_projeto_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_projeto_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_projeto_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_projeto_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projeto_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_projeto_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_projeto_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_projeto_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_projeto_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_projeto_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_projeto_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_projeto_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_projeto_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_projeto_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_projeto_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_projeto_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_projeto_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_projeto_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_projeto_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_projeto_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_projeto_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Caption", StringUtil.RTrim( Ddo_projeto_prazo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Tooltip", StringUtil.RTrim( Ddo_projeto_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Cls", StringUtil.RTrim( Ddo_projeto_prazo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_projeto_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_projeto_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_projeto_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projeto_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_projeto_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_projeto_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_projeto_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Filtertype", StringUtil.RTrim( Ddo_projeto_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_projeto_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_projeto_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_projeto_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_projeto_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_projeto_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_projeto_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Caption", StringUtil.RTrim( Ddo_projeto_custo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Tooltip", StringUtil.RTrim( Ddo_projeto_custo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Cls", StringUtil.RTrim( Ddo_projeto_custo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Filteredtext_set", StringUtil.RTrim( Ddo_projeto_custo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Filteredtextto_set", StringUtil.RTrim( Ddo_projeto_custo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_projeto_custo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projeto_custo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Includesortasc", StringUtil.BoolToStr( Ddo_projeto_custo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Includesortdsc", StringUtil.BoolToStr( Ddo_projeto_custo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Includefilter", StringUtil.BoolToStr( Ddo_projeto_custo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Filtertype", StringUtil.RTrim( Ddo_projeto_custo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Filterisrange", StringUtil.BoolToStr( Ddo_projeto_custo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Includedatalist", StringUtil.BoolToStr( Ddo_projeto_custo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Cleanfilter", StringUtil.RTrim( Ddo_projeto_custo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Rangefilterfrom", StringUtil.RTrim( Ddo_projeto_custo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Rangefilterto", StringUtil.RTrim( Ddo_projeto_custo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Searchbuttontext", StringUtil.RTrim( Ddo_projeto_custo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Caption", StringUtil.RTrim( Ddo_projeto_incremental_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Tooltip", StringUtil.RTrim( Ddo_projeto_incremental_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Cls", StringUtil.RTrim( Ddo_projeto_incremental_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Selectedvalue_set", StringUtil.RTrim( Ddo_projeto_incremental_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_projeto_incremental_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projeto_incremental_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Includesortasc", StringUtil.BoolToStr( Ddo_projeto_incremental_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Includesortdsc", StringUtil.BoolToStr( Ddo_projeto_incremental_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Sortedstatus", StringUtil.RTrim( Ddo_projeto_incremental_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Includefilter", StringUtil.BoolToStr( Ddo_projeto_incremental_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Includedatalist", StringUtil.BoolToStr( Ddo_projeto_incremental_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Datalisttype", StringUtil.RTrim( Ddo_projeto_incremental_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Datalistfixedvalues", StringUtil.RTrim( Ddo_projeto_incremental_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Sortasc", StringUtil.RTrim( Ddo_projeto_incremental_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Sortdsc", StringUtil.RTrim( Ddo_projeto_incremental_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Cleanfilter", StringUtil.RTrim( Ddo_projeto_incremental_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Searchbuttontext", StringUtil.RTrim( Ddo_projeto_incremental_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Caption", StringUtil.RTrim( Ddo_projeto_esforco_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Tooltip", StringUtil.RTrim( Ddo_projeto_esforco_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Cls", StringUtil.RTrim( Ddo_projeto_esforco_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Filteredtext_set", StringUtil.RTrim( Ddo_projeto_esforco_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Filteredtextto_set", StringUtil.RTrim( Ddo_projeto_esforco_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Dropdownoptionstype", StringUtil.RTrim( Ddo_projeto_esforco_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projeto_esforco_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Includesortasc", StringUtil.BoolToStr( Ddo_projeto_esforco_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Includesortdsc", StringUtil.BoolToStr( Ddo_projeto_esforco_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Includefilter", StringUtil.BoolToStr( Ddo_projeto_esforco_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Filtertype", StringUtil.RTrim( Ddo_projeto_esforco_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Filterisrange", StringUtil.BoolToStr( Ddo_projeto_esforco_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Includedatalist", StringUtil.BoolToStr( Ddo_projeto_esforco_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Cleanfilter", StringUtil.RTrim( Ddo_projeto_esforco_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Rangefilterfrom", StringUtil.RTrim( Ddo_projeto_esforco_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Rangefilterto", StringUtil.RTrim( Ddo_projeto_esforco_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Searchbuttontext", StringUtil.RTrim( Ddo_projeto_esforco_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Caption", StringUtil.RTrim( Ddo_projeto_status_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Tooltip", StringUtil.RTrim( Ddo_projeto_status_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Cls", StringUtil.RTrim( Ddo_projeto_status_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_projeto_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_projeto_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projeto_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_projeto_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_projeto_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Sortedstatus", StringUtil.RTrim( Ddo_projeto_status_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_projeto_status_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_projeto_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Datalisttype", StringUtil.RTrim( Ddo_projeto_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_projeto_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_projeto_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Sortasc", StringUtil.RTrim( Ddo_projeto_status_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Sortdsc", StringUtil.RTrim( Ddo_projeto_status_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_projeto_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_projeto_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_projeto_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_projeto_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_projeto_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_projeto_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_projeto_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_projeto_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_projeto_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_projeto_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_projeto_prazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Activeeventkey", StringUtil.RTrim( Ddo_projeto_custo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Filteredtext_get", StringUtil.RTrim( Ddo_projeto_custo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_CUSTO_Filteredtextto_get", StringUtil.RTrim( Ddo_projeto_custo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Activeeventkey", StringUtil.RTrim( Ddo_projeto_incremental_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_INCREMENTAL_Selectedvalue_get", StringUtil.RTrim( Ddo_projeto_incremental_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Activeeventkey", StringUtil.RTrim( Ddo_projeto_esforco_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Filteredtext_get", StringUtil.RTrim( Ddo_projeto_esforco_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_ESFORCO_Filteredtextto_get", StringUtil.RTrim( Ddo_projeto_esforco_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_projeto_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETO_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_projeto_status_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDF2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDF2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwprojeto.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWProjeto" ;
      }

      public override String GetPgmdesc( )
      {
         return " Projeto" ;
      }

      protected void WBDF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DF2( true) ;
         }
         else
         {
            wb_table1_2_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_nome_Internalname, StringUtil.RTrim( AV47TFProjeto_Nome), StringUtil.RTrim( context.localUtil.Format( AV47TFProjeto_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_nome_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_nome_sel_Internalname, StringUtil.RTrim( AV48TFProjeto_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV48TFProjeto_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_nome_sel_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_sigla_Internalname, StringUtil.RTrim( AV51TFProjeto_Sigla), StringUtil.RTrim( context.localUtil.Format( AV51TFProjeto_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_sigla_sel_Internalname, StringUtil.RTrim( AV52TFProjeto_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV52TFProjeto_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFProjeto_Prazo), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFProjeto_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_prazo_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_prazo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFProjeto_Prazo_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFProjeto_Prazo_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_prazo_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_custo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV59TFProjeto_Custo, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV59TFProjeto_Custo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_custo_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_custo_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_custo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV60TFProjeto_Custo_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV60TFProjeto_Custo_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_custo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_custo_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_incremental_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63TFProjeto_Incremental_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_incremental_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_incremental_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFProjeto_Esforco), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV66TFProjeto_Esforco), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_esforco_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_esforco_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojeto_esforco_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67TFProjeto_Esforco_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV67TFProjeto_Esforco_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojeto_esforco_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojeto_esforco_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projeto_nometitlecontrolidtoreplace_Internalname, AV49ddo_Projeto_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_projeto_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projeto_siglatitlecontrolidtoreplace_Internalname, AV53ddo_Projeto_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_projeto_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETO_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projeto_prazotitlecontrolidtoreplace_Internalname, AV57ddo_Projeto_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_projeto_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETO_CUSTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projeto_custotitlecontrolidtoreplace_Internalname, AV61ddo_Projeto_CustoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_projeto_custotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETO_INCREMENTALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Internalname, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETO_ESFORCOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projeto_esforcotitlecontrolidtoreplace_Internalname, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_projeto_esforcotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETO_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projeto_statustitlecontrolidtoreplace_Internalname, AV72ddo_Projeto_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_projeto_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjeto.htm");
         }
         wbLoad = true;
      }

      protected void STARTDF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Projeto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDF0( ) ;
      }

      protected void WSDF2( )
      {
         STARTDF2( ) ;
         EVTDF2( ) ;
      }

      protected void EVTDF2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DF2 */
                              E11DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DF2 */
                              E12DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETO_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DF2 */
                              E13DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETO_PRAZO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14DF2 */
                              E14DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETO_CUSTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15DF2 */
                              E15DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETO_INCREMENTAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16DF2 */
                              E16DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETO_ESFORCO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17DF2 */
                              E17DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETO_STATUS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18DF2 */
                              E18DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19DF2 */
                              E19DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20DF2 */
                              E20DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21DF2 */
                              E21DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22DF2 */
                              E22DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23DF2 */
                              E23DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24DF2 */
                              E24DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25DF2 */
                              E25DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26DF2 */
                              E26DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27DF2 */
                              E27DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28DF2 */
                              E28DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29DF2 */
                              E29DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_85_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
                              SubsflControlProps_852( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV111Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV112Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV43Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV43Display)) ? AV113Display_GXI : context.convertURL( context.PathToRelativeUrl( AV43Display))));
                              A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
                              A649Projeto_Nome = StringUtil.Upper( cgiGet( edtProjeto_Nome_Internalname));
                              A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
                              A656Projeto_Prazo = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Prazo_Internalname), ",", "."));
                              A655Projeto_Custo = context.localUtil.CToN( cgiGet( edtProjeto_Custo_Internalname), ",", ".");
                              A1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( chkProjeto_Incremental_Internalname));
                              n1232Projeto_Incremental = false;
                              A657Projeto_Esforco = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Esforco_Internalname), ",", "."));
                              cmbProjeto_Status.Name = cmbProjeto_Status_Internalname;
                              cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
                              A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30DF2 */
                                    E30DF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31DF2 */
                                    E31DF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E32DF2 */
                                    E32DF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_sigla1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA1"), AV31Projeto_Sigla1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME1"), AV17Projeto_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tipocontagem1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TIPOCONTAGEM1"), AV32Projeto_TipoContagem1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tecnicacontagem1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM1"), AV33Projeto_TecnicaContagem1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_status1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS1"), AV34Projeto_Status1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_sigla2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA2"), AV35Projeto_Sigla2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME2"), AV21Projeto_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tipocontagem2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TIPOCONTAGEM2"), AV36Projeto_TipoContagem2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tecnicacontagem2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM2"), AV37Projeto_TecnicaContagem2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_status2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS2"), AV38Projeto_Status2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_sigla3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA3"), AV39Projeto_Sigla3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME3"), AV25Projeto_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tipocontagem3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TIPOCONTAGEM3"), AV40Projeto_TipoContagem3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tecnicacontagem3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM3"), AV41Projeto_TecnicaContagem3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_status3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS3"), AV42Projeto_Status3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_NOME"), AV47TFProjeto_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_NOME_SEL"), AV48TFProjeto_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_sigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_SIGLA"), AV51TFProjeto_Sigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_sigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_SIGLA_SEL"), AV52TFProjeto_Sigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_prazo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_PRAZO"), ",", ".") != Convert.ToDecimal( AV55TFProjeto_Prazo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_prazo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV56TFProjeto_Prazo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_custo Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_CUSTO"), ",", ".") != AV59TFProjeto_Custo )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_custo_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_CUSTO_TO"), ",", ".") != AV60TFProjeto_Custo_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_incremental_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_INCREMENTAL_SEL"), ",", ".") != Convert.ToDecimal( AV63TFProjeto_Incremental_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_esforco Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_ESFORCO"), ",", ".") != Convert.ToDecimal( AV66TFProjeto_Esforco )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojeto_esforco_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_ESFORCO_TO"), ",", ".") != Convert.ToDecimal( AV67TFProjeto_Esforco_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_TIPOCONTAGEM", "Tipo de Contagem", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavProjeto_tipocontagem1.Name = "vPROJETO_TIPOCONTAGEM1";
            cmbavProjeto_tipocontagem1.WebTags = "";
            cmbavProjeto_tipocontagem1.addItem("", "Todos", 0);
            cmbavProjeto_tipocontagem1.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbavProjeto_tipocontagem1.addItem("M", "Projeto de Melhor�a", 0);
            cmbavProjeto_tipocontagem1.addItem("C", "Projeto de Contagem", 0);
            cmbavProjeto_tipocontagem1.addItem("A", "Aplica��o", 0);
            if ( cmbavProjeto_tipocontagem1.ItemCount > 0 )
            {
               AV32Projeto_TipoContagem1 = cmbavProjeto_tipocontagem1.getValidValue(AV32Projeto_TipoContagem1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Projeto_TipoContagem1", AV32Projeto_TipoContagem1);
            }
            cmbavProjeto_tecnicacontagem1.Name = "vPROJETO_TECNICACONTAGEM1";
            cmbavProjeto_tecnicacontagem1.WebTags = "";
            cmbavProjeto_tecnicacontagem1.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem1.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem1.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem1.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem1.ItemCount > 0 )
            {
               AV33Projeto_TecnicaContagem1 = cmbavProjeto_tecnicacontagem1.getValidValue(AV33Projeto_TecnicaContagem1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            }
            cmbavProjeto_status1.Name = "vPROJETO_STATUS1";
            cmbavProjeto_status1.WebTags = "";
            cmbavProjeto_status1.addItem("", "Todos", 0);
            cmbavProjeto_status1.addItem("A", "Aberto", 0);
            cmbavProjeto_status1.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status1.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status1.ItemCount > 0 )
            {
               AV34Projeto_Status1 = cmbavProjeto_status1.getValidValue(AV34Projeto_Status1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_TIPOCONTAGEM", "Tipo de Contagem", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavProjeto_tipocontagem2.Name = "vPROJETO_TIPOCONTAGEM2";
            cmbavProjeto_tipocontagem2.WebTags = "";
            cmbavProjeto_tipocontagem2.addItem("", "Todos", 0);
            cmbavProjeto_tipocontagem2.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbavProjeto_tipocontagem2.addItem("M", "Projeto de Melhor�a", 0);
            cmbavProjeto_tipocontagem2.addItem("C", "Projeto de Contagem", 0);
            cmbavProjeto_tipocontagem2.addItem("A", "Aplica��o", 0);
            if ( cmbavProjeto_tipocontagem2.ItemCount > 0 )
            {
               AV36Projeto_TipoContagem2 = cmbavProjeto_tipocontagem2.getValidValue(AV36Projeto_TipoContagem2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Projeto_TipoContagem2", AV36Projeto_TipoContagem2);
            }
            cmbavProjeto_tecnicacontagem2.Name = "vPROJETO_TECNICACONTAGEM2";
            cmbavProjeto_tecnicacontagem2.WebTags = "";
            cmbavProjeto_tecnicacontagem2.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem2.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem2.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem2.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem2.ItemCount > 0 )
            {
               AV37Projeto_TecnicaContagem2 = cmbavProjeto_tecnicacontagem2.getValidValue(AV37Projeto_TecnicaContagem2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
            }
            cmbavProjeto_status2.Name = "vPROJETO_STATUS2";
            cmbavProjeto_status2.WebTags = "";
            cmbavProjeto_status2.addItem("", "Todos", 0);
            cmbavProjeto_status2.addItem("A", "Aberto", 0);
            cmbavProjeto_status2.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status2.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status2.ItemCount > 0 )
            {
               AV38Projeto_Status2 = cmbavProjeto_status2.getValidValue(AV38Projeto_Status2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_TIPOCONTAGEM", "Tipo de Contagem", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavProjeto_tipocontagem3.Name = "vPROJETO_TIPOCONTAGEM3";
            cmbavProjeto_tipocontagem3.WebTags = "";
            cmbavProjeto_tipocontagem3.addItem("", "Todos", 0);
            cmbavProjeto_tipocontagem3.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbavProjeto_tipocontagem3.addItem("M", "Projeto de Melhor�a", 0);
            cmbavProjeto_tipocontagem3.addItem("C", "Projeto de Contagem", 0);
            cmbavProjeto_tipocontagem3.addItem("A", "Aplica��o", 0);
            if ( cmbavProjeto_tipocontagem3.ItemCount > 0 )
            {
               AV40Projeto_TipoContagem3 = cmbavProjeto_tipocontagem3.getValidValue(AV40Projeto_TipoContagem3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Projeto_TipoContagem3", AV40Projeto_TipoContagem3);
            }
            cmbavProjeto_tecnicacontagem3.Name = "vPROJETO_TECNICACONTAGEM3";
            cmbavProjeto_tecnicacontagem3.WebTags = "";
            cmbavProjeto_tecnicacontagem3.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem3.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem3.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem3.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem3.ItemCount > 0 )
            {
               AV41Projeto_TecnicaContagem3 = cmbavProjeto_tecnicacontagem3.getValidValue(AV41Projeto_TecnicaContagem3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
            }
            cmbavProjeto_status3.Name = "vPROJETO_STATUS3";
            cmbavProjeto_status3.WebTags = "";
            cmbavProjeto_status3.addItem("", "Todos", 0);
            cmbavProjeto_status3.addItem("A", "Aberto", 0);
            cmbavProjeto_status3.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status3.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status3.ItemCount > 0 )
            {
               AV42Projeto_Status3 = cmbavProjeto_status3.getValidValue(AV42Projeto_Status3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
            }
            GXCCtl = "PROJETO_INCREMENTAL_" + sGXsfl_85_idx;
            chkProjeto_Incremental.Name = GXCCtl;
            chkProjeto_Incremental.WebTags = "";
            chkProjeto_Incremental.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "TitleCaption", chkProjeto_Incremental.Caption);
            chkProjeto_Incremental.CheckedValue = "false";
            GXCCtl = "PROJETO_STATUS_" + sGXsfl_85_idx;
            cmbProjeto_Status.Name = GXCCtl;
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_852( ) ;
         while ( nGXsfl_85_idx <= nRC_GXsfl_85 )
         {
            sendrow_852( ) ;
            nGXsfl_85_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_85_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_85_idx+1));
            sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
            SubsflControlProps_852( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV31Projeto_Sigla1 ,
                                       String AV17Projeto_Nome1 ,
                                       String AV32Projeto_TipoContagem1 ,
                                       String AV33Projeto_TecnicaContagem1 ,
                                       String AV34Projeto_Status1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV35Projeto_Sigla2 ,
                                       String AV21Projeto_Nome2 ,
                                       String AV36Projeto_TipoContagem2 ,
                                       String AV37Projeto_TecnicaContagem2 ,
                                       String AV38Projeto_Status2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV39Projeto_Sigla3 ,
                                       String AV25Projeto_Nome3 ,
                                       String AV40Projeto_TipoContagem3 ,
                                       String AV41Projeto_TecnicaContagem3 ,
                                       String AV42Projeto_Status3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV47TFProjeto_Nome ,
                                       String AV48TFProjeto_Nome_Sel ,
                                       String AV51TFProjeto_Sigla ,
                                       String AV52TFProjeto_Sigla_Sel ,
                                       short AV55TFProjeto_Prazo ,
                                       short AV56TFProjeto_Prazo_To ,
                                       decimal AV59TFProjeto_Custo ,
                                       decimal AV60TFProjeto_Custo_To ,
                                       short AV63TFProjeto_Incremental_Sel ,
                                       short AV66TFProjeto_Esforco ,
                                       short AV67TFProjeto_Esforco_To ,
                                       String AV49ddo_Projeto_NomeTitleControlIdToReplace ,
                                       String AV53ddo_Projeto_SiglaTitleControlIdToReplace ,
                                       String AV57ddo_Projeto_PrazoTitleControlIdToReplace ,
                                       String AV61ddo_Projeto_CustoTitleControlIdToReplace ,
                                       String AV64ddo_Projeto_IncrementalTitleControlIdToReplace ,
                                       String AV68ddo_Projeto_EsforcoTitleControlIdToReplace ,
                                       String AV72ddo_Projeto_StatusTitleControlIdToReplace ,
                                       IGxCollection AV71TFProjeto_Status_Sels ,
                                       String AV114Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A648Projeto_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDF2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "PROJETO_NOME", StringUtil.RTrim( A649Projeto_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "PROJETO_SIGLA", StringUtil.RTrim( A650Projeto_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( "", A1232Projeto_Incremental));
         GxWebStd.gx_hidden_field( context, "PROJETO_INCREMENTAL", StringUtil.BoolToStr( A1232Projeto_Incremental));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_STATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         GxWebStd.gx_hidden_field( context, "PROJETO_STATUS", StringUtil.RTrim( A658Projeto_Status));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavProjeto_tipocontagem1.ItemCount > 0 )
         {
            AV32Projeto_TipoContagem1 = cmbavProjeto_tipocontagem1.getValidValue(AV32Projeto_TipoContagem1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Projeto_TipoContagem1", AV32Projeto_TipoContagem1);
         }
         if ( cmbavProjeto_tecnicacontagem1.ItemCount > 0 )
         {
            AV33Projeto_TecnicaContagem1 = cmbavProjeto_tecnicacontagem1.getValidValue(AV33Projeto_TecnicaContagem1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
         }
         if ( cmbavProjeto_status1.ItemCount > 0 )
         {
            AV34Projeto_Status1 = cmbavProjeto_status1.getValidValue(AV34Projeto_Status1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavProjeto_tipocontagem2.ItemCount > 0 )
         {
            AV36Projeto_TipoContagem2 = cmbavProjeto_tipocontagem2.getValidValue(AV36Projeto_TipoContagem2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Projeto_TipoContagem2", AV36Projeto_TipoContagem2);
         }
         if ( cmbavProjeto_tecnicacontagem2.ItemCount > 0 )
         {
            AV37Projeto_TecnicaContagem2 = cmbavProjeto_tecnicacontagem2.getValidValue(AV37Projeto_TecnicaContagem2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
         }
         if ( cmbavProjeto_status2.ItemCount > 0 )
         {
            AV38Projeto_Status2 = cmbavProjeto_status2.getValidValue(AV38Projeto_Status2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavProjeto_tipocontagem3.ItemCount > 0 )
         {
            AV40Projeto_TipoContagem3 = cmbavProjeto_tipocontagem3.getValidValue(AV40Projeto_TipoContagem3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Projeto_TipoContagem3", AV40Projeto_TipoContagem3);
         }
         if ( cmbavProjeto_tecnicacontagem3.ItemCount > 0 )
         {
            AV41Projeto_TecnicaContagem3 = cmbavProjeto_tecnicacontagem3.getValidValue(AV41Projeto_TecnicaContagem3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
         }
         if ( cmbavProjeto_status3.ItemCount > 0 )
         {
            AV42Projeto_Status3 = cmbavProjeto_status3.getValidValue(AV42Projeto_Status3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV114Pgmname = "WWProjeto";
         context.Gx_err = 0;
      }

      protected void RFDF2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 85;
         /* Execute user event: E31DF2 */
         E31DF2 ();
         nGXsfl_85_idx = 1;
         sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
         SubsflControlProps_852( ) ;
         nGXsfl_85_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_852( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A658Projeto_Status ,
                                                 AV110WWProjetoDS_32_Tfprojeto_status_sels ,
                                                 AV79WWProjetoDS_1_Dynamicfiltersselector1 ,
                                                 AV80WWProjetoDS_2_Projeto_sigla1 ,
                                                 AV81WWProjetoDS_3_Projeto_nome1 ,
                                                 AV82WWProjetoDS_4_Projeto_tipocontagem1 ,
                                                 AV83WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                                 AV84WWProjetoDS_6_Projeto_status1 ,
                                                 AV85WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                                 AV86WWProjetoDS_8_Dynamicfiltersselector2 ,
                                                 AV87WWProjetoDS_9_Projeto_sigla2 ,
                                                 AV88WWProjetoDS_10_Projeto_nome2 ,
                                                 AV89WWProjetoDS_11_Projeto_tipocontagem2 ,
                                                 AV90WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                                 AV91WWProjetoDS_13_Projeto_status2 ,
                                                 AV92WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                                 AV93WWProjetoDS_15_Dynamicfiltersselector3 ,
                                                 AV94WWProjetoDS_16_Projeto_sigla3 ,
                                                 AV95WWProjetoDS_17_Projeto_nome3 ,
                                                 AV96WWProjetoDS_18_Projeto_tipocontagem3 ,
                                                 AV97WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                                 AV98WWProjetoDS_20_Projeto_status3 ,
                                                 AV100WWProjetoDS_22_Tfprojeto_nome_sel ,
                                                 AV99WWProjetoDS_21_Tfprojeto_nome ,
                                                 AV102WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                                 AV101WWProjetoDS_23_Tfprojeto_sigla ,
                                                 AV103WWProjetoDS_25_Tfprojeto_prazo ,
                                                 AV104WWProjetoDS_26_Tfprojeto_prazo_to ,
                                                 AV105WWProjetoDS_27_Tfprojeto_custo ,
                                                 AV106WWProjetoDS_28_Tfprojeto_custo_to ,
                                                 AV107WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                                 AV108WWProjetoDS_30_Tfprojeto_esforco ,
                                                 AV109WWProjetoDS_31_Tfprojeto_esforco_to ,
                                                 AV110WWProjetoDS_32_Tfprojeto_status_sels.Count ,
                                                 A650Projeto_Sigla ,
                                                 A649Projeto_Nome ,
                                                 A651Projeto_TipoContagem ,
                                                 A652Projeto_TecnicaContagem ,
                                                 A656Projeto_Prazo ,
                                                 A655Projeto_Custo ,
                                                 A1232Projeto_Incremental ,
                                                 A657Projeto_Esforco ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV80WWProjetoDS_2_Projeto_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV80WWProjetoDS_2_Projeto_sigla1), 15, "%");
            lV81WWProjetoDS_3_Projeto_nome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWProjetoDS_3_Projeto_nome1), 50, "%");
            lV87WWProjetoDS_9_Projeto_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWProjetoDS_9_Projeto_sigla2), 15, "%");
            lV88WWProjetoDS_10_Projeto_nome2 = StringUtil.PadR( StringUtil.RTrim( AV88WWProjetoDS_10_Projeto_nome2), 50, "%");
            lV94WWProjetoDS_16_Projeto_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV94WWProjetoDS_16_Projeto_sigla3), 15, "%");
            lV95WWProjetoDS_17_Projeto_nome3 = StringUtil.PadR( StringUtil.RTrim( AV95WWProjetoDS_17_Projeto_nome3), 50, "%");
            lV99WWProjetoDS_21_Tfprojeto_nome = StringUtil.PadR( StringUtil.RTrim( AV99WWProjetoDS_21_Tfprojeto_nome), 50, "%");
            lV101WWProjetoDS_23_Tfprojeto_sigla = StringUtil.PadR( StringUtil.RTrim( AV101WWProjetoDS_23_Tfprojeto_sigla), 15, "%");
            /* Using cursor H00DF3 */
            pr_default.execute(0, new Object[] {lV80WWProjetoDS_2_Projeto_sigla1, lV81WWProjetoDS_3_Projeto_nome1, AV82WWProjetoDS_4_Projeto_tipocontagem1, AV83WWProjetoDS_5_Projeto_tecnicacontagem1, AV84WWProjetoDS_6_Projeto_status1, lV87WWProjetoDS_9_Projeto_sigla2, lV88WWProjetoDS_10_Projeto_nome2, AV89WWProjetoDS_11_Projeto_tipocontagem2, AV90WWProjetoDS_12_Projeto_tecnicacontagem2, AV91WWProjetoDS_13_Projeto_status2, lV94WWProjetoDS_16_Projeto_sigla3, lV95WWProjetoDS_17_Projeto_nome3, AV96WWProjetoDS_18_Projeto_tipocontagem3, AV97WWProjetoDS_19_Projeto_tecnicacontagem3, AV98WWProjetoDS_20_Projeto_status3, lV99WWProjetoDS_21_Tfprojeto_nome, AV100WWProjetoDS_22_Tfprojeto_nome_sel, lV101WWProjetoDS_23_Tfprojeto_sigla, AV102WWProjetoDS_24_Tfprojeto_sigla_sel, AV103WWProjetoDS_25_Tfprojeto_prazo, AV104WWProjetoDS_26_Tfprojeto_prazo_to, AV105WWProjetoDS_27_Tfprojeto_custo, AV106WWProjetoDS_28_Tfprojeto_custo_to, AV108WWProjetoDS_30_Tfprojeto_esforco, AV109WWProjetoDS_31_Tfprojeto_esforco_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_85_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A652Projeto_TecnicaContagem = H00DF3_A652Projeto_TecnicaContagem[0];
               A651Projeto_TipoContagem = H00DF3_A651Projeto_TipoContagem[0];
               A658Projeto_Status = H00DF3_A658Projeto_Status[0];
               A1232Projeto_Incremental = H00DF3_A1232Projeto_Incremental[0];
               n1232Projeto_Incremental = H00DF3_n1232Projeto_Incremental[0];
               A650Projeto_Sigla = H00DF3_A650Projeto_Sigla[0];
               A649Projeto_Nome = H00DF3_A649Projeto_Nome[0];
               A648Projeto_Codigo = H00DF3_A648Projeto_Codigo[0];
               A657Projeto_Esforco = H00DF3_A657Projeto_Esforco[0];
               A655Projeto_Custo = H00DF3_A655Projeto_Custo[0];
               A656Projeto_Prazo = H00DF3_A656Projeto_Prazo[0];
               A657Projeto_Esforco = H00DF3_A657Projeto_Esforco[0];
               A655Projeto_Custo = H00DF3_A655Projeto_Custo[0];
               A656Projeto_Prazo = H00DF3_A656Projeto_Prazo[0];
               /* Execute user event: E32DF2 */
               E32DF2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 85;
            WBDF0( ) ;
         }
         nGXsfl_85_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV80WWProjetoDS_2_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV81WWProjetoDS_3_Projeto_nome1 = AV17Projeto_Nome1;
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = AV32Projeto_TipoContagem1;
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV84WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV85WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV87WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV88WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = AV36Projeto_TipoContagem2;
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_13_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_16_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_17_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = AV40Projeto_TipoContagem3;
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV98WWProjetoDS_20_Projeto_status3 = AV42Projeto_Status3;
         AV99WWProjetoDS_21_Tfprojeto_nome = AV47TFProjeto_Nome;
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = AV48TFProjeto_Nome_Sel;
         AV101WWProjetoDS_23_Tfprojeto_sigla = AV51TFProjeto_Sigla;
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = AV52TFProjeto_Sigla_Sel;
         AV103WWProjetoDS_25_Tfprojeto_prazo = AV55TFProjeto_Prazo;
         AV104WWProjetoDS_26_Tfprojeto_prazo_to = AV56TFProjeto_Prazo_To;
         AV105WWProjetoDS_27_Tfprojeto_custo = AV59TFProjeto_Custo;
         AV106WWProjetoDS_28_Tfprojeto_custo_to = AV60TFProjeto_Custo_To;
         AV107WWProjetoDS_29_Tfprojeto_incremental_sel = AV63TFProjeto_Incremental_Sel;
         AV108WWProjetoDS_30_Tfprojeto_esforco = AV66TFProjeto_Esforco;
         AV109WWProjetoDS_31_Tfprojeto_esforco_to = AV67TFProjeto_Esforco_To;
         AV110WWProjetoDS_32_Tfprojeto_status_sels = AV71TFProjeto_Status_Sels;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A658Projeto_Status ,
                                              AV110WWProjetoDS_32_Tfprojeto_status_sels ,
                                              AV79WWProjetoDS_1_Dynamicfiltersselector1 ,
                                              AV80WWProjetoDS_2_Projeto_sigla1 ,
                                              AV81WWProjetoDS_3_Projeto_nome1 ,
                                              AV82WWProjetoDS_4_Projeto_tipocontagem1 ,
                                              AV83WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                              AV84WWProjetoDS_6_Projeto_status1 ,
                                              AV85WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                              AV86WWProjetoDS_8_Dynamicfiltersselector2 ,
                                              AV87WWProjetoDS_9_Projeto_sigla2 ,
                                              AV88WWProjetoDS_10_Projeto_nome2 ,
                                              AV89WWProjetoDS_11_Projeto_tipocontagem2 ,
                                              AV90WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                              AV91WWProjetoDS_13_Projeto_status2 ,
                                              AV92WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                              AV93WWProjetoDS_15_Dynamicfiltersselector3 ,
                                              AV94WWProjetoDS_16_Projeto_sigla3 ,
                                              AV95WWProjetoDS_17_Projeto_nome3 ,
                                              AV96WWProjetoDS_18_Projeto_tipocontagem3 ,
                                              AV97WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                              AV98WWProjetoDS_20_Projeto_status3 ,
                                              AV100WWProjetoDS_22_Tfprojeto_nome_sel ,
                                              AV99WWProjetoDS_21_Tfprojeto_nome ,
                                              AV102WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                              AV101WWProjetoDS_23_Tfprojeto_sigla ,
                                              AV103WWProjetoDS_25_Tfprojeto_prazo ,
                                              AV104WWProjetoDS_26_Tfprojeto_prazo_to ,
                                              AV105WWProjetoDS_27_Tfprojeto_custo ,
                                              AV106WWProjetoDS_28_Tfprojeto_custo_to ,
                                              AV107WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                              AV108WWProjetoDS_30_Tfprojeto_esforco ,
                                              AV109WWProjetoDS_31_Tfprojeto_esforco_to ,
                                              AV110WWProjetoDS_32_Tfprojeto_status_sels.Count ,
                                              A650Projeto_Sigla ,
                                              A649Projeto_Nome ,
                                              A651Projeto_TipoContagem ,
                                              A652Projeto_TecnicaContagem ,
                                              A656Projeto_Prazo ,
                                              A655Projeto_Custo ,
                                              A1232Projeto_Incremental ,
                                              A657Projeto_Esforco ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV80WWProjetoDS_2_Projeto_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV80WWProjetoDS_2_Projeto_sigla1), 15, "%");
         lV81WWProjetoDS_3_Projeto_nome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWProjetoDS_3_Projeto_nome1), 50, "%");
         lV87WWProjetoDS_9_Projeto_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWProjetoDS_9_Projeto_sigla2), 15, "%");
         lV88WWProjetoDS_10_Projeto_nome2 = StringUtil.PadR( StringUtil.RTrim( AV88WWProjetoDS_10_Projeto_nome2), 50, "%");
         lV94WWProjetoDS_16_Projeto_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV94WWProjetoDS_16_Projeto_sigla3), 15, "%");
         lV95WWProjetoDS_17_Projeto_nome3 = StringUtil.PadR( StringUtil.RTrim( AV95WWProjetoDS_17_Projeto_nome3), 50, "%");
         lV99WWProjetoDS_21_Tfprojeto_nome = StringUtil.PadR( StringUtil.RTrim( AV99WWProjetoDS_21_Tfprojeto_nome), 50, "%");
         lV101WWProjetoDS_23_Tfprojeto_sigla = StringUtil.PadR( StringUtil.RTrim( AV101WWProjetoDS_23_Tfprojeto_sigla), 15, "%");
         /* Using cursor H00DF5 */
         pr_default.execute(1, new Object[] {lV80WWProjetoDS_2_Projeto_sigla1, lV81WWProjetoDS_3_Projeto_nome1, AV82WWProjetoDS_4_Projeto_tipocontagem1, AV83WWProjetoDS_5_Projeto_tecnicacontagem1, AV84WWProjetoDS_6_Projeto_status1, lV87WWProjetoDS_9_Projeto_sigla2, lV88WWProjetoDS_10_Projeto_nome2, AV89WWProjetoDS_11_Projeto_tipocontagem2, AV90WWProjetoDS_12_Projeto_tecnicacontagem2, AV91WWProjetoDS_13_Projeto_status2, lV94WWProjetoDS_16_Projeto_sigla3, lV95WWProjetoDS_17_Projeto_nome3, AV96WWProjetoDS_18_Projeto_tipocontagem3, AV97WWProjetoDS_19_Projeto_tecnicacontagem3, AV98WWProjetoDS_20_Projeto_status3, lV99WWProjetoDS_21_Tfprojeto_nome, AV100WWProjetoDS_22_Tfprojeto_nome_sel, lV101WWProjetoDS_23_Tfprojeto_sigla, AV102WWProjetoDS_24_Tfprojeto_sigla_sel, AV103WWProjetoDS_25_Tfprojeto_prazo, AV104WWProjetoDS_26_Tfprojeto_prazo_to, AV105WWProjetoDS_27_Tfprojeto_custo, AV106WWProjetoDS_28_Tfprojeto_custo_to, AV108WWProjetoDS_30_Tfprojeto_esforco, AV109WWProjetoDS_31_Tfprojeto_esforco_to});
         GRID_nRecordCount = H00DF5_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV80WWProjetoDS_2_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV81WWProjetoDS_3_Projeto_nome1 = AV17Projeto_Nome1;
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = AV32Projeto_TipoContagem1;
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV84WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV85WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV87WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV88WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = AV36Projeto_TipoContagem2;
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_13_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_16_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_17_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = AV40Projeto_TipoContagem3;
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV98WWProjetoDS_20_Projeto_status3 = AV42Projeto_Status3;
         AV99WWProjetoDS_21_Tfprojeto_nome = AV47TFProjeto_Nome;
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = AV48TFProjeto_Nome_Sel;
         AV101WWProjetoDS_23_Tfprojeto_sigla = AV51TFProjeto_Sigla;
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = AV52TFProjeto_Sigla_Sel;
         AV103WWProjetoDS_25_Tfprojeto_prazo = AV55TFProjeto_Prazo;
         AV104WWProjetoDS_26_Tfprojeto_prazo_to = AV56TFProjeto_Prazo_To;
         AV105WWProjetoDS_27_Tfprojeto_custo = AV59TFProjeto_Custo;
         AV106WWProjetoDS_28_Tfprojeto_custo_to = AV60TFProjeto_Custo_To;
         AV107WWProjetoDS_29_Tfprojeto_incremental_sel = AV63TFProjeto_Incremental_Sel;
         AV108WWProjetoDS_30_Tfprojeto_esforco = AV66TFProjeto_Esforco;
         AV109WWProjetoDS_31_Tfprojeto_esforco_to = AV67TFProjeto_Esforco_To;
         AV110WWProjetoDS_32_Tfprojeto_status_sels = AV71TFProjeto_Status_Sels;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV80WWProjetoDS_2_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV81WWProjetoDS_3_Projeto_nome1 = AV17Projeto_Nome1;
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = AV32Projeto_TipoContagem1;
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV84WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV85WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV87WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV88WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = AV36Projeto_TipoContagem2;
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_13_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_16_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_17_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = AV40Projeto_TipoContagem3;
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV98WWProjetoDS_20_Projeto_status3 = AV42Projeto_Status3;
         AV99WWProjetoDS_21_Tfprojeto_nome = AV47TFProjeto_Nome;
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = AV48TFProjeto_Nome_Sel;
         AV101WWProjetoDS_23_Tfprojeto_sigla = AV51TFProjeto_Sigla;
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = AV52TFProjeto_Sigla_Sel;
         AV103WWProjetoDS_25_Tfprojeto_prazo = AV55TFProjeto_Prazo;
         AV104WWProjetoDS_26_Tfprojeto_prazo_to = AV56TFProjeto_Prazo_To;
         AV105WWProjetoDS_27_Tfprojeto_custo = AV59TFProjeto_Custo;
         AV106WWProjetoDS_28_Tfprojeto_custo_to = AV60TFProjeto_Custo_To;
         AV107WWProjetoDS_29_Tfprojeto_incremental_sel = AV63TFProjeto_Incremental_Sel;
         AV108WWProjetoDS_30_Tfprojeto_esforco = AV66TFProjeto_Esforco;
         AV109WWProjetoDS_31_Tfprojeto_esforco_to = AV67TFProjeto_Esforco_To;
         AV110WWProjetoDS_32_Tfprojeto_status_sels = AV71TFProjeto_Status_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV80WWProjetoDS_2_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV81WWProjetoDS_3_Projeto_nome1 = AV17Projeto_Nome1;
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = AV32Projeto_TipoContagem1;
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV84WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV85WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV87WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV88WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = AV36Projeto_TipoContagem2;
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_13_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_16_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_17_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = AV40Projeto_TipoContagem3;
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV98WWProjetoDS_20_Projeto_status3 = AV42Projeto_Status3;
         AV99WWProjetoDS_21_Tfprojeto_nome = AV47TFProjeto_Nome;
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = AV48TFProjeto_Nome_Sel;
         AV101WWProjetoDS_23_Tfprojeto_sigla = AV51TFProjeto_Sigla;
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = AV52TFProjeto_Sigla_Sel;
         AV103WWProjetoDS_25_Tfprojeto_prazo = AV55TFProjeto_Prazo;
         AV104WWProjetoDS_26_Tfprojeto_prazo_to = AV56TFProjeto_Prazo_To;
         AV105WWProjetoDS_27_Tfprojeto_custo = AV59TFProjeto_Custo;
         AV106WWProjetoDS_28_Tfprojeto_custo_to = AV60TFProjeto_Custo_To;
         AV107WWProjetoDS_29_Tfprojeto_incremental_sel = AV63TFProjeto_Incremental_Sel;
         AV108WWProjetoDS_30_Tfprojeto_esforco = AV66TFProjeto_Esforco;
         AV109WWProjetoDS_31_Tfprojeto_esforco_to = AV67TFProjeto_Esforco_To;
         AV110WWProjetoDS_32_Tfprojeto_status_sels = AV71TFProjeto_Status_Sels;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV80WWProjetoDS_2_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV81WWProjetoDS_3_Projeto_nome1 = AV17Projeto_Nome1;
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = AV32Projeto_TipoContagem1;
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV84WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV85WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV87WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV88WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = AV36Projeto_TipoContagem2;
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_13_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_16_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_17_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = AV40Projeto_TipoContagem3;
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV98WWProjetoDS_20_Projeto_status3 = AV42Projeto_Status3;
         AV99WWProjetoDS_21_Tfprojeto_nome = AV47TFProjeto_Nome;
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = AV48TFProjeto_Nome_Sel;
         AV101WWProjetoDS_23_Tfprojeto_sigla = AV51TFProjeto_Sigla;
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = AV52TFProjeto_Sigla_Sel;
         AV103WWProjetoDS_25_Tfprojeto_prazo = AV55TFProjeto_Prazo;
         AV104WWProjetoDS_26_Tfprojeto_prazo_to = AV56TFProjeto_Prazo_To;
         AV105WWProjetoDS_27_Tfprojeto_custo = AV59TFProjeto_Custo;
         AV106WWProjetoDS_28_Tfprojeto_custo_to = AV60TFProjeto_Custo_To;
         AV107WWProjetoDS_29_Tfprojeto_incremental_sel = AV63TFProjeto_Incremental_Sel;
         AV108WWProjetoDS_30_Tfprojeto_esforco = AV66TFProjeto_Esforco;
         AV109WWProjetoDS_31_Tfprojeto_esforco_to = AV67TFProjeto_Esforco_To;
         AV110WWProjetoDS_32_Tfprojeto_status_sels = AV71TFProjeto_Status_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV80WWProjetoDS_2_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV81WWProjetoDS_3_Projeto_nome1 = AV17Projeto_Nome1;
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = AV32Projeto_TipoContagem1;
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV84WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV85WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV87WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV88WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = AV36Projeto_TipoContagem2;
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_13_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_16_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_17_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = AV40Projeto_TipoContagem3;
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV98WWProjetoDS_20_Projeto_status3 = AV42Projeto_Status3;
         AV99WWProjetoDS_21_Tfprojeto_nome = AV47TFProjeto_Nome;
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = AV48TFProjeto_Nome_Sel;
         AV101WWProjetoDS_23_Tfprojeto_sigla = AV51TFProjeto_Sigla;
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = AV52TFProjeto_Sigla_Sel;
         AV103WWProjetoDS_25_Tfprojeto_prazo = AV55TFProjeto_Prazo;
         AV104WWProjetoDS_26_Tfprojeto_prazo_to = AV56TFProjeto_Prazo_To;
         AV105WWProjetoDS_27_Tfprojeto_custo = AV59TFProjeto_Custo;
         AV106WWProjetoDS_28_Tfprojeto_custo_to = AV60TFProjeto_Custo_To;
         AV107WWProjetoDS_29_Tfprojeto_incremental_sel = AV63TFProjeto_Incremental_Sel;
         AV108WWProjetoDS_30_Tfprojeto_esforco = AV66TFProjeto_Esforco;
         AV109WWProjetoDS_31_Tfprojeto_esforco_to = AV67TFProjeto_Esforco_To;
         AV110WWProjetoDS_32_Tfprojeto_status_sels = AV71TFProjeto_Status_Sels;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDF0( )
      {
         /* Before Start, stand alone formulas. */
         AV114Pgmname = "WWProjeto";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E30DF2 */
         E30DF2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV73DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETO_NOMETITLEFILTERDATA"), AV46Projeto_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETO_SIGLATITLEFILTERDATA"), AV50Projeto_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETO_PRAZOTITLEFILTERDATA"), AV54Projeto_PrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETO_CUSTOTITLEFILTERDATA"), AV58Projeto_CustoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETO_INCREMENTALTITLEFILTERDATA"), AV62Projeto_IncrementalTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETO_ESFORCOTITLEFILTERDATA"), AV65Projeto_EsforcoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETO_STATUSTITLEFILTERDATA"), AV69Projeto_StatusTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV31Projeto_Sigla1 = StringUtil.Upper( cgiGet( edtavProjeto_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
            AV17Projeto_Nome1 = StringUtil.Upper( cgiGet( edtavProjeto_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Projeto_Nome1", AV17Projeto_Nome1);
            cmbavProjeto_tipocontagem1.Name = cmbavProjeto_tipocontagem1_Internalname;
            cmbavProjeto_tipocontagem1.CurrentValue = cgiGet( cmbavProjeto_tipocontagem1_Internalname);
            AV32Projeto_TipoContagem1 = cgiGet( cmbavProjeto_tipocontagem1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Projeto_TipoContagem1", AV32Projeto_TipoContagem1);
            cmbavProjeto_tecnicacontagem1.Name = cmbavProjeto_tecnicacontagem1_Internalname;
            cmbavProjeto_tecnicacontagem1.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem1_Internalname);
            AV33Projeto_TecnicaContagem1 = cgiGet( cmbavProjeto_tecnicacontagem1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            cmbavProjeto_status1.Name = cmbavProjeto_status1_Internalname;
            cmbavProjeto_status1.CurrentValue = cgiGet( cmbavProjeto_status1_Internalname);
            AV34Projeto_Status1 = cgiGet( cmbavProjeto_status1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV35Projeto_Sigla2 = StringUtil.Upper( cgiGet( edtavProjeto_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
            AV21Projeto_Nome2 = StringUtil.Upper( cgiGet( edtavProjeto_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Nome2", AV21Projeto_Nome2);
            cmbavProjeto_tipocontagem2.Name = cmbavProjeto_tipocontagem2_Internalname;
            cmbavProjeto_tipocontagem2.CurrentValue = cgiGet( cmbavProjeto_tipocontagem2_Internalname);
            AV36Projeto_TipoContagem2 = cgiGet( cmbavProjeto_tipocontagem2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Projeto_TipoContagem2", AV36Projeto_TipoContagem2);
            cmbavProjeto_tecnicacontagem2.Name = cmbavProjeto_tecnicacontagem2_Internalname;
            cmbavProjeto_tecnicacontagem2.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem2_Internalname);
            AV37Projeto_TecnicaContagem2 = cgiGet( cmbavProjeto_tecnicacontagem2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
            cmbavProjeto_status2.Name = cmbavProjeto_status2_Internalname;
            cmbavProjeto_status2.CurrentValue = cgiGet( cmbavProjeto_status2_Internalname);
            AV38Projeto_Status2 = cgiGet( cmbavProjeto_status2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV39Projeto_Sigla3 = StringUtil.Upper( cgiGet( edtavProjeto_sigla3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
            AV25Projeto_Nome3 = StringUtil.Upper( cgiGet( edtavProjeto_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Projeto_Nome3", AV25Projeto_Nome3);
            cmbavProjeto_tipocontagem3.Name = cmbavProjeto_tipocontagem3_Internalname;
            cmbavProjeto_tipocontagem3.CurrentValue = cgiGet( cmbavProjeto_tipocontagem3_Internalname);
            AV40Projeto_TipoContagem3 = cgiGet( cmbavProjeto_tipocontagem3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Projeto_TipoContagem3", AV40Projeto_TipoContagem3);
            cmbavProjeto_tecnicacontagem3.Name = cmbavProjeto_tecnicacontagem3_Internalname;
            cmbavProjeto_tecnicacontagem3.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem3_Internalname);
            AV41Projeto_TecnicaContagem3 = cgiGet( cmbavProjeto_tecnicacontagem3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
            cmbavProjeto_status3.Name = cmbavProjeto_status3_Internalname;
            cmbavProjeto_status3.CurrentValue = cgiGet( cmbavProjeto_status3_Internalname);
            AV42Projeto_Status3 = cgiGet( cmbavProjeto_status3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV47TFProjeto_Nome = StringUtil.Upper( cgiGet( edtavTfprojeto_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProjeto_Nome", AV47TFProjeto_Nome);
            AV48TFProjeto_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfprojeto_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProjeto_Nome_Sel", AV48TFProjeto_Nome_Sel);
            AV51TFProjeto_Sigla = StringUtil.Upper( cgiGet( edtavTfprojeto_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProjeto_Sigla", AV51TFProjeto_Sigla);
            AV52TFProjeto_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfprojeto_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProjeto_Sigla_Sel", AV52TFProjeto_Sigla_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETO_PRAZO");
               GX_FocusControl = edtavTfprojeto_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFProjeto_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0)));
            }
            else
            {
               AV55TFProjeto_Prazo = (short)(context.localUtil.CToN( cgiGet( edtavTfprojeto_prazo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_prazo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_prazo_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETO_PRAZO_TO");
               GX_FocusControl = edtavTfprojeto_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFProjeto_Prazo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0)));
            }
            else
            {
               AV56TFProjeto_Prazo_To = (short)(context.localUtil.CToN( cgiGet( edtavTfprojeto_prazo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_custo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_custo_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETO_CUSTO");
               GX_FocusControl = edtavTfprojeto_custo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFProjeto_Custo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFProjeto_Custo", StringUtil.LTrim( StringUtil.Str( AV59TFProjeto_Custo, 12, 2)));
            }
            else
            {
               AV59TFProjeto_Custo = context.localUtil.CToN( cgiGet( edtavTfprojeto_custo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFProjeto_Custo", StringUtil.LTrim( StringUtil.Str( AV59TFProjeto_Custo, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_custo_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_custo_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETO_CUSTO_TO");
               GX_FocusControl = edtavTfprojeto_custo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFProjeto_Custo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFProjeto_Custo_To", StringUtil.LTrim( StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2)));
            }
            else
            {
               AV60TFProjeto_Custo_To = context.localUtil.CToN( cgiGet( edtavTfprojeto_custo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFProjeto_Custo_To", StringUtil.LTrim( StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_incremental_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_incremental_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETO_INCREMENTAL_SEL");
               GX_FocusControl = edtavTfprojeto_incremental_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFProjeto_Incremental_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFProjeto_Incremental_Sel", StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0));
            }
            else
            {
               AV63TFProjeto_Incremental_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfprojeto_incremental_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFProjeto_Incremental_Sel", StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_esforco_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_esforco_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETO_ESFORCO");
               GX_FocusControl = edtavTfprojeto_esforco_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFProjeto_Esforco = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFProjeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0)));
            }
            else
            {
               AV66TFProjeto_Esforco = (short)(context.localUtil.CToN( cgiGet( edtavTfprojeto_esforco_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFProjeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_esforco_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojeto_esforco_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETO_ESFORCO_TO");
               GX_FocusControl = edtavTfprojeto_esforco_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFProjeto_Esforco_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFProjeto_Esforco_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0)));
            }
            else
            {
               AV67TFProjeto_Esforco_To = (short)(context.localUtil.CToN( cgiGet( edtavTfprojeto_esforco_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFProjeto_Esforco_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0)));
            }
            AV49ddo_Projeto_NomeTitleControlIdToReplace = cgiGet( edtavDdo_projeto_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Projeto_NomeTitleControlIdToReplace", AV49ddo_Projeto_NomeTitleControlIdToReplace);
            AV53ddo_Projeto_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_projeto_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Projeto_SiglaTitleControlIdToReplace", AV53ddo_Projeto_SiglaTitleControlIdToReplace);
            AV57ddo_Projeto_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_projeto_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Projeto_PrazoTitleControlIdToReplace", AV57ddo_Projeto_PrazoTitleControlIdToReplace);
            AV61ddo_Projeto_CustoTitleControlIdToReplace = cgiGet( edtavDdo_projeto_custotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Projeto_CustoTitleControlIdToReplace", AV61ddo_Projeto_CustoTitleControlIdToReplace);
            AV64ddo_Projeto_IncrementalTitleControlIdToReplace = cgiGet( edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_Projeto_IncrementalTitleControlIdToReplace", AV64ddo_Projeto_IncrementalTitleControlIdToReplace);
            AV68ddo_Projeto_EsforcoTitleControlIdToReplace = cgiGet( edtavDdo_projeto_esforcotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Projeto_EsforcoTitleControlIdToReplace", AV68ddo_Projeto_EsforcoTitleControlIdToReplace);
            AV72ddo_Projeto_StatusTitleControlIdToReplace = cgiGet( edtavDdo_projeto_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Projeto_StatusTitleControlIdToReplace", AV72ddo_Projeto_StatusTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_85 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_85"), ",", "."));
            AV75GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV76GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_projeto_nome_Caption = cgiGet( "DDO_PROJETO_NOME_Caption");
            Ddo_projeto_nome_Tooltip = cgiGet( "DDO_PROJETO_NOME_Tooltip");
            Ddo_projeto_nome_Cls = cgiGet( "DDO_PROJETO_NOME_Cls");
            Ddo_projeto_nome_Filteredtext_set = cgiGet( "DDO_PROJETO_NOME_Filteredtext_set");
            Ddo_projeto_nome_Selectedvalue_set = cgiGet( "DDO_PROJETO_NOME_Selectedvalue_set");
            Ddo_projeto_nome_Dropdownoptionstype = cgiGet( "DDO_PROJETO_NOME_Dropdownoptionstype");
            Ddo_projeto_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETO_NOME_Titlecontrolidtoreplace");
            Ddo_projeto_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_NOME_Includesortasc"));
            Ddo_projeto_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_NOME_Includesortdsc"));
            Ddo_projeto_nome_Sortedstatus = cgiGet( "DDO_PROJETO_NOME_Sortedstatus");
            Ddo_projeto_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_NOME_Includefilter"));
            Ddo_projeto_nome_Filtertype = cgiGet( "DDO_PROJETO_NOME_Filtertype");
            Ddo_projeto_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_NOME_Filterisrange"));
            Ddo_projeto_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_NOME_Includedatalist"));
            Ddo_projeto_nome_Datalisttype = cgiGet( "DDO_PROJETO_NOME_Datalisttype");
            Ddo_projeto_nome_Datalistproc = cgiGet( "DDO_PROJETO_NOME_Datalistproc");
            Ddo_projeto_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PROJETO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_projeto_nome_Sortasc = cgiGet( "DDO_PROJETO_NOME_Sortasc");
            Ddo_projeto_nome_Sortdsc = cgiGet( "DDO_PROJETO_NOME_Sortdsc");
            Ddo_projeto_nome_Loadingdata = cgiGet( "DDO_PROJETO_NOME_Loadingdata");
            Ddo_projeto_nome_Cleanfilter = cgiGet( "DDO_PROJETO_NOME_Cleanfilter");
            Ddo_projeto_nome_Noresultsfound = cgiGet( "DDO_PROJETO_NOME_Noresultsfound");
            Ddo_projeto_nome_Searchbuttontext = cgiGet( "DDO_PROJETO_NOME_Searchbuttontext");
            Ddo_projeto_sigla_Caption = cgiGet( "DDO_PROJETO_SIGLA_Caption");
            Ddo_projeto_sigla_Tooltip = cgiGet( "DDO_PROJETO_SIGLA_Tooltip");
            Ddo_projeto_sigla_Cls = cgiGet( "DDO_PROJETO_SIGLA_Cls");
            Ddo_projeto_sigla_Filteredtext_set = cgiGet( "DDO_PROJETO_SIGLA_Filteredtext_set");
            Ddo_projeto_sigla_Selectedvalue_set = cgiGet( "DDO_PROJETO_SIGLA_Selectedvalue_set");
            Ddo_projeto_sigla_Dropdownoptionstype = cgiGet( "DDO_PROJETO_SIGLA_Dropdownoptionstype");
            Ddo_projeto_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETO_SIGLA_Titlecontrolidtoreplace");
            Ddo_projeto_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_SIGLA_Includesortasc"));
            Ddo_projeto_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_SIGLA_Includesortdsc"));
            Ddo_projeto_sigla_Sortedstatus = cgiGet( "DDO_PROJETO_SIGLA_Sortedstatus");
            Ddo_projeto_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_SIGLA_Includefilter"));
            Ddo_projeto_sigla_Filtertype = cgiGet( "DDO_PROJETO_SIGLA_Filtertype");
            Ddo_projeto_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_SIGLA_Filterisrange"));
            Ddo_projeto_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_SIGLA_Includedatalist"));
            Ddo_projeto_sigla_Datalisttype = cgiGet( "DDO_PROJETO_SIGLA_Datalisttype");
            Ddo_projeto_sigla_Datalistproc = cgiGet( "DDO_PROJETO_SIGLA_Datalistproc");
            Ddo_projeto_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PROJETO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_projeto_sigla_Sortasc = cgiGet( "DDO_PROJETO_SIGLA_Sortasc");
            Ddo_projeto_sigla_Sortdsc = cgiGet( "DDO_PROJETO_SIGLA_Sortdsc");
            Ddo_projeto_sigla_Loadingdata = cgiGet( "DDO_PROJETO_SIGLA_Loadingdata");
            Ddo_projeto_sigla_Cleanfilter = cgiGet( "DDO_PROJETO_SIGLA_Cleanfilter");
            Ddo_projeto_sigla_Noresultsfound = cgiGet( "DDO_PROJETO_SIGLA_Noresultsfound");
            Ddo_projeto_sigla_Searchbuttontext = cgiGet( "DDO_PROJETO_SIGLA_Searchbuttontext");
            Ddo_projeto_prazo_Caption = cgiGet( "DDO_PROJETO_PRAZO_Caption");
            Ddo_projeto_prazo_Tooltip = cgiGet( "DDO_PROJETO_PRAZO_Tooltip");
            Ddo_projeto_prazo_Cls = cgiGet( "DDO_PROJETO_PRAZO_Cls");
            Ddo_projeto_prazo_Filteredtext_set = cgiGet( "DDO_PROJETO_PRAZO_Filteredtext_set");
            Ddo_projeto_prazo_Filteredtextto_set = cgiGet( "DDO_PROJETO_PRAZO_Filteredtextto_set");
            Ddo_projeto_prazo_Dropdownoptionstype = cgiGet( "DDO_PROJETO_PRAZO_Dropdownoptionstype");
            Ddo_projeto_prazo_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETO_PRAZO_Titlecontrolidtoreplace");
            Ddo_projeto_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_PRAZO_Includesortasc"));
            Ddo_projeto_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_PRAZO_Includesortdsc"));
            Ddo_projeto_prazo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_PRAZO_Includefilter"));
            Ddo_projeto_prazo_Filtertype = cgiGet( "DDO_PROJETO_PRAZO_Filtertype");
            Ddo_projeto_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_PRAZO_Filterisrange"));
            Ddo_projeto_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_PRAZO_Includedatalist"));
            Ddo_projeto_prazo_Cleanfilter = cgiGet( "DDO_PROJETO_PRAZO_Cleanfilter");
            Ddo_projeto_prazo_Rangefilterfrom = cgiGet( "DDO_PROJETO_PRAZO_Rangefilterfrom");
            Ddo_projeto_prazo_Rangefilterto = cgiGet( "DDO_PROJETO_PRAZO_Rangefilterto");
            Ddo_projeto_prazo_Searchbuttontext = cgiGet( "DDO_PROJETO_PRAZO_Searchbuttontext");
            Ddo_projeto_custo_Caption = cgiGet( "DDO_PROJETO_CUSTO_Caption");
            Ddo_projeto_custo_Tooltip = cgiGet( "DDO_PROJETO_CUSTO_Tooltip");
            Ddo_projeto_custo_Cls = cgiGet( "DDO_PROJETO_CUSTO_Cls");
            Ddo_projeto_custo_Filteredtext_set = cgiGet( "DDO_PROJETO_CUSTO_Filteredtext_set");
            Ddo_projeto_custo_Filteredtextto_set = cgiGet( "DDO_PROJETO_CUSTO_Filteredtextto_set");
            Ddo_projeto_custo_Dropdownoptionstype = cgiGet( "DDO_PROJETO_CUSTO_Dropdownoptionstype");
            Ddo_projeto_custo_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETO_CUSTO_Titlecontrolidtoreplace");
            Ddo_projeto_custo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_CUSTO_Includesortasc"));
            Ddo_projeto_custo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_CUSTO_Includesortdsc"));
            Ddo_projeto_custo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_CUSTO_Includefilter"));
            Ddo_projeto_custo_Filtertype = cgiGet( "DDO_PROJETO_CUSTO_Filtertype");
            Ddo_projeto_custo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_CUSTO_Filterisrange"));
            Ddo_projeto_custo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_CUSTO_Includedatalist"));
            Ddo_projeto_custo_Cleanfilter = cgiGet( "DDO_PROJETO_CUSTO_Cleanfilter");
            Ddo_projeto_custo_Rangefilterfrom = cgiGet( "DDO_PROJETO_CUSTO_Rangefilterfrom");
            Ddo_projeto_custo_Rangefilterto = cgiGet( "DDO_PROJETO_CUSTO_Rangefilterto");
            Ddo_projeto_custo_Searchbuttontext = cgiGet( "DDO_PROJETO_CUSTO_Searchbuttontext");
            Ddo_projeto_incremental_Caption = cgiGet( "DDO_PROJETO_INCREMENTAL_Caption");
            Ddo_projeto_incremental_Tooltip = cgiGet( "DDO_PROJETO_INCREMENTAL_Tooltip");
            Ddo_projeto_incremental_Cls = cgiGet( "DDO_PROJETO_INCREMENTAL_Cls");
            Ddo_projeto_incremental_Selectedvalue_set = cgiGet( "DDO_PROJETO_INCREMENTAL_Selectedvalue_set");
            Ddo_projeto_incremental_Dropdownoptionstype = cgiGet( "DDO_PROJETO_INCREMENTAL_Dropdownoptionstype");
            Ddo_projeto_incremental_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETO_INCREMENTAL_Titlecontrolidtoreplace");
            Ddo_projeto_incremental_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_INCREMENTAL_Includesortasc"));
            Ddo_projeto_incremental_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_INCREMENTAL_Includesortdsc"));
            Ddo_projeto_incremental_Sortedstatus = cgiGet( "DDO_PROJETO_INCREMENTAL_Sortedstatus");
            Ddo_projeto_incremental_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_INCREMENTAL_Includefilter"));
            Ddo_projeto_incremental_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_INCREMENTAL_Includedatalist"));
            Ddo_projeto_incremental_Datalisttype = cgiGet( "DDO_PROJETO_INCREMENTAL_Datalisttype");
            Ddo_projeto_incremental_Datalistfixedvalues = cgiGet( "DDO_PROJETO_INCREMENTAL_Datalistfixedvalues");
            Ddo_projeto_incremental_Sortasc = cgiGet( "DDO_PROJETO_INCREMENTAL_Sortasc");
            Ddo_projeto_incremental_Sortdsc = cgiGet( "DDO_PROJETO_INCREMENTAL_Sortdsc");
            Ddo_projeto_incremental_Cleanfilter = cgiGet( "DDO_PROJETO_INCREMENTAL_Cleanfilter");
            Ddo_projeto_incremental_Searchbuttontext = cgiGet( "DDO_PROJETO_INCREMENTAL_Searchbuttontext");
            Ddo_projeto_esforco_Caption = cgiGet( "DDO_PROJETO_ESFORCO_Caption");
            Ddo_projeto_esforco_Tooltip = cgiGet( "DDO_PROJETO_ESFORCO_Tooltip");
            Ddo_projeto_esforco_Cls = cgiGet( "DDO_PROJETO_ESFORCO_Cls");
            Ddo_projeto_esforco_Filteredtext_set = cgiGet( "DDO_PROJETO_ESFORCO_Filteredtext_set");
            Ddo_projeto_esforco_Filteredtextto_set = cgiGet( "DDO_PROJETO_ESFORCO_Filteredtextto_set");
            Ddo_projeto_esforco_Dropdownoptionstype = cgiGet( "DDO_PROJETO_ESFORCO_Dropdownoptionstype");
            Ddo_projeto_esforco_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETO_ESFORCO_Titlecontrolidtoreplace");
            Ddo_projeto_esforco_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_ESFORCO_Includesortasc"));
            Ddo_projeto_esforco_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_ESFORCO_Includesortdsc"));
            Ddo_projeto_esforco_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_ESFORCO_Includefilter"));
            Ddo_projeto_esforco_Filtertype = cgiGet( "DDO_PROJETO_ESFORCO_Filtertype");
            Ddo_projeto_esforco_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_ESFORCO_Filterisrange"));
            Ddo_projeto_esforco_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_ESFORCO_Includedatalist"));
            Ddo_projeto_esforco_Cleanfilter = cgiGet( "DDO_PROJETO_ESFORCO_Cleanfilter");
            Ddo_projeto_esforco_Rangefilterfrom = cgiGet( "DDO_PROJETO_ESFORCO_Rangefilterfrom");
            Ddo_projeto_esforco_Rangefilterto = cgiGet( "DDO_PROJETO_ESFORCO_Rangefilterto");
            Ddo_projeto_esforco_Searchbuttontext = cgiGet( "DDO_PROJETO_ESFORCO_Searchbuttontext");
            Ddo_projeto_status_Caption = cgiGet( "DDO_PROJETO_STATUS_Caption");
            Ddo_projeto_status_Tooltip = cgiGet( "DDO_PROJETO_STATUS_Tooltip");
            Ddo_projeto_status_Cls = cgiGet( "DDO_PROJETO_STATUS_Cls");
            Ddo_projeto_status_Selectedvalue_set = cgiGet( "DDO_PROJETO_STATUS_Selectedvalue_set");
            Ddo_projeto_status_Dropdownoptionstype = cgiGet( "DDO_PROJETO_STATUS_Dropdownoptionstype");
            Ddo_projeto_status_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETO_STATUS_Titlecontrolidtoreplace");
            Ddo_projeto_status_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_STATUS_Includesortasc"));
            Ddo_projeto_status_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_STATUS_Includesortdsc"));
            Ddo_projeto_status_Sortedstatus = cgiGet( "DDO_PROJETO_STATUS_Sortedstatus");
            Ddo_projeto_status_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_STATUS_Includefilter"));
            Ddo_projeto_status_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_STATUS_Includedatalist"));
            Ddo_projeto_status_Datalisttype = cgiGet( "DDO_PROJETO_STATUS_Datalisttype");
            Ddo_projeto_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_PROJETO_STATUS_Allowmultipleselection"));
            Ddo_projeto_status_Datalistfixedvalues = cgiGet( "DDO_PROJETO_STATUS_Datalistfixedvalues");
            Ddo_projeto_status_Sortasc = cgiGet( "DDO_PROJETO_STATUS_Sortasc");
            Ddo_projeto_status_Sortdsc = cgiGet( "DDO_PROJETO_STATUS_Sortdsc");
            Ddo_projeto_status_Cleanfilter = cgiGet( "DDO_PROJETO_STATUS_Cleanfilter");
            Ddo_projeto_status_Searchbuttontext = cgiGet( "DDO_PROJETO_STATUS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_projeto_nome_Activeeventkey = cgiGet( "DDO_PROJETO_NOME_Activeeventkey");
            Ddo_projeto_nome_Filteredtext_get = cgiGet( "DDO_PROJETO_NOME_Filteredtext_get");
            Ddo_projeto_nome_Selectedvalue_get = cgiGet( "DDO_PROJETO_NOME_Selectedvalue_get");
            Ddo_projeto_sigla_Activeeventkey = cgiGet( "DDO_PROJETO_SIGLA_Activeeventkey");
            Ddo_projeto_sigla_Filteredtext_get = cgiGet( "DDO_PROJETO_SIGLA_Filteredtext_get");
            Ddo_projeto_sigla_Selectedvalue_get = cgiGet( "DDO_PROJETO_SIGLA_Selectedvalue_get");
            Ddo_projeto_prazo_Activeeventkey = cgiGet( "DDO_PROJETO_PRAZO_Activeeventkey");
            Ddo_projeto_prazo_Filteredtext_get = cgiGet( "DDO_PROJETO_PRAZO_Filteredtext_get");
            Ddo_projeto_prazo_Filteredtextto_get = cgiGet( "DDO_PROJETO_PRAZO_Filteredtextto_get");
            Ddo_projeto_custo_Activeeventkey = cgiGet( "DDO_PROJETO_CUSTO_Activeeventkey");
            Ddo_projeto_custo_Filteredtext_get = cgiGet( "DDO_PROJETO_CUSTO_Filteredtext_get");
            Ddo_projeto_custo_Filteredtextto_get = cgiGet( "DDO_PROJETO_CUSTO_Filteredtextto_get");
            Ddo_projeto_incremental_Activeeventkey = cgiGet( "DDO_PROJETO_INCREMENTAL_Activeeventkey");
            Ddo_projeto_incremental_Selectedvalue_get = cgiGet( "DDO_PROJETO_INCREMENTAL_Selectedvalue_get");
            Ddo_projeto_esforco_Activeeventkey = cgiGet( "DDO_PROJETO_ESFORCO_Activeeventkey");
            Ddo_projeto_esforco_Filteredtext_get = cgiGet( "DDO_PROJETO_ESFORCO_Filteredtext_get");
            Ddo_projeto_esforco_Filteredtextto_get = cgiGet( "DDO_PROJETO_ESFORCO_Filteredtextto_get");
            Ddo_projeto_status_Activeeventkey = cgiGet( "DDO_PROJETO_STATUS_Activeeventkey");
            Ddo_projeto_status_Selectedvalue_get = cgiGet( "DDO_PROJETO_STATUS_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA1"), AV31Projeto_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME1"), AV17Projeto_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TIPOCONTAGEM1"), AV32Projeto_TipoContagem1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM1"), AV33Projeto_TecnicaContagem1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS1"), AV34Projeto_Status1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA2"), AV35Projeto_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME2"), AV21Projeto_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TIPOCONTAGEM2"), AV36Projeto_TipoContagem2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM2"), AV37Projeto_TecnicaContagem2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS2"), AV38Projeto_Status2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA3"), AV39Projeto_Sigla3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME3"), AV25Projeto_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TIPOCONTAGEM3"), AV40Projeto_TipoContagem3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM3"), AV41Projeto_TecnicaContagem3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS3"), AV42Projeto_Status3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_NOME"), AV47TFProjeto_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_NOME_SEL"), AV48TFProjeto_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_SIGLA"), AV51TFProjeto_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROJETO_SIGLA_SEL"), AV52TFProjeto_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_PRAZO"), ",", ".") != Convert.ToDecimal( AV55TFProjeto_Prazo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV56TFProjeto_Prazo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_CUSTO"), ",", ".") != AV59TFProjeto_Custo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_CUSTO_TO"), ",", ".") != AV60TFProjeto_Custo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_INCREMENTAL_SEL"), ",", ".") != Convert.ToDecimal( AV63TFProjeto_Incremental_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_ESFORCO"), ",", ".") != Convert.ToDecimal( AV66TFProjeto_Esforco )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETO_ESFORCO_TO"), ",", ".") != Convert.ToDecimal( AV67TFProjeto_Esforco_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E30DF2 */
         E30DF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30DF2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV32Projeto_TipoContagem1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Projeto_TipoContagem1", AV32Projeto_TipoContagem1);
         AV33Projeto_TecnicaContagem1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
         AV34Projeto_Status1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
         AV15DynamicFiltersSelector1 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36Projeto_TipoContagem2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Projeto_TipoContagem2", AV36Projeto_TipoContagem2);
         AV37Projeto_TecnicaContagem2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
         AV38Projeto_Status2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
         AV19DynamicFiltersSelector2 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV40Projeto_TipoContagem3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Projeto_TipoContagem3", AV40Projeto_TipoContagem3);
         AV41Projeto_TecnicaContagem3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
         AV42Projeto_Status3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
         AV23DynamicFiltersSelector3 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfprojeto_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_nome_Visible), 5, 0)));
         edtavTfprojeto_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_nome_sel_Visible), 5, 0)));
         edtavTfprojeto_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_sigla_Visible), 5, 0)));
         edtavTfprojeto_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_sigla_sel_Visible), 5, 0)));
         edtavTfprojeto_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_prazo_Visible), 5, 0)));
         edtavTfprojeto_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_prazo_to_Visible), 5, 0)));
         edtavTfprojeto_custo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_custo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_custo_Visible), 5, 0)));
         edtavTfprojeto_custo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_custo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_custo_to_Visible), 5, 0)));
         edtavTfprojeto_incremental_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_incremental_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_incremental_sel_Visible), 5, 0)));
         edtavTfprojeto_esforco_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_esforco_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_esforco_Visible), 5, 0)));
         edtavTfprojeto_esforco_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojeto_esforco_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojeto_esforco_to_Visible), 5, 0)));
         Ddo_projeto_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Projeto_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "TitleControlIdToReplace", Ddo_projeto_nome_Titlecontrolidtoreplace);
         AV49ddo_Projeto_NomeTitleControlIdToReplace = Ddo_projeto_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Projeto_NomeTitleControlIdToReplace", AV49ddo_Projeto_NomeTitleControlIdToReplace);
         edtavDdo_projeto_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projeto_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projeto_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_projeto_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Projeto_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "TitleControlIdToReplace", Ddo_projeto_sigla_Titlecontrolidtoreplace);
         AV53ddo_Projeto_SiglaTitleControlIdToReplace = Ddo_projeto_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Projeto_SiglaTitleControlIdToReplace", AV53ddo_Projeto_SiglaTitleControlIdToReplace);
         edtavDdo_projeto_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projeto_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projeto_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_projeto_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_Projeto_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_prazo_Internalname, "TitleControlIdToReplace", Ddo_projeto_prazo_Titlecontrolidtoreplace);
         AV57ddo_Projeto_PrazoTitleControlIdToReplace = Ddo_projeto_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Projeto_PrazoTitleControlIdToReplace", AV57ddo_Projeto_PrazoTitleControlIdToReplace);
         edtavDdo_projeto_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projeto_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projeto_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_projeto_custo_Titlecontrolidtoreplace = subGrid_Internalname+"_Projeto_Custo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_custo_Internalname, "TitleControlIdToReplace", Ddo_projeto_custo_Titlecontrolidtoreplace);
         AV61ddo_Projeto_CustoTitleControlIdToReplace = Ddo_projeto_custo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Projeto_CustoTitleControlIdToReplace", AV61ddo_Projeto_CustoTitleControlIdToReplace);
         edtavDdo_projeto_custotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projeto_custotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projeto_custotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_projeto_incremental_Titlecontrolidtoreplace = subGrid_Internalname+"_Projeto_Incremental";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_incremental_Internalname, "TitleControlIdToReplace", Ddo_projeto_incremental_Titlecontrolidtoreplace);
         AV64ddo_Projeto_IncrementalTitleControlIdToReplace = Ddo_projeto_incremental_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_Projeto_IncrementalTitleControlIdToReplace", AV64ddo_Projeto_IncrementalTitleControlIdToReplace);
         edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_projeto_esforco_Titlecontrolidtoreplace = subGrid_Internalname+"_Projeto_Esforco";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_esforco_Internalname, "TitleControlIdToReplace", Ddo_projeto_esforco_Titlecontrolidtoreplace);
         AV68ddo_Projeto_EsforcoTitleControlIdToReplace = Ddo_projeto_esforco_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Projeto_EsforcoTitleControlIdToReplace", AV68ddo_Projeto_EsforcoTitleControlIdToReplace);
         edtavDdo_projeto_esforcotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projeto_esforcotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projeto_esforcotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_projeto_status_Titlecontrolidtoreplace = subGrid_Internalname+"_Projeto_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_status_Internalname, "TitleControlIdToReplace", Ddo_projeto_status_Titlecontrolidtoreplace);
         AV72ddo_Projeto_StatusTitleControlIdToReplace = Ddo_projeto_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Projeto_StatusTitleControlIdToReplace", AV72ddo_Projeto_StatusTitleControlIdToReplace);
         edtavDdo_projeto_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projeto_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projeto_statustitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Projeto";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Sigla", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Incremental", 0);
         cmbavOrderedby.addItem("4", "Status", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV73DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV73DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E31DF2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV46Projeto_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Projeto_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Projeto_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Projeto_CustoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Projeto_IncrementalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Projeto_EsforcoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Projeto_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtProjeto_Nome_Titleformat = 2;
         edtProjeto_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV49ddo_Projeto_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Nome_Internalname, "Title", edtProjeto_Nome_Title);
         edtProjeto_Sigla_Titleformat = 2;
         edtProjeto_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV53ddo_Projeto_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Sigla_Internalname, "Title", edtProjeto_Sigla_Title);
         edtProjeto_Prazo_Titleformat = 2;
         edtProjeto_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prazo", AV57ddo_Projeto_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Prazo_Internalname, "Title", edtProjeto_Prazo_Title);
         edtProjeto_Custo_Titleformat = 2;
         edtProjeto_Custo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Custo", AV61ddo_Projeto_CustoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Custo_Internalname, "Title", edtProjeto_Custo_Title);
         chkProjeto_Incremental_Titleformat = 2;
         chkProjeto_Incremental.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Incremental", AV64ddo_Projeto_IncrementalTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "Title", chkProjeto_Incremental.Title.Text);
         edtProjeto_Esforco_Titleformat = 2;
         edtProjeto_Esforco_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Esfor�o", AV68ddo_Projeto_EsforcoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Esforco_Internalname, "Title", edtProjeto_Esforco_Title);
         cmbProjeto_Status_Titleformat = 2;
         cmbProjeto_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV72ddo_Projeto_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Title", cmbProjeto_Status.Title.Text);
         AV75GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75GridCurrentPage), 10, 0)));
         AV76GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV80WWProjetoDS_2_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV81WWProjetoDS_3_Projeto_nome1 = AV17Projeto_Nome1;
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = AV32Projeto_TipoContagem1;
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV84WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV85WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV87WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV88WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = AV36Projeto_TipoContagem2;
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_13_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_16_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_17_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = AV40Projeto_TipoContagem3;
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV98WWProjetoDS_20_Projeto_status3 = AV42Projeto_Status3;
         AV99WWProjetoDS_21_Tfprojeto_nome = AV47TFProjeto_Nome;
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = AV48TFProjeto_Nome_Sel;
         AV101WWProjetoDS_23_Tfprojeto_sigla = AV51TFProjeto_Sigla;
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = AV52TFProjeto_Sigla_Sel;
         AV103WWProjetoDS_25_Tfprojeto_prazo = AV55TFProjeto_Prazo;
         AV104WWProjetoDS_26_Tfprojeto_prazo_to = AV56TFProjeto_Prazo_To;
         AV105WWProjetoDS_27_Tfprojeto_custo = AV59TFProjeto_Custo;
         AV106WWProjetoDS_28_Tfprojeto_custo_to = AV60TFProjeto_Custo_To;
         AV107WWProjetoDS_29_Tfprojeto_incremental_sel = AV63TFProjeto_Incremental_Sel;
         AV108WWProjetoDS_30_Tfprojeto_esforco = AV66TFProjeto_Esforco;
         AV109WWProjetoDS_31_Tfprojeto_esforco_to = AV67TFProjeto_Esforco_To;
         AV110WWProjetoDS_32_Tfprojeto_status_sels = AV71TFProjeto_Status_Sels;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Projeto_NomeTitleFilterData", AV46Projeto_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Projeto_SiglaTitleFilterData", AV50Projeto_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54Projeto_PrazoTitleFilterData", AV54Projeto_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58Projeto_CustoTitleFilterData", AV58Projeto_CustoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62Projeto_IncrementalTitleFilterData", AV62Projeto_IncrementalTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65Projeto_EsforcoTitleFilterData", AV65Projeto_EsforcoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69Projeto_StatusTitleFilterData", AV69Projeto_StatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11DF2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV74PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV74PageToGo) ;
         }
      }

      protected void E12DF2( )
      {
         /* Ddo_projeto_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projeto_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "SortedStatus", Ddo_projeto_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "SortedStatus", Ddo_projeto_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFProjeto_Nome = Ddo_projeto_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProjeto_Nome", AV47TFProjeto_Nome);
            AV48TFProjeto_Nome_Sel = Ddo_projeto_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProjeto_Nome_Sel", AV48TFProjeto_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13DF2( )
      {
         /* Ddo_projeto_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projeto_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "SortedStatus", Ddo_projeto_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "SortedStatus", Ddo_projeto_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFProjeto_Sigla = Ddo_projeto_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProjeto_Sigla", AV51TFProjeto_Sigla);
            AV52TFProjeto_Sigla_Sel = Ddo_projeto_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProjeto_Sigla_Sel", AV52TFProjeto_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14DF2( )
      {
         /* Ddo_projeto_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projeto_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFProjeto_Prazo = (short)(NumberUtil.Val( Ddo_projeto_prazo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0)));
            AV56TFProjeto_Prazo_To = (short)(NumberUtil.Val( Ddo_projeto_prazo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E15DF2( )
      {
         /* Ddo_projeto_custo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projeto_custo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFProjeto_Custo = NumberUtil.Val( Ddo_projeto_custo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFProjeto_Custo", StringUtil.LTrim( StringUtil.Str( AV59TFProjeto_Custo, 12, 2)));
            AV60TFProjeto_Custo_To = NumberUtil.Val( Ddo_projeto_custo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFProjeto_Custo_To", StringUtil.LTrim( StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16DF2( )
      {
         /* Ddo_projeto_incremental_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projeto_incremental_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_incremental_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_incremental_Internalname, "SortedStatus", Ddo_projeto_incremental_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_incremental_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_incremental_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_incremental_Internalname, "SortedStatus", Ddo_projeto_incremental_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_incremental_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFProjeto_Incremental_Sel = (short)(NumberUtil.Val( Ddo_projeto_incremental_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFProjeto_Incremental_Sel", StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17DF2( )
      {
         /* Ddo_projeto_esforco_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projeto_esforco_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFProjeto_Esforco = (short)(NumberUtil.Val( Ddo_projeto_esforco_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFProjeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0)));
            AV67TFProjeto_Esforco_To = (short)(NumberUtil.Val( Ddo_projeto_esforco_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFProjeto_Esforco_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E18DF2( )
      {
         /* Ddo_projeto_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projeto_status_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_status_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_status_Internalname, "SortedStatus", Ddo_projeto_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_status_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projeto_status_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_status_Internalname, "SortedStatus", Ddo_projeto_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projeto_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFProjeto_Status_SelsJson = Ddo_projeto_status_Selectedvalue_get;
            AV71TFProjeto_Status_Sels.FromJSonString(AV70TFProjeto_Status_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71TFProjeto_Status_Sels", AV71TFProjeto_Status_Sels);
      }

      private void E32DF2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV111Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A648Projeto_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV112Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A648Projeto_Codigo);
         AV43Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV43Display);
         AV113Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Sub projetos, Estimativas";
         edtavDisplay_Link = formatLink("viewprojeto.aspx") + "?" + UrlEncode("" +A648Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtProjeto_Nome_Link = formatLink("viewprojeto.aspx") + "?" + UrlEncode("" +A648Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 85;
         }
         sendrow_852( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_85_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(85, GridRow);
         }
      }

      protected void E19DF2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E25DF2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E20DF2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tipocontagem1.CurrentValue = StringUtil.RTrim( AV32Projeto_TipoContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem1_Internalname, "Values", cmbavProjeto_tipocontagem1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tipocontagem2.CurrentValue = StringUtil.RTrim( AV36Projeto_TipoContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem2_Internalname, "Values", cmbavProjeto_tipocontagem2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tipocontagem3.CurrentValue = StringUtil.RTrim( AV40Projeto_TipoContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem3_Internalname, "Values", cmbavProjeto_tipocontagem3.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E26DF2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27DF2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E21DF2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tipocontagem1.CurrentValue = StringUtil.RTrim( AV32Projeto_TipoContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem1_Internalname, "Values", cmbavProjeto_tipocontagem1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tipocontagem2.CurrentValue = StringUtil.RTrim( AV36Projeto_TipoContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem2_Internalname, "Values", cmbavProjeto_tipocontagem2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tipocontagem3.CurrentValue = StringUtil.RTrim( AV40Projeto_TipoContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem3_Internalname, "Values", cmbavProjeto_tipocontagem3.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E28DF2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22DF2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV32Projeto_TipoContagem1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV36Projeto_TipoContagem2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV40Projeto_TipoContagem3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV47TFProjeto_Nome, AV48TFProjeto_Nome_Sel, AV51TFProjeto_Sigla, AV52TFProjeto_Sigla_Sel, AV55TFProjeto_Prazo, AV56TFProjeto_Prazo_To, AV59TFProjeto_Custo, AV60TFProjeto_Custo_To, AV63TFProjeto_Incremental_Sel, AV66TFProjeto_Esforco, AV67TFProjeto_Esforco_To, AV49ddo_Projeto_NomeTitleControlIdToReplace, AV53ddo_Projeto_SiglaTitleControlIdToReplace, AV57ddo_Projeto_PrazoTitleControlIdToReplace, AV61ddo_Projeto_CustoTitleControlIdToReplace, AV64ddo_Projeto_IncrementalTitleControlIdToReplace, AV68ddo_Projeto_EsforcoTitleControlIdToReplace, AV72ddo_Projeto_StatusTitleControlIdToReplace, AV71TFProjeto_Status_Sels, AV114Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tipocontagem1.CurrentValue = StringUtil.RTrim( AV32Projeto_TipoContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem1_Internalname, "Values", cmbavProjeto_tipocontagem1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tipocontagem2.CurrentValue = StringUtil.RTrim( AV36Projeto_TipoContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem2_Internalname, "Values", cmbavProjeto_tipocontagem2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tipocontagem3.CurrentValue = StringUtil.RTrim( AV40Projeto_TipoContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem3_Internalname, "Values", cmbavProjeto_tipocontagem3.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E29DF2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23DF2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71TFProjeto_Status_Sels", AV71TFProjeto_Status_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavProjeto_tipocontagem1.CurrentValue = StringUtil.RTrim( AV32Projeto_TipoContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem1_Internalname, "Values", cmbavProjeto_tipocontagem1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tipocontagem2.CurrentValue = StringUtil.RTrim( AV36Projeto_TipoContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem2_Internalname, "Values", cmbavProjeto_tipocontagem2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tipocontagem3.CurrentValue = StringUtil.RTrim( AV40Projeto_TipoContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem3_Internalname, "Values", cmbavProjeto_tipocontagem3.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E24DF2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_projeto_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "SortedStatus", Ddo_projeto_nome_Sortedstatus);
         Ddo_projeto_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "SortedStatus", Ddo_projeto_sigla_Sortedstatus);
         Ddo_projeto_incremental_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_incremental_Internalname, "SortedStatus", Ddo_projeto_incremental_Sortedstatus);
         Ddo_projeto_status_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_status_Internalname, "SortedStatus", Ddo_projeto_status_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_projeto_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "SortedStatus", Ddo_projeto_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_projeto_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "SortedStatus", Ddo_projeto_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_projeto_incremental_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_incremental_Internalname, "SortedStatus", Ddo_projeto_incremental_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_projeto_status_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_status_Internalname, "SortedStatus", Ddo_projeto_status_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavProjeto_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla1_Visible), 5, 0)));
         edtavProjeto_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome1_Visible), 5, 0)));
         cmbavProjeto_tipocontagem1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tipocontagem1.Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem1.Visible), 5, 0)));
         cmbavProjeto_status1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_NOME") == 0 )
         {
            edtavProjeto_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TIPOCONTAGEM") == 0 )
         {
            cmbavProjeto_tipocontagem1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tipocontagem1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavProjeto_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla2_Visible), 5, 0)));
         edtavProjeto_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome2_Visible), 5, 0)));
         cmbavProjeto_tipocontagem2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tipocontagem2.Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem2.Visible), 5, 0)));
         cmbavProjeto_status2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_NOME") == 0 )
         {
            edtavProjeto_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TIPOCONTAGEM") == 0 )
         {
            cmbavProjeto_tipocontagem2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tipocontagem2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavProjeto_sigla3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla3_Visible), 5, 0)));
         edtavProjeto_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome3_Visible), 5, 0)));
         cmbavProjeto_tipocontagem3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tipocontagem3.Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem3.Visible), 5, 0)));
         cmbavProjeto_status3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_NOME") == 0 )
         {
            edtavProjeto_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TIPOCONTAGEM") == 0 )
         {
            cmbavProjeto_tipocontagem3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tipocontagem3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV35Projeto_Sigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV39Projeto_Sigla3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV47TFProjeto_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProjeto_Nome", AV47TFProjeto_Nome);
         Ddo_projeto_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "FilteredText_set", Ddo_projeto_nome_Filteredtext_set);
         AV48TFProjeto_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProjeto_Nome_Sel", AV48TFProjeto_Nome_Sel);
         Ddo_projeto_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "SelectedValue_set", Ddo_projeto_nome_Selectedvalue_set);
         AV51TFProjeto_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProjeto_Sigla", AV51TFProjeto_Sigla);
         Ddo_projeto_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "FilteredText_set", Ddo_projeto_sigla_Filteredtext_set);
         AV52TFProjeto_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProjeto_Sigla_Sel", AV52TFProjeto_Sigla_Sel);
         Ddo_projeto_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "SelectedValue_set", Ddo_projeto_sigla_Selectedvalue_set);
         AV55TFProjeto_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0)));
         Ddo_projeto_prazo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_prazo_Internalname, "FilteredText_set", Ddo_projeto_prazo_Filteredtext_set);
         AV56TFProjeto_Prazo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0)));
         Ddo_projeto_prazo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_prazo_Internalname, "FilteredTextTo_set", Ddo_projeto_prazo_Filteredtextto_set);
         AV59TFProjeto_Custo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFProjeto_Custo", StringUtil.LTrim( StringUtil.Str( AV59TFProjeto_Custo, 12, 2)));
         Ddo_projeto_custo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_custo_Internalname, "FilteredText_set", Ddo_projeto_custo_Filteredtext_set);
         AV60TFProjeto_Custo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFProjeto_Custo_To", StringUtil.LTrim( StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2)));
         Ddo_projeto_custo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_custo_Internalname, "FilteredTextTo_set", Ddo_projeto_custo_Filteredtextto_set);
         AV63TFProjeto_Incremental_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFProjeto_Incremental_Sel", StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0));
         Ddo_projeto_incremental_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_incremental_Internalname, "SelectedValue_set", Ddo_projeto_incremental_Selectedvalue_set);
         AV66TFProjeto_Esforco = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFProjeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0)));
         Ddo_projeto_esforco_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_esforco_Internalname, "FilteredText_set", Ddo_projeto_esforco_Filteredtext_set);
         AV67TFProjeto_Esforco_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFProjeto_Esforco_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0)));
         Ddo_projeto_esforco_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_esforco_Internalname, "FilteredTextTo_set", Ddo_projeto_esforco_Filteredtextto_set);
         AV71TFProjeto_Status_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_projeto_status_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_status_Internalname, "SelectedValue_set", Ddo_projeto_status_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV31Projeto_Sigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV114Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV114Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV114Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV115GXV1 = 1;
         while ( AV115GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV115GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_NOME") == 0 )
            {
               AV47TFProjeto_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProjeto_Nome", AV47TFProjeto_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFProjeto_Nome)) )
               {
                  Ddo_projeto_nome_Filteredtext_set = AV47TFProjeto_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "FilteredText_set", Ddo_projeto_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_NOME_SEL") == 0 )
            {
               AV48TFProjeto_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProjeto_Nome_Sel", AV48TFProjeto_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFProjeto_Nome_Sel)) )
               {
                  Ddo_projeto_nome_Selectedvalue_set = AV48TFProjeto_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_nome_Internalname, "SelectedValue_set", Ddo_projeto_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_SIGLA") == 0 )
            {
               AV51TFProjeto_Sigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProjeto_Sigla", AV51TFProjeto_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFProjeto_Sigla)) )
               {
                  Ddo_projeto_sigla_Filteredtext_set = AV51TFProjeto_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "FilteredText_set", Ddo_projeto_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_SIGLA_SEL") == 0 )
            {
               AV52TFProjeto_Sigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProjeto_Sigla_Sel", AV52TFProjeto_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFProjeto_Sigla_Sel)) )
               {
                  Ddo_projeto_sigla_Selectedvalue_set = AV52TFProjeto_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_sigla_Internalname, "SelectedValue_set", Ddo_projeto_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_PRAZO") == 0 )
            {
               AV55TFProjeto_Prazo = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0)));
               AV56TFProjeto_Prazo_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0)));
               if ( ! (0==AV55TFProjeto_Prazo) )
               {
                  Ddo_projeto_prazo_Filteredtext_set = StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_prazo_Internalname, "FilteredText_set", Ddo_projeto_prazo_Filteredtext_set);
               }
               if ( ! (0==AV56TFProjeto_Prazo_To) )
               {
                  Ddo_projeto_prazo_Filteredtextto_set = StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_prazo_Internalname, "FilteredTextTo_set", Ddo_projeto_prazo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_CUSTO") == 0 )
            {
               AV59TFProjeto_Custo = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFProjeto_Custo", StringUtil.LTrim( StringUtil.Str( AV59TFProjeto_Custo, 12, 2)));
               AV60TFProjeto_Custo_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFProjeto_Custo_To", StringUtil.LTrim( StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV59TFProjeto_Custo) )
               {
                  Ddo_projeto_custo_Filteredtext_set = StringUtil.Str( AV59TFProjeto_Custo, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_custo_Internalname, "FilteredText_set", Ddo_projeto_custo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV60TFProjeto_Custo_To) )
               {
                  Ddo_projeto_custo_Filteredtextto_set = StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_custo_Internalname, "FilteredTextTo_set", Ddo_projeto_custo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_INCREMENTAL_SEL") == 0 )
            {
               AV63TFProjeto_Incremental_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFProjeto_Incremental_Sel", StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0));
               if ( ! (0==AV63TFProjeto_Incremental_Sel) )
               {
                  Ddo_projeto_incremental_Selectedvalue_set = StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_incremental_Internalname, "SelectedValue_set", Ddo_projeto_incremental_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_ESFORCO") == 0 )
            {
               AV66TFProjeto_Esforco = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFProjeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0)));
               AV67TFProjeto_Esforco_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFProjeto_Esforco_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0)));
               if ( ! (0==AV66TFProjeto_Esforco) )
               {
                  Ddo_projeto_esforco_Filteredtext_set = StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_esforco_Internalname, "FilteredText_set", Ddo_projeto_esforco_Filteredtext_set);
               }
               if ( ! (0==AV67TFProjeto_Esforco_To) )
               {
                  Ddo_projeto_esforco_Filteredtextto_set = StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_esforco_Internalname, "FilteredTextTo_set", Ddo_projeto_esforco_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETO_STATUS_SEL") == 0 )
            {
               AV70TFProjeto_Status_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV71TFProjeto_Status_Sels.FromJSonString(AV70TFProjeto_Status_SelsJson);
               if ( ! ( AV71TFProjeto_Status_Sels.Count == 0 ) )
               {
                  Ddo_projeto_status_Selectedvalue_set = AV70TFProjeto_Status_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projeto_status_Internalname, "SelectedValue_set", Ddo_projeto_status_Selectedvalue_set);
               }
            }
            AV115GXV1 = (int)(AV115GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
            {
               AV31Projeto_Sigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_NOME") == 0 )
            {
               AV17Projeto_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Projeto_Nome1", AV17Projeto_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TIPOCONTAGEM") == 0 )
            {
               AV32Projeto_TipoContagem1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Projeto_TipoContagem1", AV32Projeto_TipoContagem1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
            {
               AV33Projeto_TecnicaContagem1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
            {
               AV34Projeto_Status1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
               {
                  AV35Projeto_Sigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_NOME") == 0 )
               {
                  AV21Projeto_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Nome2", AV21Projeto_Nome2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TIPOCONTAGEM") == 0 )
               {
                  AV36Projeto_TipoContagem2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Projeto_TipoContagem2", AV36Projeto_TipoContagem2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
               {
                  AV37Projeto_TecnicaContagem2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
               {
                  AV38Projeto_Status2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
                  {
                     AV39Projeto_Sigla3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_NOME") == 0 )
                  {
                     AV25Projeto_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Projeto_Nome3", AV25Projeto_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TIPOCONTAGEM") == 0 )
                  {
                     AV40Projeto_TipoContagem3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Projeto_TipoContagem3", AV40Projeto_TipoContagem3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
                  {
                     AV41Projeto_TecnicaContagem3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
                  {
                     AV42Projeto_Status3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV114Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFProjeto_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFProjeto_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFProjeto_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFProjeto_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFProjeto_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFProjeto_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFProjeto_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFProjeto_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV55TFProjeto_Prazo) && (0==AV56TFProjeto_Prazo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_PRAZO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFProjeto_Prazo), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV56TFProjeto_Prazo_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV59TFProjeto_Custo) && (Convert.ToDecimal(0)==AV60TFProjeto_Custo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_CUSTO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV59TFProjeto_Custo, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV60TFProjeto_Custo_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV63TFProjeto_Incremental_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_INCREMENTAL_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV63TFProjeto_Incremental_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV66TFProjeto_Esforco) && (0==AV67TFProjeto_Esforco_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_ESFORCO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV66TFProjeto_Esforco), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV67TFProjeto_Esforco_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV71TFProjeto_Status_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETO_STATUS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFProjeto_Status_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV114Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Projeto_Sigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Projeto_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Projeto_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Projeto_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TIPOCONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Projeto_TipoContagem1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV32Projeto_TipoContagem1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Projeto_TecnicaContagem1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33Projeto_TecnicaContagem1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Projeto_Status1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34Projeto_Status1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Projeto_Sigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV35Projeto_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Projeto_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Projeto_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TIPOCONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Projeto_TipoContagem2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV36Projeto_TipoContagem2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Projeto_TecnicaContagem2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV37Projeto_TecnicaContagem2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Projeto_Status2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV38Projeto_Status2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Projeto_Sigla3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39Projeto_Sigla3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Projeto_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Projeto_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TIPOCONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Projeto_TipoContagem3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV40Projeto_TipoContagem3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Projeto_TecnicaContagem3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41Projeto_TecnicaContagem3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Projeto_Status3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV42Projeto_Status3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV114Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Projeto";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_DF2( true) ;
         }
         else
         {
            wb_table2_8_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_79_DF2( true) ;
         }
         else
         {
            wb_table3_79_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table3_79_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DF2e( true) ;
         }
         else
         {
            wb_table1_2_DF2e( false) ;
         }
      }

      protected void wb_table3_79_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_82_DF2( true) ;
         }
         else
         {
            wb_table4_82_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table4_82_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_79_DF2e( true) ;
         }
         else
         {
            wb_table3_79_DF2e( false) ;
         }
      }

      protected void wb_table4_82_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"85\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Projeto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(260), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjeto_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjeto_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjeto_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjeto_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjeto_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjeto_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjeto_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjeto_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjeto_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjeto_Custo_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjeto_Custo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjeto_Custo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkProjeto_Incremental_Titleformat == 0 )
               {
                  context.SendWebValue( chkProjeto_Incremental.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkProjeto_Incremental.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(45), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjeto_Esforco_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjeto_Esforco_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjeto_Esforco_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbProjeto_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbProjeto_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbProjeto_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV43Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A649Projeto_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjeto_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjeto_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtProjeto_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A650Projeto_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjeto_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjeto_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjeto_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjeto_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjeto_Custo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjeto_Custo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1232Projeto_Incremental));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkProjeto_Incremental.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkProjeto_Incremental_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjeto_Esforco_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjeto_Esforco_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A658Projeto_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbProjeto_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbProjeto_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 85 )
         {
            wbEnd = 0;
            nRC_GXsfl_85 = (short)(nGXsfl_85_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_82_DF2e( true) ;
         }
         else
         {
            wb_table4_82_DF2e( false) ;
         }
      }

      protected void wb_table2_8_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblProjetotitle_Internalname, "Projetos", "", "", lblProjetotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_DF2( true) ;
         }
         else
         {
            wb_table5_13_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWProjeto.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_DF2( true) ;
         }
         else
         {
            wb_table6_23_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DF2e( true) ;
         }
         else
         {
            wb_table2_8_DF2e( false) ;
         }
      }

      protected void wb_table6_23_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_DF2( true) ;
         }
         else
         {
            wb_table7_28_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_DF2e( true) ;
         }
         else
         {
            wb_table6_23_DF2e( false) ;
         }
      }

      protected void wb_table7_28_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWProjeto.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla1_Internalname, StringUtil.RTrim( AV31Projeto_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV31Projeto_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_nome1_Internalname, StringUtil.RTrim( AV17Projeto_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Projeto_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_nome1_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tipocontagem1, cmbavProjeto_tipocontagem1_Internalname, StringUtil.RTrim( AV32Projeto_TipoContagem1), 1, cmbavProjeto_tipocontagem1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tipocontagem1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tipocontagem1.CurrentValue = StringUtil.RTrim( AV32Projeto_TipoContagem1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem1_Internalname, "Values", (String)(cmbavProjeto_tipocontagem1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem1, cmbavProjeto_tecnicacontagem1_Internalname, StringUtil.RTrim( AV33Projeto_TecnicaContagem1), 1, cmbavProjeto_tecnicacontagem1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status1, cmbavProjeto_status1_Internalname, StringUtil.RTrim( AV34Projeto_Status1), 1, cmbavProjeto_status1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", (String)(cmbavProjeto_status1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WWProjeto.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla2_Internalname, StringUtil.RTrim( AV35Projeto_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV35Projeto_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_nome2_Internalname, StringUtil.RTrim( AV21Projeto_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21Projeto_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_nome2_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tipocontagem2, cmbavProjeto_tipocontagem2_Internalname, StringUtil.RTrim( AV36Projeto_TipoContagem2), 1, cmbavProjeto_tipocontagem2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tipocontagem2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tipocontagem2.CurrentValue = StringUtil.RTrim( AV36Projeto_TipoContagem2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem2_Internalname, "Values", (String)(cmbavProjeto_tipocontagem2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem2, cmbavProjeto_tecnicacontagem2_Internalname, StringUtil.RTrim( AV37Projeto_TecnicaContagem2), 1, cmbavProjeto_tecnicacontagem2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status2, cmbavProjeto_status2_Internalname, StringUtil.RTrim( AV38Projeto_Status2), 1, cmbavProjeto_status2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", (String)(cmbavProjeto_status2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_WWProjeto.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla3_Internalname, StringUtil.RTrim( AV39Projeto_Sigla3), StringUtil.RTrim( context.localUtil.Format( AV39Projeto_Sigla3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_nome3_Internalname, StringUtil.RTrim( AV25Projeto_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25Projeto_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_nome3_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tipocontagem3, cmbavProjeto_tipocontagem3_Internalname, StringUtil.RTrim( AV40Projeto_TipoContagem3), 1, cmbavProjeto_tipocontagem3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tipocontagem3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tipocontagem3.CurrentValue = StringUtil.RTrim( AV40Projeto_TipoContagem3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tipocontagem3_Internalname, "Values", (String)(cmbavProjeto_tipocontagem3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem3, cmbavProjeto_tecnicacontagem3_Internalname, StringUtil.RTrim( AV41Projeto_TecnicaContagem3), 1, cmbavProjeto_tecnicacontagem3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status3, cmbavProjeto_status3_Internalname, StringUtil.RTrim( AV42Projeto_Status3), 1, cmbavProjeto_status3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", (String)(cmbavProjeto_status3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_DF2e( true) ;
         }
         else
         {
            wb_table7_28_DF2e( false) ;
         }
      }

      protected void wb_table5_13_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_DF2e( true) ;
         }
         else
         {
            wb_table5_13_DF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADF2( ) ;
         WSDF2( ) ;
         WEDF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117453684");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwprojeto.js", "?20203117453685");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_852( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_85_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_85_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_85_idx;
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO_"+sGXsfl_85_idx;
         edtProjeto_Nome_Internalname = "PROJETO_NOME_"+sGXsfl_85_idx;
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA_"+sGXsfl_85_idx;
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO_"+sGXsfl_85_idx;
         edtProjeto_Custo_Internalname = "PROJETO_CUSTO_"+sGXsfl_85_idx;
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL_"+sGXsfl_85_idx;
         edtProjeto_Esforco_Internalname = "PROJETO_ESFORCO_"+sGXsfl_85_idx;
         cmbProjeto_Status_Internalname = "PROJETO_STATUS_"+sGXsfl_85_idx;
      }

      protected void SubsflControlProps_fel_852( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_85_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_85_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_85_fel_idx;
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO_"+sGXsfl_85_fel_idx;
         edtProjeto_Nome_Internalname = "PROJETO_NOME_"+sGXsfl_85_fel_idx;
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA_"+sGXsfl_85_fel_idx;
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO_"+sGXsfl_85_fel_idx;
         edtProjeto_Custo_Internalname = "PROJETO_CUSTO_"+sGXsfl_85_fel_idx;
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL_"+sGXsfl_85_fel_idx;
         edtProjeto_Esforco_Internalname = "PROJETO_ESFORCO_"+sGXsfl_85_fel_idx;
         cmbProjeto_Status_Internalname = "PROJETO_STATUS_"+sGXsfl_85_fel_idx;
      }

      protected void sendrow_852( )
      {
         SubsflControlProps_852( ) ;
         WBDF0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_85_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_85_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_85_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV111Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV111Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV112Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV43Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV43Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV113Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV43Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV43Display)) ? AV113Display_GXI : context.PathToRelativeUrl( AV43Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV43Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Nome_Internalname,StringUtil.RTrim( A649Projeto_Nome),StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtProjeto_Nome_Link,(String)"",(String)"",(String)"",(String)edtProjeto_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)260,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Sigla_Internalname,StringUtil.RTrim( A650Projeto_Sigla),StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A656Projeto_Prazo), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Custo_Internalname,StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 18, 2, ",", "")),context.localUtil.Format( A655Projeto_Custo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Custo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkProjeto_Incremental_Internalname,StringUtil.BoolToStr( A1232Projeto_Incremental),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Esforco_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A657Projeto_Esforco), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Esforco_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)45,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_85_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PROJETO_STATUS_" + sGXsfl_85_idx;
               cmbProjeto_Status.Name = GXCCtl;
               cmbProjeto_Status.WebTags = "";
               cmbProjeto_Status.addItem("A", "Aberto", 0);
               cmbProjeto_Status.addItem("E", "Em Contagem", 0);
               cmbProjeto_Status.addItem("C", "Contado", 0);
               if ( cmbProjeto_Status.ItemCount > 0 )
               {
                  A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbProjeto_Status,(String)cmbProjeto_Status_Internalname,StringUtil.RTrim( A658Projeto_Status),(short)1,(String)cmbProjeto_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_CODIGO"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_NOME"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_SIGLA"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_INCREMENTAL"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, A1232Projeto_Incremental));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_STATUS"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_85_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_85_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_85_idx+1));
            sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
            SubsflControlProps_852( ) ;
         }
         /* End function sendrow_852 */
      }

      protected void init_default_properties( )
      {
         lblProjetotitle_Internalname = "PROJETOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavProjeto_sigla1_Internalname = "vPROJETO_SIGLA1";
         edtavProjeto_nome1_Internalname = "vPROJETO_NOME1";
         cmbavProjeto_tipocontagem1_Internalname = "vPROJETO_TIPOCONTAGEM1";
         cmbavProjeto_tecnicacontagem1_Internalname = "vPROJETO_TECNICACONTAGEM1";
         cmbavProjeto_status1_Internalname = "vPROJETO_STATUS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavProjeto_sigla2_Internalname = "vPROJETO_SIGLA2";
         edtavProjeto_nome2_Internalname = "vPROJETO_NOME2";
         cmbavProjeto_tipocontagem2_Internalname = "vPROJETO_TIPOCONTAGEM2";
         cmbavProjeto_tecnicacontagem2_Internalname = "vPROJETO_TECNICACONTAGEM2";
         cmbavProjeto_status2_Internalname = "vPROJETO_STATUS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavProjeto_sigla3_Internalname = "vPROJETO_SIGLA3";
         edtavProjeto_nome3_Internalname = "vPROJETO_NOME3";
         cmbavProjeto_tipocontagem3_Internalname = "vPROJETO_TIPOCONTAGEM3";
         cmbavProjeto_tecnicacontagem3_Internalname = "vPROJETO_TECNICACONTAGEM3";
         cmbavProjeto_status3_Internalname = "vPROJETO_STATUS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO";
         edtProjeto_Nome_Internalname = "PROJETO_NOME";
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA";
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO";
         edtProjeto_Custo_Internalname = "PROJETO_CUSTO";
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL";
         edtProjeto_Esforco_Internalname = "PROJETO_ESFORCO";
         cmbProjeto_Status_Internalname = "PROJETO_STATUS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfprojeto_nome_Internalname = "vTFPROJETO_NOME";
         edtavTfprojeto_nome_sel_Internalname = "vTFPROJETO_NOME_SEL";
         edtavTfprojeto_sigla_Internalname = "vTFPROJETO_SIGLA";
         edtavTfprojeto_sigla_sel_Internalname = "vTFPROJETO_SIGLA_SEL";
         edtavTfprojeto_prazo_Internalname = "vTFPROJETO_PRAZO";
         edtavTfprojeto_prazo_to_Internalname = "vTFPROJETO_PRAZO_TO";
         edtavTfprojeto_custo_Internalname = "vTFPROJETO_CUSTO";
         edtavTfprojeto_custo_to_Internalname = "vTFPROJETO_CUSTO_TO";
         edtavTfprojeto_incremental_sel_Internalname = "vTFPROJETO_INCREMENTAL_SEL";
         edtavTfprojeto_esforco_Internalname = "vTFPROJETO_ESFORCO";
         edtavTfprojeto_esforco_to_Internalname = "vTFPROJETO_ESFORCO_TO";
         Ddo_projeto_nome_Internalname = "DDO_PROJETO_NOME";
         edtavDdo_projeto_nometitlecontrolidtoreplace_Internalname = "vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_projeto_sigla_Internalname = "DDO_PROJETO_SIGLA";
         edtavDdo_projeto_siglatitlecontrolidtoreplace_Internalname = "vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_projeto_prazo_Internalname = "DDO_PROJETO_PRAZO";
         edtavDdo_projeto_prazotitlecontrolidtoreplace_Internalname = "vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE";
         Ddo_projeto_custo_Internalname = "DDO_PROJETO_CUSTO";
         edtavDdo_projeto_custotitlecontrolidtoreplace_Internalname = "vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE";
         Ddo_projeto_incremental_Internalname = "DDO_PROJETO_INCREMENTAL";
         edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Internalname = "vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE";
         Ddo_projeto_esforco_Internalname = "DDO_PROJETO_ESFORCO";
         edtavDdo_projeto_esforcotitlecontrolidtoreplace_Internalname = "vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE";
         Ddo_projeto_status_Internalname = "DDO_PROJETO_STATUS";
         edtavDdo_projeto_statustitlecontrolidtoreplace_Internalname = "vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbProjeto_Status_Jsonclick = "";
         edtProjeto_Esforco_Jsonclick = "";
         edtProjeto_Custo_Jsonclick = "";
         edtProjeto_Prazo_Jsonclick = "";
         edtProjeto_Sigla_Jsonclick = "";
         edtProjeto_Nome_Jsonclick = "";
         edtProjeto_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         cmbavProjeto_status3_Jsonclick = "";
         cmbavProjeto_tecnicacontagem3_Jsonclick = "";
         cmbavProjeto_tipocontagem3_Jsonclick = "";
         edtavProjeto_nome3_Jsonclick = "";
         edtavProjeto_sigla3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavProjeto_status2_Jsonclick = "";
         cmbavProjeto_tecnicacontagem2_Jsonclick = "";
         cmbavProjeto_tipocontagem2_Jsonclick = "";
         edtavProjeto_nome2_Jsonclick = "";
         edtavProjeto_sigla2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavProjeto_status1_Jsonclick = "";
         cmbavProjeto_tecnicacontagem1_Jsonclick = "";
         cmbavProjeto_tipocontagem1_Jsonclick = "";
         edtavProjeto_nome1_Jsonclick = "";
         edtavProjeto_sigla1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtProjeto_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Sub projetos, Estimativas";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbProjeto_Status_Titleformat = 0;
         edtProjeto_Esforco_Titleformat = 0;
         chkProjeto_Incremental_Titleformat = 0;
         edtProjeto_Custo_Titleformat = 0;
         edtProjeto_Prazo_Titleformat = 0;
         edtProjeto_Sigla_Titleformat = 0;
         edtProjeto_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavProjeto_status3.Visible = 1;
         cmbavProjeto_tecnicacontagem3.Visible = 1;
         cmbavProjeto_tipocontagem3.Visible = 1;
         edtavProjeto_nome3_Visible = 1;
         edtavProjeto_sigla3_Visible = 1;
         cmbavProjeto_status2.Visible = 1;
         cmbavProjeto_tecnicacontagem2.Visible = 1;
         cmbavProjeto_tipocontagem2.Visible = 1;
         edtavProjeto_nome2_Visible = 1;
         edtavProjeto_sigla2_Visible = 1;
         cmbavProjeto_status1.Visible = 1;
         cmbavProjeto_tecnicacontagem1.Visible = 1;
         cmbavProjeto_tipocontagem1.Visible = 1;
         edtavProjeto_nome1_Visible = 1;
         edtavProjeto_sigla1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         cmbProjeto_Status.Title.Text = "Status";
         edtProjeto_Esforco_Title = "Esfor�o";
         chkProjeto_Incremental.Title.Text = "Incremental";
         edtProjeto_Custo_Title = "Custo";
         edtProjeto_Prazo_Title = "Prazo";
         edtProjeto_Sigla_Title = "Sigla";
         edtProjeto_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkProjeto_Incremental.Caption = "";
         edtavDdo_projeto_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_projeto_esforcotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_projeto_custotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_projeto_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_projeto_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_projeto_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfprojeto_esforco_to_Jsonclick = "";
         edtavTfprojeto_esforco_to_Visible = 1;
         edtavTfprojeto_esforco_Jsonclick = "";
         edtavTfprojeto_esforco_Visible = 1;
         edtavTfprojeto_incremental_sel_Jsonclick = "";
         edtavTfprojeto_incremental_sel_Visible = 1;
         edtavTfprojeto_custo_to_Jsonclick = "";
         edtavTfprojeto_custo_to_Visible = 1;
         edtavTfprojeto_custo_Jsonclick = "";
         edtavTfprojeto_custo_Visible = 1;
         edtavTfprojeto_prazo_to_Jsonclick = "";
         edtavTfprojeto_prazo_to_Visible = 1;
         edtavTfprojeto_prazo_Jsonclick = "";
         edtavTfprojeto_prazo_Visible = 1;
         edtavTfprojeto_sigla_sel_Jsonclick = "";
         edtavTfprojeto_sigla_sel_Visible = 1;
         edtavTfprojeto_sigla_Jsonclick = "";
         edtavTfprojeto_sigla_Visible = 1;
         edtavTfprojeto_nome_sel_Jsonclick = "";
         edtavTfprojeto_nome_sel_Visible = 1;
         edtavTfprojeto_nome_Jsonclick = "";
         edtavTfprojeto_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_projeto_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_projeto_status_Cleanfilter = "Limpar pesquisa";
         Ddo_projeto_status_Sortdsc = "Ordenar de Z � A";
         Ddo_projeto_status_Sortasc = "Ordenar de A � Z";
         Ddo_projeto_status_Datalistfixedvalues = "A:Aberto,E:Em Contagem,C:Contado";
         Ddo_projeto_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_projeto_status_Datalisttype = "FixedValues";
         Ddo_projeto_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_projeto_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_projeto_status_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_projeto_status_Includesortasc = Convert.ToBoolean( -1);
         Ddo_projeto_status_Titlecontrolidtoreplace = "";
         Ddo_projeto_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projeto_status_Cls = "ColumnSettings";
         Ddo_projeto_status_Tooltip = "Op��es";
         Ddo_projeto_status_Caption = "";
         Ddo_projeto_esforco_Searchbuttontext = "Pesquisar";
         Ddo_projeto_esforco_Rangefilterto = "At�";
         Ddo_projeto_esforco_Rangefilterfrom = "Desde";
         Ddo_projeto_esforco_Cleanfilter = "Limpar pesquisa";
         Ddo_projeto_esforco_Includedatalist = Convert.ToBoolean( 0);
         Ddo_projeto_esforco_Filterisrange = Convert.ToBoolean( -1);
         Ddo_projeto_esforco_Filtertype = "Numeric";
         Ddo_projeto_esforco_Includefilter = Convert.ToBoolean( -1);
         Ddo_projeto_esforco_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_projeto_esforco_Includesortasc = Convert.ToBoolean( 0);
         Ddo_projeto_esforco_Titlecontrolidtoreplace = "";
         Ddo_projeto_esforco_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projeto_esforco_Cls = "ColumnSettings";
         Ddo_projeto_esforco_Tooltip = "Op��es";
         Ddo_projeto_esforco_Caption = "";
         Ddo_projeto_incremental_Searchbuttontext = "Pesquisar";
         Ddo_projeto_incremental_Cleanfilter = "Limpar pesquisa";
         Ddo_projeto_incremental_Sortdsc = "Ordenar de Z � A";
         Ddo_projeto_incremental_Sortasc = "Ordenar de A � Z";
         Ddo_projeto_incremental_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_projeto_incremental_Datalisttype = "FixedValues";
         Ddo_projeto_incremental_Includedatalist = Convert.ToBoolean( -1);
         Ddo_projeto_incremental_Includefilter = Convert.ToBoolean( 0);
         Ddo_projeto_incremental_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_projeto_incremental_Includesortasc = Convert.ToBoolean( -1);
         Ddo_projeto_incremental_Titlecontrolidtoreplace = "";
         Ddo_projeto_incremental_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projeto_incremental_Cls = "ColumnSettings";
         Ddo_projeto_incremental_Tooltip = "Op��es";
         Ddo_projeto_incremental_Caption = "";
         Ddo_projeto_custo_Searchbuttontext = "Pesquisar";
         Ddo_projeto_custo_Rangefilterto = "At�";
         Ddo_projeto_custo_Rangefilterfrom = "Desde";
         Ddo_projeto_custo_Cleanfilter = "Limpar pesquisa";
         Ddo_projeto_custo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_projeto_custo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_projeto_custo_Filtertype = "Numeric";
         Ddo_projeto_custo_Includefilter = Convert.ToBoolean( -1);
         Ddo_projeto_custo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_projeto_custo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_projeto_custo_Titlecontrolidtoreplace = "";
         Ddo_projeto_custo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projeto_custo_Cls = "ColumnSettings";
         Ddo_projeto_custo_Tooltip = "Op��es";
         Ddo_projeto_custo_Caption = "";
         Ddo_projeto_prazo_Searchbuttontext = "Pesquisar";
         Ddo_projeto_prazo_Rangefilterto = "At�";
         Ddo_projeto_prazo_Rangefilterfrom = "Desde";
         Ddo_projeto_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_projeto_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_projeto_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_projeto_prazo_Filtertype = "Numeric";
         Ddo_projeto_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_projeto_prazo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_projeto_prazo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_projeto_prazo_Titlecontrolidtoreplace = "";
         Ddo_projeto_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projeto_prazo_Cls = "ColumnSettings";
         Ddo_projeto_prazo_Tooltip = "Op��es";
         Ddo_projeto_prazo_Caption = "";
         Ddo_projeto_sigla_Searchbuttontext = "Pesquisar";
         Ddo_projeto_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_projeto_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_projeto_sigla_Loadingdata = "Carregando dados...";
         Ddo_projeto_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_projeto_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_projeto_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_projeto_sigla_Datalistproc = "GetWWProjetoFilterData";
         Ddo_projeto_sigla_Datalisttype = "Dynamic";
         Ddo_projeto_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_projeto_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_projeto_sigla_Filtertype = "Character";
         Ddo_projeto_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_projeto_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_projeto_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_projeto_sigla_Titlecontrolidtoreplace = "";
         Ddo_projeto_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projeto_sigla_Cls = "ColumnSettings";
         Ddo_projeto_sigla_Tooltip = "Op��es";
         Ddo_projeto_sigla_Caption = "";
         Ddo_projeto_nome_Searchbuttontext = "Pesquisar";
         Ddo_projeto_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_projeto_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_projeto_nome_Loadingdata = "Carregando dados...";
         Ddo_projeto_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_projeto_nome_Sortasc = "Ordenar de A � Z";
         Ddo_projeto_nome_Datalistupdateminimumcharacters = 0;
         Ddo_projeto_nome_Datalistproc = "GetWWProjetoFilterData";
         Ddo_projeto_nome_Datalisttype = "Dynamic";
         Ddo_projeto_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_projeto_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_projeto_nome_Filtertype = "Character";
         Ddo_projeto_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_projeto_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_projeto_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_projeto_nome_Titlecontrolidtoreplace = "";
         Ddo_projeto_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projeto_nome_Cls = "ColumnSettings";
         Ddo_projeto_nome_Tooltip = "Op��es";
         Ddo_projeto_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Projeto";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV46Projeto_NomeTitleFilterData',fld:'vPROJETO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV50Projeto_SiglaTitleFilterData',fld:'vPROJETO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV54Projeto_PrazoTitleFilterData',fld:'vPROJETO_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58Projeto_CustoTitleFilterData',fld:'vPROJETO_CUSTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62Projeto_IncrementalTitleFilterData',fld:'vPROJETO_INCREMENTALTITLEFILTERDATA',pic:'',nv:null},{av:'AV65Projeto_EsforcoTitleFilterData',fld:'vPROJETO_ESFORCOTITLEFILTERDATA',pic:'',nv:null},{av:'AV69Projeto_StatusTitleFilterData',fld:'vPROJETO_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'edtProjeto_Nome_Titleformat',ctrl:'PROJETO_NOME',prop:'Titleformat'},{av:'edtProjeto_Nome_Title',ctrl:'PROJETO_NOME',prop:'Title'},{av:'edtProjeto_Sigla_Titleformat',ctrl:'PROJETO_SIGLA',prop:'Titleformat'},{av:'edtProjeto_Sigla_Title',ctrl:'PROJETO_SIGLA',prop:'Title'},{av:'edtProjeto_Prazo_Titleformat',ctrl:'PROJETO_PRAZO',prop:'Titleformat'},{av:'edtProjeto_Prazo_Title',ctrl:'PROJETO_PRAZO',prop:'Title'},{av:'edtProjeto_Custo_Titleformat',ctrl:'PROJETO_CUSTO',prop:'Titleformat'},{av:'edtProjeto_Custo_Title',ctrl:'PROJETO_CUSTO',prop:'Title'},{av:'chkProjeto_Incremental_Titleformat',ctrl:'PROJETO_INCREMENTAL',prop:'Titleformat'},{av:'chkProjeto_Incremental.Title.Text',ctrl:'PROJETO_INCREMENTAL',prop:'Title'},{av:'edtProjeto_Esforco_Titleformat',ctrl:'PROJETO_ESFORCO',prop:'Titleformat'},{av:'edtProjeto_Esforco_Title',ctrl:'PROJETO_ESFORCO',prop:'Title'},{av:'cmbProjeto_Status'},{av:'AV75GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV76GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PROJETO_NOME.ONOPTIONCLICKED","{handler:'E12DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_projeto_nome_Activeeventkey',ctrl:'DDO_PROJETO_NOME',prop:'ActiveEventKey'},{av:'Ddo_projeto_nome_Filteredtext_get',ctrl:'DDO_PROJETO_NOME',prop:'FilteredText_get'},{av:'Ddo_projeto_nome_Selectedvalue_get',ctrl:'DDO_PROJETO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_projeto_nome_Sortedstatus',ctrl:'DDO_PROJETO_NOME',prop:'SortedStatus'},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_projeto_sigla_Sortedstatus',ctrl:'DDO_PROJETO_SIGLA',prop:'SortedStatus'},{av:'Ddo_projeto_incremental_Sortedstatus',ctrl:'DDO_PROJETO_INCREMENTAL',prop:'SortedStatus'},{av:'Ddo_projeto_status_Sortedstatus',ctrl:'DDO_PROJETO_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PROJETO_SIGLA.ONOPTIONCLICKED","{handler:'E13DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_projeto_sigla_Activeeventkey',ctrl:'DDO_PROJETO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_projeto_sigla_Filteredtext_get',ctrl:'DDO_PROJETO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_projeto_sigla_Selectedvalue_get',ctrl:'DDO_PROJETO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_projeto_sigla_Sortedstatus',ctrl:'DDO_PROJETO_SIGLA',prop:'SortedStatus'},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_projeto_nome_Sortedstatus',ctrl:'DDO_PROJETO_NOME',prop:'SortedStatus'},{av:'Ddo_projeto_incremental_Sortedstatus',ctrl:'DDO_PROJETO_INCREMENTAL',prop:'SortedStatus'},{av:'Ddo_projeto_status_Sortedstatus',ctrl:'DDO_PROJETO_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PROJETO_PRAZO.ONOPTIONCLICKED","{handler:'E14DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_projeto_prazo_Activeeventkey',ctrl:'DDO_PROJETO_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_projeto_prazo_Filteredtext_get',ctrl:'DDO_PROJETO_PRAZO',prop:'FilteredText_get'},{av:'Ddo_projeto_prazo_Filteredtextto_get',ctrl:'DDO_PROJETO_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_PROJETO_CUSTO.ONOPTIONCLICKED","{handler:'E15DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_projeto_custo_Activeeventkey',ctrl:'DDO_PROJETO_CUSTO',prop:'ActiveEventKey'},{av:'Ddo_projeto_custo_Filteredtext_get',ctrl:'DDO_PROJETO_CUSTO',prop:'FilteredText_get'},{av:'Ddo_projeto_custo_Filteredtextto_get',ctrl:'DDO_PROJETO_CUSTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_PROJETO_INCREMENTAL.ONOPTIONCLICKED","{handler:'E16DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_projeto_incremental_Activeeventkey',ctrl:'DDO_PROJETO_INCREMENTAL',prop:'ActiveEventKey'},{av:'Ddo_projeto_incremental_Selectedvalue_get',ctrl:'DDO_PROJETO_INCREMENTAL',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_projeto_incremental_Sortedstatus',ctrl:'DDO_PROJETO_INCREMENTAL',prop:'SortedStatus'},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'Ddo_projeto_nome_Sortedstatus',ctrl:'DDO_PROJETO_NOME',prop:'SortedStatus'},{av:'Ddo_projeto_sigla_Sortedstatus',ctrl:'DDO_PROJETO_SIGLA',prop:'SortedStatus'},{av:'Ddo_projeto_status_Sortedstatus',ctrl:'DDO_PROJETO_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PROJETO_ESFORCO.ONOPTIONCLICKED","{handler:'E17DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_projeto_esforco_Activeeventkey',ctrl:'DDO_PROJETO_ESFORCO',prop:'ActiveEventKey'},{av:'Ddo_projeto_esforco_Filteredtext_get',ctrl:'DDO_PROJETO_ESFORCO',prop:'FilteredText_get'},{av:'Ddo_projeto_esforco_Filteredtextto_get',ctrl:'DDO_PROJETO_ESFORCO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_PROJETO_STATUS.ONOPTIONCLICKED","{handler:'E18DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_projeto_status_Activeeventkey',ctrl:'DDO_PROJETO_STATUS',prop:'ActiveEventKey'},{av:'Ddo_projeto_status_Selectedvalue_get',ctrl:'DDO_PROJETO_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_projeto_status_Sortedstatus',ctrl:'DDO_PROJETO_STATUS',prop:'SortedStatus'},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'Ddo_projeto_nome_Sortedstatus',ctrl:'DDO_PROJETO_NOME',prop:'SortedStatus'},{av:'Ddo_projeto_sigla_Sortedstatus',ctrl:'DDO_PROJETO_SIGLA',prop:'SortedStatus'},{av:'Ddo_projeto_incremental_Sortedstatus',ctrl:'DDO_PROJETO_INCREMENTAL',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E32DF2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV43Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtProjeto_Nome_Link',ctrl:'PROJETO_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E19DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E25DF2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E20DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tipocontagem2'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tipocontagem3'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tipocontagem1'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E26DF2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tipocontagem1'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E27DF2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E21DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tipocontagem2'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tipocontagem3'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tipocontagem1'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E28DF2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tipocontagem2'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E22DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tipocontagem2'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tipocontagem3'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tipocontagem1'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E29DF2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tipocontagem3'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E23DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'AV49ddo_Projeto_NomeTitleControlIdToReplace',fld:'vDDO_PROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Projeto_SiglaTitleControlIdToReplace',fld:'vDDO_PROJETO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Projeto_PrazoTitleControlIdToReplace',fld:'vDDO_PROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Projeto_CustoTitleControlIdToReplace',fld:'vDDO_PROJETO_CUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Projeto_IncrementalTitleControlIdToReplace',fld:'vDDO_PROJETO_INCREMENTALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Projeto_EsforcoTitleControlIdToReplace',fld:'vDDO_PROJETO_ESFORCOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Projeto_StatusTitleControlIdToReplace',fld:'vDDO_PROJETO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV47TFProjeto_Nome',fld:'vTFPROJETO_NOME',pic:'@!',nv:''},{av:'Ddo_projeto_nome_Filteredtext_set',ctrl:'DDO_PROJETO_NOME',prop:'FilteredText_set'},{av:'AV48TFProjeto_Nome_Sel',fld:'vTFPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_projeto_nome_Selectedvalue_set',ctrl:'DDO_PROJETO_NOME',prop:'SelectedValue_set'},{av:'AV51TFProjeto_Sigla',fld:'vTFPROJETO_SIGLA',pic:'@!',nv:''},{av:'Ddo_projeto_sigla_Filteredtext_set',ctrl:'DDO_PROJETO_SIGLA',prop:'FilteredText_set'},{av:'AV52TFProjeto_Sigla_Sel',fld:'vTFPROJETO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_projeto_sigla_Selectedvalue_set',ctrl:'DDO_PROJETO_SIGLA',prop:'SelectedValue_set'},{av:'AV55TFProjeto_Prazo',fld:'vTFPROJETO_PRAZO',pic:'ZZZ9',nv:0},{av:'Ddo_projeto_prazo_Filteredtext_set',ctrl:'DDO_PROJETO_PRAZO',prop:'FilteredText_set'},{av:'AV56TFProjeto_Prazo_To',fld:'vTFPROJETO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_projeto_prazo_Filteredtextto_set',ctrl:'DDO_PROJETO_PRAZO',prop:'FilteredTextTo_set'},{av:'AV59TFProjeto_Custo',fld:'vTFPROJETO_CUSTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_projeto_custo_Filteredtext_set',ctrl:'DDO_PROJETO_CUSTO',prop:'FilteredText_set'},{av:'AV60TFProjeto_Custo_To',fld:'vTFPROJETO_CUSTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_projeto_custo_Filteredtextto_set',ctrl:'DDO_PROJETO_CUSTO',prop:'FilteredTextTo_set'},{av:'AV63TFProjeto_Incremental_Sel',fld:'vTFPROJETO_INCREMENTAL_SEL',pic:'9',nv:0},{av:'Ddo_projeto_incremental_Selectedvalue_set',ctrl:'DDO_PROJETO_INCREMENTAL',prop:'SelectedValue_set'},{av:'AV66TFProjeto_Esforco',fld:'vTFPROJETO_ESFORCO',pic:'ZZZ9',nv:0},{av:'Ddo_projeto_esforco_Filteredtext_set',ctrl:'DDO_PROJETO_ESFORCO',prop:'FilteredText_set'},{av:'AV67TFProjeto_Esforco_To',fld:'vTFPROJETO_ESFORCO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_projeto_esforco_Filteredtextto_set',ctrl:'DDO_PROJETO_ESFORCO',prop:'FilteredTextTo_set'},{av:'AV71TFProjeto_Status_Sels',fld:'vTFPROJETO_STATUS_SELS',pic:'',nv:null},{av:'Ddo_projeto_status_Selectedvalue_set',ctrl:'DDO_PROJETO_STATUS',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tipocontagem1'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV32Projeto_TipoContagem1',fld:'vPROJETO_TIPOCONTAGEM1',pic:'',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV36Projeto_TipoContagem2',fld:'vPROJETO_TIPOCONTAGEM2',pic:'',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV40Projeto_TipoContagem3',fld:'vPROJETO_TIPOCONTAGEM3',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tipocontagem2'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tipocontagem3'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E24DF2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_projeto_nome_Activeeventkey = "";
         Ddo_projeto_nome_Filteredtext_get = "";
         Ddo_projeto_nome_Selectedvalue_get = "";
         Ddo_projeto_sigla_Activeeventkey = "";
         Ddo_projeto_sigla_Filteredtext_get = "";
         Ddo_projeto_sigla_Selectedvalue_get = "";
         Ddo_projeto_prazo_Activeeventkey = "";
         Ddo_projeto_prazo_Filteredtext_get = "";
         Ddo_projeto_prazo_Filteredtextto_get = "";
         Ddo_projeto_custo_Activeeventkey = "";
         Ddo_projeto_custo_Filteredtext_get = "";
         Ddo_projeto_custo_Filteredtextto_get = "";
         Ddo_projeto_incremental_Activeeventkey = "";
         Ddo_projeto_incremental_Selectedvalue_get = "";
         Ddo_projeto_esforco_Activeeventkey = "";
         Ddo_projeto_esforco_Filteredtext_get = "";
         Ddo_projeto_esforco_Filteredtextto_get = "";
         Ddo_projeto_status_Activeeventkey = "";
         Ddo_projeto_status_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV31Projeto_Sigla1 = "";
         AV17Projeto_Nome1 = "";
         AV32Projeto_TipoContagem1 = "";
         AV33Projeto_TecnicaContagem1 = "";
         AV34Projeto_Status1 = "A";
         AV19DynamicFiltersSelector2 = "";
         AV35Projeto_Sigla2 = "";
         AV21Projeto_Nome2 = "";
         AV36Projeto_TipoContagem2 = "";
         AV37Projeto_TecnicaContagem2 = "";
         AV38Projeto_Status2 = "A";
         AV23DynamicFiltersSelector3 = "";
         AV39Projeto_Sigla3 = "";
         AV25Projeto_Nome3 = "";
         AV40Projeto_TipoContagem3 = "";
         AV41Projeto_TecnicaContagem3 = "";
         AV42Projeto_Status3 = "A";
         AV47TFProjeto_Nome = "";
         AV48TFProjeto_Nome_Sel = "";
         AV51TFProjeto_Sigla = "";
         AV52TFProjeto_Sigla_Sel = "";
         AV49ddo_Projeto_NomeTitleControlIdToReplace = "";
         AV53ddo_Projeto_SiglaTitleControlIdToReplace = "";
         AV57ddo_Projeto_PrazoTitleControlIdToReplace = "";
         AV61ddo_Projeto_CustoTitleControlIdToReplace = "";
         AV64ddo_Projeto_IncrementalTitleControlIdToReplace = "";
         AV68ddo_Projeto_EsforcoTitleControlIdToReplace = "";
         AV72ddo_Projeto_StatusTitleControlIdToReplace = "";
         AV71TFProjeto_Status_Sels = new GxSimpleCollection();
         AV114Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV73DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV46Projeto_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Projeto_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Projeto_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Projeto_CustoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Projeto_IncrementalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Projeto_EsforcoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Projeto_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_projeto_nome_Filteredtext_set = "";
         Ddo_projeto_nome_Selectedvalue_set = "";
         Ddo_projeto_nome_Sortedstatus = "";
         Ddo_projeto_sigla_Filteredtext_set = "";
         Ddo_projeto_sigla_Selectedvalue_set = "";
         Ddo_projeto_sigla_Sortedstatus = "";
         Ddo_projeto_prazo_Filteredtext_set = "";
         Ddo_projeto_prazo_Filteredtextto_set = "";
         Ddo_projeto_custo_Filteredtext_set = "";
         Ddo_projeto_custo_Filteredtextto_set = "";
         Ddo_projeto_incremental_Selectedvalue_set = "";
         Ddo_projeto_incremental_Sortedstatus = "";
         Ddo_projeto_esforco_Filteredtext_set = "";
         Ddo_projeto_esforco_Filteredtextto_set = "";
         Ddo_projeto_status_Selectedvalue_set = "";
         Ddo_projeto_status_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV111Update_GXI = "";
         AV29Delete = "";
         AV112Delete_GXI = "";
         AV43Display = "";
         AV113Display_GXI = "";
         A649Projeto_Nome = "";
         A650Projeto_Sigla = "";
         A658Projeto_Status = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV110WWProjetoDS_32_Tfprojeto_status_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV80WWProjetoDS_2_Projeto_sigla1 = "";
         lV81WWProjetoDS_3_Projeto_nome1 = "";
         lV87WWProjetoDS_9_Projeto_sigla2 = "";
         lV88WWProjetoDS_10_Projeto_nome2 = "";
         lV94WWProjetoDS_16_Projeto_sigla3 = "";
         lV95WWProjetoDS_17_Projeto_nome3 = "";
         lV99WWProjetoDS_21_Tfprojeto_nome = "";
         lV101WWProjetoDS_23_Tfprojeto_sigla = "";
         AV79WWProjetoDS_1_Dynamicfiltersselector1 = "";
         AV80WWProjetoDS_2_Projeto_sigla1 = "";
         AV81WWProjetoDS_3_Projeto_nome1 = "";
         AV82WWProjetoDS_4_Projeto_tipocontagem1 = "";
         AV83WWProjetoDS_5_Projeto_tecnicacontagem1 = "";
         AV84WWProjetoDS_6_Projeto_status1 = "";
         AV86WWProjetoDS_8_Dynamicfiltersselector2 = "";
         AV87WWProjetoDS_9_Projeto_sigla2 = "";
         AV88WWProjetoDS_10_Projeto_nome2 = "";
         AV89WWProjetoDS_11_Projeto_tipocontagem2 = "";
         AV90WWProjetoDS_12_Projeto_tecnicacontagem2 = "";
         AV91WWProjetoDS_13_Projeto_status2 = "";
         AV93WWProjetoDS_15_Dynamicfiltersselector3 = "";
         AV94WWProjetoDS_16_Projeto_sigla3 = "";
         AV95WWProjetoDS_17_Projeto_nome3 = "";
         AV96WWProjetoDS_18_Projeto_tipocontagem3 = "";
         AV97WWProjetoDS_19_Projeto_tecnicacontagem3 = "";
         AV98WWProjetoDS_20_Projeto_status3 = "";
         AV100WWProjetoDS_22_Tfprojeto_nome_sel = "";
         AV99WWProjetoDS_21_Tfprojeto_nome = "";
         AV102WWProjetoDS_24_Tfprojeto_sigla_sel = "";
         AV101WWProjetoDS_23_Tfprojeto_sigla = "";
         A651Projeto_TipoContagem = "";
         A652Projeto_TecnicaContagem = "";
         H00DF3_A652Projeto_TecnicaContagem = new String[] {""} ;
         H00DF3_A651Projeto_TipoContagem = new String[] {""} ;
         H00DF3_A658Projeto_Status = new String[] {""} ;
         H00DF3_A1232Projeto_Incremental = new bool[] {false} ;
         H00DF3_n1232Projeto_Incremental = new bool[] {false} ;
         H00DF3_A650Projeto_Sigla = new String[] {""} ;
         H00DF3_A649Projeto_Nome = new String[] {""} ;
         H00DF3_A648Projeto_Codigo = new int[1] ;
         H00DF3_A657Projeto_Esforco = new short[1] ;
         H00DF3_A655Projeto_Custo = new decimal[1] ;
         H00DF3_A656Projeto_Prazo = new short[1] ;
         H00DF5_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV70TFProjeto_Status_SelsJson = "";
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblProjetotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwprojeto__default(),
            new Object[][] {
                new Object[] {
               H00DF3_A652Projeto_TecnicaContagem, H00DF3_A651Projeto_TipoContagem, H00DF3_A658Projeto_Status, H00DF3_A1232Projeto_Incremental, H00DF3_n1232Projeto_Incremental, H00DF3_A650Projeto_Sigla, H00DF3_A649Projeto_Nome, H00DF3_A648Projeto_Codigo, H00DF3_A657Projeto_Esforco, H00DF3_A655Projeto_Custo,
               H00DF3_A656Projeto_Prazo
               }
               , new Object[] {
               H00DF5_AGRID_nRecordCount
               }
            }
         );
         AV114Pgmname = "WWProjeto";
         /* GeneXus formulas. */
         AV114Pgmname = "WWProjeto";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_85 ;
      private short nGXsfl_85_idx=1 ;
      private short AV13OrderedBy ;
      private short AV55TFProjeto_Prazo ;
      private short AV56TFProjeto_Prazo_To ;
      private short AV63TFProjeto_Incremental_Sel ;
      private short AV66TFProjeto_Esforco ;
      private short AV67TFProjeto_Esforco_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A656Projeto_Prazo ;
      private short A657Projeto_Esforco ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_85_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV103WWProjetoDS_25_Tfprojeto_prazo ;
      private short AV104WWProjetoDS_26_Tfprojeto_prazo_to ;
      private short AV107WWProjetoDS_29_Tfprojeto_incremental_sel ;
      private short AV108WWProjetoDS_30_Tfprojeto_esforco ;
      private short AV109WWProjetoDS_31_Tfprojeto_esforco_to ;
      private short edtProjeto_Nome_Titleformat ;
      private short edtProjeto_Sigla_Titleformat ;
      private short edtProjeto_Prazo_Titleformat ;
      private short edtProjeto_Custo_Titleformat ;
      private short chkProjeto_Incremental_Titleformat ;
      private short edtProjeto_Esforco_Titleformat ;
      private short cmbProjeto_Status_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A648Projeto_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_projeto_nome_Datalistupdateminimumcharacters ;
      private int Ddo_projeto_sigla_Datalistupdateminimumcharacters ;
      private int edtavTfprojeto_nome_Visible ;
      private int edtavTfprojeto_nome_sel_Visible ;
      private int edtavTfprojeto_sigla_Visible ;
      private int edtavTfprojeto_sigla_sel_Visible ;
      private int edtavTfprojeto_prazo_Visible ;
      private int edtavTfprojeto_prazo_to_Visible ;
      private int edtavTfprojeto_custo_Visible ;
      private int edtavTfprojeto_custo_to_Visible ;
      private int edtavTfprojeto_incremental_sel_Visible ;
      private int edtavTfprojeto_esforco_Visible ;
      private int edtavTfprojeto_esforco_to_Visible ;
      private int edtavDdo_projeto_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_projeto_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_projeto_prazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_projeto_custotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_projeto_esforcotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_projeto_statustitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV110WWProjetoDS_32_Tfprojeto_status_sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV74PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavProjeto_sigla1_Visible ;
      private int edtavProjeto_nome1_Visible ;
      private int edtavProjeto_sigla2_Visible ;
      private int edtavProjeto_nome2_Visible ;
      private int edtavProjeto_sigla3_Visible ;
      private int edtavProjeto_nome3_Visible ;
      private int AV115GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV75GridCurrentPage ;
      private long AV76GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV59TFProjeto_Custo ;
      private decimal AV60TFProjeto_Custo_To ;
      private decimal A655Projeto_Custo ;
      private decimal AV105WWProjetoDS_27_Tfprojeto_custo ;
      private decimal AV106WWProjetoDS_28_Tfprojeto_custo_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_projeto_nome_Activeeventkey ;
      private String Ddo_projeto_nome_Filteredtext_get ;
      private String Ddo_projeto_nome_Selectedvalue_get ;
      private String Ddo_projeto_sigla_Activeeventkey ;
      private String Ddo_projeto_sigla_Filteredtext_get ;
      private String Ddo_projeto_sigla_Selectedvalue_get ;
      private String Ddo_projeto_prazo_Activeeventkey ;
      private String Ddo_projeto_prazo_Filteredtext_get ;
      private String Ddo_projeto_prazo_Filteredtextto_get ;
      private String Ddo_projeto_custo_Activeeventkey ;
      private String Ddo_projeto_custo_Filteredtext_get ;
      private String Ddo_projeto_custo_Filteredtextto_get ;
      private String Ddo_projeto_incremental_Activeeventkey ;
      private String Ddo_projeto_incremental_Selectedvalue_get ;
      private String Ddo_projeto_esforco_Activeeventkey ;
      private String Ddo_projeto_esforco_Filteredtext_get ;
      private String Ddo_projeto_esforco_Filteredtextto_get ;
      private String Ddo_projeto_status_Activeeventkey ;
      private String Ddo_projeto_status_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_85_idx="0001" ;
      private String AV31Projeto_Sigla1 ;
      private String AV17Projeto_Nome1 ;
      private String AV32Projeto_TipoContagem1 ;
      private String AV33Projeto_TecnicaContagem1 ;
      private String AV34Projeto_Status1 ;
      private String AV35Projeto_Sigla2 ;
      private String AV21Projeto_Nome2 ;
      private String AV36Projeto_TipoContagem2 ;
      private String AV37Projeto_TecnicaContagem2 ;
      private String AV38Projeto_Status2 ;
      private String AV39Projeto_Sigla3 ;
      private String AV25Projeto_Nome3 ;
      private String AV40Projeto_TipoContagem3 ;
      private String AV41Projeto_TecnicaContagem3 ;
      private String AV42Projeto_Status3 ;
      private String AV47TFProjeto_Nome ;
      private String AV48TFProjeto_Nome_Sel ;
      private String AV51TFProjeto_Sigla ;
      private String AV52TFProjeto_Sigla_Sel ;
      private String AV114Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_projeto_nome_Caption ;
      private String Ddo_projeto_nome_Tooltip ;
      private String Ddo_projeto_nome_Cls ;
      private String Ddo_projeto_nome_Filteredtext_set ;
      private String Ddo_projeto_nome_Selectedvalue_set ;
      private String Ddo_projeto_nome_Dropdownoptionstype ;
      private String Ddo_projeto_nome_Titlecontrolidtoreplace ;
      private String Ddo_projeto_nome_Sortedstatus ;
      private String Ddo_projeto_nome_Filtertype ;
      private String Ddo_projeto_nome_Datalisttype ;
      private String Ddo_projeto_nome_Datalistproc ;
      private String Ddo_projeto_nome_Sortasc ;
      private String Ddo_projeto_nome_Sortdsc ;
      private String Ddo_projeto_nome_Loadingdata ;
      private String Ddo_projeto_nome_Cleanfilter ;
      private String Ddo_projeto_nome_Noresultsfound ;
      private String Ddo_projeto_nome_Searchbuttontext ;
      private String Ddo_projeto_sigla_Caption ;
      private String Ddo_projeto_sigla_Tooltip ;
      private String Ddo_projeto_sigla_Cls ;
      private String Ddo_projeto_sigla_Filteredtext_set ;
      private String Ddo_projeto_sigla_Selectedvalue_set ;
      private String Ddo_projeto_sigla_Dropdownoptionstype ;
      private String Ddo_projeto_sigla_Titlecontrolidtoreplace ;
      private String Ddo_projeto_sigla_Sortedstatus ;
      private String Ddo_projeto_sigla_Filtertype ;
      private String Ddo_projeto_sigla_Datalisttype ;
      private String Ddo_projeto_sigla_Datalistproc ;
      private String Ddo_projeto_sigla_Sortasc ;
      private String Ddo_projeto_sigla_Sortdsc ;
      private String Ddo_projeto_sigla_Loadingdata ;
      private String Ddo_projeto_sigla_Cleanfilter ;
      private String Ddo_projeto_sigla_Noresultsfound ;
      private String Ddo_projeto_sigla_Searchbuttontext ;
      private String Ddo_projeto_prazo_Caption ;
      private String Ddo_projeto_prazo_Tooltip ;
      private String Ddo_projeto_prazo_Cls ;
      private String Ddo_projeto_prazo_Filteredtext_set ;
      private String Ddo_projeto_prazo_Filteredtextto_set ;
      private String Ddo_projeto_prazo_Dropdownoptionstype ;
      private String Ddo_projeto_prazo_Titlecontrolidtoreplace ;
      private String Ddo_projeto_prazo_Filtertype ;
      private String Ddo_projeto_prazo_Cleanfilter ;
      private String Ddo_projeto_prazo_Rangefilterfrom ;
      private String Ddo_projeto_prazo_Rangefilterto ;
      private String Ddo_projeto_prazo_Searchbuttontext ;
      private String Ddo_projeto_custo_Caption ;
      private String Ddo_projeto_custo_Tooltip ;
      private String Ddo_projeto_custo_Cls ;
      private String Ddo_projeto_custo_Filteredtext_set ;
      private String Ddo_projeto_custo_Filteredtextto_set ;
      private String Ddo_projeto_custo_Dropdownoptionstype ;
      private String Ddo_projeto_custo_Titlecontrolidtoreplace ;
      private String Ddo_projeto_custo_Filtertype ;
      private String Ddo_projeto_custo_Cleanfilter ;
      private String Ddo_projeto_custo_Rangefilterfrom ;
      private String Ddo_projeto_custo_Rangefilterto ;
      private String Ddo_projeto_custo_Searchbuttontext ;
      private String Ddo_projeto_incremental_Caption ;
      private String Ddo_projeto_incremental_Tooltip ;
      private String Ddo_projeto_incremental_Cls ;
      private String Ddo_projeto_incremental_Selectedvalue_set ;
      private String Ddo_projeto_incremental_Dropdownoptionstype ;
      private String Ddo_projeto_incremental_Titlecontrolidtoreplace ;
      private String Ddo_projeto_incremental_Sortedstatus ;
      private String Ddo_projeto_incremental_Datalisttype ;
      private String Ddo_projeto_incremental_Datalistfixedvalues ;
      private String Ddo_projeto_incremental_Sortasc ;
      private String Ddo_projeto_incremental_Sortdsc ;
      private String Ddo_projeto_incremental_Cleanfilter ;
      private String Ddo_projeto_incremental_Searchbuttontext ;
      private String Ddo_projeto_esforco_Caption ;
      private String Ddo_projeto_esforco_Tooltip ;
      private String Ddo_projeto_esforco_Cls ;
      private String Ddo_projeto_esforco_Filteredtext_set ;
      private String Ddo_projeto_esforco_Filteredtextto_set ;
      private String Ddo_projeto_esforco_Dropdownoptionstype ;
      private String Ddo_projeto_esforco_Titlecontrolidtoreplace ;
      private String Ddo_projeto_esforco_Filtertype ;
      private String Ddo_projeto_esforco_Cleanfilter ;
      private String Ddo_projeto_esforco_Rangefilterfrom ;
      private String Ddo_projeto_esforco_Rangefilterto ;
      private String Ddo_projeto_esforco_Searchbuttontext ;
      private String Ddo_projeto_status_Caption ;
      private String Ddo_projeto_status_Tooltip ;
      private String Ddo_projeto_status_Cls ;
      private String Ddo_projeto_status_Selectedvalue_set ;
      private String Ddo_projeto_status_Dropdownoptionstype ;
      private String Ddo_projeto_status_Titlecontrolidtoreplace ;
      private String Ddo_projeto_status_Sortedstatus ;
      private String Ddo_projeto_status_Datalisttype ;
      private String Ddo_projeto_status_Datalistfixedvalues ;
      private String Ddo_projeto_status_Sortasc ;
      private String Ddo_projeto_status_Sortdsc ;
      private String Ddo_projeto_status_Cleanfilter ;
      private String Ddo_projeto_status_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfprojeto_nome_Internalname ;
      private String edtavTfprojeto_nome_Jsonclick ;
      private String edtavTfprojeto_nome_sel_Internalname ;
      private String edtavTfprojeto_nome_sel_Jsonclick ;
      private String edtavTfprojeto_sigla_Internalname ;
      private String edtavTfprojeto_sigla_Jsonclick ;
      private String edtavTfprojeto_sigla_sel_Internalname ;
      private String edtavTfprojeto_sigla_sel_Jsonclick ;
      private String edtavTfprojeto_prazo_Internalname ;
      private String edtavTfprojeto_prazo_Jsonclick ;
      private String edtavTfprojeto_prazo_to_Internalname ;
      private String edtavTfprojeto_prazo_to_Jsonclick ;
      private String edtavTfprojeto_custo_Internalname ;
      private String edtavTfprojeto_custo_Jsonclick ;
      private String edtavTfprojeto_custo_to_Internalname ;
      private String edtavTfprojeto_custo_to_Jsonclick ;
      private String edtavTfprojeto_incremental_sel_Internalname ;
      private String edtavTfprojeto_incremental_sel_Jsonclick ;
      private String edtavTfprojeto_esforco_Internalname ;
      private String edtavTfprojeto_esforco_Jsonclick ;
      private String edtavTfprojeto_esforco_to_Internalname ;
      private String edtavTfprojeto_esforco_to_Jsonclick ;
      private String edtavDdo_projeto_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_projeto_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_projeto_prazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_projeto_custotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_projeto_incrementaltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_projeto_esforcotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_projeto_statustitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtProjeto_Codigo_Internalname ;
      private String A649Projeto_Nome ;
      private String edtProjeto_Nome_Internalname ;
      private String A650Projeto_Sigla ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_Prazo_Internalname ;
      private String edtProjeto_Custo_Internalname ;
      private String chkProjeto_Incremental_Internalname ;
      private String edtProjeto_Esforco_Internalname ;
      private String cmbProjeto_Status_Internalname ;
      private String A658Projeto_Status ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV80WWProjetoDS_2_Projeto_sigla1 ;
      private String lV81WWProjetoDS_3_Projeto_nome1 ;
      private String lV87WWProjetoDS_9_Projeto_sigla2 ;
      private String lV88WWProjetoDS_10_Projeto_nome2 ;
      private String lV94WWProjetoDS_16_Projeto_sigla3 ;
      private String lV95WWProjetoDS_17_Projeto_nome3 ;
      private String lV99WWProjetoDS_21_Tfprojeto_nome ;
      private String lV101WWProjetoDS_23_Tfprojeto_sigla ;
      private String AV80WWProjetoDS_2_Projeto_sigla1 ;
      private String AV81WWProjetoDS_3_Projeto_nome1 ;
      private String AV82WWProjetoDS_4_Projeto_tipocontagem1 ;
      private String AV83WWProjetoDS_5_Projeto_tecnicacontagem1 ;
      private String AV84WWProjetoDS_6_Projeto_status1 ;
      private String AV87WWProjetoDS_9_Projeto_sigla2 ;
      private String AV88WWProjetoDS_10_Projeto_nome2 ;
      private String AV89WWProjetoDS_11_Projeto_tipocontagem2 ;
      private String AV90WWProjetoDS_12_Projeto_tecnicacontagem2 ;
      private String AV91WWProjetoDS_13_Projeto_status2 ;
      private String AV94WWProjetoDS_16_Projeto_sigla3 ;
      private String AV95WWProjetoDS_17_Projeto_nome3 ;
      private String AV96WWProjetoDS_18_Projeto_tipocontagem3 ;
      private String AV97WWProjetoDS_19_Projeto_tecnicacontagem3 ;
      private String AV98WWProjetoDS_20_Projeto_status3 ;
      private String AV100WWProjetoDS_22_Tfprojeto_nome_sel ;
      private String AV99WWProjetoDS_21_Tfprojeto_nome ;
      private String AV102WWProjetoDS_24_Tfprojeto_sigla_sel ;
      private String AV101WWProjetoDS_23_Tfprojeto_sigla ;
      private String A651Projeto_TipoContagem ;
      private String A652Projeto_TecnicaContagem ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavProjeto_sigla1_Internalname ;
      private String edtavProjeto_nome1_Internalname ;
      private String cmbavProjeto_tipocontagem1_Internalname ;
      private String cmbavProjeto_tecnicacontagem1_Internalname ;
      private String cmbavProjeto_status1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavProjeto_sigla2_Internalname ;
      private String edtavProjeto_nome2_Internalname ;
      private String cmbavProjeto_tipocontagem2_Internalname ;
      private String cmbavProjeto_tecnicacontagem2_Internalname ;
      private String cmbavProjeto_status2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavProjeto_sigla3_Internalname ;
      private String edtavProjeto_nome3_Internalname ;
      private String cmbavProjeto_tipocontagem3_Internalname ;
      private String cmbavProjeto_tecnicacontagem3_Internalname ;
      private String cmbavProjeto_status3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_projeto_nome_Internalname ;
      private String Ddo_projeto_sigla_Internalname ;
      private String Ddo_projeto_prazo_Internalname ;
      private String Ddo_projeto_custo_Internalname ;
      private String Ddo_projeto_incremental_Internalname ;
      private String Ddo_projeto_esforco_Internalname ;
      private String Ddo_projeto_status_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtProjeto_Nome_Title ;
      private String edtProjeto_Sigla_Title ;
      private String edtProjeto_Prazo_Title ;
      private String edtProjeto_Custo_Title ;
      private String edtProjeto_Esforco_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtProjeto_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblProjetotitle_Internalname ;
      private String lblProjetotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavProjeto_sigla1_Jsonclick ;
      private String edtavProjeto_nome1_Jsonclick ;
      private String cmbavProjeto_tipocontagem1_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem1_Jsonclick ;
      private String cmbavProjeto_status1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavProjeto_sigla2_Jsonclick ;
      private String edtavProjeto_nome2_Jsonclick ;
      private String cmbavProjeto_tipocontagem2_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem2_Jsonclick ;
      private String cmbavProjeto_status2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavProjeto_sigla3_Jsonclick ;
      private String edtavProjeto_nome3_Jsonclick ;
      private String cmbavProjeto_tipocontagem3_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem3_Jsonclick ;
      private String cmbavProjeto_status3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_85_fel_idx="0001" ;
      private String ROClassString ;
      private String edtProjeto_Codigo_Jsonclick ;
      private String edtProjeto_Nome_Jsonclick ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String edtProjeto_Prazo_Jsonclick ;
      private String edtProjeto_Custo_Jsonclick ;
      private String edtProjeto_Esforco_Jsonclick ;
      private String cmbProjeto_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_projeto_nome_Includesortasc ;
      private bool Ddo_projeto_nome_Includesortdsc ;
      private bool Ddo_projeto_nome_Includefilter ;
      private bool Ddo_projeto_nome_Filterisrange ;
      private bool Ddo_projeto_nome_Includedatalist ;
      private bool Ddo_projeto_sigla_Includesortasc ;
      private bool Ddo_projeto_sigla_Includesortdsc ;
      private bool Ddo_projeto_sigla_Includefilter ;
      private bool Ddo_projeto_sigla_Filterisrange ;
      private bool Ddo_projeto_sigla_Includedatalist ;
      private bool Ddo_projeto_prazo_Includesortasc ;
      private bool Ddo_projeto_prazo_Includesortdsc ;
      private bool Ddo_projeto_prazo_Includefilter ;
      private bool Ddo_projeto_prazo_Filterisrange ;
      private bool Ddo_projeto_prazo_Includedatalist ;
      private bool Ddo_projeto_custo_Includesortasc ;
      private bool Ddo_projeto_custo_Includesortdsc ;
      private bool Ddo_projeto_custo_Includefilter ;
      private bool Ddo_projeto_custo_Filterisrange ;
      private bool Ddo_projeto_custo_Includedatalist ;
      private bool Ddo_projeto_incremental_Includesortasc ;
      private bool Ddo_projeto_incremental_Includesortdsc ;
      private bool Ddo_projeto_incremental_Includefilter ;
      private bool Ddo_projeto_incremental_Includedatalist ;
      private bool Ddo_projeto_esforco_Includesortasc ;
      private bool Ddo_projeto_esforco_Includesortdsc ;
      private bool Ddo_projeto_esforco_Includefilter ;
      private bool Ddo_projeto_esforco_Filterisrange ;
      private bool Ddo_projeto_esforco_Includedatalist ;
      private bool Ddo_projeto_status_Includesortasc ;
      private bool Ddo_projeto_status_Includesortdsc ;
      private bool Ddo_projeto_status_Includefilter ;
      private bool Ddo_projeto_status_Includedatalist ;
      private bool Ddo_projeto_status_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1232Projeto_Incremental ;
      private bool n1232Projeto_Incremental ;
      private bool AV85WWProjetoDS_7_Dynamicfiltersenabled2 ;
      private bool AV92WWProjetoDS_14_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV43Display_IsBlob ;
      private String AV70TFProjeto_Status_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV49ddo_Projeto_NomeTitleControlIdToReplace ;
      private String AV53ddo_Projeto_SiglaTitleControlIdToReplace ;
      private String AV57ddo_Projeto_PrazoTitleControlIdToReplace ;
      private String AV61ddo_Projeto_CustoTitleControlIdToReplace ;
      private String AV64ddo_Projeto_IncrementalTitleControlIdToReplace ;
      private String AV68ddo_Projeto_EsforcoTitleControlIdToReplace ;
      private String AV72ddo_Projeto_StatusTitleControlIdToReplace ;
      private String AV111Update_GXI ;
      private String AV112Delete_GXI ;
      private String AV113Display_GXI ;
      private String AV79WWProjetoDS_1_Dynamicfiltersselector1 ;
      private String AV86WWProjetoDS_8_Dynamicfiltersselector2 ;
      private String AV93WWProjetoDS_15_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV43Display ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavProjeto_tipocontagem1 ;
      private GXCombobox cmbavProjeto_tecnicacontagem1 ;
      private GXCombobox cmbavProjeto_status1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavProjeto_tipocontagem2 ;
      private GXCombobox cmbavProjeto_tecnicacontagem2 ;
      private GXCombobox cmbavProjeto_status2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavProjeto_tipocontagem3 ;
      private GXCombobox cmbavProjeto_tecnicacontagem3 ;
      private GXCombobox cmbavProjeto_status3 ;
      private GXCheckbox chkProjeto_Incremental ;
      private GXCombobox cmbProjeto_Status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00DF3_A652Projeto_TecnicaContagem ;
      private String[] H00DF3_A651Projeto_TipoContagem ;
      private String[] H00DF3_A658Projeto_Status ;
      private bool[] H00DF3_A1232Projeto_Incremental ;
      private bool[] H00DF3_n1232Projeto_Incremental ;
      private String[] H00DF3_A650Projeto_Sigla ;
      private String[] H00DF3_A649Projeto_Nome ;
      private int[] H00DF3_A648Projeto_Codigo ;
      private short[] H00DF3_A657Projeto_Esforco ;
      private decimal[] H00DF3_A655Projeto_Custo ;
      private short[] H00DF3_A656Projeto_Prazo ;
      private long[] H00DF5_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV71TFProjeto_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV110WWProjetoDS_32_Tfprojeto_status_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Projeto_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50Projeto_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54Projeto_PrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58Projeto_CustoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62Projeto_IncrementalTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65Projeto_EsforcoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69Projeto_StatusTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV73DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwprojeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DF3( IGxContext context ,
                                             String A658Projeto_Status ,
                                             IGxCollection AV110WWProjetoDS_32_Tfprojeto_status_sels ,
                                             String AV79WWProjetoDS_1_Dynamicfiltersselector1 ,
                                             String AV80WWProjetoDS_2_Projeto_sigla1 ,
                                             String AV81WWProjetoDS_3_Projeto_nome1 ,
                                             String AV82WWProjetoDS_4_Projeto_tipocontagem1 ,
                                             String AV83WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                             String AV84WWProjetoDS_6_Projeto_status1 ,
                                             bool AV85WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                             String AV86WWProjetoDS_8_Dynamicfiltersselector2 ,
                                             String AV87WWProjetoDS_9_Projeto_sigla2 ,
                                             String AV88WWProjetoDS_10_Projeto_nome2 ,
                                             String AV89WWProjetoDS_11_Projeto_tipocontagem2 ,
                                             String AV90WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                             String AV91WWProjetoDS_13_Projeto_status2 ,
                                             bool AV92WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                             String AV93WWProjetoDS_15_Dynamicfiltersselector3 ,
                                             String AV94WWProjetoDS_16_Projeto_sigla3 ,
                                             String AV95WWProjetoDS_17_Projeto_nome3 ,
                                             String AV96WWProjetoDS_18_Projeto_tipocontagem3 ,
                                             String AV97WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                             String AV98WWProjetoDS_20_Projeto_status3 ,
                                             String AV100WWProjetoDS_22_Tfprojeto_nome_sel ,
                                             String AV99WWProjetoDS_21_Tfprojeto_nome ,
                                             String AV102WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                             String AV101WWProjetoDS_23_Tfprojeto_sigla ,
                                             short AV103WWProjetoDS_25_Tfprojeto_prazo ,
                                             short AV104WWProjetoDS_26_Tfprojeto_prazo_to ,
                                             decimal AV105WWProjetoDS_27_Tfprojeto_custo ,
                                             decimal AV106WWProjetoDS_28_Tfprojeto_custo_to ,
                                             short AV107WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                             short AV108WWProjetoDS_30_Tfprojeto_esforco ,
                                             short AV109WWProjetoDS_31_Tfprojeto_esforco_to ,
                                             int AV110WWProjetoDS_32_Tfprojeto_status_sels_Count ,
                                             String A650Projeto_Sigla ,
                                             String A649Projeto_Nome ,
                                             String A651Projeto_TipoContagem ,
                                             String A652Projeto_TecnicaContagem ,
                                             short A656Projeto_Prazo ,
                                             decimal A655Projeto_Custo ,
                                             bool A1232Projeto_Incremental ,
                                             short A657Projeto_Esforco ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [30] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Projeto_TecnicaContagem], T1.[Projeto_TipoContagem], T1.[Projeto_Status], T1.[Projeto_Incremental], T1.[Projeto_Sigla], T1.[Projeto_Nome], T1.[Projeto_Codigo], COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo";
         sFromString = " FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Custo]) AS Projeto_Custo, SUM([Sistema_Prazo]) AS Projeto_Prazo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = T1.[Projeto_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWProjetoDS_2_Projeto_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWProjetoDS_3_Projeto_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_3_Projeto_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_3_Projeto_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWProjetoDS_4_Projeto_tipocontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_4_Projeto_tipocontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_4_Projeto_tipocontagem1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWProjetoDS_5_Projeto_tecnicacontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWProjetoDS_6_Projeto_status1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV84WWProjetoDS_6_Projeto_status1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV84WWProjetoDS_6_Projeto_status1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWProjetoDS_9_Projeto_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV87WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV87WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_10_Projeto_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV88WWProjetoDS_10_Projeto_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV88WWProjetoDS_10_Projeto_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWProjetoDS_11_Projeto_tipocontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV89WWProjetoDS_11_Projeto_tipocontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV89WWProjetoDS_11_Projeto_tipocontagem2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWProjetoDS_12_Projeto_tecnicacontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV90WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV90WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWProjetoDS_13_Projeto_status2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV91WWProjetoDS_13_Projeto_status2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV91WWProjetoDS_13_Projeto_status2)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWProjetoDS_16_Projeto_sigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV94WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV94WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWProjetoDS_17_Projeto_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV95WWProjetoDS_17_Projeto_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV95WWProjetoDS_17_Projeto_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWProjetoDS_18_Projeto_tipocontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV96WWProjetoDS_18_Projeto_tipocontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV96WWProjetoDS_18_Projeto_tipocontagem3)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWProjetoDS_19_Projeto_tecnicacontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV97WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV97WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWProjetoDS_20_Projeto_status3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV98WWProjetoDS_20_Projeto_status3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV98WWProjetoDS_20_Projeto_status3)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWProjetoDS_22_Tfprojeto_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWProjetoDS_21_Tfprojeto_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like @lV99WWProjetoDS_21_Tfprojeto_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like @lV99WWProjetoDS_21_Tfprojeto_nome)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWProjetoDS_22_Tfprojeto_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] = @AV100WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] = @AV100WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWProjetoDS_24_Tfprojeto_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWProjetoDS_23_Tfprojeto_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like @lV101WWProjetoDS_23_Tfprojeto_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like @lV101WWProjetoDS_23_Tfprojeto_sigla)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWProjetoDS_24_Tfprojeto_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] = @AV102WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] = @AV102WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (0==AV103WWProjetoDS_25_Tfprojeto_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) >= @AV103WWProjetoDS_25_Tfprojeto_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) >= @AV103WWProjetoDS_25_Tfprojeto_prazo)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (0==AV104WWProjetoDS_26_Tfprojeto_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) <= @AV104WWProjetoDS_26_Tfprojeto_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) <= @AV104WWProjetoDS_26_Tfprojeto_prazo_to)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWProjetoDS_27_Tfprojeto_custo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Custo], 0) >= @AV105WWProjetoDS_27_Tfprojeto_custo)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Custo], 0) >= @AV105WWProjetoDS_27_Tfprojeto_custo)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWProjetoDS_28_Tfprojeto_custo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Custo], 0) <= @AV106WWProjetoDS_28_Tfprojeto_custo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Custo], 0) <= @AV106WWProjetoDS_28_Tfprojeto_custo_to)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV107WWProjetoDS_29_Tfprojeto_incremental_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 1)";
            }
         }
         if ( AV107WWProjetoDS_29_Tfprojeto_incremental_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 0)";
            }
         }
         if ( ! (0==AV108WWProjetoDS_30_Tfprojeto_esforco) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Esforco], 0) >= @AV108WWProjetoDS_30_Tfprojeto_esforco)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Esforco], 0) >= @AV108WWProjetoDS_30_Tfprojeto_esforco)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (0==AV109WWProjetoDS_31_Tfprojeto_esforco_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Esforco], 0) <= @AV109WWProjetoDS_31_Tfprojeto_esforco_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Esforco], 0) <= @AV109WWProjetoDS_31_Tfprojeto_esforco_to)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV110WWProjetoDS_32_Tfprojeto_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Sigla]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Incremental]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Incremental] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Status]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Status] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Projeto_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00DF5( IGxContext context ,
                                             String A658Projeto_Status ,
                                             IGxCollection AV110WWProjetoDS_32_Tfprojeto_status_sels ,
                                             String AV79WWProjetoDS_1_Dynamicfiltersselector1 ,
                                             String AV80WWProjetoDS_2_Projeto_sigla1 ,
                                             String AV81WWProjetoDS_3_Projeto_nome1 ,
                                             String AV82WWProjetoDS_4_Projeto_tipocontagem1 ,
                                             String AV83WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                             String AV84WWProjetoDS_6_Projeto_status1 ,
                                             bool AV85WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                             String AV86WWProjetoDS_8_Dynamicfiltersselector2 ,
                                             String AV87WWProjetoDS_9_Projeto_sigla2 ,
                                             String AV88WWProjetoDS_10_Projeto_nome2 ,
                                             String AV89WWProjetoDS_11_Projeto_tipocontagem2 ,
                                             String AV90WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                             String AV91WWProjetoDS_13_Projeto_status2 ,
                                             bool AV92WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                             String AV93WWProjetoDS_15_Dynamicfiltersselector3 ,
                                             String AV94WWProjetoDS_16_Projeto_sigla3 ,
                                             String AV95WWProjetoDS_17_Projeto_nome3 ,
                                             String AV96WWProjetoDS_18_Projeto_tipocontagem3 ,
                                             String AV97WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                             String AV98WWProjetoDS_20_Projeto_status3 ,
                                             String AV100WWProjetoDS_22_Tfprojeto_nome_sel ,
                                             String AV99WWProjetoDS_21_Tfprojeto_nome ,
                                             String AV102WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                             String AV101WWProjetoDS_23_Tfprojeto_sigla ,
                                             short AV103WWProjetoDS_25_Tfprojeto_prazo ,
                                             short AV104WWProjetoDS_26_Tfprojeto_prazo_to ,
                                             decimal AV105WWProjetoDS_27_Tfprojeto_custo ,
                                             decimal AV106WWProjetoDS_28_Tfprojeto_custo_to ,
                                             short AV107WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                             short AV108WWProjetoDS_30_Tfprojeto_esforco ,
                                             short AV109WWProjetoDS_31_Tfprojeto_esforco_to ,
                                             int AV110WWProjetoDS_32_Tfprojeto_status_sels_Count ,
                                             String A650Projeto_Sigla ,
                                             String A649Projeto_Nome ,
                                             String A651Projeto_TipoContagem ,
                                             String A652Projeto_TecnicaContagem ,
                                             short A656Projeto_Prazo ,
                                             decimal A655Projeto_Custo ,
                                             bool A1232Projeto_Incremental ,
                                             short A657Projeto_Esforco ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Custo]) AS Projeto_Custo, SUM([Sistema_Prazo]) AS Projeto_Prazo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = T1.[Projeto_Codigo])";
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWProjetoDS_2_Projeto_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWProjetoDS_3_Projeto_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_3_Projeto_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_3_Projeto_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWProjetoDS_4_Projeto_tipocontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_4_Projeto_tipocontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_4_Projeto_tipocontagem1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWProjetoDS_5_Projeto_tecnicacontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV79WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWProjetoDS_6_Projeto_status1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV84WWProjetoDS_6_Projeto_status1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV84WWProjetoDS_6_Projeto_status1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWProjetoDS_9_Projeto_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV87WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV87WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_10_Projeto_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV88WWProjetoDS_10_Projeto_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV88WWProjetoDS_10_Projeto_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWProjetoDS_11_Projeto_tipocontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV89WWProjetoDS_11_Projeto_tipocontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV89WWProjetoDS_11_Projeto_tipocontagem2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWProjetoDS_12_Projeto_tecnicacontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV90WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV90WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV85WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV86WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWProjetoDS_13_Projeto_status2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV91WWProjetoDS_13_Projeto_status2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV91WWProjetoDS_13_Projeto_status2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWProjetoDS_16_Projeto_sigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV94WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV94WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWProjetoDS_17_Projeto_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV95WWProjetoDS_17_Projeto_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV95WWProjetoDS_17_Projeto_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWProjetoDS_18_Projeto_tipocontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV96WWProjetoDS_18_Projeto_tipocontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV96WWProjetoDS_18_Projeto_tipocontagem3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWProjetoDS_19_Projeto_tecnicacontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV97WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV97WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV92WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWProjetoDS_20_Projeto_status3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV98WWProjetoDS_20_Projeto_status3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV98WWProjetoDS_20_Projeto_status3)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWProjetoDS_22_Tfprojeto_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWProjetoDS_21_Tfprojeto_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like @lV99WWProjetoDS_21_Tfprojeto_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like @lV99WWProjetoDS_21_Tfprojeto_nome)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWProjetoDS_22_Tfprojeto_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] = @AV100WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] = @AV100WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWProjetoDS_24_Tfprojeto_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWProjetoDS_23_Tfprojeto_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like @lV101WWProjetoDS_23_Tfprojeto_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like @lV101WWProjetoDS_23_Tfprojeto_sigla)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWProjetoDS_24_Tfprojeto_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] = @AV102WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] = @AV102WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV107WWProjetoDS_29_Tfprojeto_incremental_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 1)";
            }
         }
         if ( AV107WWProjetoDS_29_Tfprojeto_incremental_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 0)";
            }
         }
         if ( AV110WWProjetoDS_32_Tfprojeto_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00DF3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (short)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (decimal)dynConstraints[39] , (bool)dynConstraints[40] , (short)dynConstraints[41] , (short)dynConstraints[42] , (bool)dynConstraints[43] );
               case 1 :
                     return conditional_H00DF5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (short)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (decimal)dynConstraints[39] , (bool)dynConstraints[40] , (short)dynConstraints[41] , (short)dynConstraints[42] , (bool)dynConstraints[43] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DF3 ;
          prmH00DF3 = new Object[] {
          new Object[] {"@lV80WWProjetoDS_2_Projeto_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV81WWProjetoDS_3_Projeto_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV82WWProjetoDS_4_Projeto_tipocontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV83WWProjetoDS_5_Projeto_tecnicacontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV84WWProjetoDS_6_Projeto_status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV87WWProjetoDS_9_Projeto_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV88WWProjetoDS_10_Projeto_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWProjetoDS_11_Projeto_tipocontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV90WWProjetoDS_12_Projeto_tecnicacontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV91WWProjetoDS_13_Projeto_status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV94WWProjetoDS_16_Projeto_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV95WWProjetoDS_17_Projeto_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWProjetoDS_18_Projeto_tipocontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV97WWProjetoDS_19_Projeto_tecnicacontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV98WWProjetoDS_20_Projeto_status3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV99WWProjetoDS_21_Tfprojeto_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWProjetoDS_22_Tfprojeto_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV101WWProjetoDS_23_Tfprojeto_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV102WWProjetoDS_24_Tfprojeto_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWProjetoDS_25_Tfprojeto_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWProjetoDS_26_Tfprojeto_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV105WWProjetoDS_27_Tfprojeto_custo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV106WWProjetoDS_28_Tfprojeto_custo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV108WWProjetoDS_30_Tfprojeto_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV109WWProjetoDS_31_Tfprojeto_esforco_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00DF5 ;
          prmH00DF5 = new Object[] {
          new Object[] {"@lV80WWProjetoDS_2_Projeto_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV81WWProjetoDS_3_Projeto_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV82WWProjetoDS_4_Projeto_tipocontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV83WWProjetoDS_5_Projeto_tecnicacontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV84WWProjetoDS_6_Projeto_status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV87WWProjetoDS_9_Projeto_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV88WWProjetoDS_10_Projeto_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWProjetoDS_11_Projeto_tipocontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV90WWProjetoDS_12_Projeto_tecnicacontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV91WWProjetoDS_13_Projeto_status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV94WWProjetoDS_16_Projeto_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV95WWProjetoDS_17_Projeto_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWProjetoDS_18_Projeto_tipocontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV97WWProjetoDS_19_Projeto_tecnicacontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV98WWProjetoDS_20_Projeto_status3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV99WWProjetoDS_21_Tfprojeto_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWProjetoDS_22_Tfprojeto_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV101WWProjetoDS_23_Tfprojeto_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV102WWProjetoDS_24_Tfprojeto_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWProjetoDS_25_Tfprojeto_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWProjetoDS_26_Tfprojeto_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV105WWProjetoDS_27_Tfprojeto_custo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV106WWProjetoDS_28_Tfprojeto_custo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV108WWProjetoDS_30_Tfprojeto_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV109WWProjetoDS_31_Tfprojeto_esforco_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DF3,11,0,true,false )
             ,new CursorDef("H00DF5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DF5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((short[]) buf[8])[0] = rslt.getShort(8) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(9) ;
                ((short[]) buf[10])[0] = rslt.getShort(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                return;
       }
    }

 }

}
