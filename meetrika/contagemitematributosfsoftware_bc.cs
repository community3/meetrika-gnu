/*
               File: ContagemItemAtributosFSoftware_BC
        Description: Contagem Item Atributos FSoftware
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:16.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitematributosfsoftware_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemitematributosfsoftware_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemitematributosfsoftware_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1861( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1861( ) ;
         standaloneModal( ) ;
         AddRow1861( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
               Z379ContagemItem_AtributosCod = A379ContagemItem_AtributosCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_180( )
      {
         BeforeValidate1861( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1861( ) ;
            }
            else
            {
               CheckExtendedTable1861( ) ;
               if ( AnyError == 0 )
               {
                  ZM1861( 2) ;
                  ZM1861( 3) ;
                  ZM1861( 4) ;
               }
               CloseExtendedTableCursors1861( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1861( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z380ContagemItem_AtributosNom = A380ContagemItem_AtributosNom;
            Z381ContagemItem_AtrTabelaCod = A381ContagemItem_AtrTabelaCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z382ContagemItem_AtrTabelaNom = A382ContagemItem_AtrTabelaNom;
         }
         if ( GX_JID == -1 )
         {
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z379ContagemItem_AtributosCod = A379ContagemItem_AtributosCod;
            Z380ContagemItem_AtributosNom = A380ContagemItem_AtributosNom;
            Z381ContagemItem_AtrTabelaCod = A381ContagemItem_AtrTabelaCod;
            Z382ContagemItem_AtrTabelaNom = A382ContagemItem_AtrTabelaNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1861( )
      {
         /* Using cursor BC00187 */
         pr_default.execute(5, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound61 = 1;
            A380ContagemItem_AtributosNom = BC00187_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = BC00187_n380ContagemItem_AtributosNom[0];
            A382ContagemItem_AtrTabelaNom = BC00187_A382ContagemItem_AtrTabelaNom[0];
            n382ContagemItem_AtrTabelaNom = BC00187_n382ContagemItem_AtrTabelaNom[0];
            A381ContagemItem_AtrTabelaCod = BC00187_A381ContagemItem_AtrTabelaCod[0];
            n381ContagemItem_AtrTabelaCod = BC00187_n381ContagemItem_AtrTabelaCod[0];
            ZM1861( -1) ;
         }
         pr_default.close(5);
         OnLoadActions1861( ) ;
      }

      protected void OnLoadActions1861( )
      {
      }

      protected void CheckExtendedTable1861( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00184 */
         pr_default.execute(2, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC00185 */
         pr_default.execute(3, new Object[] {A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item_Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_ATRIBUTOSCOD");
            AnyError = 1;
         }
         A380ContagemItem_AtributosNom = BC00185_A380ContagemItem_AtributosNom[0];
         n380ContagemItem_AtributosNom = BC00185_n380ContagemItem_AtributosNom[0];
         A381ContagemItem_AtrTabelaCod = BC00185_A381ContagemItem_AtrTabelaCod[0];
         n381ContagemItem_AtrTabelaCod = BC00185_n381ContagemItem_AtrTabelaCod[0];
         pr_default.close(3);
         /* Using cursor BC00186 */
         pr_default.execute(4, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A382ContagemItem_AtrTabelaNom = BC00186_A382ContagemItem_AtrTabelaNom[0];
         n382ContagemItem_AtrTabelaNom = BC00186_n382ContagemItem_AtrTabelaNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1861( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1861( )
      {
         /* Using cursor BC00188 */
         pr_default.execute(6, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound61 = 1;
         }
         else
         {
            RcdFound61 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00183 */
         pr_default.execute(1, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1861( 1) ;
            RcdFound61 = 1;
            A224ContagemItem_Lancamento = BC00183_A224ContagemItem_Lancamento[0];
            A379ContagemItem_AtributosCod = BC00183_A379ContagemItem_AtributosCod[0];
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z379ContagemItem_AtributosCod = A379ContagemItem_AtributosCod;
            sMode61 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1861( ) ;
            if ( AnyError == 1 )
            {
               RcdFound61 = 0;
               InitializeNonKey1861( ) ;
            }
            Gx_mode = sMode61;
         }
         else
         {
            RcdFound61 = 0;
            InitializeNonKey1861( ) ;
            sMode61 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode61;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1861( ) ;
         if ( RcdFound61 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_180( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1861( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00182 */
            pr_default.execute(0, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItemAtributosFSoftware"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemItemAtributosFSoftware"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1861( )
      {
         BeforeValidate1861( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1861( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1861( 0) ;
            CheckOptimisticConcurrency1861( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1861( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1861( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00189 */
                     pr_default.execute(7, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFSoftware") ;
                     if ( (pr_default.getStatus(7) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1861( ) ;
            }
            EndLevel1861( ) ;
         }
         CloseExtendedTableCursors1861( ) ;
      }

      protected void Update1861( )
      {
         BeforeValidate1861( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1861( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1861( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1861( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1861( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContagemItemAtributosFSoftware] */
                     DeferredUpdate1861( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1861( ) ;
         }
         CloseExtendedTableCursors1861( ) ;
      }

      protected void DeferredUpdate1861( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1861( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1861( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1861( ) ;
            AfterConfirm1861( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1861( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001810 */
                  pr_default.execute(8, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFSoftware") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode61 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1861( ) ;
         Gx_mode = sMode61;
      }

      protected void OnDeleteControls1861( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001811 */
            pr_default.execute(9, new Object[] {A379ContagemItem_AtributosCod});
            A380ContagemItem_AtributosNom = BC001811_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = BC001811_n380ContagemItem_AtributosNom[0];
            A381ContagemItem_AtrTabelaCod = BC001811_A381ContagemItem_AtrTabelaCod[0];
            n381ContagemItem_AtrTabelaCod = BC001811_n381ContagemItem_AtrTabelaCod[0];
            pr_default.close(9);
            /* Using cursor BC001812 */
            pr_default.execute(10, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
            A382ContagemItem_AtrTabelaNom = BC001812_A382ContagemItem_AtrTabelaNom[0];
            n382ContagemItem_AtrTabelaNom = BC001812_n382ContagemItem_AtrTabelaNom[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel1861( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1861( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1861( )
      {
         /* Using cursor BC001813 */
         pr_default.execute(11, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         RcdFound61 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound61 = 1;
            A380ContagemItem_AtributosNom = BC001813_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = BC001813_n380ContagemItem_AtributosNom[0];
            A382ContagemItem_AtrTabelaNom = BC001813_A382ContagemItem_AtrTabelaNom[0];
            n382ContagemItem_AtrTabelaNom = BC001813_n382ContagemItem_AtrTabelaNom[0];
            A224ContagemItem_Lancamento = BC001813_A224ContagemItem_Lancamento[0];
            A379ContagemItem_AtributosCod = BC001813_A379ContagemItem_AtributosCod[0];
            A381ContagemItem_AtrTabelaCod = BC001813_A381ContagemItem_AtrTabelaCod[0];
            n381ContagemItem_AtrTabelaCod = BC001813_n381ContagemItem_AtrTabelaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1861( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound61 = 0;
         ScanKeyLoad1861( ) ;
      }

      protected void ScanKeyLoad1861( )
      {
         sMode61 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound61 = 1;
            A380ContagemItem_AtributosNom = BC001813_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = BC001813_n380ContagemItem_AtributosNom[0];
            A382ContagemItem_AtrTabelaNom = BC001813_A382ContagemItem_AtrTabelaNom[0];
            n382ContagemItem_AtrTabelaNom = BC001813_n382ContagemItem_AtrTabelaNom[0];
            A224ContagemItem_Lancamento = BC001813_A224ContagemItem_Lancamento[0];
            A379ContagemItem_AtributosCod = BC001813_A379ContagemItem_AtributosCod[0];
            A381ContagemItem_AtrTabelaCod = BC001813_A381ContagemItem_AtrTabelaCod[0];
            n381ContagemItem_AtrTabelaCod = BC001813_n381ContagemItem_AtrTabelaCod[0];
         }
         Gx_mode = sMode61;
      }

      protected void ScanKeyEnd1861( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm1861( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1861( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1861( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1861( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1861( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1861( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1861( )
      {
      }

      protected void AddRow1861( )
      {
         VarsToRow61( bcContagemItemAtributosFSoftware) ;
      }

      protected void ReadRow1861( )
      {
         RowToVars61( bcContagemItemAtributosFSoftware, 1) ;
      }

      protected void InitializeNonKey1861( )
      {
         A380ContagemItem_AtributosNom = "";
         n380ContagemItem_AtributosNom = false;
         A381ContagemItem_AtrTabelaCod = 0;
         n381ContagemItem_AtrTabelaCod = false;
         A382ContagemItem_AtrTabelaNom = "";
         n382ContagemItem_AtrTabelaNom = false;
      }

      protected void InitAll1861( )
      {
         A224ContagemItem_Lancamento = 0;
         A379ContagemItem_AtributosCod = 0;
         InitializeNonKey1861( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow61( SdtContagemItemAtributosFSoftware obj61 )
      {
         obj61.gxTpr_Mode = Gx_mode;
         obj61.gxTpr_Contagemitem_atributosnom = A380ContagemItem_AtributosNom;
         obj61.gxTpr_Contagemitem_atrtabelacod = A381ContagemItem_AtrTabelaCod;
         obj61.gxTpr_Contagemitem_atrtabelanom = A382ContagemItem_AtrTabelaNom;
         obj61.gxTpr_Contagemitem_lancamento = A224ContagemItem_Lancamento;
         obj61.gxTpr_Contagemitem_atributoscod = A379ContagemItem_AtributosCod;
         obj61.gxTpr_Contagemitem_lancamento_Z = Z224ContagemItem_Lancamento;
         obj61.gxTpr_Contagemitem_atributoscod_Z = Z379ContagemItem_AtributosCod;
         obj61.gxTpr_Contagemitem_atributosnom_Z = Z380ContagemItem_AtributosNom;
         obj61.gxTpr_Contagemitem_atrtabelacod_Z = Z381ContagemItem_AtrTabelaCod;
         obj61.gxTpr_Contagemitem_atrtabelanom_Z = Z382ContagemItem_AtrTabelaNom;
         obj61.gxTpr_Contagemitem_atributosnom_N = (short)(Convert.ToInt16(n380ContagemItem_AtributosNom));
         obj61.gxTpr_Contagemitem_atrtabelacod_N = (short)(Convert.ToInt16(n381ContagemItem_AtrTabelaCod));
         obj61.gxTpr_Contagemitem_atrtabelanom_N = (short)(Convert.ToInt16(n382ContagemItem_AtrTabelaNom));
         obj61.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow61( SdtContagemItemAtributosFSoftware obj61 )
      {
         obj61.gxTpr_Contagemitem_lancamento = A224ContagemItem_Lancamento;
         obj61.gxTpr_Contagemitem_atributoscod = A379ContagemItem_AtributosCod;
         return  ;
      }

      public void RowToVars61( SdtContagemItemAtributosFSoftware obj61 ,
                               int forceLoad )
      {
         Gx_mode = obj61.gxTpr_Mode;
         A380ContagemItem_AtributosNom = obj61.gxTpr_Contagemitem_atributosnom;
         n380ContagemItem_AtributosNom = false;
         A381ContagemItem_AtrTabelaCod = obj61.gxTpr_Contagemitem_atrtabelacod;
         n381ContagemItem_AtrTabelaCod = false;
         A382ContagemItem_AtrTabelaNom = obj61.gxTpr_Contagemitem_atrtabelanom;
         n382ContagemItem_AtrTabelaNom = false;
         A224ContagemItem_Lancamento = obj61.gxTpr_Contagemitem_lancamento;
         A379ContagemItem_AtributosCod = obj61.gxTpr_Contagemitem_atributoscod;
         Z224ContagemItem_Lancamento = obj61.gxTpr_Contagemitem_lancamento_Z;
         Z379ContagemItem_AtributosCod = obj61.gxTpr_Contagemitem_atributoscod_Z;
         Z380ContagemItem_AtributosNom = obj61.gxTpr_Contagemitem_atributosnom_Z;
         Z381ContagemItem_AtrTabelaCod = obj61.gxTpr_Contagemitem_atrtabelacod_Z;
         Z382ContagemItem_AtrTabelaNom = obj61.gxTpr_Contagemitem_atrtabelanom_Z;
         n380ContagemItem_AtributosNom = (bool)(Convert.ToBoolean(obj61.gxTpr_Contagemitem_atributosnom_N));
         n381ContagemItem_AtrTabelaCod = (bool)(Convert.ToBoolean(obj61.gxTpr_Contagemitem_atrtabelacod_N));
         n382ContagemItem_AtrTabelaNom = (bool)(Convert.ToBoolean(obj61.gxTpr_Contagemitem_atrtabelanom_N));
         Gx_mode = obj61.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A224ContagemItem_Lancamento = (int)getParm(obj,0);
         A379ContagemItem_AtributosCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1861( ) ;
         ScanKeyStart1861( ) ;
         if ( RcdFound61 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001814 */
            pr_default.execute(12, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC001811 */
            pr_default.execute(9, new Object[] {A379ContagemItem_AtributosCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Item_Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_ATRIBUTOSCOD");
               AnyError = 1;
            }
            A380ContagemItem_AtributosNom = BC001811_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = BC001811_n380ContagemItem_AtributosNom[0];
            A381ContagemItem_AtrTabelaCod = BC001811_A381ContagemItem_AtrTabelaCod[0];
            n381ContagemItem_AtrTabelaCod = BC001811_n381ContagemItem_AtrTabelaCod[0];
            pr_default.close(9);
            /* Using cursor BC001812 */
            pr_default.execute(10, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A382ContagemItem_AtrTabelaNom = BC001812_A382ContagemItem_AtrTabelaNom[0];
            n382ContagemItem_AtrTabelaNom = BC001812_n382ContagemItem_AtrTabelaNom[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z379ContagemItem_AtributosCod = A379ContagemItem_AtributosCod;
         }
         ZM1861( -1) ;
         OnLoadActions1861( ) ;
         AddRow1861( ) ;
         ScanKeyEnd1861( ) ;
         if ( RcdFound61 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars61( bcContagemItemAtributosFSoftware, 0) ;
         ScanKeyStart1861( ) ;
         if ( RcdFound61 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001814 */
            pr_default.execute(12, new Object[] {A224ContagemItem_Lancamento});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC001811 */
            pr_default.execute(9, new Object[] {A379ContagemItem_AtributosCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Item_Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_ATRIBUTOSCOD");
               AnyError = 1;
            }
            A380ContagemItem_AtributosNom = BC001811_A380ContagemItem_AtributosNom[0];
            n380ContagemItem_AtributosNom = BC001811_n380ContagemItem_AtributosNom[0];
            A381ContagemItem_AtrTabelaCod = BC001811_A381ContagemItem_AtrTabelaCod[0];
            n381ContagemItem_AtrTabelaCod = BC001811_n381ContagemItem_AtrTabelaCod[0];
            pr_default.close(9);
            /* Using cursor BC001812 */
            pr_default.execute(10, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A382ContagemItem_AtrTabelaNom = BC001812_A382ContagemItem_AtrTabelaNom[0];
            n382ContagemItem_AtrTabelaNom = BC001812_n382ContagemItem_AtrTabelaNom[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z379ContagemItem_AtributosCod = A379ContagemItem_AtributosCod;
         }
         ZM1861( -1) ;
         OnLoadActions1861( ) ;
         AddRow1861( ) ;
         ScanKeyEnd1861( ) ;
         if ( RcdFound61 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars61( bcContagemItemAtributosFSoftware, 0) ;
         nKeyPressed = 1;
         GetKey1861( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1861( ) ;
         }
         else
         {
            if ( RcdFound61 == 1 )
            {
               if ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) || ( A379ContagemItem_AtributosCod != Z379ContagemItem_AtributosCod ) )
               {
                  A224ContagemItem_Lancamento = Z224ContagemItem_Lancamento;
                  A379ContagemItem_AtributosCod = Z379ContagemItem_AtributosCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1861( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) || ( A379ContagemItem_AtributosCod != Z379ContagemItem_AtributosCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1861( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1861( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow61( bcContagemItemAtributosFSoftware) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars61( bcContagemItemAtributosFSoftware, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1861( ) ;
         if ( RcdFound61 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) || ( A379ContagemItem_AtributosCod != Z379ContagemItem_AtributosCod ) )
            {
               A224ContagemItem_Lancamento = Z224ContagemItem_Lancamento;
               A379ContagemItem_AtributosCod = Z379ContagemItem_AtributosCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) || ( A379ContagemItem_AtributosCod != Z379ContagemItem_AtributosCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "ContagemItemAtributosFSoftware_BC");
         VarsToRow61( bcContagemItemAtributosFSoftware) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemItemAtributosFSoftware.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemItemAtributosFSoftware.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemItemAtributosFSoftware )
         {
            bcContagemItemAtributosFSoftware = (SdtContagemItemAtributosFSoftware)(sdt);
            if ( StringUtil.StrCmp(bcContagemItemAtributosFSoftware.gxTpr_Mode, "") == 0 )
            {
               bcContagemItemAtributosFSoftware.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow61( bcContagemItemAtributosFSoftware) ;
            }
            else
            {
               RowToVars61( bcContagemItemAtributosFSoftware, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemItemAtributosFSoftware.gxTpr_Mode, "") == 0 )
            {
               bcContagemItemAtributosFSoftware.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars61( bcContagemItemAtributosFSoftware, 1) ;
         return  ;
      }

      public SdtContagemItemAtributosFSoftware ContagemItemAtributosFSoftware_BC
      {
         get {
            return bcContagemItemAtributosFSoftware ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z380ContagemItem_AtributosNom = "";
         A380ContagemItem_AtributosNom = "";
         Z382ContagemItem_AtrTabelaNom = "";
         A382ContagemItem_AtrTabelaNom = "";
         BC00187_A380ContagemItem_AtributosNom = new String[] {""} ;
         BC00187_n380ContagemItem_AtributosNom = new bool[] {false} ;
         BC00187_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         BC00187_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         BC00187_A224ContagemItem_Lancamento = new int[1] ;
         BC00187_A379ContagemItem_AtributosCod = new int[1] ;
         BC00187_A381ContagemItem_AtrTabelaCod = new int[1] ;
         BC00187_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         BC00184_A224ContagemItem_Lancamento = new int[1] ;
         BC00185_A380ContagemItem_AtributosNom = new String[] {""} ;
         BC00185_n380ContagemItem_AtributosNom = new bool[] {false} ;
         BC00185_A381ContagemItem_AtrTabelaCod = new int[1] ;
         BC00185_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         BC00186_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         BC00186_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         BC00188_A224ContagemItem_Lancamento = new int[1] ;
         BC00188_A379ContagemItem_AtributosCod = new int[1] ;
         BC00183_A224ContagemItem_Lancamento = new int[1] ;
         BC00183_A379ContagemItem_AtributosCod = new int[1] ;
         sMode61 = "";
         BC00182_A224ContagemItem_Lancamento = new int[1] ;
         BC00182_A379ContagemItem_AtributosCod = new int[1] ;
         BC001811_A380ContagemItem_AtributosNom = new String[] {""} ;
         BC001811_n380ContagemItem_AtributosNom = new bool[] {false} ;
         BC001811_A381ContagemItem_AtrTabelaCod = new int[1] ;
         BC001811_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         BC001812_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         BC001812_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         BC001813_A380ContagemItem_AtributosNom = new String[] {""} ;
         BC001813_n380ContagemItem_AtributosNom = new bool[] {false} ;
         BC001813_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         BC001813_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         BC001813_A224ContagemItem_Lancamento = new int[1] ;
         BC001813_A379ContagemItem_AtributosCod = new int[1] ;
         BC001813_A381ContagemItem_AtrTabelaCod = new int[1] ;
         BC001813_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC001814_A224ContagemItem_Lancamento = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitematributosfsoftware_bc__default(),
            new Object[][] {
                new Object[] {
               BC00182_A224ContagemItem_Lancamento, BC00182_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               BC00183_A224ContagemItem_Lancamento, BC00183_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               BC00184_A224ContagemItem_Lancamento
               }
               , new Object[] {
               BC00185_A380ContagemItem_AtributosNom, BC00185_n380ContagemItem_AtributosNom, BC00185_A381ContagemItem_AtrTabelaCod, BC00185_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               BC00186_A382ContagemItem_AtrTabelaNom, BC00186_n382ContagemItem_AtrTabelaNom
               }
               , new Object[] {
               BC00187_A380ContagemItem_AtributosNom, BC00187_n380ContagemItem_AtributosNom, BC00187_A382ContagemItem_AtrTabelaNom, BC00187_n382ContagemItem_AtrTabelaNom, BC00187_A224ContagemItem_Lancamento, BC00187_A379ContagemItem_AtributosCod, BC00187_A381ContagemItem_AtrTabelaCod, BC00187_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               BC00188_A224ContagemItem_Lancamento, BC00188_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001811_A380ContagemItem_AtributosNom, BC001811_n380ContagemItem_AtributosNom, BC001811_A381ContagemItem_AtrTabelaCod, BC001811_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               BC001812_A382ContagemItem_AtrTabelaNom, BC001812_n382ContagemItem_AtrTabelaNom
               }
               , new Object[] {
               BC001813_A380ContagemItem_AtributosNom, BC001813_n380ContagemItem_AtributosNom, BC001813_A382ContagemItem_AtrTabelaNom, BC001813_n382ContagemItem_AtrTabelaNom, BC001813_A224ContagemItem_Lancamento, BC001813_A379ContagemItem_AtributosCod, BC001813_A381ContagemItem_AtrTabelaCod, BC001813_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               BC001814_A224ContagemItem_Lancamento
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound61 ;
      private int trnEnded ;
      private int Z224ContagemItem_Lancamento ;
      private int A224ContagemItem_Lancamento ;
      private int Z379ContagemItem_AtributosCod ;
      private int A379ContagemItem_AtributosCod ;
      private int Z381ContagemItem_AtrTabelaCod ;
      private int A381ContagemItem_AtrTabelaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z380ContagemItem_AtributosNom ;
      private String A380ContagemItem_AtributosNom ;
      private String Z382ContagemItem_AtrTabelaNom ;
      private String A382ContagemItem_AtrTabelaNom ;
      private String sMode61 ;
      private bool n380ContagemItem_AtributosNom ;
      private bool n382ContagemItem_AtrTabelaNom ;
      private bool n381ContagemItem_AtrTabelaCod ;
      private SdtContagemItemAtributosFSoftware bcContagemItemAtributosFSoftware ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00187_A380ContagemItem_AtributosNom ;
      private bool[] BC00187_n380ContagemItem_AtributosNom ;
      private String[] BC00187_A382ContagemItem_AtrTabelaNom ;
      private bool[] BC00187_n382ContagemItem_AtrTabelaNom ;
      private int[] BC00187_A224ContagemItem_Lancamento ;
      private int[] BC00187_A379ContagemItem_AtributosCod ;
      private int[] BC00187_A381ContagemItem_AtrTabelaCod ;
      private bool[] BC00187_n381ContagemItem_AtrTabelaCod ;
      private int[] BC00184_A224ContagemItem_Lancamento ;
      private String[] BC00185_A380ContagemItem_AtributosNom ;
      private bool[] BC00185_n380ContagemItem_AtributosNom ;
      private int[] BC00185_A381ContagemItem_AtrTabelaCod ;
      private bool[] BC00185_n381ContagemItem_AtrTabelaCod ;
      private String[] BC00186_A382ContagemItem_AtrTabelaNom ;
      private bool[] BC00186_n382ContagemItem_AtrTabelaNom ;
      private int[] BC00188_A224ContagemItem_Lancamento ;
      private int[] BC00188_A379ContagemItem_AtributosCod ;
      private int[] BC00183_A224ContagemItem_Lancamento ;
      private int[] BC00183_A379ContagemItem_AtributosCod ;
      private int[] BC00182_A224ContagemItem_Lancamento ;
      private int[] BC00182_A379ContagemItem_AtributosCod ;
      private String[] BC001811_A380ContagemItem_AtributosNom ;
      private bool[] BC001811_n380ContagemItem_AtributosNom ;
      private int[] BC001811_A381ContagemItem_AtrTabelaCod ;
      private bool[] BC001811_n381ContagemItem_AtrTabelaCod ;
      private String[] BC001812_A382ContagemItem_AtrTabelaNom ;
      private bool[] BC001812_n382ContagemItem_AtrTabelaNom ;
      private String[] BC001813_A380ContagemItem_AtributosNom ;
      private bool[] BC001813_n380ContagemItem_AtributosNom ;
      private String[] BC001813_A382ContagemItem_AtrTabelaNom ;
      private bool[] BC001813_n382ContagemItem_AtrTabelaNom ;
      private int[] BC001813_A224ContagemItem_Lancamento ;
      private int[] BC001813_A379ContagemItem_AtributosCod ;
      private int[] BC001813_A381ContagemItem_AtrTabelaCod ;
      private bool[] BC001813_n381ContagemItem_AtrTabelaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC001814_A224ContagemItem_Lancamento ;
   }

   public class contagemitematributosfsoftware_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00187 ;
          prmBC00187 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00184 ;
          prmBC00184 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00185 ;
          prmBC00185 = new Object[] {
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00186 ;
          prmBC00186 = new Object[] {
          new Object[] {"@ContagemItem_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00188 ;
          prmBC00188 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00183 ;
          prmBC00183 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00182 ;
          prmBC00182 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00189 ;
          prmBC00189 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001810 ;
          prmBC001810 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001813 ;
          prmBC001813 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001814 ;
          prmBC001814 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001811 ;
          prmBC001811 = new Object[] {
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001812 ;
          prmBC001812 = new Object[] {
          new Object[] {"@ContagemItem_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00182", "SELECT [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (UPDLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00182,1,0,true,false )
             ,new CursorDef("BC00183", "SELECT [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00183,1,0,true,false )
             ,new CursorDef("BC00184", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00184,1,0,true,false )
             ,new CursorDef("BC00185", "SELECT [Atributos_Nome] AS ContagemItem_AtributosNom, [Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00185,1,0,true,false )
             ,new CursorDef("BC00186", "SELECT [Tabela_Nome] AS ContagemItem_AtrTabelaNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00186,1,0,true,false )
             ,new CursorDef("BC00187", "SELECT T2.[Atributos_Nome] AS ContagemItem_AtributosNom, T3.[Tabela_Nome] AS ContagemItem_AtrTabelaNom, TM1.[ContagemItem_Lancamento], TM1.[ContagemItem_AtributosCod] AS ContagemItem_AtributosCod, T2.[Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM (([ContagemItemAtributosFSoftware] TM1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = TM1.[ContagemItem_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE TM1.[ContagemItem_Lancamento] = @ContagemItem_Lancamento and TM1.[ContagemItem_AtributosCod] = @ContagemItem_AtributosCod ORDER BY TM1.[ContagemItem_Lancamento], TM1.[ContagemItem_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00187,100,0,true,false )
             ,new CursorDef("BC00188", "SELECT [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00188,1,0,true,false )
             ,new CursorDef("BC00189", "INSERT INTO [ContagemItemAtributosFSoftware]([ContagemItem_Lancamento], [ContagemItem_AtributosCod]) VALUES(@ContagemItem_Lancamento, @ContagemItem_AtributosCod)", GxErrorMask.GX_NOMASK,prmBC00189)
             ,new CursorDef("BC001810", "DELETE FROM [ContagemItemAtributosFSoftware]  WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod", GxErrorMask.GX_NOMASK,prmBC001810)
             ,new CursorDef("BC001811", "SELECT [Atributos_Nome] AS ContagemItem_AtributosNom, [Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001811,1,0,true,false )
             ,new CursorDef("BC001812", "SELECT [Tabela_Nome] AS ContagemItem_AtrTabelaNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001812,1,0,true,false )
             ,new CursorDef("BC001813", "SELECT T2.[Atributos_Nome] AS ContagemItem_AtributosNom, T3.[Tabela_Nome] AS ContagemItem_AtrTabelaNom, TM1.[ContagemItem_Lancamento], TM1.[ContagemItem_AtributosCod] AS ContagemItem_AtributosCod, T2.[Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM (([ContagemItemAtributosFSoftware] TM1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = TM1.[ContagemItem_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE TM1.[ContagemItem_Lancamento] = @ContagemItem_Lancamento and TM1.[ContagemItem_AtributosCod] = @ContagemItem_AtributosCod ORDER BY TM1.[ContagemItem_Lancamento], TM1.[ContagemItem_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001813,100,0,true,false )
             ,new CursorDef("BC001814", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001814,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
