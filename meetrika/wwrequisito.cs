/*
               File: WWRequisito
        Description:  Requisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:24:25.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwrequisito : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwrequisito( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwrequisito( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbRequisito_Status = new GXCombobox();
         chkRequisito_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Requisito_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Requisito_Descricao1", AV17Requisito_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21Requisito_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Requisito_Descricao2", AV21Requisito_Descricao2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25Requisito_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Requisito_Descricao3", AV25Requisito_Descricao3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV39TFRequisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0)));
               AV40TFRequisito_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFRequisito_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0)));
               AV43TFRequisito_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFRequisito_Descricao", AV43TFRequisito_Descricao);
               AV44TFRequisito_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFRequisito_Descricao_Sel", AV44TFRequisito_Descricao_Sel);
               AV47TFProposta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0)));
               AV48TFProposta_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProposta_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0)));
               AV51TFProposta_Objetivo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProposta_Objetivo", AV51TFProposta_Objetivo);
               AV52TFProposta_Objetivo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProposta_Objetivo_Sel", AV52TFProposta_Objetivo_Sel);
               AV108TFRequisito_TipoReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFRequisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0)));
               AV109TFRequisito_TipoReqCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFRequisito_TipoReqCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0)));
               AV63TFRequisito_Agrupador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFRequisito_Agrupador", AV63TFRequisito_Agrupador);
               AV64TFRequisito_Agrupador_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFRequisito_Agrupador_Sel", AV64TFRequisito_Agrupador_Sel);
               AV67TFRequisito_Titulo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFRequisito_Titulo", AV67TFRequisito_Titulo);
               AV68TFRequisito_Titulo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFRequisito_Titulo_Sel", AV68TFRequisito_Titulo_Sel);
               AV75TFRequisito_Restricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFRequisito_Restricao", AV75TFRequisito_Restricao);
               AV76TFRequisito_Restricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFRequisito_Restricao_Sel", AV76TFRequisito_Restricao_Sel);
               AV83TFRequisito_Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFRequisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0)));
               AV84TFRequisito_Ordem_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFRequisito_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0)));
               AV87TFRequisito_Pontuacao = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFRequisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5)));
               AV88TFRequisito_Pontuacao_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFRequisito_Pontuacao_To", StringUtil.LTrim( StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5)));
               AV91TFRequisito_DataHomologacao = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFRequisito_DataHomologacao", context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"));
               AV92TFRequisito_DataHomologacao_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFRequisito_DataHomologacao_To", context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"));
               AV101TFRequisito_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFRequisito_Ativo_Sel", StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0));
               AV32ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
               AV41ddo_Requisito_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Requisito_CodigoTitleControlIdToReplace", AV41ddo_Requisito_CodigoTitleControlIdToReplace);
               AV45ddo_Requisito_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Requisito_DescricaoTitleControlIdToReplace", AV45ddo_Requisito_DescricaoTitleControlIdToReplace);
               AV49ddo_Proposta_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Proposta_CodigoTitleControlIdToReplace", AV49ddo_Proposta_CodigoTitleControlIdToReplace);
               AV53ddo_Proposta_ObjetivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Proposta_ObjetivoTitleControlIdToReplace", AV53ddo_Proposta_ObjetivoTitleControlIdToReplace);
               AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace", AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace);
               AV65ddo_Requisito_AgrupadorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Requisito_AgrupadorTitleControlIdToReplace", AV65ddo_Requisito_AgrupadorTitleControlIdToReplace);
               AV69ddo_Requisito_TituloTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Requisito_TituloTitleControlIdToReplace", AV69ddo_Requisito_TituloTitleControlIdToReplace);
               AV77ddo_Requisito_RestricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Requisito_RestricaoTitleControlIdToReplace", AV77ddo_Requisito_RestricaoTitleControlIdToReplace);
               AV85ddo_Requisito_OrdemTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Requisito_OrdemTitleControlIdToReplace", AV85ddo_Requisito_OrdemTitleControlIdToReplace);
               AV89ddo_Requisito_PontuacaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Requisito_PontuacaoTitleControlIdToReplace", AV89ddo_Requisito_PontuacaoTitleControlIdToReplace);
               AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace", AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace);
               AV99ddo_Requisito_StatusTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_Requisito_StatusTitleControlIdToReplace", AV99ddo_Requisito_StatusTitleControlIdToReplace);
               AV102ddo_Requisito_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Requisito_AtivoTitleControlIdToReplace", AV102ddo_Requisito_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV98TFRequisito_Status_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV151Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1919Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAPZ2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTPZ2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221242658");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwrequisito.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vREQUISITO_DESCRICAO1", AV17Requisito_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vREQUISITO_DESCRICAO2", AV21Requisito_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vREQUISITO_DESCRICAO3", AV25Requisito_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFRequisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFRequisito_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_DESCRICAO", AV43TFRequisito_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_DESCRICAO_SEL", AV44TFRequisito_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROPOSTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFProposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROPOSTA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFProposta_Codigo_To), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROPOSTA_OBJETIVO", AV51TFProposta_Objetivo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROPOSTA_OBJETIVO_SEL", AV52TFProposta_Objetivo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_TIPOREQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_TIPOREQCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_AGRUPADOR", AV63TFRequisito_Agrupador);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_AGRUPADOR_SEL", AV64TFRequisito_Agrupador_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_TITULO", AV67TFRequisito_Titulo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_TITULO_SEL", AV68TFRequisito_Titulo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_RESTRICAO", AV75TFRequisito_Restricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_RESTRICAO_SEL", AV76TFRequisito_Restricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFRequisito_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_ORDEM_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84TFRequisito_Ordem_To), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_PONTUACAO", StringUtil.LTrim( StringUtil.NToC( AV87TFRequisito_Pontuacao, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_PONTUACAO_TO", StringUtil.LTrim( StringUtil.NToC( AV88TFRequisito_Pontuacao_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_DATAHOMOLOGACAO", context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_DATAHOMOLOGACAO_TO", context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREQUISITO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV36ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV36ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV106GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV103DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV103DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_CODIGOTITLEFILTERDATA", AV38Requisito_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_CODIGOTITLEFILTERDATA", AV38Requisito_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_DESCRICAOTITLEFILTERDATA", AV42Requisito_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_DESCRICAOTITLEFILTERDATA", AV42Requisito_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROPOSTA_CODIGOTITLEFILTERDATA", AV46Proposta_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROPOSTA_CODIGOTITLEFILTERDATA", AV46Proposta_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROPOSTA_OBJETIVOTITLEFILTERDATA", AV50Proposta_ObjetivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROPOSTA_OBJETIVOTITLEFILTERDATA", AV50Proposta_ObjetivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_TIPOREQCODTITLEFILTERDATA", AV107Requisito_TipoReqCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_TIPOREQCODTITLEFILTERDATA", AV107Requisito_TipoReqCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_AGRUPADORTITLEFILTERDATA", AV62Requisito_AgrupadorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_AGRUPADORTITLEFILTERDATA", AV62Requisito_AgrupadorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_TITULOTITLEFILTERDATA", AV66Requisito_TituloTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_TITULOTITLEFILTERDATA", AV66Requisito_TituloTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_RESTRICAOTITLEFILTERDATA", AV74Requisito_RestricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_RESTRICAOTITLEFILTERDATA", AV74Requisito_RestricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_ORDEMTITLEFILTERDATA", AV82Requisito_OrdemTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_ORDEMTITLEFILTERDATA", AV82Requisito_OrdemTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_PONTUACAOTITLEFILTERDATA", AV86Requisito_PontuacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_PONTUACAOTITLEFILTERDATA", AV86Requisito_PontuacaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_DATAHOMOLOGACAOTITLEFILTERDATA", AV90Requisito_DataHomologacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_DATAHOMOLOGACAOTITLEFILTERDATA", AV90Requisito_DataHomologacaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_STATUSTITLEFILTERDATA", AV96Requisito_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_STATUSTITLEFILTERDATA", AV96Requisito_StatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO_ATIVOTITLEFILTERDATA", AV100Requisito_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO_ATIVOTITLEFILTERDATA", AV100Requisito_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFREQUISITO_STATUS_SELS", AV98TFRequisito_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFREQUISITO_STATUS_SELS", AV98TFRequisito_Status_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV151Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Caption", StringUtil.RTrim( Ddo_requisito_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_requisito_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Cls", StringUtil.RTrim( Ddo_requisito_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_requisito_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_requisito_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_requisito_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_requisito_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_requisito_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_requisito_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_requisito_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_requisito_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_requisito_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_requisito_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_requisito_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_requisito_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_requisito_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_requisito_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_requisito_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_requisito_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_requisito_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_requisito_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_requisito_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_requisito_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_requisito_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_requisito_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_requisito_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_requisito_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Caption", StringUtil.RTrim( Ddo_proposta_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_proposta_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Cls", StringUtil.RTrim( Ddo_proposta_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_proposta_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_proposta_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_proposta_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_proposta_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_proposta_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_proposta_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_proposta_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_proposta_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_proposta_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_proposta_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_proposta_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_proposta_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_proposta_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_proposta_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_proposta_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_proposta_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_proposta_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Caption", StringUtil.RTrim( Ddo_proposta_objetivo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Tooltip", StringUtil.RTrim( Ddo_proposta_objetivo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Cls", StringUtil.RTrim( Ddo_proposta_objetivo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Filteredtext_set", StringUtil.RTrim( Ddo_proposta_objetivo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_proposta_objetivo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_proposta_objetivo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_proposta_objetivo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Includesortasc", StringUtil.BoolToStr( Ddo_proposta_objetivo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_proposta_objetivo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Sortedstatus", StringUtil.RTrim( Ddo_proposta_objetivo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Includefilter", StringUtil.BoolToStr( Ddo_proposta_objetivo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Filtertype", StringUtil.RTrim( Ddo_proposta_objetivo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Filterisrange", StringUtil.BoolToStr( Ddo_proposta_objetivo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Includedatalist", StringUtil.BoolToStr( Ddo_proposta_objetivo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Datalisttype", StringUtil.RTrim( Ddo_proposta_objetivo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Datalistproc", StringUtil.RTrim( Ddo_proposta_objetivo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_proposta_objetivo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Sortasc", StringUtil.RTrim( Ddo_proposta_objetivo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Sortdsc", StringUtil.RTrim( Ddo_proposta_objetivo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Loadingdata", StringUtil.RTrim( Ddo_proposta_objetivo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Cleanfilter", StringUtil.RTrim( Ddo_proposta_objetivo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Noresultsfound", StringUtil.RTrim( Ddo_proposta_objetivo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Searchbuttontext", StringUtil.RTrim( Ddo_proposta_objetivo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Caption", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Tooltip", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Cls", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_tiporeqcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_tiporeqcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Sortedstatus", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Includefilter", StringUtil.BoolToStr( Ddo_requisito_tiporeqcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Filtertype", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_tiporeqcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_tiporeqcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Sortasc", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Sortdsc", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Cleanfilter", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Rangefilterto", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Caption", StringUtil.RTrim( Ddo_requisito_agrupador_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Tooltip", StringUtil.RTrim( Ddo_requisito_agrupador_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Cls", StringUtil.RTrim( Ddo_requisito_agrupador_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_agrupador_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Selectedvalue_set", StringUtil.RTrim( Ddo_requisito_agrupador_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_agrupador_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_agrupador_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_agrupador_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_agrupador_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Sortedstatus", StringUtil.RTrim( Ddo_requisito_agrupador_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Includefilter", StringUtil.BoolToStr( Ddo_requisito_agrupador_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Filtertype", StringUtil.RTrim( Ddo_requisito_agrupador_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_agrupador_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_agrupador_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Datalisttype", StringUtil.RTrim( Ddo_requisito_agrupador_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Datalistproc", StringUtil.RTrim( Ddo_requisito_agrupador_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_requisito_agrupador_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Sortasc", StringUtil.RTrim( Ddo_requisito_agrupador_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Sortdsc", StringUtil.RTrim( Ddo_requisito_agrupador_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Loadingdata", StringUtil.RTrim( Ddo_requisito_agrupador_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Cleanfilter", StringUtil.RTrim( Ddo_requisito_agrupador_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Noresultsfound", StringUtil.RTrim( Ddo_requisito_agrupador_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_agrupador_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Caption", StringUtil.RTrim( Ddo_requisito_titulo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Tooltip", StringUtil.RTrim( Ddo_requisito_titulo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Cls", StringUtil.RTrim( Ddo_requisito_titulo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_titulo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Selectedvalue_set", StringUtil.RTrim( Ddo_requisito_titulo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_titulo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_titulo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_titulo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_titulo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Sortedstatus", StringUtil.RTrim( Ddo_requisito_titulo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Includefilter", StringUtil.BoolToStr( Ddo_requisito_titulo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Filtertype", StringUtil.RTrim( Ddo_requisito_titulo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_titulo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_titulo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Datalisttype", StringUtil.RTrim( Ddo_requisito_titulo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Datalistproc", StringUtil.RTrim( Ddo_requisito_titulo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_requisito_titulo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Sortasc", StringUtil.RTrim( Ddo_requisito_titulo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Sortdsc", StringUtil.RTrim( Ddo_requisito_titulo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Loadingdata", StringUtil.RTrim( Ddo_requisito_titulo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Cleanfilter", StringUtil.RTrim( Ddo_requisito_titulo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Noresultsfound", StringUtil.RTrim( Ddo_requisito_titulo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_titulo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Caption", StringUtil.RTrim( Ddo_requisito_restricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Tooltip", StringUtil.RTrim( Ddo_requisito_restricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Cls", StringUtil.RTrim( Ddo_requisito_restricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_restricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_requisito_restricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_restricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_restricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_restricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_restricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Sortedstatus", StringUtil.RTrim( Ddo_requisito_restricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Includefilter", StringUtil.BoolToStr( Ddo_requisito_restricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Filtertype", StringUtil.RTrim( Ddo_requisito_restricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_restricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_restricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Datalisttype", StringUtil.RTrim( Ddo_requisito_restricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Datalistproc", StringUtil.RTrim( Ddo_requisito_restricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_requisito_restricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Sortasc", StringUtil.RTrim( Ddo_requisito_restricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Sortdsc", StringUtil.RTrim( Ddo_requisito_restricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Loadingdata", StringUtil.RTrim( Ddo_requisito_restricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Cleanfilter", StringUtil.RTrim( Ddo_requisito_restricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Noresultsfound", StringUtil.RTrim( Ddo_requisito_restricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_restricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Caption", StringUtil.RTrim( Ddo_requisito_ordem_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Tooltip", StringUtil.RTrim( Ddo_requisito_ordem_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Cls", StringUtil.RTrim( Ddo_requisito_ordem_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_ordem_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Filteredtextto_set", StringUtil.RTrim( Ddo_requisito_ordem_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_ordem_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_ordem_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_ordem_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_ordem_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Sortedstatus", StringUtil.RTrim( Ddo_requisito_ordem_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Includefilter", StringUtil.BoolToStr( Ddo_requisito_ordem_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Filtertype", StringUtil.RTrim( Ddo_requisito_ordem_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_ordem_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_ordem_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Sortasc", StringUtil.RTrim( Ddo_requisito_ordem_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Sortdsc", StringUtil.RTrim( Ddo_requisito_ordem_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Cleanfilter", StringUtil.RTrim( Ddo_requisito_ordem_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Rangefilterfrom", StringUtil.RTrim( Ddo_requisito_ordem_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Rangefilterto", StringUtil.RTrim( Ddo_requisito_ordem_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_ordem_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Caption", StringUtil.RTrim( Ddo_requisito_pontuacao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Tooltip", StringUtil.RTrim( Ddo_requisito_pontuacao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Cls", StringUtil.RTrim( Ddo_requisito_pontuacao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_pontuacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Filteredtextto_set", StringUtil.RTrim( Ddo_requisito_pontuacao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_pontuacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_pontuacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_pontuacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_pontuacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Sortedstatus", StringUtil.RTrim( Ddo_requisito_pontuacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Includefilter", StringUtil.BoolToStr( Ddo_requisito_pontuacao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Filtertype", StringUtil.RTrim( Ddo_requisito_pontuacao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_pontuacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_pontuacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Sortasc", StringUtil.RTrim( Ddo_requisito_pontuacao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Sortdsc", StringUtil.RTrim( Ddo_requisito_pontuacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Cleanfilter", StringUtil.RTrim( Ddo_requisito_pontuacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Rangefilterfrom", StringUtil.RTrim( Ddo_requisito_pontuacao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Rangefilterto", StringUtil.RTrim( Ddo_requisito_pontuacao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_pontuacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Caption", StringUtil.RTrim( Ddo_requisito_datahomologacao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Tooltip", StringUtil.RTrim( Ddo_requisito_datahomologacao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Cls", StringUtil.RTrim( Ddo_requisito_datahomologacao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtext_set", StringUtil.RTrim( Ddo_requisito_datahomologacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtextto_set", StringUtil.RTrim( Ddo_requisito_datahomologacao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_datahomologacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_datahomologacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_datahomologacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_datahomologacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Sortedstatus", StringUtil.RTrim( Ddo_requisito_datahomologacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Includefilter", StringUtil.BoolToStr( Ddo_requisito_datahomologacao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Filtertype", StringUtil.RTrim( Ddo_requisito_datahomologacao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Filterisrange", StringUtil.BoolToStr( Ddo_requisito_datahomologacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_datahomologacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Sortasc", StringUtil.RTrim( Ddo_requisito_datahomologacao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Sortdsc", StringUtil.RTrim( Ddo_requisito_datahomologacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Cleanfilter", StringUtil.RTrim( Ddo_requisito_datahomologacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Rangefilterfrom", StringUtil.RTrim( Ddo_requisito_datahomologacao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Rangefilterto", StringUtil.RTrim( Ddo_requisito_datahomologacao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_datahomologacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Caption", StringUtil.RTrim( Ddo_requisito_status_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Tooltip", StringUtil.RTrim( Ddo_requisito_status_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Cls", StringUtil.RTrim( Ddo_requisito_status_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_requisito_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Sortedstatus", StringUtil.RTrim( Ddo_requisito_status_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_requisito_status_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Datalisttype", StringUtil.RTrim( Ddo_requisito_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_requisito_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_requisito_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Sortasc", StringUtil.RTrim( Ddo_requisito_status_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Sortdsc", StringUtil.RTrim( Ddo_requisito_status_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_requisito_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Caption", StringUtil.RTrim( Ddo_requisito_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_requisito_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Cls", StringUtil.RTrim( Ddo_requisito_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_requisito_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_requisito_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_requisito_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_requisito_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_requisito_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_requisito_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_requisito_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_requisito_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_requisito_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_requisito_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_requisito_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_requisito_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_requisito_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_requisito_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_requisito_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_requisito_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_requisito_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_requisito_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_proposta_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_proposta_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_proposta_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Activeeventkey", StringUtil.RTrim( Ddo_proposta_objetivo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Filteredtext_get", StringUtil.RTrim( Ddo_proposta_objetivo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROPOSTA_OBJETIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_proposta_objetivo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Activeeventkey", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TIPOREQCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_requisito_tiporeqcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Activeeventkey", StringUtil.RTrim( Ddo_requisito_agrupador_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_agrupador_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_AGRUPADOR_Selectedvalue_get", StringUtil.RTrim( Ddo_requisito_agrupador_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Activeeventkey", StringUtil.RTrim( Ddo_requisito_titulo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_titulo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_TITULO_Selectedvalue_get", StringUtil.RTrim( Ddo_requisito_titulo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Activeeventkey", StringUtil.RTrim( Ddo_requisito_restricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_restricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_RESTRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_requisito_restricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Activeeventkey", StringUtil.RTrim( Ddo_requisito_ordem_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_ordem_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ORDEM_Filteredtextto_get", StringUtil.RTrim( Ddo_requisito_ordem_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Activeeventkey", StringUtil.RTrim( Ddo_requisito_pontuacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_pontuacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_PONTUACAO_Filteredtextto_get", StringUtil.RTrim( Ddo_requisito_pontuacao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Activeeventkey", StringUtil.RTrim( Ddo_requisito_datahomologacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtext_get", StringUtil.RTrim( Ddo_requisito_datahomologacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtextto_get", StringUtil.RTrim( Ddo_requisito_datahomologacao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_requisito_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_requisito_status_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_requisito_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REQUISITO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_requisito_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEPZ2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTPZ2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwrequisito.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWRequisito" ;
      }

      public override String GetPgmdesc( )
      {
         return " Requisito" ;
      }

      protected void WBPZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_PZ2( true) ;
         }
         else
         {
            wb_table1_2_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(110, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFRequisito_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFRequisito_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFRequisito_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFRequisito_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfrequisito_descricao_Internalname, AV43TFRequisito_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavTfrequisito_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfrequisito_descricao_sel_Internalname, AV44TFRequisito_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavTfrequisito_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfproposta_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFProposta_Codigo), 9, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFProposta_Codigo), "ZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfproposta_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfproposta_codigo_Visible, 1, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfproposta_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFProposta_Codigo_To), 9, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFProposta_Codigo_To), "ZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfproposta_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfproposta_codigo_to_Visible, 1, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfproposta_objetivo_Internalname, AV51TFProposta_Objetivo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavTfproposta_objetivo_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfproposta_objetivo_sel_Internalname, AV52TFProposta_Objetivo_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavTfproposta_objetivo_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_tiporeqcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV108TFRequisito_TipoReqCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_tiporeqcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_tiporeqcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_tiporeqcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV109TFRequisito_TipoReqCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_tiporeqcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_tiporeqcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_agrupador_Internalname, AV63TFRequisito_Agrupador, StringUtil.RTrim( context.localUtil.Format( AV63TFRequisito_Agrupador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_agrupador_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_agrupador_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_agrupador_sel_Internalname, AV64TFRequisito_Agrupador_Sel, StringUtil.RTrim( context.localUtil.Format( AV64TFRequisito_Agrupador_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_agrupador_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_agrupador_sel_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfrequisito_titulo_Internalname, AV67TFRequisito_Titulo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavTfrequisito_titulo_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfrequisito_titulo_sel_Internalname, AV68TFRequisito_Titulo_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavTfrequisito_titulo_sel_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfrequisito_restricao_Internalname, AV75TFRequisito_Restricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavTfrequisito_restricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfrequisito_restricao_sel_Internalname, AV76TFRequisito_Restricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavTfrequisito_restricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFRequisito_Ordem), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV83TFRequisito_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_ordem_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_ordem_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_ordem_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84TFRequisito_Ordem_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV84TFRequisito_Ordem_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_ordem_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_ordem_to_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_pontuacao_Internalname, StringUtil.LTrim( StringUtil.NToC( AV87TFRequisito_Pontuacao, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV87TFRequisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_pontuacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_pontuacao_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_pontuacao_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV88TFRequisito_Pontuacao_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV88TFRequisito_Pontuacao_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_pontuacao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_pontuacao_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfrequisito_datahomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_datahomologacao_Internalname, context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"), context.localUtil.Format( AV91TFRequisito_DataHomologacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_datahomologacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_datahomologacao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            GxWebStd.gx_bitmap( context, edtavTfrequisito_datahomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfrequisito_datahomologacao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfrequisito_datahomologacao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_datahomologacao_to_Internalname, context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"), context.localUtil.Format( AV92TFRequisito_DataHomologacao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_datahomologacao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_datahomologacao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            GxWebStd.gx_bitmap( context, edtavTfrequisito_datahomologacao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfrequisito_datahomologacao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_requisito_datahomologacaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_requisito_datahomologacaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_requisito_datahomologacaoauxdate_Internalname, context.localUtil.Format(AV93DDO_Requisito_DataHomologacaoAuxDate, "99/99/99"), context.localUtil.Format( AV93DDO_Requisito_DataHomologacaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_requisito_datahomologacaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_requisito_datahomologacaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_requisito_datahomologacaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_requisito_datahomologacaoauxdateto_Internalname, context.localUtil.Format(AV94DDO_Requisito_DataHomologacaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV94DDO_Requisito_DataHomologacaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_requisito_datahomologacaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_requisito_datahomologacaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfrequisito_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV101TFRequisito_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfrequisito_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfrequisito_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_codigotitlecontrolidtoreplace_Internalname, AV41ddo_Requisito_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", 0, edtavDdo_requisito_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_descricaotitlecontrolidtoreplace_Internalname, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,141);\"", 0, edtavDdo_requisito_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROPOSTA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_proposta_codigotitlecontrolidtoreplace_Internalname, AV49ddo_Proposta_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", 0, edtavDdo_proposta_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROPOSTA_OBJETIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_proposta_objetivotitlecontrolidtoreplace_Internalname, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", 0, edtavDdo_proposta_objetivotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_TIPOREQCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Internalname, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", 0, edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_AGRUPADORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Internalname, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", 0, edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_TITULOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_titulotitlecontrolidtoreplace_Internalname, AV69ddo_Requisito_TituloTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", 0, edtavDdo_requisito_titulotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_RESTRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_restricaotitlecontrolidtoreplace_Internalname, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", 0, edtavDdo_requisito_restricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_ORDEMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_ordemtitlecontrolidtoreplace_Internalname, AV85ddo_Requisito_OrdemTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_requisito_ordemtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_PONTUACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Internalname, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_DATAHOMOLOGACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Internalname, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", 0, edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_statustitlecontrolidtoreplace_Internalname, AV99ddo_Requisito_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,161);\"", 0, edtavDdo_requisito_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REQUISITO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_requisito_ativotitlecontrolidtoreplace_Internalname, AV102ddo_Requisito_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,163);\"", 0, edtavDdo_requisito_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRequisito.htm");
         }
         wbLoad = true;
      }

      protected void STARTPZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Requisito", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPPZ0( ) ;
      }

      protected void WSPZ2( )
      {
         STARTPZ2( ) ;
         EVTPZ2( ) ;
      }

      protected void EVTPZ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11PZ2 */
                              E11PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12PZ2 */
                              E12PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13PZ2 */
                              E13PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14PZ2 */
                              E14PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROPOSTA_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15PZ2 */
                              E15PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROPOSTA_OBJETIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16PZ2 */
                              E16PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_TIPOREQCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17PZ2 */
                              E17PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_AGRUPADOR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18PZ2 */
                              E18PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_TITULO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19PZ2 */
                              E19PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_RESTRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20PZ2 */
                              E20PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_ORDEM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21PZ2 */
                              E21PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_PONTUACAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22PZ2 */
                              E22PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_DATAHOMOLOGACAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23PZ2 */
                              E23PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_STATUS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24PZ2 */
                              E24PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REQUISITO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25PZ2 */
                              E25PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26PZ2 */
                              E26PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27PZ2 */
                              E27PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28PZ2 */
                              E28PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29PZ2 */
                              E29PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E30PZ2 */
                              E30PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E31PZ2 */
                              E31PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E32PZ2 */
                              E32PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E33PZ2 */
                              E33PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E34PZ2 */
                              E34PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E35PZ2 */
                              E35PZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV148Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV149Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV30Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Display)) ? AV150Display_GXI : context.convertURL( context.PathToRelativeUrl( AV30Display))));
                              A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtRequisito_Codigo_Internalname), ",", "."));
                              A1923Requisito_Descricao = cgiGet( edtRequisito_Descricao_Internalname);
                              n1923Requisito_Descricao = false;
                              A1685Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProposta_Codigo_Internalname), ",", "."));
                              n1685Proposta_Codigo = false;
                              A1690Proposta_Objetivo = cgiGet( edtProposta_Objetivo_Internalname);
                              A2049Requisito_TipoReqCod = (int)(context.localUtil.CToN( cgiGet( edtRequisito_TipoReqCod_Internalname), ",", "."));
                              n2049Requisito_TipoReqCod = false;
                              A1926Requisito_Agrupador = cgiGet( edtRequisito_Agrupador_Internalname);
                              n1926Requisito_Agrupador = false;
                              A1927Requisito_Titulo = cgiGet( edtRequisito_Titulo_Internalname);
                              n1927Requisito_Titulo = false;
                              A1929Requisito_Restricao = cgiGet( edtRequisito_Restricao_Internalname);
                              n1929Requisito_Restricao = false;
                              A1931Requisito_Ordem = (short)(context.localUtil.CToN( cgiGet( edtRequisito_Ordem_Internalname), ",", "."));
                              n1931Requisito_Ordem = false;
                              A1932Requisito_Pontuacao = context.localUtil.CToN( cgiGet( edtRequisito_Pontuacao_Internalname), ",", ".");
                              n1932Requisito_Pontuacao = false;
                              A1933Requisito_DataHomologacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtRequisito_DataHomologacao_Internalname), 0));
                              n1933Requisito_DataHomologacao = false;
                              cmbRequisito_Status.Name = cmbRequisito_Status_Internalname;
                              cmbRequisito_Status.CurrentValue = cgiGet( cmbRequisito_Status_Internalname);
                              A1934Requisito_Status = (short)(NumberUtil.Val( cgiGet( cmbRequisito_Status_Internalname), "."));
                              A1935Requisito_Ativo = StringUtil.StrToBool( cgiGet( chkRequisito_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E36PZ2 */
                                    E36PZ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E37PZ2 */
                                    E37PZ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E38PZ2 */
                                    E38PZ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Requisito_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREQUISITO_DESCRICAO1"), AV17Requisito_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Requisito_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREQUISITO_DESCRICAO2"), AV21Requisito_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Requisito_descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREQUISITO_DESCRICAO3"), AV25Requisito_Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_CODIGO"), ",", ".") != Convert.ToDecimal( AV39TFRequisito_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV40TFRequisito_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_DESCRICAO"), AV43TFRequisito_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_DESCRICAO_SEL"), AV44TFRequisito_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfproposta_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROPOSTA_CODIGO"), ",", ".") != Convert.ToDecimal( AV47TFProposta_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfproposta_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROPOSTA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV48TFProposta_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfproposta_objetivo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROPOSTA_OBJETIVO"), AV51TFProposta_Objetivo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfproposta_objetivo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROPOSTA_OBJETIVO_SEL"), AV52TFProposta_Objetivo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_tiporeqcod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_TIPOREQCOD"), ",", ".") != Convert.ToDecimal( AV108TFRequisito_TipoReqCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_tiporeqcod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_TIPOREQCOD_TO"), ",", ".") != Convert.ToDecimal( AV109TFRequisito_TipoReqCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_agrupador Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_AGRUPADOR"), AV63TFRequisito_Agrupador) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_agrupador_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_AGRUPADOR_SEL"), AV64TFRequisito_Agrupador_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_titulo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_TITULO"), AV67TFRequisito_Titulo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_titulo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_TITULO_SEL"), AV68TFRequisito_Titulo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_restricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_RESTRICAO"), AV75TFRequisito_Restricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_restricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_RESTRICAO_SEL"), AV76TFRequisito_Restricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_ordem Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_ORDEM"), ",", ".") != Convert.ToDecimal( AV83TFRequisito_Ordem )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_ordem_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV84TFRequisito_Ordem_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_pontuacao Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_PONTUACAO"), ",", ".") != AV87TFRequisito_Pontuacao )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_pontuacao_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_PONTUACAO_TO"), ",", ".") != AV88TFRequisito_Pontuacao_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_datahomologacao Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFREQUISITO_DATAHOMOLOGACAO"), 0) != AV91TFRequisito_DataHomologacao )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_datahomologacao_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFREQUISITO_DATAHOMOLOGACAO_TO"), 0) != AV92TFRequisito_DataHomologacao_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfrequisito_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV101TFRequisito_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEPZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAPZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("REQUISITO_DESCRICAO", "do Requisito", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("REQUISITO_DESCRICAO", "do Requisito", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("REQUISITO_DESCRICAO", "do Requisito", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "REQUISITO_STATUS_" + sGXsfl_88_idx;
            cmbRequisito_Status.Name = GXCCtl;
            cmbRequisito_Status.WebTags = "";
            cmbRequisito_Status.addItem("0", "Rascunho", 0);
            cmbRequisito_Status.addItem("1", "Solicitado", 0);
            cmbRequisito_Status.addItem("2", "Aprovado", 0);
            cmbRequisito_Status.addItem("3", "N�o Aprovado", 0);
            cmbRequisito_Status.addItem("4", "Pausado", 0);
            cmbRequisito_Status.addItem("5", "Cancelado", 0);
            if ( cmbRequisito_Status.ItemCount > 0 )
            {
               A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
            }
            GXCCtl = "REQUISITO_ATIVO_" + sGXsfl_88_idx;
            chkRequisito_Ativo.Name = GXCCtl;
            chkRequisito_Ativo.WebTags = "";
            chkRequisito_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkRequisito_Ativo_Internalname, "TitleCaption", chkRequisito_Ativo.Caption);
            chkRequisito_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Requisito_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21Requisito_Descricao2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25Requisito_Descricao3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV39TFRequisito_Codigo ,
                                       int AV40TFRequisito_Codigo_To ,
                                       String AV43TFRequisito_Descricao ,
                                       String AV44TFRequisito_Descricao_Sel ,
                                       int AV47TFProposta_Codigo ,
                                       int AV48TFProposta_Codigo_To ,
                                       String AV51TFProposta_Objetivo ,
                                       String AV52TFProposta_Objetivo_Sel ,
                                       int AV108TFRequisito_TipoReqCod ,
                                       int AV109TFRequisito_TipoReqCod_To ,
                                       String AV63TFRequisito_Agrupador ,
                                       String AV64TFRequisito_Agrupador_Sel ,
                                       String AV67TFRequisito_Titulo ,
                                       String AV68TFRequisito_Titulo_Sel ,
                                       String AV75TFRequisito_Restricao ,
                                       String AV76TFRequisito_Restricao_Sel ,
                                       short AV83TFRequisito_Ordem ,
                                       short AV84TFRequisito_Ordem_To ,
                                       decimal AV87TFRequisito_Pontuacao ,
                                       decimal AV88TFRequisito_Pontuacao_To ,
                                       DateTime AV91TFRequisito_DataHomologacao ,
                                       DateTime AV92TFRequisito_DataHomologacao_To ,
                                       short AV101TFRequisito_Ativo_Sel ,
                                       short AV32ManageFiltersExecutionStep ,
                                       String AV41ddo_Requisito_CodigoTitleControlIdToReplace ,
                                       String AV45ddo_Requisito_DescricaoTitleControlIdToReplace ,
                                       String AV49ddo_Proposta_CodigoTitleControlIdToReplace ,
                                       String AV53ddo_Proposta_ObjetivoTitleControlIdToReplace ,
                                       String AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace ,
                                       String AV65ddo_Requisito_AgrupadorTitleControlIdToReplace ,
                                       String AV69ddo_Requisito_TituloTitleControlIdToReplace ,
                                       String AV77ddo_Requisito_RestricaoTitleControlIdToReplace ,
                                       String AV85ddo_Requisito_OrdemTitleControlIdToReplace ,
                                       String AV89ddo_Requisito_PontuacaoTitleControlIdToReplace ,
                                       String AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace ,
                                       String AV99ddo_Requisito_StatusTitleControlIdToReplace ,
                                       String AV102ddo_Requisito_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV98TFRequisito_Status_Sels ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV151Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1919Requisito_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFPZ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_DESCRICAO", GetSecureSignedToken( "", A1923Requisito_Descricao));
         GxWebStd.gx_hidden_field( context, "REQUISITO_DESCRICAO", A1923Requisito_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1685Proposta_Codigo), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_TIPOREQCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_TIPOREQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2049Requisito_TipoReqCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_AGRUPADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1926Requisito_Agrupador, ""))));
         GxWebStd.gx_hidden_field( context, "REQUISITO_AGRUPADOR", A1926Requisito_Agrupador);
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_TITULO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1927Requisito_Titulo, ""))));
         GxWebStd.gx_hidden_field( context, "REQUISITO_TITULO", A1927Requisito_Titulo);
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_RESTRICAO", GetSecureSignedToken( "", A1929Requisito_Restricao));
         GxWebStd.gx_hidden_field( context, "REQUISITO_RESTRICAO", A1929Requisito_Restricao);
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_ORDEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_PONTUACAO", GetSecureSignedToken( "", context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_PONTUACAO", StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_DATAHOMOLOGACAO", GetSecureSignedToken( "", A1933Requisito_DataHomologacao));
         GxWebStd.gx_hidden_field( context, "REQUISITO_DATAHOMOLOGACAO", context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_STATUS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1934Requisito_Status), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_STATUS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1934Requisito_Status), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_ATIVO", GetSecureSignedToken( "", A1935Requisito_Ativo));
         GxWebStd.gx_hidden_field( context, "REQUISITO_ATIVO", StringUtil.BoolToStr( A1935Requisito_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFPZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV151Pgmname = "WWRequisito";
         context.Gx_err = 0;
      }

      protected void RFPZ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E37PZ2 */
         E37PZ2 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1934Requisito_Status ,
                                                 AV146WWRequisitoDS_34_Tfrequisito_status_sels ,
                                                 AV113WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                                 AV114WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                                 AV115WWRequisitoDS_3_Requisito_descricao1 ,
                                                 AV116WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                                 AV117WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                                 AV118WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                                 AV119WWRequisitoDS_7_Requisito_descricao2 ,
                                                 AV120WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                                 AV121WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                                 AV122WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                                 AV123WWRequisitoDS_11_Requisito_descricao3 ,
                                                 AV124WWRequisitoDS_12_Tfrequisito_codigo ,
                                                 AV125WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                                 AV127WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                                 AV126WWRequisitoDS_14_Tfrequisito_descricao ,
                                                 AV128WWRequisitoDS_16_Tfproposta_codigo ,
                                                 AV129WWRequisitoDS_17_Tfproposta_codigo_to ,
                                                 AV131WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                                 AV130WWRequisitoDS_18_Tfproposta_objetivo ,
                                                 AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                                 AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                                 AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                                 AV134WWRequisitoDS_22_Tfrequisito_agrupador ,
                                                 AV137WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                                 AV136WWRequisitoDS_24_Tfrequisito_titulo ,
                                                 AV139WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                                 AV138WWRequisitoDS_26_Tfrequisito_restricao ,
                                                 AV140WWRequisitoDS_28_Tfrequisito_ordem ,
                                                 AV141WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                                 AV142WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                                 AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                                 AV144WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                                 AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                                 AV146WWRequisitoDS_34_Tfrequisito_status_sels.Count ,
                                                 AV147WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                                 A1923Requisito_Descricao ,
                                                 A1919Requisito_Codigo ,
                                                 A1685Proposta_Codigo ,
                                                 A1690Proposta_Objetivo ,
                                                 A2049Requisito_TipoReqCod ,
                                                 A1926Requisito_Agrupador ,
                                                 A1927Requisito_Titulo ,
                                                 A1929Requisito_Restricao ,
                                                 A1931Requisito_Ordem ,
                                                 A1932Requisito_Pontuacao ,
                                                 A1933Requisito_DataHomologacao ,
                                                 A1935Requisito_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV115WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1), "%", "");
            lV115WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1), "%", "");
            lV119WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2), "%", "");
            lV119WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2), "%", "");
            lV123WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3), "%", "");
            lV123WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3), "%", "");
            lV126WWRequisitoDS_14_Tfrequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV126WWRequisitoDS_14_Tfrequisito_descricao), "%", "");
            lV130WWRequisitoDS_18_Tfproposta_objetivo = StringUtil.Concat( StringUtil.RTrim( AV130WWRequisitoDS_18_Tfproposta_objetivo), "%", "");
            lV134WWRequisitoDS_22_Tfrequisito_agrupador = StringUtil.Concat( StringUtil.RTrim( AV134WWRequisitoDS_22_Tfrequisito_agrupador), "%", "");
            lV136WWRequisitoDS_24_Tfrequisito_titulo = StringUtil.Concat( StringUtil.RTrim( AV136WWRequisitoDS_24_Tfrequisito_titulo), "%", "");
            lV138WWRequisitoDS_26_Tfrequisito_restricao = StringUtil.Concat( StringUtil.RTrim( AV138WWRequisitoDS_26_Tfrequisito_restricao), "%", "");
            /* Using cursor H00PZ2 */
            pr_default.execute(0, new Object[] {lV115WWRequisitoDS_3_Requisito_descricao1, lV115WWRequisitoDS_3_Requisito_descricao1, lV119WWRequisitoDS_7_Requisito_descricao2, lV119WWRequisitoDS_7_Requisito_descricao2, lV123WWRequisitoDS_11_Requisito_descricao3, lV123WWRequisitoDS_11_Requisito_descricao3, AV124WWRequisitoDS_12_Tfrequisito_codigo, AV125WWRequisitoDS_13_Tfrequisito_codigo_to, lV126WWRequisitoDS_14_Tfrequisito_descricao, AV127WWRequisitoDS_15_Tfrequisito_descricao_sel, AV128WWRequisitoDS_16_Tfproposta_codigo, AV129WWRequisitoDS_17_Tfproposta_codigo_to, lV130WWRequisitoDS_18_Tfproposta_objetivo, AV131WWRequisitoDS_19_Tfproposta_objetivo_sel, AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod, AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to, lV134WWRequisitoDS_22_Tfrequisito_agrupador, AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel, lV136WWRequisitoDS_24_Tfrequisito_titulo, AV137WWRequisitoDS_25_Tfrequisito_titulo_sel, lV138WWRequisitoDS_26_Tfrequisito_restricao, AV139WWRequisitoDS_27_Tfrequisito_restricao_sel, AV140WWRequisitoDS_28_Tfrequisito_ordem, AV141WWRequisitoDS_29_Tfrequisito_ordem_to, AV142WWRequisitoDS_30_Tfrequisito_pontuacao, AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to, AV144WWRequisitoDS_32_Tfrequisito_datahomologacao, AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1935Requisito_Ativo = H00PZ2_A1935Requisito_Ativo[0];
               A1934Requisito_Status = H00PZ2_A1934Requisito_Status[0];
               A1933Requisito_DataHomologacao = H00PZ2_A1933Requisito_DataHomologacao[0];
               n1933Requisito_DataHomologacao = H00PZ2_n1933Requisito_DataHomologacao[0];
               A1932Requisito_Pontuacao = H00PZ2_A1932Requisito_Pontuacao[0];
               n1932Requisito_Pontuacao = H00PZ2_n1932Requisito_Pontuacao[0];
               A1931Requisito_Ordem = H00PZ2_A1931Requisito_Ordem[0];
               n1931Requisito_Ordem = H00PZ2_n1931Requisito_Ordem[0];
               A1929Requisito_Restricao = H00PZ2_A1929Requisito_Restricao[0];
               n1929Requisito_Restricao = H00PZ2_n1929Requisito_Restricao[0];
               A1927Requisito_Titulo = H00PZ2_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00PZ2_n1927Requisito_Titulo[0];
               A1926Requisito_Agrupador = H00PZ2_A1926Requisito_Agrupador[0];
               n1926Requisito_Agrupador = H00PZ2_n1926Requisito_Agrupador[0];
               A2049Requisito_TipoReqCod = H00PZ2_A2049Requisito_TipoReqCod[0];
               n2049Requisito_TipoReqCod = H00PZ2_n2049Requisito_TipoReqCod[0];
               A1690Proposta_Objetivo = H00PZ2_A1690Proposta_Objetivo[0];
               A1685Proposta_Codigo = H00PZ2_A1685Proposta_Codigo[0];
               n1685Proposta_Codigo = H00PZ2_n1685Proposta_Codigo[0];
               A1923Requisito_Descricao = H00PZ2_A1923Requisito_Descricao[0];
               n1923Requisito_Descricao = H00PZ2_n1923Requisito_Descricao[0];
               A1919Requisito_Codigo = H00PZ2_A1919Requisito_Codigo[0];
               A1690Proposta_Objetivo = H00PZ2_A1690Proposta_Objetivo[0];
               /* Execute user event: E38PZ2 */
               E38PZ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBPZ0( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV114WWRequisitoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV115WWRequisitoDS_3_Requisito_descricao1 = AV17Requisito_Descricao1;
         AV116WWRequisitoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV118WWRequisitoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV119WWRequisitoDS_7_Requisito_descricao2 = AV21Requisito_Descricao2;
         AV120WWRequisitoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV122WWRequisitoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV123WWRequisitoDS_11_Requisito_descricao3 = AV25Requisito_Descricao3;
         AV124WWRequisitoDS_12_Tfrequisito_codigo = AV39TFRequisito_Codigo;
         AV125WWRequisitoDS_13_Tfrequisito_codigo_to = AV40TFRequisito_Codigo_To;
         AV126WWRequisitoDS_14_Tfrequisito_descricao = AV43TFRequisito_Descricao;
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = AV44TFRequisito_Descricao_Sel;
         AV128WWRequisitoDS_16_Tfproposta_codigo = AV47TFProposta_Codigo;
         AV129WWRequisitoDS_17_Tfproposta_codigo_to = AV48TFProposta_Codigo_To;
         AV130WWRequisitoDS_18_Tfproposta_objetivo = AV51TFProposta_Objetivo;
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = AV52TFProposta_Objetivo_Sel;
         AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV108TFRequisito_TipoReqCod;
         AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV109TFRequisito_TipoReqCod_To;
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = AV63TFRequisito_Agrupador;
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV64TFRequisito_Agrupador_Sel;
         AV136WWRequisitoDS_24_Tfrequisito_titulo = AV67TFRequisito_Titulo;
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = AV68TFRequisito_Titulo_Sel;
         AV138WWRequisitoDS_26_Tfrequisito_restricao = AV75TFRequisito_Restricao;
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = AV76TFRequisito_Restricao_Sel;
         AV140WWRequisitoDS_28_Tfrequisito_ordem = AV83TFRequisito_Ordem;
         AV141WWRequisitoDS_29_Tfrequisito_ordem_to = AV84TFRequisito_Ordem_To;
         AV142WWRequisitoDS_30_Tfrequisito_pontuacao = AV87TFRequisito_Pontuacao;
         AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV88TFRequisito_Pontuacao_To;
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = AV91TFRequisito_DataHomologacao;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV92TFRequisito_DataHomologacao_To;
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = AV98TFRequisito_Status_Sels;
         AV147WWRequisitoDS_35_Tfrequisito_ativo_sel = AV101TFRequisito_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1934Requisito_Status ,
                                              AV146WWRequisitoDS_34_Tfrequisito_status_sels ,
                                              AV113WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                              AV114WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                              AV115WWRequisitoDS_3_Requisito_descricao1 ,
                                              AV116WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                              AV117WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                              AV118WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                              AV119WWRequisitoDS_7_Requisito_descricao2 ,
                                              AV120WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                              AV121WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                              AV122WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                              AV123WWRequisitoDS_11_Requisito_descricao3 ,
                                              AV124WWRequisitoDS_12_Tfrequisito_codigo ,
                                              AV125WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                              AV127WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                              AV126WWRequisitoDS_14_Tfrequisito_descricao ,
                                              AV128WWRequisitoDS_16_Tfproposta_codigo ,
                                              AV129WWRequisitoDS_17_Tfproposta_codigo_to ,
                                              AV131WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                              AV130WWRequisitoDS_18_Tfproposta_objetivo ,
                                              AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                              AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                              AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                              AV134WWRequisitoDS_22_Tfrequisito_agrupador ,
                                              AV137WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                              AV136WWRequisitoDS_24_Tfrequisito_titulo ,
                                              AV139WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                              AV138WWRequisitoDS_26_Tfrequisito_restricao ,
                                              AV140WWRequisitoDS_28_Tfrequisito_ordem ,
                                              AV141WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                              AV142WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                              AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                              AV144WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                              AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                              AV146WWRequisitoDS_34_Tfrequisito_status_sels.Count ,
                                              AV147WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                              A1923Requisito_Descricao ,
                                              A1919Requisito_Codigo ,
                                              A1685Proposta_Codigo ,
                                              A1690Proposta_Objetivo ,
                                              A2049Requisito_TipoReqCod ,
                                              A1926Requisito_Agrupador ,
                                              A1927Requisito_Titulo ,
                                              A1929Requisito_Restricao ,
                                              A1931Requisito_Ordem ,
                                              A1932Requisito_Pontuacao ,
                                              A1933Requisito_DataHomologacao ,
                                              A1935Requisito_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV115WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV115WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV119WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV119WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV123WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV123WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV126WWRequisitoDS_14_Tfrequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV126WWRequisitoDS_14_Tfrequisito_descricao), "%", "");
         lV130WWRequisitoDS_18_Tfproposta_objetivo = StringUtil.Concat( StringUtil.RTrim( AV130WWRequisitoDS_18_Tfproposta_objetivo), "%", "");
         lV134WWRequisitoDS_22_Tfrequisito_agrupador = StringUtil.Concat( StringUtil.RTrim( AV134WWRequisitoDS_22_Tfrequisito_agrupador), "%", "");
         lV136WWRequisitoDS_24_Tfrequisito_titulo = StringUtil.Concat( StringUtil.RTrim( AV136WWRequisitoDS_24_Tfrequisito_titulo), "%", "");
         lV138WWRequisitoDS_26_Tfrequisito_restricao = StringUtil.Concat( StringUtil.RTrim( AV138WWRequisitoDS_26_Tfrequisito_restricao), "%", "");
         /* Using cursor H00PZ3 */
         pr_default.execute(1, new Object[] {lV115WWRequisitoDS_3_Requisito_descricao1, lV115WWRequisitoDS_3_Requisito_descricao1, lV119WWRequisitoDS_7_Requisito_descricao2, lV119WWRequisitoDS_7_Requisito_descricao2, lV123WWRequisitoDS_11_Requisito_descricao3, lV123WWRequisitoDS_11_Requisito_descricao3, AV124WWRequisitoDS_12_Tfrequisito_codigo, AV125WWRequisitoDS_13_Tfrequisito_codigo_to, lV126WWRequisitoDS_14_Tfrequisito_descricao, AV127WWRequisitoDS_15_Tfrequisito_descricao_sel, AV128WWRequisitoDS_16_Tfproposta_codigo, AV129WWRequisitoDS_17_Tfproposta_codigo_to, lV130WWRequisitoDS_18_Tfproposta_objetivo, AV131WWRequisitoDS_19_Tfproposta_objetivo_sel, AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod, AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to, lV134WWRequisitoDS_22_Tfrequisito_agrupador, AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel, lV136WWRequisitoDS_24_Tfrequisito_titulo, AV137WWRequisitoDS_25_Tfrequisito_titulo_sel, lV138WWRequisitoDS_26_Tfrequisito_restricao, AV139WWRequisitoDS_27_Tfrequisito_restricao_sel, AV140WWRequisitoDS_28_Tfrequisito_ordem, AV141WWRequisitoDS_29_Tfrequisito_ordem_to, AV142WWRequisitoDS_30_Tfrequisito_pontuacao, AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to, AV144WWRequisitoDS_32_Tfrequisito_datahomologacao, AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to});
         GRID_nRecordCount = H00PZ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV114WWRequisitoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV115WWRequisitoDS_3_Requisito_descricao1 = AV17Requisito_Descricao1;
         AV116WWRequisitoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV118WWRequisitoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV119WWRequisitoDS_7_Requisito_descricao2 = AV21Requisito_Descricao2;
         AV120WWRequisitoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV122WWRequisitoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV123WWRequisitoDS_11_Requisito_descricao3 = AV25Requisito_Descricao3;
         AV124WWRequisitoDS_12_Tfrequisito_codigo = AV39TFRequisito_Codigo;
         AV125WWRequisitoDS_13_Tfrequisito_codigo_to = AV40TFRequisito_Codigo_To;
         AV126WWRequisitoDS_14_Tfrequisito_descricao = AV43TFRequisito_Descricao;
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = AV44TFRequisito_Descricao_Sel;
         AV128WWRequisitoDS_16_Tfproposta_codigo = AV47TFProposta_Codigo;
         AV129WWRequisitoDS_17_Tfproposta_codigo_to = AV48TFProposta_Codigo_To;
         AV130WWRequisitoDS_18_Tfproposta_objetivo = AV51TFProposta_Objetivo;
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = AV52TFProposta_Objetivo_Sel;
         AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV108TFRequisito_TipoReqCod;
         AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV109TFRequisito_TipoReqCod_To;
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = AV63TFRequisito_Agrupador;
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV64TFRequisito_Agrupador_Sel;
         AV136WWRequisitoDS_24_Tfrequisito_titulo = AV67TFRequisito_Titulo;
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = AV68TFRequisito_Titulo_Sel;
         AV138WWRequisitoDS_26_Tfrequisito_restricao = AV75TFRequisito_Restricao;
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = AV76TFRequisito_Restricao_Sel;
         AV140WWRequisitoDS_28_Tfrequisito_ordem = AV83TFRequisito_Ordem;
         AV141WWRequisitoDS_29_Tfrequisito_ordem_to = AV84TFRequisito_Ordem_To;
         AV142WWRequisitoDS_30_Tfrequisito_pontuacao = AV87TFRequisito_Pontuacao;
         AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV88TFRequisito_Pontuacao_To;
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = AV91TFRequisito_DataHomologacao;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV92TFRequisito_DataHomologacao_To;
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = AV98TFRequisito_Status_Sels;
         AV147WWRequisitoDS_35_Tfrequisito_ativo_sel = AV101TFRequisito_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV114WWRequisitoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV115WWRequisitoDS_3_Requisito_descricao1 = AV17Requisito_Descricao1;
         AV116WWRequisitoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV118WWRequisitoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV119WWRequisitoDS_7_Requisito_descricao2 = AV21Requisito_Descricao2;
         AV120WWRequisitoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV122WWRequisitoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV123WWRequisitoDS_11_Requisito_descricao3 = AV25Requisito_Descricao3;
         AV124WWRequisitoDS_12_Tfrequisito_codigo = AV39TFRequisito_Codigo;
         AV125WWRequisitoDS_13_Tfrequisito_codigo_to = AV40TFRequisito_Codigo_To;
         AV126WWRequisitoDS_14_Tfrequisito_descricao = AV43TFRequisito_Descricao;
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = AV44TFRequisito_Descricao_Sel;
         AV128WWRequisitoDS_16_Tfproposta_codigo = AV47TFProposta_Codigo;
         AV129WWRequisitoDS_17_Tfproposta_codigo_to = AV48TFProposta_Codigo_To;
         AV130WWRequisitoDS_18_Tfproposta_objetivo = AV51TFProposta_Objetivo;
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = AV52TFProposta_Objetivo_Sel;
         AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV108TFRequisito_TipoReqCod;
         AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV109TFRequisito_TipoReqCod_To;
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = AV63TFRequisito_Agrupador;
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV64TFRequisito_Agrupador_Sel;
         AV136WWRequisitoDS_24_Tfrequisito_titulo = AV67TFRequisito_Titulo;
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = AV68TFRequisito_Titulo_Sel;
         AV138WWRequisitoDS_26_Tfrequisito_restricao = AV75TFRequisito_Restricao;
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = AV76TFRequisito_Restricao_Sel;
         AV140WWRequisitoDS_28_Tfrequisito_ordem = AV83TFRequisito_Ordem;
         AV141WWRequisitoDS_29_Tfrequisito_ordem_to = AV84TFRequisito_Ordem_To;
         AV142WWRequisitoDS_30_Tfrequisito_pontuacao = AV87TFRequisito_Pontuacao;
         AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV88TFRequisito_Pontuacao_To;
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = AV91TFRequisito_DataHomologacao;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV92TFRequisito_DataHomologacao_To;
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = AV98TFRequisito_Status_Sels;
         AV147WWRequisitoDS_35_Tfrequisito_ativo_sel = AV101TFRequisito_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV114WWRequisitoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV115WWRequisitoDS_3_Requisito_descricao1 = AV17Requisito_Descricao1;
         AV116WWRequisitoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV118WWRequisitoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV119WWRequisitoDS_7_Requisito_descricao2 = AV21Requisito_Descricao2;
         AV120WWRequisitoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV122WWRequisitoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV123WWRequisitoDS_11_Requisito_descricao3 = AV25Requisito_Descricao3;
         AV124WWRequisitoDS_12_Tfrequisito_codigo = AV39TFRequisito_Codigo;
         AV125WWRequisitoDS_13_Tfrequisito_codigo_to = AV40TFRequisito_Codigo_To;
         AV126WWRequisitoDS_14_Tfrequisito_descricao = AV43TFRequisito_Descricao;
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = AV44TFRequisito_Descricao_Sel;
         AV128WWRequisitoDS_16_Tfproposta_codigo = AV47TFProposta_Codigo;
         AV129WWRequisitoDS_17_Tfproposta_codigo_to = AV48TFProposta_Codigo_To;
         AV130WWRequisitoDS_18_Tfproposta_objetivo = AV51TFProposta_Objetivo;
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = AV52TFProposta_Objetivo_Sel;
         AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV108TFRequisito_TipoReqCod;
         AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV109TFRequisito_TipoReqCod_To;
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = AV63TFRequisito_Agrupador;
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV64TFRequisito_Agrupador_Sel;
         AV136WWRequisitoDS_24_Tfrequisito_titulo = AV67TFRequisito_Titulo;
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = AV68TFRequisito_Titulo_Sel;
         AV138WWRequisitoDS_26_Tfrequisito_restricao = AV75TFRequisito_Restricao;
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = AV76TFRequisito_Restricao_Sel;
         AV140WWRequisitoDS_28_Tfrequisito_ordem = AV83TFRequisito_Ordem;
         AV141WWRequisitoDS_29_Tfrequisito_ordem_to = AV84TFRequisito_Ordem_To;
         AV142WWRequisitoDS_30_Tfrequisito_pontuacao = AV87TFRequisito_Pontuacao;
         AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV88TFRequisito_Pontuacao_To;
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = AV91TFRequisito_DataHomologacao;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV92TFRequisito_DataHomologacao_To;
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = AV98TFRequisito_Status_Sels;
         AV147WWRequisitoDS_35_Tfrequisito_ativo_sel = AV101TFRequisito_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV114WWRequisitoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV115WWRequisitoDS_3_Requisito_descricao1 = AV17Requisito_Descricao1;
         AV116WWRequisitoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV118WWRequisitoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV119WWRequisitoDS_7_Requisito_descricao2 = AV21Requisito_Descricao2;
         AV120WWRequisitoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV122WWRequisitoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV123WWRequisitoDS_11_Requisito_descricao3 = AV25Requisito_Descricao3;
         AV124WWRequisitoDS_12_Tfrequisito_codigo = AV39TFRequisito_Codigo;
         AV125WWRequisitoDS_13_Tfrequisito_codigo_to = AV40TFRequisito_Codigo_To;
         AV126WWRequisitoDS_14_Tfrequisito_descricao = AV43TFRequisito_Descricao;
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = AV44TFRequisito_Descricao_Sel;
         AV128WWRequisitoDS_16_Tfproposta_codigo = AV47TFProposta_Codigo;
         AV129WWRequisitoDS_17_Tfproposta_codigo_to = AV48TFProposta_Codigo_To;
         AV130WWRequisitoDS_18_Tfproposta_objetivo = AV51TFProposta_Objetivo;
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = AV52TFProposta_Objetivo_Sel;
         AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV108TFRequisito_TipoReqCod;
         AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV109TFRequisito_TipoReqCod_To;
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = AV63TFRequisito_Agrupador;
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV64TFRequisito_Agrupador_Sel;
         AV136WWRequisitoDS_24_Tfrequisito_titulo = AV67TFRequisito_Titulo;
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = AV68TFRequisito_Titulo_Sel;
         AV138WWRequisitoDS_26_Tfrequisito_restricao = AV75TFRequisito_Restricao;
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = AV76TFRequisito_Restricao_Sel;
         AV140WWRequisitoDS_28_Tfrequisito_ordem = AV83TFRequisito_Ordem;
         AV141WWRequisitoDS_29_Tfrequisito_ordem_to = AV84TFRequisito_Ordem_To;
         AV142WWRequisitoDS_30_Tfrequisito_pontuacao = AV87TFRequisito_Pontuacao;
         AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV88TFRequisito_Pontuacao_To;
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = AV91TFRequisito_DataHomologacao;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV92TFRequisito_DataHomologacao_To;
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = AV98TFRequisito_Status_Sels;
         AV147WWRequisitoDS_35_Tfrequisito_ativo_sel = AV101TFRequisito_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV114WWRequisitoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV115WWRequisitoDS_3_Requisito_descricao1 = AV17Requisito_Descricao1;
         AV116WWRequisitoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV118WWRequisitoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV119WWRequisitoDS_7_Requisito_descricao2 = AV21Requisito_Descricao2;
         AV120WWRequisitoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV122WWRequisitoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV123WWRequisitoDS_11_Requisito_descricao3 = AV25Requisito_Descricao3;
         AV124WWRequisitoDS_12_Tfrequisito_codigo = AV39TFRequisito_Codigo;
         AV125WWRequisitoDS_13_Tfrequisito_codigo_to = AV40TFRequisito_Codigo_To;
         AV126WWRequisitoDS_14_Tfrequisito_descricao = AV43TFRequisito_Descricao;
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = AV44TFRequisito_Descricao_Sel;
         AV128WWRequisitoDS_16_Tfproposta_codigo = AV47TFProposta_Codigo;
         AV129WWRequisitoDS_17_Tfproposta_codigo_to = AV48TFProposta_Codigo_To;
         AV130WWRequisitoDS_18_Tfproposta_objetivo = AV51TFProposta_Objetivo;
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = AV52TFProposta_Objetivo_Sel;
         AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV108TFRequisito_TipoReqCod;
         AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV109TFRequisito_TipoReqCod_To;
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = AV63TFRequisito_Agrupador;
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV64TFRequisito_Agrupador_Sel;
         AV136WWRequisitoDS_24_Tfrequisito_titulo = AV67TFRequisito_Titulo;
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = AV68TFRequisito_Titulo_Sel;
         AV138WWRequisitoDS_26_Tfrequisito_restricao = AV75TFRequisito_Restricao;
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = AV76TFRequisito_Restricao_Sel;
         AV140WWRequisitoDS_28_Tfrequisito_ordem = AV83TFRequisito_Ordem;
         AV141WWRequisitoDS_29_Tfrequisito_ordem_to = AV84TFRequisito_Ordem_To;
         AV142WWRequisitoDS_30_Tfrequisito_pontuacao = AV87TFRequisito_Pontuacao;
         AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV88TFRequisito_Pontuacao_To;
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = AV91TFRequisito_DataHomologacao;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV92TFRequisito_DataHomologacao_To;
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = AV98TFRequisito_Status_Sels;
         AV147WWRequisitoDS_35_Tfrequisito_ativo_sel = AV101TFRequisito_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPPZ0( )
      {
         /* Before Start, stand alone formulas. */
         AV151Pgmname = "WWRequisito";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E36PZ2 */
         E36PZ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV36ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV103DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_CODIGOTITLEFILTERDATA"), AV38Requisito_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_DESCRICAOTITLEFILTERDATA"), AV42Requisito_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROPOSTA_CODIGOTITLEFILTERDATA"), AV46Proposta_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROPOSTA_OBJETIVOTITLEFILTERDATA"), AV50Proposta_ObjetivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_TIPOREQCODTITLEFILTERDATA"), AV107Requisito_TipoReqCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_AGRUPADORTITLEFILTERDATA"), AV62Requisito_AgrupadorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_TITULOTITLEFILTERDATA"), AV66Requisito_TituloTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_RESTRICAOTITLEFILTERDATA"), AV74Requisito_RestricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_ORDEMTITLEFILTERDATA"), AV82Requisito_OrdemTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_PONTUACAOTITLEFILTERDATA"), AV86Requisito_PontuacaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_DATAHOMOLOGACAOTITLEFILTERDATA"), AV90Requisito_DataHomologacaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_STATUSTITLEFILTERDATA"), AV96Requisito_StatusTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREQUISITO_ATIVOTITLEFILTERDATA"), AV100Requisito_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Requisito_Descricao1 = cgiGet( edtavRequisito_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Requisito_Descricao1", AV17Requisito_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21Requisito_Descricao2 = cgiGet( edtavRequisito_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Requisito_Descricao2", AV21Requisito_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25Requisito_Descricao3 = cgiGet( edtavRequisito_descricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Requisito_Descricao3", AV25Requisito_Descricao3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV32ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_CODIGO");
               GX_FocusControl = edtavTfrequisito_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFRequisito_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0)));
            }
            else
            {
               AV39TFRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfrequisito_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_CODIGO_TO");
               GX_FocusControl = edtavTfrequisito_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFRequisito_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFRequisito_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0)));
            }
            else
            {
               AV40TFRequisito_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfrequisito_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFRequisito_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0)));
            }
            AV43TFRequisito_Descricao = cgiGet( edtavTfrequisito_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFRequisito_Descricao", AV43TFRequisito_Descricao);
            AV44TFRequisito_Descricao_Sel = cgiGet( edtavTfrequisito_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFRequisito_Descricao_Sel", AV44TFRequisito_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfproposta_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfproposta_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROPOSTA_CODIGO");
               GX_FocusControl = edtavTfproposta_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFProposta_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0)));
            }
            else
            {
               AV47TFProposta_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfproposta_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfproposta_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfproposta_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROPOSTA_CODIGO_TO");
               GX_FocusControl = edtavTfproposta_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFProposta_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProposta_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0)));
            }
            else
            {
               AV48TFProposta_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfproposta_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProposta_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0)));
            }
            AV51TFProposta_Objetivo = cgiGet( edtavTfproposta_objetivo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProposta_Objetivo", AV51TFProposta_Objetivo);
            AV52TFProposta_Objetivo_Sel = cgiGet( edtavTfproposta_objetivo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProposta_Objetivo_Sel", AV52TFProposta_Objetivo_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_tiporeqcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_tiporeqcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_TIPOREQCOD");
               GX_FocusControl = edtavTfrequisito_tiporeqcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV108TFRequisito_TipoReqCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFRequisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0)));
            }
            else
            {
               AV108TFRequisito_TipoReqCod = (int)(context.localUtil.CToN( cgiGet( edtavTfrequisito_tiporeqcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFRequisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_tiporeqcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_tiporeqcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_TIPOREQCOD_TO");
               GX_FocusControl = edtavTfrequisito_tiporeqcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV109TFRequisito_TipoReqCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFRequisito_TipoReqCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0)));
            }
            else
            {
               AV109TFRequisito_TipoReqCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfrequisito_tiporeqcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFRequisito_TipoReqCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0)));
            }
            AV63TFRequisito_Agrupador = cgiGet( edtavTfrequisito_agrupador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFRequisito_Agrupador", AV63TFRequisito_Agrupador);
            AV64TFRequisito_Agrupador_Sel = cgiGet( edtavTfrequisito_agrupador_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFRequisito_Agrupador_Sel", AV64TFRequisito_Agrupador_Sel);
            AV67TFRequisito_Titulo = cgiGet( edtavTfrequisito_titulo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFRequisito_Titulo", AV67TFRequisito_Titulo);
            AV68TFRequisito_Titulo_Sel = cgiGet( edtavTfrequisito_titulo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFRequisito_Titulo_Sel", AV68TFRequisito_Titulo_Sel);
            AV75TFRequisito_Restricao = cgiGet( edtavTfrequisito_restricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFRequisito_Restricao", AV75TFRequisito_Restricao);
            AV76TFRequisito_Restricao_Sel = cgiGet( edtavTfrequisito_restricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFRequisito_Restricao_Sel", AV76TFRequisito_Restricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_ORDEM");
               GX_FocusControl = edtavTfrequisito_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83TFRequisito_Ordem = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFRequisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0)));
            }
            else
            {
               AV83TFRequisito_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavTfrequisito_ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFRequisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_ordem_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_ordem_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_ORDEM_TO");
               GX_FocusControl = edtavTfrequisito_ordem_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84TFRequisito_Ordem_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFRequisito_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0)));
            }
            else
            {
               AV84TFRequisito_Ordem_To = (short)(context.localUtil.CToN( cgiGet( edtavTfrequisito_ordem_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFRequisito_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_pontuacao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_pontuacao_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_PONTUACAO");
               GX_FocusControl = edtavTfrequisito_pontuacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV87TFRequisito_Pontuacao = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFRequisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5)));
            }
            else
            {
               AV87TFRequisito_Pontuacao = context.localUtil.CToN( cgiGet( edtavTfrequisito_pontuacao_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFRequisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_pontuacao_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_pontuacao_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_PONTUACAO_TO");
               GX_FocusControl = edtavTfrequisito_pontuacao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV88TFRequisito_Pontuacao_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFRequisito_Pontuacao_To", StringUtil.LTrim( StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5)));
            }
            else
            {
               AV88TFRequisito_Pontuacao_To = context.localUtil.CToN( cgiGet( edtavTfrequisito_pontuacao_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFRequisito_Pontuacao_To", StringUtil.LTrim( StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfrequisito_datahomologacao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFRequisito_Data Homologacao"}), 1, "vTFREQUISITO_DATAHOMOLOGACAO");
               GX_FocusControl = edtavTfrequisito_datahomologacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV91TFRequisito_DataHomologacao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFRequisito_DataHomologacao", context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"));
            }
            else
            {
               AV91TFRequisito_DataHomologacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfrequisito_datahomologacao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFRequisito_DataHomologacao", context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfrequisito_datahomologacao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFRequisito_Data Homologacao_To"}), 1, "vTFREQUISITO_DATAHOMOLOGACAO_TO");
               GX_FocusControl = edtavTfrequisito_datahomologacao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV92TFRequisito_DataHomologacao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFRequisito_DataHomologacao_To", context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"));
            }
            else
            {
               AV92TFRequisito_DataHomologacao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfrequisito_datahomologacao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFRequisito_DataHomologacao_To", context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_requisito_datahomologacaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Requisito_Data Homologacao Aux Date"}), 1, "vDDO_REQUISITO_DATAHOMOLOGACAOAUXDATE");
               GX_FocusControl = edtavDdo_requisito_datahomologacaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV93DDO_Requisito_DataHomologacaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93DDO_Requisito_DataHomologacaoAuxDate", context.localUtil.Format(AV93DDO_Requisito_DataHomologacaoAuxDate, "99/99/99"));
            }
            else
            {
               AV93DDO_Requisito_DataHomologacaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_requisito_datahomologacaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93DDO_Requisito_DataHomologacaoAuxDate", context.localUtil.Format(AV93DDO_Requisito_DataHomologacaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_requisito_datahomologacaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Requisito_Data Homologacao Aux Date To"}), 1, "vDDO_REQUISITO_DATAHOMOLOGACAOAUXDATETO");
               GX_FocusControl = edtavDdo_requisito_datahomologacaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV94DDO_Requisito_DataHomologacaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94DDO_Requisito_DataHomologacaoAuxDateTo", context.localUtil.Format(AV94DDO_Requisito_DataHomologacaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV94DDO_Requisito_DataHomologacaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_requisito_datahomologacaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94DDO_Requisito_DataHomologacaoAuxDateTo", context.localUtil.Format(AV94DDO_Requisito_DataHomologacaoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfrequisito_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREQUISITO_ATIVO_SEL");
               GX_FocusControl = edtavTfrequisito_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV101TFRequisito_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFRequisito_Ativo_Sel", StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0));
            }
            else
            {
               AV101TFRequisito_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfrequisito_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFRequisito_Ativo_Sel", StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0));
            }
            AV41ddo_Requisito_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_requisito_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Requisito_CodigoTitleControlIdToReplace", AV41ddo_Requisito_CodigoTitleControlIdToReplace);
            AV45ddo_Requisito_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_requisito_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Requisito_DescricaoTitleControlIdToReplace", AV45ddo_Requisito_DescricaoTitleControlIdToReplace);
            AV49ddo_Proposta_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_proposta_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Proposta_CodigoTitleControlIdToReplace", AV49ddo_Proposta_CodigoTitleControlIdToReplace);
            AV53ddo_Proposta_ObjetivoTitleControlIdToReplace = cgiGet( edtavDdo_proposta_objetivotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Proposta_ObjetivoTitleControlIdToReplace", AV53ddo_Proposta_ObjetivoTitleControlIdToReplace);
            AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace = cgiGet( edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace", AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace);
            AV65ddo_Requisito_AgrupadorTitleControlIdToReplace = cgiGet( edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Requisito_AgrupadorTitleControlIdToReplace", AV65ddo_Requisito_AgrupadorTitleControlIdToReplace);
            AV69ddo_Requisito_TituloTitleControlIdToReplace = cgiGet( edtavDdo_requisito_titulotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Requisito_TituloTitleControlIdToReplace", AV69ddo_Requisito_TituloTitleControlIdToReplace);
            AV77ddo_Requisito_RestricaoTitleControlIdToReplace = cgiGet( edtavDdo_requisito_restricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Requisito_RestricaoTitleControlIdToReplace", AV77ddo_Requisito_RestricaoTitleControlIdToReplace);
            AV85ddo_Requisito_OrdemTitleControlIdToReplace = cgiGet( edtavDdo_requisito_ordemtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Requisito_OrdemTitleControlIdToReplace", AV85ddo_Requisito_OrdemTitleControlIdToReplace);
            AV89ddo_Requisito_PontuacaoTitleControlIdToReplace = cgiGet( edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Requisito_PontuacaoTitleControlIdToReplace", AV89ddo_Requisito_PontuacaoTitleControlIdToReplace);
            AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace = cgiGet( edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace", AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace);
            AV99ddo_Requisito_StatusTitleControlIdToReplace = cgiGet( edtavDdo_requisito_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_Requisito_StatusTitleControlIdToReplace", AV99ddo_Requisito_StatusTitleControlIdToReplace);
            AV102ddo_Requisito_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_requisito_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Requisito_AtivoTitleControlIdToReplace", AV102ddo_Requisito_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV105GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV106GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_requisito_codigo_Caption = cgiGet( "DDO_REQUISITO_CODIGO_Caption");
            Ddo_requisito_codigo_Tooltip = cgiGet( "DDO_REQUISITO_CODIGO_Tooltip");
            Ddo_requisito_codigo_Cls = cgiGet( "DDO_REQUISITO_CODIGO_Cls");
            Ddo_requisito_codigo_Filteredtext_set = cgiGet( "DDO_REQUISITO_CODIGO_Filteredtext_set");
            Ddo_requisito_codigo_Filteredtextto_set = cgiGet( "DDO_REQUISITO_CODIGO_Filteredtextto_set");
            Ddo_requisito_codigo_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_CODIGO_Dropdownoptionstype");
            Ddo_requisito_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_CODIGO_Titlecontrolidtoreplace");
            Ddo_requisito_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_CODIGO_Includesortasc"));
            Ddo_requisito_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_CODIGO_Includesortdsc"));
            Ddo_requisito_codigo_Sortedstatus = cgiGet( "DDO_REQUISITO_CODIGO_Sortedstatus");
            Ddo_requisito_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_CODIGO_Includefilter"));
            Ddo_requisito_codigo_Filtertype = cgiGet( "DDO_REQUISITO_CODIGO_Filtertype");
            Ddo_requisito_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_CODIGO_Filterisrange"));
            Ddo_requisito_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_CODIGO_Includedatalist"));
            Ddo_requisito_codigo_Sortasc = cgiGet( "DDO_REQUISITO_CODIGO_Sortasc");
            Ddo_requisito_codigo_Sortdsc = cgiGet( "DDO_REQUISITO_CODIGO_Sortdsc");
            Ddo_requisito_codigo_Cleanfilter = cgiGet( "DDO_REQUISITO_CODIGO_Cleanfilter");
            Ddo_requisito_codigo_Rangefilterfrom = cgiGet( "DDO_REQUISITO_CODIGO_Rangefilterfrom");
            Ddo_requisito_codigo_Rangefilterto = cgiGet( "DDO_REQUISITO_CODIGO_Rangefilterto");
            Ddo_requisito_codigo_Searchbuttontext = cgiGet( "DDO_REQUISITO_CODIGO_Searchbuttontext");
            Ddo_requisito_descricao_Caption = cgiGet( "DDO_REQUISITO_DESCRICAO_Caption");
            Ddo_requisito_descricao_Tooltip = cgiGet( "DDO_REQUISITO_DESCRICAO_Tooltip");
            Ddo_requisito_descricao_Cls = cgiGet( "DDO_REQUISITO_DESCRICAO_Cls");
            Ddo_requisito_descricao_Filteredtext_set = cgiGet( "DDO_REQUISITO_DESCRICAO_Filteredtext_set");
            Ddo_requisito_descricao_Selectedvalue_set = cgiGet( "DDO_REQUISITO_DESCRICAO_Selectedvalue_set");
            Ddo_requisito_descricao_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_DESCRICAO_Dropdownoptionstype");
            Ddo_requisito_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_requisito_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DESCRICAO_Includesortasc"));
            Ddo_requisito_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DESCRICAO_Includesortdsc"));
            Ddo_requisito_descricao_Sortedstatus = cgiGet( "DDO_REQUISITO_DESCRICAO_Sortedstatus");
            Ddo_requisito_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DESCRICAO_Includefilter"));
            Ddo_requisito_descricao_Filtertype = cgiGet( "DDO_REQUISITO_DESCRICAO_Filtertype");
            Ddo_requisito_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DESCRICAO_Filterisrange"));
            Ddo_requisito_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DESCRICAO_Includedatalist"));
            Ddo_requisito_descricao_Datalisttype = cgiGet( "DDO_REQUISITO_DESCRICAO_Datalisttype");
            Ddo_requisito_descricao_Datalistproc = cgiGet( "DDO_REQUISITO_DESCRICAO_Datalistproc");
            Ddo_requisito_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REQUISITO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_requisito_descricao_Sortasc = cgiGet( "DDO_REQUISITO_DESCRICAO_Sortasc");
            Ddo_requisito_descricao_Sortdsc = cgiGet( "DDO_REQUISITO_DESCRICAO_Sortdsc");
            Ddo_requisito_descricao_Loadingdata = cgiGet( "DDO_REQUISITO_DESCRICAO_Loadingdata");
            Ddo_requisito_descricao_Cleanfilter = cgiGet( "DDO_REQUISITO_DESCRICAO_Cleanfilter");
            Ddo_requisito_descricao_Noresultsfound = cgiGet( "DDO_REQUISITO_DESCRICAO_Noresultsfound");
            Ddo_requisito_descricao_Searchbuttontext = cgiGet( "DDO_REQUISITO_DESCRICAO_Searchbuttontext");
            Ddo_proposta_codigo_Caption = cgiGet( "DDO_PROPOSTA_CODIGO_Caption");
            Ddo_proposta_codigo_Tooltip = cgiGet( "DDO_PROPOSTA_CODIGO_Tooltip");
            Ddo_proposta_codigo_Cls = cgiGet( "DDO_PROPOSTA_CODIGO_Cls");
            Ddo_proposta_codigo_Filteredtext_set = cgiGet( "DDO_PROPOSTA_CODIGO_Filteredtext_set");
            Ddo_proposta_codigo_Filteredtextto_set = cgiGet( "DDO_PROPOSTA_CODIGO_Filteredtextto_set");
            Ddo_proposta_codigo_Dropdownoptionstype = cgiGet( "DDO_PROPOSTA_CODIGO_Dropdownoptionstype");
            Ddo_proposta_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_PROPOSTA_CODIGO_Titlecontrolidtoreplace");
            Ddo_proposta_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_CODIGO_Includesortasc"));
            Ddo_proposta_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_CODIGO_Includesortdsc"));
            Ddo_proposta_codigo_Sortedstatus = cgiGet( "DDO_PROPOSTA_CODIGO_Sortedstatus");
            Ddo_proposta_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_CODIGO_Includefilter"));
            Ddo_proposta_codigo_Filtertype = cgiGet( "DDO_PROPOSTA_CODIGO_Filtertype");
            Ddo_proposta_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_CODIGO_Filterisrange"));
            Ddo_proposta_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_CODIGO_Includedatalist"));
            Ddo_proposta_codigo_Sortasc = cgiGet( "DDO_PROPOSTA_CODIGO_Sortasc");
            Ddo_proposta_codigo_Sortdsc = cgiGet( "DDO_PROPOSTA_CODIGO_Sortdsc");
            Ddo_proposta_codigo_Cleanfilter = cgiGet( "DDO_PROPOSTA_CODIGO_Cleanfilter");
            Ddo_proposta_codigo_Rangefilterfrom = cgiGet( "DDO_PROPOSTA_CODIGO_Rangefilterfrom");
            Ddo_proposta_codigo_Rangefilterto = cgiGet( "DDO_PROPOSTA_CODIGO_Rangefilterto");
            Ddo_proposta_codigo_Searchbuttontext = cgiGet( "DDO_PROPOSTA_CODIGO_Searchbuttontext");
            Ddo_proposta_objetivo_Caption = cgiGet( "DDO_PROPOSTA_OBJETIVO_Caption");
            Ddo_proposta_objetivo_Tooltip = cgiGet( "DDO_PROPOSTA_OBJETIVO_Tooltip");
            Ddo_proposta_objetivo_Cls = cgiGet( "DDO_PROPOSTA_OBJETIVO_Cls");
            Ddo_proposta_objetivo_Filteredtext_set = cgiGet( "DDO_PROPOSTA_OBJETIVO_Filteredtext_set");
            Ddo_proposta_objetivo_Selectedvalue_set = cgiGet( "DDO_PROPOSTA_OBJETIVO_Selectedvalue_set");
            Ddo_proposta_objetivo_Dropdownoptionstype = cgiGet( "DDO_PROPOSTA_OBJETIVO_Dropdownoptionstype");
            Ddo_proposta_objetivo_Titlecontrolidtoreplace = cgiGet( "DDO_PROPOSTA_OBJETIVO_Titlecontrolidtoreplace");
            Ddo_proposta_objetivo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_OBJETIVO_Includesortasc"));
            Ddo_proposta_objetivo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_OBJETIVO_Includesortdsc"));
            Ddo_proposta_objetivo_Sortedstatus = cgiGet( "DDO_PROPOSTA_OBJETIVO_Sortedstatus");
            Ddo_proposta_objetivo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_OBJETIVO_Includefilter"));
            Ddo_proposta_objetivo_Filtertype = cgiGet( "DDO_PROPOSTA_OBJETIVO_Filtertype");
            Ddo_proposta_objetivo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_OBJETIVO_Filterisrange"));
            Ddo_proposta_objetivo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROPOSTA_OBJETIVO_Includedatalist"));
            Ddo_proposta_objetivo_Datalisttype = cgiGet( "DDO_PROPOSTA_OBJETIVO_Datalisttype");
            Ddo_proposta_objetivo_Datalistproc = cgiGet( "DDO_PROPOSTA_OBJETIVO_Datalistproc");
            Ddo_proposta_objetivo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PROPOSTA_OBJETIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_proposta_objetivo_Sortasc = cgiGet( "DDO_PROPOSTA_OBJETIVO_Sortasc");
            Ddo_proposta_objetivo_Sortdsc = cgiGet( "DDO_PROPOSTA_OBJETIVO_Sortdsc");
            Ddo_proposta_objetivo_Loadingdata = cgiGet( "DDO_PROPOSTA_OBJETIVO_Loadingdata");
            Ddo_proposta_objetivo_Cleanfilter = cgiGet( "DDO_PROPOSTA_OBJETIVO_Cleanfilter");
            Ddo_proposta_objetivo_Noresultsfound = cgiGet( "DDO_PROPOSTA_OBJETIVO_Noresultsfound");
            Ddo_proposta_objetivo_Searchbuttontext = cgiGet( "DDO_PROPOSTA_OBJETIVO_Searchbuttontext");
            Ddo_requisito_tiporeqcod_Caption = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Caption");
            Ddo_requisito_tiporeqcod_Tooltip = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Tooltip");
            Ddo_requisito_tiporeqcod_Cls = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Cls");
            Ddo_requisito_tiporeqcod_Filteredtext_set = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Filteredtext_set");
            Ddo_requisito_tiporeqcod_Filteredtextto_set = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Filteredtextto_set");
            Ddo_requisito_tiporeqcod_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Dropdownoptionstype");
            Ddo_requisito_tiporeqcod_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Titlecontrolidtoreplace");
            Ddo_requisito_tiporeqcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TIPOREQCOD_Includesortasc"));
            Ddo_requisito_tiporeqcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TIPOREQCOD_Includesortdsc"));
            Ddo_requisito_tiporeqcod_Sortedstatus = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Sortedstatus");
            Ddo_requisito_tiporeqcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TIPOREQCOD_Includefilter"));
            Ddo_requisito_tiporeqcod_Filtertype = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Filtertype");
            Ddo_requisito_tiporeqcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TIPOREQCOD_Filterisrange"));
            Ddo_requisito_tiporeqcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TIPOREQCOD_Includedatalist"));
            Ddo_requisito_tiporeqcod_Sortasc = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Sortasc");
            Ddo_requisito_tiporeqcod_Sortdsc = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Sortdsc");
            Ddo_requisito_tiporeqcod_Cleanfilter = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Cleanfilter");
            Ddo_requisito_tiporeqcod_Rangefilterfrom = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Rangefilterfrom");
            Ddo_requisito_tiporeqcod_Rangefilterto = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Rangefilterto");
            Ddo_requisito_tiporeqcod_Searchbuttontext = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Searchbuttontext");
            Ddo_requisito_agrupador_Caption = cgiGet( "DDO_REQUISITO_AGRUPADOR_Caption");
            Ddo_requisito_agrupador_Tooltip = cgiGet( "DDO_REQUISITO_AGRUPADOR_Tooltip");
            Ddo_requisito_agrupador_Cls = cgiGet( "DDO_REQUISITO_AGRUPADOR_Cls");
            Ddo_requisito_agrupador_Filteredtext_set = cgiGet( "DDO_REQUISITO_AGRUPADOR_Filteredtext_set");
            Ddo_requisito_agrupador_Selectedvalue_set = cgiGet( "DDO_REQUISITO_AGRUPADOR_Selectedvalue_set");
            Ddo_requisito_agrupador_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_AGRUPADOR_Dropdownoptionstype");
            Ddo_requisito_agrupador_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_AGRUPADOR_Titlecontrolidtoreplace");
            Ddo_requisito_agrupador_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_AGRUPADOR_Includesortasc"));
            Ddo_requisito_agrupador_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_AGRUPADOR_Includesortdsc"));
            Ddo_requisito_agrupador_Sortedstatus = cgiGet( "DDO_REQUISITO_AGRUPADOR_Sortedstatus");
            Ddo_requisito_agrupador_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_AGRUPADOR_Includefilter"));
            Ddo_requisito_agrupador_Filtertype = cgiGet( "DDO_REQUISITO_AGRUPADOR_Filtertype");
            Ddo_requisito_agrupador_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_AGRUPADOR_Filterisrange"));
            Ddo_requisito_agrupador_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_AGRUPADOR_Includedatalist"));
            Ddo_requisito_agrupador_Datalisttype = cgiGet( "DDO_REQUISITO_AGRUPADOR_Datalisttype");
            Ddo_requisito_agrupador_Datalistproc = cgiGet( "DDO_REQUISITO_AGRUPADOR_Datalistproc");
            Ddo_requisito_agrupador_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REQUISITO_AGRUPADOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_requisito_agrupador_Sortasc = cgiGet( "DDO_REQUISITO_AGRUPADOR_Sortasc");
            Ddo_requisito_agrupador_Sortdsc = cgiGet( "DDO_REQUISITO_AGRUPADOR_Sortdsc");
            Ddo_requisito_agrupador_Loadingdata = cgiGet( "DDO_REQUISITO_AGRUPADOR_Loadingdata");
            Ddo_requisito_agrupador_Cleanfilter = cgiGet( "DDO_REQUISITO_AGRUPADOR_Cleanfilter");
            Ddo_requisito_agrupador_Noresultsfound = cgiGet( "DDO_REQUISITO_AGRUPADOR_Noresultsfound");
            Ddo_requisito_agrupador_Searchbuttontext = cgiGet( "DDO_REQUISITO_AGRUPADOR_Searchbuttontext");
            Ddo_requisito_titulo_Caption = cgiGet( "DDO_REQUISITO_TITULO_Caption");
            Ddo_requisito_titulo_Tooltip = cgiGet( "DDO_REQUISITO_TITULO_Tooltip");
            Ddo_requisito_titulo_Cls = cgiGet( "DDO_REQUISITO_TITULO_Cls");
            Ddo_requisito_titulo_Filteredtext_set = cgiGet( "DDO_REQUISITO_TITULO_Filteredtext_set");
            Ddo_requisito_titulo_Selectedvalue_set = cgiGet( "DDO_REQUISITO_TITULO_Selectedvalue_set");
            Ddo_requisito_titulo_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_TITULO_Dropdownoptionstype");
            Ddo_requisito_titulo_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_TITULO_Titlecontrolidtoreplace");
            Ddo_requisito_titulo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TITULO_Includesortasc"));
            Ddo_requisito_titulo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TITULO_Includesortdsc"));
            Ddo_requisito_titulo_Sortedstatus = cgiGet( "DDO_REQUISITO_TITULO_Sortedstatus");
            Ddo_requisito_titulo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TITULO_Includefilter"));
            Ddo_requisito_titulo_Filtertype = cgiGet( "DDO_REQUISITO_TITULO_Filtertype");
            Ddo_requisito_titulo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TITULO_Filterisrange"));
            Ddo_requisito_titulo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_TITULO_Includedatalist"));
            Ddo_requisito_titulo_Datalisttype = cgiGet( "DDO_REQUISITO_TITULO_Datalisttype");
            Ddo_requisito_titulo_Datalistproc = cgiGet( "DDO_REQUISITO_TITULO_Datalistproc");
            Ddo_requisito_titulo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REQUISITO_TITULO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_requisito_titulo_Sortasc = cgiGet( "DDO_REQUISITO_TITULO_Sortasc");
            Ddo_requisito_titulo_Sortdsc = cgiGet( "DDO_REQUISITO_TITULO_Sortdsc");
            Ddo_requisito_titulo_Loadingdata = cgiGet( "DDO_REQUISITO_TITULO_Loadingdata");
            Ddo_requisito_titulo_Cleanfilter = cgiGet( "DDO_REQUISITO_TITULO_Cleanfilter");
            Ddo_requisito_titulo_Noresultsfound = cgiGet( "DDO_REQUISITO_TITULO_Noresultsfound");
            Ddo_requisito_titulo_Searchbuttontext = cgiGet( "DDO_REQUISITO_TITULO_Searchbuttontext");
            Ddo_requisito_restricao_Caption = cgiGet( "DDO_REQUISITO_RESTRICAO_Caption");
            Ddo_requisito_restricao_Tooltip = cgiGet( "DDO_REQUISITO_RESTRICAO_Tooltip");
            Ddo_requisito_restricao_Cls = cgiGet( "DDO_REQUISITO_RESTRICAO_Cls");
            Ddo_requisito_restricao_Filteredtext_set = cgiGet( "DDO_REQUISITO_RESTRICAO_Filteredtext_set");
            Ddo_requisito_restricao_Selectedvalue_set = cgiGet( "DDO_REQUISITO_RESTRICAO_Selectedvalue_set");
            Ddo_requisito_restricao_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_RESTRICAO_Dropdownoptionstype");
            Ddo_requisito_restricao_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_RESTRICAO_Titlecontrolidtoreplace");
            Ddo_requisito_restricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_RESTRICAO_Includesortasc"));
            Ddo_requisito_restricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_RESTRICAO_Includesortdsc"));
            Ddo_requisito_restricao_Sortedstatus = cgiGet( "DDO_REQUISITO_RESTRICAO_Sortedstatus");
            Ddo_requisito_restricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_RESTRICAO_Includefilter"));
            Ddo_requisito_restricao_Filtertype = cgiGet( "DDO_REQUISITO_RESTRICAO_Filtertype");
            Ddo_requisito_restricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_RESTRICAO_Filterisrange"));
            Ddo_requisito_restricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_RESTRICAO_Includedatalist"));
            Ddo_requisito_restricao_Datalisttype = cgiGet( "DDO_REQUISITO_RESTRICAO_Datalisttype");
            Ddo_requisito_restricao_Datalistproc = cgiGet( "DDO_REQUISITO_RESTRICAO_Datalistproc");
            Ddo_requisito_restricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REQUISITO_RESTRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_requisito_restricao_Sortasc = cgiGet( "DDO_REQUISITO_RESTRICAO_Sortasc");
            Ddo_requisito_restricao_Sortdsc = cgiGet( "DDO_REQUISITO_RESTRICAO_Sortdsc");
            Ddo_requisito_restricao_Loadingdata = cgiGet( "DDO_REQUISITO_RESTRICAO_Loadingdata");
            Ddo_requisito_restricao_Cleanfilter = cgiGet( "DDO_REQUISITO_RESTRICAO_Cleanfilter");
            Ddo_requisito_restricao_Noresultsfound = cgiGet( "DDO_REQUISITO_RESTRICAO_Noresultsfound");
            Ddo_requisito_restricao_Searchbuttontext = cgiGet( "DDO_REQUISITO_RESTRICAO_Searchbuttontext");
            Ddo_requisito_ordem_Caption = cgiGet( "DDO_REQUISITO_ORDEM_Caption");
            Ddo_requisito_ordem_Tooltip = cgiGet( "DDO_REQUISITO_ORDEM_Tooltip");
            Ddo_requisito_ordem_Cls = cgiGet( "DDO_REQUISITO_ORDEM_Cls");
            Ddo_requisito_ordem_Filteredtext_set = cgiGet( "DDO_REQUISITO_ORDEM_Filteredtext_set");
            Ddo_requisito_ordem_Filteredtextto_set = cgiGet( "DDO_REQUISITO_ORDEM_Filteredtextto_set");
            Ddo_requisito_ordem_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_ORDEM_Dropdownoptionstype");
            Ddo_requisito_ordem_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_ORDEM_Titlecontrolidtoreplace");
            Ddo_requisito_ordem_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ORDEM_Includesortasc"));
            Ddo_requisito_ordem_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ORDEM_Includesortdsc"));
            Ddo_requisito_ordem_Sortedstatus = cgiGet( "DDO_REQUISITO_ORDEM_Sortedstatus");
            Ddo_requisito_ordem_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ORDEM_Includefilter"));
            Ddo_requisito_ordem_Filtertype = cgiGet( "DDO_REQUISITO_ORDEM_Filtertype");
            Ddo_requisito_ordem_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ORDEM_Filterisrange"));
            Ddo_requisito_ordem_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ORDEM_Includedatalist"));
            Ddo_requisito_ordem_Sortasc = cgiGet( "DDO_REQUISITO_ORDEM_Sortasc");
            Ddo_requisito_ordem_Sortdsc = cgiGet( "DDO_REQUISITO_ORDEM_Sortdsc");
            Ddo_requisito_ordem_Cleanfilter = cgiGet( "DDO_REQUISITO_ORDEM_Cleanfilter");
            Ddo_requisito_ordem_Rangefilterfrom = cgiGet( "DDO_REQUISITO_ORDEM_Rangefilterfrom");
            Ddo_requisito_ordem_Rangefilterto = cgiGet( "DDO_REQUISITO_ORDEM_Rangefilterto");
            Ddo_requisito_ordem_Searchbuttontext = cgiGet( "DDO_REQUISITO_ORDEM_Searchbuttontext");
            Ddo_requisito_pontuacao_Caption = cgiGet( "DDO_REQUISITO_PONTUACAO_Caption");
            Ddo_requisito_pontuacao_Tooltip = cgiGet( "DDO_REQUISITO_PONTUACAO_Tooltip");
            Ddo_requisito_pontuacao_Cls = cgiGet( "DDO_REQUISITO_PONTUACAO_Cls");
            Ddo_requisito_pontuacao_Filteredtext_set = cgiGet( "DDO_REQUISITO_PONTUACAO_Filteredtext_set");
            Ddo_requisito_pontuacao_Filteredtextto_set = cgiGet( "DDO_REQUISITO_PONTUACAO_Filteredtextto_set");
            Ddo_requisito_pontuacao_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_PONTUACAO_Dropdownoptionstype");
            Ddo_requisito_pontuacao_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_PONTUACAO_Titlecontrolidtoreplace");
            Ddo_requisito_pontuacao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_PONTUACAO_Includesortasc"));
            Ddo_requisito_pontuacao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_PONTUACAO_Includesortdsc"));
            Ddo_requisito_pontuacao_Sortedstatus = cgiGet( "DDO_REQUISITO_PONTUACAO_Sortedstatus");
            Ddo_requisito_pontuacao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_PONTUACAO_Includefilter"));
            Ddo_requisito_pontuacao_Filtertype = cgiGet( "DDO_REQUISITO_PONTUACAO_Filtertype");
            Ddo_requisito_pontuacao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_PONTUACAO_Filterisrange"));
            Ddo_requisito_pontuacao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_PONTUACAO_Includedatalist"));
            Ddo_requisito_pontuacao_Sortasc = cgiGet( "DDO_REQUISITO_PONTUACAO_Sortasc");
            Ddo_requisito_pontuacao_Sortdsc = cgiGet( "DDO_REQUISITO_PONTUACAO_Sortdsc");
            Ddo_requisito_pontuacao_Cleanfilter = cgiGet( "DDO_REQUISITO_PONTUACAO_Cleanfilter");
            Ddo_requisito_pontuacao_Rangefilterfrom = cgiGet( "DDO_REQUISITO_PONTUACAO_Rangefilterfrom");
            Ddo_requisito_pontuacao_Rangefilterto = cgiGet( "DDO_REQUISITO_PONTUACAO_Rangefilterto");
            Ddo_requisito_pontuacao_Searchbuttontext = cgiGet( "DDO_REQUISITO_PONTUACAO_Searchbuttontext");
            Ddo_requisito_datahomologacao_Caption = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Caption");
            Ddo_requisito_datahomologacao_Tooltip = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Tooltip");
            Ddo_requisito_datahomologacao_Cls = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Cls");
            Ddo_requisito_datahomologacao_Filteredtext_set = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtext_set");
            Ddo_requisito_datahomologacao_Filteredtextto_set = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtextto_set");
            Ddo_requisito_datahomologacao_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Dropdownoptionstype");
            Ddo_requisito_datahomologacao_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Titlecontrolidtoreplace");
            Ddo_requisito_datahomologacao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Includesortasc"));
            Ddo_requisito_datahomologacao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Includesortdsc"));
            Ddo_requisito_datahomologacao_Sortedstatus = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Sortedstatus");
            Ddo_requisito_datahomologacao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Includefilter"));
            Ddo_requisito_datahomologacao_Filtertype = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Filtertype");
            Ddo_requisito_datahomologacao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Filterisrange"));
            Ddo_requisito_datahomologacao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Includedatalist"));
            Ddo_requisito_datahomologacao_Sortasc = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Sortasc");
            Ddo_requisito_datahomologacao_Sortdsc = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Sortdsc");
            Ddo_requisito_datahomologacao_Cleanfilter = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Cleanfilter");
            Ddo_requisito_datahomologacao_Rangefilterfrom = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Rangefilterfrom");
            Ddo_requisito_datahomologacao_Rangefilterto = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Rangefilterto");
            Ddo_requisito_datahomologacao_Searchbuttontext = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Searchbuttontext");
            Ddo_requisito_status_Caption = cgiGet( "DDO_REQUISITO_STATUS_Caption");
            Ddo_requisito_status_Tooltip = cgiGet( "DDO_REQUISITO_STATUS_Tooltip");
            Ddo_requisito_status_Cls = cgiGet( "DDO_REQUISITO_STATUS_Cls");
            Ddo_requisito_status_Selectedvalue_set = cgiGet( "DDO_REQUISITO_STATUS_Selectedvalue_set");
            Ddo_requisito_status_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_STATUS_Dropdownoptionstype");
            Ddo_requisito_status_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_STATUS_Titlecontrolidtoreplace");
            Ddo_requisito_status_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_STATUS_Includesortasc"));
            Ddo_requisito_status_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_STATUS_Includesortdsc"));
            Ddo_requisito_status_Sortedstatus = cgiGet( "DDO_REQUISITO_STATUS_Sortedstatus");
            Ddo_requisito_status_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_STATUS_Includefilter"));
            Ddo_requisito_status_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_STATUS_Includedatalist"));
            Ddo_requisito_status_Datalisttype = cgiGet( "DDO_REQUISITO_STATUS_Datalisttype");
            Ddo_requisito_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_STATUS_Allowmultipleselection"));
            Ddo_requisito_status_Datalistfixedvalues = cgiGet( "DDO_REQUISITO_STATUS_Datalistfixedvalues");
            Ddo_requisito_status_Sortasc = cgiGet( "DDO_REQUISITO_STATUS_Sortasc");
            Ddo_requisito_status_Sortdsc = cgiGet( "DDO_REQUISITO_STATUS_Sortdsc");
            Ddo_requisito_status_Cleanfilter = cgiGet( "DDO_REQUISITO_STATUS_Cleanfilter");
            Ddo_requisito_status_Searchbuttontext = cgiGet( "DDO_REQUISITO_STATUS_Searchbuttontext");
            Ddo_requisito_ativo_Caption = cgiGet( "DDO_REQUISITO_ATIVO_Caption");
            Ddo_requisito_ativo_Tooltip = cgiGet( "DDO_REQUISITO_ATIVO_Tooltip");
            Ddo_requisito_ativo_Cls = cgiGet( "DDO_REQUISITO_ATIVO_Cls");
            Ddo_requisito_ativo_Selectedvalue_set = cgiGet( "DDO_REQUISITO_ATIVO_Selectedvalue_set");
            Ddo_requisito_ativo_Dropdownoptionstype = cgiGet( "DDO_REQUISITO_ATIVO_Dropdownoptionstype");
            Ddo_requisito_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_REQUISITO_ATIVO_Titlecontrolidtoreplace");
            Ddo_requisito_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ATIVO_Includesortasc"));
            Ddo_requisito_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ATIVO_Includesortdsc"));
            Ddo_requisito_ativo_Sortedstatus = cgiGet( "DDO_REQUISITO_ATIVO_Sortedstatus");
            Ddo_requisito_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ATIVO_Includefilter"));
            Ddo_requisito_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REQUISITO_ATIVO_Includedatalist"));
            Ddo_requisito_ativo_Datalisttype = cgiGet( "DDO_REQUISITO_ATIVO_Datalisttype");
            Ddo_requisito_ativo_Datalistfixedvalues = cgiGet( "DDO_REQUISITO_ATIVO_Datalistfixedvalues");
            Ddo_requisito_ativo_Sortasc = cgiGet( "DDO_REQUISITO_ATIVO_Sortasc");
            Ddo_requisito_ativo_Sortdsc = cgiGet( "DDO_REQUISITO_ATIVO_Sortdsc");
            Ddo_requisito_ativo_Cleanfilter = cgiGet( "DDO_REQUISITO_ATIVO_Cleanfilter");
            Ddo_requisito_ativo_Searchbuttontext = cgiGet( "DDO_REQUISITO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_requisito_codigo_Activeeventkey = cgiGet( "DDO_REQUISITO_CODIGO_Activeeventkey");
            Ddo_requisito_codigo_Filteredtext_get = cgiGet( "DDO_REQUISITO_CODIGO_Filteredtext_get");
            Ddo_requisito_codigo_Filteredtextto_get = cgiGet( "DDO_REQUISITO_CODIGO_Filteredtextto_get");
            Ddo_requisito_descricao_Activeeventkey = cgiGet( "DDO_REQUISITO_DESCRICAO_Activeeventkey");
            Ddo_requisito_descricao_Filteredtext_get = cgiGet( "DDO_REQUISITO_DESCRICAO_Filteredtext_get");
            Ddo_requisito_descricao_Selectedvalue_get = cgiGet( "DDO_REQUISITO_DESCRICAO_Selectedvalue_get");
            Ddo_proposta_codigo_Activeeventkey = cgiGet( "DDO_PROPOSTA_CODIGO_Activeeventkey");
            Ddo_proposta_codigo_Filteredtext_get = cgiGet( "DDO_PROPOSTA_CODIGO_Filteredtext_get");
            Ddo_proposta_codigo_Filteredtextto_get = cgiGet( "DDO_PROPOSTA_CODIGO_Filteredtextto_get");
            Ddo_proposta_objetivo_Activeeventkey = cgiGet( "DDO_PROPOSTA_OBJETIVO_Activeeventkey");
            Ddo_proposta_objetivo_Filteredtext_get = cgiGet( "DDO_PROPOSTA_OBJETIVO_Filteredtext_get");
            Ddo_proposta_objetivo_Selectedvalue_get = cgiGet( "DDO_PROPOSTA_OBJETIVO_Selectedvalue_get");
            Ddo_requisito_tiporeqcod_Activeeventkey = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Activeeventkey");
            Ddo_requisito_tiporeqcod_Filteredtext_get = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Filteredtext_get");
            Ddo_requisito_tiporeqcod_Filteredtextto_get = cgiGet( "DDO_REQUISITO_TIPOREQCOD_Filteredtextto_get");
            Ddo_requisito_agrupador_Activeeventkey = cgiGet( "DDO_REQUISITO_AGRUPADOR_Activeeventkey");
            Ddo_requisito_agrupador_Filteredtext_get = cgiGet( "DDO_REQUISITO_AGRUPADOR_Filteredtext_get");
            Ddo_requisito_agrupador_Selectedvalue_get = cgiGet( "DDO_REQUISITO_AGRUPADOR_Selectedvalue_get");
            Ddo_requisito_titulo_Activeeventkey = cgiGet( "DDO_REQUISITO_TITULO_Activeeventkey");
            Ddo_requisito_titulo_Filteredtext_get = cgiGet( "DDO_REQUISITO_TITULO_Filteredtext_get");
            Ddo_requisito_titulo_Selectedvalue_get = cgiGet( "DDO_REQUISITO_TITULO_Selectedvalue_get");
            Ddo_requisito_restricao_Activeeventkey = cgiGet( "DDO_REQUISITO_RESTRICAO_Activeeventkey");
            Ddo_requisito_restricao_Filteredtext_get = cgiGet( "DDO_REQUISITO_RESTRICAO_Filteredtext_get");
            Ddo_requisito_restricao_Selectedvalue_get = cgiGet( "DDO_REQUISITO_RESTRICAO_Selectedvalue_get");
            Ddo_requisito_ordem_Activeeventkey = cgiGet( "DDO_REQUISITO_ORDEM_Activeeventkey");
            Ddo_requisito_ordem_Filteredtext_get = cgiGet( "DDO_REQUISITO_ORDEM_Filteredtext_get");
            Ddo_requisito_ordem_Filteredtextto_get = cgiGet( "DDO_REQUISITO_ORDEM_Filteredtextto_get");
            Ddo_requisito_pontuacao_Activeeventkey = cgiGet( "DDO_REQUISITO_PONTUACAO_Activeeventkey");
            Ddo_requisito_pontuacao_Filteredtext_get = cgiGet( "DDO_REQUISITO_PONTUACAO_Filteredtext_get");
            Ddo_requisito_pontuacao_Filteredtextto_get = cgiGet( "DDO_REQUISITO_PONTUACAO_Filteredtextto_get");
            Ddo_requisito_datahomologacao_Activeeventkey = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Activeeventkey");
            Ddo_requisito_datahomologacao_Filteredtext_get = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtext_get");
            Ddo_requisito_datahomologacao_Filteredtextto_get = cgiGet( "DDO_REQUISITO_DATAHOMOLOGACAO_Filteredtextto_get");
            Ddo_requisito_status_Activeeventkey = cgiGet( "DDO_REQUISITO_STATUS_Activeeventkey");
            Ddo_requisito_status_Selectedvalue_get = cgiGet( "DDO_REQUISITO_STATUS_Selectedvalue_get");
            Ddo_requisito_ativo_Activeeventkey = cgiGet( "DDO_REQUISITO_ATIVO_Activeeventkey");
            Ddo_requisito_ativo_Selectedvalue_get = cgiGet( "DDO_REQUISITO_ATIVO_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREQUISITO_DESCRICAO1"), AV17Requisito_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREQUISITO_DESCRICAO2"), AV21Requisito_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREQUISITO_DESCRICAO3"), AV25Requisito_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_CODIGO"), ",", ".") != Convert.ToDecimal( AV39TFRequisito_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV40TFRequisito_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_DESCRICAO"), AV43TFRequisito_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_DESCRICAO_SEL"), AV44TFRequisito_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROPOSTA_CODIGO"), ",", ".") != Convert.ToDecimal( AV47TFProposta_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROPOSTA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV48TFProposta_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROPOSTA_OBJETIVO"), AV51TFProposta_Objetivo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPROPOSTA_OBJETIVO_SEL"), AV52TFProposta_Objetivo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_TIPOREQCOD"), ",", ".") != Convert.ToDecimal( AV108TFRequisito_TipoReqCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_TIPOREQCOD_TO"), ",", ".") != Convert.ToDecimal( AV109TFRequisito_TipoReqCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_AGRUPADOR"), AV63TFRequisito_Agrupador) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_AGRUPADOR_SEL"), AV64TFRequisito_Agrupador_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_TITULO"), AV67TFRequisito_Titulo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_TITULO_SEL"), AV68TFRequisito_Titulo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_RESTRICAO"), AV75TFRequisito_Restricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREQUISITO_RESTRICAO_SEL"), AV76TFRequisito_Restricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_ORDEM"), ",", ".") != Convert.ToDecimal( AV83TFRequisito_Ordem )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV84TFRequisito_Ordem_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_PONTUACAO"), ",", ".") != AV87TFRequisito_Pontuacao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_PONTUACAO_TO"), ",", ".") != AV88TFRequisito_Pontuacao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFREQUISITO_DATAHOMOLOGACAO"), 0) != AV91TFRequisito_DataHomologacao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFREQUISITO_DATAHOMOLOGACAO_TO"), 0) != AV92TFRequisito_DataHomologacao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREQUISITO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV101TFRequisito_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E36PZ2 */
         E36PZ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E36PZ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "REQUISITO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "REQUISITO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "REQUISITO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfrequisito_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_codigo_Visible), 5, 0)));
         edtavTfrequisito_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_codigo_to_Visible), 5, 0)));
         edtavTfrequisito_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_descricao_Visible), 5, 0)));
         edtavTfrequisito_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_descricao_sel_Visible), 5, 0)));
         edtavTfproposta_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfproposta_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfproposta_codigo_Visible), 5, 0)));
         edtavTfproposta_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfproposta_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfproposta_codigo_to_Visible), 5, 0)));
         edtavTfproposta_objetivo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfproposta_objetivo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfproposta_objetivo_Visible), 5, 0)));
         edtavTfproposta_objetivo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfproposta_objetivo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfproposta_objetivo_sel_Visible), 5, 0)));
         edtavTfrequisito_tiporeqcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_tiporeqcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_tiporeqcod_Visible), 5, 0)));
         edtavTfrequisito_tiporeqcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_tiporeqcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_tiporeqcod_to_Visible), 5, 0)));
         edtavTfrequisito_agrupador_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_agrupador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_agrupador_Visible), 5, 0)));
         edtavTfrequisito_agrupador_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_agrupador_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_agrupador_sel_Visible), 5, 0)));
         edtavTfrequisito_titulo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_titulo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_titulo_Visible), 5, 0)));
         edtavTfrequisito_titulo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_titulo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_titulo_sel_Visible), 5, 0)));
         edtavTfrequisito_restricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_restricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_restricao_Visible), 5, 0)));
         edtavTfrequisito_restricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_restricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_restricao_sel_Visible), 5, 0)));
         edtavTfrequisito_ordem_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_ordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_ordem_Visible), 5, 0)));
         edtavTfrequisito_ordem_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_ordem_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_ordem_to_Visible), 5, 0)));
         edtavTfrequisito_pontuacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_pontuacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_pontuacao_Visible), 5, 0)));
         edtavTfrequisito_pontuacao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_pontuacao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_pontuacao_to_Visible), 5, 0)));
         edtavTfrequisito_datahomologacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_datahomologacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_datahomologacao_Visible), 5, 0)));
         edtavTfrequisito_datahomologacao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_datahomologacao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_datahomologacao_to_Visible), 5, 0)));
         edtavTfrequisito_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfrequisito_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfrequisito_ativo_sel_Visible), 5, 0)));
         Ddo_requisito_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "TitleControlIdToReplace", Ddo_requisito_codigo_Titlecontrolidtoreplace);
         AV41ddo_Requisito_CodigoTitleControlIdToReplace = Ddo_requisito_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Requisito_CodigoTitleControlIdToReplace", AV41ddo_Requisito_CodigoTitleControlIdToReplace);
         edtavDdo_requisito_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "TitleControlIdToReplace", Ddo_requisito_descricao_Titlecontrolidtoreplace);
         AV45ddo_Requisito_DescricaoTitleControlIdToReplace = Ddo_requisito_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Requisito_DescricaoTitleControlIdToReplace", AV45ddo_Requisito_DescricaoTitleControlIdToReplace);
         edtavDdo_requisito_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_proposta_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Proposta_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "TitleControlIdToReplace", Ddo_proposta_codigo_Titlecontrolidtoreplace);
         AV49ddo_Proposta_CodigoTitleControlIdToReplace = Ddo_proposta_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Proposta_CodigoTitleControlIdToReplace", AV49ddo_Proposta_CodigoTitleControlIdToReplace);
         edtavDdo_proposta_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_proposta_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_proposta_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_proposta_objetivo_Titlecontrolidtoreplace = subGrid_Internalname+"_Proposta_Objetivo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "TitleControlIdToReplace", Ddo_proposta_objetivo_Titlecontrolidtoreplace);
         AV53ddo_Proposta_ObjetivoTitleControlIdToReplace = Ddo_proposta_objetivo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Proposta_ObjetivoTitleControlIdToReplace", AV53ddo_Proposta_ObjetivoTitleControlIdToReplace);
         edtavDdo_proposta_objetivotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_proposta_objetivotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_proposta_objetivotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_tiporeqcod_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_TipoReqCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "TitleControlIdToReplace", Ddo_requisito_tiporeqcod_Titlecontrolidtoreplace);
         AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace = Ddo_requisito_tiporeqcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace", AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace);
         edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_agrupador_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Agrupador";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "TitleControlIdToReplace", Ddo_requisito_agrupador_Titlecontrolidtoreplace);
         AV65ddo_Requisito_AgrupadorTitleControlIdToReplace = Ddo_requisito_agrupador_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Requisito_AgrupadorTitleControlIdToReplace", AV65ddo_Requisito_AgrupadorTitleControlIdToReplace);
         edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_titulo_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Titulo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "TitleControlIdToReplace", Ddo_requisito_titulo_Titlecontrolidtoreplace);
         AV69ddo_Requisito_TituloTitleControlIdToReplace = Ddo_requisito_titulo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Requisito_TituloTitleControlIdToReplace", AV69ddo_Requisito_TituloTitleControlIdToReplace);
         edtavDdo_requisito_titulotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_titulotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_titulotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_restricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Restricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "TitleControlIdToReplace", Ddo_requisito_restricao_Titlecontrolidtoreplace);
         AV77ddo_Requisito_RestricaoTitleControlIdToReplace = Ddo_requisito_restricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Requisito_RestricaoTitleControlIdToReplace", AV77ddo_Requisito_RestricaoTitleControlIdToReplace);
         edtavDdo_requisito_restricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_restricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_restricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_ordem_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Ordem";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "TitleControlIdToReplace", Ddo_requisito_ordem_Titlecontrolidtoreplace);
         AV85ddo_Requisito_OrdemTitleControlIdToReplace = Ddo_requisito_ordem_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Requisito_OrdemTitleControlIdToReplace", AV85ddo_Requisito_OrdemTitleControlIdToReplace);
         edtavDdo_requisito_ordemtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_ordemtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_ordemtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_pontuacao_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Pontuacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "TitleControlIdToReplace", Ddo_requisito_pontuacao_Titlecontrolidtoreplace);
         AV89ddo_Requisito_PontuacaoTitleControlIdToReplace = Ddo_requisito_pontuacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Requisito_PontuacaoTitleControlIdToReplace", AV89ddo_Requisito_PontuacaoTitleControlIdToReplace);
         edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_datahomologacao_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_DataHomologacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "TitleControlIdToReplace", Ddo_requisito_datahomologacao_Titlecontrolidtoreplace);
         AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace = Ddo_requisito_datahomologacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace", AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace);
         edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_status_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_status_Internalname, "TitleControlIdToReplace", Ddo_requisito_status_Titlecontrolidtoreplace);
         AV99ddo_Requisito_StatusTitleControlIdToReplace = Ddo_requisito_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_Requisito_StatusTitleControlIdToReplace", AV99ddo_Requisito_StatusTitleControlIdToReplace);
         edtavDdo_requisito_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_statustitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_requisito_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Requisito_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ativo_Internalname, "TitleControlIdToReplace", Ddo_requisito_ativo_Titlecontrolidtoreplace);
         AV102ddo_Requisito_AtivoTitleControlIdToReplace = Ddo_requisito_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Requisito_AtivoTitleControlIdToReplace", AV102ddo_Requisito_AtivoTitleControlIdToReplace);
         edtavDdo_requisito_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_requisito_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_requisito_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Requisito";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "do Requisito", 0);
         cmbavOrderedby.addItem("2", "do Requisito", 0);
         cmbavOrderedby.addItem("3", "Proposta_Codigo", 0);
         cmbavOrderedby.addItem("4", "Proposta_Objetivo", 0);
         cmbavOrderedby.addItem("5", "Tipo", 0);
         cmbavOrderedby.addItem("6", "Agrupador", 0);
         cmbavOrderedby.addItem("7", "T�tulo", 0);
         cmbavOrderedby.addItem("8", "Restri��o", 0);
         cmbavOrderedby.addItem("9", "Ordem", 0);
         cmbavOrderedby.addItem("10", "Prevista", 0);
         cmbavOrderedby.addItem("11", "Homologa��o", 0);
         cmbavOrderedby.addItem("12", "Status", 0);
         cmbavOrderedby.addItem("13", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV103DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV103DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E37PZ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV38Requisito_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Requisito_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Proposta_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Proposta_ObjetivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107Requisito_TipoReqCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Requisito_AgrupadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Requisito_TituloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Requisito_RestricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Requisito_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86Requisito_PontuacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90Requisito_DataHomologacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV96Requisito_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV100Requisito_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV32ManageFiltersExecutionStep == 1 )
         {
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV32ManageFiltersExecutionStep == 2 )
         {
            AV32ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtRequisito_Codigo_Titleformat = 2;
         edtRequisito_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Requisito", AV41ddo_Requisito_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Codigo_Internalname, "Title", edtRequisito_Codigo_Title);
         edtRequisito_Descricao_Titleformat = 2;
         edtRequisito_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Requisito", AV45ddo_Requisito_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Descricao_Internalname, "Title", edtRequisito_Descricao_Title);
         edtProposta_Codigo_Titleformat = 2;
         edtProposta_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Proposta_Codigo", AV49ddo_Proposta_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Codigo_Internalname, "Title", edtProposta_Codigo_Title);
         edtProposta_Objetivo_Titleformat = 2;
         edtProposta_Objetivo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Proposta_Objetivo", AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Objetivo_Internalname, "Title", edtProposta_Objetivo_Title);
         edtRequisito_TipoReqCod_Titleformat = 2;
         edtRequisito_TipoReqCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_TipoReqCod_Internalname, "Title", edtRequisito_TipoReqCod_Title);
         edtRequisito_Agrupador_Titleformat = 2;
         edtRequisito_Agrupador_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Agrupador", AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Agrupador_Internalname, "Title", edtRequisito_Agrupador_Title);
         edtRequisito_Titulo_Titleformat = 2;
         edtRequisito_Titulo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "T�tulo", AV69ddo_Requisito_TituloTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Titulo_Internalname, "Title", edtRequisito_Titulo_Title);
         edtRequisito_Restricao_Titleformat = 2;
         edtRequisito_Restricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Restri��o", AV77ddo_Requisito_RestricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Restricao_Internalname, "Title", edtRequisito_Restricao_Title);
         edtRequisito_Ordem_Titleformat = 2;
         edtRequisito_Ordem_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ordem", AV85ddo_Requisito_OrdemTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Ordem_Internalname, "Title", edtRequisito_Ordem_Title);
         edtRequisito_Pontuacao_Titleformat = 2;
         edtRequisito_Pontuacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prevista", AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Pontuacao_Internalname, "Title", edtRequisito_Pontuacao_Title);
         edtRequisito_DataHomologacao_Titleformat = 2;
         edtRequisito_DataHomologacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Homologa��o", AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_DataHomologacao_Internalname, "Title", edtRequisito_DataHomologacao_Title);
         cmbRequisito_Status_Titleformat = 2;
         cmbRequisito_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV99ddo_Requisito_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRequisito_Status_Internalname, "Title", cmbRequisito_Status.Title.Text);
         chkRequisito_Ativo_Titleformat = 2;
         chkRequisito_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV102ddo_Requisito_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkRequisito_Ativo_Internalname, "Title", chkRequisito_Ativo.Title.Text);
         AV105GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105GridCurrentPage), 10, 0)));
         AV106GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106GridPageCount), 10, 0)));
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV114WWRequisitoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV115WWRequisitoDS_3_Requisito_descricao1 = AV17Requisito_Descricao1;
         AV116WWRequisitoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV118WWRequisitoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV119WWRequisitoDS_7_Requisito_descricao2 = AV21Requisito_Descricao2;
         AV120WWRequisitoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV122WWRequisitoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV123WWRequisitoDS_11_Requisito_descricao3 = AV25Requisito_Descricao3;
         AV124WWRequisitoDS_12_Tfrequisito_codigo = AV39TFRequisito_Codigo;
         AV125WWRequisitoDS_13_Tfrequisito_codigo_to = AV40TFRequisito_Codigo_To;
         AV126WWRequisitoDS_14_Tfrequisito_descricao = AV43TFRequisito_Descricao;
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = AV44TFRequisito_Descricao_Sel;
         AV128WWRequisitoDS_16_Tfproposta_codigo = AV47TFProposta_Codigo;
         AV129WWRequisitoDS_17_Tfproposta_codigo_to = AV48TFProposta_Codigo_To;
         AV130WWRequisitoDS_18_Tfproposta_objetivo = AV51TFProposta_Objetivo;
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = AV52TFProposta_Objetivo_Sel;
         AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV108TFRequisito_TipoReqCod;
         AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV109TFRequisito_TipoReqCod_To;
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = AV63TFRequisito_Agrupador;
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV64TFRequisito_Agrupador_Sel;
         AV136WWRequisitoDS_24_Tfrequisito_titulo = AV67TFRequisito_Titulo;
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = AV68TFRequisito_Titulo_Sel;
         AV138WWRequisitoDS_26_Tfrequisito_restricao = AV75TFRequisito_Restricao;
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = AV76TFRequisito_Restricao_Sel;
         AV140WWRequisitoDS_28_Tfrequisito_ordem = AV83TFRequisito_Ordem;
         AV141WWRequisitoDS_29_Tfrequisito_ordem_to = AV84TFRequisito_Ordem_To;
         AV142WWRequisitoDS_30_Tfrequisito_pontuacao = AV87TFRequisito_Pontuacao;
         AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV88TFRequisito_Pontuacao_To;
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = AV91TFRequisito_DataHomologacao;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV92TFRequisito_DataHomologacao_To;
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = AV98TFRequisito_Status_Sels;
         AV147WWRequisitoDS_35_Tfrequisito_ativo_sel = AV101TFRequisito_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38Requisito_CodigoTitleFilterData", AV38Requisito_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Requisito_DescricaoTitleFilterData", AV42Requisito_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Proposta_CodigoTitleFilterData", AV46Proposta_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Proposta_ObjetivoTitleFilterData", AV50Proposta_ObjetivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107Requisito_TipoReqCodTitleFilterData", AV107Requisito_TipoReqCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62Requisito_AgrupadorTitleFilterData", AV62Requisito_AgrupadorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66Requisito_TituloTitleFilterData", AV66Requisito_TituloTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74Requisito_RestricaoTitleFilterData", AV74Requisito_RestricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82Requisito_OrdemTitleFilterData", AV82Requisito_OrdemTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV86Requisito_PontuacaoTitleFilterData", AV86Requisito_PontuacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV90Requisito_DataHomologacaoTitleFilterData", AV90Requisito_DataHomologacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96Requisito_StatusTitleFilterData", AV96Requisito_StatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV100Requisito_AtivoTitleFilterData", AV100Requisito_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36ManageFiltersData", AV36ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12PZ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV104PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV104PageToGo) ;
         }
      }

      protected void E13PZ2( )
      {
         /* Ddo_requisito_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "SortedStatus", Ddo_requisito_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "SortedStatus", Ddo_requisito_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFRequisito_Codigo = (int)(NumberUtil.Val( Ddo_requisito_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0)));
            AV40TFRequisito_Codigo_To = (int)(NumberUtil.Val( Ddo_requisito_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFRequisito_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14PZ2( )
      {
         /* Ddo_requisito_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "SortedStatus", Ddo_requisito_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "SortedStatus", Ddo_requisito_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFRequisito_Descricao = Ddo_requisito_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFRequisito_Descricao", AV43TFRequisito_Descricao);
            AV44TFRequisito_Descricao_Sel = Ddo_requisito_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFRequisito_Descricao_Sel", AV44TFRequisito_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15PZ2( )
      {
         /* Ddo_proposta_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_proposta_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_proposta_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "SortedStatus", Ddo_proposta_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_proposta_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_proposta_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "SortedStatus", Ddo_proposta_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_proposta_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFProposta_Codigo = (int)(NumberUtil.Val( Ddo_proposta_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0)));
            AV48TFProposta_Codigo_To = (int)(NumberUtil.Val( Ddo_proposta_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProposta_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16PZ2( )
      {
         /* Ddo_proposta_objetivo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_proposta_objetivo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_proposta_objetivo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "SortedStatus", Ddo_proposta_objetivo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_proposta_objetivo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_proposta_objetivo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "SortedStatus", Ddo_proposta_objetivo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_proposta_objetivo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFProposta_Objetivo = Ddo_proposta_objetivo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProposta_Objetivo", AV51TFProposta_Objetivo);
            AV52TFProposta_Objetivo_Sel = Ddo_proposta_objetivo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProposta_Objetivo_Sel", AV52TFProposta_Objetivo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17PZ2( )
      {
         /* Ddo_requisito_tiporeqcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_tiporeqcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_tiporeqcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "SortedStatus", Ddo_requisito_tiporeqcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_tiporeqcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_tiporeqcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "SortedStatus", Ddo_requisito_tiporeqcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_tiporeqcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV108TFRequisito_TipoReqCod = (int)(NumberUtil.Val( Ddo_requisito_tiporeqcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFRequisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0)));
            AV109TFRequisito_TipoReqCod_To = (int)(NumberUtil.Val( Ddo_requisito_tiporeqcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFRequisito_TipoReqCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18PZ2( )
      {
         /* Ddo_requisito_agrupador_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_agrupador_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_agrupador_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "SortedStatus", Ddo_requisito_agrupador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_agrupador_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_agrupador_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "SortedStatus", Ddo_requisito_agrupador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_agrupador_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFRequisito_Agrupador = Ddo_requisito_agrupador_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFRequisito_Agrupador", AV63TFRequisito_Agrupador);
            AV64TFRequisito_Agrupador_Sel = Ddo_requisito_agrupador_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFRequisito_Agrupador_Sel", AV64TFRequisito_Agrupador_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19PZ2( )
      {
         /* Ddo_requisito_titulo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_titulo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_titulo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "SortedStatus", Ddo_requisito_titulo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_titulo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_titulo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "SortedStatus", Ddo_requisito_titulo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_titulo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFRequisito_Titulo = Ddo_requisito_titulo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFRequisito_Titulo", AV67TFRequisito_Titulo);
            AV68TFRequisito_Titulo_Sel = Ddo_requisito_titulo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFRequisito_Titulo_Sel", AV68TFRequisito_Titulo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20PZ2( )
      {
         /* Ddo_requisito_restricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_restricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_restricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "SortedStatus", Ddo_requisito_restricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_restricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_restricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "SortedStatus", Ddo_requisito_restricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_restricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFRequisito_Restricao = Ddo_requisito_restricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFRequisito_Restricao", AV75TFRequisito_Restricao);
            AV76TFRequisito_Restricao_Sel = Ddo_requisito_restricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFRequisito_Restricao_Sel", AV76TFRequisito_Restricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E21PZ2( )
      {
         /* Ddo_requisito_ordem_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_ordem_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_ordem_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "SortedStatus", Ddo_requisito_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_ordem_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_ordem_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "SortedStatus", Ddo_requisito_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_ordem_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFRequisito_Ordem = (short)(NumberUtil.Val( Ddo_requisito_ordem_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFRequisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0)));
            AV84TFRequisito_Ordem_To = (short)(NumberUtil.Val( Ddo_requisito_ordem_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFRequisito_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E22PZ2( )
      {
         /* Ddo_requisito_pontuacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_pontuacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_pontuacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "SortedStatus", Ddo_requisito_pontuacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_pontuacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_pontuacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "SortedStatus", Ddo_requisito_pontuacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_pontuacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV87TFRequisito_Pontuacao = NumberUtil.Val( Ddo_requisito_pontuacao_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFRequisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5)));
            AV88TFRequisito_Pontuacao_To = NumberUtil.Val( Ddo_requisito_pontuacao_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFRequisito_Pontuacao_To", StringUtil.LTrim( StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E23PZ2( )
      {
         /* Ddo_requisito_datahomologacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_datahomologacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_datahomologacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "SortedStatus", Ddo_requisito_datahomologacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_datahomologacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_datahomologacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "SortedStatus", Ddo_requisito_datahomologacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_datahomologacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV91TFRequisito_DataHomologacao = context.localUtil.CToD( Ddo_requisito_datahomologacao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFRequisito_DataHomologacao", context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"));
            AV92TFRequisito_DataHomologacao_To = context.localUtil.CToD( Ddo_requisito_datahomologacao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFRequisito_DataHomologacao_To", context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E24PZ2( )
      {
         /* Ddo_requisito_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_status_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_status_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_status_Internalname, "SortedStatus", Ddo_requisito_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_status_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_status_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_status_Internalname, "SortedStatus", Ddo_requisito_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV97TFRequisito_Status_SelsJson = Ddo_requisito_status_Selectedvalue_get;
            AV98TFRequisito_Status_Sels.FromJSonString(StringUtil.StringReplace( AV97TFRequisito_Status_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV98TFRequisito_Status_Sels", AV98TFRequisito_Status_Sels);
      }

      protected void E25PZ2( )
      {
         /* Ddo_requisito_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_requisito_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ativo_Internalname, "SortedStatus", Ddo_requisito_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_requisito_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ativo_Internalname, "SortedStatus", Ddo_requisito_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_requisito_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV101TFRequisito_Ativo_Sel = (short)(NumberUtil.Val( Ddo_requisito_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFRequisito_Ativo_Sel", StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E38PZ2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1919Requisito_Codigo);
            AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV148Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV28Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV148Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1919Requisito_Codigo);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV149Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV149Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewrequisito.aspx") + "?" + UrlEncode("" +A1919Requisito_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV30Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV30Display);
            AV150Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV30Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV30Display);
            AV150Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtRequisito_Descricao_Link = formatLink("viewrequisito.aspx") + "?" + UrlEncode("" +A1919Requisito_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E26PZ2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E31PZ2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E27PZ2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E32PZ2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E33PZ2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E28PZ2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E34PZ2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29PZ2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Requisito_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Requisito_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Requisito_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV39TFRequisito_Codigo, AV40TFRequisito_Codigo_To, AV43TFRequisito_Descricao, AV44TFRequisito_Descricao_Sel, AV47TFProposta_Codigo, AV48TFProposta_Codigo_To, AV51TFProposta_Objetivo, AV52TFProposta_Objetivo_Sel, AV108TFRequisito_TipoReqCod, AV109TFRequisito_TipoReqCod_To, AV63TFRequisito_Agrupador, AV64TFRequisito_Agrupador_Sel, AV67TFRequisito_Titulo, AV68TFRequisito_Titulo_Sel, AV75TFRequisito_Restricao, AV76TFRequisito_Restricao_Sel, AV83TFRequisito_Ordem, AV84TFRequisito_Ordem_To, AV87TFRequisito_Pontuacao, AV88TFRequisito_Pontuacao_To, AV91TFRequisito_DataHomologacao, AV92TFRequisito_DataHomologacao_To, AV101TFRequisito_Ativo_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Requisito_CodigoTitleControlIdToReplace, AV45ddo_Requisito_DescricaoTitleControlIdToReplace, AV49ddo_Proposta_CodigoTitleControlIdToReplace, AV53ddo_Proposta_ObjetivoTitleControlIdToReplace, AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace, AV65ddo_Requisito_AgrupadorTitleControlIdToReplace, AV69ddo_Requisito_TituloTitleControlIdToReplace, AV77ddo_Requisito_RestricaoTitleControlIdToReplace, AV85ddo_Requisito_OrdemTitleControlIdToReplace, AV89ddo_Requisito_PontuacaoTitleControlIdToReplace, AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace, AV99ddo_Requisito_StatusTitleControlIdToReplace, AV102ddo_Requisito_AtivoTitleControlIdToReplace, AV98TFRequisito_Status_Sels, AV6WWPContext, AV151Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1919Requisito_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E35PZ2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11PZ2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWRequisitoFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWRequisitoFilters")), new Object[] {});
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV33ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWRequisitoFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV33ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV151Pgmname+"GridState",  AV33ManageFiltersXml) ;
               AV10GridState.FromXml(AV33ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV98TFRequisito_Status_Sels", AV98TFRequisito_Status_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E30PZ2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_requisito_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "SortedStatus", Ddo_requisito_codigo_Sortedstatus);
         Ddo_requisito_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "SortedStatus", Ddo_requisito_descricao_Sortedstatus);
         Ddo_proposta_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "SortedStatus", Ddo_proposta_codigo_Sortedstatus);
         Ddo_proposta_objetivo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "SortedStatus", Ddo_proposta_objetivo_Sortedstatus);
         Ddo_requisito_tiporeqcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "SortedStatus", Ddo_requisito_tiporeqcod_Sortedstatus);
         Ddo_requisito_agrupador_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "SortedStatus", Ddo_requisito_agrupador_Sortedstatus);
         Ddo_requisito_titulo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "SortedStatus", Ddo_requisito_titulo_Sortedstatus);
         Ddo_requisito_restricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "SortedStatus", Ddo_requisito_restricao_Sortedstatus);
         Ddo_requisito_ordem_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "SortedStatus", Ddo_requisito_ordem_Sortedstatus);
         Ddo_requisito_pontuacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "SortedStatus", Ddo_requisito_pontuacao_Sortedstatus);
         Ddo_requisito_datahomologacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "SortedStatus", Ddo_requisito_datahomologacao_Sortedstatus);
         Ddo_requisito_status_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_status_Internalname, "SortedStatus", Ddo_requisito_status_Sortedstatus);
         Ddo_requisito_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ativo_Internalname, "SortedStatus", Ddo_requisito_ativo_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_requisito_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "SortedStatus", Ddo_requisito_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_requisito_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "SortedStatus", Ddo_requisito_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_proposta_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "SortedStatus", Ddo_proposta_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_proposta_objetivo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "SortedStatus", Ddo_proposta_objetivo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_requisito_tiporeqcod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "SortedStatus", Ddo_requisito_tiporeqcod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_requisito_agrupador_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "SortedStatus", Ddo_requisito_agrupador_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_requisito_titulo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "SortedStatus", Ddo_requisito_titulo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_requisito_restricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "SortedStatus", Ddo_requisito_restricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_requisito_ordem_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "SortedStatus", Ddo_requisito_ordem_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_requisito_pontuacao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "SortedStatus", Ddo_requisito_pontuacao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 11 )
         {
            Ddo_requisito_datahomologacao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "SortedStatus", Ddo_requisito_datahomologacao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 12 )
         {
            Ddo_requisito_status_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_status_Internalname, "SortedStatus", Ddo_requisito_status_Sortedstatus);
         }
         else if ( AV13OrderedBy == 13 )
         {
            Ddo_requisito_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ativo_Internalname, "SortedStatus", Ddo_requisito_ativo_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavRequisito_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REQUISITO_DESCRICAO") == 0 )
         {
            edtavRequisito_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavRequisito_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REQUISITO_DESCRICAO") == 0 )
         {
            edtavRequisito_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavRequisito_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REQUISITO_DESCRICAO") == 0 )
         {
            edtavRequisito_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "REQUISITO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21Requisito_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Requisito_Descricao2", AV21Requisito_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "REQUISITO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25Requisito_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Requisito_Descricao3", AV25Requisito_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV36ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV34ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWRequisitoFilters"), "");
         AV152GXV1 = 1;
         while ( AV152GXV1 <= AV34ManageFiltersItems.Count )
         {
            AV35ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV34ManageFiltersItems.Item(AV152GXV1));
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Title = AV35ManageFiltersItem.gxTpr_Title;
            AV37ManageFiltersDataItem.gxTpr_Eventkey = AV35ManageFiltersItem.gxTpr_Title;
            AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
            if ( AV36ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV152GXV1 = (int)(AV152GXV1+1);
         }
         if ( AV36ManageFiltersData.Count > 3 )
         {
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV39TFRequisito_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0)));
         Ddo_requisito_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "FilteredText_set", Ddo_requisito_codigo_Filteredtext_set);
         AV40TFRequisito_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFRequisito_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0)));
         Ddo_requisito_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "FilteredTextTo_set", Ddo_requisito_codigo_Filteredtextto_set);
         AV43TFRequisito_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFRequisito_Descricao", AV43TFRequisito_Descricao);
         Ddo_requisito_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "FilteredText_set", Ddo_requisito_descricao_Filteredtext_set);
         AV44TFRequisito_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFRequisito_Descricao_Sel", AV44TFRequisito_Descricao_Sel);
         Ddo_requisito_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "SelectedValue_set", Ddo_requisito_descricao_Selectedvalue_set);
         AV47TFProposta_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0)));
         Ddo_proposta_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "FilteredText_set", Ddo_proposta_codigo_Filteredtext_set);
         AV48TFProposta_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProposta_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0)));
         Ddo_proposta_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "FilteredTextTo_set", Ddo_proposta_codigo_Filteredtextto_set);
         AV51TFProposta_Objetivo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProposta_Objetivo", AV51TFProposta_Objetivo);
         Ddo_proposta_objetivo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "FilteredText_set", Ddo_proposta_objetivo_Filteredtext_set);
         AV52TFProposta_Objetivo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProposta_Objetivo_Sel", AV52TFProposta_Objetivo_Sel);
         Ddo_proposta_objetivo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "SelectedValue_set", Ddo_proposta_objetivo_Selectedvalue_set);
         AV108TFRequisito_TipoReqCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFRequisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0)));
         Ddo_requisito_tiporeqcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "FilteredText_set", Ddo_requisito_tiporeqcod_Filteredtext_set);
         AV109TFRequisito_TipoReqCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFRequisito_TipoReqCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0)));
         Ddo_requisito_tiporeqcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "FilteredTextTo_set", Ddo_requisito_tiporeqcod_Filteredtextto_set);
         AV63TFRequisito_Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFRequisito_Agrupador", AV63TFRequisito_Agrupador);
         Ddo_requisito_agrupador_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "FilteredText_set", Ddo_requisito_agrupador_Filteredtext_set);
         AV64TFRequisito_Agrupador_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFRequisito_Agrupador_Sel", AV64TFRequisito_Agrupador_Sel);
         Ddo_requisito_agrupador_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "SelectedValue_set", Ddo_requisito_agrupador_Selectedvalue_set);
         AV67TFRequisito_Titulo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFRequisito_Titulo", AV67TFRequisito_Titulo);
         Ddo_requisito_titulo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "FilteredText_set", Ddo_requisito_titulo_Filteredtext_set);
         AV68TFRequisito_Titulo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFRequisito_Titulo_Sel", AV68TFRequisito_Titulo_Sel);
         Ddo_requisito_titulo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "SelectedValue_set", Ddo_requisito_titulo_Selectedvalue_set);
         AV75TFRequisito_Restricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFRequisito_Restricao", AV75TFRequisito_Restricao);
         Ddo_requisito_restricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "FilteredText_set", Ddo_requisito_restricao_Filteredtext_set);
         AV76TFRequisito_Restricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFRequisito_Restricao_Sel", AV76TFRequisito_Restricao_Sel);
         Ddo_requisito_restricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "SelectedValue_set", Ddo_requisito_restricao_Selectedvalue_set);
         AV83TFRequisito_Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFRequisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0)));
         Ddo_requisito_ordem_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "FilteredText_set", Ddo_requisito_ordem_Filteredtext_set);
         AV84TFRequisito_Ordem_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFRequisito_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0)));
         Ddo_requisito_ordem_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "FilteredTextTo_set", Ddo_requisito_ordem_Filteredtextto_set);
         AV87TFRequisito_Pontuacao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFRequisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5)));
         Ddo_requisito_pontuacao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "FilteredText_set", Ddo_requisito_pontuacao_Filteredtext_set);
         AV88TFRequisito_Pontuacao_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFRequisito_Pontuacao_To", StringUtil.LTrim( StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5)));
         Ddo_requisito_pontuacao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "FilteredTextTo_set", Ddo_requisito_pontuacao_Filteredtextto_set);
         AV91TFRequisito_DataHomologacao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFRequisito_DataHomologacao", context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"));
         Ddo_requisito_datahomologacao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "FilteredText_set", Ddo_requisito_datahomologacao_Filteredtext_set);
         AV92TFRequisito_DataHomologacao_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFRequisito_DataHomologacao_To", context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"));
         Ddo_requisito_datahomologacao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "FilteredTextTo_set", Ddo_requisito_datahomologacao_Filteredtextto_set);
         AV98TFRequisito_Status_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_requisito_status_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_status_Internalname, "SelectedValue_set", Ddo_requisito_status_Selectedvalue_set);
         AV101TFRequisito_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFRequisito_Ativo_Sel", StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0));
         Ddo_requisito_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ativo_Internalname, "SelectedValue_set", Ddo_requisito_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "REQUISITO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Requisito_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Requisito_Descricao1", AV17Requisito_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV151Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV151Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV31Session.Get(AV151Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV153GXV2 = 1;
         while ( AV153GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV153GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_CODIGO") == 0 )
            {
               AV39TFRequisito_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0)));
               AV40TFRequisito_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFRequisito_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0)));
               if ( ! (0==AV39TFRequisito_Codigo) )
               {
                  Ddo_requisito_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "FilteredText_set", Ddo_requisito_codigo_Filteredtext_set);
               }
               if ( ! (0==AV40TFRequisito_Codigo_To) )
               {
                  Ddo_requisito_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_codigo_Internalname, "FilteredTextTo_set", Ddo_requisito_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_DESCRICAO") == 0 )
            {
               AV43TFRequisito_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFRequisito_Descricao", AV43TFRequisito_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFRequisito_Descricao)) )
               {
                  Ddo_requisito_descricao_Filteredtext_set = AV43TFRequisito_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "FilteredText_set", Ddo_requisito_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_DESCRICAO_SEL") == 0 )
            {
               AV44TFRequisito_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFRequisito_Descricao_Sel", AV44TFRequisito_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFRequisito_Descricao_Sel)) )
               {
                  Ddo_requisito_descricao_Selectedvalue_set = AV44TFRequisito_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_descricao_Internalname, "SelectedValue_set", Ddo_requisito_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROPOSTA_CODIGO") == 0 )
            {
               AV47TFProposta_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFProposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0)));
               AV48TFProposta_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFProposta_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0)));
               if ( ! (0==AV47TFProposta_Codigo) )
               {
                  Ddo_proposta_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "FilteredText_set", Ddo_proposta_codigo_Filteredtext_set);
               }
               if ( ! (0==AV48TFProposta_Codigo_To) )
               {
                  Ddo_proposta_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_codigo_Internalname, "FilteredTextTo_set", Ddo_proposta_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROPOSTA_OBJETIVO") == 0 )
            {
               AV51TFProposta_Objetivo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFProposta_Objetivo", AV51TFProposta_Objetivo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFProposta_Objetivo)) )
               {
                  Ddo_proposta_objetivo_Filteredtext_set = AV51TFProposta_Objetivo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "FilteredText_set", Ddo_proposta_objetivo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROPOSTA_OBJETIVO_SEL") == 0 )
            {
               AV52TFProposta_Objetivo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFProposta_Objetivo_Sel", AV52TFProposta_Objetivo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFProposta_Objetivo_Sel)) )
               {
                  Ddo_proposta_objetivo_Selectedvalue_set = AV52TFProposta_Objetivo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_proposta_objetivo_Internalname, "SelectedValue_set", Ddo_proposta_objetivo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_TIPOREQCOD") == 0 )
            {
               AV108TFRequisito_TipoReqCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFRequisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0)));
               AV109TFRequisito_TipoReqCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFRequisito_TipoReqCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0)));
               if ( ! (0==AV108TFRequisito_TipoReqCod) )
               {
                  Ddo_requisito_tiporeqcod_Filteredtext_set = StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "FilteredText_set", Ddo_requisito_tiporeqcod_Filteredtext_set);
               }
               if ( ! (0==AV109TFRequisito_TipoReqCod_To) )
               {
                  Ddo_requisito_tiporeqcod_Filteredtextto_set = StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_tiporeqcod_Internalname, "FilteredTextTo_set", Ddo_requisito_tiporeqcod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_AGRUPADOR") == 0 )
            {
               AV63TFRequisito_Agrupador = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFRequisito_Agrupador", AV63TFRequisito_Agrupador);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFRequisito_Agrupador)) )
               {
                  Ddo_requisito_agrupador_Filteredtext_set = AV63TFRequisito_Agrupador;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "FilteredText_set", Ddo_requisito_agrupador_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_AGRUPADOR_SEL") == 0 )
            {
               AV64TFRequisito_Agrupador_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFRequisito_Agrupador_Sel", AV64TFRequisito_Agrupador_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFRequisito_Agrupador_Sel)) )
               {
                  Ddo_requisito_agrupador_Selectedvalue_set = AV64TFRequisito_Agrupador_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_agrupador_Internalname, "SelectedValue_set", Ddo_requisito_agrupador_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_TITULO") == 0 )
            {
               AV67TFRequisito_Titulo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFRequisito_Titulo", AV67TFRequisito_Titulo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFRequisito_Titulo)) )
               {
                  Ddo_requisito_titulo_Filteredtext_set = AV67TFRequisito_Titulo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "FilteredText_set", Ddo_requisito_titulo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_TITULO_SEL") == 0 )
            {
               AV68TFRequisito_Titulo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFRequisito_Titulo_Sel", AV68TFRequisito_Titulo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFRequisito_Titulo_Sel)) )
               {
                  Ddo_requisito_titulo_Selectedvalue_set = AV68TFRequisito_Titulo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_titulo_Internalname, "SelectedValue_set", Ddo_requisito_titulo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_RESTRICAO") == 0 )
            {
               AV75TFRequisito_Restricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFRequisito_Restricao", AV75TFRequisito_Restricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFRequisito_Restricao)) )
               {
                  Ddo_requisito_restricao_Filteredtext_set = AV75TFRequisito_Restricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "FilteredText_set", Ddo_requisito_restricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_RESTRICAO_SEL") == 0 )
            {
               AV76TFRequisito_Restricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFRequisito_Restricao_Sel", AV76TFRequisito_Restricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFRequisito_Restricao_Sel)) )
               {
                  Ddo_requisito_restricao_Selectedvalue_set = AV76TFRequisito_Restricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_restricao_Internalname, "SelectedValue_set", Ddo_requisito_restricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_ORDEM") == 0 )
            {
               AV83TFRequisito_Ordem = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFRequisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0)));
               AV84TFRequisito_Ordem_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFRequisito_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0)));
               if ( ! (0==AV83TFRequisito_Ordem) )
               {
                  Ddo_requisito_ordem_Filteredtext_set = StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "FilteredText_set", Ddo_requisito_ordem_Filteredtext_set);
               }
               if ( ! (0==AV84TFRequisito_Ordem_To) )
               {
                  Ddo_requisito_ordem_Filteredtextto_set = StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ordem_Internalname, "FilteredTextTo_set", Ddo_requisito_ordem_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_PONTUACAO") == 0 )
            {
               AV87TFRequisito_Pontuacao = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFRequisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5)));
               AV88TFRequisito_Pontuacao_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFRequisito_Pontuacao_To", StringUtil.LTrim( StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV87TFRequisito_Pontuacao) )
               {
                  Ddo_requisito_pontuacao_Filteredtext_set = StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "FilteredText_set", Ddo_requisito_pontuacao_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV88TFRequisito_Pontuacao_To) )
               {
                  Ddo_requisito_pontuacao_Filteredtextto_set = StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_pontuacao_Internalname, "FilteredTextTo_set", Ddo_requisito_pontuacao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_DATAHOMOLOGACAO") == 0 )
            {
               AV91TFRequisito_DataHomologacao = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFRequisito_DataHomologacao", context.localUtil.Format(AV91TFRequisito_DataHomologacao, "99/99/99"));
               AV92TFRequisito_DataHomologacao_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFRequisito_DataHomologacao_To", context.localUtil.Format(AV92TFRequisito_DataHomologacao_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV91TFRequisito_DataHomologacao) )
               {
                  Ddo_requisito_datahomologacao_Filteredtext_set = context.localUtil.DToC( AV91TFRequisito_DataHomologacao, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "FilteredText_set", Ddo_requisito_datahomologacao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV92TFRequisito_DataHomologacao_To) )
               {
                  Ddo_requisito_datahomologacao_Filteredtextto_set = context.localUtil.DToC( AV92TFRequisito_DataHomologacao_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_datahomologacao_Internalname, "FilteredTextTo_set", Ddo_requisito_datahomologacao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_STATUS_SEL") == 0 )
            {
               AV97TFRequisito_Status_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV98TFRequisito_Status_Sels.FromJSonString(AV97TFRequisito_Status_SelsJson);
               if ( ! ( AV98TFRequisito_Status_Sels.Count == 0 ) )
               {
                  Ddo_requisito_status_Selectedvalue_set = AV97TFRequisito_Status_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_status_Internalname, "SelectedValue_set", Ddo_requisito_status_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREQUISITO_ATIVO_SEL") == 0 )
            {
               AV101TFRequisito_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFRequisito_Ativo_Sel", StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0));
               if ( ! (0==AV101TFRequisito_Ativo_Sel) )
               {
                  Ddo_requisito_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_requisito_ativo_Internalname, "SelectedValue_set", Ddo_requisito_ativo_Selectedvalue_set);
               }
            }
            AV153GXV2 = (int)(AV153GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REQUISITO_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Requisito_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Requisito_Descricao1", AV17Requisito_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REQUISITO_DESCRICAO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21Requisito_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Requisito_Descricao2", AV21Requisito_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REQUISITO_DESCRICAO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25Requisito_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Requisito_Descricao3", AV25Requisito_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV31Session.Get(AV151Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV39TFRequisito_Codigo) && (0==AV40TFRequisito_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV39TFRequisito_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV40TFRequisito_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFRequisito_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFRequisito_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFRequisito_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFRequisito_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV47TFProposta_Codigo) && (0==AV48TFProposta_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROPOSTA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV47TFProposta_Codigo), 9, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV48TFProposta_Codigo_To), 9, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFProposta_Objetivo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROPOSTA_OBJETIVO";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFProposta_Objetivo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFProposta_Objetivo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROPOSTA_OBJETIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFProposta_Objetivo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV108TFRequisito_TipoReqCod) && (0==AV109TFRequisito_TipoReqCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_TIPOREQCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV108TFRequisito_TipoReqCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV109TFRequisito_TipoReqCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFRequisito_Agrupador)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_AGRUPADOR";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFRequisito_Agrupador;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFRequisito_Agrupador_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_AGRUPADOR_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFRequisito_Agrupador_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFRequisito_Titulo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_TITULO";
            AV11GridStateFilterValue.gxTpr_Value = AV67TFRequisito_Titulo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFRequisito_Titulo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_TITULO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV68TFRequisito_Titulo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFRequisito_Restricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_RESTRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFRequisito_Restricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFRequisito_Restricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_RESTRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV76TFRequisito_Restricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV83TFRequisito_Ordem) && (0==AV84TFRequisito_Ordem_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_ORDEM";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV83TFRequisito_Ordem), 3, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV84TFRequisito_Ordem_To), 3, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV87TFRequisito_Pontuacao) && (Convert.ToDecimal(0)==AV88TFRequisito_Pontuacao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_PONTUACAO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV87TFRequisito_Pontuacao, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV88TFRequisito_Pontuacao_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV91TFRequisito_DataHomologacao) && (DateTime.MinValue==AV92TFRequisito_DataHomologacao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_DATAHOMOLOGACAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV91TFRequisito_DataHomologacao, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV92TFRequisito_DataHomologacao_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV98TFRequisito_Status_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_STATUS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV98TFRequisito_Status_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV101TFRequisito_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREQUISITO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV101TFRequisito_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV151Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REQUISITO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Requisito_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Requisito_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REQUISITO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Requisito_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Requisito_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REQUISITO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Requisito_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Requisito_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV151Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Requisito";
         AV31Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_PZ2( true) ;
         }
         else
         {
            wb_table2_8_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_PZ2( true) ;
         }
         else
         {
            wb_table3_82_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_PZ2e( true) ;
         }
         else
         {
            wb_table1_2_PZ2e( false) ;
         }
      }

      protected void wb_table3_82_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_PZ2( true) ;
         }
         else
         {
            wb_table4_85_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_PZ2e( true) ;
         }
         else
         {
            wb_table3_82_PZ2e( false) ;
         }
      }

      protected void wb_table4_85_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProposta_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtProposta_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProposta_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProposta_Objetivo_Titleformat == 0 )
               {
                  context.SendWebValue( edtProposta_Objetivo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProposta_Objetivo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_TipoReqCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_TipoReqCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_TipoReqCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_Agrupador_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_Agrupador_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_Agrupador_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_Titulo_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_Titulo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_Titulo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_Restricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_Restricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_Restricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_Ordem_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_Ordem_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_Ordem_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_Pontuacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_Pontuacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_Pontuacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRequisito_DataHomologacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtRequisito_DataHomologacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRequisito_DataHomologacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbRequisito_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbRequisito_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbRequisito_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkRequisito_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkRequisito_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkRequisito_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1923Requisito_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtRequisito_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1685Proposta_Codigo), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProposta_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProposta_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1690Proposta_Objetivo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProposta_Objetivo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProposta_Objetivo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2049Requisito_TipoReqCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_TipoReqCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_TipoReqCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1926Requisito_Agrupador);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_Agrupador_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Agrupador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1927Requisito_Titulo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_Titulo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Titulo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1929Requisito_Restricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_Restricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Restricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_Ordem_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Ordem_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_Pontuacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Pontuacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRequisito_DataHomologacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_DataHomologacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1934Requisito_Status), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbRequisito_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbRequisito_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1935Requisito_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkRequisito_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkRequisito_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_PZ2e( true) ;
         }
         else
         {
            wb_table4_85_PZ2e( false) ;
         }
      }

      protected void wb_table2_8_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_PZ2( true) ;
         }
         else
         {
            wb_table5_11_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_PZ2( true) ;
         }
         else
         {
            wb_table6_23_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_PZ2e( true) ;
         }
         else
         {
            wb_table2_8_PZ2e( false) ;
         }
      }

      protected void wb_table6_23_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_PZ2( true) ;
         }
         else
         {
            wb_table7_28_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_PZ2e( true) ;
         }
         else
         {
            wb_table6_23_PZ2e( false) ;
         }
      }

      protected void wb_table7_28_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWRequisito.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_PZ2( true) ;
         }
         else
         {
            wb_table8_37_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWRequisito.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_PZ2( true) ;
         }
         else
         {
            wb_table9_54_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWRequisito.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_PZ2( true) ;
         }
         else
         {
            wb_table10_71_PZ2( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_PZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_PZ2e( true) ;
         }
         else
         {
            wb_table7_28_PZ2e( false) ;
         }
      }

      protected void wb_table10_71_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWRequisito.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_descricao3_Internalname, AV25Requisito_Descricao3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 2, edtavRequisito_descricao3_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_PZ2e( true) ;
         }
         else
         {
            wb_table10_71_PZ2e( false) ;
         }
      }

      protected void wb_table9_54_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWRequisito.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_descricao2_Internalname, AV21Requisito_Descricao2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 2, edtavRequisito_descricao2_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_PZ2e( true) ;
         }
         else
         {
            wb_table9_54_PZ2e( false) ;
         }
      }

      protected void wb_table8_37_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWRequisito.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_descricao1_Internalname, AV17Requisito_Descricao1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 2, edtavRequisito_descricao1_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_PZ2e( true) ;
         }
         else
         {
            wb_table8_37_PZ2e( false) ;
         }
      }

      protected void wb_table5_11_PZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblRequisitotitle_Internalname, "Requisito", "", "", lblRequisitotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWRequisito.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_PZ2e( true) ;
         }
         else
         {
            wb_table5_11_PZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAPZ2( ) ;
         WSPZ2( ) ;
         WEPZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122124487");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwrequisito.js", "?20203122124487");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_idx;
         edtRequisito_Codigo_Internalname = "REQUISITO_CODIGO_"+sGXsfl_88_idx;
         edtRequisito_Descricao_Internalname = "REQUISITO_DESCRICAO_"+sGXsfl_88_idx;
         edtProposta_Codigo_Internalname = "PROPOSTA_CODIGO_"+sGXsfl_88_idx;
         edtProposta_Objetivo_Internalname = "PROPOSTA_OBJETIVO_"+sGXsfl_88_idx;
         edtRequisito_TipoReqCod_Internalname = "REQUISITO_TIPOREQCOD_"+sGXsfl_88_idx;
         edtRequisito_Agrupador_Internalname = "REQUISITO_AGRUPADOR_"+sGXsfl_88_idx;
         edtRequisito_Titulo_Internalname = "REQUISITO_TITULO_"+sGXsfl_88_idx;
         edtRequisito_Restricao_Internalname = "REQUISITO_RESTRICAO_"+sGXsfl_88_idx;
         edtRequisito_Ordem_Internalname = "REQUISITO_ORDEM_"+sGXsfl_88_idx;
         edtRequisito_Pontuacao_Internalname = "REQUISITO_PONTUACAO_"+sGXsfl_88_idx;
         edtRequisito_DataHomologacao_Internalname = "REQUISITO_DATAHOMOLOGACAO_"+sGXsfl_88_idx;
         cmbRequisito_Status_Internalname = "REQUISITO_STATUS_"+sGXsfl_88_idx;
         chkRequisito_Ativo_Internalname = "REQUISITO_ATIVO_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_fel_idx;
         edtRequisito_Codigo_Internalname = "REQUISITO_CODIGO_"+sGXsfl_88_fel_idx;
         edtRequisito_Descricao_Internalname = "REQUISITO_DESCRICAO_"+sGXsfl_88_fel_idx;
         edtProposta_Codigo_Internalname = "PROPOSTA_CODIGO_"+sGXsfl_88_fel_idx;
         edtProposta_Objetivo_Internalname = "PROPOSTA_OBJETIVO_"+sGXsfl_88_fel_idx;
         edtRequisito_TipoReqCod_Internalname = "REQUISITO_TIPOREQCOD_"+sGXsfl_88_fel_idx;
         edtRequisito_Agrupador_Internalname = "REQUISITO_AGRUPADOR_"+sGXsfl_88_fel_idx;
         edtRequisito_Titulo_Internalname = "REQUISITO_TITULO_"+sGXsfl_88_fel_idx;
         edtRequisito_Restricao_Internalname = "REQUISITO_RESTRICAO_"+sGXsfl_88_fel_idx;
         edtRequisito_Ordem_Internalname = "REQUISITO_ORDEM_"+sGXsfl_88_fel_idx;
         edtRequisito_Pontuacao_Internalname = "REQUISITO_PONTUACAO_"+sGXsfl_88_fel_idx;
         edtRequisito_DataHomologacao_Internalname = "REQUISITO_DATAHOMOLOGACAO_"+sGXsfl_88_fel_idx;
         cmbRequisito_Status_Internalname = "REQUISITO_STATUS_"+sGXsfl_88_fel_idx;
         chkRequisito_Ativo_Internalname = "REQUISITO_ATIVO_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBPZ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV148Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV148Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV149Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV149Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV150Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Display)) ? AV150Display_GXI : context.PathToRelativeUrl( AV30Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Descricao_Internalname,(String)A1923Requisito_Descricao,(String)A1923Requisito_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtRequisito_Descricao_Link,(String)"",(String)"",(String)"",(String)edtRequisito_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)2,(short)88,(short)1,(short)0,(short)-1,(bool)true,(String)"ReqDescricao",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProposta_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1685Proposta_Codigo), 9, 0, ",", "")),context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProposta_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProposta_Objetivo_Internalname,(String)A1690Proposta_Objetivo,(String)A1690Proposta_Objetivo,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProposta_Objetivo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2048,(short)0,(short)0,(short)88,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_TipoReqCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2049Requisito_TipoReqCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_TipoReqCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Agrupador_Internalname,(String)A1926Requisito_Agrupador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Agrupador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"ReqAgrupador",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Titulo_Internalname,(String)A1927Requisito_Titulo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Titulo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)250,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"ReqNome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Restricao_Internalname,(String)A1929Requisito_Restricao,(String)A1929Requisito_Restricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Restricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)88,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Ordem_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Ordem_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Ordem",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Pontuacao_Internalname,StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ",", "")),context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Pontuacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_DataHomologacao_Internalname,context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99"),context.localUtil.Format( A1933Requisito_DataHomologacao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_DataHomologacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_88_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "REQUISITO_STATUS_" + sGXsfl_88_idx;
               cmbRequisito_Status.Name = GXCCtl;
               cmbRequisito_Status.WebTags = "";
               cmbRequisito_Status.addItem("0", "Rascunho", 0);
               cmbRequisito_Status.addItem("1", "Solicitado", 0);
               cmbRequisito_Status.addItem("2", "Aprovado", 0);
               cmbRequisito_Status.addItem("3", "N�o Aprovado", 0);
               cmbRequisito_Status.addItem("4", "Pausado", 0);
               cmbRequisito_Status.addItem("5", "Cancelado", 0);
               if ( cmbRequisito_Status.ItemCount > 0 )
               {
                  A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbRequisito_Status,(String)cmbRequisito_Status_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)),(short)1,(String)cmbRequisito_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbRequisito_Status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRequisito_Status_Internalname, "Values", (String)(cmbRequisito_Status.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkRequisito_Ativo_Internalname,StringUtil.BoolToStr( A1935Requisito_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_DESCRICAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, A1923Requisito_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_TIPOREQCOD"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_AGRUPADOR"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A1926Requisito_Agrupador, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_TITULO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A1927Requisito_Titulo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_RESTRICAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, A1929Requisito_Restricao));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_ORDEM"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_PONTUACAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_DATAHOMOLOGACAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, A1933Requisito_DataHomologacao));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_STATUS"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1934Requisito_Status), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_ATIVO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, A1935Requisito_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblRequisitotitle_Internalname = "REQUISITOTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavRequisito_descricao1_Internalname = "vREQUISITO_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavRequisito_descricao2_Internalname = "vREQUISITO_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavRequisito_descricao3_Internalname = "vREQUISITO_DESCRICAO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtRequisito_Codigo_Internalname = "REQUISITO_CODIGO";
         edtRequisito_Descricao_Internalname = "REQUISITO_DESCRICAO";
         edtProposta_Codigo_Internalname = "PROPOSTA_CODIGO";
         edtProposta_Objetivo_Internalname = "PROPOSTA_OBJETIVO";
         edtRequisito_TipoReqCod_Internalname = "REQUISITO_TIPOREQCOD";
         edtRequisito_Agrupador_Internalname = "REQUISITO_AGRUPADOR";
         edtRequisito_Titulo_Internalname = "REQUISITO_TITULO";
         edtRequisito_Restricao_Internalname = "REQUISITO_RESTRICAO";
         edtRequisito_Ordem_Internalname = "REQUISITO_ORDEM";
         edtRequisito_Pontuacao_Internalname = "REQUISITO_PONTUACAO";
         edtRequisito_DataHomologacao_Internalname = "REQUISITO_DATAHOMOLOGACAO";
         cmbRequisito_Status_Internalname = "REQUISITO_STATUS";
         chkRequisito_Ativo_Internalname = "REQUISITO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfrequisito_codigo_Internalname = "vTFREQUISITO_CODIGO";
         edtavTfrequisito_codigo_to_Internalname = "vTFREQUISITO_CODIGO_TO";
         edtavTfrequisito_descricao_Internalname = "vTFREQUISITO_DESCRICAO";
         edtavTfrequisito_descricao_sel_Internalname = "vTFREQUISITO_DESCRICAO_SEL";
         edtavTfproposta_codigo_Internalname = "vTFPROPOSTA_CODIGO";
         edtavTfproposta_codigo_to_Internalname = "vTFPROPOSTA_CODIGO_TO";
         edtavTfproposta_objetivo_Internalname = "vTFPROPOSTA_OBJETIVO";
         edtavTfproposta_objetivo_sel_Internalname = "vTFPROPOSTA_OBJETIVO_SEL";
         edtavTfrequisito_tiporeqcod_Internalname = "vTFREQUISITO_TIPOREQCOD";
         edtavTfrequisito_tiporeqcod_to_Internalname = "vTFREQUISITO_TIPOREQCOD_TO";
         edtavTfrequisito_agrupador_Internalname = "vTFREQUISITO_AGRUPADOR";
         edtavTfrequisito_agrupador_sel_Internalname = "vTFREQUISITO_AGRUPADOR_SEL";
         edtavTfrequisito_titulo_Internalname = "vTFREQUISITO_TITULO";
         edtavTfrequisito_titulo_sel_Internalname = "vTFREQUISITO_TITULO_SEL";
         edtavTfrequisito_restricao_Internalname = "vTFREQUISITO_RESTRICAO";
         edtavTfrequisito_restricao_sel_Internalname = "vTFREQUISITO_RESTRICAO_SEL";
         edtavTfrequisito_ordem_Internalname = "vTFREQUISITO_ORDEM";
         edtavTfrequisito_ordem_to_Internalname = "vTFREQUISITO_ORDEM_TO";
         edtavTfrequisito_pontuacao_Internalname = "vTFREQUISITO_PONTUACAO";
         edtavTfrequisito_pontuacao_to_Internalname = "vTFREQUISITO_PONTUACAO_TO";
         edtavTfrequisito_datahomologacao_Internalname = "vTFREQUISITO_DATAHOMOLOGACAO";
         edtavTfrequisito_datahomologacao_to_Internalname = "vTFREQUISITO_DATAHOMOLOGACAO_TO";
         edtavDdo_requisito_datahomologacaoauxdate_Internalname = "vDDO_REQUISITO_DATAHOMOLOGACAOAUXDATE";
         edtavDdo_requisito_datahomologacaoauxdateto_Internalname = "vDDO_REQUISITO_DATAHOMOLOGACAOAUXDATETO";
         divDdo_requisito_datahomologacaoauxdates_Internalname = "DDO_REQUISITO_DATAHOMOLOGACAOAUXDATES";
         edtavTfrequisito_ativo_sel_Internalname = "vTFREQUISITO_ATIVO_SEL";
         Ddo_requisito_codigo_Internalname = "DDO_REQUISITO_CODIGO";
         edtavDdo_requisito_codigotitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_requisito_descricao_Internalname = "DDO_REQUISITO_DESCRICAO";
         edtavDdo_requisito_descricaotitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_proposta_codigo_Internalname = "DDO_PROPOSTA_CODIGO";
         edtavDdo_proposta_codigotitlecontrolidtoreplace_Internalname = "vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_proposta_objetivo_Internalname = "DDO_PROPOSTA_OBJETIVO";
         edtavDdo_proposta_objetivotitlecontrolidtoreplace_Internalname = "vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE";
         Ddo_requisito_tiporeqcod_Internalname = "DDO_REQUISITO_TIPOREQCOD";
         edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE";
         Ddo_requisito_agrupador_Internalname = "DDO_REQUISITO_AGRUPADOR";
         edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE";
         Ddo_requisito_titulo_Internalname = "DDO_REQUISITO_TITULO";
         edtavDdo_requisito_titulotitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE";
         Ddo_requisito_restricao_Internalname = "DDO_REQUISITO_RESTRICAO";
         edtavDdo_requisito_restricaotitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE";
         Ddo_requisito_ordem_Internalname = "DDO_REQUISITO_ORDEM";
         edtavDdo_requisito_ordemtitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE";
         Ddo_requisito_pontuacao_Internalname = "DDO_REQUISITO_PONTUACAO";
         edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE";
         Ddo_requisito_datahomologacao_Internalname = "DDO_REQUISITO_DATAHOMOLOGACAO";
         edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE";
         Ddo_requisito_status_Internalname = "DDO_REQUISITO_STATUS";
         edtavDdo_requisito_statustitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE";
         Ddo_requisito_ativo_Internalname = "DDO_REQUISITO_ATIVO";
         edtavDdo_requisito_ativotitlecontrolidtoreplace_Internalname = "vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbRequisito_Status_Jsonclick = "";
         edtRequisito_DataHomologacao_Jsonclick = "";
         edtRequisito_Pontuacao_Jsonclick = "";
         edtRequisito_Ordem_Jsonclick = "";
         edtRequisito_Restricao_Jsonclick = "";
         edtRequisito_Titulo_Jsonclick = "";
         edtRequisito_Agrupador_Jsonclick = "";
         edtRequisito_TipoReqCod_Jsonclick = "";
         edtProposta_Objetivo_Jsonclick = "";
         edtProposta_Codigo_Jsonclick = "";
         edtRequisito_Descricao_Jsonclick = "";
         edtRequisito_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtRequisito_Descricao_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkRequisito_Ativo_Titleformat = 0;
         cmbRequisito_Status_Titleformat = 0;
         edtRequisito_DataHomologacao_Titleformat = 0;
         edtRequisito_Pontuacao_Titleformat = 0;
         edtRequisito_Ordem_Titleformat = 0;
         edtRequisito_Restricao_Titleformat = 0;
         edtRequisito_Titulo_Titleformat = 0;
         edtRequisito_Agrupador_Titleformat = 0;
         edtRequisito_TipoReqCod_Titleformat = 0;
         edtProposta_Objetivo_Titleformat = 0;
         edtProposta_Codigo_Titleformat = 0;
         edtRequisito_Descricao_Titleformat = 0;
         edtRequisito_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavRequisito_descricao3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavRequisito_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavRequisito_descricao1_Visible = 1;
         chkRequisito_Ativo.Title.Text = "Ativo";
         cmbRequisito_Status.Title.Text = "Status";
         edtRequisito_DataHomologacao_Title = "Homologa��o";
         edtRequisito_Pontuacao_Title = "Prevista";
         edtRequisito_Ordem_Title = "Ordem";
         edtRequisito_Restricao_Title = "Restri��o";
         edtRequisito_Titulo_Title = "T�tulo";
         edtRequisito_Agrupador_Title = "Agrupador";
         edtRequisito_TipoReqCod_Title = "Tipo";
         edtProposta_Objetivo_Title = "Proposta_Objetivo";
         edtProposta_Codigo_Title = "Proposta_Codigo";
         edtRequisito_Descricao_Title = "do Requisito";
         edtRequisito_Codigo_Title = "do Requisito";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkRequisito_Ativo.Caption = "";
         edtavDdo_requisito_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_ordemtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_restricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_titulotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_proposta_objetivotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_proposta_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_requisito_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfrequisito_ativo_sel_Jsonclick = "";
         edtavTfrequisito_ativo_sel_Visible = 1;
         edtavDdo_requisito_datahomologacaoauxdateto_Jsonclick = "";
         edtavDdo_requisito_datahomologacaoauxdate_Jsonclick = "";
         edtavTfrequisito_datahomologacao_to_Jsonclick = "";
         edtavTfrequisito_datahomologacao_to_Visible = 1;
         edtavTfrequisito_datahomologacao_Jsonclick = "";
         edtavTfrequisito_datahomologacao_Visible = 1;
         edtavTfrequisito_pontuacao_to_Jsonclick = "";
         edtavTfrequisito_pontuacao_to_Visible = 1;
         edtavTfrequisito_pontuacao_Jsonclick = "";
         edtavTfrequisito_pontuacao_Visible = 1;
         edtavTfrequisito_ordem_to_Jsonclick = "";
         edtavTfrequisito_ordem_to_Visible = 1;
         edtavTfrequisito_ordem_Jsonclick = "";
         edtavTfrequisito_ordem_Visible = 1;
         edtavTfrequisito_restricao_sel_Visible = 1;
         edtavTfrequisito_restricao_Visible = 1;
         edtavTfrequisito_titulo_sel_Visible = 1;
         edtavTfrequisito_titulo_Visible = 1;
         edtavTfrequisito_agrupador_sel_Jsonclick = "";
         edtavTfrequisito_agrupador_sel_Visible = 1;
         edtavTfrequisito_agrupador_Jsonclick = "";
         edtavTfrequisito_agrupador_Visible = 1;
         edtavTfrequisito_tiporeqcod_to_Jsonclick = "";
         edtavTfrequisito_tiporeqcod_to_Visible = 1;
         edtavTfrequisito_tiporeqcod_Jsonclick = "";
         edtavTfrequisito_tiporeqcod_Visible = 1;
         edtavTfproposta_objetivo_sel_Visible = 1;
         edtavTfproposta_objetivo_Visible = 1;
         edtavTfproposta_codigo_to_Jsonclick = "";
         edtavTfproposta_codigo_to_Visible = 1;
         edtavTfproposta_codigo_Jsonclick = "";
         edtavTfproposta_codigo_Visible = 1;
         edtavTfrequisito_descricao_sel_Visible = 1;
         edtavTfrequisito_descricao_Visible = 1;
         edtavTfrequisito_codigo_to_Jsonclick = "";
         edtavTfrequisito_codigo_to_Visible = 1;
         edtavTfrequisito_codigo_Jsonclick = "";
         edtavTfrequisito_codigo_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_requisito_ativo_Searchbuttontext = "Pesquisar";
         Ddo_requisito_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_requisito_ativo_Datalisttype = "FixedValues";
         Ddo_requisito_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_requisito_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_requisito_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_ativo_Titlecontrolidtoreplace = "";
         Ddo_requisito_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_ativo_Cls = "ColumnSettings";
         Ddo_requisito_ativo_Tooltip = "Op��es";
         Ddo_requisito_ativo_Caption = "";
         Ddo_requisito_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_requisito_status_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_status_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_status_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_status_Datalistfixedvalues = "0:Rascunho,1:Solicitado,2:Aprovado,3:N�o Aprovado,4:Pausado,5:Cancelado";
         Ddo_requisito_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_requisito_status_Datalisttype = "FixedValues";
         Ddo_requisito_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_requisito_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_requisito_status_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_status_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_status_Titlecontrolidtoreplace = "";
         Ddo_requisito_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_status_Cls = "ColumnSettings";
         Ddo_requisito_status_Tooltip = "Op��es";
         Ddo_requisito_status_Caption = "";
         Ddo_requisito_datahomologacao_Searchbuttontext = "Pesquisar";
         Ddo_requisito_datahomologacao_Rangefilterto = "At�";
         Ddo_requisito_datahomologacao_Rangefilterfrom = "Desde";
         Ddo_requisito_datahomologacao_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_datahomologacao_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_datahomologacao_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_datahomologacao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_requisito_datahomologacao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_requisito_datahomologacao_Filtertype = "Date";
         Ddo_requisito_datahomologacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_datahomologacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_datahomologacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_datahomologacao_Titlecontrolidtoreplace = "";
         Ddo_requisito_datahomologacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_datahomologacao_Cls = "ColumnSettings";
         Ddo_requisito_datahomologacao_Tooltip = "Op��es";
         Ddo_requisito_datahomologacao_Caption = "";
         Ddo_requisito_pontuacao_Searchbuttontext = "Pesquisar";
         Ddo_requisito_pontuacao_Rangefilterto = "At�";
         Ddo_requisito_pontuacao_Rangefilterfrom = "Desde";
         Ddo_requisito_pontuacao_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_pontuacao_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_pontuacao_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_pontuacao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_requisito_pontuacao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_requisito_pontuacao_Filtertype = "Numeric";
         Ddo_requisito_pontuacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_pontuacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_pontuacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_pontuacao_Titlecontrolidtoreplace = "";
         Ddo_requisito_pontuacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_pontuacao_Cls = "ColumnSettings";
         Ddo_requisito_pontuacao_Tooltip = "Op��es";
         Ddo_requisito_pontuacao_Caption = "";
         Ddo_requisito_ordem_Searchbuttontext = "Pesquisar";
         Ddo_requisito_ordem_Rangefilterto = "At�";
         Ddo_requisito_ordem_Rangefilterfrom = "Desde";
         Ddo_requisito_ordem_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_ordem_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_ordem_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_ordem_Includedatalist = Convert.ToBoolean( 0);
         Ddo_requisito_ordem_Filterisrange = Convert.ToBoolean( -1);
         Ddo_requisito_ordem_Filtertype = "Numeric";
         Ddo_requisito_ordem_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_ordem_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_ordem_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_ordem_Titlecontrolidtoreplace = "";
         Ddo_requisito_ordem_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_ordem_Cls = "ColumnSettings";
         Ddo_requisito_ordem_Tooltip = "Op��es";
         Ddo_requisito_ordem_Caption = "";
         Ddo_requisito_restricao_Searchbuttontext = "Pesquisar";
         Ddo_requisito_restricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_requisito_restricao_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_restricao_Loadingdata = "Carregando dados...";
         Ddo_requisito_restricao_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_restricao_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_restricao_Datalistupdateminimumcharacters = 0;
         Ddo_requisito_restricao_Datalistproc = "GetWWRequisitoFilterData";
         Ddo_requisito_restricao_Datalisttype = "Dynamic";
         Ddo_requisito_restricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_requisito_restricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_requisito_restricao_Filtertype = "Character";
         Ddo_requisito_restricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_restricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_restricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_restricao_Titlecontrolidtoreplace = "";
         Ddo_requisito_restricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_restricao_Cls = "ColumnSettings";
         Ddo_requisito_restricao_Tooltip = "Op��es";
         Ddo_requisito_restricao_Caption = "";
         Ddo_requisito_titulo_Searchbuttontext = "Pesquisar";
         Ddo_requisito_titulo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_requisito_titulo_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_titulo_Loadingdata = "Carregando dados...";
         Ddo_requisito_titulo_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_titulo_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_titulo_Datalistupdateminimumcharacters = 0;
         Ddo_requisito_titulo_Datalistproc = "GetWWRequisitoFilterData";
         Ddo_requisito_titulo_Datalisttype = "Dynamic";
         Ddo_requisito_titulo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_requisito_titulo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_requisito_titulo_Filtertype = "Character";
         Ddo_requisito_titulo_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_titulo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_titulo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_titulo_Titlecontrolidtoreplace = "";
         Ddo_requisito_titulo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_titulo_Cls = "ColumnSettings";
         Ddo_requisito_titulo_Tooltip = "Op��es";
         Ddo_requisito_titulo_Caption = "";
         Ddo_requisito_agrupador_Searchbuttontext = "Pesquisar";
         Ddo_requisito_agrupador_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_requisito_agrupador_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_agrupador_Loadingdata = "Carregando dados...";
         Ddo_requisito_agrupador_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_agrupador_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_agrupador_Datalistupdateminimumcharacters = 0;
         Ddo_requisito_agrupador_Datalistproc = "GetWWRequisitoFilterData";
         Ddo_requisito_agrupador_Datalisttype = "Dynamic";
         Ddo_requisito_agrupador_Includedatalist = Convert.ToBoolean( -1);
         Ddo_requisito_agrupador_Filterisrange = Convert.ToBoolean( 0);
         Ddo_requisito_agrupador_Filtertype = "Character";
         Ddo_requisito_agrupador_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_agrupador_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_agrupador_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_agrupador_Titlecontrolidtoreplace = "";
         Ddo_requisito_agrupador_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_agrupador_Cls = "ColumnSettings";
         Ddo_requisito_agrupador_Tooltip = "Op��es";
         Ddo_requisito_agrupador_Caption = "";
         Ddo_requisito_tiporeqcod_Searchbuttontext = "Pesquisar";
         Ddo_requisito_tiporeqcod_Rangefilterto = "At�";
         Ddo_requisito_tiporeqcod_Rangefilterfrom = "Desde";
         Ddo_requisito_tiporeqcod_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_tiporeqcod_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_tiporeqcod_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_tiporeqcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_requisito_tiporeqcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_requisito_tiporeqcod_Filtertype = "Numeric";
         Ddo_requisito_tiporeqcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_tiporeqcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_tiporeqcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_tiporeqcod_Titlecontrolidtoreplace = "";
         Ddo_requisito_tiporeqcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_tiporeqcod_Cls = "ColumnSettings";
         Ddo_requisito_tiporeqcod_Tooltip = "Op��es";
         Ddo_requisito_tiporeqcod_Caption = "";
         Ddo_proposta_objetivo_Searchbuttontext = "Pesquisar";
         Ddo_proposta_objetivo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_proposta_objetivo_Cleanfilter = "Limpar pesquisa";
         Ddo_proposta_objetivo_Loadingdata = "Carregando dados...";
         Ddo_proposta_objetivo_Sortdsc = "Ordenar de Z � A";
         Ddo_proposta_objetivo_Sortasc = "Ordenar de A � Z";
         Ddo_proposta_objetivo_Datalistupdateminimumcharacters = 0;
         Ddo_proposta_objetivo_Datalistproc = "GetWWRequisitoFilterData";
         Ddo_proposta_objetivo_Datalisttype = "Dynamic";
         Ddo_proposta_objetivo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_proposta_objetivo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_proposta_objetivo_Filtertype = "Character";
         Ddo_proposta_objetivo_Includefilter = Convert.ToBoolean( -1);
         Ddo_proposta_objetivo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_proposta_objetivo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_proposta_objetivo_Titlecontrolidtoreplace = "";
         Ddo_proposta_objetivo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_proposta_objetivo_Cls = "ColumnSettings";
         Ddo_proposta_objetivo_Tooltip = "Op��es";
         Ddo_proposta_objetivo_Caption = "";
         Ddo_proposta_codigo_Searchbuttontext = "Pesquisar";
         Ddo_proposta_codigo_Rangefilterto = "At�";
         Ddo_proposta_codigo_Rangefilterfrom = "Desde";
         Ddo_proposta_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_proposta_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_proposta_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_proposta_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_proposta_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_proposta_codigo_Filtertype = "Numeric";
         Ddo_proposta_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_proposta_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_proposta_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_proposta_codigo_Titlecontrolidtoreplace = "";
         Ddo_proposta_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_proposta_codigo_Cls = "ColumnSettings";
         Ddo_proposta_codigo_Tooltip = "Op��es";
         Ddo_proposta_codigo_Caption = "";
         Ddo_requisito_descricao_Searchbuttontext = "Pesquisar";
         Ddo_requisito_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_requisito_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_descricao_Loadingdata = "Carregando dados...";
         Ddo_requisito_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_requisito_descricao_Datalistproc = "GetWWRequisitoFilterData";
         Ddo_requisito_descricao_Datalisttype = "Dynamic";
         Ddo_requisito_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_requisito_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_requisito_descricao_Filtertype = "Character";
         Ddo_requisito_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_descricao_Titlecontrolidtoreplace = "";
         Ddo_requisito_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_descricao_Cls = "ColumnSettings";
         Ddo_requisito_descricao_Tooltip = "Op��es";
         Ddo_requisito_descricao_Caption = "";
         Ddo_requisito_codigo_Searchbuttontext = "Pesquisar";
         Ddo_requisito_codigo_Rangefilterto = "At�";
         Ddo_requisito_codigo_Rangefilterfrom = "Desde";
         Ddo_requisito_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_requisito_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_requisito_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_requisito_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_requisito_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_requisito_codigo_Filtertype = "Numeric";
         Ddo_requisito_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_requisito_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_requisito_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_requisito_codigo_Titlecontrolidtoreplace = "";
         Ddo_requisito_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_requisito_codigo_Cls = "ColumnSettings";
         Ddo_requisito_codigo_Tooltip = "Op��es";
         Ddo_requisito_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Requisito";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV38Requisito_CodigoTitleFilterData',fld:'vREQUISITO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV42Requisito_DescricaoTitleFilterData',fld:'vREQUISITO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46Proposta_CodigoTitleFilterData',fld:'vPROPOSTA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV50Proposta_ObjetivoTitleFilterData',fld:'vPROPOSTA_OBJETIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV107Requisito_TipoReqCodTitleFilterData',fld:'vREQUISITO_TIPOREQCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV62Requisito_AgrupadorTitleFilterData',fld:'vREQUISITO_AGRUPADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV66Requisito_TituloTitleFilterData',fld:'vREQUISITO_TITULOTITLEFILTERDATA',pic:'',nv:null},{av:'AV74Requisito_RestricaoTitleFilterData',fld:'vREQUISITO_RESTRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV82Requisito_OrdemTitleFilterData',fld:'vREQUISITO_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV86Requisito_PontuacaoTitleFilterData',fld:'vREQUISITO_PONTUACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV90Requisito_DataHomologacaoTitleFilterData',fld:'vREQUISITO_DATAHOMOLOGACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV96Requisito_StatusTitleFilterData',fld:'vREQUISITO_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV100Requisito_AtivoTitleFilterData',fld:'vREQUISITO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtRequisito_Codigo_Titleformat',ctrl:'REQUISITO_CODIGO',prop:'Titleformat'},{av:'edtRequisito_Codigo_Title',ctrl:'REQUISITO_CODIGO',prop:'Title'},{av:'edtRequisito_Descricao_Titleformat',ctrl:'REQUISITO_DESCRICAO',prop:'Titleformat'},{av:'edtRequisito_Descricao_Title',ctrl:'REQUISITO_DESCRICAO',prop:'Title'},{av:'edtProposta_Codigo_Titleformat',ctrl:'PROPOSTA_CODIGO',prop:'Titleformat'},{av:'edtProposta_Codigo_Title',ctrl:'PROPOSTA_CODIGO',prop:'Title'},{av:'edtProposta_Objetivo_Titleformat',ctrl:'PROPOSTA_OBJETIVO',prop:'Titleformat'},{av:'edtProposta_Objetivo_Title',ctrl:'PROPOSTA_OBJETIVO',prop:'Title'},{av:'edtRequisito_TipoReqCod_Titleformat',ctrl:'REQUISITO_TIPOREQCOD',prop:'Titleformat'},{av:'edtRequisito_TipoReqCod_Title',ctrl:'REQUISITO_TIPOREQCOD',prop:'Title'},{av:'edtRequisito_Agrupador_Titleformat',ctrl:'REQUISITO_AGRUPADOR',prop:'Titleformat'},{av:'edtRequisito_Agrupador_Title',ctrl:'REQUISITO_AGRUPADOR',prop:'Title'},{av:'edtRequisito_Titulo_Titleformat',ctrl:'REQUISITO_TITULO',prop:'Titleformat'},{av:'edtRequisito_Titulo_Title',ctrl:'REQUISITO_TITULO',prop:'Title'},{av:'edtRequisito_Restricao_Titleformat',ctrl:'REQUISITO_RESTRICAO',prop:'Titleformat'},{av:'edtRequisito_Restricao_Title',ctrl:'REQUISITO_RESTRICAO',prop:'Title'},{av:'edtRequisito_Ordem_Titleformat',ctrl:'REQUISITO_ORDEM',prop:'Titleformat'},{av:'edtRequisito_Ordem_Title',ctrl:'REQUISITO_ORDEM',prop:'Title'},{av:'edtRequisito_Pontuacao_Titleformat',ctrl:'REQUISITO_PONTUACAO',prop:'Titleformat'},{av:'edtRequisito_Pontuacao_Title',ctrl:'REQUISITO_PONTUACAO',prop:'Title'},{av:'edtRequisito_DataHomologacao_Titleformat',ctrl:'REQUISITO_DATAHOMOLOGACAO',prop:'Titleformat'},{av:'edtRequisito_DataHomologacao_Title',ctrl:'REQUISITO_DATAHOMOLOGACAO',prop:'Title'},{av:'cmbRequisito_Status'},{av:'chkRequisito_Ativo_Titleformat',ctrl:'REQUISITO_ATIVO',prop:'Titleformat'},{av:'chkRequisito_Ativo.Title.Text',ctrl:'REQUISITO_ATIVO',prop:'Title'},{av:'AV105GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV106GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV36ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_REQUISITO_CODIGO.ONOPTIONCLICKED","{handler:'E13PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_codigo_Activeeventkey',ctrl:'DDO_REQUISITO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_requisito_codigo_Filteredtext_get',ctrl:'DDO_REQUISITO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_requisito_codigo_Filteredtextto_get',ctrl:'DDO_REQUISITO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_DESCRICAO.ONOPTIONCLICKED","{handler:'E14PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_descricao_Activeeventkey',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_requisito_descricao_Filteredtext_get',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_requisito_descricao_Selectedvalue_get',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PROPOSTA_CODIGO.ONOPTIONCLICKED","{handler:'E15PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_proposta_codigo_Activeeventkey',ctrl:'DDO_PROPOSTA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_proposta_codigo_Filteredtext_get',ctrl:'DDO_PROPOSTA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_proposta_codigo_Filteredtextto_get',ctrl:'DDO_PROPOSTA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PROPOSTA_OBJETIVO.ONOPTIONCLICKED","{handler:'E16PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_proposta_objetivo_Activeeventkey',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'ActiveEventKey'},{av:'Ddo_proposta_objetivo_Filteredtext_get',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'FilteredText_get'},{av:'Ddo_proposta_objetivo_Selectedvalue_get',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_TIPOREQCOD.ONOPTIONCLICKED","{handler:'E17PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_tiporeqcod_Activeeventkey',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'ActiveEventKey'},{av:'Ddo_requisito_tiporeqcod_Filteredtext_get',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'FilteredText_get'},{av:'Ddo_requisito_tiporeqcod_Filteredtextto_get',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_AGRUPADOR.ONOPTIONCLICKED","{handler:'E18PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_agrupador_Activeeventkey',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'ActiveEventKey'},{av:'Ddo_requisito_agrupador_Filteredtext_get',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'FilteredText_get'},{av:'Ddo_requisito_agrupador_Selectedvalue_get',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_TITULO.ONOPTIONCLICKED","{handler:'E19PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_titulo_Activeeventkey',ctrl:'DDO_REQUISITO_TITULO',prop:'ActiveEventKey'},{av:'Ddo_requisito_titulo_Filteredtext_get',ctrl:'DDO_REQUISITO_TITULO',prop:'FilteredText_get'},{av:'Ddo_requisito_titulo_Selectedvalue_get',ctrl:'DDO_REQUISITO_TITULO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_RESTRICAO.ONOPTIONCLICKED","{handler:'E20PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_restricao_Activeeventkey',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'ActiveEventKey'},{av:'Ddo_requisito_restricao_Filteredtext_get',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'FilteredText_get'},{av:'Ddo_requisito_restricao_Selectedvalue_get',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_ORDEM.ONOPTIONCLICKED","{handler:'E21PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_ordem_Activeeventkey',ctrl:'DDO_REQUISITO_ORDEM',prop:'ActiveEventKey'},{av:'Ddo_requisito_ordem_Filteredtext_get',ctrl:'DDO_REQUISITO_ORDEM',prop:'FilteredText_get'},{av:'Ddo_requisito_ordem_Filteredtextto_get',ctrl:'DDO_REQUISITO_ORDEM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_PONTUACAO.ONOPTIONCLICKED","{handler:'E22PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_pontuacao_Activeeventkey',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'ActiveEventKey'},{av:'Ddo_requisito_pontuacao_Filteredtext_get',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'FilteredText_get'},{av:'Ddo_requisito_pontuacao_Filteredtextto_get',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_DATAHOMOLOGACAO.ONOPTIONCLICKED","{handler:'E23PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_datahomologacao_Activeeventkey',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'ActiveEventKey'},{av:'Ddo_requisito_datahomologacao_Filteredtext_get',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'FilteredText_get'},{av:'Ddo_requisito_datahomologacao_Filteredtextto_get',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_STATUS.ONOPTIONCLICKED","{handler:'E24PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_status_Activeeventkey',ctrl:'DDO_REQUISITO_STATUS',prop:'ActiveEventKey'},{av:'Ddo_requisito_status_Selectedvalue_get',ctrl:'DDO_REQUISITO_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REQUISITO_ATIVO.ONOPTIONCLICKED","{handler:'E25PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_requisito_ativo_Activeeventkey',ctrl:'DDO_REQUISITO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_requisito_ativo_Selectedvalue_get',ctrl:'DDO_REQUISITO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E38PZ2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV30Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtRequisito_Descricao_Link',ctrl:'REQUISITO_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E26PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E31PZ2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E27PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavRequisito_descricao2_Visible',ctrl:'vREQUISITO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavRequisito_descricao3_Visible',ctrl:'vREQUISITO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavRequisito_descricao1_Visible',ctrl:'vREQUISITO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E32PZ2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavRequisito_descricao1_Visible',ctrl:'vREQUISITO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E33PZ2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E28PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavRequisito_descricao2_Visible',ctrl:'vREQUISITO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavRequisito_descricao3_Visible',ctrl:'vREQUISITO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavRequisito_descricao1_Visible',ctrl:'vREQUISITO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E34PZ2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavRequisito_descricao2_Visible',ctrl:'vREQUISITO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E29PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavRequisito_descricao2_Visible',ctrl:'vREQUISITO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavRequisito_descricao3_Visible',ctrl:'vREQUISITO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavRequisito_descricao1_Visible',ctrl:'vREQUISITO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E35PZ2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavRequisito_descricao3_Visible',ctrl:'vREQUISITO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11PZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Requisito_CodigoTitleControlIdToReplace',fld:'vDDO_REQUISITO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Requisito_DescricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Proposta_CodigoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Proposta_ObjetivoTitleControlIdToReplace',fld:'vDDO_PROPOSTA_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace',fld:'vDDO_REQUISITO_TIPOREQCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Requisito_AgrupadorTitleControlIdToReplace',fld:'vDDO_REQUISITO_AGRUPADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Requisito_TituloTitleControlIdToReplace',fld:'vDDO_REQUISITO_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Requisito_RestricaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_RESTRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Requisito_OrdemTitleControlIdToReplace',fld:'vDDO_REQUISITO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Requisito_PontuacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_PONTUACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_REQUISITO_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Requisito_StatusTitleControlIdToReplace',fld:'vDDO_REQUISITO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Requisito_AtivoTitleControlIdToReplace',fld:'vDDO_REQUISITO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV151Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV39TFRequisito_Codigo',fld:'vTFREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_requisito_codigo_Filteredtext_set',ctrl:'DDO_REQUISITO_CODIGO',prop:'FilteredText_set'},{av:'AV40TFRequisito_Codigo_To',fld:'vTFREQUISITO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_requisito_codigo_Filteredtextto_set',ctrl:'DDO_REQUISITO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV43TFRequisito_Descricao',fld:'vTFREQUISITO_DESCRICAO',pic:'',nv:''},{av:'Ddo_requisito_descricao_Filteredtext_set',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'FilteredText_set'},{av:'AV44TFRequisito_Descricao_Sel',fld:'vTFREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_requisito_descricao_Selectedvalue_set',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV47TFProposta_Codigo',fld:'vTFPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'Ddo_proposta_codigo_Filteredtext_set',ctrl:'DDO_PROPOSTA_CODIGO',prop:'FilteredText_set'},{av:'AV48TFProposta_Codigo_To',fld:'vTFPROPOSTA_CODIGO_TO',pic:'ZZZZZZZZ9',nv:0},{av:'Ddo_proposta_codigo_Filteredtextto_set',ctrl:'DDO_PROPOSTA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV51TFProposta_Objetivo',fld:'vTFPROPOSTA_OBJETIVO',pic:'',nv:''},{av:'Ddo_proposta_objetivo_Filteredtext_set',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'FilteredText_set'},{av:'AV52TFProposta_Objetivo_Sel',fld:'vTFPROPOSTA_OBJETIVO_SEL',pic:'',nv:''},{av:'Ddo_proposta_objetivo_Selectedvalue_set',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SelectedValue_set'},{av:'AV108TFRequisito_TipoReqCod',fld:'vTFREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_requisito_tiporeqcod_Filteredtext_set',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'FilteredText_set'},{av:'AV109TFRequisito_TipoReqCod_To',fld:'vTFREQUISITO_TIPOREQCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_requisito_tiporeqcod_Filteredtextto_set',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'FilteredTextTo_set'},{av:'AV63TFRequisito_Agrupador',fld:'vTFREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'Ddo_requisito_agrupador_Filteredtext_set',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'FilteredText_set'},{av:'AV64TFRequisito_Agrupador_Sel',fld:'vTFREQUISITO_AGRUPADOR_SEL',pic:'',nv:''},{av:'Ddo_requisito_agrupador_Selectedvalue_set',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SelectedValue_set'},{av:'AV67TFRequisito_Titulo',fld:'vTFREQUISITO_TITULO',pic:'',nv:''},{av:'Ddo_requisito_titulo_Filteredtext_set',ctrl:'DDO_REQUISITO_TITULO',prop:'FilteredText_set'},{av:'AV68TFRequisito_Titulo_Sel',fld:'vTFREQUISITO_TITULO_SEL',pic:'',nv:''},{av:'Ddo_requisito_titulo_Selectedvalue_set',ctrl:'DDO_REQUISITO_TITULO',prop:'SelectedValue_set'},{av:'AV75TFRequisito_Restricao',fld:'vTFREQUISITO_RESTRICAO',pic:'',nv:''},{av:'Ddo_requisito_restricao_Filteredtext_set',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'FilteredText_set'},{av:'AV76TFRequisito_Restricao_Sel',fld:'vTFREQUISITO_RESTRICAO_SEL',pic:'',nv:''},{av:'Ddo_requisito_restricao_Selectedvalue_set',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SelectedValue_set'},{av:'AV83TFRequisito_Ordem',fld:'vTFREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'Ddo_requisito_ordem_Filteredtext_set',ctrl:'DDO_REQUISITO_ORDEM',prop:'FilteredText_set'},{av:'AV84TFRequisito_Ordem_To',fld:'vTFREQUISITO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_requisito_ordem_Filteredtextto_set',ctrl:'DDO_REQUISITO_ORDEM',prop:'FilteredTextTo_set'},{av:'AV87TFRequisito_Pontuacao',fld:'vTFREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_requisito_pontuacao_Filteredtext_set',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'FilteredText_set'},{av:'AV88TFRequisito_Pontuacao_To',fld:'vTFREQUISITO_PONTUACAO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_requisito_pontuacao_Filteredtextto_set',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'FilteredTextTo_set'},{av:'AV91TFRequisito_DataHomologacao',fld:'vTFREQUISITO_DATAHOMOLOGACAO',pic:'',nv:''},{av:'Ddo_requisito_datahomologacao_Filteredtext_set',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'FilteredText_set'},{av:'AV92TFRequisito_DataHomologacao_To',fld:'vTFREQUISITO_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'Ddo_requisito_datahomologacao_Filteredtextto_set',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'FilteredTextTo_set'},{av:'AV98TFRequisito_Status_Sels',fld:'vTFREQUISITO_STATUS_SELS',pic:'',nv:null},{av:'Ddo_requisito_status_Selectedvalue_set',ctrl:'DDO_REQUISITO_STATUS',prop:'SelectedValue_set'},{av:'AV101TFRequisito_Ativo_Sel',fld:'vTFREQUISITO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_requisito_ativo_Selectedvalue_set',ctrl:'DDO_REQUISITO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Requisito_Descricao1',fld:'vREQUISITO_DESCRICAO1',pic:'',nv:''},{av:'Ddo_requisito_ativo_Sortedstatus',ctrl:'DDO_REQUISITO_ATIVO',prop:'SortedStatus'},{av:'Ddo_requisito_status_Sortedstatus',ctrl:'DDO_REQUISITO_STATUS',prop:'SortedStatus'},{av:'Ddo_requisito_datahomologacao_Sortedstatus',ctrl:'DDO_REQUISITO_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_requisito_pontuacao_Sortedstatus',ctrl:'DDO_REQUISITO_PONTUACAO',prop:'SortedStatus'},{av:'Ddo_requisito_ordem_Sortedstatus',ctrl:'DDO_REQUISITO_ORDEM',prop:'SortedStatus'},{av:'Ddo_requisito_restricao_Sortedstatus',ctrl:'DDO_REQUISITO_RESTRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_titulo_Sortedstatus',ctrl:'DDO_REQUISITO_TITULO',prop:'SortedStatus'},{av:'Ddo_requisito_agrupador_Sortedstatus',ctrl:'DDO_REQUISITO_AGRUPADOR',prop:'SortedStatus'},{av:'Ddo_requisito_tiporeqcod_Sortedstatus',ctrl:'DDO_REQUISITO_TIPOREQCOD',prop:'SortedStatus'},{av:'Ddo_proposta_objetivo_Sortedstatus',ctrl:'DDO_PROPOSTA_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_proposta_codigo_Sortedstatus',ctrl:'DDO_PROPOSTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_requisito_descricao_Sortedstatus',ctrl:'DDO_REQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_requisito_codigo_Sortedstatus',ctrl:'DDO_REQUISITO_CODIGO',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Requisito_Descricao2',fld:'vREQUISITO_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Requisito_Descricao3',fld:'vREQUISITO_DESCRICAO3',pic:'',nv:''},{av:'edtavRequisito_descricao1_Visible',ctrl:'vREQUISITO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavRequisito_descricao2_Visible',ctrl:'vREQUISITO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavRequisito_descricao3_Visible',ctrl:'vREQUISITO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E30PZ2',iparms:[{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_requisito_codigo_Activeeventkey = "";
         Ddo_requisito_codigo_Filteredtext_get = "";
         Ddo_requisito_codigo_Filteredtextto_get = "";
         Ddo_requisito_descricao_Activeeventkey = "";
         Ddo_requisito_descricao_Filteredtext_get = "";
         Ddo_requisito_descricao_Selectedvalue_get = "";
         Ddo_proposta_codigo_Activeeventkey = "";
         Ddo_proposta_codigo_Filteredtext_get = "";
         Ddo_proposta_codigo_Filteredtextto_get = "";
         Ddo_proposta_objetivo_Activeeventkey = "";
         Ddo_proposta_objetivo_Filteredtext_get = "";
         Ddo_proposta_objetivo_Selectedvalue_get = "";
         Ddo_requisito_tiporeqcod_Activeeventkey = "";
         Ddo_requisito_tiporeqcod_Filteredtext_get = "";
         Ddo_requisito_tiporeqcod_Filteredtextto_get = "";
         Ddo_requisito_agrupador_Activeeventkey = "";
         Ddo_requisito_agrupador_Filteredtext_get = "";
         Ddo_requisito_agrupador_Selectedvalue_get = "";
         Ddo_requisito_titulo_Activeeventkey = "";
         Ddo_requisito_titulo_Filteredtext_get = "";
         Ddo_requisito_titulo_Selectedvalue_get = "";
         Ddo_requisito_restricao_Activeeventkey = "";
         Ddo_requisito_restricao_Filteredtext_get = "";
         Ddo_requisito_restricao_Selectedvalue_get = "";
         Ddo_requisito_ordem_Activeeventkey = "";
         Ddo_requisito_ordem_Filteredtext_get = "";
         Ddo_requisito_ordem_Filteredtextto_get = "";
         Ddo_requisito_pontuacao_Activeeventkey = "";
         Ddo_requisito_pontuacao_Filteredtext_get = "";
         Ddo_requisito_pontuacao_Filteredtextto_get = "";
         Ddo_requisito_datahomologacao_Activeeventkey = "";
         Ddo_requisito_datahomologacao_Filteredtext_get = "";
         Ddo_requisito_datahomologacao_Filteredtextto_get = "";
         Ddo_requisito_status_Activeeventkey = "";
         Ddo_requisito_status_Selectedvalue_get = "";
         Ddo_requisito_ativo_Activeeventkey = "";
         Ddo_requisito_ativo_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Requisito_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21Requisito_Descricao2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25Requisito_Descricao3 = "";
         AV43TFRequisito_Descricao = "";
         AV44TFRequisito_Descricao_Sel = "";
         AV51TFProposta_Objetivo = "";
         AV52TFProposta_Objetivo_Sel = "";
         AV63TFRequisito_Agrupador = "";
         AV64TFRequisito_Agrupador_Sel = "";
         AV67TFRequisito_Titulo = "";
         AV68TFRequisito_Titulo_Sel = "";
         AV75TFRequisito_Restricao = "";
         AV76TFRequisito_Restricao_Sel = "";
         AV91TFRequisito_DataHomologacao = DateTime.MinValue;
         AV92TFRequisito_DataHomologacao_To = DateTime.MinValue;
         AV41ddo_Requisito_CodigoTitleControlIdToReplace = "";
         AV45ddo_Requisito_DescricaoTitleControlIdToReplace = "";
         AV49ddo_Proposta_CodigoTitleControlIdToReplace = "";
         AV53ddo_Proposta_ObjetivoTitleControlIdToReplace = "";
         AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace = "";
         AV65ddo_Requisito_AgrupadorTitleControlIdToReplace = "";
         AV69ddo_Requisito_TituloTitleControlIdToReplace = "";
         AV77ddo_Requisito_RestricaoTitleControlIdToReplace = "";
         AV85ddo_Requisito_OrdemTitleControlIdToReplace = "";
         AV89ddo_Requisito_PontuacaoTitleControlIdToReplace = "";
         AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace = "";
         AV99ddo_Requisito_StatusTitleControlIdToReplace = "";
         AV102ddo_Requisito_AtivoTitleControlIdToReplace = "";
         AV98TFRequisito_Status_Sels = new GxSimpleCollection();
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV151Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV36ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV38Requisito_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Requisito_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Proposta_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Proposta_ObjetivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107Requisito_TipoReqCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Requisito_AgrupadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Requisito_TituloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Requisito_RestricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Requisito_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86Requisito_PontuacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90Requisito_DataHomologacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV96Requisito_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV100Requisito_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_requisito_codigo_Filteredtext_set = "";
         Ddo_requisito_codigo_Filteredtextto_set = "";
         Ddo_requisito_codigo_Sortedstatus = "";
         Ddo_requisito_descricao_Filteredtext_set = "";
         Ddo_requisito_descricao_Selectedvalue_set = "";
         Ddo_requisito_descricao_Sortedstatus = "";
         Ddo_proposta_codigo_Filteredtext_set = "";
         Ddo_proposta_codigo_Filteredtextto_set = "";
         Ddo_proposta_codigo_Sortedstatus = "";
         Ddo_proposta_objetivo_Filteredtext_set = "";
         Ddo_proposta_objetivo_Selectedvalue_set = "";
         Ddo_proposta_objetivo_Sortedstatus = "";
         Ddo_requisito_tiporeqcod_Filteredtext_set = "";
         Ddo_requisito_tiporeqcod_Filteredtextto_set = "";
         Ddo_requisito_tiporeqcod_Sortedstatus = "";
         Ddo_requisito_agrupador_Filteredtext_set = "";
         Ddo_requisito_agrupador_Selectedvalue_set = "";
         Ddo_requisito_agrupador_Sortedstatus = "";
         Ddo_requisito_titulo_Filteredtext_set = "";
         Ddo_requisito_titulo_Selectedvalue_set = "";
         Ddo_requisito_titulo_Sortedstatus = "";
         Ddo_requisito_restricao_Filteredtext_set = "";
         Ddo_requisito_restricao_Selectedvalue_set = "";
         Ddo_requisito_restricao_Sortedstatus = "";
         Ddo_requisito_ordem_Filteredtext_set = "";
         Ddo_requisito_ordem_Filteredtextto_set = "";
         Ddo_requisito_ordem_Sortedstatus = "";
         Ddo_requisito_pontuacao_Filteredtext_set = "";
         Ddo_requisito_pontuacao_Filteredtextto_set = "";
         Ddo_requisito_pontuacao_Sortedstatus = "";
         Ddo_requisito_datahomologacao_Filteredtext_set = "";
         Ddo_requisito_datahomologacao_Filteredtextto_set = "";
         Ddo_requisito_datahomologacao_Sortedstatus = "";
         Ddo_requisito_status_Selectedvalue_set = "";
         Ddo_requisito_status_Sortedstatus = "";
         Ddo_requisito_ativo_Selectedvalue_set = "";
         Ddo_requisito_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV93DDO_Requisito_DataHomologacaoAuxDate = DateTime.MinValue;
         AV94DDO_Requisito_DataHomologacaoAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV148Update_GXI = "";
         AV29Delete = "";
         AV149Delete_GXI = "";
         AV30Display = "";
         AV150Display_GXI = "";
         A1923Requisito_Descricao = "";
         A1690Proposta_Objetivo = "";
         A1926Requisito_Agrupador = "";
         A1927Requisito_Titulo = "";
         A1929Requisito_Restricao = "";
         A1933Requisito_DataHomologacao = DateTime.MinValue;
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV146WWRequisitoDS_34_Tfrequisito_status_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV115WWRequisitoDS_3_Requisito_descricao1 = "";
         lV119WWRequisitoDS_7_Requisito_descricao2 = "";
         lV123WWRequisitoDS_11_Requisito_descricao3 = "";
         lV126WWRequisitoDS_14_Tfrequisito_descricao = "";
         lV130WWRequisitoDS_18_Tfproposta_objetivo = "";
         lV134WWRequisitoDS_22_Tfrequisito_agrupador = "";
         lV136WWRequisitoDS_24_Tfrequisito_titulo = "";
         lV138WWRequisitoDS_26_Tfrequisito_restricao = "";
         AV113WWRequisitoDS_1_Dynamicfiltersselector1 = "";
         AV115WWRequisitoDS_3_Requisito_descricao1 = "";
         AV117WWRequisitoDS_5_Dynamicfiltersselector2 = "";
         AV119WWRequisitoDS_7_Requisito_descricao2 = "";
         AV121WWRequisitoDS_9_Dynamicfiltersselector3 = "";
         AV123WWRequisitoDS_11_Requisito_descricao3 = "";
         AV127WWRequisitoDS_15_Tfrequisito_descricao_sel = "";
         AV126WWRequisitoDS_14_Tfrequisito_descricao = "";
         AV131WWRequisitoDS_19_Tfproposta_objetivo_sel = "";
         AV130WWRequisitoDS_18_Tfproposta_objetivo = "";
         AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel = "";
         AV134WWRequisitoDS_22_Tfrequisito_agrupador = "";
         AV137WWRequisitoDS_25_Tfrequisito_titulo_sel = "";
         AV136WWRequisitoDS_24_Tfrequisito_titulo = "";
         AV139WWRequisitoDS_27_Tfrequisito_restricao_sel = "";
         AV138WWRequisitoDS_26_Tfrequisito_restricao = "";
         AV144WWRequisitoDS_32_Tfrequisito_datahomologacao = DateTime.MinValue;
         AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to = DateTime.MinValue;
         H00PZ2_A1935Requisito_Ativo = new bool[] {false} ;
         H00PZ2_A1934Requisito_Status = new short[1] ;
         H00PZ2_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         H00PZ2_n1933Requisito_DataHomologacao = new bool[] {false} ;
         H00PZ2_A1932Requisito_Pontuacao = new decimal[1] ;
         H00PZ2_n1932Requisito_Pontuacao = new bool[] {false} ;
         H00PZ2_A1931Requisito_Ordem = new short[1] ;
         H00PZ2_n1931Requisito_Ordem = new bool[] {false} ;
         H00PZ2_A1929Requisito_Restricao = new String[] {""} ;
         H00PZ2_n1929Requisito_Restricao = new bool[] {false} ;
         H00PZ2_A1927Requisito_Titulo = new String[] {""} ;
         H00PZ2_n1927Requisito_Titulo = new bool[] {false} ;
         H00PZ2_A1926Requisito_Agrupador = new String[] {""} ;
         H00PZ2_n1926Requisito_Agrupador = new bool[] {false} ;
         H00PZ2_A2049Requisito_TipoReqCod = new int[1] ;
         H00PZ2_n2049Requisito_TipoReqCod = new bool[] {false} ;
         H00PZ2_A1690Proposta_Objetivo = new String[] {""} ;
         H00PZ2_A1685Proposta_Codigo = new int[1] ;
         H00PZ2_n1685Proposta_Codigo = new bool[] {false} ;
         H00PZ2_A1923Requisito_Descricao = new String[] {""} ;
         H00PZ2_n1923Requisito_Descricao = new bool[] {false} ;
         H00PZ2_A1919Requisito_Codigo = new int[1] ;
         H00PZ3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV97TFRequisito_Status_SelsJson = "";
         GridRow = new GXWebRow();
         AV33ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV34ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV35ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV31Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblRequisitotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwrequisito__default(),
            new Object[][] {
                new Object[] {
               H00PZ2_A1935Requisito_Ativo, H00PZ2_A1934Requisito_Status, H00PZ2_A1933Requisito_DataHomologacao, H00PZ2_n1933Requisito_DataHomologacao, H00PZ2_A1932Requisito_Pontuacao, H00PZ2_n1932Requisito_Pontuacao, H00PZ2_A1931Requisito_Ordem, H00PZ2_n1931Requisito_Ordem, H00PZ2_A1929Requisito_Restricao, H00PZ2_n1929Requisito_Restricao,
               H00PZ2_A1927Requisito_Titulo, H00PZ2_n1927Requisito_Titulo, H00PZ2_A1926Requisito_Agrupador, H00PZ2_n1926Requisito_Agrupador, H00PZ2_A2049Requisito_TipoReqCod, H00PZ2_n2049Requisito_TipoReqCod, H00PZ2_A1690Proposta_Objetivo, H00PZ2_A1685Proposta_Codigo, H00PZ2_n1685Proposta_Codigo, H00PZ2_A1923Requisito_Descricao,
               H00PZ2_n1923Requisito_Descricao, H00PZ2_A1919Requisito_Codigo
               }
               , new Object[] {
               H00PZ3_AGRID_nRecordCount
               }
            }
         );
         AV151Pgmname = "WWRequisito";
         /* GeneXus formulas. */
         AV151Pgmname = "WWRequisito";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV83TFRequisito_Ordem ;
      private short AV84TFRequisito_Ordem_To ;
      private short AV101TFRequisito_Ativo_Sel ;
      private short AV32ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1931Requisito_Ordem ;
      private short A1934Requisito_Status ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV114WWRequisitoDS_2_Dynamicfiltersoperator1 ;
      private short AV118WWRequisitoDS_6_Dynamicfiltersoperator2 ;
      private short AV122WWRequisitoDS_10_Dynamicfiltersoperator3 ;
      private short AV140WWRequisitoDS_28_Tfrequisito_ordem ;
      private short AV141WWRequisitoDS_29_Tfrequisito_ordem_to ;
      private short AV147WWRequisitoDS_35_Tfrequisito_ativo_sel ;
      private short edtRequisito_Codigo_Titleformat ;
      private short edtRequisito_Descricao_Titleformat ;
      private short edtProposta_Codigo_Titleformat ;
      private short edtProposta_Objetivo_Titleformat ;
      private short edtRequisito_TipoReqCod_Titleformat ;
      private short edtRequisito_Agrupador_Titleformat ;
      private short edtRequisito_Titulo_Titleformat ;
      private short edtRequisito_Restricao_Titleformat ;
      private short edtRequisito_Ordem_Titleformat ;
      private short edtRequisito_Pontuacao_Titleformat ;
      private short edtRequisito_DataHomologacao_Titleformat ;
      private short cmbRequisito_Status_Titleformat ;
      private short chkRequisito_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV39TFRequisito_Codigo ;
      private int AV40TFRequisito_Codigo_To ;
      private int AV47TFProposta_Codigo ;
      private int AV48TFProposta_Codigo_To ;
      private int AV108TFRequisito_TipoReqCod ;
      private int AV109TFRequisito_TipoReqCod_To ;
      private int A1919Requisito_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_requisito_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_proposta_objetivo_Datalistupdateminimumcharacters ;
      private int Ddo_requisito_agrupador_Datalistupdateminimumcharacters ;
      private int Ddo_requisito_titulo_Datalistupdateminimumcharacters ;
      private int Ddo_requisito_restricao_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfrequisito_codigo_Visible ;
      private int edtavTfrequisito_codigo_to_Visible ;
      private int edtavTfrequisito_descricao_Visible ;
      private int edtavTfrequisito_descricao_sel_Visible ;
      private int edtavTfproposta_codigo_Visible ;
      private int edtavTfproposta_codigo_to_Visible ;
      private int edtavTfproposta_objetivo_Visible ;
      private int edtavTfproposta_objetivo_sel_Visible ;
      private int edtavTfrequisito_tiporeqcod_Visible ;
      private int edtavTfrequisito_tiporeqcod_to_Visible ;
      private int edtavTfrequisito_agrupador_Visible ;
      private int edtavTfrequisito_agrupador_sel_Visible ;
      private int edtavTfrequisito_titulo_Visible ;
      private int edtavTfrequisito_titulo_sel_Visible ;
      private int edtavTfrequisito_restricao_Visible ;
      private int edtavTfrequisito_restricao_sel_Visible ;
      private int edtavTfrequisito_ordem_Visible ;
      private int edtavTfrequisito_ordem_to_Visible ;
      private int edtavTfrequisito_pontuacao_Visible ;
      private int edtavTfrequisito_pontuacao_to_Visible ;
      private int edtavTfrequisito_datahomologacao_Visible ;
      private int edtavTfrequisito_datahomologacao_to_Visible ;
      private int edtavTfrequisito_ativo_sel_Visible ;
      private int edtavDdo_requisito_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_proposta_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_proposta_objetivotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_titulotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_restricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_ordemtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_statustitlecontrolidtoreplace_Visible ;
      private int edtavDdo_requisito_ativotitlecontrolidtoreplace_Visible ;
      private int A1685Proposta_Codigo ;
      private int A2049Requisito_TipoReqCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV146WWRequisitoDS_34_Tfrequisito_status_sels_Count ;
      private int AV124WWRequisitoDS_12_Tfrequisito_codigo ;
      private int AV125WWRequisitoDS_13_Tfrequisito_codigo_to ;
      private int AV128WWRequisitoDS_16_Tfproposta_codigo ;
      private int AV129WWRequisitoDS_17_Tfproposta_codigo_to ;
      private int AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod ;
      private int AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV104PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavRequisito_descricao1_Visible ;
      private int edtavRequisito_descricao2_Visible ;
      private int edtavRequisito_descricao3_Visible ;
      private int AV152GXV1 ;
      private int AV153GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV105GridCurrentPage ;
      private long AV106GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV87TFRequisito_Pontuacao ;
      private decimal AV88TFRequisito_Pontuacao_To ;
      private decimal A1932Requisito_Pontuacao ;
      private decimal AV142WWRequisitoDS_30_Tfrequisito_pontuacao ;
      private decimal AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_requisito_codigo_Activeeventkey ;
      private String Ddo_requisito_codigo_Filteredtext_get ;
      private String Ddo_requisito_codigo_Filteredtextto_get ;
      private String Ddo_requisito_descricao_Activeeventkey ;
      private String Ddo_requisito_descricao_Filteredtext_get ;
      private String Ddo_requisito_descricao_Selectedvalue_get ;
      private String Ddo_proposta_codigo_Activeeventkey ;
      private String Ddo_proposta_codigo_Filteredtext_get ;
      private String Ddo_proposta_codigo_Filteredtextto_get ;
      private String Ddo_proposta_objetivo_Activeeventkey ;
      private String Ddo_proposta_objetivo_Filteredtext_get ;
      private String Ddo_proposta_objetivo_Selectedvalue_get ;
      private String Ddo_requisito_tiporeqcod_Activeeventkey ;
      private String Ddo_requisito_tiporeqcod_Filteredtext_get ;
      private String Ddo_requisito_tiporeqcod_Filteredtextto_get ;
      private String Ddo_requisito_agrupador_Activeeventkey ;
      private String Ddo_requisito_agrupador_Filteredtext_get ;
      private String Ddo_requisito_agrupador_Selectedvalue_get ;
      private String Ddo_requisito_titulo_Activeeventkey ;
      private String Ddo_requisito_titulo_Filteredtext_get ;
      private String Ddo_requisito_titulo_Selectedvalue_get ;
      private String Ddo_requisito_restricao_Activeeventkey ;
      private String Ddo_requisito_restricao_Filteredtext_get ;
      private String Ddo_requisito_restricao_Selectedvalue_get ;
      private String Ddo_requisito_ordem_Activeeventkey ;
      private String Ddo_requisito_ordem_Filteredtext_get ;
      private String Ddo_requisito_ordem_Filteredtextto_get ;
      private String Ddo_requisito_pontuacao_Activeeventkey ;
      private String Ddo_requisito_pontuacao_Filteredtext_get ;
      private String Ddo_requisito_pontuacao_Filteredtextto_get ;
      private String Ddo_requisito_datahomologacao_Activeeventkey ;
      private String Ddo_requisito_datahomologacao_Filteredtext_get ;
      private String Ddo_requisito_datahomologacao_Filteredtextto_get ;
      private String Ddo_requisito_status_Activeeventkey ;
      private String Ddo_requisito_status_Selectedvalue_get ;
      private String Ddo_requisito_ativo_Activeeventkey ;
      private String Ddo_requisito_ativo_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV151Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_requisito_codigo_Caption ;
      private String Ddo_requisito_codigo_Tooltip ;
      private String Ddo_requisito_codigo_Cls ;
      private String Ddo_requisito_codigo_Filteredtext_set ;
      private String Ddo_requisito_codigo_Filteredtextto_set ;
      private String Ddo_requisito_codigo_Dropdownoptionstype ;
      private String Ddo_requisito_codigo_Titlecontrolidtoreplace ;
      private String Ddo_requisito_codigo_Sortedstatus ;
      private String Ddo_requisito_codigo_Filtertype ;
      private String Ddo_requisito_codigo_Sortasc ;
      private String Ddo_requisito_codigo_Sortdsc ;
      private String Ddo_requisito_codigo_Cleanfilter ;
      private String Ddo_requisito_codigo_Rangefilterfrom ;
      private String Ddo_requisito_codigo_Rangefilterto ;
      private String Ddo_requisito_codigo_Searchbuttontext ;
      private String Ddo_requisito_descricao_Caption ;
      private String Ddo_requisito_descricao_Tooltip ;
      private String Ddo_requisito_descricao_Cls ;
      private String Ddo_requisito_descricao_Filteredtext_set ;
      private String Ddo_requisito_descricao_Selectedvalue_set ;
      private String Ddo_requisito_descricao_Dropdownoptionstype ;
      private String Ddo_requisito_descricao_Titlecontrolidtoreplace ;
      private String Ddo_requisito_descricao_Sortedstatus ;
      private String Ddo_requisito_descricao_Filtertype ;
      private String Ddo_requisito_descricao_Datalisttype ;
      private String Ddo_requisito_descricao_Datalistproc ;
      private String Ddo_requisito_descricao_Sortasc ;
      private String Ddo_requisito_descricao_Sortdsc ;
      private String Ddo_requisito_descricao_Loadingdata ;
      private String Ddo_requisito_descricao_Cleanfilter ;
      private String Ddo_requisito_descricao_Noresultsfound ;
      private String Ddo_requisito_descricao_Searchbuttontext ;
      private String Ddo_proposta_codigo_Caption ;
      private String Ddo_proposta_codigo_Tooltip ;
      private String Ddo_proposta_codigo_Cls ;
      private String Ddo_proposta_codigo_Filteredtext_set ;
      private String Ddo_proposta_codigo_Filteredtextto_set ;
      private String Ddo_proposta_codigo_Dropdownoptionstype ;
      private String Ddo_proposta_codigo_Titlecontrolidtoreplace ;
      private String Ddo_proposta_codigo_Sortedstatus ;
      private String Ddo_proposta_codigo_Filtertype ;
      private String Ddo_proposta_codigo_Sortasc ;
      private String Ddo_proposta_codigo_Sortdsc ;
      private String Ddo_proposta_codigo_Cleanfilter ;
      private String Ddo_proposta_codigo_Rangefilterfrom ;
      private String Ddo_proposta_codigo_Rangefilterto ;
      private String Ddo_proposta_codigo_Searchbuttontext ;
      private String Ddo_proposta_objetivo_Caption ;
      private String Ddo_proposta_objetivo_Tooltip ;
      private String Ddo_proposta_objetivo_Cls ;
      private String Ddo_proposta_objetivo_Filteredtext_set ;
      private String Ddo_proposta_objetivo_Selectedvalue_set ;
      private String Ddo_proposta_objetivo_Dropdownoptionstype ;
      private String Ddo_proposta_objetivo_Titlecontrolidtoreplace ;
      private String Ddo_proposta_objetivo_Sortedstatus ;
      private String Ddo_proposta_objetivo_Filtertype ;
      private String Ddo_proposta_objetivo_Datalisttype ;
      private String Ddo_proposta_objetivo_Datalistproc ;
      private String Ddo_proposta_objetivo_Sortasc ;
      private String Ddo_proposta_objetivo_Sortdsc ;
      private String Ddo_proposta_objetivo_Loadingdata ;
      private String Ddo_proposta_objetivo_Cleanfilter ;
      private String Ddo_proposta_objetivo_Noresultsfound ;
      private String Ddo_proposta_objetivo_Searchbuttontext ;
      private String Ddo_requisito_tiporeqcod_Caption ;
      private String Ddo_requisito_tiporeqcod_Tooltip ;
      private String Ddo_requisito_tiporeqcod_Cls ;
      private String Ddo_requisito_tiporeqcod_Filteredtext_set ;
      private String Ddo_requisito_tiporeqcod_Filteredtextto_set ;
      private String Ddo_requisito_tiporeqcod_Dropdownoptionstype ;
      private String Ddo_requisito_tiporeqcod_Titlecontrolidtoreplace ;
      private String Ddo_requisito_tiporeqcod_Sortedstatus ;
      private String Ddo_requisito_tiporeqcod_Filtertype ;
      private String Ddo_requisito_tiporeqcod_Sortasc ;
      private String Ddo_requisito_tiporeqcod_Sortdsc ;
      private String Ddo_requisito_tiporeqcod_Cleanfilter ;
      private String Ddo_requisito_tiporeqcod_Rangefilterfrom ;
      private String Ddo_requisito_tiporeqcod_Rangefilterto ;
      private String Ddo_requisito_tiporeqcod_Searchbuttontext ;
      private String Ddo_requisito_agrupador_Caption ;
      private String Ddo_requisito_agrupador_Tooltip ;
      private String Ddo_requisito_agrupador_Cls ;
      private String Ddo_requisito_agrupador_Filteredtext_set ;
      private String Ddo_requisito_agrupador_Selectedvalue_set ;
      private String Ddo_requisito_agrupador_Dropdownoptionstype ;
      private String Ddo_requisito_agrupador_Titlecontrolidtoreplace ;
      private String Ddo_requisito_agrupador_Sortedstatus ;
      private String Ddo_requisito_agrupador_Filtertype ;
      private String Ddo_requisito_agrupador_Datalisttype ;
      private String Ddo_requisito_agrupador_Datalistproc ;
      private String Ddo_requisito_agrupador_Sortasc ;
      private String Ddo_requisito_agrupador_Sortdsc ;
      private String Ddo_requisito_agrupador_Loadingdata ;
      private String Ddo_requisito_agrupador_Cleanfilter ;
      private String Ddo_requisito_agrupador_Noresultsfound ;
      private String Ddo_requisito_agrupador_Searchbuttontext ;
      private String Ddo_requisito_titulo_Caption ;
      private String Ddo_requisito_titulo_Tooltip ;
      private String Ddo_requisito_titulo_Cls ;
      private String Ddo_requisito_titulo_Filteredtext_set ;
      private String Ddo_requisito_titulo_Selectedvalue_set ;
      private String Ddo_requisito_titulo_Dropdownoptionstype ;
      private String Ddo_requisito_titulo_Titlecontrolidtoreplace ;
      private String Ddo_requisito_titulo_Sortedstatus ;
      private String Ddo_requisito_titulo_Filtertype ;
      private String Ddo_requisito_titulo_Datalisttype ;
      private String Ddo_requisito_titulo_Datalistproc ;
      private String Ddo_requisito_titulo_Sortasc ;
      private String Ddo_requisito_titulo_Sortdsc ;
      private String Ddo_requisito_titulo_Loadingdata ;
      private String Ddo_requisito_titulo_Cleanfilter ;
      private String Ddo_requisito_titulo_Noresultsfound ;
      private String Ddo_requisito_titulo_Searchbuttontext ;
      private String Ddo_requisito_restricao_Caption ;
      private String Ddo_requisito_restricao_Tooltip ;
      private String Ddo_requisito_restricao_Cls ;
      private String Ddo_requisito_restricao_Filteredtext_set ;
      private String Ddo_requisito_restricao_Selectedvalue_set ;
      private String Ddo_requisito_restricao_Dropdownoptionstype ;
      private String Ddo_requisito_restricao_Titlecontrolidtoreplace ;
      private String Ddo_requisito_restricao_Sortedstatus ;
      private String Ddo_requisito_restricao_Filtertype ;
      private String Ddo_requisito_restricao_Datalisttype ;
      private String Ddo_requisito_restricao_Datalistproc ;
      private String Ddo_requisito_restricao_Sortasc ;
      private String Ddo_requisito_restricao_Sortdsc ;
      private String Ddo_requisito_restricao_Loadingdata ;
      private String Ddo_requisito_restricao_Cleanfilter ;
      private String Ddo_requisito_restricao_Noresultsfound ;
      private String Ddo_requisito_restricao_Searchbuttontext ;
      private String Ddo_requisito_ordem_Caption ;
      private String Ddo_requisito_ordem_Tooltip ;
      private String Ddo_requisito_ordem_Cls ;
      private String Ddo_requisito_ordem_Filteredtext_set ;
      private String Ddo_requisito_ordem_Filteredtextto_set ;
      private String Ddo_requisito_ordem_Dropdownoptionstype ;
      private String Ddo_requisito_ordem_Titlecontrolidtoreplace ;
      private String Ddo_requisito_ordem_Sortedstatus ;
      private String Ddo_requisito_ordem_Filtertype ;
      private String Ddo_requisito_ordem_Sortasc ;
      private String Ddo_requisito_ordem_Sortdsc ;
      private String Ddo_requisito_ordem_Cleanfilter ;
      private String Ddo_requisito_ordem_Rangefilterfrom ;
      private String Ddo_requisito_ordem_Rangefilterto ;
      private String Ddo_requisito_ordem_Searchbuttontext ;
      private String Ddo_requisito_pontuacao_Caption ;
      private String Ddo_requisito_pontuacao_Tooltip ;
      private String Ddo_requisito_pontuacao_Cls ;
      private String Ddo_requisito_pontuacao_Filteredtext_set ;
      private String Ddo_requisito_pontuacao_Filteredtextto_set ;
      private String Ddo_requisito_pontuacao_Dropdownoptionstype ;
      private String Ddo_requisito_pontuacao_Titlecontrolidtoreplace ;
      private String Ddo_requisito_pontuacao_Sortedstatus ;
      private String Ddo_requisito_pontuacao_Filtertype ;
      private String Ddo_requisito_pontuacao_Sortasc ;
      private String Ddo_requisito_pontuacao_Sortdsc ;
      private String Ddo_requisito_pontuacao_Cleanfilter ;
      private String Ddo_requisito_pontuacao_Rangefilterfrom ;
      private String Ddo_requisito_pontuacao_Rangefilterto ;
      private String Ddo_requisito_pontuacao_Searchbuttontext ;
      private String Ddo_requisito_datahomologacao_Caption ;
      private String Ddo_requisito_datahomologacao_Tooltip ;
      private String Ddo_requisito_datahomologacao_Cls ;
      private String Ddo_requisito_datahomologacao_Filteredtext_set ;
      private String Ddo_requisito_datahomologacao_Filteredtextto_set ;
      private String Ddo_requisito_datahomologacao_Dropdownoptionstype ;
      private String Ddo_requisito_datahomologacao_Titlecontrolidtoreplace ;
      private String Ddo_requisito_datahomologacao_Sortedstatus ;
      private String Ddo_requisito_datahomologacao_Filtertype ;
      private String Ddo_requisito_datahomologacao_Sortasc ;
      private String Ddo_requisito_datahomologacao_Sortdsc ;
      private String Ddo_requisito_datahomologacao_Cleanfilter ;
      private String Ddo_requisito_datahomologacao_Rangefilterfrom ;
      private String Ddo_requisito_datahomologacao_Rangefilterto ;
      private String Ddo_requisito_datahomologacao_Searchbuttontext ;
      private String Ddo_requisito_status_Caption ;
      private String Ddo_requisito_status_Tooltip ;
      private String Ddo_requisito_status_Cls ;
      private String Ddo_requisito_status_Selectedvalue_set ;
      private String Ddo_requisito_status_Dropdownoptionstype ;
      private String Ddo_requisito_status_Titlecontrolidtoreplace ;
      private String Ddo_requisito_status_Sortedstatus ;
      private String Ddo_requisito_status_Datalisttype ;
      private String Ddo_requisito_status_Datalistfixedvalues ;
      private String Ddo_requisito_status_Sortasc ;
      private String Ddo_requisito_status_Sortdsc ;
      private String Ddo_requisito_status_Cleanfilter ;
      private String Ddo_requisito_status_Searchbuttontext ;
      private String Ddo_requisito_ativo_Caption ;
      private String Ddo_requisito_ativo_Tooltip ;
      private String Ddo_requisito_ativo_Cls ;
      private String Ddo_requisito_ativo_Selectedvalue_set ;
      private String Ddo_requisito_ativo_Dropdownoptionstype ;
      private String Ddo_requisito_ativo_Titlecontrolidtoreplace ;
      private String Ddo_requisito_ativo_Sortedstatus ;
      private String Ddo_requisito_ativo_Datalisttype ;
      private String Ddo_requisito_ativo_Datalistfixedvalues ;
      private String Ddo_requisito_ativo_Sortasc ;
      private String Ddo_requisito_ativo_Sortdsc ;
      private String Ddo_requisito_ativo_Cleanfilter ;
      private String Ddo_requisito_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfrequisito_codigo_Internalname ;
      private String edtavTfrequisito_codigo_Jsonclick ;
      private String edtavTfrequisito_codigo_to_Internalname ;
      private String edtavTfrequisito_codigo_to_Jsonclick ;
      private String edtavTfrequisito_descricao_Internalname ;
      private String edtavTfrequisito_descricao_sel_Internalname ;
      private String edtavTfproposta_codigo_Internalname ;
      private String edtavTfproposta_codigo_Jsonclick ;
      private String edtavTfproposta_codigo_to_Internalname ;
      private String edtavTfproposta_codigo_to_Jsonclick ;
      private String edtavTfproposta_objetivo_Internalname ;
      private String edtavTfproposta_objetivo_sel_Internalname ;
      private String edtavTfrequisito_tiporeqcod_Internalname ;
      private String edtavTfrequisito_tiporeqcod_Jsonclick ;
      private String edtavTfrequisito_tiporeqcod_to_Internalname ;
      private String edtavTfrequisito_tiporeqcod_to_Jsonclick ;
      private String edtavTfrequisito_agrupador_Internalname ;
      private String edtavTfrequisito_agrupador_Jsonclick ;
      private String edtavTfrequisito_agrupador_sel_Internalname ;
      private String edtavTfrequisito_agrupador_sel_Jsonclick ;
      private String edtavTfrequisito_titulo_Internalname ;
      private String edtavTfrequisito_titulo_sel_Internalname ;
      private String edtavTfrequisito_restricao_Internalname ;
      private String edtavTfrequisito_restricao_sel_Internalname ;
      private String edtavTfrequisito_ordem_Internalname ;
      private String edtavTfrequisito_ordem_Jsonclick ;
      private String edtavTfrequisito_ordem_to_Internalname ;
      private String edtavTfrequisito_ordem_to_Jsonclick ;
      private String edtavTfrequisito_pontuacao_Internalname ;
      private String edtavTfrequisito_pontuacao_Jsonclick ;
      private String edtavTfrequisito_pontuacao_to_Internalname ;
      private String edtavTfrequisito_pontuacao_to_Jsonclick ;
      private String edtavTfrequisito_datahomologacao_Internalname ;
      private String edtavTfrequisito_datahomologacao_Jsonclick ;
      private String edtavTfrequisito_datahomologacao_to_Internalname ;
      private String edtavTfrequisito_datahomologacao_to_Jsonclick ;
      private String divDdo_requisito_datahomologacaoauxdates_Internalname ;
      private String edtavDdo_requisito_datahomologacaoauxdate_Internalname ;
      private String edtavDdo_requisito_datahomologacaoauxdate_Jsonclick ;
      private String edtavDdo_requisito_datahomologacaoauxdateto_Internalname ;
      private String edtavDdo_requisito_datahomologacaoauxdateto_Jsonclick ;
      private String edtavTfrequisito_ativo_sel_Internalname ;
      private String edtavTfrequisito_ativo_sel_Jsonclick ;
      private String edtavDdo_requisito_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_proposta_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_proposta_objetivotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_tiporeqcodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_agrupadortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_titulotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_restricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_ordemtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_pontuacaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_datahomologacaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_statustitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_requisito_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtRequisito_Codigo_Internalname ;
      private String edtRequisito_Descricao_Internalname ;
      private String edtProposta_Codigo_Internalname ;
      private String edtProposta_Objetivo_Internalname ;
      private String edtRequisito_TipoReqCod_Internalname ;
      private String edtRequisito_Agrupador_Internalname ;
      private String edtRequisito_Titulo_Internalname ;
      private String edtRequisito_Restricao_Internalname ;
      private String edtRequisito_Ordem_Internalname ;
      private String edtRequisito_Pontuacao_Internalname ;
      private String edtRequisito_DataHomologacao_Internalname ;
      private String cmbRequisito_Status_Internalname ;
      private String chkRequisito_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavRequisito_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavRequisito_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavRequisito_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_requisito_codigo_Internalname ;
      private String Ddo_requisito_descricao_Internalname ;
      private String Ddo_proposta_codigo_Internalname ;
      private String Ddo_proposta_objetivo_Internalname ;
      private String Ddo_requisito_tiporeqcod_Internalname ;
      private String Ddo_requisito_agrupador_Internalname ;
      private String Ddo_requisito_titulo_Internalname ;
      private String Ddo_requisito_restricao_Internalname ;
      private String Ddo_requisito_ordem_Internalname ;
      private String Ddo_requisito_pontuacao_Internalname ;
      private String Ddo_requisito_datahomologacao_Internalname ;
      private String Ddo_requisito_status_Internalname ;
      private String Ddo_requisito_ativo_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtRequisito_Codigo_Title ;
      private String edtRequisito_Descricao_Title ;
      private String edtProposta_Codigo_Title ;
      private String edtProposta_Objetivo_Title ;
      private String edtRequisito_TipoReqCod_Title ;
      private String edtRequisito_Agrupador_Title ;
      private String edtRequisito_Titulo_Title ;
      private String edtRequisito_Restricao_Title ;
      private String edtRequisito_Ordem_Title ;
      private String edtRequisito_Pontuacao_Title ;
      private String edtRequisito_DataHomologacao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtRequisito_Descricao_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblRequisitotitle_Internalname ;
      private String lblRequisitotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtRequisito_Codigo_Jsonclick ;
      private String edtRequisito_Descricao_Jsonclick ;
      private String edtProposta_Codigo_Jsonclick ;
      private String edtProposta_Objetivo_Jsonclick ;
      private String edtRequisito_TipoReqCod_Jsonclick ;
      private String edtRequisito_Agrupador_Jsonclick ;
      private String edtRequisito_Titulo_Jsonclick ;
      private String edtRequisito_Restricao_Jsonclick ;
      private String edtRequisito_Ordem_Jsonclick ;
      private String edtRequisito_Pontuacao_Jsonclick ;
      private String edtRequisito_DataHomologacao_Jsonclick ;
      private String cmbRequisito_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV91TFRequisito_DataHomologacao ;
      private DateTime AV92TFRequisito_DataHomologacao_To ;
      private DateTime AV93DDO_Requisito_DataHomologacaoAuxDate ;
      private DateTime AV94DDO_Requisito_DataHomologacaoAuxDateTo ;
      private DateTime A1933Requisito_DataHomologacao ;
      private DateTime AV144WWRequisitoDS_32_Tfrequisito_datahomologacao ;
      private DateTime AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_requisito_codigo_Includesortasc ;
      private bool Ddo_requisito_codigo_Includesortdsc ;
      private bool Ddo_requisito_codigo_Includefilter ;
      private bool Ddo_requisito_codigo_Filterisrange ;
      private bool Ddo_requisito_codigo_Includedatalist ;
      private bool Ddo_requisito_descricao_Includesortasc ;
      private bool Ddo_requisito_descricao_Includesortdsc ;
      private bool Ddo_requisito_descricao_Includefilter ;
      private bool Ddo_requisito_descricao_Filterisrange ;
      private bool Ddo_requisito_descricao_Includedatalist ;
      private bool Ddo_proposta_codigo_Includesortasc ;
      private bool Ddo_proposta_codigo_Includesortdsc ;
      private bool Ddo_proposta_codigo_Includefilter ;
      private bool Ddo_proposta_codigo_Filterisrange ;
      private bool Ddo_proposta_codigo_Includedatalist ;
      private bool Ddo_proposta_objetivo_Includesortasc ;
      private bool Ddo_proposta_objetivo_Includesortdsc ;
      private bool Ddo_proposta_objetivo_Includefilter ;
      private bool Ddo_proposta_objetivo_Filterisrange ;
      private bool Ddo_proposta_objetivo_Includedatalist ;
      private bool Ddo_requisito_tiporeqcod_Includesortasc ;
      private bool Ddo_requisito_tiporeqcod_Includesortdsc ;
      private bool Ddo_requisito_tiporeqcod_Includefilter ;
      private bool Ddo_requisito_tiporeqcod_Filterisrange ;
      private bool Ddo_requisito_tiporeqcod_Includedatalist ;
      private bool Ddo_requisito_agrupador_Includesortasc ;
      private bool Ddo_requisito_agrupador_Includesortdsc ;
      private bool Ddo_requisito_agrupador_Includefilter ;
      private bool Ddo_requisito_agrupador_Filterisrange ;
      private bool Ddo_requisito_agrupador_Includedatalist ;
      private bool Ddo_requisito_titulo_Includesortasc ;
      private bool Ddo_requisito_titulo_Includesortdsc ;
      private bool Ddo_requisito_titulo_Includefilter ;
      private bool Ddo_requisito_titulo_Filterisrange ;
      private bool Ddo_requisito_titulo_Includedatalist ;
      private bool Ddo_requisito_restricao_Includesortasc ;
      private bool Ddo_requisito_restricao_Includesortdsc ;
      private bool Ddo_requisito_restricao_Includefilter ;
      private bool Ddo_requisito_restricao_Filterisrange ;
      private bool Ddo_requisito_restricao_Includedatalist ;
      private bool Ddo_requisito_ordem_Includesortasc ;
      private bool Ddo_requisito_ordem_Includesortdsc ;
      private bool Ddo_requisito_ordem_Includefilter ;
      private bool Ddo_requisito_ordem_Filterisrange ;
      private bool Ddo_requisito_ordem_Includedatalist ;
      private bool Ddo_requisito_pontuacao_Includesortasc ;
      private bool Ddo_requisito_pontuacao_Includesortdsc ;
      private bool Ddo_requisito_pontuacao_Includefilter ;
      private bool Ddo_requisito_pontuacao_Filterisrange ;
      private bool Ddo_requisito_pontuacao_Includedatalist ;
      private bool Ddo_requisito_datahomologacao_Includesortasc ;
      private bool Ddo_requisito_datahomologacao_Includesortdsc ;
      private bool Ddo_requisito_datahomologacao_Includefilter ;
      private bool Ddo_requisito_datahomologacao_Filterisrange ;
      private bool Ddo_requisito_datahomologacao_Includedatalist ;
      private bool Ddo_requisito_status_Includesortasc ;
      private bool Ddo_requisito_status_Includesortdsc ;
      private bool Ddo_requisito_status_Includefilter ;
      private bool Ddo_requisito_status_Includedatalist ;
      private bool Ddo_requisito_status_Allowmultipleselection ;
      private bool Ddo_requisito_ativo_Includesortasc ;
      private bool Ddo_requisito_ativo_Includesortdsc ;
      private bool Ddo_requisito_ativo_Includefilter ;
      private bool Ddo_requisito_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1923Requisito_Descricao ;
      private bool n1685Proposta_Codigo ;
      private bool n2049Requisito_TipoReqCod ;
      private bool n1926Requisito_Agrupador ;
      private bool n1927Requisito_Titulo ;
      private bool n1929Requisito_Restricao ;
      private bool n1931Requisito_Ordem ;
      private bool n1932Requisito_Pontuacao ;
      private bool n1933Requisito_DataHomologacao ;
      private bool A1935Requisito_Ativo ;
      private bool AV116WWRequisitoDS_4_Dynamicfiltersenabled2 ;
      private bool AV120WWRequisitoDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV30Display_IsBlob ;
      private String AV17Requisito_Descricao1 ;
      private String AV21Requisito_Descricao2 ;
      private String AV25Requisito_Descricao3 ;
      private String A1923Requisito_Descricao ;
      private String A1690Proposta_Objetivo ;
      private String A1929Requisito_Restricao ;
      private String lV115WWRequisitoDS_3_Requisito_descricao1 ;
      private String lV119WWRequisitoDS_7_Requisito_descricao2 ;
      private String lV123WWRequisitoDS_11_Requisito_descricao3 ;
      private String AV115WWRequisitoDS_3_Requisito_descricao1 ;
      private String AV119WWRequisitoDS_7_Requisito_descricao2 ;
      private String AV123WWRequisitoDS_11_Requisito_descricao3 ;
      private String AV97TFRequisito_Status_SelsJson ;
      private String AV33ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV43TFRequisito_Descricao ;
      private String AV44TFRequisito_Descricao_Sel ;
      private String AV51TFProposta_Objetivo ;
      private String AV52TFProposta_Objetivo_Sel ;
      private String AV63TFRequisito_Agrupador ;
      private String AV64TFRequisito_Agrupador_Sel ;
      private String AV67TFRequisito_Titulo ;
      private String AV68TFRequisito_Titulo_Sel ;
      private String AV75TFRequisito_Restricao ;
      private String AV76TFRequisito_Restricao_Sel ;
      private String AV41ddo_Requisito_CodigoTitleControlIdToReplace ;
      private String AV45ddo_Requisito_DescricaoTitleControlIdToReplace ;
      private String AV49ddo_Proposta_CodigoTitleControlIdToReplace ;
      private String AV53ddo_Proposta_ObjetivoTitleControlIdToReplace ;
      private String AV110ddo_Requisito_TipoReqCodTitleControlIdToReplace ;
      private String AV65ddo_Requisito_AgrupadorTitleControlIdToReplace ;
      private String AV69ddo_Requisito_TituloTitleControlIdToReplace ;
      private String AV77ddo_Requisito_RestricaoTitleControlIdToReplace ;
      private String AV85ddo_Requisito_OrdemTitleControlIdToReplace ;
      private String AV89ddo_Requisito_PontuacaoTitleControlIdToReplace ;
      private String AV95ddo_Requisito_DataHomologacaoTitleControlIdToReplace ;
      private String AV99ddo_Requisito_StatusTitleControlIdToReplace ;
      private String AV102ddo_Requisito_AtivoTitleControlIdToReplace ;
      private String AV148Update_GXI ;
      private String AV149Delete_GXI ;
      private String AV150Display_GXI ;
      private String A1926Requisito_Agrupador ;
      private String A1927Requisito_Titulo ;
      private String lV126WWRequisitoDS_14_Tfrequisito_descricao ;
      private String lV130WWRequisitoDS_18_Tfproposta_objetivo ;
      private String lV134WWRequisitoDS_22_Tfrequisito_agrupador ;
      private String lV136WWRequisitoDS_24_Tfrequisito_titulo ;
      private String lV138WWRequisitoDS_26_Tfrequisito_restricao ;
      private String AV113WWRequisitoDS_1_Dynamicfiltersselector1 ;
      private String AV117WWRequisitoDS_5_Dynamicfiltersselector2 ;
      private String AV121WWRequisitoDS_9_Dynamicfiltersselector3 ;
      private String AV127WWRequisitoDS_15_Tfrequisito_descricao_sel ;
      private String AV126WWRequisitoDS_14_Tfrequisito_descricao ;
      private String AV131WWRequisitoDS_19_Tfproposta_objetivo_sel ;
      private String AV130WWRequisitoDS_18_Tfproposta_objetivo ;
      private String AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel ;
      private String AV134WWRequisitoDS_22_Tfrequisito_agrupador ;
      private String AV137WWRequisitoDS_25_Tfrequisito_titulo_sel ;
      private String AV136WWRequisitoDS_24_Tfrequisito_titulo ;
      private String AV139WWRequisitoDS_27_Tfrequisito_restricao_sel ;
      private String AV138WWRequisitoDS_26_Tfrequisito_restricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV30Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbRequisito_Status ;
      private GXCheckbox chkRequisito_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00PZ2_A1935Requisito_Ativo ;
      private short[] H00PZ2_A1934Requisito_Status ;
      private DateTime[] H00PZ2_A1933Requisito_DataHomologacao ;
      private bool[] H00PZ2_n1933Requisito_DataHomologacao ;
      private decimal[] H00PZ2_A1932Requisito_Pontuacao ;
      private bool[] H00PZ2_n1932Requisito_Pontuacao ;
      private short[] H00PZ2_A1931Requisito_Ordem ;
      private bool[] H00PZ2_n1931Requisito_Ordem ;
      private String[] H00PZ2_A1929Requisito_Restricao ;
      private bool[] H00PZ2_n1929Requisito_Restricao ;
      private String[] H00PZ2_A1927Requisito_Titulo ;
      private bool[] H00PZ2_n1927Requisito_Titulo ;
      private String[] H00PZ2_A1926Requisito_Agrupador ;
      private bool[] H00PZ2_n1926Requisito_Agrupador ;
      private int[] H00PZ2_A2049Requisito_TipoReqCod ;
      private bool[] H00PZ2_n2049Requisito_TipoReqCod ;
      private String[] H00PZ2_A1690Proposta_Objetivo ;
      private int[] H00PZ2_A1685Proposta_Codigo ;
      private bool[] H00PZ2_n1685Proposta_Codigo ;
      private String[] H00PZ2_A1923Requisito_Descricao ;
      private bool[] H00PZ2_n1923Requisito_Descricao ;
      private int[] H00PZ2_A1919Requisito_Codigo ;
      private long[] H00PZ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV98TFRequisito_Status_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV146WWRequisitoDS_34_Tfrequisito_status_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38Requisito_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42Requisito_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Proposta_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50Proposta_ObjetivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV107Requisito_TipoReqCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62Requisito_AgrupadorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66Requisito_TituloTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74Requisito_RestricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82Requisito_OrdemTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV86Requisito_PontuacaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV90Requisito_DataHomologacaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV96Requisito_StatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV100Requisito_AtivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV34ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV103DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV37ManageFiltersDataItem ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV35ManageFiltersItem ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wwrequisito__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00PZ2( IGxContext context ,
                                             short A1934Requisito_Status ,
                                             IGxCollection AV146WWRequisitoDS_34_Tfrequisito_status_sels ,
                                             String AV113WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                             short AV114WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                             String AV115WWRequisitoDS_3_Requisito_descricao1 ,
                                             bool AV116WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                             String AV117WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                             short AV118WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                             String AV119WWRequisitoDS_7_Requisito_descricao2 ,
                                             bool AV120WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                             String AV121WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                             short AV122WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                             String AV123WWRequisitoDS_11_Requisito_descricao3 ,
                                             int AV124WWRequisitoDS_12_Tfrequisito_codigo ,
                                             int AV125WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                             String AV127WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                             String AV126WWRequisitoDS_14_Tfrequisito_descricao ,
                                             int AV128WWRequisitoDS_16_Tfproposta_codigo ,
                                             int AV129WWRequisitoDS_17_Tfproposta_codigo_to ,
                                             String AV131WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                             String AV130WWRequisitoDS_18_Tfproposta_objetivo ,
                                             int AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                             int AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                             String AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                             String AV134WWRequisitoDS_22_Tfrequisito_agrupador ,
                                             String AV137WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                             String AV136WWRequisitoDS_24_Tfrequisito_titulo ,
                                             String AV139WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                             String AV138WWRequisitoDS_26_Tfrequisito_restricao ,
                                             short AV140WWRequisitoDS_28_Tfrequisito_ordem ,
                                             short AV141WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                             decimal AV142WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                             decimal AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                             DateTime AV144WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                             DateTime AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                             int AV146WWRequisitoDS_34_Tfrequisito_status_sels_Count ,
                                             short AV147WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                             String A1923Requisito_Descricao ,
                                             int A1919Requisito_Codigo ,
                                             int A1685Proposta_Codigo ,
                                             String A1690Proposta_Objetivo ,
                                             int A2049Requisito_TipoReqCod ,
                                             String A1926Requisito_Agrupador ,
                                             String A1927Requisito_Titulo ,
                                             String A1929Requisito_Restricao ,
                                             short A1931Requisito_Ordem ,
                                             decimal A1932Requisito_Pontuacao ,
                                             DateTime A1933Requisito_DataHomologacao ,
                                             bool A1935Requisito_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [33] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Requisito_Ativo], T1.[Requisito_Status], T1.[Requisito_DataHomologacao], T1.[Requisito_Pontuacao], T1.[Requisito_Ordem], T1.[Requisito_Restricao], T1.[Requisito_Titulo], T1.[Requisito_Agrupador], T1.[Requisito_TipoReqCod], T2.[Proposta_Objetivo], T1.[Proposta_Codigo], T1.[Requisito_Descricao], T1.[Requisito_Codigo]";
         sFromString = " FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = T1.[Proposta_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV113WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV114WWRequisitoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV113WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV114WWRequisitoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV116WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV118WWRequisitoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV116WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV118WWRequisitoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV120WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV121WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV122WWRequisitoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV120WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV121WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV122WWRequisitoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV124WWRequisitoDS_12_Tfrequisito_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] >= @AV124WWRequisitoDS_12_Tfrequisito_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] >= @AV124WWRequisitoDS_12_Tfrequisito_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV125WWRequisitoDS_13_Tfrequisito_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] <= @AV125WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] <= @AV125WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWRequisitoDS_14_Tfrequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV126WWRequisitoDS_14_Tfrequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV126WWRequisitoDS_14_Tfrequisito_descricao)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] = @AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] = @AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV128WWRequisitoDS_16_Tfproposta_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] >= @AV128WWRequisitoDS_16_Tfproposta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] >= @AV128WWRequisitoDS_16_Tfproposta_codigo)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV129WWRequisitoDS_17_Tfproposta_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] <= @AV129WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] <= @AV129WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWRequisitoDS_18_Tfproposta_objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] like @lV130WWRequisitoDS_18_Tfproposta_objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] like @lV130WWRequisitoDS_18_Tfproposta_objetivo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] = @AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] = @AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] >= @AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] >= @AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] <= @AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] <= @AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134WWRequisitoDS_22_Tfrequisito_agrupador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] like @lV134WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] like @lV134WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] = @AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] = @AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV136WWRequisitoDS_24_Tfrequisito_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] like @lV136WWRequisitoDS_24_Tfrequisito_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] like @lV136WWRequisitoDS_24_Tfrequisito_titulo)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] = @AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] = @AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV138WWRequisitoDS_26_Tfrequisito_restricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] like @lV138WWRequisitoDS_26_Tfrequisito_restricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] like @lV138WWRequisitoDS_26_Tfrequisito_restricao)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] = @AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] = @AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV140WWRequisitoDS_28_Tfrequisito_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] >= @AV140WWRequisitoDS_28_Tfrequisito_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] >= @AV140WWRequisitoDS_28_Tfrequisito_ordem)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV141WWRequisitoDS_29_Tfrequisito_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] <= @AV141WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] <= @AV141WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV142WWRequisitoDS_30_Tfrequisito_pontuacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] >= @AV142WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] >= @AV142WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] <= @AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] <= @AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV144WWRequisitoDS_32_Tfrequisito_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] >= @AV144WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] >= @AV144WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] <= @AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] <= @AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( AV146WWRequisitoDS_34_Tfrequisito_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV146WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV146WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
         }
         if ( AV147WWRequisitoDS_35_Tfrequisito_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 1)";
            }
         }
         if ( AV147WWRequisitoDS_35_Tfrequisito_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Proposta_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Proposta_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Proposta_Objetivo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Proposta_Objetivo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_TipoReqCod]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_TipoReqCod] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Agrupador]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Agrupador] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Titulo]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Titulo] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Restricao]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Restricao] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Ordem]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Ordem] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Pontuacao]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Pontuacao] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_DataHomologacao]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_DataHomologacao] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Status]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Status] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Ativo]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Requisito_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00PZ3( IGxContext context ,
                                             short A1934Requisito_Status ,
                                             IGxCollection AV146WWRequisitoDS_34_Tfrequisito_status_sels ,
                                             String AV113WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                             short AV114WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                             String AV115WWRequisitoDS_3_Requisito_descricao1 ,
                                             bool AV116WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                             String AV117WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                             short AV118WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                             String AV119WWRequisitoDS_7_Requisito_descricao2 ,
                                             bool AV120WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                             String AV121WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                             short AV122WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                             String AV123WWRequisitoDS_11_Requisito_descricao3 ,
                                             int AV124WWRequisitoDS_12_Tfrequisito_codigo ,
                                             int AV125WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                             String AV127WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                             String AV126WWRequisitoDS_14_Tfrequisito_descricao ,
                                             int AV128WWRequisitoDS_16_Tfproposta_codigo ,
                                             int AV129WWRequisitoDS_17_Tfproposta_codigo_to ,
                                             String AV131WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                             String AV130WWRequisitoDS_18_Tfproposta_objetivo ,
                                             int AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                             int AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                             String AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                             String AV134WWRequisitoDS_22_Tfrequisito_agrupador ,
                                             String AV137WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                             String AV136WWRequisitoDS_24_Tfrequisito_titulo ,
                                             String AV139WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                             String AV138WWRequisitoDS_26_Tfrequisito_restricao ,
                                             short AV140WWRequisitoDS_28_Tfrequisito_ordem ,
                                             short AV141WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                             decimal AV142WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                             decimal AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                             DateTime AV144WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                             DateTime AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                             int AV146WWRequisitoDS_34_Tfrequisito_status_sels_Count ,
                                             short AV147WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                             String A1923Requisito_Descricao ,
                                             int A1919Requisito_Codigo ,
                                             int A1685Proposta_Codigo ,
                                             String A1690Proposta_Objetivo ,
                                             int A2049Requisito_TipoReqCod ,
                                             String A1926Requisito_Agrupador ,
                                             String A1927Requisito_Titulo ,
                                             String A1929Requisito_Restricao ,
                                             short A1931Requisito_Ordem ,
                                             decimal A1932Requisito_Pontuacao ,
                                             DateTime A1933Requisito_DataHomologacao ,
                                             bool A1935Requisito_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [28] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = T1.[Proposta_Codigo])";
         if ( ( StringUtil.StrCmp(AV113WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV114WWRequisitoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV113WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV114WWRequisitoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV115WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV116WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV118WWRequisitoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV116WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV118WWRequisitoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV119WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV120WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV121WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV122WWRequisitoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV120WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV121WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV122WWRequisitoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV123WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV124WWRequisitoDS_12_Tfrequisito_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] >= @AV124WWRequisitoDS_12_Tfrequisito_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] >= @AV124WWRequisitoDS_12_Tfrequisito_codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV125WWRequisitoDS_13_Tfrequisito_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] <= @AV125WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] <= @AV125WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWRequisitoDS_14_Tfrequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV126WWRequisitoDS_14_Tfrequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV126WWRequisitoDS_14_Tfrequisito_descricao)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] = @AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] = @AV127WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (0==AV128WWRequisitoDS_16_Tfproposta_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] >= @AV128WWRequisitoDS_16_Tfproposta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] >= @AV128WWRequisitoDS_16_Tfproposta_codigo)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (0==AV129WWRequisitoDS_17_Tfproposta_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] <= @AV129WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] <= @AV129WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWRequisitoDS_18_Tfproposta_objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] like @lV130WWRequisitoDS_18_Tfproposta_objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] like @lV130WWRequisitoDS_18_Tfproposta_objetivo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] = @AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] = @AV131WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] >= @AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] >= @AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] <= @AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] <= @AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134WWRequisitoDS_22_Tfrequisito_agrupador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] like @lV134WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] like @lV134WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] = @AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] = @AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV136WWRequisitoDS_24_Tfrequisito_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] like @lV136WWRequisitoDS_24_Tfrequisito_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] like @lV136WWRequisitoDS_24_Tfrequisito_titulo)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] = @AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] = @AV137WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV138WWRequisitoDS_26_Tfrequisito_restricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] like @lV138WWRequisitoDS_26_Tfrequisito_restricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] like @lV138WWRequisitoDS_26_Tfrequisito_restricao)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] = @AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] = @AV139WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (0==AV140WWRequisitoDS_28_Tfrequisito_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] >= @AV140WWRequisitoDS_28_Tfrequisito_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] >= @AV140WWRequisitoDS_28_Tfrequisito_ordem)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV141WWRequisitoDS_29_Tfrequisito_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] <= @AV141WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] <= @AV141WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV142WWRequisitoDS_30_Tfrequisito_pontuacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] >= @AV142WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] >= @AV142WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] <= @AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] <= @AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV144WWRequisitoDS_32_Tfrequisito_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] >= @AV144WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] >= @AV144WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] <= @AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] <= @AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( AV146WWRequisitoDS_34_Tfrequisito_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV146WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV146WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
         }
         if ( AV147WWRequisitoDS_35_Tfrequisito_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 1)";
            }
         }
         if ( AV147WWRequisitoDS_35_Tfrequisito_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00PZ2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (short)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (DateTime)dynConstraints[47] , (bool)dynConstraints[48] , (short)dynConstraints[49] , (bool)dynConstraints[50] );
               case 1 :
                     return conditional_H00PZ3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (short)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (DateTime)dynConstraints[47] , (bool)dynConstraints[48] , (short)dynConstraints[49] , (bool)dynConstraints[50] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00PZ2 ;
          prmH00PZ2 = new Object[] {
          new Object[] {"@lV115WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV115WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV119WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV119WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV123WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV123WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV124WWRequisitoDS_12_Tfrequisito_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWRequisitoDS_13_Tfrequisito_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV126WWRequisitoDS_14_Tfrequisito_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV127WWRequisitoDS_15_Tfrequisito_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV128WWRequisitoDS_16_Tfproposta_codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@AV129WWRequisitoDS_17_Tfproposta_codigo_to",SqlDbType.Int,9,0} ,
          new Object[] {"@lV130WWRequisitoDS_18_Tfproposta_objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV131WWRequisitoDS_19_Tfproposta_objetivo_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV134WWRequisitoDS_22_Tfrequisito_agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel",SqlDbType.VarChar,25,0} ,
          new Object[] {"@lV136WWRequisitoDS_24_Tfrequisito_titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV137WWRequisitoDS_25_Tfrequisito_titulo_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@lV138WWRequisitoDS_26_Tfrequisito_restricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV139WWRequisitoDS_27_Tfrequisito_restricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV140WWRequisitoDS_28_Tfrequisito_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV141WWRequisitoDS_29_Tfrequisito_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV142WWRequisitoDS_30_Tfrequisito_pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV144WWRequisitoDS_32_Tfrequisito_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00PZ3 ;
          prmH00PZ3 = new Object[] {
          new Object[] {"@lV115WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV115WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV119WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV119WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV123WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV123WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV124WWRequisitoDS_12_Tfrequisito_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWRequisitoDS_13_Tfrequisito_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV126WWRequisitoDS_14_Tfrequisito_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV127WWRequisitoDS_15_Tfrequisito_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV128WWRequisitoDS_16_Tfproposta_codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@AV129WWRequisitoDS_17_Tfproposta_codigo_to",SqlDbType.Int,9,0} ,
          new Object[] {"@lV130WWRequisitoDS_18_Tfproposta_objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV131WWRequisitoDS_19_Tfproposta_objetivo_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV132WWRequisitoDS_20_Tfrequisito_tiporeqcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWRequisitoDS_21_Tfrequisito_tiporeqcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV134WWRequisitoDS_22_Tfrequisito_agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@AV135WWRequisitoDS_23_Tfrequisito_agrupador_sel",SqlDbType.VarChar,25,0} ,
          new Object[] {"@lV136WWRequisitoDS_24_Tfrequisito_titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV137WWRequisitoDS_25_Tfrequisito_titulo_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@lV138WWRequisitoDS_26_Tfrequisito_restricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV139WWRequisitoDS_27_Tfrequisito_restricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV140WWRequisitoDS_28_Tfrequisito_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV141WWRequisitoDS_29_Tfrequisito_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV142WWRequisitoDS_30_Tfrequisito_pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV143WWRequisitoDS_31_Tfrequisito_pontuacao_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV144WWRequisitoDS_32_Tfrequisito_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWRequisitoDS_33_Tfrequisito_datahomologacao_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00PZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00PZ2,11,0,true,false )
             ,new CursorDef("H00PZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00PZ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[55]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[56]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[57]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                return;
       }
    }

 }

}
