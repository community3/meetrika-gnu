/*
               File: SistemaModuloWC
        Description: Sistema Modulo WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:23.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemamodulowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemamodulowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemamodulowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo )
      {
         this.AV7Sistema_Codigo = aP0_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_42 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_42_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_42_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV18Modulo_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Modulo_Nome1", AV18Modulo_Nome1);
                  AV59TFModulo_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFModulo_Nome", AV59TFModulo_Nome);
                  AV60TFModulo_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFModulo_Nome_Sel", AV60TFModulo_Nome_Sel);
                  AV63TFModulo_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFModulo_Sigla", AV63TFModulo_Sigla);
                  AV64TFModulo_Sigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFModulo_Sigla_Sel", AV64TFModulo_Sigla_Sel);
                  AV7Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
                  AV61ddo_Modulo_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ddo_Modulo_NomeTitleControlIdToReplace", AV61ddo_Modulo_NomeTitleControlIdToReplace);
                  AV65ddo_Modulo_SiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65ddo_Modulo_SiglaTitleControlIdToReplace", AV65ddo_Modulo_SiglaTitleControlIdToReplace);
                  AV76Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  A146Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Modulo_Nome1, AV59TFModulo_Nome, AV60TFModulo_Nome_Sel, AV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel, AV7Sistema_Codigo, AV61ddo_Modulo_NomeTitleControlIdToReplace, AV65ddo_Modulo_SiglaTitleControlIdToReplace, AV76Pgmname, AV11GridState, A146Modulo_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "SistemaModuloWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("sistemamodulowc:[SendSecurityCheck value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA3J2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV76Pgmname = "SistemaModuloWC";
               context.Gx_err = 0;
               WS3J2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Modulo WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203269122355");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemamodulowc.aspx") + "?" + UrlEncode("" +AV7Sistema_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vMODULO_NOME1", StringUtil.RTrim( AV18Modulo_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFMODULO_NOME", StringUtil.RTrim( AV59TFModulo_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFMODULO_NOME_SEL", StringUtil.RTrim( AV60TFModulo_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFMODULO_SIGLA", StringUtil.RTrim( AV63TFModulo_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFMODULO_SIGLA_SEL", StringUtil.RTrim( AV64TFModulo_Sigla_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_42", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_42), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vMODULO_NOMETITLEFILTERDATA", AV58Modulo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vMODULO_NOMETITLEFILTERDATA", AV58Modulo_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vMODULO_SIGLATITLEFILTERDATA", AV62Modulo_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vMODULO_SIGLATITLEFILTERDATA", AV62Modulo_SiglaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV76Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Caption", StringUtil.RTrim( Ddo_modulo_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Tooltip", StringUtil.RTrim( Ddo_modulo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Cls", StringUtil.RTrim( Ddo_modulo_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_modulo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_modulo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_modulo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_modulo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_modulo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_modulo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_modulo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_modulo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Filtertype", StringUtil.RTrim( Ddo_modulo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_modulo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_modulo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Datalisttype", StringUtil.RTrim( Ddo_modulo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Datalistproc", StringUtil.RTrim( Ddo_modulo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_modulo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Sortasc", StringUtil.RTrim( Ddo_modulo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Sortdsc", StringUtil.RTrim( Ddo_modulo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Loadingdata", StringUtil.RTrim( Ddo_modulo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_modulo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_modulo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_modulo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Caption", StringUtil.RTrim( Ddo_modulo_sigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_modulo_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Cls", StringUtil.RTrim( Ddo_modulo_sigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_modulo_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_modulo_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_modulo_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_modulo_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_modulo_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_modulo_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_modulo_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_modulo_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_modulo_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_modulo_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_modulo_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_modulo_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_modulo_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_modulo_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_modulo_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_modulo_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_modulo_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_modulo_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_modulo_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_modulo_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_modulo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_modulo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_modulo_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_modulo_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_modulo_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_MODULO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_modulo_sigla_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "SistemaModuloWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("sistemamodulowc:[SendSecurityCheck value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm3J2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemamodulowc.js", "?20203269122433");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaModuloWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Modulo WC" ;
      }

      protected void WB3J0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemamodulowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_3J2( true) ;
         }
         else
         {
            wb_table1_2_3J2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3J2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSistema_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaModuloWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_nome_Internalname, StringUtil.RTrim( AV59TFModulo_Nome), StringUtil.RTrim( context.localUtil.Format( AV59TFModulo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaModuloWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_nome_sel_Internalname, StringUtil.RTrim( AV60TFModulo_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV60TFModulo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaModuloWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_sigla_Internalname, StringUtil.RTrim( AV63TFModulo_Sigla), StringUtil.RTrim( context.localUtil.Format( AV63TFModulo_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaModuloWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_sigla_sel_Internalname, StringUtil.RTrim( AV64TFModulo_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV64TFModulo_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaModuloWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_MODULO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname, AV61ddo_Modulo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", 0, edtavDdo_modulo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaModuloWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_MODULO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname, AV65ddo_Modulo_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", 0, edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaModuloWC.htm");
         }
         wbLoad = true;
      }

      protected void START3J2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Modulo WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP3J0( ) ;
            }
         }
      }

      protected void WS3J2( )
      {
         START3J2( ) ;
         EVT3J2( ) ;
      }

      protected void EVT3J2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E113J2 */
                                    E113J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MODULO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E123J2 */
                                    E123J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MODULO_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E133J2 */
                                    E133J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E143J2 */
                                    E143J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E153J2 */
                                    E153J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E163J2 */
                                    E163J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E173J2 */
                                    E173J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3J0( ) ;
                              }
                              nGXsfl_42_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
                              SubsflControlProps_422( ) ;
                              AV30Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV73Update_GXI : context.convertURL( context.PathToRelativeUrl( AV30Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV74Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV52Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV52Display)) ? AV75Display_GXI : context.convertURL( context.PathToRelativeUrl( AV52Display))));
                              A146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtModulo_Codigo_Internalname), ",", "."));
                              A143Modulo_Nome = StringUtil.Upper( cgiGet( edtModulo_Nome_Internalname));
                              A145Modulo_Sigla = StringUtil.Upper( cgiGet( edtModulo_Sigla_Internalname));
                              AV55ImagemFuncoesTRN = cgiGet( edtavImagemfuncoestrn_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavImagemfuncoestrn_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV55ImagemFuncoesTRN)) ? AV72Imagemfuncoestrn_GXI : context.convertURL( context.PathToRelativeUrl( AV55ImagemFuncoesTRN))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E183J2 */
                                          E183J2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E193J2 */
                                          E193J2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E203J2 */
                                          E203J2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Modulo_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vMODULO_NOME1"), AV18Modulo_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfmodulo_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_NOME"), AV59TFModulo_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfmodulo_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_NOME_SEL"), AV60TFModulo_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfmodulo_sigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_SIGLA"), AV63TFModulo_Sigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfmodulo_sigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_SIGLA_SEL"), AV64TFModulo_Sigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP3J0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3J2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm3J2( ) ;
            }
         }
      }

      protected void PA3J2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("MODULO_NOME", "M�dulo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_422( ) ;
         while ( nGXsfl_42_idx <= nRC_GXsfl_42 )
         {
            sendrow_422( ) ;
            nGXsfl_42_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_42_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_422( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV18Modulo_Nome1 ,
                                       String AV59TFModulo_Nome ,
                                       String AV60TFModulo_Nome_Sel ,
                                       String AV63TFModulo_Sigla ,
                                       String AV64TFModulo_Sigla_Sel ,
                                       int AV7Sistema_Codigo ,
                                       String AV61ddo_Modulo_NomeTitleControlIdToReplace ,
                                       String AV65ddo_Modulo_SiglaTitleControlIdToReplace ,
                                       String AV76Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       int A146Modulo_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3J2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MODULO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"MODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MODULO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"MODULO_NOME", StringUtil.RTrim( A143Modulo_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MODULO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A145Modulo_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"MODULO_SIGLA", StringUtil.RTrim( A145Modulo_Sigla));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3J2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV76Pgmname = "SistemaModuloWC";
         context.Gx_err = 0;
      }

      protected void RF3J2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 42;
         /* Execute user event: E193J2 */
         E193J2 ();
         nGXsfl_42_idx = 1;
         sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
         SubsflControlProps_422( ) ;
         nGXsfl_42_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_422( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV18Modulo_Nome1 ,
                                                 AV60TFModulo_Nome_Sel ,
                                                 AV59TFModulo_Nome ,
                                                 AV64TFModulo_Sigla_Sel ,
                                                 AV63TFModulo_Sigla ,
                                                 A143Modulo_Nome ,
                                                 A145Modulo_Sigla ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A127Sistema_Codigo ,
                                                 AV7Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV18Modulo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Modulo_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Modulo_Nome1", AV18Modulo_Nome1);
            lV59TFModulo_Nome = StringUtil.PadR( StringUtil.RTrim( AV59TFModulo_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFModulo_Nome", AV59TFModulo_Nome);
            lV63TFModulo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV63TFModulo_Sigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFModulo_Sigla", AV63TFModulo_Sigla);
            /* Using cursor H003J2 */
            pr_default.execute(0, new Object[] {AV7Sistema_Codigo, lV18Modulo_Nome1, lV59TFModulo_Nome, AV60TFModulo_Nome_Sel, lV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_42_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A127Sistema_Codigo = H003J2_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               A145Modulo_Sigla = H003J2_A145Modulo_Sigla[0];
               A143Modulo_Nome = H003J2_A143Modulo_Nome[0];
               A146Modulo_Codigo = H003J2_A146Modulo_Codigo[0];
               /* Execute user event: E203J2 */
               E203J2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 42;
            WB3J0( ) ;
         }
         nGXsfl_42_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV18Modulo_Nome1 ,
                                              AV60TFModulo_Nome_Sel ,
                                              AV59TFModulo_Nome ,
                                              AV64TFModulo_Sigla_Sel ,
                                              AV63TFModulo_Sigla ,
                                              A143Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A127Sistema_Codigo ,
                                              AV7Sistema_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV18Modulo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Modulo_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Modulo_Nome1", AV18Modulo_Nome1);
         lV59TFModulo_Nome = StringUtil.PadR( StringUtil.RTrim( AV59TFModulo_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFModulo_Nome", AV59TFModulo_Nome);
         lV63TFModulo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV63TFModulo_Sigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFModulo_Sigla", AV63TFModulo_Sigla);
         /* Using cursor H003J3 */
         pr_default.execute(1, new Object[] {AV7Sistema_Codigo, lV18Modulo_Nome1, lV59TFModulo_Nome, AV60TFModulo_Nome_Sel, lV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel});
         GRID_nRecordCount = H003J3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Modulo_Nome1, AV59TFModulo_Nome, AV60TFModulo_Nome_Sel, AV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel, AV7Sistema_Codigo, AV61ddo_Modulo_NomeTitleControlIdToReplace, AV65ddo_Modulo_SiglaTitleControlIdToReplace, AV76Pgmname, AV11GridState, A146Modulo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Modulo_Nome1, AV59TFModulo_Nome, AV60TFModulo_Nome_Sel, AV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel, AV7Sistema_Codigo, AV61ddo_Modulo_NomeTitleControlIdToReplace, AV65ddo_Modulo_SiglaTitleControlIdToReplace, AV76Pgmname, AV11GridState, A146Modulo_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Modulo_Nome1, AV59TFModulo_Nome, AV60TFModulo_Nome_Sel, AV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel, AV7Sistema_Codigo, AV61ddo_Modulo_NomeTitleControlIdToReplace, AV65ddo_Modulo_SiglaTitleControlIdToReplace, AV76Pgmname, AV11GridState, A146Modulo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Modulo_Nome1, AV59TFModulo_Nome, AV60TFModulo_Nome_Sel, AV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel, AV7Sistema_Codigo, AV61ddo_Modulo_NomeTitleControlIdToReplace, AV65ddo_Modulo_SiglaTitleControlIdToReplace, AV76Pgmname, AV11GridState, A146Modulo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18Modulo_Nome1, AV59TFModulo_Nome, AV60TFModulo_Nome_Sel, AV63TFModulo_Sigla, AV64TFModulo_Sigla_Sel, AV7Sistema_Codigo, AV61ddo_Modulo_NomeTitleControlIdToReplace, AV65ddo_Modulo_SiglaTitleControlIdToReplace, AV76Pgmname, AV11GridState, A146Modulo_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3J0( )
      {
         /* Before Start, stand alone formulas. */
         AV76Pgmname = "SistemaModuloWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E183J2 */
         E183J2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV66DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vMODULO_NOMETITLEFILTERDATA"), AV58Modulo_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vMODULO_SIGLATITLEFILTERDATA"), AV62Modulo_SiglaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV18Modulo_Nome1 = StringUtil.Upper( cgiGet( edtavModulo_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Modulo_Nome1", AV18Modulo_Nome1);
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            AV59TFModulo_Nome = StringUtil.Upper( cgiGet( edtavTfmodulo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFModulo_Nome", AV59TFModulo_Nome);
            AV60TFModulo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmodulo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFModulo_Nome_Sel", AV60TFModulo_Nome_Sel);
            AV63TFModulo_Sigla = StringUtil.Upper( cgiGet( edtavTfmodulo_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFModulo_Sigla", AV63TFModulo_Sigla);
            AV64TFModulo_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfmodulo_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFModulo_Sigla_Sel", AV64TFModulo_Sigla_Sel);
            AV61ddo_Modulo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ddo_Modulo_NomeTitleControlIdToReplace", AV61ddo_Modulo_NomeTitleControlIdToReplace);
            AV65ddo_Modulo_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65ddo_Modulo_SiglaTitleControlIdToReplace", AV65ddo_Modulo_SiglaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_42 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_42"), ",", "."));
            AV68GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV69GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Sistema_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_modulo_nome_Caption = cgiGet( sPrefix+"DDO_MODULO_NOME_Caption");
            Ddo_modulo_nome_Tooltip = cgiGet( sPrefix+"DDO_MODULO_NOME_Tooltip");
            Ddo_modulo_nome_Cls = cgiGet( sPrefix+"DDO_MODULO_NOME_Cls");
            Ddo_modulo_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_MODULO_NOME_Filteredtext_set");
            Ddo_modulo_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_MODULO_NOME_Selectedvalue_set");
            Ddo_modulo_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_MODULO_NOME_Dropdownoptionstype");
            Ddo_modulo_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_MODULO_NOME_Titlecontrolidtoreplace");
            Ddo_modulo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_NOME_Includesortasc"));
            Ddo_modulo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_NOME_Includesortdsc"));
            Ddo_modulo_nome_Sortedstatus = cgiGet( sPrefix+"DDO_MODULO_NOME_Sortedstatus");
            Ddo_modulo_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_NOME_Includefilter"));
            Ddo_modulo_nome_Filtertype = cgiGet( sPrefix+"DDO_MODULO_NOME_Filtertype");
            Ddo_modulo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_NOME_Filterisrange"));
            Ddo_modulo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_NOME_Includedatalist"));
            Ddo_modulo_nome_Datalisttype = cgiGet( sPrefix+"DDO_MODULO_NOME_Datalisttype");
            Ddo_modulo_nome_Datalistproc = cgiGet( sPrefix+"DDO_MODULO_NOME_Datalistproc");
            Ddo_modulo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_MODULO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_modulo_nome_Sortasc = cgiGet( sPrefix+"DDO_MODULO_NOME_Sortasc");
            Ddo_modulo_nome_Sortdsc = cgiGet( sPrefix+"DDO_MODULO_NOME_Sortdsc");
            Ddo_modulo_nome_Loadingdata = cgiGet( sPrefix+"DDO_MODULO_NOME_Loadingdata");
            Ddo_modulo_nome_Cleanfilter = cgiGet( sPrefix+"DDO_MODULO_NOME_Cleanfilter");
            Ddo_modulo_nome_Noresultsfound = cgiGet( sPrefix+"DDO_MODULO_NOME_Noresultsfound");
            Ddo_modulo_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_MODULO_NOME_Searchbuttontext");
            Ddo_modulo_sigla_Caption = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Caption");
            Ddo_modulo_sigla_Tooltip = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Tooltip");
            Ddo_modulo_sigla_Cls = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Cls");
            Ddo_modulo_sigla_Filteredtext_set = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Filteredtext_set");
            Ddo_modulo_sigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Selectedvalue_set");
            Ddo_modulo_sigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Dropdownoptionstype");
            Ddo_modulo_sigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Titlecontrolidtoreplace");
            Ddo_modulo_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_SIGLA_Includesortasc"));
            Ddo_modulo_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_SIGLA_Includesortdsc"));
            Ddo_modulo_sigla_Sortedstatus = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Sortedstatus");
            Ddo_modulo_sigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_SIGLA_Includefilter"));
            Ddo_modulo_sigla_Filtertype = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Filtertype");
            Ddo_modulo_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_SIGLA_Filterisrange"));
            Ddo_modulo_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_MODULO_SIGLA_Includedatalist"));
            Ddo_modulo_sigla_Datalisttype = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Datalisttype");
            Ddo_modulo_sigla_Datalistproc = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Datalistproc");
            Ddo_modulo_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_MODULO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_modulo_sigla_Sortasc = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Sortasc");
            Ddo_modulo_sigla_Sortdsc = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Sortdsc");
            Ddo_modulo_sigla_Loadingdata = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Loadingdata");
            Ddo_modulo_sigla_Cleanfilter = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Cleanfilter");
            Ddo_modulo_sigla_Noresultsfound = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Noresultsfound");
            Ddo_modulo_sigla_Searchbuttontext = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_modulo_nome_Activeeventkey = cgiGet( sPrefix+"DDO_MODULO_NOME_Activeeventkey");
            Ddo_modulo_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_MODULO_NOME_Filteredtext_get");
            Ddo_modulo_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_MODULO_NOME_Selectedvalue_get");
            Ddo_modulo_sigla_Activeeventkey = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Activeeventkey");
            Ddo_modulo_sigla_Filteredtext_get = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Filteredtext_get");
            Ddo_modulo_sigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_MODULO_SIGLA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "SistemaModuloWC";
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("sistemamodulowc:[SecurityCheckFailed value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vMODULO_NOME1"), AV18Modulo_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_NOME"), AV59TFModulo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_NOME_SEL"), AV60TFModulo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_SIGLA"), AV63TFModulo_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFMODULO_SIGLA_SEL"), AV64TFModulo_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E183J2 */
         E183J2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E183J2( )
      {
         /* Start Routine */
         subGrid_Rows = 50;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV16DynamicFiltersSelector1 = "MODULO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTfmodulo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfmodulo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_nome_Visible), 5, 0)));
         edtavTfmodulo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfmodulo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_nome_sel_Visible), 5, 0)));
         edtavTfmodulo_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfmodulo_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_sigla_Visible), 5, 0)));
         edtavTfmodulo_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfmodulo_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_sigla_sel_Visible), 5, 0)));
         Ddo_modulo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Modulo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "TitleControlIdToReplace", Ddo_modulo_nome_Titlecontrolidtoreplace);
         AV61ddo_Modulo_NomeTitleControlIdToReplace = Ddo_modulo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ddo_Modulo_NomeTitleControlIdToReplace", AV61ddo_Modulo_NomeTitleControlIdToReplace);
         edtavDdo_modulo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_modulo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_modulo_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Modulo_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "TitleControlIdToReplace", Ddo_modulo_sigla_Titlecontrolidtoreplace);
         AV65ddo_Modulo_SiglaTitleControlIdToReplace = Ddo_modulo_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65ddo_Modulo_SiglaTitleControlIdToReplace", AV65ddo_Modulo_SiglaTitleControlIdToReplace);
         edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         edtSistema_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "M�dulo", 0);
         cmbavOrderedby.addItem("2", "Sigla", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV66DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV66DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E193J2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV58Modulo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Modulo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtModulo_Nome_Titleformat = 2;
         edtModulo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "M�dulo", AV61ddo_Modulo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtModulo_Nome_Internalname, "Title", edtModulo_Nome_Title);
         edtModulo_Sigla_Titleformat = 2;
         edtModulo_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV65ddo_Modulo_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtModulo_Sigla_Internalname, "Title", edtModulo_Sigla_Title);
         AV68GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68GridCurrentPage), 10, 0)));
         AV69GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV58Modulo_NomeTitleFilterData", AV58Modulo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV62Modulo_SiglaTitleFilterData", AV62Modulo_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E113J2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV67PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV67PageToGo) ;
         }
      }

      protected void E123J2( )
      {
         /* Ddo_modulo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_modulo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_modulo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_modulo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFModulo_Nome = Ddo_modulo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFModulo_Nome", AV59TFModulo_Nome);
            AV60TFModulo_Nome_Sel = Ddo_modulo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFModulo_Nome_Sel", AV60TFModulo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E133J2( )
      {
         /* Ddo_modulo_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_modulo_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_modulo_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_modulo_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFModulo_Sigla = Ddo_modulo_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFModulo_Sigla", AV63TFModulo_Sigla);
            AV64TFModulo_Sigla_Sel = Ddo_modulo_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFModulo_Sigla_Sel", AV64TFModulo_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E203J2( )
      {
         /* Grid_Load Routine */
         AV55ImagemFuncoesTRN = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavImagemfuncoestrn_Internalname, AV55ImagemFuncoesTRN);
         AV72Imagemfuncoestrn_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         AV30Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV30Update);
         AV73Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("modulo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode("" +AV7Sistema_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
         AV74Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("modulo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode("" +AV7Sistema_Codigo);
         AV52Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV52Display);
         AV75Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewmodulo.aspx") + "?" + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV30Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV30Update);
         AV73Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
         AV74Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         AV52Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV52Display);
         AV75Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 42;
         }
         sendrow_422( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_42_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(42, GridRow);
         }
      }

      protected void E143J2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E173J2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E153J2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E163J2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("modulo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7Sistema_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_modulo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
         Ddo_modulo_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_modulo_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_modulo_sigla_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavModulo_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavModulo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavModulo_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MODULO_NOME") == 0 )
         {
            edtavModulo_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavModulo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavModulo_nome1_Visible), 5, 0)));
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV59TFModulo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFModulo_Nome", AV59TFModulo_Nome);
         Ddo_modulo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "FilteredText_set", Ddo_modulo_nome_Filteredtext_set);
         AV60TFModulo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFModulo_Nome_Sel", AV60TFModulo_Nome_Sel);
         Ddo_modulo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "SelectedValue_set", Ddo_modulo_nome_Selectedvalue_set);
         AV63TFModulo_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFModulo_Sigla", AV63TFModulo_Sigla);
         Ddo_modulo_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "FilteredText_set", Ddo_modulo_sigla_Filteredtext_set);
         AV64TFModulo_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFModulo_Sigla_Sel", AV64TFModulo_Sigla_Sel);
         Ddo_modulo_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "SelectedValue_set", Ddo_modulo_sigla_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "MODULO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV18Modulo_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Modulo_Nome1", AV18Modulo_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV76Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV76Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV76Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV77GXV1 = 1;
         while ( AV77GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV77GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME") == 0 )
            {
               AV59TFModulo_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFModulo_Nome", AV59TFModulo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFModulo_Nome)) )
               {
                  Ddo_modulo_nome_Filteredtext_set = AV59TFModulo_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "FilteredText_set", Ddo_modulo_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME_SEL") == 0 )
            {
               AV60TFModulo_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFModulo_Nome_Sel", AV60TFModulo_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFModulo_Nome_Sel)) )
               {
                  Ddo_modulo_nome_Selectedvalue_set = AV60TFModulo_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_nome_Internalname, "SelectedValue_set", Ddo_modulo_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA") == 0 )
            {
               AV63TFModulo_Sigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFModulo_Sigla", AV63TFModulo_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFModulo_Sigla)) )
               {
                  Ddo_modulo_sigla_Filteredtext_set = AV63TFModulo_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "FilteredText_set", Ddo_modulo_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA_SEL") == 0 )
            {
               AV64TFModulo_Sigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFModulo_Sigla_Sel", AV64TFModulo_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFModulo_Sigla_Sel)) )
               {
                  Ddo_modulo_sigla_Selectedvalue_set = AV64TFModulo_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_modulo_sigla_Internalname, "SelectedValue_set", Ddo_modulo_sigla_Selectedvalue_set);
               }
            }
            AV77GXV1 = (int)(AV77GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MODULO_NOME") == 0 )
            {
               AV18Modulo_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Modulo_Nome1", AV18Modulo_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV76Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFModulo_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFMODULO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV59TFModulo_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFModulo_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFMODULO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV60TFModulo_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFModulo_Sigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFMODULO_SIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV63TFModulo_Sigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFModulo_Sigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFMODULO_SIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV64TFModulo_Sigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Sistema_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&SISTEMA_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV76Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MODULO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Modulo_Nome1)) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = AV18Modulo_Nome1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV76Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Modulo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Sistema_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_3J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_3J2( true) ;
         }
         else
         {
            wb_table2_8_3J2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_39_3J2( true) ;
         }
         else
         {
            wb_table3_39_3J2( false) ;
         }
         return  ;
      }

      protected void wb_table3_39_3J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3J2e( true) ;
         }
         else
         {
            wb_table1_2_3J2e( false) ;
         }
      }

      protected void wb_table3_39_3J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"42\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtModulo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtModulo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtModulo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtModulo_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtModulo_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtModulo_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fun. de Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV52Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A143Modulo_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtModulo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtModulo_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A145Modulo_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtModulo_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtModulo_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV55ImagemFuncoesTRN));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 42 )
         {
            wbEnd = 0;
            nRC_GXsfl_42 = (short)(nGXsfl_42_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_3J2e( true) ;
         }
         else
         {
            wb_table3_39_3J2e( false) ;
         }
      }

      protected void wb_table2_8_3J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_3J2( true) ;
         }
         else
         {
            wb_table4_11_3J2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_3J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_SistemaModuloWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_SistemaModuloWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SistemaModuloWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_3J2( true) ;
         }
         else
         {
            wb_table5_21_3J2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_3J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3J2e( true) ;
         }
         else
         {
            wb_table2_8_3J2e( false) ;
         }
      }

      protected void wb_table5_21_3J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaModuloWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_26_3J2( true) ;
         }
         else
         {
            wb_table6_26_3J2( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_3J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_3J2e( true) ;
         }
         else
         {
            wb_table5_21_3J2e( false) ;
         }
      }

      protected void wb_table6_26_3J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaModuloWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_SistemaModuloWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaModuloWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavModulo_nome1_Internalname, StringUtil.RTrim( AV18Modulo_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Modulo_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavModulo_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavModulo_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaModuloWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_3J2e( true) ;
         }
         else
         {
            wb_table6_26_3J2e( false) ;
         }
      }

      protected void wb_table4_11_3J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaModuloWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_3J2e( true) ;
         }
         else
         {
            wb_table4_11_3J2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3J2( ) ;
         WS3J2( ) ;
         WE3J2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Sistema_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA3J2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemamodulowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA3J2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Sistema_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
         }
         wcpOAV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Sistema_Codigo != wcpOAV7Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Sistema_Codigo = AV7Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Sistema_Codigo = cgiGet( sPrefix+"AV7Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Sistema_Codigo) > 0 )
         {
            AV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Sistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
         }
         else
         {
            AV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Sistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA3J2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS3J2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS3J2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE3J2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020326912274");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemamodulowc.js", "?2020326912274");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_422( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_42_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_42_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_42_idx;
         edtModulo_Codigo_Internalname = sPrefix+"MODULO_CODIGO_"+sGXsfl_42_idx;
         edtModulo_Nome_Internalname = sPrefix+"MODULO_NOME_"+sGXsfl_42_idx;
         edtModulo_Sigla_Internalname = sPrefix+"MODULO_SIGLA_"+sGXsfl_42_idx;
         edtavImagemfuncoestrn_Internalname = sPrefix+"vIMAGEMFUNCOESTRN_"+sGXsfl_42_idx;
      }

      protected void SubsflControlProps_fel_422( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_42_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_42_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_42_fel_idx;
         edtModulo_Codigo_Internalname = sPrefix+"MODULO_CODIGO_"+sGXsfl_42_fel_idx;
         edtModulo_Nome_Internalname = sPrefix+"MODULO_NOME_"+sGXsfl_42_fel_idx;
         edtModulo_Sigla_Internalname = sPrefix+"MODULO_SIGLA_"+sGXsfl_42_fel_idx;
         edtavImagemfuncoestrn_Internalname = sPrefix+"vIMAGEMFUNCOESTRN_"+sGXsfl_42_fel_idx;
      }

      protected void sendrow_422( )
      {
         SubsflControlProps_422( ) ;
         WB3J0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_42_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_42_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_42_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV73Update_GXI : context.PathToRelativeUrl( AV30Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV74Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV74Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV52Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV52Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV75Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV52Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV52Display)) ? AV75Display_GXI : context.PathToRelativeUrl( AV52Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV52Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtModulo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtModulo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtModulo_Nome_Internalname,StringUtil.RTrim( A143Modulo_Nome),StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtModulo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtModulo_Sigla_Internalname,StringUtil.RTrim( A145Modulo_Sigla),StringUtil.RTrim( context.localUtil.Format( A145Modulo_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtModulo_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavImagemfuncoestrn_Enabled!=0)&&(edtavImagemfuncoestrn_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 49,'"+sPrefix+"',false,'',42)\"" : " ");
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV55ImagemFuncoesTRN_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV55ImagemFuncoesTRN))&&String.IsNullOrEmpty(StringUtil.RTrim( AV72Imagemfuncoestrn_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV55ImagemFuncoesTRN)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavImagemfuncoestrn_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV55ImagemFuncoesTRN)) ? AV72Imagemfuncoestrn_GXI : context.PathToRelativeUrl( AV55ImagemFuncoesTRN)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavImagemfuncoestrn_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e213j2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV55ImagemFuncoesTRN_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MODULO_CODIGO"+"_"+sGXsfl_42_idx, GetSecureSignedToken( sPrefix+sGXsfl_42_idx, context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MODULO_NOME"+"_"+sGXsfl_42_idx, GetSecureSignedToken( sPrefix+sGXsfl_42_idx, StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MODULO_SIGLA"+"_"+sGXsfl_42_idx, GetSecureSignedToken( sPrefix+sGXsfl_42_idx, StringUtil.RTrim( context.localUtil.Format( A145Modulo_Sigla, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_42_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_42_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_422( ) ;
         }
         /* End function sendrow_422 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavModulo_nome1_Internalname = sPrefix+"vMODULO_NOME1";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtModulo_Codigo_Internalname = sPrefix+"MODULO_CODIGO";
         edtModulo_Nome_Internalname = sPrefix+"MODULO_NOME";
         edtModulo_Sigla_Internalname = sPrefix+"MODULO_SIGLA";
         edtavImagemfuncoestrn_Internalname = sPrefix+"vIMAGEMFUNCOESTRN";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtSistema_Codigo_Internalname = sPrefix+"SISTEMA_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavTfmodulo_nome_Internalname = sPrefix+"vTFMODULO_NOME";
         edtavTfmodulo_nome_sel_Internalname = sPrefix+"vTFMODULO_NOME_SEL";
         edtavTfmodulo_sigla_Internalname = sPrefix+"vTFMODULO_SIGLA";
         edtavTfmodulo_sigla_sel_Internalname = sPrefix+"vTFMODULO_SIGLA_SEL";
         Ddo_modulo_nome_Internalname = sPrefix+"DDO_MODULO_NOME";
         edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_modulo_sigla_Internalname = sPrefix+"DDO_MODULO_SIGLA";
         edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavImagemfuncoestrn_Jsonclick = "";
         edtavImagemfuncoestrn_Visible = -1;
         edtavImagemfuncoestrn_Enabled = 1;
         edtModulo_Sigla_Jsonclick = "";
         edtModulo_Nome_Jsonclick = "";
         edtModulo_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavModulo_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtModulo_Sigla_Titleformat = 0;
         edtModulo_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavModulo_nome1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtModulo_Sigla_Title = "Sigla";
         edtModulo_Nome_Title = "M�dulo";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_modulo_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfmodulo_sigla_sel_Jsonclick = "";
         edtavTfmodulo_sigla_sel_Visible = 1;
         edtavTfmodulo_sigla_Jsonclick = "";
         edtavTfmodulo_sigla_Visible = 1;
         edtavTfmodulo_nome_sel_Jsonclick = "";
         edtavTfmodulo_nome_sel_Visible = 1;
         edtavTfmodulo_nome_Jsonclick = "";
         edtavTfmodulo_nome_Visible = 1;
         edtSistema_Codigo_Jsonclick = "";
         edtSistema_Codigo_Visible = 1;
         Ddo_modulo_sigla_Searchbuttontext = "Pesquisar";
         Ddo_modulo_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_modulo_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_modulo_sigla_Loadingdata = "Carregando dados...";
         Ddo_modulo_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_modulo_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_modulo_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_modulo_sigla_Datalistproc = "GetSistemaModuloWCFilterData";
         Ddo_modulo_sigla_Datalisttype = "Dynamic";
         Ddo_modulo_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_modulo_sigla_Filtertype = "Character";
         Ddo_modulo_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Titlecontrolidtoreplace = "";
         Ddo_modulo_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_modulo_sigla_Cls = "ColumnSettings";
         Ddo_modulo_sigla_Tooltip = "Op��es";
         Ddo_modulo_sigla_Caption = "";
         Ddo_modulo_nome_Searchbuttontext = "Pesquisar";
         Ddo_modulo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_modulo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_modulo_nome_Loadingdata = "Carregando dados...";
         Ddo_modulo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_modulo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_modulo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_modulo_nome_Datalistproc = "GetSistemaModuloWCFilterData";
         Ddo_modulo_nome_Datalisttype = "Dynamic";
         Ddo_modulo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_modulo_nome_Filtertype = "Character";
         Ddo_modulo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Titlecontrolidtoreplace = "";
         Ddo_modulo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_modulo_nome_Cls = "ColumnSettings";
         Ddo_modulo_nome_Tooltip = "Op��es";
         Ddo_modulo_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV61ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''}],oparms:[{av:'AV58Modulo_NomeTitleFilterData',fld:'vMODULO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV62Modulo_SiglaTitleFilterData',fld:'vMODULO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtModulo_Nome_Titleformat',ctrl:'MODULO_NOME',prop:'Titleformat'},{av:'edtModulo_Nome_Title',ctrl:'MODULO_NOME',prop:'Title'},{av:'edtModulo_Sigla_Titleformat',ctrl:'MODULO_SIGLA',prop:'Titleformat'},{av:'edtModulo_Sigla_Title',ctrl:'MODULO_SIGLA',prop:'Title'},{av:'AV68GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV69GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E113J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_MODULO_NOME.ONOPTIONCLICKED","{handler:'E123J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_modulo_nome_Activeeventkey',ctrl:'DDO_MODULO_NOME',prop:'ActiveEventKey'},{av:'Ddo_modulo_nome_Filteredtext_get',ctrl:'DDO_MODULO_NOME',prop:'FilteredText_get'},{av:'Ddo_modulo_nome_Selectedvalue_get',ctrl:'DDO_MODULO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_modulo_nome_Sortedstatus',ctrl:'DDO_MODULO_NOME',prop:'SortedStatus'},{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_sigla_Sortedstatus',ctrl:'DDO_MODULO_SIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MODULO_SIGLA.ONOPTIONCLICKED","{handler:'E133J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_modulo_sigla_Activeeventkey',ctrl:'DDO_MODULO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_modulo_sigla_Filteredtext_get',ctrl:'DDO_MODULO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_modulo_sigla_Selectedvalue_get',ctrl:'DDO_MODULO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_modulo_sigla_Sortedstatus',ctrl:'DDO_MODULO_SIGLA',prop:'SortedStatus'},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_nome_Sortedstatus',ctrl:'DDO_MODULO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E203J2',iparms:[{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV55ImagemFuncoesTRN',fld:'vIMAGEMFUNCOESTRN',pic:'',nv:''},{av:'AV30Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV52Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E143J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E173J2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavModulo_nome1_Visible',ctrl:'vMODULO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E153J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV59TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'Ddo_modulo_nome_Filteredtext_set',ctrl:'DDO_MODULO_NOME',prop:'FilteredText_set'},{av:'AV60TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_nome_Selectedvalue_set',ctrl:'DDO_MODULO_NOME',prop:'SelectedValue_set'},{av:'AV63TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'Ddo_modulo_sigla_Filteredtext_set',ctrl:'DDO_MODULO_SIGLA',prop:'FilteredText_set'},{av:'AV64TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_sigla_Selectedvalue_set',ctrl:'DDO_MODULO_SIGLA',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavModulo_nome1_Visible',ctrl:'vMODULO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E163J2',iparms:[{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VIMAGEMFUNCOESTRN.CLICK","{handler:'E213J2',iparms:[{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_modulo_nome_Activeeventkey = "";
         Ddo_modulo_nome_Filteredtext_get = "";
         Ddo_modulo_nome_Selectedvalue_get = "";
         Ddo_modulo_sigla_Activeeventkey = "";
         Ddo_modulo_sigla_Filteredtext_get = "";
         Ddo_modulo_sigla_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Modulo_Nome1 = "";
         AV59TFModulo_Nome = "";
         AV60TFModulo_Nome_Sel = "";
         AV63TFModulo_Sigla = "";
         AV64TFModulo_Sigla_Sel = "";
         AV61ddo_Modulo_NomeTitleControlIdToReplace = "";
         AV65ddo_Modulo_SiglaTitleControlIdToReplace = "";
         AV76Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV66DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV58Modulo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Modulo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_modulo_nome_Filteredtext_set = "";
         Ddo_modulo_nome_Selectedvalue_set = "";
         Ddo_modulo_nome_Sortedstatus = "";
         Ddo_modulo_sigla_Filteredtext_set = "";
         Ddo_modulo_sigla_Selectedvalue_set = "";
         Ddo_modulo_sigla_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30Update = "";
         AV73Update_GXI = "";
         AV29Delete = "";
         AV74Delete_GXI = "";
         AV52Display = "";
         AV75Display_GXI = "";
         A143Modulo_Nome = "";
         A145Modulo_Sigla = "";
         AV55ImagemFuncoesTRN = "";
         AV72Imagemfuncoestrn_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Modulo_Nome1 = "";
         lV59TFModulo_Nome = "";
         lV63TFModulo_Sigla = "";
         H003J2_A127Sistema_Codigo = new int[1] ;
         H003J2_A145Modulo_Sigla = new String[] {""} ;
         H003J2_A143Modulo_Nome = new String[] {""} ;
         H003J2_A146Modulo_Codigo = new int[1] ;
         H003J3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV31Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Sistema_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemamodulowc__default(),
            new Object[][] {
                new Object[] {
               H003J2_A127Sistema_Codigo, H003J2_A145Modulo_Sigla, H003J2_A143Modulo_Nome, H003J2_A146Modulo_Codigo
               }
               , new Object[] {
               H003J3_AGRID_nRecordCount
               }
            }
         );
         AV76Pgmname = "SistemaModuloWC";
         /* GeneXus formulas. */
         AV76Pgmname = "SistemaModuloWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_42 ;
      private short nGXsfl_42_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_42_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtModulo_Nome_Titleformat ;
      private short edtModulo_Sigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Sistema_Codigo ;
      private int wcpOAV7Sistema_Codigo ;
      private int subGrid_Rows ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_modulo_nome_Datalistupdateminimumcharacters ;
      private int Ddo_modulo_sigla_Datalistupdateminimumcharacters ;
      private int edtSistema_Codigo_Visible ;
      private int edtavTfmodulo_nome_Visible ;
      private int edtavTfmodulo_nome_sel_Visible ;
      private int edtavTfmodulo_sigla_Visible ;
      private int edtavTfmodulo_sigla_sel_Visible ;
      private int edtavDdo_modulo_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV67PageToGo ;
      private int edtavModulo_nome1_Visible ;
      private int AV77GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavImagemfuncoestrn_Enabled ;
      private int edtavImagemfuncoestrn_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV68GridCurrentPage ;
      private long AV69GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_modulo_nome_Activeeventkey ;
      private String Ddo_modulo_nome_Filteredtext_get ;
      private String Ddo_modulo_nome_Selectedvalue_get ;
      private String Ddo_modulo_sigla_Activeeventkey ;
      private String Ddo_modulo_sigla_Filteredtext_get ;
      private String Ddo_modulo_sigla_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_42_idx="0001" ;
      private String AV18Modulo_Nome1 ;
      private String AV59TFModulo_Nome ;
      private String AV60TFModulo_Nome_Sel ;
      private String AV63TFModulo_Sigla ;
      private String AV64TFModulo_Sigla_Sel ;
      private String AV76Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_modulo_nome_Caption ;
      private String Ddo_modulo_nome_Tooltip ;
      private String Ddo_modulo_nome_Cls ;
      private String Ddo_modulo_nome_Filteredtext_set ;
      private String Ddo_modulo_nome_Selectedvalue_set ;
      private String Ddo_modulo_nome_Dropdownoptionstype ;
      private String Ddo_modulo_nome_Titlecontrolidtoreplace ;
      private String Ddo_modulo_nome_Sortedstatus ;
      private String Ddo_modulo_nome_Filtertype ;
      private String Ddo_modulo_nome_Datalisttype ;
      private String Ddo_modulo_nome_Datalistproc ;
      private String Ddo_modulo_nome_Sortasc ;
      private String Ddo_modulo_nome_Sortdsc ;
      private String Ddo_modulo_nome_Loadingdata ;
      private String Ddo_modulo_nome_Cleanfilter ;
      private String Ddo_modulo_nome_Noresultsfound ;
      private String Ddo_modulo_nome_Searchbuttontext ;
      private String Ddo_modulo_sigla_Caption ;
      private String Ddo_modulo_sigla_Tooltip ;
      private String Ddo_modulo_sigla_Cls ;
      private String Ddo_modulo_sigla_Filteredtext_set ;
      private String Ddo_modulo_sigla_Selectedvalue_set ;
      private String Ddo_modulo_sigla_Dropdownoptionstype ;
      private String Ddo_modulo_sigla_Titlecontrolidtoreplace ;
      private String Ddo_modulo_sigla_Sortedstatus ;
      private String Ddo_modulo_sigla_Filtertype ;
      private String Ddo_modulo_sigla_Datalisttype ;
      private String Ddo_modulo_sigla_Datalistproc ;
      private String Ddo_modulo_sigla_Sortasc ;
      private String Ddo_modulo_sigla_Sortdsc ;
      private String Ddo_modulo_sigla_Loadingdata ;
      private String Ddo_modulo_sigla_Cleanfilter ;
      private String Ddo_modulo_sigla_Noresultsfound ;
      private String Ddo_modulo_sigla_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavTfmodulo_nome_Internalname ;
      private String edtavTfmodulo_nome_Jsonclick ;
      private String edtavTfmodulo_nome_sel_Internalname ;
      private String edtavTfmodulo_nome_sel_Jsonclick ;
      private String edtavTfmodulo_sigla_Internalname ;
      private String edtavTfmodulo_sigla_Jsonclick ;
      private String edtavTfmodulo_sigla_sel_Internalname ;
      private String edtavTfmodulo_sigla_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtModulo_Codigo_Internalname ;
      private String A143Modulo_Nome ;
      private String edtModulo_Nome_Internalname ;
      private String A145Modulo_Sigla ;
      private String edtModulo_Sigla_Internalname ;
      private String edtavImagemfuncoestrn_Internalname ;
      private String scmdbuf ;
      private String lV18Modulo_Nome1 ;
      private String lV59TFModulo_Nome ;
      private String lV63TFModulo_Sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavModulo_nome1_Internalname ;
      private String hsh ;
      private String subGrid_Internalname ;
      private String Ddo_modulo_nome_Internalname ;
      private String Ddo_modulo_sigla_Internalname ;
      private String edtModulo_Nome_Title ;
      private String edtModulo_Sigla_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavModulo_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Sistema_Codigo ;
      private String sGXsfl_42_fel_idx="0001" ;
      private String ROClassString ;
      private String edtModulo_Codigo_Jsonclick ;
      private String edtModulo_Nome_Jsonclick ;
      private String edtModulo_Sigla_Jsonclick ;
      private String edtavImagemfuncoestrn_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_modulo_nome_Includesortasc ;
      private bool Ddo_modulo_nome_Includesortdsc ;
      private bool Ddo_modulo_nome_Includefilter ;
      private bool Ddo_modulo_nome_Filterisrange ;
      private bool Ddo_modulo_nome_Includedatalist ;
      private bool Ddo_modulo_sigla_Includesortasc ;
      private bool Ddo_modulo_sigla_Includesortdsc ;
      private bool Ddo_modulo_sigla_Includefilter ;
      private bool Ddo_modulo_sigla_Filterisrange ;
      private bool Ddo_modulo_sigla_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV30Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV52Display_IsBlob ;
      private bool AV55ImagemFuncoesTRN_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV61ddo_Modulo_NomeTitleControlIdToReplace ;
      private String AV65ddo_Modulo_SiglaTitleControlIdToReplace ;
      private String AV73Update_GXI ;
      private String AV74Delete_GXI ;
      private String AV75Display_GXI ;
      private String AV72Imagemfuncoestrn_GXI ;
      private String AV30Update ;
      private String AV29Delete ;
      private String AV52Display ;
      private String AV55ImagemFuncoesTRN ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private IDataStoreProvider pr_default ;
      private int[] H003J2_A127Sistema_Codigo ;
      private String[] H003J2_A145Modulo_Sigla ;
      private String[] H003J2_A143Modulo_Nome ;
      private int[] H003J2_A146Modulo_Codigo ;
      private long[] H003J3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58Modulo_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62Modulo_SiglaTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV66DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class sistemamodulowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003J2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV18Modulo_Nome1 ,
                                             String AV60TFModulo_Nome_Sel ,
                                             String AV59TFModulo_Nome ,
                                             String AV64TFModulo_Sigla_Sel ,
                                             String AV63TFModulo_Sigla ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A127Sistema_Codigo ,
                                             int AV7Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Sistema_Codigo], [Modulo_Sigla], [Modulo_Nome], [Modulo_Codigo]";
         sFromString = " FROM [Modulo] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Sistema_Codigo] = @AV7Sistema_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Modulo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like '%' + @lV18Modulo_Nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFModulo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFModulo_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like @lV59TFModulo_Nome)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFModulo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] = @AV60TFModulo_Nome_Sel)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFModulo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFModulo_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] like @lV63TFModulo_Sigla)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFModulo_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] = @AV64TFModulo_Sigla_Sel)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Sistema_Codigo], [Modulo_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Sistema_Codigo] DESC, [Modulo_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Sistema_Codigo], [Modulo_Sigla]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Sistema_Codigo] DESC, [Modulo_Sigla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Modulo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H003J3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV18Modulo_Nome1 ,
                                             String AV60TFModulo_Nome_Sel ,
                                             String AV59TFModulo_Nome ,
                                             String AV64TFModulo_Sigla_Sel ,
                                             String AV63TFModulo_Sigla ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A127Sistema_Codigo ,
                                             int AV7Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Modulo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Sistema_Codigo] = @AV7Sistema_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Modulo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like '%' + @lV18Modulo_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFModulo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFModulo_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like @lV59TFModulo_Nome)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFModulo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] = @AV60TFModulo_Nome_Sel)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFModulo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFModulo_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] like @lV63TFModulo_Sigla)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFModulo_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] = @AV64TFModulo_Sigla_Sel)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H003J2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (bool)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
               case 1 :
                     return conditional_H003J3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (bool)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003J2 ;
          prmH003J2 = new Object[] {
          new Object[] {"@AV7Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Modulo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59TFModulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60TFModulo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63TFModulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV64TFModulo_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH003J3 ;
          prmH003J3 = new Object[] {
          new Object[] {"@AV7Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Modulo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59TFModulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60TFModulo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63TFModulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV64TFModulo_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003J2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003J2,11,0,true,false )
             ,new CursorDef("H003J3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003J3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
