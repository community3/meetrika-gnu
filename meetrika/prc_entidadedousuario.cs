/*
               File: PRC_EntidadeDoUsuario
        Description: Entidade Do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:44.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_entidadedousuario : GXProcedure
   {
      public prc_entidadedousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_entidadedousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           int aP1_Area_Codigo ,
                           out String aP2_Nome )
      {
         this.AV10Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV13Area_Codigo = aP1_Area_Codigo;
         this.AV8Nome = "" ;
         initialize();
         executePrivate();
         aP2_Nome=this.AV8Nome;
      }

      public String executeUdp( int aP0_Usuario_Codigo ,
                                int aP1_Area_Codigo )
      {
         this.AV10Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV13Area_Codigo = aP1_Area_Codigo;
         this.AV8Nome = "" ;
         initialize();
         executePrivate();
         aP2_Nome=this.AV8Nome;
         return AV8Nome ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 int aP1_Area_Codigo ,
                                 out String aP2_Nome )
      {
         prc_entidadedousuario objprc_entidadedousuario;
         objprc_entidadedousuario = new prc_entidadedousuario();
         objprc_entidadedousuario.AV10Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_entidadedousuario.AV13Area_Codigo = aP1_Area_Codigo;
         objprc_entidadedousuario.AV8Nome = "" ;
         objprc_entidadedousuario.context.SetSubmitInitialConfig(context);
         objprc_entidadedousuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_entidadedousuario);
         aP2_Nome=this.AV8Nome;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_entidadedousuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (0==AV13Area_Codigo) )
         {
            AV12AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV14WebSession.Get("AreaDeTrabalho"), "."));
            if ( (0==AV12AreaTrabalho_Codigo) )
            {
               new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV11WWPContext) ;
               AV12AreaTrabalho_Codigo = AV11WWPContext.gxTpr_Areatrabalho_codigo;
            }
         }
         else
         {
            AV12AreaTrabalho_Codigo = AV13Area_Codigo;
         }
         AV17GXLvl12 = 0;
         /* Using cursor P00752 */
         pr_default.execute(0, new Object[] {AV10Usuario_Codigo, AV12AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = P00752_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00752_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00752_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00752_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P00752_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P00752_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00752_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00752_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00752_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00752_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P00752_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P00752_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00752_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00752_n68ContratadaUsuario_ContratadaPessoaNom[0];
            AV17GXLvl12 = 1;
            AV8Nome = A68ContratadaUsuario_ContratadaPessoaNom;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV17GXLvl12 == 0 )
         {
            /* Using cursor P00753 */
            pr_default.execute(1, new Object[] {AV12AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A29Contratante_Codigo = P00753_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00753_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00753_A5AreaTrabalho_Codigo[0];
               /* Using cursor P00754 */
               pr_default.execute(2, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo, AV10Usuario_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A340ContratanteUsuario_ContratantePesCod = P00754_A340ContratanteUsuario_ContratantePesCod[0];
                  n340ContratanteUsuario_ContratantePesCod = P00754_n340ContratanteUsuario_ContratantePesCod[0];
                  A63ContratanteUsuario_ContratanteCod = P00754_A63ContratanteUsuario_ContratanteCod[0];
                  A60ContratanteUsuario_UsuarioCod = P00754_A60ContratanteUsuario_UsuarioCod[0];
                  A64ContratanteUsuario_ContratanteRaz = P00754_A64ContratanteUsuario_ContratanteRaz[0];
                  n64ContratanteUsuario_ContratanteRaz = P00754_n64ContratanteUsuario_ContratanteRaz[0];
                  A340ContratanteUsuario_ContratantePesCod = P00754_A340ContratanteUsuario_ContratantePesCod[0];
                  n340ContratanteUsuario_ContratantePesCod = P00754_n340ContratanteUsuario_ContratantePesCod[0];
                  A64ContratanteUsuario_ContratanteRaz = P00754_A64ContratanteUsuario_ContratanteRaz[0];
                  n64ContratanteUsuario_ContratanteRaz = P00754_n64ContratanteUsuario_ContratanteRaz[0];
                  AV8Nome = A64ContratanteUsuario_ContratanteRaz;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV14WebSession = context.GetSession();
         AV11WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00752_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00752_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P00752_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P00752_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00752_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P00752_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00752_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P00752_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         P00753_A29Contratante_Codigo = new int[1] ;
         P00753_n29Contratante_Codigo = new bool[] {false} ;
         P00753_A5AreaTrabalho_Codigo = new int[1] ;
         P00754_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         P00754_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         P00754_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00754_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00754_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         P00754_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         A64ContratanteUsuario_ContratanteRaz = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_entidadedousuario__default(),
            new Object[][] {
                new Object[] {
               P00752_A66ContratadaUsuario_ContratadaCod, P00752_A67ContratadaUsuario_ContratadaPessoaCod, P00752_n67ContratadaUsuario_ContratadaPessoaCod, P00752_A69ContratadaUsuario_UsuarioCod, P00752_A1228ContratadaUsuario_AreaTrabalhoCod, P00752_n1228ContratadaUsuario_AreaTrabalhoCod, P00752_A68ContratadaUsuario_ContratadaPessoaNom, P00752_n68ContratadaUsuario_ContratadaPessoaNom
               }
               , new Object[] {
               P00753_A29Contratante_Codigo, P00753_n29Contratante_Codigo, P00753_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               P00754_A340ContratanteUsuario_ContratantePesCod, P00754_n340ContratanteUsuario_ContratantePesCod, P00754_A63ContratanteUsuario_ContratanteCod, P00754_A60ContratanteUsuario_UsuarioCod, P00754_A64ContratanteUsuario_ContratanteRaz, P00754_n64ContratanteUsuario_ContratanteRaz
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV17GXLvl12 ;
      private int AV10Usuario_Codigo ;
      private int AV13Area_Codigo ;
      private int AV12AreaTrabalho_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private String AV8Nome ;
      private String scmdbuf ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n29Contratante_Codigo ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private IGxSession AV14WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00752_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00752_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P00752_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P00752_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00752_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P00752_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] P00752_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P00752_n68ContratadaUsuario_ContratadaPessoaNom ;
      private int[] P00753_A29Contratante_Codigo ;
      private bool[] P00753_n29Contratante_Codigo ;
      private int[] P00753_A5AreaTrabalho_Codigo ;
      private int[] P00754_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] P00754_n340ContratanteUsuario_ContratantePesCod ;
      private int[] P00754_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00754_A60ContratanteUsuario_UsuarioCod ;
      private String[] P00754_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] P00754_n64ContratanteUsuario_ContratanteRaz ;
      private String aP2_Nome ;
      private wwpbaseobjects.SdtWWPContext AV11WWPContext ;
   }

   public class prc_entidadedousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00752 ;
          prmP00752 = new Object[] {
          new Object[] {"@AV10Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00753 ;
          prmP00753 = new Object[] {
          new Object[] {"@AV12AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00754 ;
          prmP00754 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00752", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV10Usuario_Codigo) AND (T2.[Contratada_AreaTrabalhoCod] = @AV12AreaTrabalho_Codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00752,100,0,false,false )
             ,new CursorDef("P00753", "SELECT [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV12AreaTrabalho_Codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00753,1,0,true,true )
             ,new CursorDef("P00754", "SELECT T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T1.[ContratanteUsuario_UsuarioCod], T3.[Pessoa_Nome] AS ContratanteUsuario_Contratante FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @Contratante_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @AV10Usuario_Codigo ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00754,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
