/*
               File: ContagemResultadoExecucao
        Description: Contagem Resultado Execucao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:11:22.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoexecucao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action20") == 0 )
         {
            A1408ContagemResultadoExecucao_Prevista = context.localUtil.ParseDTimeParm( GetNextPar( ));
            n1408ContagemResultadoExecucao_Prevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_20_3N166( A1408ContagemResultadoExecucao_Prevista) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel14"+"_"+"CONTAGEMRESULTADOEXECUCAO_DIAS") == 0 )
         {
            A1407ContagemResultadoExecucao_Fim = context.localUtil.ParseDTimeParm( GetNextPar( ));
            n1407ContagemResultadoExecucao_Fim = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            AV25TipoDias = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A1406ContagemResultadoExecucao_Inicio = context.localUtil.ParseDTimeParm( GetNextPar( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX14ASACONTAGEMRESULTADOEXECUCAO_DIAS3N166( A1407ContagemResultadoExecucao_Fim, AV25TipoDias, A1406ContagemResultadoExecucao_Inicio) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel15"+"_"+"CONTAGEMRESULTADOEXECUCAO_PRAZODIAS") == 0 )
         {
            A1408ContagemResultadoExecucao_Prevista = context.localUtil.ParseDTimeParm( GetNextPar( ));
            n1408ContagemResultadoExecucao_Prevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            AV25TipoDias = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            AV20Dias = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
            A1406ContagemResultadoExecucao_Inicio = context.localUtil.ParseDTimeParm( GetNextPar( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX15ASACONTAGEMRESULTADOEXECUCAO_PRAZODIAS3N166( A1408ContagemResultadoExecucao_Prevista, AV25TipoDias, AV20Dias, A1406ContagemResultadoExecucao_Inicio) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel16"+"_"+"CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
         {
            A1406ContagemResultadoExecucao_Inicio = context.localUtil.ParseDTimeParm( GetNextPar( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            AV20Dias = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
            AV25TipoDias = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX16ASACONTAGEMRESULTADOEXECUCAO_PREVISTA3N166( A1406ContagemResultadoExecucao_Inicio, AV20Dias, AV25TipoDias) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_22") == 0 )
         {
            A1404ContagemResultadoExecucao_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_22( A1404ContagemResultadoExecucao_OSCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_23") == 0 )
         {
            A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1553ContagemResultado_CntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_23( A1553ContagemResultado_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV14ContagemResultadoExecucao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultadoExecucao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADOEXECUCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14ContagemResultadoExecucao_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Execucao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoexecucao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoexecucao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContagemResultadoExecucao_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV14ContagemResultadoExecucao_Codigo = aP1_ContagemResultadoExecucao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3N166( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3N166e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ",", "")), ((edtContagemResultadoExecucao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoExecucao_Codigo_Visible, edtContagemResultadoExecucao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoExecucao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3N166( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3N166( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3N166e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_64_3N166( true) ;
         }
         return  ;
      }

      protected void wb_table3_64_3N166e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3N166e( true) ;
         }
         else
         {
            wb_table1_2_3N166e( false) ;
         }
      }

      protected void wb_table3_64_3N166( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_64_3N166e( true) ;
         }
         else
         {
            wb_table3_64_3N166e( false) ;
         }
      }

      protected void wb_table2_5_3N166( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3N166( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3N166e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3N166e( true) ;
         }
         else
         {
            wb_table2_5_3N166e( false) ;
         }
      }

      protected void wb_table4_13_3N166( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_oscod_Internalname, "ID da OS", "", "", lblTextblockcontagemresultadoexecucao_oscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_18_3N166( true) ;
         }
         return  ;
      }

      protected void wb_table5_18_3N166e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_inicio_Internalname, "Inicio", "", "", lblTextblockcontagemresultadoexecucao_inicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_30_3N166( true) ;
         }
         return  ;
      }

      protected void wb_table6_30_3N166e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_prevista_Internalname, "Previsto", "", "", lblTextblockcontagemresultadoexecucao_prevista_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoExecucao_Prevista_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Prevista_Internalname, context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Prevista_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagemResultadoExecucao_Prevista_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoExecucao_Prevista_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoExecucao_Prevista_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_prazodias_Internalname, "Dias", "", "", lblTextblockcontagemresultadoexecucao_prazodias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_PrazoDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")), ((edtContagemResultadoExecucao_PrazoDias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")) : context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_PrazoDias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoExecucao_PrazoDias_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_fim_Internalname, "Fim", "", "", lblTextblockcontagemresultadoexecucao_fim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoExecucao_Fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Fim_Internalname, context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Fim_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagemResultadoExecucao_Fim_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoExecucao_Fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoExecucao_Fim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_dias_Internalname, "Usado", "", "", lblTextblockcontagemresultadoexecucao_dias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ",", "")), ((edtContagemResultadoExecucao_Dias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")) : context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Dias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoExecucao_Dias_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3N166e( true) ;
         }
         else
         {
            wb_table4_13_3N166e( false) ;
         }
      }

      protected void wb_table6_30_3N166( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultadoexecucao_inicio_Internalname, tblTablemergedcontagemresultadoexecucao_inicio_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoExecucao_Inicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Inicio_Internalname, context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Inicio_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagemResultadoExecucao_Inicio_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoExecucao_Inicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoExecucao_Inicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdias_Internalname, "", "", "", lblTextblockdias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Dias), 4, 0, ",", "")), ((edtavDias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20Dias), "ZZZ9")) : context.localUtil.Format( (decimal)(AV20Dias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavDias_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDias_righttext_Internalname, "�dias �teis", "", "", lblDias_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAcrescentardias_Internalname, context.GetImagePath( "6235c4eb-f913-43b6-a04a-91a52d975b13", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAcrescentardias_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgAcrescentardias_Jsonclick, "'"+""+"'"+",false,"+"'"+"e113n166_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_30_3N166e( true) ;
         }
         else
         {
            wb_table6_30_3N166e( false) ;
         }
      }

      protected void wb_table5_18_3N166( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultadoexecucao_oscod_Internalname, tblTablemergedcontagemresultadoexecucao_oscod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_OSCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoExecucao_OSCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoExecucao.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_1404_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_1404_Link, "", "", context.GetTheme( ), imgprompt_1404_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_demanda_Internalname, "No. OS", "", "", lblTextblockcontagemresultado_demanda_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda_Internalname, AV13ContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV13ContagemResultado_Demanda, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_demanda_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_18_3N166e( true) ;
         }
         else
         {
            wb_table5_18_3N166e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E123N2 */
         E123N2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_OSCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_OSCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOEXECUCAO_OSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1404ContagemResultadoExecucao_OSCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
               }
               else
               {
                  A1404ContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_OSCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
               }
               AV13ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda_Internalname));
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoExecucao_Inicio_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Inicio"}), 1, "CONTAGEMRESULTADOEXECUCAO_INICIO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoExecucao_Inicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1406ContagemResultadoExecucao_Inicio = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Inicio_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vDIAS");
                  AnyError = 1;
                  GX_FocusControl = edtavDias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV20Dias = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
               }
               else
               {
                  AV20Dias = (short)(context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoExecucao_Prevista_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Previsto"}), 1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoExecucao_Prevista_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
                  n1408ContagemResultadoExecucao_Prevista = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1408ContagemResultadoExecucao_Prevista = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Prevista_Internalname));
                  n1408ContagemResultadoExecucao_Prevista = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
               }
               n1408ContagemResultadoExecucao_Prevista = ((DateTime.MinValue==A1408ContagemResultadoExecucao_Prevista) ? true : false);
               A1411ContagemResultadoExecucao_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_PrazoDias_Internalname), ",", "."));
               n1411ContagemResultadoExecucao_PrazoDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoExecucao_Fim_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Fim"}), 1, "CONTAGEMRESULTADOEXECUCAO_FIM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoExecucao_Fim_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
                  n1407ContagemResultadoExecucao_Fim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1407ContagemResultadoExecucao_Fim = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Fim_Internalname));
                  n1407ContagemResultadoExecucao_Fim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
               }
               n1407ContagemResultadoExecucao_Fim = ((DateTime.MinValue==A1407ContagemResultadoExecucao_Fim) ? true : false);
               A1410ContagemResultadoExecucao_Dias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Dias_Internalname), ",", "."));
               n1410ContagemResultadoExecucao_Dias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
               A1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
               /* Read saved values. */
               Z1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1405ContagemResultadoExecucao_Codigo"), ",", "."));
               Z1406ContagemResultadoExecucao_Inicio = context.localUtil.CToT( cgiGet( "Z1406ContagemResultadoExecucao_Inicio"), 0);
               Z1408ContagemResultadoExecucao_Prevista = context.localUtil.CToT( cgiGet( "Z1408ContagemResultadoExecucao_Prevista"), 0);
               n1408ContagemResultadoExecucao_Prevista = ((DateTime.MinValue==A1408ContagemResultadoExecucao_Prevista) ? true : false);
               Z1411ContagemResultadoExecucao_PrazoDias = (short)(context.localUtil.CToN( cgiGet( "Z1411ContagemResultadoExecucao_PrazoDias"), ",", "."));
               n1411ContagemResultadoExecucao_PrazoDias = ((0==A1411ContagemResultadoExecucao_PrazoDias) ? true : false);
               Z1407ContagemResultadoExecucao_Fim = context.localUtil.CToT( cgiGet( "Z1407ContagemResultadoExecucao_Fim"), 0);
               n1407ContagemResultadoExecucao_Fim = ((DateTime.MinValue==A1407ContagemResultadoExecucao_Fim) ? true : false);
               Z1410ContagemResultadoExecucao_Dias = (short)(context.localUtil.CToN( cgiGet( "Z1410ContagemResultadoExecucao_Dias"), ",", "."));
               n1410ContagemResultadoExecucao_Dias = ((0==A1410ContagemResultadoExecucao_Dias) ? true : false);
               Z2028ContagemResultadoExecucao_Glosavel = StringUtil.StrToBool( cgiGet( "Z2028ContagemResultadoExecucao_Glosavel"));
               n2028ContagemResultadoExecucao_Glosavel = ((false==A2028ContagemResultadoExecucao_Glosavel) ? true : false);
               Z1404ContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( "Z1404ContagemResultadoExecucao_OSCod"), ",", "."));
               A2028ContagemResultadoExecucao_Glosavel = StringUtil.StrToBool( cgiGet( "Z2028ContagemResultadoExecucao_Glosavel"));
               n2028ContagemResultadoExecucao_Glosavel = false;
               n2028ContagemResultadoExecucao_Glosavel = ((false==A2028ContagemResultadoExecucao_Glosavel) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1404ContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( "N1404ContagemResultadoExecucao_OSCod"), ",", "."));
               A457ContagemResultado_Demanda = cgiGet( "CONTAGEMRESULTADO_DEMANDA");
               n457ContagemResultado_Demanda = false;
               A493ContagemResultado_DemandaFM = cgiGet( "CONTAGEMRESULTADO_DEMANDAFM");
               n493ContagemResultado_DemandaFM = false;
               A501ContagemResultado_OsFsOsFm = cgiGet( "CONTAGEMRESULTADO_OSFSOSFM");
               AV14ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTAGEMRESULTADOEXECUCAO_CODIGO"), ",", "."));
               AV18Insert_ContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMRESULTADOEXECUCAO_OSCOD"), ",", "."));
               A1611ContagemResultado_PrzTpDias = cgiGet( "CONTAGEMRESULTADO_PRZTPDIAS");
               n1611ContagemResultado_PrzTpDias = false;
               AV25TipoDias = cgiGet( "vTIPODIAS");
               AV24FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( "vFIMDOEXPEDIENTE"), 0));
               AV22Horas = context.localUtil.CToN( cgiGet( "vHORAS"), ",", ".");
               AV23Minutos = (short)(context.localUtil.CToN( cgiGet( "vMINUTOS"), ",", "."));
               A2028ContagemResultadoExecucao_Glosavel = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADOEXECUCAO_GLOSAVEL"));
               n2028ContagemResultadoExecucao_Glosavel = ((false==A2028ContagemResultadoExecucao_Glosavel) ? true : false);
               A1553ContagemResultado_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CNTSRVCOD"), ",", "."));
               A490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CONTRATADACOD"), ",", "."));
               n490ContagemResultado_ContratadaCod = false;
               A601ContagemResultado_Servico = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_SERVICO"), ",", "."));
               n601ContagemResultado_Servico = false;
               AV27Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContagemResultadoExecucao";
               A1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2028ContagemResultadoExecucao_Glosavel);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1405ContagemResultadoExecucao_Codigo != Z1405ContagemResultadoExecucao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[SecurityCheckFailed value for]"+"ContagemResultadoExecucao_Codigo:"+context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contagemresultadoexecucao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contagemresultadoexecucao:[SecurityCheckFailed value for]"+"ContagemResultadoExecucao_Glosavel:"+StringUtil.BoolToStr( A2028ContagemResultadoExecucao_Glosavel));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1405ContagemResultadoExecucao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode166 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode166;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound166 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3N0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTAGEMRESULTADOEXECUCAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultadoExecucao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123N2 */
                           E123N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E133N2 */
                           E133N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTAGEMRESULTADOEXECUCAO_INICIO.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E143N2 */
                           E143N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTAGEMRESULTADOEXECUCAO_PREVISTA.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E153N2 */
                           E153N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E133N2 */
            E133N2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3N166( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3N166( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDias_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3N0( )
      {
         BeforeValidate3N166( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3N166( ) ;
            }
            else
            {
               CheckExtendedTable3N166( ) ;
               CloseExtendedTableCursors3N166( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3N0( )
      {
      }

      protected void E123N2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV15WWPContext) ;
         AV16TrnContext.FromXml(AV17WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV16TrnContext.gxTpr_Transactionname, AV27Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV28GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GXV1), 8, 0)));
            while ( AV28GXV1 <= AV16TrnContext.gxTpr_Attributes.Count )
            {
               AV19TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV16TrnContext.gxTpr_Attributes.Item(AV28GXV1));
               if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "ContagemResultadoExecucao_OSCod") == 0 )
               {
                  AV18Insert_ContagemResultadoExecucao_OSCod = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Insert_ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Insert_ContagemResultadoExecucao_OSCod), 6, 0)));
               }
               AV28GXV1 = (int)(AV28GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GXV1), 8, 0)));
            }
         }
         edtContagemResultadoExecucao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Codigo_Visible), 5, 0)));
      }

      protected void E133N2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV16TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontagemresultadoexecucao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E143N2( )
      {
         /* ContagemResultadoExecucao_Inicio_Isvalid Routine */
         AV21Date = DateTimeUtil.ResetTime(A1406ContagemResultadoExecucao_Inicio);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Date", context.localUtil.Format(AV21Date, "99/99/99"));
         /* Execute user subroutine: 'CONFERIRDIAHABIL' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E153N2( )
      {
         /* ContagemResultadoExecucao_Prevista_Isvalid Routine */
         AV21Date = DateTimeUtil.ResetTime(A1408ContagemResultadoExecucao_Prevista);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Date", context.localUtil.Format(AV21Date, "99/99/99"));
         /* Execute user subroutine: 'CONFERIRDIAHABIL' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'CONFERIRDIAHABIL' Routine */
         GXt_dtime1 = DateTimeUtil.ResetTime( AV21Date ) ;
         if ( new prc_ehferiado(context).executeUdp( ref  GXt_dtime1) )
         {
            GX_msglist.addItem("A data informada foi feriado!");
         }
         else if ( DateTimeUtil.Dow( AV21Date) == 7 )
         {
            GX_msglist.addItem("A data informada foi s�bado!");
         }
         else if ( DateTimeUtil.Dow( AV21Date) == 1 )
         {
            GX_msglist.addItem("A data informada foi domingo!");
         }
      }

      protected void ZM3N166( short GX_JID )
      {
         if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1406ContagemResultadoExecucao_Inicio = T003N3_A1406ContagemResultadoExecucao_Inicio[0];
               Z1408ContagemResultadoExecucao_Prevista = T003N3_A1408ContagemResultadoExecucao_Prevista[0];
               Z1411ContagemResultadoExecucao_PrazoDias = T003N3_A1411ContagemResultadoExecucao_PrazoDias[0];
               Z1407ContagemResultadoExecucao_Fim = T003N3_A1407ContagemResultadoExecucao_Fim[0];
               Z1410ContagemResultadoExecucao_Dias = T003N3_A1410ContagemResultadoExecucao_Dias[0];
               Z2028ContagemResultadoExecucao_Glosavel = T003N3_A2028ContagemResultadoExecucao_Glosavel[0];
               Z1404ContagemResultadoExecucao_OSCod = T003N3_A1404ContagemResultadoExecucao_OSCod[0];
            }
            else
            {
               Z1406ContagemResultadoExecucao_Inicio = A1406ContagemResultadoExecucao_Inicio;
               Z1408ContagemResultadoExecucao_Prevista = A1408ContagemResultadoExecucao_Prevista;
               Z1411ContagemResultadoExecucao_PrazoDias = A1411ContagemResultadoExecucao_PrazoDias;
               Z1407ContagemResultadoExecucao_Fim = A1407ContagemResultadoExecucao_Fim;
               Z1410ContagemResultadoExecucao_Dias = A1410ContagemResultadoExecucao_Dias;
               Z2028ContagemResultadoExecucao_Glosavel = A2028ContagemResultadoExecucao_Glosavel;
               Z1404ContagemResultadoExecucao_OSCod = A1404ContagemResultadoExecucao_OSCod;
            }
         }
         if ( GX_JID == -21 )
         {
            Z1405ContagemResultadoExecucao_Codigo = A1405ContagemResultadoExecucao_Codigo;
            Z1406ContagemResultadoExecucao_Inicio = A1406ContagemResultadoExecucao_Inicio;
            Z1408ContagemResultadoExecucao_Prevista = A1408ContagemResultadoExecucao_Prevista;
            Z1411ContagemResultadoExecucao_PrazoDias = A1411ContagemResultadoExecucao_PrazoDias;
            Z1407ContagemResultadoExecucao_Fim = A1407ContagemResultadoExecucao_Fim;
            Z1410ContagemResultadoExecucao_Dias = A1410ContagemResultadoExecucao_Dias;
            Z2028ContagemResultadoExecucao_Glosavel = A2028ContagemResultadoExecucao_Glosavel;
            Z1404ContagemResultadoExecucao_OSCod = A1404ContagemResultadoExecucao_OSCod;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z1611ContagemResultado_PrzTpDias = A1611ContagemResultado_PrzTpDias;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContagemResultadoExecucao_PrazoDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_PrazoDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_PrazoDias_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Dias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Dias_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Codigo_Enabled), 5, 0)));
         AV27Pgmname = "ContagemResultadoExecucao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pgmname", AV27Pgmname);
         imgprompt_1404_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptcontagemresultado.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEMRESULTADOEXECUCAO_OSCOD"+"'), id:'"+"CONTAGEMRESULTADOEXECUCAO_OSCOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"vCONTAGEMRESULTADO_DEMANDA"+"'), id:'"+"vCONTAGEMRESULTADO_DEMANDA"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"vCONTAGEMRESULTADO_SISTEMACOD"+"'), id:'"+"vCONTAGEMRESULTADO_SISTEMACOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"vMODULO_CODIGO"+"'), id:'"+"vMODULO_CODIGO"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"vCONTRATADA"+"'), id:'"+"vCONTRATADA"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtContagemResultadoExecucao_PrazoDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_PrazoDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_PrazoDias_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Dias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Dias_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV14ContagemResultadoExecucao_Codigo) )
         {
            A1405ContagemResultadoExecucao_Codigo = AV14ContagemResultadoExecucao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_ContagemResultadoExecucao_OSCod) )
         {
            edtContagemResultadoExecucao_OSCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_OSCod_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultadoExecucao_OSCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_OSCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_ContagemResultadoExecucao_OSCod) )
         {
            A1404ContagemResultadoExecucao_OSCod = AV18Insert_ContagemResultadoExecucao_OSCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003N4 */
            pr_default.execute(2, new Object[] {A1404ContagemResultadoExecucao_OSCod});
            A1553ContagemResultado_CntSrvCod = T003N4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T003N4_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = T003N4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003N4_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003N4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003N4_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = T003N4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T003N4_n490ContagemResultado_ContratadaCod[0];
            pr_default.close(2);
            /* Using cursor T003N5 */
            pr_default.execute(3, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A1611ContagemResultado_PrzTpDias = T003N5_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = T003N5_n1611ContagemResultado_PrzTpDias[0];
            A601ContagemResultado_Servico = T003N5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T003N5_n601ContagemResultado_Servico[0];
            pr_default.close(3);
            AV25TipoDias = A1611ContagemResultado_PrzTpDias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         }
      }

      protected void Load3N166( )
      {
         /* Using cursor T003N6 */
         pr_default.execute(4, new Object[] {A1405ContagemResultadoExecucao_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound166 = 1;
            A1553ContagemResultado_CntSrvCod = T003N6_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T003N6_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = T003N6_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003N6_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003N6_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003N6_n493ContagemResultado_DemandaFM[0];
            A1611ContagemResultado_PrzTpDias = T003N6_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = T003N6_n1611ContagemResultado_PrzTpDias[0];
            A1406ContagemResultadoExecucao_Inicio = T003N6_A1406ContagemResultadoExecucao_Inicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            A1408ContagemResultadoExecucao_Prevista = T003N6_A1408ContagemResultadoExecucao_Prevista[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            n1408ContagemResultadoExecucao_Prevista = T003N6_n1408ContagemResultadoExecucao_Prevista[0];
            A1411ContagemResultadoExecucao_PrazoDias = T003N6_A1411ContagemResultadoExecucao_PrazoDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
            n1411ContagemResultadoExecucao_PrazoDias = T003N6_n1411ContagemResultadoExecucao_PrazoDias[0];
            A1407ContagemResultadoExecucao_Fim = T003N6_A1407ContagemResultadoExecucao_Fim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            n1407ContagemResultadoExecucao_Fim = T003N6_n1407ContagemResultadoExecucao_Fim[0];
            A1410ContagemResultadoExecucao_Dias = T003N6_A1410ContagemResultadoExecucao_Dias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
            n1410ContagemResultadoExecucao_Dias = T003N6_n1410ContagemResultadoExecucao_Dias[0];
            A2028ContagemResultadoExecucao_Glosavel = T003N6_A2028ContagemResultadoExecucao_Glosavel[0];
            n2028ContagemResultadoExecucao_Glosavel = T003N6_n2028ContagemResultadoExecucao_Glosavel[0];
            A1404ContagemResultadoExecucao_OSCod = T003N6_A1404ContagemResultadoExecucao_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
            A490ContagemResultado_ContratadaCod = T003N6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T003N6_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = T003N6_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T003N6_n601ContagemResultado_Servico[0];
            ZM3N166( -21) ;
         }
         pr_default.close(4);
         OnLoadActions3N166( ) ;
      }

      protected void OnLoadActions3N166( )
      {
         AV25TipoDias = A1611ContagemResultado_PrzTpDias;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
         GXt_int2 = A1410ContagemResultadoExecucao_Dias;
         new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1407ContagemResultadoExecucao_Fim),  AV25TipoDias, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
         A1410ContagemResultadoExecucao_Dias = GXt_int2;
         n1410ContagemResultadoExecucao_Dias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         AV22Horas = (decimal)(DateTimeUtil.Hour( AV24FimDoExpediente));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Horas", StringUtil.LTrim( StringUtil.Str( AV22Horas, 4, 5)));
         AV23Minutos = (short)(DateTimeUtil.Minute( AV24FimDoExpediente));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Minutos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Minutos), 4, 0)));
         if ( AV20Dias > 0 )
         {
            GXt_dtime1 = A1408ContagemResultadoExecucao_Prevista;
            new prc_adddiasuteis(context ).execute(  A1406ContagemResultadoExecucao_Inicio,  AV20Dias,  AV25TipoDias, out  GXt_dtime1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A1408ContagemResultadoExecucao_Prevista = GXt_dtime1;
            n1408ContagemResultadoExecucao_Prevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( (0==AV20Dias) )
         {
            GXt_int2 = A1411ContagemResultadoExecucao_PrazoDias;
            new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1408ContagemResultadoExecucao_Prevista),  AV25TipoDias, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A1411ContagemResultadoExecucao_PrazoDias = GXt_int2;
            n1411ContagemResultadoExecucao_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
         }
         else
         {
            if ( AV20Dias > 0 )
            {
               A1411ContagemResultadoExecucao_PrazoDias = AV20Dias;
               n1411ContagemResultadoExecucao_PrazoDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
            }
         }
      }

      protected void CheckExtendedTable3N166( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003N4 */
         pr_default.execute(2, new Object[] {A1404ContagemResultadoExecucao_OSCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Execucao_Contagem Resultado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOEXECUCAO_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1553ContagemResultado_CntSrvCod = T003N4_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T003N4_n1553ContagemResultado_CntSrvCod[0];
         A457ContagemResultado_Demanda = T003N4_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003N4_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003N4_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003N4_n493ContagemResultado_DemandaFM[0];
         A490ContagemResultado_ContratadaCod = T003N4_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T003N4_n490ContagemResultado_ContratadaCod[0];
         pr_default.close(2);
         /* Using cursor T003N5 */
         pr_default.execute(3, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1611ContagemResultado_PrzTpDias = T003N5_A1611ContagemResultado_PrzTpDias[0];
         n1611ContagemResultado_PrzTpDias = T003N5_n1611ContagemResultado_PrzTpDias[0];
         A601ContagemResultado_Servico = T003N5_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T003N5_n601ContagemResultado_Servico[0];
         pr_default.close(3);
         AV25TipoDias = A1611ContagemResultado_PrzTpDias;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
         GXt_int2 = A1410ContagemResultadoExecucao_Dias;
         new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1407ContagemResultadoExecucao_Fim),  AV25TipoDias, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
         A1410ContagemResultadoExecucao_Dias = GXt_int2;
         n1410ContagemResultadoExecucao_Dias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         if ( ! ( (DateTime.MinValue==A1406ContagemResultadoExecucao_Inicio) || ( A1406ContagemResultadoExecucao_Inicio >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Inicio fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOEXECUCAO_INICIO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_Inicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! T003N3_n1408ContagemResultadoExecucao_Prevista[0] && ( DateTimeUtil.Hour( A1408ContagemResultadoExecucao_Prevista) + DateTimeUtil.Minute( A1408ContagemResultadoExecucao_Prevista) == 0 ) )
         {
            GXt_int3 = AV15WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_getfimdoexpediente(context ).execute( ref  GXt_int3, out  AV24FimDoExpediente) ;
            AV15WWPContext.gxTpr_Areatrabalho_codigo = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FimDoExpediente", context.localUtil.TToC( AV24FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
         }
         AV22Horas = (decimal)(DateTimeUtil.Hour( AV24FimDoExpediente));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Horas", StringUtil.LTrim( StringUtil.Str( AV22Horas, 4, 5)));
         AV23Minutos = (short)(DateTimeUtil.Minute( AV24FimDoExpediente));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Minutos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Minutos), 4, 0)));
         if ( ! ( (DateTime.MinValue==A1407ContagemResultadoExecucao_Fim) || ( A1407ContagemResultadoExecucao_Fim >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Fim fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOEXECUCAO_FIM");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_Fim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AV20Dias > 0 )
         {
            GXt_dtime1 = A1408ContagemResultadoExecucao_Prevista;
            new prc_adddiasuteis(context ).execute(  A1406ContagemResultadoExecucao_Inicio,  AV20Dias,  AV25TipoDias, out  GXt_dtime1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A1408ContagemResultadoExecucao_Prevista = GXt_dtime1;
            n1408ContagemResultadoExecucao_Prevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( (0==AV20Dias) )
         {
            GXt_int2 = A1411ContagemResultadoExecucao_PrazoDias;
            new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1408ContagemResultadoExecucao_Prevista),  AV25TipoDias, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A1411ContagemResultadoExecucao_PrazoDias = GXt_int2;
            n1411ContagemResultadoExecucao_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
         }
         else
         {
            if ( AV20Dias > 0 )
            {
               A1411ContagemResultadoExecucao_PrazoDias = AV20Dias;
               n1411ContagemResultadoExecucao_PrazoDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
            }
         }
         if ( ! ( (DateTime.MinValue==A1408ContagemResultadoExecucao_Prevista) || ( A1408ContagemResultadoExecucao_Prevista >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Previsto fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_Prevista_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3N166( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_22( int A1404ContagemResultadoExecucao_OSCod )
      {
         /* Using cursor T003N7 */
         pr_default.execute(5, new Object[] {A1404ContagemResultadoExecucao_OSCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Execucao_Contagem Resultado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOEXECUCAO_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1553ContagemResultado_CntSrvCod = T003N7_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T003N7_n1553ContagemResultado_CntSrvCod[0];
         A457ContagemResultado_Demanda = T003N7_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003N7_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003N7_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003N7_n493ContagemResultado_DemandaFM[0];
         A490ContagemResultado_ContratadaCod = T003N7_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T003N7_n490ContagemResultado_ContratadaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( A457ContagemResultado_Demanda)+"\""+","+"\""+GXUtil.EncodeJSConstant( A493ContagemResultado_DemandaFM)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_23( int A1553ContagemResultado_CntSrvCod )
      {
         /* Using cursor T003N8 */
         pr_default.execute(6, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1611ContagemResultado_PrzTpDias = T003N8_A1611ContagemResultado_PrzTpDias[0];
         n1611ContagemResultado_PrzTpDias = T003N8_n1611ContagemResultado_PrzTpDias[0];
         A601ContagemResultado_Servico = T003N8_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T003N8_n601ContagemResultado_Servico[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1611ContagemResultado_PrzTpDias))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey3N166( )
      {
         /* Using cursor T003N9 */
         pr_default.execute(7, new Object[] {A1405ContagemResultadoExecucao_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound166 = 1;
         }
         else
         {
            RcdFound166 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003N3 */
         pr_default.execute(1, new Object[] {A1405ContagemResultadoExecucao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3N166( 21) ;
            RcdFound166 = 1;
            A1405ContagemResultadoExecucao_Codigo = T003N3_A1405ContagemResultadoExecucao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
            A1406ContagemResultadoExecucao_Inicio = T003N3_A1406ContagemResultadoExecucao_Inicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            A1408ContagemResultadoExecucao_Prevista = T003N3_A1408ContagemResultadoExecucao_Prevista[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            n1408ContagemResultadoExecucao_Prevista = T003N3_n1408ContagemResultadoExecucao_Prevista[0];
            A1411ContagemResultadoExecucao_PrazoDias = T003N3_A1411ContagemResultadoExecucao_PrazoDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
            n1411ContagemResultadoExecucao_PrazoDias = T003N3_n1411ContagemResultadoExecucao_PrazoDias[0];
            A1407ContagemResultadoExecucao_Fim = T003N3_A1407ContagemResultadoExecucao_Fim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            n1407ContagemResultadoExecucao_Fim = T003N3_n1407ContagemResultadoExecucao_Fim[0];
            A1410ContagemResultadoExecucao_Dias = T003N3_A1410ContagemResultadoExecucao_Dias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
            n1410ContagemResultadoExecucao_Dias = T003N3_n1410ContagemResultadoExecucao_Dias[0];
            A2028ContagemResultadoExecucao_Glosavel = T003N3_A2028ContagemResultadoExecucao_Glosavel[0];
            n2028ContagemResultadoExecucao_Glosavel = T003N3_n2028ContagemResultadoExecucao_Glosavel[0];
            A1404ContagemResultadoExecucao_OSCod = T003N3_A1404ContagemResultadoExecucao_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
            Z1405ContagemResultadoExecucao_Codigo = A1405ContagemResultadoExecucao_Codigo;
            sMode166 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3N166( ) ;
            if ( AnyError == 1 )
            {
               RcdFound166 = 0;
               InitializeNonKey3N166( ) ;
            }
            Gx_mode = sMode166;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound166 = 0;
            InitializeNonKey3N166( ) ;
            sMode166 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode166;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3N166( ) ;
         if ( RcdFound166 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound166 = 0;
         /* Using cursor T003N10 */
         pr_default.execute(8, new Object[] {A1405ContagemResultadoExecucao_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003N10_A1405ContagemResultadoExecucao_Codigo[0] < A1405ContagemResultadoExecucao_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003N10_A1405ContagemResultadoExecucao_Codigo[0] > A1405ContagemResultadoExecucao_Codigo ) ) )
            {
               A1405ContagemResultadoExecucao_Codigo = T003N10_A1405ContagemResultadoExecucao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
               RcdFound166 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound166 = 0;
         /* Using cursor T003N11 */
         pr_default.execute(9, new Object[] {A1405ContagemResultadoExecucao_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T003N11_A1405ContagemResultadoExecucao_Codigo[0] > A1405ContagemResultadoExecucao_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T003N11_A1405ContagemResultadoExecucao_Codigo[0] < A1405ContagemResultadoExecucao_Codigo ) ) )
            {
               A1405ContagemResultadoExecucao_Codigo = T003N11_A1405ContagemResultadoExecucao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
               RcdFound166 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3N166( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3N166( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound166 == 1 )
            {
               if ( A1405ContagemResultadoExecucao_Codigo != Z1405ContagemResultadoExecucao_Codigo )
               {
                  A1405ContagemResultadoExecucao_Codigo = Z1405ContagemResultadoExecucao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOEXECUCAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoExecucao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3N166( ) ;
                  GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1405ContagemResultadoExecucao_Codigo != Z1405ContagemResultadoExecucao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3N166( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOEXECUCAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoExecucao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3N166( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1405ContagemResultadoExecucao_Codigo != Z1405ContagemResultadoExecucao_Codigo )
         {
            A1405ContagemResultadoExecucao_Codigo = Z1405ContagemResultadoExecucao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOEXECUCAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3N166( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003N2 */
            pr_default.execute(0, new Object[] {A1405ContagemResultadoExecucao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoExecucao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1406ContagemResultadoExecucao_Inicio != T003N2_A1406ContagemResultadoExecucao_Inicio[0] ) || ( Z1408ContagemResultadoExecucao_Prevista != T003N2_A1408ContagemResultadoExecucao_Prevista[0] ) || ( Z1411ContagemResultadoExecucao_PrazoDias != T003N2_A1411ContagemResultadoExecucao_PrazoDias[0] ) || ( Z1407ContagemResultadoExecucao_Fim != T003N2_A1407ContagemResultadoExecucao_Fim[0] ) || ( Z1410ContagemResultadoExecucao_Dias != T003N2_A1410ContagemResultadoExecucao_Dias[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2028ContagemResultadoExecucao_Glosavel != T003N2_A2028ContagemResultadoExecucao_Glosavel[0] ) || ( Z1404ContagemResultadoExecucao_OSCod != T003N2_A1404ContagemResultadoExecucao_OSCod[0] ) )
            {
               if ( Z1406ContagemResultadoExecucao_Inicio != T003N2_A1406ContagemResultadoExecucao_Inicio[0] )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[seudo value changed for attri]"+"ContagemResultadoExecucao_Inicio");
                  GXUtil.WriteLogRaw("Old: ",Z1406ContagemResultadoExecucao_Inicio);
                  GXUtil.WriteLogRaw("Current: ",T003N2_A1406ContagemResultadoExecucao_Inicio[0]);
               }
               if ( Z1408ContagemResultadoExecucao_Prevista != T003N2_A1408ContagemResultadoExecucao_Prevista[0] )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[seudo value changed for attri]"+"ContagemResultadoExecucao_Prevista");
                  GXUtil.WriteLogRaw("Old: ",Z1408ContagemResultadoExecucao_Prevista);
                  GXUtil.WriteLogRaw("Current: ",T003N2_A1408ContagemResultadoExecucao_Prevista[0]);
               }
               if ( Z1411ContagemResultadoExecucao_PrazoDias != T003N2_A1411ContagemResultadoExecucao_PrazoDias[0] )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[seudo value changed for attri]"+"ContagemResultadoExecucao_PrazoDias");
                  GXUtil.WriteLogRaw("Old: ",Z1411ContagemResultadoExecucao_PrazoDias);
                  GXUtil.WriteLogRaw("Current: ",T003N2_A1411ContagemResultadoExecucao_PrazoDias[0]);
               }
               if ( Z1407ContagemResultadoExecucao_Fim != T003N2_A1407ContagemResultadoExecucao_Fim[0] )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[seudo value changed for attri]"+"ContagemResultadoExecucao_Fim");
                  GXUtil.WriteLogRaw("Old: ",Z1407ContagemResultadoExecucao_Fim);
                  GXUtil.WriteLogRaw("Current: ",T003N2_A1407ContagemResultadoExecucao_Fim[0]);
               }
               if ( Z1410ContagemResultadoExecucao_Dias != T003N2_A1410ContagemResultadoExecucao_Dias[0] )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[seudo value changed for attri]"+"ContagemResultadoExecucao_Dias");
                  GXUtil.WriteLogRaw("Old: ",Z1410ContagemResultadoExecucao_Dias);
                  GXUtil.WriteLogRaw("Current: ",T003N2_A1410ContagemResultadoExecucao_Dias[0]);
               }
               if ( Z2028ContagemResultadoExecucao_Glosavel != T003N2_A2028ContagemResultadoExecucao_Glosavel[0] )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[seudo value changed for attri]"+"ContagemResultadoExecucao_Glosavel");
                  GXUtil.WriteLogRaw("Old: ",Z2028ContagemResultadoExecucao_Glosavel);
                  GXUtil.WriteLogRaw("Current: ",T003N2_A2028ContagemResultadoExecucao_Glosavel[0]);
               }
               if ( Z1404ContagemResultadoExecucao_OSCod != T003N2_A1404ContagemResultadoExecucao_OSCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoexecucao:[seudo value changed for attri]"+"ContagemResultadoExecucao_OSCod");
                  GXUtil.WriteLogRaw("Old: ",Z1404ContagemResultadoExecucao_OSCod);
                  GXUtil.WriteLogRaw("Current: ",T003N2_A1404ContagemResultadoExecucao_OSCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoExecucao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3N166( )
      {
         BeforeValidate3N166( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3N166( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3N166( 0) ;
            CheckOptimisticConcurrency3N166( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3N166( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3N166( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003N12 */
                     pr_default.execute(10, new Object[] {A1406ContagemResultadoExecucao_Inicio, n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, n2028ContagemResultadoExecucao_Glosavel, A2028ContagemResultadoExecucao_Glosavel, A1404ContagemResultadoExecucao_OSCod});
                     A1405ContagemResultadoExecucao_Codigo = T003N12_A1405ContagemResultadoExecucao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3N0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3N166( ) ;
            }
            EndLevel3N166( ) ;
         }
         CloseExtendedTableCursors3N166( ) ;
      }

      protected void Update3N166( )
      {
         BeforeValidate3N166( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3N166( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3N166( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3N166( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3N166( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003N13 */
                     pr_default.execute(11, new Object[] {A1406ContagemResultadoExecucao_Inicio, n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, n2028ContagemResultadoExecucao_Glosavel, A2028ContagemResultadoExecucao_Glosavel, A1404ContagemResultadoExecucao_OSCod, A1405ContagemResultadoExecucao_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoExecucao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3N166( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3N166( ) ;
         }
         CloseExtendedTableCursors3N166( ) ;
      }

      protected void DeferredUpdate3N166( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3N166( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3N166( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3N166( ) ;
            AfterConfirm3N166( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3N166( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003N14 */
                  pr_default.execute(12, new Object[] {A1405ContagemResultadoExecucao_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode166 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3N166( ) ;
         Gx_mode = sMode166;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3N166( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003N15 */
            pr_default.execute(13, new Object[] {A1404ContagemResultadoExecucao_OSCod});
            A1553ContagemResultado_CntSrvCod = T003N15_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T003N15_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = T003N15_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003N15_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003N15_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003N15_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = T003N15_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T003N15_n490ContagemResultado_ContratadaCod[0];
            pr_default.close(13);
            /* Using cursor T003N16 */
            pr_default.execute(14, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A1611ContagemResultado_PrzTpDias = T003N16_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = T003N16_n1611ContagemResultado_PrzTpDias[0];
            A601ContagemResultado_Servico = T003N16_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T003N16_n601ContagemResultado_Servico[0];
            pr_default.close(14);
            AV25TipoDias = A1611ContagemResultado_PrzTpDias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         }
      }

      protected void EndLevel3N166( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3N166( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.CommitDataStores( "ContagemResultadoExecucao");
            if ( AnyError == 0 )
            {
               ConfirmValues3N0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.RollbackDataStores( "ContagemResultadoExecucao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3N166( )
      {
         /* Scan By routine */
         /* Using cursor T003N17 */
         pr_default.execute(15);
         RcdFound166 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound166 = 1;
            A1405ContagemResultadoExecucao_Codigo = T003N17_A1405ContagemResultadoExecucao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3N166( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound166 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound166 = 1;
            A1405ContagemResultadoExecucao_Codigo = T003N17_A1405ContagemResultadoExecucao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3N166( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm3N166( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3N166( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3N166( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3N166( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3N166( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3N166( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3N166( )
      {
         edtContagemResultadoExecucao_OSCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_OSCod_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Inicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Inicio_Enabled), 5, 0)));
         edtavDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDias_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Prevista_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Prevista_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Prevista_Enabled), 5, 0)));
         edtContagemResultadoExecucao_PrazoDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_PrazoDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_PrazoDias_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Fim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Fim_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Dias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Dias_Enabled), 5, 0)));
         edtContagemResultadoExecucao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3N0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221112442");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoexecucao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV14ContagemResultadoExecucao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1405ContagemResultadoExecucao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( Z1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( Z1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1411ContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( Z1407ContagemResultadoExecucao_Fim, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1410ContagemResultadoExecucao_Dias), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2028ContagemResultadoExecucao_Glosavel", Z2028ContagemResultadoExecucao_Glosavel);
         GxWebStd.gx_hidden_field( context, "Z1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1404ContagemResultadoExecucao_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV16TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV16TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vDATE", context.localUtil.DToC( AV21Date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSFSOSFM", A501ContagemResultado_OsFsOsFm);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADOEXECUCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ContagemResultadoExecucao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMRESULTADOEXECUCAO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Insert_ContagemResultadoExecucao_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PRZTPDIAS", StringUtil.RTrim( A1611ContagemResultado_PrzTpDias));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV25TipoDias));
         GxWebStd.gx_hidden_field( context, "vFIMDOEXPEDIENTE", context.localUtil.TToC( AV24FimDoExpediente, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vHORAS", StringUtil.LTrim( StringUtil.NToC( AV22Horas, 4, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMINUTOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Minutos), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_GLOSAVEL", A2028ContagemResultadoExecucao_Glosavel);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV27Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADOEXECUCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14ContagemResultadoExecucao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContagemResultadoExecucao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2028ContagemResultadoExecucao_Glosavel);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadoexecucao:[SendSecurityCheck value for]"+"ContagemResultadoExecucao_Codigo:"+context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contagemresultadoexecucao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contagemresultadoexecucao:[SendSecurityCheck value for]"+"ContagemResultadoExecucao_Glosavel:"+StringUtil.BoolToStr( A2028ContagemResultadoExecucao_Glosavel));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoexecucao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV14ContagemResultadoExecucao_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoExecucao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Execucao" ;
      }

      protected void InitializeNonKey3N166( )
      {
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
         A1404ContagemResultadoExecucao_OSCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
         AV20Dias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
         AV25TipoDias = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
         AV24FimDoExpediente = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FimDoExpediente", context.localUtil.TToC( AV24FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
         A501ContagemResultado_OsFsOsFm = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
         A601ContagemResultado_Servico = 0;
         n601ContagemResultado_Servico = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
         A1611ContagemResultado_PrzTpDias = "";
         n1611ContagemResultado_PrzTpDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1611ContagemResultado_PrzTpDias", A1611ContagemResultado_PrzTpDias);
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         n1408ContagemResultadoExecucao_Prevista = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
         n1408ContagemResultadoExecucao_Prevista = ((DateTime.MinValue==A1408ContagemResultadoExecucao_Prevista) ? true : false);
         A1411ContagemResultadoExecucao_PrazoDias = 0;
         n1411ContagemResultadoExecucao_PrazoDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
         n1411ContagemResultadoExecucao_PrazoDias = ((0==A1411ContagemResultadoExecucao_PrazoDias) ? true : false);
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         n1407ContagemResultadoExecucao_Fim = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
         n1407ContagemResultadoExecucao_Fim = ((DateTime.MinValue==A1407ContagemResultadoExecucao_Fim) ? true : false);
         A1410ContagemResultadoExecucao_Dias = 0;
         n1410ContagemResultadoExecucao_Dias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
         n1410ContagemResultadoExecucao_Dias = ((0==A1410ContagemResultadoExecucao_Dias) ? true : false);
         A2028ContagemResultadoExecucao_Glosavel = false;
         n2028ContagemResultadoExecucao_Glosavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2028ContagemResultadoExecucao_Glosavel", A2028ContagemResultadoExecucao_Glosavel);
         AV22Horas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Horas", StringUtil.LTrim( StringUtil.Str( AV22Horas, 4, 5)));
         AV23Minutos = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Minutos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Minutos), 4, 0)));
         Z1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         Z1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         Z1411ContagemResultadoExecucao_PrazoDias = 0;
         Z1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         Z1410ContagemResultadoExecucao_Dias = 0;
         Z2028ContagemResultadoExecucao_Glosavel = false;
         Z1404ContagemResultadoExecucao_OSCod = 0;
      }

      protected void InitAll3N166( )
      {
         A1405ContagemResultadoExecucao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
         InitializeNonKey3N166( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221112474");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoexecucao.js", "?202031221112475");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultadoexecucao_oscod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_OSCOD";
         edtContagemResultadoExecucao_OSCod_Internalname = "CONTAGEMRESULTADOEXECUCAO_OSCOD";
         lblTextblockcontagemresultado_demanda_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DEMANDA";
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA";
         tblTablemergedcontagemresultadoexecucao_oscod_Internalname = "TABLEMERGEDCONTAGEMRESULTADOEXECUCAO_OSCOD";
         lblTextblockcontagemresultadoexecucao_inicio_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_INICIO";
         edtContagemResultadoExecucao_Inicio_Internalname = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         lblTextblockdias_Internalname = "TEXTBLOCKDIAS";
         edtavDias_Internalname = "vDIAS";
         lblDias_righttext_Internalname = "DIAS_RIGHTTEXT";
         imgAcrescentardias_Internalname = "ACRESCENTARDIAS";
         tblTablemergedcontagemresultadoexecucao_inicio_Internalname = "TABLEMERGEDCONTAGEMRESULTADOEXECUCAO_INICIO";
         lblTextblockcontagemresultadoexecucao_prevista_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtContagemResultadoExecucao_Prevista_Internalname = "CONTAGEMRESULTADOEXECUCAO_PREVISTA";
         lblTextblockcontagemresultadoexecucao_prazodias_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtContagemResultadoExecucao_PrazoDias_Internalname = "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         lblTextblockcontagemresultadoexecucao_fim_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_FIM";
         edtContagemResultadoExecucao_Fim_Internalname = "CONTAGEMRESULTADOEXECUCAO_FIM";
         lblTextblockcontagemresultadoexecucao_dias_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_DIAS";
         edtContagemResultadoExecucao_Dias_Internalname = "CONTAGEMRESULTADOEXECUCAO_DIAS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContagemResultadoExecucao_Codigo_Internalname = "CONTAGEMRESULTADOEXECUCAO_CODIGO";
         Form.Internalname = "FORM";
         imgprompt_1404_Internalname = "PROMPT_1404";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Ciclo de execu��o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Execucao";
         edtavContagemresultado_demanda_Jsonclick = "";
         edtavContagemresultado_demanda_Enabled = 0;
         imgprompt_1404_Visible = 1;
         imgprompt_1404_Link = "";
         edtContagemResultadoExecucao_OSCod_Jsonclick = "";
         edtContagemResultadoExecucao_OSCod_Enabled = 1;
         imgAcrescentardias_Visible = 1;
         edtavDias_Jsonclick = "";
         edtavDias_Enabled = 1;
         edtContagemResultadoExecucao_Inicio_Jsonclick = "";
         edtContagemResultadoExecucao_Inicio_Enabled = 1;
         edtContagemResultadoExecucao_Dias_Jsonclick = "";
         edtContagemResultadoExecucao_Dias_Enabled = 0;
         edtContagemResultadoExecucao_Fim_Jsonclick = "";
         edtContagemResultadoExecucao_Fim_Enabled = 1;
         edtContagemResultadoExecucao_PrazoDias_Jsonclick = "";
         edtContagemResultadoExecucao_PrazoDias_Enabled = 0;
         edtContagemResultadoExecucao_Prevista_Jsonclick = "";
         edtContagemResultadoExecucao_Prevista_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContagemResultadoExecucao_Codigo_Jsonclick = "";
         edtContagemResultadoExecucao_Codigo_Enabled = 0;
         edtContagemResultadoExecucao_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX14ASACONTAGEMRESULTADOEXECUCAO_DIAS3N166( DateTime A1407ContagemResultadoExecucao_Fim ,
                                                                 String AV25TipoDias ,
                                                                 DateTime A1406ContagemResultadoExecucao_Inicio )
      {
         GXt_int2 = A1410ContagemResultadoExecucao_Dias;
         new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1407ContagemResultadoExecucao_Fim),  AV25TipoDias, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
         A1410ContagemResultadoExecucao_Dias = GXt_int2;
         n1410ContagemResultadoExecucao_Dias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX15ASACONTAGEMRESULTADOEXECUCAO_PRAZODIAS3N166( DateTime A1408ContagemResultadoExecucao_Prevista ,
                                                                      String AV25TipoDias ,
                                                                      short AV20Dias ,
                                                                      DateTime A1406ContagemResultadoExecucao_Inicio )
      {
         if ( (0==AV20Dias) )
         {
            GXt_int2 = A1411ContagemResultadoExecucao_PrazoDias;
            new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1408ContagemResultadoExecucao_Prevista),  AV25TipoDias, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A1411ContagemResultadoExecucao_PrazoDias = GXt_int2;
            n1411ContagemResultadoExecucao_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
         }
         else
         {
            if ( AV20Dias > 0 )
            {
               A1411ContagemResultadoExecucao_PrazoDias = AV20Dias;
               n1411ContagemResultadoExecucao_PrazoDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX16ASACONTAGEMRESULTADOEXECUCAO_PREVISTA3N166( DateTime A1406ContagemResultadoExecucao_Inicio ,
                                                                     short AV20Dias ,
                                                                     String AV25TipoDias )
      {
         if ( AV20Dias > 0 )
         {
            GXt_dtime1 = A1408ContagemResultadoExecucao_Prevista;
            new prc_adddiasuteis(context ).execute(  A1406ContagemResultadoExecucao_Inicio,  AV20Dias,  AV25TipoDias, out  GXt_dtime1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Dias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDias", AV25TipoDias);
            A1408ContagemResultadoExecucao_Prevista = GXt_dtime1;
            n1408ContagemResultadoExecucao_Prevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_20_3N166( DateTime A1408ContagemResultadoExecucao_Prevista )
      {
         if ( ! T003N3_n1408ContagemResultadoExecucao_Prevista[0] && ( DateTimeUtil.Hour( A1408ContagemResultadoExecucao_Prevista) + DateTimeUtil.Minute( A1408ContagemResultadoExecucao_Prevista) == 0 ) )
         {
            GXt_int3 = AV15WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_getfimdoexpediente(context ).execute( ref  GXt_int3, out  AV24FimDoExpediente) ;
            AV15WWPContext.gxTpr_Areatrabalho_codigo = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FimDoExpediente", context.localUtil.TToC( AV24FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( context.localUtil.TToC( AV24FimDoExpediente, 10, 8, 0, 3, "/", ":", " "))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contagemresultadoexecucao_oscod( int GX_Parm1 ,
                                                         int GX_Parm2 ,
                                                         String GX_Parm3 ,
                                                         String GX_Parm4 ,
                                                         String GX_Parm5 ,
                                                         int GX_Parm6 ,
                                                         int GX_Parm7 ,
                                                         String GX_Parm8 ,
                                                         String GX_Parm9 )
      {
         A1404ContagemResultadoExecucao_OSCod = GX_Parm1;
         A1553ContagemResultado_CntSrvCod = GX_Parm2;
         n1553ContagemResultado_CntSrvCod = false;
         A1611ContagemResultado_PrzTpDias = GX_Parm3;
         n1611ContagemResultado_PrzTpDias = false;
         A457ContagemResultado_Demanda = GX_Parm4;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = GX_Parm5;
         n493ContagemResultado_DemandaFM = false;
         A490ContagemResultado_ContratadaCod = GX_Parm6;
         n490ContagemResultado_ContratadaCod = false;
         A601ContagemResultado_Servico = GX_Parm7;
         n601ContagemResultado_Servico = false;
         AV25TipoDias = GX_Parm8;
         A501ContagemResultado_OsFsOsFm = GX_Parm9;
         /* Using cursor T003N15 */
         pr_default.execute(13, new Object[] {A1404ContagemResultadoExecucao_OSCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Execucao_Contagem Resultado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOEXECUCAO_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_OSCod_Internalname;
         }
         A1553ContagemResultado_CntSrvCod = T003N15_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T003N15_n1553ContagemResultado_CntSrvCod[0];
         A457ContagemResultado_Demanda = T003N15_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003N15_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003N15_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003N15_n493ContagemResultado_DemandaFM[0];
         A490ContagemResultado_ContratadaCod = T003N15_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T003N15_n490ContagemResultado_ContratadaCod[0];
         pr_default.close(13);
         /* Using cursor T003N16 */
         pr_default.execute(14, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1611ContagemResultado_PrzTpDias = T003N16_A1611ContagemResultado_PrzTpDias[0];
         n1611ContagemResultado_PrzTpDias = T003N16_n1611ContagemResultado_PrzTpDias[0];
         A601ContagemResultado_Servico = T003N16_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T003N16_n601ContagemResultado_Servico[0];
         pr_default.close(14);
         AV25TipoDias = A1611ContagemResultado_PrzTpDias;
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1553ContagemResultado_CntSrvCod = 0;
            n1553ContagemResultado_CntSrvCod = false;
            A457ContagemResultado_Demanda = "";
            n457ContagemResultado_Demanda = false;
            A493ContagemResultado_DemandaFM = "";
            n493ContagemResultado_DemandaFM = false;
            A490ContagemResultado_ContratadaCod = 0;
            n490ContagemResultado_ContratadaCod = false;
            A1611ContagemResultado_PrzTpDias = "";
            n1611ContagemResultado_PrzTpDias = false;
            A601ContagemResultado_Servico = 0;
            n601ContagemResultado_Servico = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ".", "")));
         isValidOutput.Add(A457ContagemResultado_Demanda);
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1611ContagemResultado_PrzTpDias));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( AV25TipoDias));
         isValidOutput.Add(A501ContagemResultado_OsFsOsFm);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Dias( DateTime GX_Parm1 ,
                               short GX_Parm2 ,
                               String GX_Parm3 ,
                               DateTime GX_Parm4 )
      {
         A1406ContagemResultadoExecucao_Inicio = GX_Parm1;
         AV20Dias = GX_Parm2;
         AV25TipoDias = GX_Parm3;
         A1408ContagemResultadoExecucao_Prevista = GX_Parm4;
         n1408ContagemResultadoExecucao_Prevista = false;
         if ( AV20Dias > 0 )
         {
            GXt_dtime1 = A1408ContagemResultadoExecucao_Prevista;
            new prc_adddiasuteis(context ).execute(  A1406ContagemResultadoExecucao_Inicio,  AV20Dias,  AV25TipoDias, out  GXt_dtime1) ;
            A1408ContagemResultadoExecucao_Prevista = GXt_dtime1;
            n1408ContagemResultadoExecucao_Prevista = false;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoexecucao_prevista( DateTime GX_Parm1 ,
                                                            DateTime GX_Parm2 ,
                                                            String GX_Parm3 ,
                                                            short GX_Parm4 ,
                                                            DateTime GX_Parm5 ,
                                                            short GX_Parm6 ,
                                                            decimal GX_Parm7 ,
                                                            short GX_Parm8 )
      {
         A1408ContagemResultadoExecucao_Prevista = GX_Parm1;
         n1408ContagemResultadoExecucao_Prevista = false;
         A1406ContagemResultadoExecucao_Inicio = GX_Parm2;
         AV25TipoDias = GX_Parm3;
         AV20Dias = GX_Parm4;
         AV24FimDoExpediente = GX_Parm5;
         A1411ContagemResultadoExecucao_PrazoDias = GX_Parm6;
         n1411ContagemResultadoExecucao_PrazoDias = false;
         AV22Horas = GX_Parm7;
         AV23Minutos = GX_Parm8;
         if ( ! ( (DateTime.MinValue==A1408ContagemResultadoExecucao_Prevista) || ( A1408ContagemResultadoExecucao_Prevista >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Previsto fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_Prevista_Internalname;
         }
         if ( (0==AV20Dias) )
         {
            GXt_int2 = A1411ContagemResultadoExecucao_PrazoDias;
            new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1408ContagemResultadoExecucao_Prevista),  AV25TipoDias, out  GXt_int2) ;
            A1411ContagemResultadoExecucao_PrazoDias = GXt_int2;
            n1411ContagemResultadoExecucao_PrazoDias = false;
         }
         else
         {
            if ( AV20Dias > 0 )
            {
               A1411ContagemResultadoExecucao_PrazoDias = AV20Dias;
               n1411ContagemResultadoExecucao_PrazoDias = false;
            }
         }
         if ( ! T003N3_n1408ContagemResultadoExecucao_Prevista[0] && ( DateTimeUtil.Hour( A1408ContagemResultadoExecucao_Prevista) + DateTimeUtil.Minute( A1408ContagemResultadoExecucao_Prevista) == 0 ) )
         {
            GXt_int3 = AV15WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_getfimdoexpediente(context ).execute( ref  GXt_int3, out  AV24FimDoExpediente) ;
            AV15WWPContext.gxTpr_Areatrabalho_codigo = GXt_int3;
         }
         AV22Horas = (decimal)(DateTimeUtil.Hour( AV24FimDoExpediente));
         AV23Minutos = (short)(DateTimeUtil.Minute( AV24FimDoExpediente));
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ".", "")));
         isValidOutput.Add(AV15WWPContext.gxTpr_Areatrabalho_codigo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( AV22Horas, 4, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Minutos), 4, 0, ".", "")));
         isValidOutput.Add(context.localUtil.TToC( AV24FimDoExpediente, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoexecucao_fim( DateTime GX_Parm1 ,
                                                       DateTime GX_Parm2 ,
                                                       String GX_Parm3 ,
                                                       short GX_Parm4 )
      {
         A1407ContagemResultadoExecucao_Fim = GX_Parm1;
         n1407ContagemResultadoExecucao_Fim = false;
         A1406ContagemResultadoExecucao_Inicio = GX_Parm2;
         AV25TipoDias = GX_Parm3;
         A1410ContagemResultadoExecucao_Dias = GX_Parm4;
         n1410ContagemResultadoExecucao_Dias = false;
         if ( ! ( (DateTime.MinValue==A1407ContagemResultadoExecucao_Fim) || ( A1407ContagemResultadoExecucao_Fim >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Fim fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOEXECUCAO_FIM");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoExecucao_Fim_Internalname;
         }
         GXt_int2 = A1410ContagemResultadoExecucao_Dias;
         new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1407ContagemResultadoExecucao_Fim),  AV25TipoDias, out  GXt_int2) ;
         A1410ContagemResultadoExecucao_Dias = GXt_int2;
         n1410ContagemResultadoExecucao_Dias = false;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV14ContagemResultadoExecucao_Codigo',fld:'vCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E133N2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV16TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOACRESCENTARDIAS'","{handler:'E113N166',iparms:[],oparms:[]}");
         setEventMetadata("CONTAGEMRESULTADOEXECUCAO_INICIO.ISVALID","{handler:'E143N2',iparms:[{av:'A1406ContagemResultadoExecucao_Inicio',fld:'CONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV21Date',fld:'vDATE',pic:'',nv:''}],oparms:[{av:'AV21Date',fld:'vDATE',pic:'',nv:''}]}");
         setEventMetadata("CONTAGEMRESULTADOEXECUCAO_PREVISTA.ISVALID","{handler:'E153N2',iparms:[{av:'A1408ContagemResultadoExecucao_Prevista',fld:'CONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV21Date',fld:'vDATE',pic:'',nv:''}],oparms:[{av:'AV21Date',fld:'vDATE',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         Z1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         Z1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         AV25TipoDias = "";
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_oscod_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_inicio_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_prevista_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_prazodias_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_fim_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_dias_Jsonclick = "";
         lblTextblockdias_Jsonclick = "";
         lblDias_righttext_Jsonclick = "";
         imgAcrescentardias_Jsonclick = "";
         lblTextblockcontagemresultado_demanda_Jsonclick = "";
         AV13ContagemResultado_Demanda = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A501ContagemResultado_OsFsOsFm = "";
         A1611ContagemResultado_PrzTpDias = "";
         AV24FimDoExpediente = (DateTime)(DateTime.MinValue);
         AV27Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode166 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV16TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV17WebSession = context.GetSession();
         AV19TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV21Date = DateTime.MinValue;
         Z457ContagemResultado_Demanda = "";
         Z493ContagemResultado_DemandaFM = "";
         Z1611ContagemResultado_PrzTpDias = "";
         T003N4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003N4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003N4_A457ContagemResultado_Demanda = new String[] {""} ;
         T003N4_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003N4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003N4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003N4_A490ContagemResultado_ContratadaCod = new int[1] ;
         T003N4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T003N5_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         T003N5_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         T003N5_A601ContagemResultado_Servico = new int[1] ;
         T003N5_n601ContagemResultado_Servico = new bool[] {false} ;
         T003N6_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003N6_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003N6_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T003N6_A457ContagemResultado_Demanda = new String[] {""} ;
         T003N6_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003N6_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003N6_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003N6_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         T003N6_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         T003N6_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         T003N6_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         T003N6_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         T003N6_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         T003N6_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         T003N6_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         T003N6_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         T003N6_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         T003N6_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         T003N6_A2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         T003N6_n2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         T003N6_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         T003N6_A490ContagemResultado_ContratadaCod = new int[1] ;
         T003N6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T003N6_A601ContagemResultado_Servico = new int[1] ;
         T003N6_n601ContagemResultado_Servico = new bool[] {false} ;
         T003N3_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         T003N7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003N7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003N7_A457ContagemResultado_Demanda = new String[] {""} ;
         T003N7_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003N7_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003N7_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003N7_A490ContagemResultado_ContratadaCod = new int[1] ;
         T003N7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T003N8_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         T003N8_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         T003N8_A601ContagemResultado_Servico = new int[1] ;
         T003N8_n601ContagemResultado_Servico = new bool[] {false} ;
         T003N9_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T003N3_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T003N3_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         T003N3_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         T003N3_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         T003N3_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         T003N3_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         T003N3_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         T003N3_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         T003N3_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         T003N3_A2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         T003N3_n2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         T003N3_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         T003N10_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T003N11_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T003N2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T003N2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         T003N2_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         T003N2_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         T003N2_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         T003N2_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         T003N2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         T003N2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         T003N2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         T003N2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         T003N2_A2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         T003N2_n2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         T003N2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         T003N12_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T003N15_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T003N15_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T003N15_A457ContagemResultado_Demanda = new String[] {""} ;
         T003N15_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003N15_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003N15_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003N15_A490ContagemResultado_ContratadaCod = new int[1] ;
         T003N15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T003N16_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         T003N16_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         T003N16_A601ContagemResultado_Servico = new int[1] ;
         T003N16_n601ContagemResultado_Servico = new bool[] {false} ;
         T003N17_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoexecucao__default(),
            new Object[][] {
                new Object[] {
               T003N2_A1405ContagemResultadoExecucao_Codigo, T003N2_A1406ContagemResultadoExecucao_Inicio, T003N2_A1408ContagemResultadoExecucao_Prevista, T003N2_n1408ContagemResultadoExecucao_Prevista, T003N2_A1411ContagemResultadoExecucao_PrazoDias, T003N2_n1411ContagemResultadoExecucao_PrazoDias, T003N2_A1407ContagemResultadoExecucao_Fim, T003N2_n1407ContagemResultadoExecucao_Fim, T003N2_A1410ContagemResultadoExecucao_Dias, T003N2_n1410ContagemResultadoExecucao_Dias,
               T003N2_A2028ContagemResultadoExecucao_Glosavel, T003N2_n2028ContagemResultadoExecucao_Glosavel, T003N2_A1404ContagemResultadoExecucao_OSCod
               }
               , new Object[] {
               T003N3_A1405ContagemResultadoExecucao_Codigo, T003N3_A1406ContagemResultadoExecucao_Inicio, T003N3_A1408ContagemResultadoExecucao_Prevista, T003N3_n1408ContagemResultadoExecucao_Prevista, T003N3_A1411ContagemResultadoExecucao_PrazoDias, T003N3_n1411ContagemResultadoExecucao_PrazoDias, T003N3_A1407ContagemResultadoExecucao_Fim, T003N3_n1407ContagemResultadoExecucao_Fim, T003N3_A1410ContagemResultadoExecucao_Dias, T003N3_n1410ContagemResultadoExecucao_Dias,
               T003N3_A2028ContagemResultadoExecucao_Glosavel, T003N3_n2028ContagemResultadoExecucao_Glosavel, T003N3_A1404ContagemResultadoExecucao_OSCod
               }
               , new Object[] {
               T003N4_A1553ContagemResultado_CntSrvCod, T003N4_n1553ContagemResultado_CntSrvCod, T003N4_A457ContagemResultado_Demanda, T003N4_n457ContagemResultado_Demanda, T003N4_A493ContagemResultado_DemandaFM, T003N4_n493ContagemResultado_DemandaFM, T003N4_A490ContagemResultado_ContratadaCod, T003N4_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T003N5_A1611ContagemResultado_PrzTpDias, T003N5_n1611ContagemResultado_PrzTpDias, T003N5_A601ContagemResultado_Servico, T003N5_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003N6_A1553ContagemResultado_CntSrvCod, T003N6_n1553ContagemResultado_CntSrvCod, T003N6_A1405ContagemResultadoExecucao_Codigo, T003N6_A457ContagemResultado_Demanda, T003N6_n457ContagemResultado_Demanda, T003N6_A493ContagemResultado_DemandaFM, T003N6_n493ContagemResultado_DemandaFM, T003N6_A1611ContagemResultado_PrzTpDias, T003N6_n1611ContagemResultado_PrzTpDias, T003N6_A1406ContagemResultadoExecucao_Inicio,
               T003N6_A1408ContagemResultadoExecucao_Prevista, T003N6_n1408ContagemResultadoExecucao_Prevista, T003N6_A1411ContagemResultadoExecucao_PrazoDias, T003N6_n1411ContagemResultadoExecucao_PrazoDias, T003N6_A1407ContagemResultadoExecucao_Fim, T003N6_n1407ContagemResultadoExecucao_Fim, T003N6_A1410ContagemResultadoExecucao_Dias, T003N6_n1410ContagemResultadoExecucao_Dias, T003N6_A2028ContagemResultadoExecucao_Glosavel, T003N6_n2028ContagemResultadoExecucao_Glosavel,
               T003N6_A1404ContagemResultadoExecucao_OSCod, T003N6_A490ContagemResultado_ContratadaCod, T003N6_n490ContagemResultado_ContratadaCod, T003N6_A601ContagemResultado_Servico, T003N6_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003N7_A1553ContagemResultado_CntSrvCod, T003N7_n1553ContagemResultado_CntSrvCod, T003N7_A457ContagemResultado_Demanda, T003N7_n457ContagemResultado_Demanda, T003N7_A493ContagemResultado_DemandaFM, T003N7_n493ContagemResultado_DemandaFM, T003N7_A490ContagemResultado_ContratadaCod, T003N7_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T003N8_A1611ContagemResultado_PrzTpDias, T003N8_n1611ContagemResultado_PrzTpDias, T003N8_A601ContagemResultado_Servico, T003N8_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003N9_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               T003N10_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               T003N11_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               T003N12_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003N15_A1553ContagemResultado_CntSrvCod, T003N15_n1553ContagemResultado_CntSrvCod, T003N15_A457ContagemResultado_Demanda, T003N15_n457ContagemResultado_Demanda, T003N15_A493ContagemResultado_DemandaFM, T003N15_n493ContagemResultado_DemandaFM, T003N15_A490ContagemResultado_ContratadaCod, T003N15_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T003N16_A1611ContagemResultado_PrzTpDias, T003N16_n1611ContagemResultado_PrzTpDias, T003N16_A601ContagemResultado_Servico, T003N16_n601ContagemResultado_Servico
               }
               , new Object[] {
               T003N17_A1405ContagemResultadoExecucao_Codigo
               }
            }
         );
         AV27Pgmname = "ContagemResultadoExecucao";
      }

      private short Z1411ContagemResultadoExecucao_PrazoDias ;
      private short Z1410ContagemResultadoExecucao_Dias ;
      private short GxWebError ;
      private short AV20Dias ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short A1410ContagemResultadoExecucao_Dias ;
      private short AV23Minutos ;
      private short RcdFound166 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private short GXt_int2 ;
      private int wcpOAV14ContagemResultadoExecucao_Codigo ;
      private int Z1405ContagemResultadoExecucao_Codigo ;
      private int Z1404ContagemResultadoExecucao_OSCod ;
      private int N1404ContagemResultadoExecucao_OSCod ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV14ContagemResultadoExecucao_Codigo ;
      private int trnEnded ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int edtContagemResultadoExecucao_Codigo_Enabled ;
      private int edtContagemResultadoExecucao_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContagemResultadoExecucao_Prevista_Enabled ;
      private int edtContagemResultadoExecucao_PrazoDias_Enabled ;
      private int edtContagemResultadoExecucao_Fim_Enabled ;
      private int edtContagemResultadoExecucao_Dias_Enabled ;
      private int edtContagemResultadoExecucao_Inicio_Enabled ;
      private int edtavDias_Enabled ;
      private int imgAcrescentardias_Visible ;
      private int edtContagemResultadoExecucao_OSCod_Enabled ;
      private int imgprompt_1404_Visible ;
      private int edtavContagemresultado_demanda_Enabled ;
      private int AV18Insert_ContagemResultadoExecucao_OSCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int AV28GXV1 ;
      private int Z1553ContagemResultado_CntSrvCod ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int Z601ContagemResultado_Servico ;
      private int idxLst ;
      private int GXt_int3 ;
      private decimal AV22Horas ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String AV25TipoDias ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoExecucao_OSCod_Internalname ;
      private String edtContagemResultadoExecucao_Codigo_Internalname ;
      private String edtContagemResultadoExecucao_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_oscod_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_oscod_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_inicio_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_inicio_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_prevista_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_prevista_Jsonclick ;
      private String edtContagemResultadoExecucao_Prevista_Internalname ;
      private String edtContagemResultadoExecucao_Prevista_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_prazodias_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_prazodias_Jsonclick ;
      private String edtContagemResultadoExecucao_PrazoDias_Internalname ;
      private String edtContagemResultadoExecucao_PrazoDias_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_fim_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_fim_Jsonclick ;
      private String edtContagemResultadoExecucao_Fim_Internalname ;
      private String edtContagemResultadoExecucao_Fim_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_dias_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_dias_Jsonclick ;
      private String edtContagemResultadoExecucao_Dias_Internalname ;
      private String edtContagemResultadoExecucao_Dias_Jsonclick ;
      private String tblTablemergedcontagemresultadoexecucao_inicio_Internalname ;
      private String edtContagemResultadoExecucao_Inicio_Internalname ;
      private String edtContagemResultadoExecucao_Inicio_Jsonclick ;
      private String lblTextblockdias_Internalname ;
      private String lblTextblockdias_Jsonclick ;
      private String edtavDias_Internalname ;
      private String edtavDias_Jsonclick ;
      private String lblDias_righttext_Internalname ;
      private String lblDias_righttext_Jsonclick ;
      private String imgAcrescentardias_Internalname ;
      private String imgAcrescentardias_Jsonclick ;
      private String tblTablemergedcontagemresultadoexecucao_oscod_Internalname ;
      private String edtContagemResultadoExecucao_OSCod_Jsonclick ;
      private String imgprompt_1404_Internalname ;
      private String imgprompt_1404_Link ;
      private String lblTextblockcontagemresultado_demanda_Internalname ;
      private String lblTextblockcontagemresultado_demanda_Jsonclick ;
      private String edtavContagemresultado_demanda_Internalname ;
      private String edtavContagemresultado_demanda_Jsonclick ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV27Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode166 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1611ContagemResultado_PrzTpDias ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z1406ContagemResultadoExecucao_Inicio ;
      private DateTime Z1408ContagemResultadoExecucao_Prevista ;
      private DateTime Z1407ContagemResultadoExecucao_Fim ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime AV24FimDoExpediente ;
      private DateTime GXt_dtime1 ;
      private DateTime AV21Date ;
      private bool Z2028ContagemResultadoExecucao_Glosavel ;
      private bool entryPointCalled ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool n2028ContagemResultadoExecucao_Glosavel ;
      private bool A2028ContagemResultadoExecucao_Glosavel ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String AV13ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String Z457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private IGxSession AV17WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T003N4_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003N4_n1553ContagemResultado_CntSrvCod ;
      private String[] T003N4_A457ContagemResultado_Demanda ;
      private bool[] T003N4_n457ContagemResultado_Demanda ;
      private String[] T003N4_A493ContagemResultado_DemandaFM ;
      private bool[] T003N4_n493ContagemResultado_DemandaFM ;
      private int[] T003N4_A490ContagemResultado_ContratadaCod ;
      private bool[] T003N4_n490ContagemResultado_ContratadaCod ;
      private String[] T003N5_A1611ContagemResultado_PrzTpDias ;
      private bool[] T003N5_n1611ContagemResultado_PrzTpDias ;
      private int[] T003N5_A601ContagemResultado_Servico ;
      private bool[] T003N5_n601ContagemResultado_Servico ;
      private int[] T003N6_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003N6_n1553ContagemResultado_CntSrvCod ;
      private int[] T003N6_A1405ContagemResultadoExecucao_Codigo ;
      private String[] T003N6_A457ContagemResultado_Demanda ;
      private bool[] T003N6_n457ContagemResultado_Demanda ;
      private String[] T003N6_A493ContagemResultado_DemandaFM ;
      private bool[] T003N6_n493ContagemResultado_DemandaFM ;
      private String[] T003N6_A1611ContagemResultado_PrzTpDias ;
      private bool[] T003N6_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] T003N6_A1406ContagemResultadoExecucao_Inicio ;
      private DateTime[] T003N6_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] T003N6_n1408ContagemResultadoExecucao_Prevista ;
      private short[] T003N6_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] T003N6_n1411ContagemResultadoExecucao_PrazoDias ;
      private DateTime[] T003N6_A1407ContagemResultadoExecucao_Fim ;
      private bool[] T003N6_n1407ContagemResultadoExecucao_Fim ;
      private short[] T003N6_A1410ContagemResultadoExecucao_Dias ;
      private bool[] T003N6_n1410ContagemResultadoExecucao_Dias ;
      private bool[] T003N6_A2028ContagemResultadoExecucao_Glosavel ;
      private bool[] T003N6_n2028ContagemResultadoExecucao_Glosavel ;
      private int[] T003N6_A1404ContagemResultadoExecucao_OSCod ;
      private int[] T003N6_A490ContagemResultado_ContratadaCod ;
      private bool[] T003N6_n490ContagemResultado_ContratadaCod ;
      private int[] T003N6_A601ContagemResultado_Servico ;
      private bool[] T003N6_n601ContagemResultado_Servico ;
      private bool[] T003N3_n1408ContagemResultadoExecucao_Prevista ;
      private int[] T003N7_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003N7_n1553ContagemResultado_CntSrvCod ;
      private String[] T003N7_A457ContagemResultado_Demanda ;
      private bool[] T003N7_n457ContagemResultado_Demanda ;
      private String[] T003N7_A493ContagemResultado_DemandaFM ;
      private bool[] T003N7_n493ContagemResultado_DemandaFM ;
      private int[] T003N7_A490ContagemResultado_ContratadaCod ;
      private bool[] T003N7_n490ContagemResultado_ContratadaCod ;
      private String[] T003N8_A1611ContagemResultado_PrzTpDias ;
      private bool[] T003N8_n1611ContagemResultado_PrzTpDias ;
      private int[] T003N8_A601ContagemResultado_Servico ;
      private bool[] T003N8_n601ContagemResultado_Servico ;
      private int[] T003N9_A1405ContagemResultadoExecucao_Codigo ;
      private int[] T003N3_A1405ContagemResultadoExecucao_Codigo ;
      private DateTime[] T003N3_A1406ContagemResultadoExecucao_Inicio ;
      private DateTime[] T003N3_A1408ContagemResultadoExecucao_Prevista ;
      private short[] T003N3_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] T003N3_n1411ContagemResultadoExecucao_PrazoDias ;
      private DateTime[] T003N3_A1407ContagemResultadoExecucao_Fim ;
      private bool[] T003N3_n1407ContagemResultadoExecucao_Fim ;
      private short[] T003N3_A1410ContagemResultadoExecucao_Dias ;
      private bool[] T003N3_n1410ContagemResultadoExecucao_Dias ;
      private bool[] T003N3_A2028ContagemResultadoExecucao_Glosavel ;
      private bool[] T003N3_n2028ContagemResultadoExecucao_Glosavel ;
      private int[] T003N3_A1404ContagemResultadoExecucao_OSCod ;
      private int[] T003N10_A1405ContagemResultadoExecucao_Codigo ;
      private int[] T003N11_A1405ContagemResultadoExecucao_Codigo ;
      private int[] T003N2_A1405ContagemResultadoExecucao_Codigo ;
      private DateTime[] T003N2_A1406ContagemResultadoExecucao_Inicio ;
      private DateTime[] T003N2_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] T003N2_n1408ContagemResultadoExecucao_Prevista ;
      private short[] T003N2_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] T003N2_n1411ContagemResultadoExecucao_PrazoDias ;
      private DateTime[] T003N2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] T003N2_n1407ContagemResultadoExecucao_Fim ;
      private short[] T003N2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] T003N2_n1410ContagemResultadoExecucao_Dias ;
      private bool[] T003N2_A2028ContagemResultadoExecucao_Glosavel ;
      private bool[] T003N2_n2028ContagemResultadoExecucao_Glosavel ;
      private int[] T003N2_A1404ContagemResultadoExecucao_OSCod ;
      private int[] T003N12_A1405ContagemResultadoExecucao_Codigo ;
      private int[] T003N15_A1553ContagemResultado_CntSrvCod ;
      private bool[] T003N15_n1553ContagemResultado_CntSrvCod ;
      private String[] T003N15_A457ContagemResultado_Demanda ;
      private bool[] T003N15_n457ContagemResultado_Demanda ;
      private String[] T003N15_A493ContagemResultado_DemandaFM ;
      private bool[] T003N15_n493ContagemResultado_DemandaFM ;
      private int[] T003N15_A490ContagemResultado_ContratadaCod ;
      private bool[] T003N15_n490ContagemResultado_ContratadaCod ;
      private String[] T003N16_A1611ContagemResultado_PrzTpDias ;
      private bool[] T003N16_n1611ContagemResultado_PrzTpDias ;
      private int[] T003N16_A601ContagemResultado_Servico ;
      private bool[] T003N16_n601ContagemResultado_Servico ;
      private int[] T003N17_A1405ContagemResultadoExecucao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV16TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV19TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV15WWPContext ;
   }

   public class contagemresultadoexecucao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003N6 ;
          prmT003N6 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N4 ;
          prmT003N4 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N5 ;
          prmT003N5 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N7 ;
          prmT003N7 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N8 ;
          prmT003N8 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N9 ;
          prmT003N9 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N3 ;
          prmT003N3 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N10 ;
          prmT003N10 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N11 ;
          prmT003N11 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N2 ;
          prmT003N2 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N12 ;
          prmT003N12 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Glosavel",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N13 ;
          prmT003N13 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Glosavel",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N14 ;
          prmT003N14 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N17 ;
          prmT003N17 = new Object[] {
          } ;
          Object[] prmT003N15 ;
          prmT003N15 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003N16 ;
          prmT003N16 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003N2", "SELECT [ContagemResultadoExecucao_Codigo], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Glosavel], [ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod FROM [ContagemResultadoExecucao] WITH (UPDLOCK) WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N2,1,0,true,false )
             ,new CursorDef("T003N3", "SELECT [ContagemResultadoExecucao_Codigo], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Glosavel], [ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N3,1,0,true,false )
             ,new CursorDef("T003N4", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_ContratadaCod] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoExecucao_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N4,1,0,true,false )
             ,new CursorDef("T003N5", "SELECT [ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N5,1,0,true,false )
             ,new CursorDef("T003N6", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, TM1.[ContagemResultadoExecucao_Codigo], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, TM1.[ContagemResultadoExecucao_Inicio], TM1.[ContagemResultadoExecucao_Prevista], TM1.[ContagemResultadoExecucao_PrazoDias], TM1.[ContagemResultadoExecucao_Fim], TM1.[ContagemResultadoExecucao_Dias], TM1.[ContagemResultadoExecucao_Glosavel], TM1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T2.[ContagemResultado_ContratadaCod], T3.[Servico_Codigo] AS ContagemResultado_Servico FROM (([ContagemResultadoExecucao] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE TM1.[ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo ORDER BY TM1.[ContagemResultadoExecucao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003N6,100,0,true,false )
             ,new CursorDef("T003N7", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_ContratadaCod] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoExecucao_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N7,1,0,true,false )
             ,new CursorDef("T003N8", "SELECT [ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N8,1,0,true,false )
             ,new CursorDef("T003N9", "SELECT [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003N9,1,0,true,false )
             ,new CursorDef("T003N10", "SELECT TOP 1 [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE ( [ContagemResultadoExecucao_Codigo] > @ContagemResultadoExecucao_Codigo) ORDER BY [ContagemResultadoExecucao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003N10,1,0,true,true )
             ,new CursorDef("T003N11", "SELECT TOP 1 [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE ( [ContagemResultadoExecucao_Codigo] < @ContagemResultadoExecucao_Codigo) ORDER BY [ContagemResultadoExecucao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003N11,1,0,true,true )
             ,new CursorDef("T003N12", "INSERT INTO [ContagemResultadoExecucao]([ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Glosavel], [ContagemResultadoExecucao_OSCod]) VALUES(@ContagemResultadoExecucao_Inicio, @ContagemResultadoExecucao_Prevista, @ContagemResultadoExecucao_PrazoDias, @ContagemResultadoExecucao_Fim, @ContagemResultadoExecucao_Dias, @ContagemResultadoExecucao_Glosavel, @ContagemResultadoExecucao_OSCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003N12)
             ,new CursorDef("T003N13", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Inicio]=@ContagemResultadoExecucao_Inicio, [ContagemResultadoExecucao_Prevista]=@ContagemResultadoExecucao_Prevista, [ContagemResultadoExecucao_PrazoDias]=@ContagemResultadoExecucao_PrazoDias, [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias, [ContagemResultadoExecucao_Glosavel]=@ContagemResultadoExecucao_Glosavel, [ContagemResultadoExecucao_OSCod]=@ContagemResultadoExecucao_OSCod  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK,prmT003N13)
             ,new CursorDef("T003N14", "DELETE FROM [ContagemResultadoExecucao]  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK,prmT003N14)
             ,new CursorDef("T003N15", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM], [ContagemResultado_ContratadaCod] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoExecucao_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N15,1,0,true,false )
             ,new CursorDef("T003N16", "SELECT [ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003N16,1,0,true,false )
             ,new CursorDef("T003N17", "SELECT [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) ORDER BY [ContagemResultadoExecucao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003N17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((short[]) buf[16])[0] = rslt.getShort(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((bool[]) buf[18])[0] = rslt.getBool(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 11 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
