/*
               File: SistemaFuncaoDadosWC
        Description: Sistema Funcao Dados WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:25.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemafuncaodadoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemafuncaodadoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemafuncaodadoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_SistemaCod )
      {
         this.AV7FuncaoDados_SistemaCod = aP0_FuncaoDados_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavFuncaodados_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavFuncaodados_tipo2 = new GXCombobox();
         cmbFuncaoDados_Tipo = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
         cmbFuncaoDados_Ativo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7FuncaoDados_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_70 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_70_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_70_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  edtFuncaoDados_SolucaoTecnica_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_SolucaoTecnica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_SolucaoTecnica_Visible), 5, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV23DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector1", AV23DynamicFiltersSelector1);
                  AV24DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0)));
                  AV25FuncaoDados_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25FuncaoDados_Nome1", AV25FuncaoDados_Nome1);
                  AV26FuncaoDados_Tipo1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo1", AV26FuncaoDados_Tipo1);
                  AV37FuncaoDados_PF1 = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37FuncaoDados_PF1", StringUtil.LTrim( StringUtil.Str( AV37FuncaoDados_PF1, 14, 5)));
                  AV28DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector2", AV28DynamicFiltersSelector2);
                  AV29DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0)));
                  AV30FuncaoDados_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30FuncaoDados_Nome2", AV30FuncaoDados_Nome2);
                  AV31FuncaoDados_Tipo2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31FuncaoDados_Tipo2", AV31FuncaoDados_Tipo2);
                  AV38FuncaoDados_PF2 = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoDados_PF2", StringUtil.LTrim( StringUtil.Str( AV38FuncaoDados_PF2, 14, 5)));
                  AV27DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled2", AV27DynamicFiltersEnabled2);
                  AV42TFFuncaoDados_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_Nome", AV42TFFuncaoDados_Nome);
                  AV43TFFuncaoDados_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_Nome_Sel", AV43TFFuncaoDados_Nome_Sel);
                  AV50TFFuncaoDados_FuncaoDadosSisDes = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFFuncaoDados_FuncaoDadosSisDes", AV50TFFuncaoDados_FuncaoDadosSisDes);
                  AV51TFFuncaoDados_FuncaoDadosSisDes_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoDados_FuncaoDadosSisDes_Sel", AV51TFFuncaoDados_FuncaoDadosSisDes_Sel);
                  AV58TFFuncaoDados_DER = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0)));
                  AV59TFFuncaoDados_DER_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0)));
                  AV62TFFuncaoDados_RLR = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0)));
                  AV63TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0)));
                  AV66TFFuncaoDados_PF = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5)));
                  AV67TFFuncaoDados_PF_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5)));
                  AV74TFFuncaoDados_SolucaoTecnica = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFFuncaoDados_SolucaoTecnica", AV74TFFuncaoDados_SolucaoTecnica);
                  AV75TFFuncaoDados_SolucaoTecnica_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75TFFuncaoDados_SolucaoTecnica_Sel", AV75TFFuncaoDados_SolucaoTecnica_Sel);
                  AV7FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
                  edtFuncaoDados_SolucaoTecnica_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_SolucaoTecnica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_SolucaoTecnica_Visible), 5, 0)));
                  AV44ddo_FuncaoDados_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_FuncaoDados_NomeTitleControlIdToReplace", AV44ddo_FuncaoDados_NomeTitleControlIdToReplace);
                  AV48ddo_FuncaoDados_TipoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_FuncaoDados_TipoTitleControlIdToReplace", AV48ddo_FuncaoDados_TipoTitleControlIdToReplace);
                  AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace", AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace);
                  AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
                  AV60ddo_FuncaoDados_DERTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ddo_FuncaoDados_DERTitleControlIdToReplace", AV60ddo_FuncaoDados_DERTitleControlIdToReplace);
                  AV64ddo_FuncaoDados_RLRTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_FuncaoDados_RLRTitleControlIdToReplace", AV64ddo_FuncaoDados_RLRTitleControlIdToReplace);
                  AV68ddo_FuncaoDados_PFTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_FuncaoDados_PFTitleControlIdToReplace", AV68ddo_FuncaoDados_PFTitleControlIdToReplace);
                  AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace);
                  AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace", AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace);
                  AV88Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV47TFFuncaoDados_Tipo_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV55TFFuncaoDados_Complexidade_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV71TFFuncaoDados_Ativo_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
                  AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
                  A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n391FuncaoDados_FuncaoDadosCod = false;
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A404FuncaoDados_FuncaoDadosSisCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n404FuncaoDados_FuncaoDadosSisCod = false;
                  A753FuncaoDados_SolucaoTecnica = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA9G2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV88Pgmname = "SistemaFuncaoDadosWC";
               context.Gx_err = 0;
               WS9G2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Funcao Dados WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203269122660");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemafuncaodadoswc.aspx") + "?" + UrlEncode("" +AV7FuncaoDados_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV23DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_NOME1", AV25FuncaoDados_Nome1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_TIPO1", StringUtil.RTrim( AV26FuncaoDados_Tipo1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_PF1", StringUtil.LTrim( StringUtil.NToC( AV37FuncaoDados_PF1, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV28DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_NOME2", AV30FuncaoDados_Nome2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_TIPO2", StringUtil.RTrim( AV31FuncaoDados_Tipo2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAODADOS_PF2", StringUtil.LTrim( StringUtil.NToC( AV38FuncaoDados_PF2, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV27DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_NOME", AV42TFFuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_NOME_SEL", AV43TFFuncaoDados_Nome_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_FUNCAODADOSSISDES", AV50TFFuncaoDados_FuncaoDadosSisDes);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL", AV51TFFuncaoDados_FuncaoDadosSisDes_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFFuncaoDados_DER), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_DER_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFFuncaoDados_RLR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_RLR_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( AV66TFFuncaoDados_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_PF_TO", StringUtil.LTrim( StringUtil.NToC( AV67TFFuncaoDados_PF_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_SOLUCAOTECNICA", AV74TFFuncaoDados_SolucaoTecnica);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAODADOS_SOLUCAOTECNICA_SEL", AV75TFFuncaoDados_SolucaoTecnica_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_70", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_70), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_NOMETITLEFILTERDATA", AV41FuncaoDados_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_NOMETITLEFILTERDATA", AV41FuncaoDados_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_TIPOTITLEFILTERDATA", AV45FuncaoDados_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_TIPOTITLEFILTERDATA", AV45FuncaoDados_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_FUNCAODADOSSISDESTITLEFILTERDATA", AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_FUNCAODADOSSISDESTITLEFILTERDATA", AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV53FuncaoDados_ComplexidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV53FuncaoDados_ComplexidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_DERTITLEFILTERDATA", AV57FuncaoDados_DERTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_DERTITLEFILTERDATA", AV57FuncaoDados_DERTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_RLRTITLEFILTERDATA", AV61FuncaoDados_RLRTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_RLRTITLEFILTERDATA", AV61FuncaoDados_RLRTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_PFTITLEFILTERDATA", AV65FuncaoDados_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_PFTITLEFILTERDATA", AV65FuncaoDados_PFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_ATIVOTITLEFILTERDATA", AV69FuncaoDados_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_ATIVOTITLEFILTERDATA", AV69FuncaoDados_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAODADOS_SOLUCAOTECNICATITLEFILTERDATA", AV73FuncaoDados_SolucaoTecnicaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAODADOS_SOLUCAOTECNICATITLEFILTERDATA", AV73FuncaoDados_SolucaoTecnicaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV88Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFFUNCAODADOS_TIPO_SELS", AV47TFFuncaoDados_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFFUNCAODADOS_TIPO_SELS", AV47TFFuncaoDados_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV55TFFuncaoDados_Complexidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV55TFFuncaoDados_Complexidade_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFFUNCAODADOS_ATIVO_SELS", AV71TFFuncaoDados_Ativo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFFUNCAODADOS_ATIVO_SELS", AV71TFFuncaoDados_Ativo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_SISTEMAPROJCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A752FuncaoDados_SistemaProjCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Caption", StringUtil.RTrim( Ddo_funcaodados_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaodados_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Cls", StringUtil.RTrim( Ddo_funcaodados_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaodados_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaodados_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Caption", StringUtil.RTrim( Ddo_funcaodados_tipo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Tooltip", StringUtil.RTrim( Ddo_funcaodados_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Cls", StringUtil.RTrim( Ddo_funcaodados_tipo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Sortasc", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Caption", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Tooltip", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Cls", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_funcaodadossisdes_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_funcaodadossisdes_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_funcaodadossisdes_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filtertype", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_funcaodadossisdes_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_funcaodadossisdes_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Datalistproc", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_funcaodadossisdes_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Sortasc", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Caption", StringUtil.RTrim( Ddo_funcaodados_complexidade_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip", StringUtil.RTrim( Ddo_funcaodados_complexidade_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cls", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_complexidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_complexidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Caption", StringUtil.RTrim( Ddo_funcaodados_der_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Tooltip", StringUtil.RTrim( Ddo_funcaodados_der_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Cls", StringUtil.RTrim( Ddo_funcaodados_der_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_der_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_der_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_der_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filtertype", StringUtil.RTrim( Ddo_funcaodados_der_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_der_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_der_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_der_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_der_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Caption", StringUtil.RTrim( Ddo_funcaodados_rlr_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Tooltip", StringUtil.RTrim( Ddo_funcaodados_rlr_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Cls", StringUtil.RTrim( Ddo_funcaodados_rlr_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_rlr_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_rlr_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filtertype", StringUtil.RTrim( Ddo_funcaodados_rlr_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_rlr_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_rlr_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Caption", StringUtil.RTrim( Ddo_funcaodados_pf_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Tooltip", StringUtil.RTrim( Ddo_funcaodados_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Cls", StringUtil.RTrim( Ddo_funcaodados_pf_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filtertype", StringUtil.RTrim( Ddo_funcaodados_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Caption", StringUtil.RTrim( Ddo_funcaodados_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Tooltip", StringUtil.RTrim( Ddo_funcaodados_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Cls", StringUtil.RTrim( Ddo_funcaodados_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Sortasc", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Caption", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Tooltip", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Cls", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_solucaotecnica_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_solucaotecnica_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_solucaotecnica_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filtertype", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_solucaotecnica_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_solucaotecnica_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Datalistproc", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_solucaotecnica_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_SOLUCAOTECNICA_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_SolucaoTecnica_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_funcaodadossisdes_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_complexidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_der_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_rlr_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_solucaotecnica_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9G2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemafuncaodadoswc.js", "?20203269122953");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaFuncaoDadosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Funcao Dados WC" ;
      }

      protected void WB9G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemafuncaodadoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_9G2( true) ;
         }
         else
         {
            wb_table1_2_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV27DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_Internalname, AV42TFFuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavTffuncaodados_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_sel_Internalname, AV43TFFuncaoDados_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", 0, edtavTffuncaodados_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_funcaodadossisdes_Internalname, AV50TFFuncaoDados_FuncaoDadosSisDes, StringUtil.RTrim( context.localUtil.Format( AV50TFFuncaoDados_FuncaoDadosSisDes, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_funcaodadossisdes_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_funcaodadossisdes_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_funcaodadossisdes_sel_Internalname, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, StringUtil.RTrim( context.localUtil.Format( AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,95);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_funcaodadossisdes_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_funcaodadossisdes_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFFuncaoDados_DER), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TFFuncaoDados_DER), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59TFFuncaoDados_DER_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFFuncaoDados_RLR), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62TFFuncaoDados_RLR), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63TFFuncaoDados_RLR_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV66TFFuncaoDados_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV66TFFuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,100);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV67TFFuncaoDados_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV67TFFuncaoDados_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,101);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_solucaotecnica_Internalname, AV74TFFuncaoDados_SolucaoTecnica, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", 0, edtavTffuncaodados_solucaotecnica_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_solucaotecnica_sel_Internalname, AV75TFFuncaoDados_SolucaoTecnica_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavTffuncaodados_solucaotecnica_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Internalname, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", 0, edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", 0, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_DERContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_RLRContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Internalname, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
         }
         wbLoad = true;
      }

      protected void START9G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Funcao Dados WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP9G0( ) ;
            }
         }
      }

      protected void WS9G2( )
      {
         START9G2( ) ;
         EVT9G2( ) ;
      }

      protected void EVT9G2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E119G2 */
                                    E119G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E129G2 */
                                    E129G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E139G2 */
                                    E139G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_FUNCAODADOSSISDES.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E149G2 */
                                    E149G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E159G2 */
                                    E159G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_DER.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E169G2 */
                                    E169G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E179G2 */
                                    E179G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_PF.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E189G2 */
                                    E189G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E199G2 */
                                    E199G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_SOLUCAOTECNICA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E209G2 */
                                    E209G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E219G2 */
                                    E219G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E229G2 */
                                    E229G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E239G2 */
                                    E239G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E249G2 */
                                    E249G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E259G2 */
                                    E259G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E269G2 */
                                    E269G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E279G2 */
                                    E279G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E289G2 */
                                    E289G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9G0( ) ;
                              }
                              nGXsfl_70_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_70_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_70_idx), 4, 0)), 4, "0");
                              SubsflControlProps_702( ) ;
                              AV15Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV85Update_GXI : context.convertURL( context.PathToRelativeUrl( AV15Update))));
                              AV16Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV86Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV16Delete))));
                              AV17Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Display)) ? AV87Display_GXI : context.convertURL( context.PathToRelativeUrl( AV17Display))));
                              A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
                              A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_SistemaCod_Internalname), ",", "."));
                              A391FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_FuncaoDadosCod_Internalname), ",", "."));
                              n391FuncaoDados_FuncaoDadosCod = false;
                              A404FuncaoDados_FuncaoDadosSisCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_FuncaoDadosSisCod_Internalname), ",", "."));
                              n404FuncaoDados_FuncaoDadosSisCod = false;
                              A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
                              cmbFuncaoDados_Tipo.Name = cmbFuncaoDados_Tipo_Internalname;
                              cmbFuncaoDados_Tipo.CurrentValue = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                              A373FuncaoDados_Tipo = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                              A405FuncaoDados_FuncaoDadosSisDes = StringUtil.Upper( cgiGet( edtFuncaoDados_FuncaoDadosSisDes_Internalname));
                              n405FuncaoDados_FuncaoDadosSisDes = false;
                              cmbFuncaoDados_Complexidade.Name = cmbFuncaoDados_Complexidade_Internalname;
                              cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
                              A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
                              A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
                              cmbFuncaoDados_Ativo.Name = cmbFuncaoDados_Ativo_Internalname;
                              cmbFuncaoDados_Ativo.CurrentValue = cgiGet( cmbFuncaoDados_Ativo_Internalname);
                              A394FuncaoDados_Ativo = cgiGet( cmbFuncaoDados_Ativo_Internalname);
                              A753FuncaoDados_SolucaoTecnica = cgiGet( edtFuncaoDados_SolucaoTecnica_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E299G2 */
                                          E299G2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E309G2 */
                                          E309G2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E319G2 */
                                          E319G2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV23DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME1"), AV25FuncaoDados_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_tipo1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_TIPO1"), AV26FuncaoDados_Tipo1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_pf1 Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAODADOS_PF1"), ",", ".") != AV37FuncaoDados_PF1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV28DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV29DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME2"), AV30FuncaoDados_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_tipo2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_TIPO2"), AV31FuncaoDados_Tipo2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaodados_pf2 Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAODADOS_PF2"), ",", ".") != AV38FuncaoDados_PF2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV27DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME"), AV42TFFuncaoDados_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME_SEL"), AV43TFFuncaoDados_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_funcaodadossisdes Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_FUNCAODADOSSISDES"), AV50TFFuncaoDados_FuncaoDadosSisDes) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_funcaodadossisdes_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL"), AV51TFFuncaoDados_FuncaoDadosSisDes_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_der Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER"), ",", ".") != Convert.ToDecimal( AV58TFFuncaoDados_DER )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_der_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER_TO"), ",", ".") != Convert.ToDecimal( AV59TFFuncaoDados_DER_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_rlr Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR"), ",", ".") != Convert.ToDecimal( AV62TFFuncaoDados_RLR )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_rlr_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR_TO"), ",", ".") != Convert.ToDecimal( AV63TFFuncaoDados_RLR_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_pf Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF"), ",", ".") != AV66TFFuncaoDados_PF )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_pf_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF_TO"), ",", ".") != AV67TFFuncaoDados_PF_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_solucaotecnica Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_SOLUCAOTECNICA"), AV74TFFuncaoDados_SolucaoTecnica) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaodados_solucaotecnica_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_SOLUCAOTECNICA_SEL"), AV75TFFuncaoDados_SolucaoTecnica_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP9G0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9G2( ) ;
            }
         }
      }

      protected void PA9G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAODADOS_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAODADOS_TIPO", "Tipo de Fun��o", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAODADOS_PF", "Pontos de Fun��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV23DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector1", AV23DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0)));
            }
            cmbavFuncaodados_tipo1.Name = "vFUNCAODADOS_TIPO1";
            cmbavFuncaodados_tipo1.WebTags = "";
            cmbavFuncaodados_tipo1.addItem("", "Todos", 0);
            cmbavFuncaodados_tipo1.addItem("ALI", "ALI", 0);
            cmbavFuncaodados_tipo1.addItem("AIE", "AIE", 0);
            cmbavFuncaodados_tipo1.addItem("DC", "DC", 0);
            if ( cmbavFuncaodados_tipo1.ItemCount > 0 )
            {
               AV26FuncaoDados_Tipo1 = cmbavFuncaodados_tipo1.getValidValue(AV26FuncaoDados_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo1", AV26FuncaoDados_Tipo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAODADOS_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAODADOS_TIPO", "Tipo de Fun��o", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAODADOS_PF", "Pontos de Fun��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV28DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV28DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector2", AV28DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV29DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0)));
            }
            cmbavFuncaodados_tipo2.Name = "vFUNCAODADOS_TIPO2";
            cmbavFuncaodados_tipo2.WebTags = "";
            cmbavFuncaodados_tipo2.addItem("", "Todos", 0);
            cmbavFuncaodados_tipo2.addItem("ALI", "ALI", 0);
            cmbavFuncaodados_tipo2.addItem("AIE", "AIE", 0);
            cmbavFuncaodados_tipo2.addItem("DC", "DC", 0);
            if ( cmbavFuncaodados_tipo2.ItemCount > 0 )
            {
               AV31FuncaoDados_Tipo2 = cmbavFuncaodados_tipo2.getValidValue(AV31FuncaoDados_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31FuncaoDados_Tipo2", AV31FuncaoDados_Tipo2);
            }
            GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_70_idx;
            cmbFuncaoDados_Tipo.Name = GXCCtl;
            cmbFuncaoDados_Tipo.WebTags = "";
            cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
            cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
            cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
            if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
            {
               A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_70_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            GXCCtl = "FUNCAODADOS_ATIVO_" + sGXsfl_70_idx;
            cmbFuncaoDados_Ativo.Name = GXCCtl;
            cmbFuncaoDados_Ativo.WebTags = "";
            cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
            {
               A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_702( ) ;
         while ( nGXsfl_70_idx <= nRC_GXsfl_70 )
         {
            sendrow_702( ) ;
            nGXsfl_70_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_70_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_70_idx+1));
            sGXsfl_70_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_70_idx), 4, 0)), 4, "0");
            SubsflControlProps_702( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV23DynamicFiltersSelector1 ,
                                       short AV24DynamicFiltersOperator1 ,
                                       String AV25FuncaoDados_Nome1 ,
                                       String AV26FuncaoDados_Tipo1 ,
                                       decimal AV37FuncaoDados_PF1 ,
                                       String AV28DynamicFiltersSelector2 ,
                                       short AV29DynamicFiltersOperator2 ,
                                       String AV30FuncaoDados_Nome2 ,
                                       String AV31FuncaoDados_Tipo2 ,
                                       decimal AV38FuncaoDados_PF2 ,
                                       bool AV27DynamicFiltersEnabled2 ,
                                       String AV42TFFuncaoDados_Nome ,
                                       String AV43TFFuncaoDados_Nome_Sel ,
                                       String AV50TFFuncaoDados_FuncaoDadosSisDes ,
                                       String AV51TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                       short AV58TFFuncaoDados_DER ,
                                       short AV59TFFuncaoDados_DER_To ,
                                       short AV62TFFuncaoDados_RLR ,
                                       short AV63TFFuncaoDados_RLR_To ,
                                       decimal AV66TFFuncaoDados_PF ,
                                       decimal AV67TFFuncaoDados_PF_To ,
                                       String AV74TFFuncaoDados_SolucaoTecnica ,
                                       String AV75TFFuncaoDados_SolucaoTecnica_Sel ,
                                       int AV7FuncaoDados_SistemaCod ,
                                       String AV44ddo_FuncaoDados_NomeTitleControlIdToReplace ,
                                       String AV48ddo_FuncaoDados_TipoTitleControlIdToReplace ,
                                       String AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace ,
                                       String AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ,
                                       String AV60ddo_FuncaoDados_DERTitleControlIdToReplace ,
                                       String AV64ddo_FuncaoDados_RLRTitleControlIdToReplace ,
                                       String AV68ddo_FuncaoDados_PFTitleControlIdToReplace ,
                                       String AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace ,
                                       String AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace ,
                                       String AV88Pgmname ,
                                       IGxCollection AV47TFFuncaoDados_Tipo_Sels ,
                                       IGxCollection AV55TFFuncaoDados_Complexidade_Sels ,
                                       IGxCollection AV71TFFuncaoDados_Ativo_Sels ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A391FuncaoDados_FuncaoDadosCod ,
                                       int A368FuncaoDados_Codigo ,
                                       int A370FuncaoDados_SistemaCod ,
                                       int A404FuncaoDados_FuncaoDadosSisCod ,
                                       String A753FuncaoDados_SolucaoTecnica ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9G2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_FUNCAODADOSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A391FuncaoDados_FuncaoDadosCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_NOME", A369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_COMPLEXIDADE", StringUtil.RTrim( A376FuncaoDados_Complexidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_ATIVO", StringUtil.RTrim( A394FuncaoDados_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_SOLUCAOTECNICA", GetSecureSignedToken( sPrefix, A753FuncaoDados_SolucaoTecnica));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_SOLUCAOTECNICA", A753FuncaoDados_SolucaoTecnica);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV23DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector1", AV23DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavFuncaodados_tipo1.ItemCount > 0 )
         {
            AV26FuncaoDados_Tipo1 = cmbavFuncaodados_tipo1.getValidValue(AV26FuncaoDados_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo1", AV26FuncaoDados_Tipo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV28DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV28DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector2", AV28DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV29DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavFuncaodados_tipo2.ItemCount > 0 )
         {
            AV31FuncaoDados_Tipo2 = cmbavFuncaodados_tipo2.getValidValue(AV31FuncaoDados_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31FuncaoDados_Tipo2", AV31FuncaoDados_Tipo2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9G2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV88Pgmname = "SistemaFuncaoDadosWC";
         context.Gx_err = 0;
      }

      protected void RF9G2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 70;
         /* Execute user event: E309G2 */
         E309G2 ();
         nGXsfl_70_idx = 1;
         sGXsfl_70_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_70_idx), 4, 0)), 4, "0");
         SubsflControlProps_702( ) ;
         nGXsfl_70_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_702( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A376FuncaoDados_Complexidade ,
                                                 AV55TFFuncaoDados_Complexidade_Sels ,
                                                 A373FuncaoDados_Tipo ,
                                                 AV47TFFuncaoDados_Tipo_Sels ,
                                                 A394FuncaoDados_Ativo ,
                                                 AV71TFFuncaoDados_Ativo_Sels ,
                                                 AV23DynamicFiltersSelector1 ,
                                                 AV25FuncaoDados_Nome1 ,
                                                 AV26FuncaoDados_Tipo1 ,
                                                 AV27DynamicFiltersEnabled2 ,
                                                 AV28DynamicFiltersSelector2 ,
                                                 AV30FuncaoDados_Nome2 ,
                                                 AV31FuncaoDados_Tipo2 ,
                                                 AV43TFFuncaoDados_Nome_Sel ,
                                                 AV42TFFuncaoDados_Nome ,
                                                 AV47TFFuncaoDados_Tipo_Sels.Count ,
                                                 AV51TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                                 AV50TFFuncaoDados_FuncaoDadosSisDes ,
                                                 AV71TFFuncaoDados_Ativo_Sels.Count ,
                                                 A369FuncaoDados_Nome ,
                                                 A405FuncaoDados_FuncaoDadosSisDes ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A370FuncaoDados_SistemaCod ,
                                                 AV7FuncaoDados_SistemaCod ,
                                                 AV24DynamicFiltersOperator1 ,
                                                 AV37FuncaoDados_PF1 ,
                                                 A377FuncaoDados_PF ,
                                                 AV29DynamicFiltersOperator2 ,
                                                 AV38FuncaoDados_PF2 ,
                                                 AV55TFFuncaoDados_Complexidade_Sels.Count ,
                                                 AV58TFFuncaoDados_DER ,
                                                 A374FuncaoDados_DER ,
                                                 AV59TFFuncaoDados_DER_To ,
                                                 AV62TFFuncaoDados_RLR ,
                                                 A375FuncaoDados_RLR ,
                                                 AV63TFFuncaoDados_RLR_To ,
                                                 AV66TFFuncaoDados_PF ,
                                                 AV67TFFuncaoDados_PF_To ,
                                                 AV75TFFuncaoDados_SolucaoTecnica_Sel ,
                                                 AV74TFFuncaoDados_SolucaoTecnica ,
                                                 A753FuncaoDados_SolucaoTecnica },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                                 }
            });
            lV25FuncaoDados_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV25FuncaoDados_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25FuncaoDados_Nome1", AV25FuncaoDados_Nome1);
            lV30FuncaoDados_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV30FuncaoDados_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30FuncaoDados_Nome2", AV30FuncaoDados_Nome2);
            lV42TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV42TFFuncaoDados_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_Nome", AV42TFFuncaoDados_Nome);
            lV50TFFuncaoDados_FuncaoDadosSisDes = StringUtil.Concat( StringUtil.RTrim( AV50TFFuncaoDados_FuncaoDadosSisDes), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFFuncaoDados_FuncaoDadosSisDes", AV50TFFuncaoDados_FuncaoDadosSisDes);
            /* Using cursor H009G2 */
            pr_default.execute(0, new Object[] {AV7FuncaoDados_SistemaCod, AV55TFFuncaoDados_Complexidade_Sels.Count, lV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, lV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, lV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, lV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel});
            nGXsfl_70_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A394FuncaoDados_Ativo = H009G2_A394FuncaoDados_Ativo[0];
               A405FuncaoDados_FuncaoDadosSisDes = H009G2_A405FuncaoDados_FuncaoDadosSisDes[0];
               n405FuncaoDados_FuncaoDadosSisDes = H009G2_n405FuncaoDados_FuncaoDadosSisDes[0];
               A373FuncaoDados_Tipo = H009G2_A373FuncaoDados_Tipo[0];
               A369FuncaoDados_Nome = H009G2_A369FuncaoDados_Nome[0];
               A404FuncaoDados_FuncaoDadosSisCod = H009G2_A404FuncaoDados_FuncaoDadosSisCod[0];
               n404FuncaoDados_FuncaoDadosSisCod = H009G2_n404FuncaoDados_FuncaoDadosSisCod[0];
               A391FuncaoDados_FuncaoDadosCod = H009G2_A391FuncaoDados_FuncaoDadosCod[0];
               n391FuncaoDados_FuncaoDadosCod = H009G2_n391FuncaoDados_FuncaoDadosCod[0];
               A370FuncaoDados_SistemaCod = H009G2_A370FuncaoDados_SistemaCod[0];
               A755FuncaoDados_Contar = H009G2_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H009G2_n755FuncaoDados_Contar[0];
               A368FuncaoDados_Codigo = H009G2_A368FuncaoDados_Codigo[0];
               A752FuncaoDados_SistemaProjCod = H009G2_A752FuncaoDados_SistemaProjCod[0];
               n752FuncaoDados_SistemaProjCod = H009G2_n752FuncaoDados_SistemaProjCod[0];
               A745FuncaoDados_MelhoraCod = H009G2_A745FuncaoDados_MelhoraCod[0];
               n745FuncaoDados_MelhoraCod = H009G2_n745FuncaoDados_MelhoraCod[0];
               A404FuncaoDados_FuncaoDadosSisCod = H009G2_A404FuncaoDados_FuncaoDadosSisCod[0];
               n404FuncaoDados_FuncaoDadosSisCod = H009G2_n404FuncaoDados_FuncaoDadosSisCod[0];
               A405FuncaoDados_FuncaoDadosSisDes = H009G2_A405FuncaoDados_FuncaoDadosSisDes[0];
               n405FuncaoDados_FuncaoDadosSisDes = H009G2_n405FuncaoDados_FuncaoDadosSisDes[0];
               A752FuncaoDados_SistemaProjCod = H009G2_A752FuncaoDados_SistemaProjCod[0];
               n752FuncaoDados_SistemaProjCod = H009G2_n752FuncaoDados_SistemaProjCod[0];
               if ( ! H009G2_n752FuncaoDados_SistemaProjCod[0] )
               {
                  GXt_char1 = A753FuncaoDados_SolucaoTecnica;
                  new prc_solucaotecnica(context ).execute( ref  A745FuncaoDados_MelhoraCod, ref  A752FuncaoDados_SistemaProjCod,  "D", out  GXt_char1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A752FuncaoDados_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A752FuncaoDados_SistemaProjCod), 6, 0)));
                  A753FuncaoDados_SolucaoTecnica = GXt_char1;
               }
               else
               {
                  A753FuncaoDados_SolucaoTecnica = "";
               }
               if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncaoDados_SolucaoTecnica_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFFuncaoDados_SolucaoTecnica)) ) ) || ( StringUtil.Like( A753FuncaoDados_SolucaoTecnica , StringUtil.PadR( AV74TFFuncaoDados_SolucaoTecnica , 500 , "%"),  ' ' ) ) )
               {
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncaoDados_SolucaoTecnica_Sel)) || ( ( StringUtil.StrCmp(A753FuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel) == 0 ) ) )
                  {
                     GXt_char1 = A376FuncaoDados_Complexidade;
                     new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
                     A376FuncaoDados_Complexidade = GXt_char1;
                     if ( ( AV55TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV55TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
                     {
                        GXt_int2 = A374FuncaoDados_DER;
                        new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                        A374FuncaoDados_DER = GXt_int2;
                        if ( (0==AV58TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV58TFFuncaoDados_DER ) ) )
                        {
                           if ( (0==AV59TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV59TFFuncaoDados_DER_To ) ) )
                           {
                              GXt_int2 = A375FuncaoDados_RLR;
                              new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                              A375FuncaoDados_RLR = GXt_int2;
                              if ( (0==AV62TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV62TFFuncaoDados_RLR ) ) )
                              {
                                 if ( (0==AV63TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV63TFFuncaoDados_RLR_To ) ) )
                                 {
                                    if ( A755FuncaoDados_Contar )
                                    {
                                       GXt_int2 = (short)(A377FuncaoDados_PF);
                                       new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                                       A377FuncaoDados_PF = (decimal)(GXt_int2);
                                    }
                                    else
                                    {
                                       A377FuncaoDados_PF = 0;
                                    }
                                    if ( ! ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV24DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV37FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF < AV37FuncaoDados_PF1 ) ) )
                                    {
                                       if ( ! ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV24DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV37FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF == AV37FuncaoDados_PF1 ) ) )
                                       {
                                          if ( ! ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ( AV24DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV37FuncaoDados_PF1) ) ) || ( ( A377FuncaoDados_PF > AV37FuncaoDados_PF1 ) ) )
                                          {
                                             if ( ! ( AV27DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV29DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV38FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF < AV38FuncaoDados_PF2 ) ) )
                                             {
                                                if ( ! ( AV27DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV29DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV38FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF == AV38FuncaoDados_PF2 ) ) )
                                                {
                                                   if ( ! ( AV27DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ( AV29DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV38FuncaoDados_PF2) ) ) || ( ( A377FuncaoDados_PF > AV38FuncaoDados_PF2 ) ) )
                                                   {
                                                      if ( (Convert.ToDecimal(0)==AV66TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV66TFFuncaoDados_PF ) ) )
                                                      {
                                                         if ( (Convert.ToDecimal(0)==AV67TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV67TFFuncaoDados_PF_To ) ) )
                                                         {
                                                            /* Execute user event: E319G2 */
                                                            E319G2 ();
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 70;
            WB9G0( ) ;
         }
         nGXsfl_70_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9G0( )
      {
         /* Before Start, stand alone formulas. */
         AV88Pgmname = "SistemaFuncaoDadosWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E299G2 */
         E299G2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV77DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_NOMETITLEFILTERDATA"), AV41FuncaoDados_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_TIPOTITLEFILTERDATA"), AV45FuncaoDados_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_FUNCAODADOSSISDESTITLEFILTERDATA"), AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA"), AV53FuncaoDados_ComplexidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_DERTITLEFILTERDATA"), AV57FuncaoDados_DERTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_RLRTITLEFILTERDATA"), AV61FuncaoDados_RLRTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_PFTITLEFILTERDATA"), AV65FuncaoDados_PFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_ATIVOTITLEFILTERDATA"), AV69FuncaoDados_AtivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAODADOS_SOLUCAOTECNICATITLEFILTERDATA"), AV73FuncaoDados_SolucaoTecnicaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV23DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector1", AV23DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV24DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0)));
            AV25FuncaoDados_Nome1 = cgiGet( edtavFuncaodados_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25FuncaoDados_Nome1", AV25FuncaoDados_Nome1);
            cmbavFuncaodados_tipo1.Name = cmbavFuncaodados_tipo1_Internalname;
            cmbavFuncaodados_tipo1.CurrentValue = cgiGet( cmbavFuncaodados_tipo1_Internalname);
            AV26FuncaoDados_Tipo1 = cgiGet( cmbavFuncaodados_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo1", AV26FuncaoDados_Tipo1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaodados_pf1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaodados_pf1_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAODADOS_PF1");
               GX_FocusControl = edtavFuncaodados_pf1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37FuncaoDados_PF1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37FuncaoDados_PF1", StringUtil.LTrim( StringUtil.Str( AV37FuncaoDados_PF1, 14, 5)));
            }
            else
            {
               AV37FuncaoDados_PF1 = context.localUtil.CToN( cgiGet( edtavFuncaodados_pf1_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37FuncaoDados_PF1", StringUtil.LTrim( StringUtil.Str( AV37FuncaoDados_PF1, 14, 5)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV28DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector2", AV28DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV29DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0)));
            AV30FuncaoDados_Nome2 = cgiGet( edtavFuncaodados_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30FuncaoDados_Nome2", AV30FuncaoDados_Nome2);
            cmbavFuncaodados_tipo2.Name = cmbavFuncaodados_tipo2_Internalname;
            cmbavFuncaodados_tipo2.CurrentValue = cgiGet( cmbavFuncaodados_tipo2_Internalname);
            AV31FuncaoDados_Tipo2 = cgiGet( cmbavFuncaodados_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31FuncaoDados_Tipo2", AV31FuncaoDados_Tipo2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaodados_pf2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaodados_pf2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAODADOS_PF2");
               GX_FocusControl = edtavFuncaodados_pf2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38FuncaoDados_PF2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoDados_PF2", StringUtil.LTrim( StringUtil.Str( AV38FuncaoDados_PF2, 14, 5)));
            }
            else
            {
               AV38FuncaoDados_PF2 = context.localUtil.CToN( cgiGet( edtavFuncaodados_pf2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoDados_PF2", StringUtil.LTrim( StringUtil.Str( AV38FuncaoDados_PF2, 14, 5)));
            }
            AV27DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled2", AV27DynamicFiltersEnabled2);
            AV42TFFuncaoDados_Nome = cgiGet( edtavTffuncaodados_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_Nome", AV42TFFuncaoDados_Nome);
            AV43TFFuncaoDados_Nome_Sel = cgiGet( edtavTffuncaodados_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_Nome_Sel", AV43TFFuncaoDados_Nome_Sel);
            AV50TFFuncaoDados_FuncaoDadosSisDes = StringUtil.Upper( cgiGet( edtavTffuncaodados_funcaodadossisdes_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFFuncaoDados_FuncaoDadosSisDes", AV50TFFuncaoDados_FuncaoDadosSisDes);
            AV51TFFuncaoDados_FuncaoDadosSisDes_Sel = StringUtil.Upper( cgiGet( edtavTffuncaodados_funcaodadossisdes_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoDados_FuncaoDadosSisDes_Sel", AV51TFFuncaoDados_FuncaoDadosSisDes_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER");
               GX_FocusControl = edtavTffuncaodados_der_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFFuncaoDados_DER = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0)));
            }
            else
            {
               AV58TFFuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER_TO");
               GX_FocusControl = edtavTffuncaodados_der_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFFuncaoDados_DER_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0)));
            }
            else
            {
               AV59TFFuncaoDados_DER_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR");
               GX_FocusControl = edtavTffuncaodados_rlr_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFFuncaoDados_RLR = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0)));
            }
            else
            {
               AV62TFFuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR_TO");
               GX_FocusControl = edtavTffuncaodados_rlr_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFFuncaoDados_RLR_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0)));
            }
            else
            {
               AV63TFFuncaoDados_RLR_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF");
               GX_FocusControl = edtavTffuncaodados_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFFuncaoDados_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5)));
            }
            else
            {
               AV66TFFuncaoDados_PF = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF_TO");
               GX_FocusControl = edtavTffuncaodados_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFFuncaoDados_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5)));
            }
            else
            {
               AV67TFFuncaoDados_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5)));
            }
            AV74TFFuncaoDados_SolucaoTecnica = cgiGet( edtavTffuncaodados_solucaotecnica_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFFuncaoDados_SolucaoTecnica", AV74TFFuncaoDados_SolucaoTecnica);
            AV75TFFuncaoDados_SolucaoTecnica_Sel = cgiGet( edtavTffuncaodados_solucaotecnica_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75TFFuncaoDados_SolucaoTecnica_Sel", AV75TFFuncaoDados_SolucaoTecnica_Sel);
            AV44ddo_FuncaoDados_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_FuncaoDados_NomeTitleControlIdToReplace", AV44ddo_FuncaoDados_NomeTitleControlIdToReplace);
            AV48ddo_FuncaoDados_TipoTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_FuncaoDados_TipoTitleControlIdToReplace", AV48ddo_FuncaoDados_TipoTitleControlIdToReplace);
            AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace", AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace);
            AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
            AV60ddo_FuncaoDados_DERTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ddo_FuncaoDados_DERTitleControlIdToReplace", AV60ddo_FuncaoDados_DERTitleControlIdToReplace);
            AV64ddo_FuncaoDados_RLRTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_FuncaoDados_RLRTitleControlIdToReplace", AV64ddo_FuncaoDados_RLRTitleControlIdToReplace);
            AV68ddo_FuncaoDados_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_FuncaoDados_PFTitleControlIdToReplace", AV68ddo_FuncaoDados_PFTitleControlIdToReplace);
            AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace);
            AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace", AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_70 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_70"), ",", "."));
            AV79GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV80GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoDados_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaodados_nome_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Caption");
            Ddo_funcaodados_nome_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Tooltip");
            Ddo_funcaodados_nome_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Cls");
            Ddo_funcaodados_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_set");
            Ddo_funcaodados_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_set");
            Ddo_funcaodados_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Dropdownoptionstype");
            Ddo_funcaodados_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace");
            Ddo_funcaodados_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includesortasc"));
            Ddo_funcaodados_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includesortdsc"));
            Ddo_funcaodados_nome_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Sortedstatus");
            Ddo_funcaodados_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includefilter"));
            Ddo_funcaodados_nome_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filtertype");
            Ddo_funcaodados_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filterisrange"));
            Ddo_funcaodados_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Includedatalist"));
            Ddo_funcaodados_nome_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Datalisttype");
            Ddo_funcaodados_nome_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Datalistproc");
            Ddo_funcaodados_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_nome_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Sortasc");
            Ddo_funcaodados_nome_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Sortdsc");
            Ddo_funcaodados_nome_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Loadingdata");
            Ddo_funcaodados_nome_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Cleanfilter");
            Ddo_funcaodados_nome_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Noresultsfound");
            Ddo_funcaodados_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Searchbuttontext");
            Ddo_funcaodados_tipo_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Caption");
            Ddo_funcaodados_tipo_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Tooltip");
            Ddo_funcaodados_tipo_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Cls");
            Ddo_funcaodados_tipo_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Selectedvalue_set");
            Ddo_funcaodados_tipo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Dropdownoptionstype");
            Ddo_funcaodados_tipo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Titlecontrolidtoreplace");
            Ddo_funcaodados_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Includesortasc"));
            Ddo_funcaodados_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Includesortdsc"));
            Ddo_funcaodados_tipo_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Sortedstatus");
            Ddo_funcaodados_tipo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Includefilter"));
            Ddo_funcaodados_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Includedatalist"));
            Ddo_funcaodados_tipo_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Datalisttype");
            Ddo_funcaodados_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Allowmultipleselection"));
            Ddo_funcaodados_tipo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Datalistfixedvalues");
            Ddo_funcaodados_tipo_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Sortasc");
            Ddo_funcaodados_tipo_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Sortdsc");
            Ddo_funcaodados_tipo_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Cleanfilter");
            Ddo_funcaodados_tipo_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Searchbuttontext");
            Ddo_funcaodados_funcaodadossisdes_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Caption");
            Ddo_funcaodados_funcaodadossisdes_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Tooltip");
            Ddo_funcaodados_funcaodadossisdes_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Cls");
            Ddo_funcaodados_funcaodadossisdes_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filteredtext_set");
            Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Selectedvalue_set");
            Ddo_funcaodados_funcaodadossisdes_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Dropdownoptionstype");
            Ddo_funcaodados_funcaodadossisdes_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Titlecontrolidtoreplace");
            Ddo_funcaodados_funcaodadossisdes_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includesortasc"));
            Ddo_funcaodados_funcaodadossisdes_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includesortdsc"));
            Ddo_funcaodados_funcaodadossisdes_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Sortedstatus");
            Ddo_funcaodados_funcaodadossisdes_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includefilter"));
            Ddo_funcaodados_funcaodadossisdes_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filtertype");
            Ddo_funcaodados_funcaodadossisdes_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filterisrange"));
            Ddo_funcaodados_funcaodadossisdes_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Includedatalist"));
            Ddo_funcaodados_funcaodadossisdes_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Datalisttype");
            Ddo_funcaodados_funcaodadossisdes_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Datalistproc");
            Ddo_funcaodados_funcaodadossisdes_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_funcaodadossisdes_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Sortasc");
            Ddo_funcaodados_funcaodadossisdes_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Sortdsc");
            Ddo_funcaodados_funcaodadossisdes_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Loadingdata");
            Ddo_funcaodados_funcaodadossisdes_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Cleanfilter");
            Ddo_funcaodados_funcaodadossisdes_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Noresultsfound");
            Ddo_funcaodados_funcaodadossisdes_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Searchbuttontext");
            Ddo_funcaodados_complexidade_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Caption");
            Ddo_funcaodados_complexidade_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip");
            Ddo_funcaodados_complexidade_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cls");
            Ddo_funcaodados_complexidade_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set");
            Ddo_funcaodados_complexidade_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype");
            Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace");
            Ddo_funcaodados_complexidade_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc"));
            Ddo_funcaodados_complexidade_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc"));
            Ddo_funcaodados_complexidade_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter"));
            Ddo_funcaodados_complexidade_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist"));
            Ddo_funcaodados_complexidade_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype");
            Ddo_funcaodados_complexidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection"));
            Ddo_funcaodados_complexidade_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues");
            Ddo_funcaodados_complexidade_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter");
            Ddo_funcaodados_complexidade_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext");
            Ddo_funcaodados_der_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Caption");
            Ddo_funcaodados_der_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Tooltip");
            Ddo_funcaodados_der_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Cls");
            Ddo_funcaodados_der_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_set");
            Ddo_funcaodados_der_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_set");
            Ddo_funcaodados_der_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Dropdownoptionstype");
            Ddo_funcaodados_der_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace");
            Ddo_funcaodados_der_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includesortasc"));
            Ddo_funcaodados_der_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includesortdsc"));
            Ddo_funcaodados_der_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includefilter"));
            Ddo_funcaodados_der_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filtertype");
            Ddo_funcaodados_der_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filterisrange"));
            Ddo_funcaodados_der_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Includedatalist"));
            Ddo_funcaodados_der_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Cleanfilter");
            Ddo_funcaodados_der_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterfrom");
            Ddo_funcaodados_der_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Rangefilterto");
            Ddo_funcaodados_der_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Searchbuttontext");
            Ddo_funcaodados_rlr_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Caption");
            Ddo_funcaodados_rlr_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Tooltip");
            Ddo_funcaodados_rlr_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Cls");
            Ddo_funcaodados_rlr_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_set");
            Ddo_funcaodados_rlr_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_set");
            Ddo_funcaodados_rlr_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Dropdownoptionstype");
            Ddo_funcaodados_rlr_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace");
            Ddo_funcaodados_rlr_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includesortasc"));
            Ddo_funcaodados_rlr_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includesortdsc"));
            Ddo_funcaodados_rlr_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includefilter"));
            Ddo_funcaodados_rlr_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filtertype");
            Ddo_funcaodados_rlr_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filterisrange"));
            Ddo_funcaodados_rlr_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Includedatalist"));
            Ddo_funcaodados_rlr_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Cleanfilter");
            Ddo_funcaodados_rlr_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterfrom");
            Ddo_funcaodados_rlr_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Rangefilterto");
            Ddo_funcaodados_rlr_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Searchbuttontext");
            Ddo_funcaodados_pf_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Caption");
            Ddo_funcaodados_pf_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Tooltip");
            Ddo_funcaodados_pf_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Cls");
            Ddo_funcaodados_pf_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_set");
            Ddo_funcaodados_pf_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_set");
            Ddo_funcaodados_pf_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Dropdownoptionstype");
            Ddo_funcaodados_pf_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace");
            Ddo_funcaodados_pf_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includesortasc"));
            Ddo_funcaodados_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includesortdsc"));
            Ddo_funcaodados_pf_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includefilter"));
            Ddo_funcaodados_pf_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filtertype");
            Ddo_funcaodados_pf_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filterisrange"));
            Ddo_funcaodados_pf_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Includedatalist"));
            Ddo_funcaodados_pf_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Cleanfilter");
            Ddo_funcaodados_pf_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterfrom");
            Ddo_funcaodados_pf_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Rangefilterto");
            Ddo_funcaodados_pf_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Searchbuttontext");
            Ddo_funcaodados_ativo_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Caption");
            Ddo_funcaodados_ativo_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Tooltip");
            Ddo_funcaodados_ativo_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Cls");
            Ddo_funcaodados_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Selectedvalue_set");
            Ddo_funcaodados_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Dropdownoptionstype");
            Ddo_funcaodados_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Titlecontrolidtoreplace");
            Ddo_funcaodados_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Includesortasc"));
            Ddo_funcaodados_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Includesortdsc"));
            Ddo_funcaodados_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Sortedstatus");
            Ddo_funcaodados_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Includefilter"));
            Ddo_funcaodados_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Includedatalist"));
            Ddo_funcaodados_ativo_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Datalisttype");
            Ddo_funcaodados_ativo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Allowmultipleselection"));
            Ddo_funcaodados_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Datalistfixedvalues");
            Ddo_funcaodados_ativo_Sortasc = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Sortasc");
            Ddo_funcaodados_ativo_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Sortdsc");
            Ddo_funcaodados_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Cleanfilter");
            Ddo_funcaodados_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Searchbuttontext");
            Ddo_funcaodados_solucaotecnica_Caption = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Caption");
            Ddo_funcaodados_solucaotecnica_Tooltip = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Tooltip");
            Ddo_funcaodados_solucaotecnica_Cls = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Cls");
            Ddo_funcaodados_solucaotecnica_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filteredtext_set");
            Ddo_funcaodados_solucaotecnica_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Selectedvalue_set");
            Ddo_funcaodados_solucaotecnica_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Dropdownoptionstype");
            Ddo_funcaodados_solucaotecnica_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Titlecontrolidtoreplace");
            Ddo_funcaodados_solucaotecnica_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includesortasc"));
            Ddo_funcaodados_solucaotecnica_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includesortdsc"));
            Ddo_funcaodados_solucaotecnica_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includefilter"));
            Ddo_funcaodados_solucaotecnica_Filtertype = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filtertype");
            Ddo_funcaodados_solucaotecnica_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filterisrange"));
            Ddo_funcaodados_solucaotecnica_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Includedatalist"));
            Ddo_funcaodados_solucaotecnica_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Datalisttype");
            Ddo_funcaodados_solucaotecnica_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Datalistproc");
            Ddo_funcaodados_solucaotecnica_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_solucaotecnica_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Loadingdata");
            Ddo_funcaodados_solucaotecnica_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Cleanfilter");
            Ddo_funcaodados_solucaotecnica_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Noresultsfound");
            Ddo_funcaodados_solucaotecnica_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaodados_nome_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Activeeventkey");
            Ddo_funcaodados_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Filteredtext_get");
            Ddo_funcaodados_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_NOME_Selectedvalue_get");
            Ddo_funcaodados_tipo_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Activeeventkey");
            Ddo_funcaodados_tipo_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_TIPO_Selectedvalue_get");
            Ddo_funcaodados_funcaodadossisdes_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Activeeventkey");
            Ddo_funcaodados_funcaodadossisdes_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Filteredtext_get");
            Ddo_funcaodados_funcaodadossisdes_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES_Selectedvalue_get");
            Ddo_funcaodados_complexidade_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey");
            Ddo_funcaodados_complexidade_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get");
            Ddo_funcaodados_der_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Activeeventkey");
            Ddo_funcaodados_der_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtext_get");
            Ddo_funcaodados_der_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_DER_Filteredtextto_get");
            Ddo_funcaodados_rlr_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Activeeventkey");
            Ddo_funcaodados_rlr_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtext_get");
            Ddo_funcaodados_rlr_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_RLR_Filteredtextto_get");
            Ddo_funcaodados_pf_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Activeeventkey");
            Ddo_funcaodados_pf_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtext_get");
            Ddo_funcaodados_pf_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_PF_Filteredtextto_get");
            Ddo_funcaodados_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Activeeventkey");
            Ddo_funcaodados_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_ATIVO_Selectedvalue_get");
            Ddo_funcaodados_solucaotecnica_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Activeeventkey");
            Ddo_funcaodados_solucaotecnica_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Filteredtext_get");
            Ddo_funcaodados_solucaotecnica_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV23DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME1"), AV25FuncaoDados_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_TIPO1"), AV26FuncaoDados_Tipo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAODADOS_PF1"), ",", ".") != AV37FuncaoDados_PF1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV28DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV29DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_NOME2"), AV30FuncaoDados_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAODADOS_TIPO2"), AV31FuncaoDados_Tipo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAODADOS_PF2"), ",", ".") != AV38FuncaoDados_PF2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV27DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME"), AV42TFFuncaoDados_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_NOME_SEL"), AV43TFFuncaoDados_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_FUNCAODADOSSISDES"), AV50TFFuncaoDados_FuncaoDadosSisDes) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL"), AV51TFFuncaoDados_FuncaoDadosSisDes_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER"), ",", ".") != Convert.ToDecimal( AV58TFFuncaoDados_DER )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_DER_TO"), ",", ".") != Convert.ToDecimal( AV59TFFuncaoDados_DER_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR"), ",", ".") != Convert.ToDecimal( AV62TFFuncaoDados_RLR )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_RLR_TO"), ",", ".") != Convert.ToDecimal( AV63TFFuncaoDados_RLR_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF"), ",", ".") != AV66TFFuncaoDados_PF )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_PF_TO"), ",", ".") != AV67TFFuncaoDados_PF_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_SOLUCAOTECNICA"), AV74TFFuncaoDados_SolucaoTecnica) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAODADOS_SOLUCAOTECNICA_SEL"), AV75TFFuncaoDados_SolucaoTecnica_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E299G2 */
         E299G2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E299G2( )
      {
         /* Start Routine */
         subGrid_Rows = 50;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV26FuncaoDados_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo1", AV26FuncaoDados_Tipo1);
         AV23DynamicFiltersSelector1 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector1", AV23DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV31FuncaoDados_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31FuncaoDados_Tipo2", AV31FuncaoDados_Tipo2);
         AV28DynamicFiltersSelector2 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector2", AV28DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTffuncaodados_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_Visible), 5, 0)));
         edtavTffuncaodados_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_sel_Visible), 5, 0)));
         edtavTffuncaodados_funcaodadossisdes_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_funcaodadossisdes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_funcaodadossisdes_Visible), 5, 0)));
         edtavTffuncaodados_funcaodadossisdes_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_funcaodadossisdes_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_funcaodadossisdes_sel_Visible), 5, 0)));
         edtavTffuncaodados_der_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_der_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_Visible), 5, 0)));
         edtavTffuncaodados_der_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_der_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_to_Visible), 5, 0)));
         edtavTffuncaodados_rlr_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_rlr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_Visible), 5, 0)));
         edtavTffuncaodados_rlr_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_rlr_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_to_Visible), 5, 0)));
         edtavTffuncaodados_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_Visible), 5, 0)));
         edtavTffuncaodados_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_to_Visible), 5, 0)));
         edtavTffuncaodados_solucaotecnica_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_solucaotecnica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_solucaotecnica_Visible), 5, 0)));
         edtavTffuncaodados_solucaotecnica_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaodados_solucaotecnica_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_solucaotecnica_sel_Visible), 5, 0)));
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_nome_Titlecontrolidtoreplace);
         AV44ddo_FuncaoDados_NomeTitleControlIdToReplace = Ddo_funcaodados_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_FuncaoDados_NomeTitleControlIdToReplace", AV44ddo_FuncaoDados_NomeTitleControlIdToReplace);
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_tipo_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_tipo_Titlecontrolidtoreplace);
         AV48ddo_FuncaoDados_TipoTitleControlIdToReplace = Ddo_funcaodados_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_FuncaoDados_TipoTitleControlIdToReplace", AV48ddo_FuncaoDados_TipoTitleControlIdToReplace);
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_funcaodadossisdes_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_FuncaoDadosSisDes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_funcaodadossisdes_Titlecontrolidtoreplace);
         AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace = Ddo_funcaodados_funcaodadossisdes_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace", AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace);
         edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Complexidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_complexidade_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_complexidade_Titlecontrolidtoreplace);
         AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = Ddo_funcaodados_complexidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_der_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_DER";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_der_Titlecontrolidtoreplace);
         AV60ddo_FuncaoDados_DERTitleControlIdToReplace = Ddo_funcaodados_der_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ddo_FuncaoDados_DERTitleControlIdToReplace", AV60ddo_FuncaoDados_DERTitleControlIdToReplace);
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_RLR";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_rlr_Titlecontrolidtoreplace);
         AV64ddo_FuncaoDados_RLRTitleControlIdToReplace = Ddo_funcaodados_rlr_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_FuncaoDados_RLRTitleControlIdToReplace", AV64ddo_FuncaoDados_RLRTitleControlIdToReplace);
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_pf_Titlecontrolidtoreplace);
         AV68ddo_FuncaoDados_PFTitleControlIdToReplace = Ddo_funcaodados_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_FuncaoDados_PFTitleControlIdToReplace", AV68ddo_FuncaoDados_PFTitleControlIdToReplace);
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_ativo_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_ativo_Titlecontrolidtoreplace);
         AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace = Ddo_funcaodados_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace);
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_solucaotecnica_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_SolucaoTecnica";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_solucaotecnica_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_solucaotecnica_Titlecontrolidtoreplace);
         AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace = Ddo_funcaodados_solucaotecnica_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace", AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace);
         edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Cronologicamente", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Tipo", 0);
         cmbavOrderedby.addItem("4", "Sistema externo", 0);
         cmbavOrderedby.addItem("5", "Status", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = AV77DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3) ;
         AV77DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3;
         /* Using cursor H009G3 */
         pr_default.execute(1, new Object[] {AV7FuncaoDados_SistemaCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A127Sistema_Codigo = H009G3_A127Sistema_Codigo[0];
            A699Sistema_Tipo = H009G3_A699Sistema_Tipo[0];
            n699Sistema_Tipo = H009G3_n699Sistema_Tipo[0];
            edtFuncaoDados_SolucaoTecnica_Visible = ((StringUtil.StrCmp(A699Sistema_Tipo, "M")==0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_SolucaoTecnica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_SolucaoTecnica_Visible), 5, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void E309G2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV41FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45FuncaoDados_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69FuncaoDados_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73FuncaoDados_SolucaoTecnicaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoDados_Nome_Titleformat = 2;
         edtFuncaoDados_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_Nome_Internalname, "Title", edtFuncaoDados_Nome_Title);
         cmbFuncaoDados_Tipo_Titleformat = 2;
         cmbFuncaoDados_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Tipo_Internalname, "Title", cmbFuncaoDados_Tipo.Title.Text);
         edtFuncaoDados_FuncaoDadosSisDes_Titleformat = 2;
         edtFuncaoDados_FuncaoDadosSisDes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema externo", AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_FuncaoDadosSisDes_Internalname, "Title", edtFuncaoDados_FuncaoDadosSisDes_Title);
         cmbFuncaoDados_Complexidade_Titleformat = 2;
         cmbFuncaoDados_Complexidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Complexidade", AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Complexidade_Internalname, "Title", cmbFuncaoDados_Complexidade.Title.Text);
         edtFuncaoDados_DER_Titleformat = 2;
         edtFuncaoDados_DER_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "DER", AV60ddo_FuncaoDados_DERTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_DER_Internalname, "Title", edtFuncaoDados_DER_Title);
         edtFuncaoDados_RLR_Titleformat = 2;
         edtFuncaoDados_RLR_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "RLR", AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_RLR_Internalname, "Title", edtFuncaoDados_RLR_Title);
         edtFuncaoDados_PF_Titleformat = 2;
         edtFuncaoDados_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV68ddo_FuncaoDados_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_PF_Internalname, "Title", edtFuncaoDados_PF_Title);
         cmbFuncaoDados_Ativo_Titleformat = 2;
         cmbFuncaoDados_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Ativo_Internalname, "Title", cmbFuncaoDados_Ativo.Title.Text);
         edtFuncaoDados_SolucaoTecnica_Titleformat = 2;
         edtFuncaoDados_SolucaoTecnica_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Solu��o t�cnica", AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoDados_SolucaoTecnica_Internalname, "Title", edtFuncaoDados_SolucaoTecnica_Title);
         AV79GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV79GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridCurrentPage), 10, 0)));
         AV80GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV80GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV41FuncaoDados_NomeTitleFilterData", AV41FuncaoDados_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV45FuncaoDados_TipoTitleFilterData", AV45FuncaoDados_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData", AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV53FuncaoDados_ComplexidadeTitleFilterData", AV53FuncaoDados_ComplexidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV57FuncaoDados_DERTitleFilterData", AV57FuncaoDados_DERTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV61FuncaoDados_RLRTitleFilterData", AV61FuncaoDados_RLRTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV65FuncaoDados_PFTitleFilterData", AV65FuncaoDados_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV69FuncaoDados_AtivoTitleFilterData", AV69FuncaoDados_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV73FuncaoDados_SolucaoTecnicaTitleFilterData", AV73FuncaoDados_SolucaoTecnicaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E119G2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV78PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV78PageToGo) ;
         }
      }

      protected void E129G2( )
      {
         /* Ddo_funcaodados_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFFuncaoDados_Nome = Ddo_funcaodados_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_Nome", AV42TFFuncaoDados_Nome);
            AV43TFFuncaoDados_Nome_Sel = Ddo_funcaodados_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_Nome_Sel", AV43TFFuncaoDados_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E139G2( )
      {
         /* Ddo_funcaodados_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFFuncaoDados_Tipo_SelsJson = Ddo_funcaodados_tipo_Selectedvalue_get;
            AV47TFFuncaoDados_Tipo_Sels.FromJSonString(AV46TFFuncaoDados_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47TFFuncaoDados_Tipo_Sels", AV47TFFuncaoDados_Tipo_Sels);
      }

      protected void E149G2( )
      {
         /* Ddo_funcaodados_funcaodadossisdes_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_funcaodadossisdes_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_funcaodadossisdes_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "SortedStatus", Ddo_funcaodados_funcaodadossisdes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_funcaodadossisdes_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_funcaodadossisdes_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "SortedStatus", Ddo_funcaodados_funcaodadossisdes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_funcaodadossisdes_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFFuncaoDados_FuncaoDadosSisDes = Ddo_funcaodados_funcaodadossisdes_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFFuncaoDados_FuncaoDadosSisDes", AV50TFFuncaoDados_FuncaoDadosSisDes);
            AV51TFFuncaoDados_FuncaoDadosSisDes_Sel = Ddo_funcaodados_funcaodadossisdes_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoDados_FuncaoDadosSisDes_Sel", AV51TFFuncaoDados_FuncaoDadosSisDes_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E159G2( )
      {
         /* Ddo_funcaodados_complexidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_complexidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFFuncaoDados_Complexidade_SelsJson = Ddo_funcaodados_complexidade_Selectedvalue_get;
            AV55TFFuncaoDados_Complexidade_Sels.FromJSonString(AV54TFFuncaoDados_Complexidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV55TFFuncaoDados_Complexidade_Sels", AV55TFFuncaoDados_Complexidade_Sels);
      }

      protected void E169G2( )
      {
         /* Ddo_funcaodados_der_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_der_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFFuncaoDados_DER = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0)));
            AV59TFFuncaoDados_DER_To = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E179G2( )
      {
         /* Ddo_funcaodados_rlr_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_rlr_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFFuncaoDados_RLR = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0)));
            AV63TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E189G2( )
      {
         /* Ddo_funcaodados_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFFuncaoDados_PF = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5)));
            AV67TFFuncaoDados_PF_To = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E199G2( )
      {
         /* Ddo_funcaodados_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaodados_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFFuncaoDados_Ativo_SelsJson = Ddo_funcaodados_ativo_Selectedvalue_get;
            AV71TFFuncaoDados_Ativo_Sels.FromJSonString(AV70TFFuncaoDados_Ativo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV71TFFuncaoDados_Ativo_Sels", AV71TFFuncaoDados_Ativo_Sels);
      }

      protected void E209G2( )
      {
         /* Ddo_funcaodados_solucaotecnica_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_solucaotecnica_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFFuncaoDados_SolucaoTecnica = Ddo_funcaodados_solucaotecnica_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFFuncaoDados_SolucaoTecnica", AV74TFFuncaoDados_SolucaoTecnica);
            AV75TFFuncaoDados_SolucaoTecnica_Sel = Ddo_funcaodados_solucaotecnica_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75TFFuncaoDados_SolucaoTecnica_Sel", AV75TFFuncaoDados_SolucaoTecnica_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E319G2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Visible = (((0==A391FuncaoDados_FuncaoDadosCod)) ? 1 : 0);
         AV15Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV15Update);
         AV85Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("funcaodados.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod);
         AV16Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV16Delete);
         AV86Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("funcaodados.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +AV7FuncaoDados_SistemaCod);
         AV17Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV17Display);
         AV87Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("wp_funcaodadosdetalhes.aspx") + "?" + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod);
         edtFuncaoDados_Nome_Link = formatLink("viewfuncaodados.aspx") + "?" + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( ! (0==A391FuncaoDados_FuncaoDadosCod) )
         {
            edtavDisplay_Link = formatLink("wp_funcaodadosdetalhes.aspx") + "?" + UrlEncode("" +A391FuncaoDados_FuncaoDadosCod) + "," + UrlEncode("" +A404FuncaoDados_FuncaoDadosSisCod);
         }
         edtFuncaoDados_SolucaoTecnica_Tooltiptext = A753FuncaoDados_SolucaoTecnica;
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 70;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_702( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_70_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(70, GridRow);
         }
      }

      protected void E219G2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E269G2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV27DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled2", AV27DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E229G2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaodados_tipo1.CurrentValue = StringUtil.RTrim( AV26FuncaoDados_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo1_Internalname, "Values", cmbavFuncaodados_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaodados_tipo2.CurrentValue = StringUtil.RTrim( AV31FuncaoDados_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo2_Internalname, "Values", cmbavFuncaodados_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E279G2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E239G2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV27DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled2", AV27DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23DynamicFiltersSelector1, AV24DynamicFiltersOperator1, AV25FuncaoDados_Nome1, AV26FuncaoDados_Tipo1, AV37FuncaoDados_PF1, AV28DynamicFiltersSelector2, AV29DynamicFiltersOperator2, AV30FuncaoDados_Nome2, AV31FuncaoDados_Tipo2, AV38FuncaoDados_PF2, AV27DynamicFiltersEnabled2, AV42TFFuncaoDados_Nome, AV43TFFuncaoDados_Nome_Sel, AV50TFFuncaoDados_FuncaoDadosSisDes, AV51TFFuncaoDados_FuncaoDadosSisDes_Sel, AV58TFFuncaoDados_DER, AV59TFFuncaoDados_DER_To, AV62TFFuncaoDados_RLR, AV63TFFuncaoDados_RLR_To, AV66TFFuncaoDados_PF, AV67TFFuncaoDados_PF_To, AV74TFFuncaoDados_SolucaoTecnica, AV75TFFuncaoDados_SolucaoTecnica_Sel, AV7FuncaoDados_SistemaCod, AV44ddo_FuncaoDados_NomeTitleControlIdToReplace, AV48ddo_FuncaoDados_TipoTitleControlIdToReplace, AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace, AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV60ddo_FuncaoDados_DERTitleControlIdToReplace, AV64ddo_FuncaoDados_RLRTitleControlIdToReplace, AV68ddo_FuncaoDados_PFTitleControlIdToReplace, AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace, AV88Pgmname, AV47TFFuncaoDados_Tipo_Sels, AV55TFFuncaoDados_Complexidade_Sels, AV71TFFuncaoDados_Ativo_Sels, AV11GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A391FuncaoDados_FuncaoDadosCod, A368FuncaoDados_Codigo, A370FuncaoDados_SistemaCod, A404FuncaoDados_FuncaoDadosSisCod, A753FuncaoDados_SolucaoTecnica, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaodados_tipo1.CurrentValue = StringUtil.RTrim( AV26FuncaoDados_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo1_Internalname, "Values", cmbavFuncaodados_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaodados_tipo2.CurrentValue = StringUtil.RTrim( AV31FuncaoDados_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo2_Internalname, "Values", cmbavFuncaodados_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E289G2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E249G2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47TFFuncaoDados_Tipo_Sels", AV47TFFuncaoDados_Tipo_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV55TFFuncaoDados_Complexidade_Sels", AV55TFFuncaoDados_Complexidade_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV71TFFuncaoDados_Ativo_Sels", AV71TFFuncaoDados_Ativo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavFuncaodados_tipo1.CurrentValue = StringUtil.RTrim( AV26FuncaoDados_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo1_Internalname, "Values", cmbavFuncaodados_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaodados_tipo2.CurrentValue = StringUtil.RTrim( AV31FuncaoDados_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo2_Internalname, "Values", cmbavFuncaodados_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E259G2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("funcaodados.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7FuncaoDados_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaodados_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
         Ddo_funcaodados_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
         Ddo_funcaodados_funcaodadossisdes_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "SortedStatus", Ddo_funcaodados_funcaodadossisdes_Sortedstatus);
         Ddo_funcaodados_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_funcaodados_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_funcaodados_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_funcaodados_funcaodadossisdes_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "SortedStatus", Ddo_funcaodados_funcaodadossisdes_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_funcaodados_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaodados_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome1_Visible), 5, 0)));
         cmbavFuncaodados_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo1.Visible), 5, 0)));
         edtavFuncaodados_pf1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_pf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_pf1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
         {
            edtavFuncaodados_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 )
         {
            cmbavFuncaodados_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 )
         {
            edtavFuncaodados_pf1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_pf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_pf1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaodados_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome2_Visible), 5, 0)));
         cmbavFuncaodados_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo2.Visible), 5, 0)));
         edtavFuncaodados_pf2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_pf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_pf2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
         {
            edtavFuncaodados_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 )
         {
            cmbavFuncaodados_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaodados_tipo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 )
         {
            edtavFuncaodados_pf2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaodados_pf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_pf2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV27DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled2", AV27DynamicFiltersEnabled2);
         AV28DynamicFiltersSelector2 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector2", AV28DynamicFiltersSelector2);
         AV30FuncaoDados_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30FuncaoDados_Nome2", AV30FuncaoDados_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV42TFFuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_Nome", AV42TFFuncaoDados_Nome);
         Ddo_funcaodados_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "FilteredText_set", Ddo_funcaodados_nome_Filteredtext_set);
         AV43TFFuncaoDados_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_Nome_Sel", AV43TFFuncaoDados_Nome_Sel);
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SelectedValue_set", Ddo_funcaodados_nome_Selectedvalue_set);
         AV47TFFuncaoDados_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_tipo_Internalname, "SelectedValue_set", Ddo_funcaodados_tipo_Selectedvalue_set);
         AV50TFFuncaoDados_FuncaoDadosSisDes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFFuncaoDados_FuncaoDadosSisDes", AV50TFFuncaoDados_FuncaoDadosSisDes);
         Ddo_funcaodados_funcaodadossisdes_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "FilteredText_set", Ddo_funcaodados_funcaodadossisdes_Filteredtext_set);
         AV51TFFuncaoDados_FuncaoDadosSisDes_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoDados_FuncaoDadosSisDes_Sel", AV51TFFuncaoDados_FuncaoDadosSisDes_Sel);
         Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "SelectedValue_set", Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set);
         AV55TFFuncaoDados_Complexidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_complexidade_Internalname, "SelectedValue_set", Ddo_funcaodados_complexidade_Selectedvalue_set);
         AV58TFFuncaoDados_DER = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0)));
         Ddo_funcaodados_der_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredText_set", Ddo_funcaodados_der_Filteredtext_set);
         AV59TFFuncaoDados_DER_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0)));
         Ddo_funcaodados_der_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredTextTo_set", Ddo_funcaodados_der_Filteredtextto_set);
         AV62TFFuncaoDados_RLR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredText_set", Ddo_funcaodados_rlr_Filteredtext_set);
         AV63TFFuncaoDados_RLR_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredTextTo_set", Ddo_funcaodados_rlr_Filteredtextto_set);
         AV66TFFuncaoDados_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5)));
         Ddo_funcaodados_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredText_set", Ddo_funcaodados_pf_Filteredtext_set);
         AV67TFFuncaoDados_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5)));
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredTextTo_set", Ddo_funcaodados_pf_Filteredtextto_set);
         AV71TFFuncaoDados_Ativo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_ativo_Internalname, "SelectedValue_set", Ddo_funcaodados_ativo_Selectedvalue_set);
         AV74TFFuncaoDados_SolucaoTecnica = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFFuncaoDados_SolucaoTecnica", AV74TFFuncaoDados_SolucaoTecnica);
         Ddo_funcaodados_solucaotecnica_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_solucaotecnica_Internalname, "FilteredText_set", Ddo_funcaodados_solucaotecnica_Filteredtext_set);
         AV75TFFuncaoDados_SolucaoTecnica_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75TFFuncaoDados_SolucaoTecnica_Sel", AV75TFFuncaoDados_SolucaoTecnica_Sel);
         Ddo_funcaodados_solucaotecnica_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_solucaotecnica_Internalname, "SelectedValue_set", Ddo_funcaodados_solucaotecnica_Selectedvalue_set);
         AV23DynamicFiltersSelector1 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector1", AV23DynamicFiltersSelector1);
         AV25FuncaoDados_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25FuncaoDados_Nome1", AV25FuncaoDados_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV18Session.Get(AV88Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV88Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV18Session.Get(AV88Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV89GXV1 = 1;
         while ( AV89GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV89GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME") == 0 )
            {
               AV42TFFuncaoDados_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFFuncaoDados_Nome", AV42TFFuncaoDados_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFFuncaoDados_Nome)) )
               {
                  Ddo_funcaodados_nome_Filteredtext_set = AV42TFFuncaoDados_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "FilteredText_set", Ddo_funcaodados_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME_SEL") == 0 )
            {
               AV43TFFuncaoDados_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoDados_Nome_Sel", AV43TFFuncaoDados_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoDados_Nome_Sel)) )
               {
                  Ddo_funcaodados_nome_Selectedvalue_set = AV43TFFuncaoDados_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_nome_Internalname, "SelectedValue_set", Ddo_funcaodados_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_TIPO_SEL") == 0 )
            {
               AV46TFFuncaoDados_Tipo_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV47TFFuncaoDados_Tipo_Sels.FromJSonString(AV46TFFuncaoDados_Tipo_SelsJson);
               if ( ! ( AV47TFFuncaoDados_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_funcaodados_tipo_Selectedvalue_set = AV46TFFuncaoDados_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_tipo_Internalname, "SelectedValue_set", Ddo_funcaodados_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_FUNCAODADOSSISDES") == 0 )
            {
               AV50TFFuncaoDados_FuncaoDadosSisDes = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFFuncaoDados_FuncaoDadosSisDes", AV50TFFuncaoDados_FuncaoDadosSisDes);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFFuncaoDados_FuncaoDadosSisDes)) )
               {
                  Ddo_funcaodados_funcaodadossisdes_Filteredtext_set = AV50TFFuncaoDados_FuncaoDadosSisDes;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "FilteredText_set", Ddo_funcaodados_funcaodadossisdes_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_FUNCAODADOSSISDES_SEL") == 0 )
            {
               AV51TFFuncaoDados_FuncaoDadosSisDes_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoDados_FuncaoDadosSisDes_Sel", AV51TFFuncaoDados_FuncaoDadosSisDes_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoDados_FuncaoDadosSisDes_Sel)) )
               {
                  Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set = AV51TFFuncaoDados_FuncaoDadosSisDes_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_funcaodadossisdes_Internalname, "SelectedValue_set", Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_COMPLEXIDADE_SEL") == 0 )
            {
               AV54TFFuncaoDados_Complexidade_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV55TFFuncaoDados_Complexidade_Sels.FromJSonString(AV54TFFuncaoDados_Complexidade_SelsJson);
               if ( ! ( AV55TFFuncaoDados_Complexidade_Sels.Count == 0 ) )
               {
                  Ddo_funcaodados_complexidade_Selectedvalue_set = AV54TFFuncaoDados_Complexidade_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_complexidade_Internalname, "SelectedValue_set", Ddo_funcaodados_complexidade_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_DER") == 0 )
            {
               AV58TFFuncaoDados_DER = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0)));
               AV59TFFuncaoDados_DER_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0)));
               if ( ! (0==AV58TFFuncaoDados_DER) )
               {
                  Ddo_funcaodados_der_Filteredtext_set = StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredText_set", Ddo_funcaodados_der_Filteredtext_set);
               }
               if ( ! (0==AV59TFFuncaoDados_DER_To) )
               {
                  Ddo_funcaodados_der_Filteredtextto_set = StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_der_Internalname, "FilteredTextTo_set", Ddo_funcaodados_der_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_RLR") == 0 )
            {
               AV62TFFuncaoDados_RLR = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0)));
               AV63TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0)));
               if ( ! (0==AV62TFFuncaoDados_RLR) )
               {
                  Ddo_funcaodados_rlr_Filteredtext_set = StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredText_set", Ddo_funcaodados_rlr_Filteredtext_set);
               }
               if ( ! (0==AV63TFFuncaoDados_RLR_To) )
               {
                  Ddo_funcaodados_rlr_Filteredtextto_set = StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_rlr_Internalname, "FilteredTextTo_set", Ddo_funcaodados_rlr_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_PF") == 0 )
            {
               AV66TFFuncaoDados_PF = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5)));
               AV67TFFuncaoDados_PF_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV66TFFuncaoDados_PF) )
               {
                  Ddo_funcaodados_pf_Filteredtext_set = StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredText_set", Ddo_funcaodados_pf_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV67TFFuncaoDados_PF_To) )
               {
                  Ddo_funcaodados_pf_Filteredtextto_set = StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_pf_Internalname, "FilteredTextTo_set", Ddo_funcaodados_pf_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_ATIVO_SEL") == 0 )
            {
               AV70TFFuncaoDados_Ativo_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV71TFFuncaoDados_Ativo_Sels.FromJSonString(AV70TFFuncaoDados_Ativo_SelsJson);
               if ( ! ( AV71TFFuncaoDados_Ativo_Sels.Count == 0 ) )
               {
                  Ddo_funcaodados_ativo_Selectedvalue_set = AV70TFFuncaoDados_Ativo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_ativo_Internalname, "SelectedValue_set", Ddo_funcaodados_ativo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_SOLUCAOTECNICA") == 0 )
            {
               AV74TFFuncaoDados_SolucaoTecnica = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFFuncaoDados_SolucaoTecnica", AV74TFFuncaoDados_SolucaoTecnica);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFFuncaoDados_SolucaoTecnica)) )
               {
                  Ddo_funcaodados_solucaotecnica_Filteredtext_set = AV74TFFuncaoDados_SolucaoTecnica;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_solucaotecnica_Internalname, "FilteredText_set", Ddo_funcaodados_solucaotecnica_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_SOLUCAOTECNICA_SEL") == 0 )
            {
               AV75TFFuncaoDados_SolucaoTecnica_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75TFFuncaoDados_SolucaoTecnica_Sel", AV75TFFuncaoDados_SolucaoTecnica_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncaoDados_SolucaoTecnica_Sel)) )
               {
                  Ddo_funcaodados_solucaotecnica_Selectedvalue_set = AV75TFFuncaoDados_SolucaoTecnica_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaodados_solucaotecnica_Internalname, "SelectedValue_set", Ddo_funcaodados_solucaotecnica_Selectedvalue_set);
               }
            }
            AV89GXV1 = (int)(AV89GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV22GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV23DynamicFiltersSelector1 = AV22GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector1", AV23DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
            {
               AV25FuncaoDados_Nome1 = AV22GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25FuncaoDados_Nome1", AV25FuncaoDados_Nome1);
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 )
            {
               AV26FuncaoDados_Tipo1 = AV22GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26FuncaoDados_Tipo1", AV26FuncaoDados_Tipo1);
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 )
            {
               AV24DynamicFiltersOperator1 = AV22GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0)));
               AV37FuncaoDados_PF1 = NumberUtil.Val( AV22GridStateDynamicFilter.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37FuncaoDados_PF1", StringUtil.LTrim( StringUtil.Str( AV37FuncaoDados_PF1, 14, 5)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV27DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled2", AV27DynamicFiltersEnabled2);
               AV22GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV28DynamicFiltersSelector2 = AV22GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector2", AV28DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
               {
                  AV30FuncaoDados_Nome2 = AV22GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30FuncaoDados_Nome2", AV30FuncaoDados_Nome2);
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 )
               {
                  AV31FuncaoDados_Tipo2 = AV22GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31FuncaoDados_Tipo2", AV31FuncaoDados_Tipo2);
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 )
               {
                  AV29DynamicFiltersOperator2 = AV22GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0)));
                  AV38FuncaoDados_PF2 = NumberUtil.Val( AV22GridStateDynamicFilter.gxTpr_Value, ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoDados_PF2", StringUtil.LTrim( StringUtil.Str( AV38FuncaoDados_PF2, 14, 5)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV18Session.Get(AV88Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFFuncaoDados_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV42TFFuncaoDados_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoDados_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV43TFFuncaoDados_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV47TFFuncaoDados_Tipo_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_TIPO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV47TFFuncaoDados_Tipo_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFFuncaoDados_FuncaoDadosSisDes)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_FUNCAODADOSSISDES";
            AV12GridStateFilterValue.gxTpr_Value = AV50TFFuncaoDados_FuncaoDadosSisDes;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoDados_FuncaoDadosSisDes_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_FUNCAODADOSSISDES_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV51TFFuncaoDados_FuncaoDadosSisDes_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV55TFFuncaoDados_Complexidade_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_COMPLEXIDADE_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV55TFFuncaoDados_Complexidade_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV58TFFuncaoDados_DER) && (0==AV59TFFuncaoDados_DER_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_DER";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV58TFFuncaoDados_DER), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV59TFFuncaoDados_DER_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV62TFFuncaoDados_RLR) && (0==AV63TFFuncaoDados_RLR_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_RLR";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV62TFFuncaoDados_RLR), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV63TFFuncaoDados_RLR_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV66TFFuncaoDados_PF) && (Convert.ToDecimal(0)==AV67TFFuncaoDados_PF_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_PF";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV66TFFuncaoDados_PF, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV67TFFuncaoDados_PF_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV71TFFuncaoDados_Ativo_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV71TFFuncaoDados_Ativo_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFFuncaoDados_SolucaoTecnica)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_SOLUCAOTECNICA";
            AV12GridStateFilterValue.gxTpr_Value = AV74TFFuncaoDados_SolucaoTecnica;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncaoDados_SolucaoTecnica_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_SOLUCAOTECNICA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV75TFFuncaoDados_SolucaoTecnica_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7FuncaoDados_SistemaCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAODADOS_SISTEMACOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV88Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV22GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV22GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25FuncaoDados_Nome1)) )
            {
               AV22GridStateDynamicFilter.gxTpr_Value = AV25FuncaoDados_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26FuncaoDados_Tipo1)) )
            {
               AV22GridStateDynamicFilter.gxTpr_Value = AV26FuncaoDados_Tipo1;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_PF") == 0 ) && ! (Convert.ToDecimal(0)==AV37FuncaoDados_PF1) )
            {
               AV22GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV37FuncaoDados_PF1, 14, 5);
               AV22GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV22GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV22GridStateDynamicFilter, 0);
            }
         }
         if ( AV27DynamicFiltersEnabled2 )
         {
            AV22GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV22GridStateDynamicFilter.gxTpr_Selected = AV28DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30FuncaoDados_Nome2)) )
            {
               AV22GridStateDynamicFilter.gxTpr_Value = AV30FuncaoDados_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31FuncaoDados_Tipo2)) )
            {
               AV22GridStateDynamicFilter.gxTpr_Value = AV31FuncaoDados_Tipo2;
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_PF") == 0 ) && ! (Convert.ToDecimal(0)==AV38FuncaoDados_PF2) )
            {
               AV22GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV38FuncaoDados_PF2, 14, 5);
               AV22GridStateDynamicFilter.gxTpr_Operator = AV29DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV22GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV22GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV88Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "FuncaoDados";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "FuncaoDados_SistemaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV18Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_8_9G2( true) ;
         }
         else
         {
            wb_table2_8_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_9G2( true) ;
         }
         else
         {
            wb_table3_67_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table3_67_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9G2e( true) ;
         }
         else
         {
            wb_table1_2_9G2e( false) ;
         }
      }

      protected void wb_table3_67_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"70\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Dados Externa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Funcao Dados_Funcao Dados Sis Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_FuncaoDadosSisDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_FuncaoDadosSisDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_FuncaoDadosSisDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Complexidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_DER_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_DER_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_DER_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_RLR_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_RLR_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_RLR_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtFuncaoDados_SolucaoTecnica_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               if ( edtFuncaoDados_SolucaoTecnica_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_SolucaoTecnica_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_SolucaoTecnica_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A369FuncaoDados_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoDados_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A373FuncaoDados_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A405FuncaoDados_FuncaoDadosSisDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_FuncaoDadosSisDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_FuncaoDadosSisDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A376FuncaoDados_Complexidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Complexidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Complexidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_DER_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_DER_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_RLR_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_RLR_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A394FuncaoDados_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A753FuncaoDados_SolucaoTecnica);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_SolucaoTecnica_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_SolucaoTecnica_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtFuncaoDados_SolucaoTecnica_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_SolucaoTecnica_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 70 )
         {
            wbEnd = 0;
            nRC_GXsfl_70 = (short)(nGXsfl_70_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_9G2e( true) ;
         }
         else
         {
            wb_table3_67_9G2e( false) ;
         }
      }

      protected void wb_table2_8_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_9G2( true) ;
         }
         else
         {
            wb_table4_11_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_SistemaFuncaoDadosWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_21_9G2( true) ;
         }
         else
         {
            wb_table5_21_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9G2e( true) ;
         }
         else
         {
            wb_table2_8_9G2e( false) ;
         }
      }

      protected void wb_table5_21_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_26_9G2( true) ;
         }
         else
         {
            wb_table6_26_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_9G2e( true) ;
         }
         else
         {
            wb_table5_21_9G2e( false) ;
         }
      }

      protected void wb_table6_26_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_SistemaFuncaoDadosWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_35_9G2( true) ;
         }
         else
         {
            wb_table7_35_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table7_35_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaoDadosWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV28DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_SistemaFuncaoDadosWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_54_9G2( true) ;
         }
         else
         {
            wb_table8_54_9G2( false) ;
         }
         return  ;
      }

      protected void wb_table8_54_9G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_9G2e( true) ;
         }
         else
         {
            wb_table6_26_9G2e( false) ;
         }
      }

      protected void wb_table8_54_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_SistemaFuncaoDadosWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome2_Internalname, AV30FuncaoDados_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavFuncaodados_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaodados_tipo2, cmbavFuncaodados_tipo2_Internalname, StringUtil.RTrim( AV31FuncaoDados_Tipo2), 1, cmbavFuncaodados_tipo2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaodados_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_SistemaFuncaoDadosWC.htm");
            cmbavFuncaodados_tipo2.CurrentValue = StringUtil.RTrim( AV31FuncaoDados_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo2_Internalname, "Values", (String)(cmbavFuncaodados_tipo2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaodados_pf2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV38FuncaoDados_PF2, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV38FuncaoDados_PF2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,61);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaodados_pf2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaodados_pf2_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_54_9G2e( true) ;
         }
         else
         {
            wb_table8_54_9G2e( false) ;
         }
      }

      protected void wb_table7_35_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_SistemaFuncaoDadosWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome1_Internalname, AV25FuncaoDados_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", 0, edtavFuncaodados_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaoDadosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaodados_tipo1, cmbavFuncaodados_tipo1_Internalname, StringUtil.RTrim( AV26FuncaoDados_Tipo1), 1, cmbavFuncaodados_tipo1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaodados_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_SistemaFuncaoDadosWC.htm");
            cmbavFuncaodados_tipo1.CurrentValue = StringUtil.RTrim( AV26FuncaoDados_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaodados_tipo1_Internalname, "Values", (String)(cmbavFuncaodados_tipo1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_70_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaodados_pf1_Internalname, StringUtil.LTrim( StringUtil.NToC( AV37FuncaoDados_PF1, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV37FuncaoDados_PF1, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaodados_pf1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaodados_pf1_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_35_9G2e( true) ;
         }
         else
         {
            wb_table7_35_9G2e( false) ;
         }
      }

      protected void wb_table4_11_9G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaoDadosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_9G2e( true) ;
         }
         else
         {
            wb_table4_11_9G2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9G2( ) ;
         WS9G2( ) ;
         WE9G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7FuncaoDados_SistemaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA9G2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemafuncaodadoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA9G2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
         }
         wcpOAV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoDados_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7FuncaoDados_SistemaCod != wcpOAV7FuncaoDados_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7FuncaoDados_SistemaCod = AV7FuncaoDados_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7FuncaoDados_SistemaCod = cgiGet( sPrefix+"AV7FuncaoDados_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7FuncaoDados_SistemaCod) > 0 )
         {
            AV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7FuncaoDados_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0)));
         }
         else
         {
            AV7FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7FuncaoDados_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA9G2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS9G2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS9G2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoDados_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoDados_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7FuncaoDados_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoDados_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV7FuncaoDados_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE9G2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203269123811");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemafuncaodadoswc.js", "?20203269123811");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_702( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_70_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_70_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_70_idx;
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO_"+sGXsfl_70_idx;
         edtFuncaoDados_SistemaCod_Internalname = sPrefix+"FUNCAODADOS_SISTEMACOD_"+sGXsfl_70_idx;
         edtFuncaoDados_FuncaoDadosCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSCOD_"+sGXsfl_70_idx;
         edtFuncaoDados_FuncaoDadosSisCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISCOD_"+sGXsfl_70_idx;
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME_"+sGXsfl_70_idx;
         cmbFuncaoDados_Tipo_Internalname = sPrefix+"FUNCAODADOS_TIPO_"+sGXsfl_70_idx;
         edtFuncaoDados_FuncaoDadosSisDes_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISDES_"+sGXsfl_70_idx;
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_70_idx;
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER_"+sGXsfl_70_idx;
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR_"+sGXsfl_70_idx;
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF_"+sGXsfl_70_idx;
         cmbFuncaoDados_Ativo_Internalname = sPrefix+"FUNCAODADOS_ATIVO_"+sGXsfl_70_idx;
         edtFuncaoDados_SolucaoTecnica_Internalname = sPrefix+"FUNCAODADOS_SOLUCAOTECNICA_"+sGXsfl_70_idx;
      }

      protected void SubsflControlProps_fel_702( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_70_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_70_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_SistemaCod_Internalname = sPrefix+"FUNCAODADOS_SISTEMACOD_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_FuncaoDadosCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSCOD_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_FuncaoDadosSisCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISCOD_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME_"+sGXsfl_70_fel_idx;
         cmbFuncaoDados_Tipo_Internalname = sPrefix+"FUNCAODADOS_TIPO_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_FuncaoDadosSisDes_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISDES_"+sGXsfl_70_fel_idx;
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF_"+sGXsfl_70_fel_idx;
         cmbFuncaoDados_Ativo_Internalname = sPrefix+"FUNCAODADOS_ATIVO_"+sGXsfl_70_fel_idx;
         edtFuncaoDados_SolucaoTecnica_Internalname = sPrefix+"FUNCAODADOS_SOLUCAOTECNICA_"+sGXsfl_70_fel_idx;
      }

      protected void sendrow_702( )
      {
         SubsflControlProps_702( ) ;
         WB9G0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_70_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_70_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_70_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV85Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV85Update_GXI : context.PathToRelativeUrl( AV15Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV86Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV86Delete_GXI : context.PathToRelativeUrl( AV16Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV17Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV87Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Display)) ? AV87Display_GXI : context.PathToRelativeUrl( AV17Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV17Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_SistemaCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_FuncaoDadosCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A391FuncaoDados_FuncaoDadosCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_FuncaoDadosCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_FuncaoDadosSisCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_FuncaoDadosSisCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Nome_Internalname,(String)A369FuncaoDados_Nome,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtFuncaoDados_Nome_Link,(String)"",(String)"",(String)"",(String)edtFuncaoDados_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_70_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_70_idx;
               cmbFuncaoDados_Tipo.Name = GXCCtl;
               cmbFuncaoDados_Tipo.WebTags = "";
               cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
               cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
               cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
               cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
               if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
               {
                  A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Tipo,(String)cmbFuncaoDados_Tipo_Internalname,StringUtil.RTrim( A373FuncaoDados_Tipo),(short)1,(String)cmbFuncaoDados_Tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Tipo.CurrentValue = StringUtil.RTrim( A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Tipo_Internalname, "Values", (String)(cmbFuncaoDados_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_FuncaoDadosSisDes_Internalname,(String)A405FuncaoDados_FuncaoDadosSisDes,StringUtil.RTrim( context.localUtil.Format( A405FuncaoDados_FuncaoDadosSisDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_FuncaoDadosSisDes_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_70_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Complexidade,(String)cmbFuncaoDados_Complexidade_Internalname,StringUtil.RTrim( A376FuncaoDados_Complexidade),(short)1,(String)cmbFuncaoDados_Complexidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_DER_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_DER_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_RLR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_RLR_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")),context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_PF_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)70,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_70_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAODADOS_ATIVO_" + sGXsfl_70_idx;
               cmbFuncaoDados_Ativo.Name = GXCCtl;
               cmbFuncaoDados_Ativo.WebTags = "";
               cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
               cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
               cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
               if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
               {
                  A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Ativo,(String)cmbFuncaoDados_Ativo_Internalname,StringUtil.RTrim( A394FuncaoDados_Ativo),(short)1,(String)cmbFuncaoDados_Ativo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Ativo.CurrentValue = StringUtil.RTrim( A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Ativo_Internalname, "Values", (String)(cmbFuncaoDados_Ativo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtFuncaoDados_SolucaoTecnica_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_SolucaoTecnica_Internalname,(String)A753FuncaoDados_SolucaoTecnica,(String)A753FuncaoDados_SolucaoTecnica,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)edtFuncaoDados_SolucaoTecnica_Tooltiptext,(String)"",(String)edtFuncaoDados_SolucaoTecnica_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtFuncaoDados_SolucaoTecnica_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)70,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_CODIGO"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_SISTEMACOD"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_FUNCAODADOSCOD"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, context.localUtil.Format( (decimal)(A391FuncaoDados_FuncaoDadosCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_NOME"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_TIPO"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_COMPLEXIDADE"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DER"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_RLR"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_PF"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_ATIVO"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_SOLUCAOTECNICA"+"_"+sGXsfl_70_idx, GetSecureSignedToken( sPrefix+sGXsfl_70_idx, A753FuncaoDados_SolucaoTecnica));
            GridContainer.AddRow(GridRow);
            nGXsfl_70_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_70_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_70_idx+1));
            sGXsfl_70_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_70_idx), 4, 0)), 4, "0");
            SubsflControlProps_702( ) ;
         }
         /* End function sendrow_702 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavFuncaodados_nome1_Internalname = sPrefix+"vFUNCAODADOS_NOME1";
         cmbavFuncaodados_tipo1_Internalname = sPrefix+"vFUNCAODADOS_TIPO1";
         edtavFuncaodados_pf1_Internalname = sPrefix+"vFUNCAODADOS_PF1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavFuncaodados_nome2_Internalname = sPrefix+"vFUNCAODADOS_NOME2";
         cmbavFuncaodados_tipo2_Internalname = sPrefix+"vFUNCAODADOS_TIPO2";
         edtavFuncaodados_pf2_Internalname = sPrefix+"vFUNCAODADOS_PF2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO";
         edtFuncaoDados_SistemaCod_Internalname = sPrefix+"FUNCAODADOS_SISTEMACOD";
         edtFuncaoDados_FuncaoDadosCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSCOD";
         edtFuncaoDados_FuncaoDadosSisCod_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISCOD";
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME";
         cmbFuncaoDados_Tipo_Internalname = sPrefix+"FUNCAODADOS_TIPO";
         edtFuncaoDados_FuncaoDadosSisDes_Internalname = sPrefix+"FUNCAODADOS_FUNCAODADOSSISDES";
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE";
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER";
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR";
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF";
         cmbFuncaoDados_Ativo_Internalname = sPrefix+"FUNCAODADOS_ATIVO";
         edtFuncaoDados_SolucaoTecnica_Internalname = sPrefix+"FUNCAODADOS_SOLUCAOTECNICA";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         edtavTffuncaodados_nome_Internalname = sPrefix+"vTFFUNCAODADOS_NOME";
         edtavTffuncaodados_nome_sel_Internalname = sPrefix+"vTFFUNCAODADOS_NOME_SEL";
         edtavTffuncaodados_funcaodadossisdes_Internalname = sPrefix+"vTFFUNCAODADOS_FUNCAODADOSSISDES";
         edtavTffuncaodados_funcaodadossisdes_sel_Internalname = sPrefix+"vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL";
         edtavTffuncaodados_der_Internalname = sPrefix+"vTFFUNCAODADOS_DER";
         edtavTffuncaodados_der_to_Internalname = sPrefix+"vTFFUNCAODADOS_DER_TO";
         edtavTffuncaodados_rlr_Internalname = sPrefix+"vTFFUNCAODADOS_RLR";
         edtavTffuncaodados_rlr_to_Internalname = sPrefix+"vTFFUNCAODADOS_RLR_TO";
         edtavTffuncaodados_pf_Internalname = sPrefix+"vTFFUNCAODADOS_PF";
         edtavTffuncaodados_pf_to_Internalname = sPrefix+"vTFFUNCAODADOS_PF_TO";
         edtavTffuncaodados_solucaotecnica_Internalname = sPrefix+"vTFFUNCAODADOS_SOLUCAOTECNICA";
         edtavTffuncaodados_solucaotecnica_sel_Internalname = sPrefix+"vTFFUNCAODADOS_SOLUCAOTECNICA_SEL";
         Ddo_funcaodados_nome_Internalname = sPrefix+"DDO_FUNCAODADOS_NOME";
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_tipo_Internalname = sPrefix+"DDO_FUNCAODADOS_TIPO";
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_funcaodadossisdes_Internalname = sPrefix+"DDO_FUNCAODADOS_FUNCAODADOSSISDES";
         edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_complexidade_Internalname = sPrefix+"DDO_FUNCAODADOS_COMPLEXIDADE";
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_der_Internalname = sPrefix+"DDO_FUNCAODADOS_DER";
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_rlr_Internalname = sPrefix+"DDO_FUNCAODADOS_RLR";
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_pf_Internalname = sPrefix+"DDO_FUNCAODADOS_PF";
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_ativo_Internalname = sPrefix+"DDO_FUNCAODADOS_ATIVO";
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_solucaotecnica_Internalname = sPrefix+"DDO_FUNCAODADOS_SOLUCAOTECNICA";
         edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoDados_SolucaoTecnica_Jsonclick = "";
         cmbFuncaoDados_Ativo_Jsonclick = "";
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_DER_Jsonclick = "";
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         edtFuncaoDados_FuncaoDadosSisDes_Jsonclick = "";
         cmbFuncaoDados_Tipo_Jsonclick = "";
         edtFuncaoDados_Nome_Jsonclick = "";
         edtFuncaoDados_FuncaoDadosSisCod_Jsonclick = "";
         edtFuncaoDados_FuncaoDadosCod_Jsonclick = "";
         edtFuncaoDados_SistemaCod_Jsonclick = "";
         edtFuncaoDados_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavFuncaodados_pf1_Jsonclick = "";
         cmbavFuncaodados_tipo1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavFuncaodados_pf2_Jsonclick = "";
         cmbavFuncaodados_tipo2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoDados_SolucaoTecnica_Tooltiptext = "";
         edtFuncaoDados_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtFuncaoDados_SolucaoTecnica_Titleformat = 0;
         cmbFuncaoDados_Ativo_Titleformat = 0;
         edtFuncaoDados_PF_Titleformat = 0;
         edtFuncaoDados_RLR_Titleformat = 0;
         edtFuncaoDados_DER_Titleformat = 0;
         cmbFuncaoDados_Complexidade_Titleformat = 0;
         edtFuncaoDados_FuncaoDadosSisDes_Titleformat = 0;
         cmbFuncaoDados_Tipo_Titleformat = 0;
         edtFuncaoDados_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavFuncaodados_pf2_Visible = 1;
         cmbavFuncaodados_tipo2.Visible = 1;
         edtavFuncaodados_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavFuncaodados_pf1_Visible = 1;
         cmbavFuncaodados_tipo1.Visible = 1;
         edtavFuncaodados_nome1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtFuncaoDados_SolucaoTecnica_Title = "Solu��o t�cnica";
         cmbFuncaoDados_Ativo.Title.Text = "Status";
         edtFuncaoDados_PF_Title = "PF";
         edtFuncaoDados_RLR_Title = "RLR";
         edtFuncaoDados_DER_Title = "DER";
         cmbFuncaoDados_Complexidade.Title.Text = "Complexidade";
         edtFuncaoDados_FuncaoDadosSisDes_Title = "Sistema externo";
         cmbFuncaoDados_Tipo.Title.Text = "Tipo";
         edtFuncaoDados_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaodados_solucaotecnica_sel_Visible = 1;
         edtavTffuncaodados_solucaotecnica_Visible = 1;
         edtavTffuncaodados_pf_to_Jsonclick = "";
         edtavTffuncaodados_pf_to_Visible = 1;
         edtavTffuncaodados_pf_Jsonclick = "";
         edtavTffuncaodados_pf_Visible = 1;
         edtavTffuncaodados_rlr_to_Jsonclick = "";
         edtavTffuncaodados_rlr_to_Visible = 1;
         edtavTffuncaodados_rlr_Jsonclick = "";
         edtavTffuncaodados_rlr_Visible = 1;
         edtavTffuncaodados_der_to_Jsonclick = "";
         edtavTffuncaodados_der_to_Visible = 1;
         edtavTffuncaodados_der_Jsonclick = "";
         edtavTffuncaodados_der_Visible = 1;
         edtavTffuncaodados_funcaodadossisdes_sel_Jsonclick = "";
         edtavTffuncaodados_funcaodadossisdes_sel_Visible = 1;
         edtavTffuncaodados_funcaodadossisdes_Jsonclick = "";
         edtavTffuncaodados_funcaodadossisdes_Visible = 1;
         edtavTffuncaodados_nome_sel_Visible = 1;
         edtavTffuncaodados_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcaodados_solucaotecnica_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_solucaotecnica_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_solucaotecnica_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_solucaotecnica_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_solucaotecnica_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_solucaotecnica_Datalistproc = "GetSistemaFuncaoDadosWCFilterData";
         Ddo_funcaodados_solucaotecnica_Datalisttype = "Dynamic";
         Ddo_funcaodados_solucaotecnica_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_solucaotecnica_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaodados_solucaotecnica_Filtertype = "Character";
         Ddo_funcaodados_solucaotecnica_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_solucaotecnica_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_solucaotecnica_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_solucaotecnica_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_solucaotecnica_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_solucaotecnica_Cls = "ColumnSettings";
         Ddo_funcaodados_solucaotecnica_Tooltip = "Op��es";
         Ddo_funcaodados_solucaotecnica_Caption = "";
         Ddo_funcaodados_ativo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_ativo_Datalistfixedvalues = "A:Ativa,E:Exclu�da,R:Rejeitada";
         Ddo_funcaodados_ativo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Datalisttype = "FixedValues";
         Ddo_funcaodados_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_ativo_Cls = "ColumnSettings";
         Ddo_funcaodados_ativo_Tooltip = "Op��es";
         Ddo_funcaodados_ativo_Caption = "";
         Ddo_funcaodados_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_pf_Rangefilterto = "At�";
         Ddo_funcaodados_pf_Rangefilterfrom = "Desde";
         Ddo_funcaodados_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Filtertype = "Numeric";
         Ddo_funcaodados_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_pf_Cls = "ColumnSettings";
         Ddo_funcaodados_pf_Tooltip = "Op��es";
         Ddo_funcaodados_pf_Caption = "";
         Ddo_funcaodados_rlr_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_rlr_Rangefilterto = "At�";
         Ddo_funcaodados_rlr_Rangefilterfrom = "Desde";
         Ddo_funcaodados_rlr_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_rlr_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Filtertype = "Numeric";
         Ddo_funcaodados_rlr_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_rlr_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_rlr_Cls = "ColumnSettings";
         Ddo_funcaodados_rlr_Tooltip = "Op��es";
         Ddo_funcaodados_rlr_Caption = "";
         Ddo_funcaodados_der_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_der_Rangefilterto = "At�";
         Ddo_funcaodados_der_Rangefilterfrom = "Desde";
         Ddo_funcaodados_der_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_der_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Filtertype = "Numeric";
         Ddo_funcaodados_der_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_der_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_der_Cls = "ColumnSettings";
         Ddo_funcaodados_der_Tooltip = "Op��es";
         Ddo_funcaodados_der_Caption = "";
         Ddo_funcaodados_complexidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_complexidade_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_complexidade_Datalistfixedvalues = "E:,B:Baixa,M:M�dia,A:Alta";
         Ddo_funcaodados_complexidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Datalisttype = "FixedValues";
         Ddo_funcaodados_complexidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_complexidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_complexidade_Cls = "ColumnSettings";
         Ddo_funcaodados_complexidade_Tooltip = "Op��es";
         Ddo_funcaodados_complexidade_Caption = "";
         Ddo_funcaodados_funcaodadossisdes_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_funcaodadossisdes_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_funcaodadossisdes_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_funcaodadossisdes_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_funcaodadossisdes_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_funcaodadossisdes_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_funcaodadossisdes_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_funcaodadossisdes_Datalistproc = "GetSistemaFuncaoDadosWCFilterData";
         Ddo_funcaodados_funcaodadossisdes_Datalisttype = "Dynamic";
         Ddo_funcaodados_funcaodadossisdes_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_funcaodadossisdes_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaodados_funcaodadossisdes_Filtertype = "Character";
         Ddo_funcaodados_funcaodadossisdes_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_funcaodadossisdes_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_funcaodadossisdes_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_funcaodadossisdes_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_funcaodadossisdes_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_funcaodadossisdes_Cls = "ColumnSettings";
         Ddo_funcaodados_funcaodadossisdes_Tooltip = "Op��es";
         Ddo_funcaodados_funcaodadossisdes_Caption = "";
         Ddo_funcaodados_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_tipo_Datalistfixedvalues = "ALI:ALI,AIE:AIE,DC:DC";
         Ddo_funcaodados_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Datalisttype = "FixedValues";
         Ddo_funcaodados_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_tipo_Cls = "ColumnSettings";
         Ddo_funcaodados_tipo_Tooltip = "Op��es";
         Ddo_funcaodados_tipo_Caption = "";
         Ddo_funcaodados_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_nome_Datalistproc = "GetSistemaFuncaoDadosWCFilterData";
         Ddo_funcaodados_nome_Datalisttype = "Dynamic";
         Ddo_funcaodados_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaodados_nome_Filtertype = "Character";
         Ddo_funcaodados_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_nome_Cls = "ColumnSettings";
         Ddo_funcaodados_nome_Tooltip = "Op��es";
         Ddo_funcaodados_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         edtFuncaoDados_SolucaoTecnica_Visible = -1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0}],oparms:[{av:'AV41FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV45FuncaoDados_TipoTitleFilterData',fld:'vFUNCAODADOS_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData',fld:'vFUNCAODADOS_FUNCAODADOSSISDESTITLEFILTERDATA',pic:'',nv:null},{av:'AV53FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV57FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV61FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV65FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV69FuncaoDados_AtivoTitleFilterData',fld:'vFUNCAODADOS_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV73FuncaoDados_SolucaoTecnicaTitleFilterData',fld:'vFUNCAODADOS_SOLUCAOTECNICATITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Tipo'},{av:'edtFuncaoDados_FuncaoDadosSisDes_Titleformat',ctrl:'FUNCAODADOS_FUNCAODADOSSISDES',prop:'Titleformat'},{av:'edtFuncaoDados_FuncaoDadosSisDes_Title',ctrl:'FUNCAODADOS_FUNCAODADOSSISDES',prop:'Title'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'cmbFuncaoDados_Ativo'},{av:'edtFuncaoDados_SolucaoTecnica_Titleformat',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Titleformat'},{av:'edtFuncaoDados_SolucaoTecnica_Title',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Title'},{av:'AV79GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV80GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E119G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED","{handler:'E129G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_nome_Activeeventkey',ctrl:'DDO_FUNCAODADOS_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_nome_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaodados_nome_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaodados_funcaodadossisdes_Sortedstatus',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'SortedStatus'},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_TIPO.ONOPTIONCLICKED","{handler:'E139G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_tipo_Activeeventkey',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_tipo_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcaodados_funcaodadossisdes_Sortedstatus',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'SortedStatus'},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_FUNCAODADOSSISDES.ONOPTIONCLICKED","{handler:'E149G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_funcaodadossisdes_Activeeventkey',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_funcaodadossisdes_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'FilteredText_get'},{av:'Ddo_funcaodados_funcaodadossisdes_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_funcaodadossisdes_Sortedstatus',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'SortedStatus'},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED","{handler:'E159G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_complexidade_Activeeventkey',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_complexidade_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAODADOS_DER.ONOPTIONCLICKED","{handler:'E169G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_der_Activeeventkey',ctrl:'DDO_FUNCAODADOS_DER',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_der_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_get'},{av:'Ddo_funcaodados_der_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_get'}],oparms:[{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED","{handler:'E179G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_rlr_Activeeventkey',ctrl:'DDO_FUNCAODADOS_RLR',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_rlr_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_get'},{av:'Ddo_funcaodados_rlr_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_PF.ONOPTIONCLICKED","{handler:'E189G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_pf_Activeeventkey',ctrl:'DDO_FUNCAODADOS_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_pf_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_get'},{av:'Ddo_funcaodados_pf_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_FUNCAODADOS_ATIVO.ONOPTIONCLICKED","{handler:'E199G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_ativo_Activeeventkey',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_ativo_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaodados_funcaodadossisdes_Sortedstatus',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_SOLUCAOTECNICA.ONOPTIONCLICKED","{handler:'E209G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaodados_solucaotecnica_Activeeventkey',ctrl:'DDO_FUNCAODADOS_SOLUCAOTECNICA',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_solucaotecnica_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_SOLUCAOTECNICA',prop:'FilteredText_get'},{av:'Ddo_funcaodados_solucaotecnica_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_SOLUCAOTECNICA',prop:'SelectedValue_get'}],oparms:[{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E319G2',iparms:[{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''}],oparms:[{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'AV15Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV17Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtFuncaoDados_Nome_Link',ctrl:'FUNCAODADOS_NOME',prop:'Link'},{av:'edtFuncaoDados_SolucaoTecnica_Tooltiptext',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Tooltiptext'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E219G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E269G2',iparms:[],oparms:[{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E229G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'cmbavFuncaodados_tipo2'},{av:'edtavFuncaodados_pf2_Visible',ctrl:'vFUNCAODADOS_PF2',prop:'Visible'},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'cmbavFuncaodados_tipo1'},{av:'edtavFuncaodados_pf1_Visible',ctrl:'vFUNCAODADOS_PF1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E279G2',iparms:[{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'cmbavFuncaodados_tipo1'},{av:'edtavFuncaodados_pf1_Visible',ctrl:'vFUNCAODADOS_PF1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E239G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'cmbavFuncaodados_tipo2'},{av:'edtavFuncaodados_pf2_Visible',ctrl:'vFUNCAODADOS_PF2',prop:'Visible'},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'cmbavFuncaodados_tipo1'},{av:'edtavFuncaodados_pf1_Visible',ctrl:'vFUNCAODADOS_PF1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E289G2',iparms:[{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'cmbavFuncaodados_tipo2'},{av:'edtavFuncaodados_pf2_Visible',ctrl:'vFUNCAODADOS_PF2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E249G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoDados_SolucaoTecnica_Visible',ctrl:'FUNCAODADOS_SOLUCAOTECNICA',prop:'Visible'},{av:'AV44ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_FUNCAODADOSSISDESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_SOLUCAOTECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A404FuncaoDados_FuncaoDadosSisCod',fld:'FUNCAODADOS_FUNCAODADOSSISCOD',pic:'ZZZZZ9',nv:0},{av:'A753FuncaoDados_SolucaoTecnica',fld:'FUNCAODADOS_SOLUCAOTECNICA',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV42TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_set'},{av:'AV43TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_set'},{av:'AV47TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_tipo_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SelectedValue_set'},{av:'AV50TFFuncaoDados_FuncaoDadosSisDes',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES',pic:'@!',nv:''},{av:'Ddo_funcaodados_funcaodadossisdes_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'FilteredText_set'},{av:'AV51TFFuncaoDados_FuncaoDadosSisDes_Sel',fld:'vTFFUNCAODADOS_FUNCAODADOSSISDES_SEL',pic:'@!',nv:''},{av:'Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_FUNCAODADOSSISDES',prop:'SelectedValue_set'},{av:'AV55TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_complexidade_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_set'},{av:'AV58TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_set'},{av:'AV59TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_set'},{av:'AV62TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_set'},{av:'AV63TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_set'},{av:'AV66TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_set'},{av:'AV67TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_set'},{av:'AV71TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_ativo_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SelectedValue_set'},{av:'AV74TFFuncaoDados_SolucaoTecnica',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA',pic:'',nv:''},{av:'Ddo_funcaodados_solucaotecnica_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_SOLUCAOTECNICA',prop:'FilteredText_set'},{av:'AV75TFFuncaoDados_SolucaoTecnica_Sel',fld:'vTFFUNCAODADOS_SOLUCAOTECNICA_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_solucaotecnica_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_SOLUCAOTECNICA',prop:'SelectedValue_set'},{av:'AV23DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'cmbavFuncaodados_tipo1'},{av:'edtavFuncaodados_pf1_Visible',ctrl:'vFUNCAODADOS_PF1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV27DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV26FuncaoDados_Tipo1',fld:'vFUNCAODADOS_TIPO1',pic:'',nv:''},{av:'AV24DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37FuncaoDados_PF1',fld:'vFUNCAODADOS_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31FuncaoDados_Tipo2',fld:'vFUNCAODADOS_TIPO2',pic:'',nv:''},{av:'AV29DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV38FuncaoDados_PF2',fld:'vFUNCAODADOS_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'cmbavFuncaodados_tipo2'},{av:'edtavFuncaodados_pf2_Visible',ctrl:'vFUNCAODADOS_PF2',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E259G2',iparms:[{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaodados_nome_Activeeventkey = "";
         Ddo_funcaodados_nome_Filteredtext_get = "";
         Ddo_funcaodados_nome_Selectedvalue_get = "";
         Ddo_funcaodados_tipo_Activeeventkey = "";
         Ddo_funcaodados_tipo_Selectedvalue_get = "";
         Ddo_funcaodados_funcaodadossisdes_Activeeventkey = "";
         Ddo_funcaodados_funcaodadossisdes_Filteredtext_get = "";
         Ddo_funcaodados_funcaodadossisdes_Selectedvalue_get = "";
         Ddo_funcaodados_complexidade_Activeeventkey = "";
         Ddo_funcaodados_complexidade_Selectedvalue_get = "";
         Ddo_funcaodados_der_Activeeventkey = "";
         Ddo_funcaodados_der_Filteredtext_get = "";
         Ddo_funcaodados_der_Filteredtextto_get = "";
         Ddo_funcaodados_rlr_Activeeventkey = "";
         Ddo_funcaodados_rlr_Filteredtext_get = "";
         Ddo_funcaodados_rlr_Filteredtextto_get = "";
         Ddo_funcaodados_pf_Activeeventkey = "";
         Ddo_funcaodados_pf_Filteredtext_get = "";
         Ddo_funcaodados_pf_Filteredtextto_get = "";
         Ddo_funcaodados_ativo_Activeeventkey = "";
         Ddo_funcaodados_ativo_Selectedvalue_get = "";
         Ddo_funcaodados_solucaotecnica_Activeeventkey = "";
         Ddo_funcaodados_solucaotecnica_Filteredtext_get = "";
         Ddo_funcaodados_solucaotecnica_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV23DynamicFiltersSelector1 = "";
         AV25FuncaoDados_Nome1 = "";
         AV26FuncaoDados_Tipo1 = "";
         AV28DynamicFiltersSelector2 = "";
         AV30FuncaoDados_Nome2 = "";
         AV31FuncaoDados_Tipo2 = "";
         AV42TFFuncaoDados_Nome = "";
         AV43TFFuncaoDados_Nome_Sel = "";
         AV50TFFuncaoDados_FuncaoDadosSisDes = "";
         AV51TFFuncaoDados_FuncaoDadosSisDes_Sel = "";
         AV74TFFuncaoDados_SolucaoTecnica = "";
         AV75TFFuncaoDados_SolucaoTecnica_Sel = "";
         AV44ddo_FuncaoDados_NomeTitleControlIdToReplace = "";
         AV48ddo_FuncaoDados_TipoTitleControlIdToReplace = "";
         AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace = "";
         AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = "";
         AV60ddo_FuncaoDados_DERTitleControlIdToReplace = "";
         AV64ddo_FuncaoDados_RLRTitleControlIdToReplace = "";
         AV68ddo_FuncaoDados_PFTitleControlIdToReplace = "";
         AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace = "";
         AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace = "";
         AV88Pgmname = "";
         AV47TFFuncaoDados_Tipo_Sels = new GxSimpleCollection();
         AV55TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV71TFFuncaoDados_Ativo_Sels = new GxSimpleCollection();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A753FuncaoDados_SolucaoTecnica = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV77DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV41FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45FuncaoDados_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69FuncaoDados_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73FuncaoDados_SolucaoTecnicaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaodados_nome_Filteredtext_set = "";
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         Ddo_funcaodados_nome_Sortedstatus = "";
         Ddo_funcaodados_tipo_Selectedvalue_set = "";
         Ddo_funcaodados_tipo_Sortedstatus = "";
         Ddo_funcaodados_funcaodadossisdes_Filteredtext_set = "";
         Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set = "";
         Ddo_funcaodados_funcaodadossisdes_Sortedstatus = "";
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         Ddo_funcaodados_der_Filteredtext_set = "";
         Ddo_funcaodados_der_Filteredtextto_set = "";
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         Ddo_funcaodados_pf_Filteredtext_set = "";
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         Ddo_funcaodados_ativo_Selectedvalue_set = "";
         Ddo_funcaodados_ativo_Sortedstatus = "";
         Ddo_funcaodados_solucaotecnica_Filteredtext_set = "";
         Ddo_funcaodados_solucaotecnica_Selectedvalue_set = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Update = "";
         AV85Update_GXI = "";
         AV16Delete = "";
         AV86Delete_GXI = "";
         AV17Display = "";
         AV87Display_GXI = "";
         A369FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         A405FuncaoDados_FuncaoDadosSisDes = "";
         A376FuncaoDados_Complexidade = "";
         A394FuncaoDados_Ativo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV25FuncaoDados_Nome1 = "";
         lV30FuncaoDados_Nome2 = "";
         lV42TFFuncaoDados_Nome = "";
         lV50TFFuncaoDados_FuncaoDadosSisDes = "";
         H009G2_A394FuncaoDados_Ativo = new String[] {""} ;
         H009G2_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         H009G2_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         H009G2_A373FuncaoDados_Tipo = new String[] {""} ;
         H009G2_A369FuncaoDados_Nome = new String[] {""} ;
         H009G2_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         H009G2_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         H009G2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         H009G2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         H009G2_A370FuncaoDados_SistemaCod = new int[1] ;
         H009G2_A755FuncaoDados_Contar = new bool[] {false} ;
         H009G2_n755FuncaoDados_Contar = new bool[] {false} ;
         H009G2_A368FuncaoDados_Codigo = new int[1] ;
         H009G2_A752FuncaoDados_SistemaProjCod = new int[1] ;
         H009G2_n752FuncaoDados_SistemaProjCod = new bool[] {false} ;
         H009G2_A745FuncaoDados_MelhoraCod = new int[1] ;
         H009G2_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         GXt_char1 = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H009G3_A127Sistema_Codigo = new int[1] ;
         H009G3_A699Sistema_Tipo = new String[] {""} ;
         H009G3_n699Sistema_Tipo = new bool[] {false} ;
         A699Sistema_Tipo = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV46TFFuncaoDados_Tipo_SelsJson = "";
         AV54TFFuncaoDados_Complexidade_SelsJson = "";
         AV70TFFuncaoDados_Ativo_SelsJson = "";
         GridRow = new GXWebRow();
         AV18Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV22GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7FuncaoDados_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemafuncaodadoswc__default(),
            new Object[][] {
                new Object[] {
               H009G2_A394FuncaoDados_Ativo, H009G2_A405FuncaoDados_FuncaoDadosSisDes, H009G2_n405FuncaoDados_FuncaoDadosSisDes, H009G2_A373FuncaoDados_Tipo, H009G2_A369FuncaoDados_Nome, H009G2_A404FuncaoDados_FuncaoDadosSisCod, H009G2_n404FuncaoDados_FuncaoDadosSisCod, H009G2_A391FuncaoDados_FuncaoDadosCod, H009G2_n391FuncaoDados_FuncaoDadosCod, H009G2_A370FuncaoDados_SistemaCod,
               H009G2_A755FuncaoDados_Contar, H009G2_n755FuncaoDados_Contar, H009G2_A368FuncaoDados_Codigo, H009G2_A752FuncaoDados_SistemaProjCod, H009G2_n752FuncaoDados_SistemaProjCod, H009G2_A745FuncaoDados_MelhoraCod, H009G2_n745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               H009G3_A127Sistema_Codigo, H009G3_A699Sistema_Tipo, H009G3_n699Sistema_Tipo
               }
            }
         );
         AV88Pgmname = "SistemaFuncaoDadosWC";
         /* GeneXus formulas. */
         AV88Pgmname = "SistemaFuncaoDadosWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_70 ;
      private short nGXsfl_70_idx=1 ;
      private short AV13OrderedBy ;
      private short AV24DynamicFiltersOperator1 ;
      private short AV29DynamicFiltersOperator2 ;
      private short AV58TFFuncaoDados_DER ;
      private short AV59TFFuncaoDados_DER_To ;
      private short AV62TFFuncaoDados_RLR ;
      private short AV63TFFuncaoDados_RLR_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_70_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int2 ;
      private short edtFuncaoDados_Nome_Titleformat ;
      private short cmbFuncaoDados_Tipo_Titleformat ;
      private short edtFuncaoDados_FuncaoDadosSisDes_Titleformat ;
      private short cmbFuncaoDados_Complexidade_Titleformat ;
      private short edtFuncaoDados_DER_Titleformat ;
      private short edtFuncaoDados_RLR_Titleformat ;
      private short edtFuncaoDados_PF_Titleformat ;
      private short cmbFuncaoDados_Ativo_Titleformat ;
      private short edtFuncaoDados_SolucaoTecnica_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7FuncaoDados_SistemaCod ;
      private int wcpOAV7FuncaoDados_SistemaCod ;
      private int edtFuncaoDados_SolucaoTecnica_Visible ;
      private int subGrid_Rows ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A368FuncaoDados_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A404FuncaoDados_FuncaoDadosSisCod ;
      private int A745FuncaoDados_MelhoraCod ;
      private int A752FuncaoDados_SistemaProjCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaodados_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcaodados_funcaodadossisdes_Datalistupdateminimumcharacters ;
      private int Ddo_funcaodados_solucaotecnica_Datalistupdateminimumcharacters ;
      private int edtavTffuncaodados_nome_Visible ;
      private int edtavTffuncaodados_nome_sel_Visible ;
      private int edtavTffuncaodados_funcaodadossisdes_Visible ;
      private int edtavTffuncaodados_funcaodadossisdes_sel_Visible ;
      private int edtavTffuncaodados_der_Visible ;
      private int edtavTffuncaodados_der_to_Visible ;
      private int edtavTffuncaodados_rlr_Visible ;
      private int edtavTffuncaodados_rlr_to_Visible ;
      private int edtavTffuncaodados_pf_Visible ;
      private int edtavTffuncaodados_pf_to_Visible ;
      private int edtavTffuncaodados_solucaotecnica_Visible ;
      private int edtavTffuncaodados_solucaotecnica_sel_Visible ;
      private int edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int AV47TFFuncaoDados_Tipo_Sels_Count ;
      private int AV71TFFuncaoDados_Ativo_Sels_Count ;
      private int AV55TFFuncaoDados_Complexidade_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int A127Sistema_Codigo ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV78PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavFuncaodados_nome1_Visible ;
      private int edtavFuncaodados_pf1_Visible ;
      private int edtavFuncaodados_nome2_Visible ;
      private int edtavFuncaodados_pf2_Visible ;
      private int AV89GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV79GridCurrentPage ;
      private long AV80GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV37FuncaoDados_PF1 ;
      private decimal AV38FuncaoDados_PF2 ;
      private decimal AV66TFFuncaoDados_PF ;
      private decimal AV67TFFuncaoDados_PF_To ;
      private decimal A377FuncaoDados_PF ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaodados_nome_Activeeventkey ;
      private String Ddo_funcaodados_nome_Filteredtext_get ;
      private String Ddo_funcaodados_nome_Selectedvalue_get ;
      private String Ddo_funcaodados_tipo_Activeeventkey ;
      private String Ddo_funcaodados_tipo_Selectedvalue_get ;
      private String Ddo_funcaodados_funcaodadossisdes_Activeeventkey ;
      private String Ddo_funcaodados_funcaodadossisdes_Filteredtext_get ;
      private String Ddo_funcaodados_funcaodadossisdes_Selectedvalue_get ;
      private String Ddo_funcaodados_complexidade_Activeeventkey ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_get ;
      private String Ddo_funcaodados_der_Activeeventkey ;
      private String Ddo_funcaodados_der_Filteredtext_get ;
      private String Ddo_funcaodados_der_Filteredtextto_get ;
      private String Ddo_funcaodados_rlr_Activeeventkey ;
      private String Ddo_funcaodados_rlr_Filteredtext_get ;
      private String Ddo_funcaodados_rlr_Filteredtextto_get ;
      private String Ddo_funcaodados_pf_Activeeventkey ;
      private String Ddo_funcaodados_pf_Filteredtext_get ;
      private String Ddo_funcaodados_pf_Filteredtextto_get ;
      private String Ddo_funcaodados_ativo_Activeeventkey ;
      private String Ddo_funcaodados_ativo_Selectedvalue_get ;
      private String Ddo_funcaodados_solucaotecnica_Activeeventkey ;
      private String Ddo_funcaodados_solucaotecnica_Filteredtext_get ;
      private String Ddo_funcaodados_solucaotecnica_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_70_idx="0001" ;
      private String edtFuncaoDados_SolucaoTecnica_Internalname ;
      private String AV26FuncaoDados_Tipo1 ;
      private String AV31FuncaoDados_Tipo2 ;
      private String AV88Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaodados_nome_Caption ;
      private String Ddo_funcaodados_nome_Tooltip ;
      private String Ddo_funcaodados_nome_Cls ;
      private String Ddo_funcaodados_nome_Filteredtext_set ;
      private String Ddo_funcaodados_nome_Selectedvalue_set ;
      private String Ddo_funcaodados_nome_Dropdownoptionstype ;
      private String Ddo_funcaodados_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_nome_Sortedstatus ;
      private String Ddo_funcaodados_nome_Filtertype ;
      private String Ddo_funcaodados_nome_Datalisttype ;
      private String Ddo_funcaodados_nome_Datalistproc ;
      private String Ddo_funcaodados_nome_Sortasc ;
      private String Ddo_funcaodados_nome_Sortdsc ;
      private String Ddo_funcaodados_nome_Loadingdata ;
      private String Ddo_funcaodados_nome_Cleanfilter ;
      private String Ddo_funcaodados_nome_Noresultsfound ;
      private String Ddo_funcaodados_nome_Searchbuttontext ;
      private String Ddo_funcaodados_tipo_Caption ;
      private String Ddo_funcaodados_tipo_Tooltip ;
      private String Ddo_funcaodados_tipo_Cls ;
      private String Ddo_funcaodados_tipo_Selectedvalue_set ;
      private String Ddo_funcaodados_tipo_Dropdownoptionstype ;
      private String Ddo_funcaodados_tipo_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_tipo_Sortedstatus ;
      private String Ddo_funcaodados_tipo_Datalisttype ;
      private String Ddo_funcaodados_tipo_Datalistfixedvalues ;
      private String Ddo_funcaodados_tipo_Sortasc ;
      private String Ddo_funcaodados_tipo_Sortdsc ;
      private String Ddo_funcaodados_tipo_Cleanfilter ;
      private String Ddo_funcaodados_tipo_Searchbuttontext ;
      private String Ddo_funcaodados_funcaodadossisdes_Caption ;
      private String Ddo_funcaodados_funcaodadossisdes_Tooltip ;
      private String Ddo_funcaodados_funcaodadossisdes_Cls ;
      private String Ddo_funcaodados_funcaodadossisdes_Filteredtext_set ;
      private String Ddo_funcaodados_funcaodadossisdes_Selectedvalue_set ;
      private String Ddo_funcaodados_funcaodadossisdes_Dropdownoptionstype ;
      private String Ddo_funcaodados_funcaodadossisdes_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_funcaodadossisdes_Sortedstatus ;
      private String Ddo_funcaodados_funcaodadossisdes_Filtertype ;
      private String Ddo_funcaodados_funcaodadossisdes_Datalisttype ;
      private String Ddo_funcaodados_funcaodadossisdes_Datalistproc ;
      private String Ddo_funcaodados_funcaodadossisdes_Sortasc ;
      private String Ddo_funcaodados_funcaodadossisdes_Sortdsc ;
      private String Ddo_funcaodados_funcaodadossisdes_Loadingdata ;
      private String Ddo_funcaodados_funcaodadossisdes_Cleanfilter ;
      private String Ddo_funcaodados_funcaodadossisdes_Noresultsfound ;
      private String Ddo_funcaodados_funcaodadossisdes_Searchbuttontext ;
      private String Ddo_funcaodados_complexidade_Caption ;
      private String Ddo_funcaodados_complexidade_Tooltip ;
      private String Ddo_funcaodados_complexidade_Cls ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_set ;
      private String Ddo_funcaodados_complexidade_Dropdownoptionstype ;
      private String Ddo_funcaodados_complexidade_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_complexidade_Datalisttype ;
      private String Ddo_funcaodados_complexidade_Datalistfixedvalues ;
      private String Ddo_funcaodados_complexidade_Cleanfilter ;
      private String Ddo_funcaodados_complexidade_Searchbuttontext ;
      private String Ddo_funcaodados_der_Caption ;
      private String Ddo_funcaodados_der_Tooltip ;
      private String Ddo_funcaodados_der_Cls ;
      private String Ddo_funcaodados_der_Filteredtext_set ;
      private String Ddo_funcaodados_der_Filteredtextto_set ;
      private String Ddo_funcaodados_der_Dropdownoptionstype ;
      private String Ddo_funcaodados_der_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_der_Filtertype ;
      private String Ddo_funcaodados_der_Cleanfilter ;
      private String Ddo_funcaodados_der_Rangefilterfrom ;
      private String Ddo_funcaodados_der_Rangefilterto ;
      private String Ddo_funcaodados_der_Searchbuttontext ;
      private String Ddo_funcaodados_rlr_Caption ;
      private String Ddo_funcaodados_rlr_Tooltip ;
      private String Ddo_funcaodados_rlr_Cls ;
      private String Ddo_funcaodados_rlr_Filteredtext_set ;
      private String Ddo_funcaodados_rlr_Filteredtextto_set ;
      private String Ddo_funcaodados_rlr_Dropdownoptionstype ;
      private String Ddo_funcaodados_rlr_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_rlr_Filtertype ;
      private String Ddo_funcaodados_rlr_Cleanfilter ;
      private String Ddo_funcaodados_rlr_Rangefilterfrom ;
      private String Ddo_funcaodados_rlr_Rangefilterto ;
      private String Ddo_funcaodados_rlr_Searchbuttontext ;
      private String Ddo_funcaodados_pf_Caption ;
      private String Ddo_funcaodados_pf_Tooltip ;
      private String Ddo_funcaodados_pf_Cls ;
      private String Ddo_funcaodados_pf_Filteredtext_set ;
      private String Ddo_funcaodados_pf_Filteredtextto_set ;
      private String Ddo_funcaodados_pf_Dropdownoptionstype ;
      private String Ddo_funcaodados_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_pf_Filtertype ;
      private String Ddo_funcaodados_pf_Cleanfilter ;
      private String Ddo_funcaodados_pf_Rangefilterfrom ;
      private String Ddo_funcaodados_pf_Rangefilterto ;
      private String Ddo_funcaodados_pf_Searchbuttontext ;
      private String Ddo_funcaodados_ativo_Caption ;
      private String Ddo_funcaodados_ativo_Tooltip ;
      private String Ddo_funcaodados_ativo_Cls ;
      private String Ddo_funcaodados_ativo_Selectedvalue_set ;
      private String Ddo_funcaodados_ativo_Dropdownoptionstype ;
      private String Ddo_funcaodados_ativo_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_ativo_Sortedstatus ;
      private String Ddo_funcaodados_ativo_Datalisttype ;
      private String Ddo_funcaodados_ativo_Datalistfixedvalues ;
      private String Ddo_funcaodados_ativo_Sortasc ;
      private String Ddo_funcaodados_ativo_Sortdsc ;
      private String Ddo_funcaodados_ativo_Cleanfilter ;
      private String Ddo_funcaodados_ativo_Searchbuttontext ;
      private String Ddo_funcaodados_solucaotecnica_Caption ;
      private String Ddo_funcaodados_solucaotecnica_Tooltip ;
      private String Ddo_funcaodados_solucaotecnica_Cls ;
      private String Ddo_funcaodados_solucaotecnica_Filteredtext_set ;
      private String Ddo_funcaodados_solucaotecnica_Selectedvalue_set ;
      private String Ddo_funcaodados_solucaotecnica_Dropdownoptionstype ;
      private String Ddo_funcaodados_solucaotecnica_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_solucaotecnica_Filtertype ;
      private String Ddo_funcaodados_solucaotecnica_Datalisttype ;
      private String Ddo_funcaodados_solucaotecnica_Datalistproc ;
      private String Ddo_funcaodados_solucaotecnica_Loadingdata ;
      private String Ddo_funcaodados_solucaotecnica_Cleanfilter ;
      private String Ddo_funcaodados_solucaotecnica_Noresultsfound ;
      private String Ddo_funcaodados_solucaotecnica_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTffuncaodados_nome_Internalname ;
      private String edtavTffuncaodados_nome_sel_Internalname ;
      private String edtavTffuncaodados_funcaodadossisdes_Internalname ;
      private String edtavTffuncaodados_funcaodadossisdes_Jsonclick ;
      private String edtavTffuncaodados_funcaodadossisdes_sel_Internalname ;
      private String edtavTffuncaodados_funcaodadossisdes_sel_Jsonclick ;
      private String edtavTffuncaodados_der_Internalname ;
      private String edtavTffuncaodados_der_Jsonclick ;
      private String edtavTffuncaodados_der_to_Internalname ;
      private String edtavTffuncaodados_der_to_Jsonclick ;
      private String edtavTffuncaodados_rlr_Internalname ;
      private String edtavTffuncaodados_rlr_Jsonclick ;
      private String edtavTffuncaodados_rlr_to_Internalname ;
      private String edtavTffuncaodados_rlr_to_Jsonclick ;
      private String edtavTffuncaodados_pf_Internalname ;
      private String edtavTffuncaodados_pf_Jsonclick ;
      private String edtavTffuncaodados_pf_to_Internalname ;
      private String edtavTffuncaodados_pf_to_Jsonclick ;
      private String edtavTffuncaodados_solucaotecnica_Internalname ;
      private String edtavTffuncaodados_solucaotecnica_sel_Internalname ;
      private String edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_funcaodadossisdestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_solucaotecnicatitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String edtFuncaoDados_SistemaCod_Internalname ;
      private String edtFuncaoDados_FuncaoDadosCod_Internalname ;
      private String edtFuncaoDados_FuncaoDadosSisCod_Internalname ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String cmbFuncaoDados_Tipo_Internalname ;
      private String A373FuncaoDados_Tipo ;
      private String edtFuncaoDados_FuncaoDadosSisDes_Internalname ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String A376FuncaoDados_Complexidade ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_PF_Internalname ;
      private String cmbFuncaoDados_Ativo_Internalname ;
      private String A394FuncaoDados_Ativo ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaodados_nome1_Internalname ;
      private String cmbavFuncaodados_tipo1_Internalname ;
      private String edtavFuncaodados_pf1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavFuncaodados_nome2_Internalname ;
      private String cmbavFuncaodados_tipo2_Internalname ;
      private String edtavFuncaodados_pf2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaodados_nome_Internalname ;
      private String Ddo_funcaodados_tipo_Internalname ;
      private String Ddo_funcaodados_funcaodadossisdes_Internalname ;
      private String Ddo_funcaodados_complexidade_Internalname ;
      private String Ddo_funcaodados_der_Internalname ;
      private String Ddo_funcaodados_rlr_Internalname ;
      private String Ddo_funcaodados_pf_Internalname ;
      private String Ddo_funcaodados_ativo_Internalname ;
      private String Ddo_funcaodados_solucaotecnica_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String A699Sistema_Tipo ;
      private String edtFuncaoDados_Nome_Title ;
      private String edtFuncaoDados_FuncaoDadosSisDes_Title ;
      private String edtFuncaoDados_DER_Title ;
      private String edtFuncaoDados_RLR_Title ;
      private String edtFuncaoDados_PF_Title ;
      private String edtFuncaoDados_SolucaoTecnica_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtFuncaoDados_Nome_Link ;
      private String edtFuncaoDados_SolucaoTecnica_Tooltiptext ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String cmbavFuncaodados_tipo2_Jsonclick ;
      private String edtavFuncaodados_pf2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String cmbavFuncaodados_tipo1_Jsonclick ;
      private String edtavFuncaodados_pf1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7FuncaoDados_SistemaCod ;
      private String sGXsfl_70_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String edtFuncaoDados_SistemaCod_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosCod_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosSisCod_Jsonclick ;
      private String edtFuncaoDados_Nome_Jsonclick ;
      private String cmbFuncaoDados_Tipo_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosSisDes_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String cmbFuncaoDados_Ativo_Jsonclick ;
      private String edtFuncaoDados_SolucaoTecnica_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV27DynamicFiltersEnabled2 ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n404FuncaoDados_FuncaoDadosSisCod ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaodados_nome_Includesortasc ;
      private bool Ddo_funcaodados_nome_Includesortdsc ;
      private bool Ddo_funcaodados_nome_Includefilter ;
      private bool Ddo_funcaodados_nome_Filterisrange ;
      private bool Ddo_funcaodados_nome_Includedatalist ;
      private bool Ddo_funcaodados_tipo_Includesortasc ;
      private bool Ddo_funcaodados_tipo_Includesortdsc ;
      private bool Ddo_funcaodados_tipo_Includefilter ;
      private bool Ddo_funcaodados_tipo_Includedatalist ;
      private bool Ddo_funcaodados_tipo_Allowmultipleselection ;
      private bool Ddo_funcaodados_funcaodadossisdes_Includesortasc ;
      private bool Ddo_funcaodados_funcaodadossisdes_Includesortdsc ;
      private bool Ddo_funcaodados_funcaodadossisdes_Includefilter ;
      private bool Ddo_funcaodados_funcaodadossisdes_Filterisrange ;
      private bool Ddo_funcaodados_funcaodadossisdes_Includedatalist ;
      private bool Ddo_funcaodados_complexidade_Includesortasc ;
      private bool Ddo_funcaodados_complexidade_Includesortdsc ;
      private bool Ddo_funcaodados_complexidade_Includefilter ;
      private bool Ddo_funcaodados_complexidade_Includedatalist ;
      private bool Ddo_funcaodados_complexidade_Allowmultipleselection ;
      private bool Ddo_funcaodados_der_Includesortasc ;
      private bool Ddo_funcaodados_der_Includesortdsc ;
      private bool Ddo_funcaodados_der_Includefilter ;
      private bool Ddo_funcaodados_der_Filterisrange ;
      private bool Ddo_funcaodados_der_Includedatalist ;
      private bool Ddo_funcaodados_rlr_Includesortasc ;
      private bool Ddo_funcaodados_rlr_Includesortdsc ;
      private bool Ddo_funcaodados_rlr_Includefilter ;
      private bool Ddo_funcaodados_rlr_Filterisrange ;
      private bool Ddo_funcaodados_rlr_Includedatalist ;
      private bool Ddo_funcaodados_pf_Includesortasc ;
      private bool Ddo_funcaodados_pf_Includesortdsc ;
      private bool Ddo_funcaodados_pf_Includefilter ;
      private bool Ddo_funcaodados_pf_Filterisrange ;
      private bool Ddo_funcaodados_pf_Includedatalist ;
      private bool Ddo_funcaodados_ativo_Includesortasc ;
      private bool Ddo_funcaodados_ativo_Includesortdsc ;
      private bool Ddo_funcaodados_ativo_Includefilter ;
      private bool Ddo_funcaodados_ativo_Includedatalist ;
      private bool Ddo_funcaodados_ativo_Allowmultipleselection ;
      private bool Ddo_funcaodados_solucaotecnica_Includesortasc ;
      private bool Ddo_funcaodados_solucaotecnica_Includesortdsc ;
      private bool Ddo_funcaodados_solucaotecnica_Includefilter ;
      private bool Ddo_funcaodados_solucaotecnica_Filterisrange ;
      private bool Ddo_funcaodados_solucaotecnica_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n405FuncaoDados_FuncaoDadosSisDes ;
      private bool n755FuncaoDados_Contar ;
      private bool n752FuncaoDados_SistemaProjCod ;
      private bool n745FuncaoDados_MelhoraCod ;
      private bool returnInSub ;
      private bool n699Sistema_Tipo ;
      private bool gx_refresh_fired ;
      private bool AV15Update_IsBlob ;
      private bool AV16Delete_IsBlob ;
      private bool AV17Display_IsBlob ;
      private String A753FuncaoDados_SolucaoTecnica ;
      private String AV46TFFuncaoDados_Tipo_SelsJson ;
      private String AV54TFFuncaoDados_Complexidade_SelsJson ;
      private String AV70TFFuncaoDados_Ativo_SelsJson ;
      private String AV23DynamicFiltersSelector1 ;
      private String AV25FuncaoDados_Nome1 ;
      private String AV28DynamicFiltersSelector2 ;
      private String AV30FuncaoDados_Nome2 ;
      private String AV42TFFuncaoDados_Nome ;
      private String AV43TFFuncaoDados_Nome_Sel ;
      private String AV50TFFuncaoDados_FuncaoDadosSisDes ;
      private String AV51TFFuncaoDados_FuncaoDadosSisDes_Sel ;
      private String AV74TFFuncaoDados_SolucaoTecnica ;
      private String AV75TFFuncaoDados_SolucaoTecnica_Sel ;
      private String AV44ddo_FuncaoDados_NomeTitleControlIdToReplace ;
      private String AV48ddo_FuncaoDados_TipoTitleControlIdToReplace ;
      private String AV52ddo_FuncaoDados_FuncaoDadosSisDesTitleControlIdToReplace ;
      private String AV56ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ;
      private String AV60ddo_FuncaoDados_DERTitleControlIdToReplace ;
      private String AV64ddo_FuncaoDados_RLRTitleControlIdToReplace ;
      private String AV68ddo_FuncaoDados_PFTitleControlIdToReplace ;
      private String AV72ddo_FuncaoDados_AtivoTitleControlIdToReplace ;
      private String AV76ddo_FuncaoDados_SolucaoTecnicaTitleControlIdToReplace ;
      private String AV85Update_GXI ;
      private String AV86Delete_GXI ;
      private String AV87Display_GXI ;
      private String A369FuncaoDados_Nome ;
      private String A405FuncaoDados_FuncaoDadosSisDes ;
      private String lV25FuncaoDados_Nome1 ;
      private String lV30FuncaoDados_Nome2 ;
      private String lV42TFFuncaoDados_Nome ;
      private String lV50TFFuncaoDados_FuncaoDadosSisDes ;
      private String AV15Update ;
      private String AV16Delete ;
      private String AV17Display ;
      private IGxSession AV18Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavFuncaodados_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavFuncaodados_tipo2 ;
      private GXCombobox cmbFuncaoDados_Tipo ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private GXCombobox cmbFuncaoDados_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private String[] H009G2_A394FuncaoDados_Ativo ;
      private String[] H009G2_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] H009G2_n405FuncaoDados_FuncaoDadosSisDes ;
      private String[] H009G2_A373FuncaoDados_Tipo ;
      private String[] H009G2_A369FuncaoDados_Nome ;
      private int[] H009G2_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] H009G2_n404FuncaoDados_FuncaoDadosSisCod ;
      private int[] H009G2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] H009G2_n391FuncaoDados_FuncaoDadosCod ;
      private int[] H009G2_A370FuncaoDados_SistemaCod ;
      private bool[] H009G2_A755FuncaoDados_Contar ;
      private bool[] H009G2_n755FuncaoDados_Contar ;
      private int[] H009G2_A368FuncaoDados_Codigo ;
      private int[] H009G2_A752FuncaoDados_SistemaProjCod ;
      private bool[] H009G2_n752FuncaoDados_SistemaProjCod ;
      private int[] H009G2_A745FuncaoDados_MelhoraCod ;
      private bool[] H009G2_n745FuncaoDados_MelhoraCod ;
      private int[] H009G3_A127Sistema_Codigo ;
      private String[] H009G3_A699Sistema_Tipo ;
      private bool[] H009G3_n699Sistema_Tipo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV47TFFuncaoDados_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV55TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV71TFFuncaoDados_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41FuncaoDados_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45FuncaoDados_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49FuncaoDados_FuncaoDadosSisDesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53FuncaoDados_ComplexidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57FuncaoDados_DERTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61FuncaoDados_RLRTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65FuncaoDados_PFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69FuncaoDados_AtivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73FuncaoDados_SolucaoTecnicaTitleFilterData ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV77DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV22GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class sistemafuncaodadoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009G2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV55TFFuncaoDados_Complexidade_Sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV47TFFuncaoDados_Tipo_Sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV71TFFuncaoDados_Ativo_Sels ,
                                             String AV23DynamicFiltersSelector1 ,
                                             String AV25FuncaoDados_Nome1 ,
                                             String AV26FuncaoDados_Tipo1 ,
                                             bool AV27DynamicFiltersEnabled2 ,
                                             String AV28DynamicFiltersSelector2 ,
                                             String AV30FuncaoDados_Nome2 ,
                                             String AV31FuncaoDados_Tipo2 ,
                                             String AV43TFFuncaoDados_Nome_Sel ,
                                             String AV42TFFuncaoDados_Nome ,
                                             int AV47TFFuncaoDados_Tipo_Sels_Count ,
                                             String AV51TFFuncaoDados_FuncaoDadosSisDes_Sel ,
                                             String AV50TFFuncaoDados_FuncaoDadosSisDes ,
                                             int AV71TFFuncaoDados_Ativo_Sels_Count ,
                                             String A369FuncaoDados_Nome ,
                                             String A405FuncaoDados_FuncaoDadosSisDes ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A370FuncaoDados_SistemaCod ,
                                             int AV7FuncaoDados_SistemaCod ,
                                             short AV24DynamicFiltersOperator1 ,
                                             decimal AV37FuncaoDados_PF1 ,
                                             decimal A377FuncaoDados_PF ,
                                             short AV29DynamicFiltersOperator2 ,
                                             decimal AV38FuncaoDados_PF2 ,
                                             int AV55TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV58TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV59TFFuncaoDados_DER_To ,
                                             short AV62TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV63TFFuncaoDados_RLR_To ,
                                             decimal AV66TFFuncaoDados_PF ,
                                             decimal AV67TFFuncaoDados_PF_To ,
                                             String AV75TFFuncaoDados_SolucaoTecnica_Sel ,
                                             String AV74TFFuncaoDados_SolucaoTecnica ,
                                             String A753FuncaoDados_SolucaoTecnica )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoDados_Ativo], T3.[Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes, T1.[FuncaoDados_Tipo], T1.[FuncaoDados_Nome], T2.[FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod, T1.[FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, T1.[FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, T1.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo], T4.[Sistema_ProjetoCod] AS FuncaoDados_SistemaProjCod, T1.[FuncaoDados_MelhoraCod] FROM ((([FuncaoDados] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_FuncaoDadosCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[FuncaoDados_SistemaCod]) INNER JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[FuncaoDados_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoDados_SistemaCod] = @AV7FuncaoDados_SistemaCod)";
         scmdbuf = scmdbuf + " and (T1.[FuncaoDados_Ativo] = 'A')";
         if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25FuncaoDados_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV25FuncaoDados_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26FuncaoDados_Tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV26FuncaoDados_Tipo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV27DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30FuncaoDados_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV30FuncaoDados_Nome2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV27DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "FUNCAODADOS_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31FuncaoDados_Tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Tipo] = @AV31FuncaoDados_Tipo2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFFuncaoDados_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV42TFFuncaoDados_Nome)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoDados_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] = @AV43TFFuncaoDados_Nome_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV47TFFuncaoDados_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV47TFFuncaoDados_Tipo_Sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoDados_FuncaoDadosSisDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFFuncaoDados_FuncaoDadosSisDes)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV50TFFuncaoDados_FuncaoDadosSisDes)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoDados_FuncaoDadosSisDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV51TFFuncaoDados_FuncaoDadosSisDes_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV71TFFuncaoDados_Ativo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV71TFFuncaoDados_Ativo_Sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod] DESC, T1.[FuncaoDados_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Tipo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod] DESC, T1.[FuncaoDados_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod], T3.[Sistema_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod] DESC, T3.[Sistema_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod], T1.[FuncaoDados_Ativo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_SistemaCod] DESC, T1.[FuncaoDados_Ativo] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H009G2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (bool)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (short)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (short)dynConstraints[28] , (decimal)dynConstraints[29] , (int)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (short)dynConstraints[34] , (short)dynConstraints[35] , (short)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009G3 ;
          prmH009G3 = new Object[] {
          new Object[] {"@AV7FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009G2 ;
          prmH009G2 = new Object[] {
          new Object[] {"@AV7FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV25FuncaoDados_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26FuncaoDados_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV30FuncaoDados_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV31FuncaoDados_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV42TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV43TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV50TFFuncaoDados_FuncaoDadosSisDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV51TFFuncaoDados_FuncaoDadosSisDes_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009G2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009G2,11,0,true,false )
             ,new CursorDef("H009G3", "SELECT TOP 1 [Sistema_Codigo], [Sistema_Tipo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV7FuncaoDados_SistemaCod ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009G3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
