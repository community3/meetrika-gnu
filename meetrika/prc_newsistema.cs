/*
               File: PRC_NewSistema
        Description: New Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:27.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newsistema : GXProcedure
   {
      public prc_newsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_ProjetoCod ,
                           String aP1_Sistema_Nome ,
                           int aP2_AreaTrabalho_Codigo ,
                           ref decimal aP3_Sistema_FatorAjuste ,
                           ref short aP4_Sistema_Prazo ,
                           ref decimal aP5_Sistema_Custo ,
                           ref short aP6_Sistema_Esforco ,
                           ref String aP7_Sistema_Tipo ,
                           ref String aP8_Sistema_Tecnica ,
                           out int aP9_Sistema_Codigo )
      {
         this.AV20Sistema_ProjetoCod = aP0_Sistema_ProjetoCod;
         this.AV10Sistema_Nome = aP1_Sistema_Nome;
         this.AV9AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         this.AV14Sistema_FatorAjuste = aP3_Sistema_FatorAjuste;
         this.AV15Sistema_Prazo = aP4_Sistema_Prazo;
         this.AV16Sistema_Custo = aP5_Sistema_Custo;
         this.AV13Sistema_Esforco = aP6_Sistema_Esforco;
         this.AV17Sistema_Tipo = aP7_Sistema_Tipo;
         this.AV19Sistema_Tecnica = aP8_Sistema_Tecnica;
         this.AV12Sistema_Codigo = 0 ;
         initialize();
         executePrivate();
         aP3_Sistema_FatorAjuste=this.AV14Sistema_FatorAjuste;
         aP4_Sistema_Prazo=this.AV15Sistema_Prazo;
         aP5_Sistema_Custo=this.AV16Sistema_Custo;
         aP6_Sistema_Esforco=this.AV13Sistema_Esforco;
         aP7_Sistema_Tipo=this.AV17Sistema_Tipo;
         aP8_Sistema_Tecnica=this.AV19Sistema_Tecnica;
         aP9_Sistema_Codigo=this.AV12Sistema_Codigo;
      }

      public int executeUdp( int aP0_Sistema_ProjetoCod ,
                             String aP1_Sistema_Nome ,
                             int aP2_AreaTrabalho_Codigo ,
                             ref decimal aP3_Sistema_FatorAjuste ,
                             ref short aP4_Sistema_Prazo ,
                             ref decimal aP5_Sistema_Custo ,
                             ref short aP6_Sistema_Esforco ,
                             ref String aP7_Sistema_Tipo ,
                             ref String aP8_Sistema_Tecnica )
      {
         this.AV20Sistema_ProjetoCod = aP0_Sistema_ProjetoCod;
         this.AV10Sistema_Nome = aP1_Sistema_Nome;
         this.AV9AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         this.AV14Sistema_FatorAjuste = aP3_Sistema_FatorAjuste;
         this.AV15Sistema_Prazo = aP4_Sistema_Prazo;
         this.AV16Sistema_Custo = aP5_Sistema_Custo;
         this.AV13Sistema_Esforco = aP6_Sistema_Esforco;
         this.AV17Sistema_Tipo = aP7_Sistema_Tipo;
         this.AV19Sistema_Tecnica = aP8_Sistema_Tecnica;
         this.AV12Sistema_Codigo = 0 ;
         initialize();
         executePrivate();
         aP3_Sistema_FatorAjuste=this.AV14Sistema_FatorAjuste;
         aP4_Sistema_Prazo=this.AV15Sistema_Prazo;
         aP5_Sistema_Custo=this.AV16Sistema_Custo;
         aP6_Sistema_Esforco=this.AV13Sistema_Esforco;
         aP7_Sistema_Tipo=this.AV17Sistema_Tipo;
         aP8_Sistema_Tecnica=this.AV19Sistema_Tecnica;
         aP9_Sistema_Codigo=this.AV12Sistema_Codigo;
         return AV12Sistema_Codigo ;
      }

      public void executeSubmit( int aP0_Sistema_ProjetoCod ,
                                 String aP1_Sistema_Nome ,
                                 int aP2_AreaTrabalho_Codigo ,
                                 ref decimal aP3_Sistema_FatorAjuste ,
                                 ref short aP4_Sistema_Prazo ,
                                 ref decimal aP5_Sistema_Custo ,
                                 ref short aP6_Sistema_Esforco ,
                                 ref String aP7_Sistema_Tipo ,
                                 ref String aP8_Sistema_Tecnica ,
                                 out int aP9_Sistema_Codigo )
      {
         prc_newsistema objprc_newsistema;
         objprc_newsistema = new prc_newsistema();
         objprc_newsistema.AV20Sistema_ProjetoCod = aP0_Sistema_ProjetoCod;
         objprc_newsistema.AV10Sistema_Nome = aP1_Sistema_Nome;
         objprc_newsistema.AV9AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         objprc_newsistema.AV14Sistema_FatorAjuste = aP3_Sistema_FatorAjuste;
         objprc_newsistema.AV15Sistema_Prazo = aP4_Sistema_Prazo;
         objprc_newsistema.AV16Sistema_Custo = aP5_Sistema_Custo;
         objprc_newsistema.AV13Sistema_Esforco = aP6_Sistema_Esforco;
         objprc_newsistema.AV17Sistema_Tipo = aP7_Sistema_Tipo;
         objprc_newsistema.AV19Sistema_Tecnica = aP8_Sistema_Tecnica;
         objprc_newsistema.AV12Sistema_Codigo = 0 ;
         objprc_newsistema.context.SetSubmitInitialConfig(context);
         objprc_newsistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newsistema);
         aP3_Sistema_FatorAjuste=this.AV14Sistema_FatorAjuste;
         aP4_Sistema_Prazo=this.AV15Sistema_Prazo;
         aP5_Sistema_Custo=this.AV16Sistema_Custo;
         aP6_Sistema_Esforco=this.AV13Sistema_Esforco;
         aP7_Sistema_Tipo=this.AV17Sistema_Tipo;
         aP8_Sistema_Tecnica=this.AV19Sistema_Tecnica;
         aP9_Sistema_Codigo=this.AV12Sistema_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newsistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Sistema = new SdtSistema(context);
         AV8Sistema.gxTpr_Sistema_areatrabalhocod = AV9AreaTrabalho_Codigo;
         AV8Sistema.gxTpr_Sistema_nome = AV10Sistema_Nome;
         AV8Sistema.gxTpr_Sistema_sigla = AV10Sistema_Nome;
         AV8Sistema.gxTpr_Sistema_tipo = AV17Sistema_Tipo;
         AV8Sistema.gxTpr_Sistema_tecnica = AV19Sistema_Tecnica;
         AV8Sistema.gxTpr_Sistema_fatorajuste = AV14Sistema_FatorAjuste;
         AV8Sistema.gxTpr_Sistema_prazo = AV15Sistema_Prazo;
         AV8Sistema.gxTpr_Sistema_custo = AV16Sistema_Custo;
         AV8Sistema.gxTpr_Sistema_esforco = AV13Sistema_Esforco;
         AV8Sistema.gxTpr_Sistema_projetocod = AV20Sistema_ProjetoCod;
         AV8Sistema.gxTv_SdtSistema_Ambientetecnologico_codigo_SetNull();
         AV8Sistema.gxTv_SdtSistema_Metodologia_codigo_SetNull();
         AV8Sistema.gxTv_SdtSistema_Sistema_gpoobjctrlcod_SetNull();
         AV8Sistema.Save();
         AV12Sistema_Codigo = AV8Sistema.gxTpr_Sistema_codigo;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Sistema = new SdtSistema(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15Sistema_Prazo ;
      private short AV13Sistema_Esforco ;
      private int AV20Sistema_ProjetoCod ;
      private int AV9AreaTrabalho_Codigo ;
      private int AV12Sistema_Codigo ;
      private decimal AV14Sistema_FatorAjuste ;
      private decimal AV16Sistema_Custo ;
      private String AV17Sistema_Tipo ;
      private String AV19Sistema_Tecnica ;
      private String AV10Sistema_Nome ;
      private decimal aP3_Sistema_FatorAjuste ;
      private short aP4_Sistema_Prazo ;
      private decimal aP5_Sistema_Custo ;
      private short aP6_Sistema_Esforco ;
      private String aP7_Sistema_Tipo ;
      private String aP8_Sistema_Tecnica ;
      private int aP9_Sistema_Codigo ;
      private SdtSistema AV8Sistema ;
   }

}
