/*
               File: type_SdtContrato
        Description: Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:35.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Contrato" )]
   [XmlType(TypeName =  "Contrato" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContrato : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContrato( )
      {
         /* Constructor for serialization */
         gxTv_SdtContrato_Contrato_areatrabalhodes = "";
         gxTv_SdtContrato_Contratada_pessoanom = "";
         gxTv_SdtContrato_Contratada_pessoacnpj = "";
         gxTv_SdtContrato_Contratada_sigla = "";
         gxTv_SdtContrato_Contratada_tipofabrica = "";
         gxTv_SdtContrato_Contrato_numero = "";
         gxTv_SdtContrato_Contrato_numeroata = "";
         gxTv_SdtContrato_Contrato_objeto = "";
         gxTv_SdtContrato_Contrato_datavigenciainicio = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datavigenciatermino = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datainiciota = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimta = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapublicacaodou = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datatermino = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataassinatura = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapedidoreajuste = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataterminoata = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimadaptacao = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_regraspagto = "";
         gxTv_SdtContrato_Contrato_calculodivergencia = "";
         gxTv_SdtContrato_Contrato_prepostonom = "";
         gxTv_SdtContrato_Contrato_prdftrcada = "";
         gxTv_SdtContrato_Contrato_identificacao = "";
         gxTv_SdtContrato_Mode = "";
         gxTv_SdtContrato_Contrato_areatrabalhodes_Z = "";
         gxTv_SdtContrato_Contratada_pessoanom_Z = "";
         gxTv_SdtContrato_Contratada_pessoacnpj_Z = "";
         gxTv_SdtContrato_Contratada_sigla_Z = "";
         gxTv_SdtContrato_Contratada_tipofabrica_Z = "";
         gxTv_SdtContrato_Contrato_numero_Z = "";
         gxTv_SdtContrato_Contrato_numeroata_Z = "";
         gxTv_SdtContrato_Contrato_datavigenciainicio_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datavigenciatermino_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datainiciota_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimta_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapublicacaodou_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datatermino_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataassinatura_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataterminoata_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimadaptacao_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_calculodivergencia_Z = "";
         gxTv_SdtContrato_Contrato_prepostonom_Z = "";
         gxTv_SdtContrato_Contrato_prdftrcada_Z = "";
         gxTv_SdtContrato_Contrato_identificacao_Z = "";
      }

      public SdtContrato( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV74Contrato_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV74Contrato_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Contrato_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Contrato");
         metadata.Set("BT", "Contrato");
         metadata.Set("PK", "[ \"Contrato_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Contrato_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[ \"Contrato_AreaTrabalhoCod-AreaTrabalho_Codigo\" ] },{ \"FK\":[ \"Contratada_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"Contrato_PrepostoCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_areatrabalhodes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacnpj_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_tipofabrica_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_numero_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_numeroata_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_ano_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_unidadecontratacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_quantidade_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datavigenciainicio_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datavigenciatermino_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datainiciota_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datafimta_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datapublicacaodou_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datatermino_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_dataassinatura_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datapedidoreajuste_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_dataterminoata_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datafimadaptacao_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_valor_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_valorunidadecontratacao_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_valorundcntatual_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_diaspagto_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_calculodivergencia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_indicedivergencia_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_aceitapffs_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prepostocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prepostopescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prepostonom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prdftrcada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_lmtftr_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prdftrini_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prdftrfim_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_identificacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_areatrabalhodes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacnpj_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_numeroata_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datainiciota_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_datafimta_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_aceitapffs_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prepostocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prepostopescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prepostonom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prdftrcada_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_lmtftr_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prdftrini_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contrato_prdftrfim_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContrato deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContrato)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContrato obj ;
         obj = this;
         obj.gxTpr_Contrato_codigo = deserialized.gxTpr_Contrato_codigo;
         obj.gxTpr_Contrato_areatrabalhocod = deserialized.gxTpr_Contrato_areatrabalhocod;
         obj.gxTpr_Contrato_areatrabalhodes = deserialized.gxTpr_Contrato_areatrabalhodes;
         obj.gxTpr_Contratada_codigo = deserialized.gxTpr_Contratada_codigo;
         obj.gxTpr_Contratada_pessoacod = deserialized.gxTpr_Contratada_pessoacod;
         obj.gxTpr_Contratada_pessoanom = deserialized.gxTpr_Contratada_pessoanom;
         obj.gxTpr_Contratada_pessoacnpj = deserialized.gxTpr_Contratada_pessoacnpj;
         obj.gxTpr_Contratada_sigla = deserialized.gxTpr_Contratada_sigla;
         obj.gxTpr_Contratada_tipofabrica = deserialized.gxTpr_Contratada_tipofabrica;
         obj.gxTpr_Contrato_numero = deserialized.gxTpr_Contrato_numero;
         obj.gxTpr_Contrato_numeroata = deserialized.gxTpr_Contrato_numeroata;
         obj.gxTpr_Contrato_ano = deserialized.gxTpr_Contrato_ano;
         obj.gxTpr_Contrato_objeto = deserialized.gxTpr_Contrato_objeto;
         obj.gxTpr_Contrato_unidadecontratacao = deserialized.gxTpr_Contrato_unidadecontratacao;
         obj.gxTpr_Contrato_quantidade = deserialized.gxTpr_Contrato_quantidade;
         obj.gxTpr_Contrato_datavigenciainicio = deserialized.gxTpr_Contrato_datavigenciainicio;
         obj.gxTpr_Contrato_datavigenciatermino = deserialized.gxTpr_Contrato_datavigenciatermino;
         obj.gxTpr_Contrato_datainiciota = deserialized.gxTpr_Contrato_datainiciota;
         obj.gxTpr_Contrato_datafimta = deserialized.gxTpr_Contrato_datafimta;
         obj.gxTpr_Contrato_datapublicacaodou = deserialized.gxTpr_Contrato_datapublicacaodou;
         obj.gxTpr_Contrato_datatermino = deserialized.gxTpr_Contrato_datatermino;
         obj.gxTpr_Contrato_dataassinatura = deserialized.gxTpr_Contrato_dataassinatura;
         obj.gxTpr_Contrato_datapedidoreajuste = deserialized.gxTpr_Contrato_datapedidoreajuste;
         obj.gxTpr_Contrato_dataterminoata = deserialized.gxTpr_Contrato_dataterminoata;
         obj.gxTpr_Contrato_datafimadaptacao = deserialized.gxTpr_Contrato_datafimadaptacao;
         obj.gxTpr_Contrato_valor = deserialized.gxTpr_Contrato_valor;
         obj.gxTpr_Contrato_valorunidadecontratacao = deserialized.gxTpr_Contrato_valorunidadecontratacao;
         obj.gxTpr_Contrato_valorundcntatual = deserialized.gxTpr_Contrato_valorundcntatual;
         obj.gxTpr_Contrato_regraspagto = deserialized.gxTpr_Contrato_regraspagto;
         obj.gxTpr_Contrato_diaspagto = deserialized.gxTpr_Contrato_diaspagto;
         obj.gxTpr_Contrato_calculodivergencia = deserialized.gxTpr_Contrato_calculodivergencia;
         obj.gxTpr_Contrato_indicedivergencia = deserialized.gxTpr_Contrato_indicedivergencia;
         obj.gxTpr_Contrato_aceitapffs = deserialized.gxTpr_Contrato_aceitapffs;
         obj.gxTpr_Contrato_ativo = deserialized.gxTpr_Contrato_ativo;
         obj.gxTpr_Contrato_prepostocod = deserialized.gxTpr_Contrato_prepostocod;
         obj.gxTpr_Contrato_prepostopescod = deserialized.gxTpr_Contrato_prepostopescod;
         obj.gxTpr_Contrato_prepostonom = deserialized.gxTpr_Contrato_prepostonom;
         obj.gxTpr_Contrato_prdftrcada = deserialized.gxTpr_Contrato_prdftrcada;
         obj.gxTpr_Contrato_lmtftr = deserialized.gxTpr_Contrato_lmtftr;
         obj.gxTpr_Contrato_prdftrini = deserialized.gxTpr_Contrato_prdftrini;
         obj.gxTpr_Contrato_prdftrfim = deserialized.gxTpr_Contrato_prdftrfim;
         obj.gxTpr_Contrato_identificacao = deserialized.gxTpr_Contrato_identificacao;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contrato_codigo_Z = deserialized.gxTpr_Contrato_codigo_Z;
         obj.gxTpr_Contrato_areatrabalhocod_Z = deserialized.gxTpr_Contrato_areatrabalhocod_Z;
         obj.gxTpr_Contrato_areatrabalhodes_Z = deserialized.gxTpr_Contrato_areatrabalhodes_Z;
         obj.gxTpr_Contratada_codigo_Z = deserialized.gxTpr_Contratada_codigo_Z;
         obj.gxTpr_Contratada_pessoacod_Z = deserialized.gxTpr_Contratada_pessoacod_Z;
         obj.gxTpr_Contratada_pessoanom_Z = deserialized.gxTpr_Contratada_pessoanom_Z;
         obj.gxTpr_Contratada_pessoacnpj_Z = deserialized.gxTpr_Contratada_pessoacnpj_Z;
         obj.gxTpr_Contratada_sigla_Z = deserialized.gxTpr_Contratada_sigla_Z;
         obj.gxTpr_Contratada_tipofabrica_Z = deserialized.gxTpr_Contratada_tipofabrica_Z;
         obj.gxTpr_Contrato_numero_Z = deserialized.gxTpr_Contrato_numero_Z;
         obj.gxTpr_Contrato_numeroata_Z = deserialized.gxTpr_Contrato_numeroata_Z;
         obj.gxTpr_Contrato_ano_Z = deserialized.gxTpr_Contrato_ano_Z;
         obj.gxTpr_Contrato_unidadecontratacao_Z = deserialized.gxTpr_Contrato_unidadecontratacao_Z;
         obj.gxTpr_Contrato_quantidade_Z = deserialized.gxTpr_Contrato_quantidade_Z;
         obj.gxTpr_Contrato_datavigenciainicio_Z = deserialized.gxTpr_Contrato_datavigenciainicio_Z;
         obj.gxTpr_Contrato_datavigenciatermino_Z = deserialized.gxTpr_Contrato_datavigenciatermino_Z;
         obj.gxTpr_Contrato_datainiciota_Z = deserialized.gxTpr_Contrato_datainiciota_Z;
         obj.gxTpr_Contrato_datafimta_Z = deserialized.gxTpr_Contrato_datafimta_Z;
         obj.gxTpr_Contrato_datapublicacaodou_Z = deserialized.gxTpr_Contrato_datapublicacaodou_Z;
         obj.gxTpr_Contrato_datatermino_Z = deserialized.gxTpr_Contrato_datatermino_Z;
         obj.gxTpr_Contrato_dataassinatura_Z = deserialized.gxTpr_Contrato_dataassinatura_Z;
         obj.gxTpr_Contrato_datapedidoreajuste_Z = deserialized.gxTpr_Contrato_datapedidoreajuste_Z;
         obj.gxTpr_Contrato_dataterminoata_Z = deserialized.gxTpr_Contrato_dataterminoata_Z;
         obj.gxTpr_Contrato_datafimadaptacao_Z = deserialized.gxTpr_Contrato_datafimadaptacao_Z;
         obj.gxTpr_Contrato_valor_Z = deserialized.gxTpr_Contrato_valor_Z;
         obj.gxTpr_Contrato_valorunidadecontratacao_Z = deserialized.gxTpr_Contrato_valorunidadecontratacao_Z;
         obj.gxTpr_Contrato_valorundcntatual_Z = deserialized.gxTpr_Contrato_valorundcntatual_Z;
         obj.gxTpr_Contrato_diaspagto_Z = deserialized.gxTpr_Contrato_diaspagto_Z;
         obj.gxTpr_Contrato_calculodivergencia_Z = deserialized.gxTpr_Contrato_calculodivergencia_Z;
         obj.gxTpr_Contrato_indicedivergencia_Z = deserialized.gxTpr_Contrato_indicedivergencia_Z;
         obj.gxTpr_Contrato_aceitapffs_Z = deserialized.gxTpr_Contrato_aceitapffs_Z;
         obj.gxTpr_Contrato_ativo_Z = deserialized.gxTpr_Contrato_ativo_Z;
         obj.gxTpr_Contrato_prepostocod_Z = deserialized.gxTpr_Contrato_prepostocod_Z;
         obj.gxTpr_Contrato_prepostopescod_Z = deserialized.gxTpr_Contrato_prepostopescod_Z;
         obj.gxTpr_Contrato_prepostonom_Z = deserialized.gxTpr_Contrato_prepostonom_Z;
         obj.gxTpr_Contrato_prdftrcada_Z = deserialized.gxTpr_Contrato_prdftrcada_Z;
         obj.gxTpr_Contrato_lmtftr_Z = deserialized.gxTpr_Contrato_lmtftr_Z;
         obj.gxTpr_Contrato_prdftrini_Z = deserialized.gxTpr_Contrato_prdftrini_Z;
         obj.gxTpr_Contrato_prdftrfim_Z = deserialized.gxTpr_Contrato_prdftrfim_Z;
         obj.gxTpr_Contrato_identificacao_Z = deserialized.gxTpr_Contrato_identificacao_Z;
         obj.gxTpr_Contrato_codigo_N = deserialized.gxTpr_Contrato_codigo_N;
         obj.gxTpr_Contrato_areatrabalhodes_N = deserialized.gxTpr_Contrato_areatrabalhodes_N;
         obj.gxTpr_Contratada_pessoanom_N = deserialized.gxTpr_Contratada_pessoanom_N;
         obj.gxTpr_Contratada_pessoacnpj_N = deserialized.gxTpr_Contratada_pessoacnpj_N;
         obj.gxTpr_Contrato_numeroata_N = deserialized.gxTpr_Contrato_numeroata_N;
         obj.gxTpr_Contrato_datainiciota_N = deserialized.gxTpr_Contrato_datainiciota_N;
         obj.gxTpr_Contrato_datafimta_N = deserialized.gxTpr_Contrato_datafimta_N;
         obj.gxTpr_Contrato_aceitapffs_N = deserialized.gxTpr_Contrato_aceitapffs_N;
         obj.gxTpr_Contrato_prepostocod_N = deserialized.gxTpr_Contrato_prepostocod_N;
         obj.gxTpr_Contrato_prepostopescod_N = deserialized.gxTpr_Contrato_prepostopescod_N;
         obj.gxTpr_Contrato_prepostonom_N = deserialized.gxTpr_Contrato_prepostonom_N;
         obj.gxTpr_Contrato_prdftrcada_N = deserialized.gxTpr_Contrato_prdftrcada_N;
         obj.gxTpr_Contrato_lmtftr_N = deserialized.gxTpr_Contrato_lmtftr_N;
         obj.gxTpr_Contrato_prdftrini_N = deserialized.gxTpr_Contrato_prdftrini_N;
         obj.gxTpr_Contrato_prdftrfim_N = deserialized.gxTpr_Contrato_prdftrfim_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Codigo") )
               {
                  gxTv_SdtContrato_Contrato_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AreaTrabalhoCod") )
               {
                  gxTv_SdtContrato_Contrato_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AreaTrabalhoDes") )
               {
                  gxTv_SdtContrato_Contrato_areatrabalhodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo") )
               {
                  gxTv_SdtContrato_Contratada_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCod") )
               {
                  gxTv_SdtContrato_Contratada_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom") )
               {
                  gxTv_SdtContrato_Contratada_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ") )
               {
                  gxTv_SdtContrato_Contratada_pessoacnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Sigla") )
               {
                  gxTv_SdtContrato_Contratada_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_TipoFabrica") )
               {
                  gxTv_SdtContrato_Contratada_tipofabrica = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Numero") )
               {
                  gxTv_SdtContrato_Contrato_numero = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_NumeroAta") )
               {
                  gxTv_SdtContrato_Contrato_numeroata = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Ano") )
               {
                  gxTv_SdtContrato_Contrato_ano = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Objeto") )
               {
                  gxTv_SdtContrato_Contrato_objeto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_UnidadeContratacao") )
               {
                  gxTv_SdtContrato_Contrato_unidadecontratacao = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Quantidade") )
               {
                  gxTv_SdtContrato_Contrato_quantidade = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataVigenciaInicio") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datavigenciainicio = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datavigenciainicio = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataVigenciaTermino") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datavigenciatermino = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datavigenciatermino = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataInicioTA") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datainiciota = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datainiciota = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataFimTA") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datafimta = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datafimta = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataPublicacaoDOU") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datapublicacaodou = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datapublicacaodou = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataTermino") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datatermino = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datatermino = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataAssinatura") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_dataassinatura = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_dataassinatura = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataPedidoReajuste") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datapedidoreajuste = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datapedidoreajuste = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataTerminoAta") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_dataterminoata = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_dataterminoata = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataFimAdaptacao") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datafimadaptacao = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datafimadaptacao = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Valor") )
               {
                  gxTv_SdtContrato_Contrato_valor = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_ValorUnidadeContratacao") )
               {
                  gxTv_SdtContrato_Contrato_valorunidadecontratacao = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_ValorUndCntAtual") )
               {
                  gxTv_SdtContrato_Contrato_valorundcntatual = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_RegrasPagto") )
               {
                  gxTv_SdtContrato_Contrato_regraspagto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DiasPagto") )
               {
                  gxTv_SdtContrato_Contrato_diaspagto = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_CalculoDivergencia") )
               {
                  gxTv_SdtContrato_Contrato_calculodivergencia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_IndiceDivergencia") )
               {
                  gxTv_SdtContrato_Contrato_indicedivergencia = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AceitaPFFS") )
               {
                  gxTv_SdtContrato_Contrato_aceitapffs = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Ativo") )
               {
                  gxTv_SdtContrato_Contrato_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoCod") )
               {
                  gxTv_SdtContrato_Contrato_prepostocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoPesCod") )
               {
                  gxTv_SdtContrato_Contrato_prepostopescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoNom") )
               {
                  gxTv_SdtContrato_Contrato_prepostonom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrCada") )
               {
                  gxTv_SdtContrato_Contrato_prdftrcada = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_LmtFtr") )
               {
                  gxTv_SdtContrato_Contrato_lmtftr = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrIni") )
               {
                  gxTv_SdtContrato_Contrato_prdftrini = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrFim") )
               {
                  gxTv_SdtContrato_Contrato_prdftrfim = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Identificacao") )
               {
                  gxTv_SdtContrato_Contrato_identificacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContrato_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContrato_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Codigo_Z") )
               {
                  gxTv_SdtContrato_Contrato_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContrato_Contrato_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AreaTrabalhoDes_Z") )
               {
                  gxTv_SdtContrato_Contrato_areatrabalhodes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo_Z") )
               {
                  gxTv_SdtContrato_Contratada_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCod_Z") )
               {
                  gxTv_SdtContrato_Contratada_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom_Z") )
               {
                  gxTv_SdtContrato_Contratada_pessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ_Z") )
               {
                  gxTv_SdtContrato_Contratada_pessoacnpj_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Sigla_Z") )
               {
                  gxTv_SdtContrato_Contratada_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_TipoFabrica_Z") )
               {
                  gxTv_SdtContrato_Contratada_tipofabrica_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Numero_Z") )
               {
                  gxTv_SdtContrato_Contrato_numero_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_NumeroAta_Z") )
               {
                  gxTv_SdtContrato_Contrato_numeroata_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Ano_Z") )
               {
                  gxTv_SdtContrato_Contrato_ano_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_UnidadeContratacao_Z") )
               {
                  gxTv_SdtContrato_Contrato_unidadecontratacao_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Quantidade_Z") )
               {
                  gxTv_SdtContrato_Contrato_quantidade_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataVigenciaInicio_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datavigenciainicio_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datavigenciainicio_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataVigenciaTermino_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datavigenciatermino_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datavigenciatermino_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataInicioTA_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datainiciota_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datainiciota_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataFimTA_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datafimta_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datafimta_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataPublicacaoDOU_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datapublicacaodou_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datapublicacaodou_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataTermino_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datatermino_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datatermino_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataAssinatura_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_dataassinatura_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_dataassinatura_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataPedidoReajuste_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataTerminoAta_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_dataterminoata_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_dataterminoata_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataFimAdaptacao_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContrato_Contrato_datafimadaptacao_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContrato_Contrato_datafimadaptacao_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Valor_Z") )
               {
                  gxTv_SdtContrato_Contrato_valor_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_ValorUnidadeContratacao_Z") )
               {
                  gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_ValorUndCntAtual_Z") )
               {
                  gxTv_SdtContrato_Contrato_valorundcntatual_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DiasPagto_Z") )
               {
                  gxTv_SdtContrato_Contrato_diaspagto_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_CalculoDivergencia_Z") )
               {
                  gxTv_SdtContrato_Contrato_calculodivergencia_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_IndiceDivergencia_Z") )
               {
                  gxTv_SdtContrato_Contrato_indicedivergencia_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AceitaPFFS_Z") )
               {
                  gxTv_SdtContrato_Contrato_aceitapffs_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Ativo_Z") )
               {
                  gxTv_SdtContrato_Contrato_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoCod_Z") )
               {
                  gxTv_SdtContrato_Contrato_prepostocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoPesCod_Z") )
               {
                  gxTv_SdtContrato_Contrato_prepostopescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoNom_Z") )
               {
                  gxTv_SdtContrato_Contrato_prepostonom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrCada_Z") )
               {
                  gxTv_SdtContrato_Contrato_prdftrcada_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_LmtFtr_Z") )
               {
                  gxTv_SdtContrato_Contrato_lmtftr_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrIni_Z") )
               {
                  gxTv_SdtContrato_Contrato_prdftrini_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrFim_Z") )
               {
                  gxTv_SdtContrato_Contrato_prdftrfim_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Identificacao_Z") )
               {
                  gxTv_SdtContrato_Contrato_identificacao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Codigo_N") )
               {
                  gxTv_SdtContrato_Contrato_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AreaTrabalhoDes_N") )
               {
                  gxTv_SdtContrato_Contrato_areatrabalhodes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom_N") )
               {
                  gxTv_SdtContrato_Contratada_pessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ_N") )
               {
                  gxTv_SdtContrato_Contratada_pessoacnpj_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_NumeroAta_N") )
               {
                  gxTv_SdtContrato_Contrato_numeroata_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataInicioTA_N") )
               {
                  gxTv_SdtContrato_Contrato_datainiciota_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_DataFimTA_N") )
               {
                  gxTv_SdtContrato_Contrato_datafimta_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_AceitaPFFS_N") )
               {
                  gxTv_SdtContrato_Contrato_aceitapffs_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoCod_N") )
               {
                  gxTv_SdtContrato_Contrato_prepostocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoPesCod_N") )
               {
                  gxTv_SdtContrato_Contrato_prepostopescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrepostoNom_N") )
               {
                  gxTv_SdtContrato_Contrato_prepostonom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrCada_N") )
               {
                  gxTv_SdtContrato_Contrato_prdftrcada_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_LmtFtr_N") )
               {
                  gxTv_SdtContrato_Contrato_lmtftr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrIni_N") )
               {
                  gxTv_SdtContrato_Contrato_prdftrini_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_PrdFtrFim_N") )
               {
                  gxTv_SdtContrato_Contrato_prdftrfim_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Contrato";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Contrato_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_AreaTrabalhoDes", StringUtil.RTrim( gxTv_SdtContrato_Contrato_areatrabalhodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contratada_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contratada_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaNom", StringUtil.RTrim( gxTv_SdtContrato_Contratada_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaCNPJ", StringUtil.RTrim( gxTv_SdtContrato_Contratada_pessoacnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Sigla", StringUtil.RTrim( gxTv_SdtContrato_Contratada_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_TipoFabrica", StringUtil.RTrim( gxTv_SdtContrato_Contratada_tipofabrica));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_Numero", StringUtil.RTrim( gxTv_SdtContrato_Contrato_numero));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_NumeroAta", StringUtil.RTrim( gxTv_SdtContrato_Contrato_numeroata));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_Ano", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_ano), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_Objeto", StringUtil.RTrim( gxTv_SdtContrato_Contrato_objeto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_UnidadeContratacao", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_unidadecontratacao), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_Quantidade", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_quantidade), 9, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datavigenciainicio) )
         {
            oWriter.WriteStartElement("Contrato_DataVigenciaInicio");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciainicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciainicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciainicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataVigenciaInicio", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datavigenciatermino) )
         {
            oWriter.WriteStartElement("Contrato_DataVigenciaTermino");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciatermino)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciatermino)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciatermino)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataVigenciaTermino", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datainiciota) )
         {
            oWriter.WriteStartElement("Contrato_DataInicioTA");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datainiciota)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datainiciota)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datainiciota)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataInicioTA", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datafimta) )
         {
            oWriter.WriteStartElement("Contrato_DataFimTA");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimta)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimta)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimta)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataFimTA", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datapublicacaodou) )
         {
            oWriter.WriteStartElement("Contrato_DataPublicacaoDOU");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapublicacaodou)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapublicacaodou)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapublicacaodou)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataPublicacaoDOU", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datatermino) )
         {
            oWriter.WriteStartElement("Contrato_DataTermino");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datatermino)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datatermino)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datatermino)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataTermino", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_dataassinatura) )
         {
            oWriter.WriteStartElement("Contrato_DataAssinatura");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataassinatura)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataassinatura)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataassinatura)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataAssinatura", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datapedidoreajuste) )
         {
            oWriter.WriteStartElement("Contrato_DataPedidoReajuste");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapedidoreajuste)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapedidoreajuste)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapedidoreajuste)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataPedidoReajuste", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_dataterminoata) )
         {
            oWriter.WriteStartElement("Contrato_DataTerminoAta");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataterminoata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataterminoata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataterminoata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataTerminoAta", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datafimadaptacao) )
         {
            oWriter.WriteStartElement("Contrato_DataFimAdaptacao");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimadaptacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimadaptacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimadaptacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contrato_DataFimAdaptacao", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Contrato_Valor", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_valor, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_ValorUnidadeContratacao", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorunidadecontratacao, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_ValorUndCntAtual", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorundcntatual, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_RegrasPagto", StringUtil.RTrim( gxTv_SdtContrato_Contrato_regraspagto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_DiasPagto", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_diaspagto), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_CalculoDivergencia", StringUtil.RTrim( gxTv_SdtContrato_Contrato_calculodivergencia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_IndiceDivergencia", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_indicedivergencia, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_AceitaPFFS", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContrato_Contrato_aceitapffs)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContrato_Contrato_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_PrepostoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prepostocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_PrepostoPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prepostopescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_PrepostoNom", StringUtil.RTrim( gxTv_SdtContrato_Contrato_prepostonom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_PrdFtrCada", StringUtil.RTrim( gxTv_SdtContrato_Contrato_prdftrcada));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_LmtFtr", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_lmtftr, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_PrdFtrIni", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prdftrini), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_PrdFtrFim", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prdftrfim), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contrato_Identificacao", StringUtil.RTrim( gxTv_SdtContrato_Contrato_identificacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContrato_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_AreaTrabalhoDes_Z", StringUtil.RTrim( gxTv_SdtContrato_Contrato_areatrabalhodes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contratada_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contratada_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaNom_Z", StringUtil.RTrim( gxTv_SdtContrato_Contratada_pessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaCNPJ_Z", StringUtil.RTrim( gxTv_SdtContrato_Contratada_pessoacnpj_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Sigla_Z", StringUtil.RTrim( gxTv_SdtContrato_Contratada_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_TipoFabrica_Z", StringUtil.RTrim( gxTv_SdtContrato_Contratada_tipofabrica_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_Numero_Z", StringUtil.RTrim( gxTv_SdtContrato_Contrato_numero_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_NumeroAta_Z", StringUtil.RTrim( gxTv_SdtContrato_Contrato_numeroata_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_Ano_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_ano_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_UnidadeContratacao_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_unidadecontratacao_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_Quantidade_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_quantidade_Z), 9, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datavigenciainicio_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataVigenciaInicio_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciainicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciainicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciainicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataVigenciaInicio_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datavigenciatermino_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataVigenciaTermino_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciatermino_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciatermino_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciatermino_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataVigenciaTermino_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datainiciota_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataInicioTA_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datainiciota_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datainiciota_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datainiciota_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataInicioTA_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datafimta_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataFimTA_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimta_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimta_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimta_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataFimTA_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datapublicacaodou_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataPublicacaoDOU_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapublicacaodou_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapublicacaodou_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapublicacaodou_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataPublicacaoDOU_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datatermino_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataTermino_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datatermino_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datatermino_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datatermino_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataTermino_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_dataassinatura_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataAssinatura_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataassinatura_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataassinatura_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataassinatura_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataAssinatura_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datapedidoreajuste_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataPedidoReajuste_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapedidoreajuste_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapedidoreajuste_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapedidoreajuste_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataPedidoReajuste_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_dataterminoata_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataTerminoAta_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataterminoata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataterminoata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataterminoata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataTerminoAta_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContrato_Contrato_datafimadaptacao_Z) )
            {
               oWriter.WriteStartElement("Contrato_DataFimAdaptacao_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimadaptacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimadaptacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimadaptacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contrato_DataFimAdaptacao_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Contrato_Valor_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_valor_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_ValorUnidadeContratacao_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_ValorUndCntAtual_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorundcntatual_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_DiasPagto_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_diaspagto_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_CalculoDivergencia_Z", StringUtil.RTrim( gxTv_SdtContrato_Contrato_calculodivergencia_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_IndiceDivergencia_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_indicedivergencia_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_AceitaPFFS_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContrato_Contrato_aceitapffs_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContrato_Contrato_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrepostoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prepostocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrepostoPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prepostopescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrepostoNom_Z", StringUtil.RTrim( gxTv_SdtContrato_Contrato_prepostonom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrdFtrCada_Z", StringUtil.RTrim( gxTv_SdtContrato_Contrato_prdftrcada_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_LmtFtr_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContrato_Contrato_lmtftr_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrdFtrIni_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prdftrini_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrdFtrFim_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prdftrfim_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_Identificacao_Z", StringUtil.RTrim( gxTv_SdtContrato_Contrato_identificacao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_AreaTrabalhoDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_areatrabalhodes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contratada_pessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaCNPJ_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contratada_pessoacnpj_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_NumeroAta_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_numeroata_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_DataInicioTA_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_datainiciota_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_DataFimTA_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_datafimta_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_AceitaPFFS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_aceitapffs_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrepostoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prepostocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrepostoPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prepostopescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrepostoNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prepostonom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrdFtrCada_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prdftrcada_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_LmtFtr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_lmtftr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrdFtrIni_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prdftrini_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contrato_PrdFtrFim_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContrato_Contrato_prdftrfim_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Contrato_Codigo", gxTv_SdtContrato_Contrato_codigo, false);
         AddObjectProperty("Contrato_AreaTrabalhoCod", gxTv_SdtContrato_Contrato_areatrabalhocod, false);
         AddObjectProperty("Contrato_AreaTrabalhoDes", gxTv_SdtContrato_Contrato_areatrabalhodes, false);
         AddObjectProperty("Contratada_Codigo", gxTv_SdtContrato_Contratada_codigo, false);
         AddObjectProperty("Contratada_PessoaCod", gxTv_SdtContrato_Contratada_pessoacod, false);
         AddObjectProperty("Contratada_PessoaNom", gxTv_SdtContrato_Contratada_pessoanom, false);
         AddObjectProperty("Contratada_PessoaCNPJ", gxTv_SdtContrato_Contratada_pessoacnpj, false);
         AddObjectProperty("Contratada_Sigla", gxTv_SdtContrato_Contratada_sigla, false);
         AddObjectProperty("Contratada_TipoFabrica", gxTv_SdtContrato_Contratada_tipofabrica, false);
         AddObjectProperty("Contrato_Numero", gxTv_SdtContrato_Contrato_numero, false);
         AddObjectProperty("Contrato_NumeroAta", gxTv_SdtContrato_Contrato_numeroata, false);
         AddObjectProperty("Contrato_Ano", gxTv_SdtContrato_Contrato_ano, false);
         AddObjectProperty("Contrato_Objeto", gxTv_SdtContrato_Contrato_objeto, false);
         AddObjectProperty("Contrato_UnidadeContratacao", gxTv_SdtContrato_Contrato_unidadecontratacao, false);
         AddObjectProperty("Contrato_Quantidade", gxTv_SdtContrato_Contrato_quantidade, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciainicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciainicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciainicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataVigenciaInicio", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciatermino)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciatermino)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciatermino)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataVigenciaTermino", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datainiciota)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datainiciota)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datainiciota)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataInicioTA", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimta)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimta)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimta)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataFimTA", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapublicacaodou)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapublicacaodou)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapublicacaodou)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataPublicacaoDOU", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datatermino)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datatermino)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datatermino)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataTermino", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataassinatura)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataassinatura)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataassinatura)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataAssinatura", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapedidoreajuste)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapedidoreajuste)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapedidoreajuste)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataPedidoReajuste", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataterminoata)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataterminoata)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataterminoata)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataTerminoAta", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimadaptacao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimadaptacao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimadaptacao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contrato_DataFimAdaptacao", sDateCnv, false);
         AddObjectProperty("Contrato_Valor", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContrato_Contrato_valor, 18, 5)), false);
         AddObjectProperty("Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorunidadecontratacao, 18, 5)), false);
         AddObjectProperty("Contrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorundcntatual, 18, 5)), false);
         AddObjectProperty("Contrato_RegrasPagto", gxTv_SdtContrato_Contrato_regraspagto, false);
         AddObjectProperty("Contrato_DiasPagto", gxTv_SdtContrato_Contrato_diaspagto, false);
         AddObjectProperty("Contrato_CalculoDivergencia", gxTv_SdtContrato_Contrato_calculodivergencia, false);
         AddObjectProperty("Contrato_IndiceDivergencia", gxTv_SdtContrato_Contrato_indicedivergencia, false);
         AddObjectProperty("Contrato_AceitaPFFS", gxTv_SdtContrato_Contrato_aceitapffs, false);
         AddObjectProperty("Contrato_Ativo", gxTv_SdtContrato_Contrato_ativo, false);
         AddObjectProperty("Contrato_PrepostoCod", gxTv_SdtContrato_Contrato_prepostocod, false);
         AddObjectProperty("Contrato_PrepostoPesCod", gxTv_SdtContrato_Contrato_prepostopescod, false);
         AddObjectProperty("Contrato_PrepostoNom", gxTv_SdtContrato_Contrato_prepostonom, false);
         AddObjectProperty("Contrato_PrdFtrCada", gxTv_SdtContrato_Contrato_prdftrcada, false);
         AddObjectProperty("Contrato_LmtFtr", gxTv_SdtContrato_Contrato_lmtftr, false);
         AddObjectProperty("Contrato_PrdFtrIni", gxTv_SdtContrato_Contrato_prdftrini, false);
         AddObjectProperty("Contrato_PrdFtrFim", gxTv_SdtContrato_Contrato_prdftrfim, false);
         AddObjectProperty("Contrato_Identificacao", gxTv_SdtContrato_Contrato_identificacao, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContrato_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContrato_Initialized, false);
            AddObjectProperty("Contrato_Codigo_Z", gxTv_SdtContrato_Contrato_codigo_Z, false);
            AddObjectProperty("Contrato_AreaTrabalhoCod_Z", gxTv_SdtContrato_Contrato_areatrabalhocod_Z, false);
            AddObjectProperty("Contrato_AreaTrabalhoDes_Z", gxTv_SdtContrato_Contrato_areatrabalhodes_Z, false);
            AddObjectProperty("Contratada_Codigo_Z", gxTv_SdtContrato_Contratada_codigo_Z, false);
            AddObjectProperty("Contratada_PessoaCod_Z", gxTv_SdtContrato_Contratada_pessoacod_Z, false);
            AddObjectProperty("Contratada_PessoaNom_Z", gxTv_SdtContrato_Contratada_pessoanom_Z, false);
            AddObjectProperty("Contratada_PessoaCNPJ_Z", gxTv_SdtContrato_Contratada_pessoacnpj_Z, false);
            AddObjectProperty("Contratada_Sigla_Z", gxTv_SdtContrato_Contratada_sigla_Z, false);
            AddObjectProperty("Contratada_TipoFabrica_Z", gxTv_SdtContrato_Contratada_tipofabrica_Z, false);
            AddObjectProperty("Contrato_Numero_Z", gxTv_SdtContrato_Contrato_numero_Z, false);
            AddObjectProperty("Contrato_NumeroAta_Z", gxTv_SdtContrato_Contrato_numeroata_Z, false);
            AddObjectProperty("Contrato_Ano_Z", gxTv_SdtContrato_Contrato_ano_Z, false);
            AddObjectProperty("Contrato_UnidadeContratacao_Z", gxTv_SdtContrato_Contrato_unidadecontratacao_Z, false);
            AddObjectProperty("Contrato_Quantidade_Z", gxTv_SdtContrato_Contrato_quantidade_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciainicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciainicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciainicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataVigenciaInicio_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datavigenciatermino_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datavigenciatermino_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datavigenciatermino_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataVigenciaTermino_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datainiciota_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datainiciota_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datainiciota_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataInicioTA_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimta_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimta_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimta_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataFimTA_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapublicacaodou_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapublicacaodou_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapublicacaodou_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataPublicacaoDOU_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datatermino_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datatermino_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datatermino_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataTermino_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataassinatura_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataassinatura_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataassinatura_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataAssinatura_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datapedidoreajuste_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datapedidoreajuste_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datapedidoreajuste_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataPedidoReajuste_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_dataterminoata_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_dataterminoata_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_dataterminoata_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataTerminoAta_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContrato_Contrato_datafimadaptacao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContrato_Contrato_datafimadaptacao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContrato_Contrato_datafimadaptacao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contrato_DataFimAdaptacao_Z", sDateCnv, false);
            AddObjectProperty("Contrato_Valor_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContrato_Contrato_valor_Z, 18, 5)), false);
            AddObjectProperty("Contrato_ValorUnidadeContratacao_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z, 18, 5)), false);
            AddObjectProperty("Contrato_ValorUndCntAtual_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContrato_Contrato_valorundcntatual_Z, 18, 5)), false);
            AddObjectProperty("Contrato_DiasPagto_Z", gxTv_SdtContrato_Contrato_diaspagto_Z, false);
            AddObjectProperty("Contrato_CalculoDivergencia_Z", gxTv_SdtContrato_Contrato_calculodivergencia_Z, false);
            AddObjectProperty("Contrato_IndiceDivergencia_Z", gxTv_SdtContrato_Contrato_indicedivergencia_Z, false);
            AddObjectProperty("Contrato_AceitaPFFS_Z", gxTv_SdtContrato_Contrato_aceitapffs_Z, false);
            AddObjectProperty("Contrato_Ativo_Z", gxTv_SdtContrato_Contrato_ativo_Z, false);
            AddObjectProperty("Contrato_PrepostoCod_Z", gxTv_SdtContrato_Contrato_prepostocod_Z, false);
            AddObjectProperty("Contrato_PrepostoPesCod_Z", gxTv_SdtContrato_Contrato_prepostopescod_Z, false);
            AddObjectProperty("Contrato_PrepostoNom_Z", gxTv_SdtContrato_Contrato_prepostonom_Z, false);
            AddObjectProperty("Contrato_PrdFtrCada_Z", gxTv_SdtContrato_Contrato_prdftrcada_Z, false);
            AddObjectProperty("Contrato_LmtFtr_Z", gxTv_SdtContrato_Contrato_lmtftr_Z, false);
            AddObjectProperty("Contrato_PrdFtrIni_Z", gxTv_SdtContrato_Contrato_prdftrini_Z, false);
            AddObjectProperty("Contrato_PrdFtrFim_Z", gxTv_SdtContrato_Contrato_prdftrfim_Z, false);
            AddObjectProperty("Contrato_Identificacao_Z", gxTv_SdtContrato_Contrato_identificacao_Z, false);
            AddObjectProperty("Contrato_Codigo_N", gxTv_SdtContrato_Contrato_codigo_N, false);
            AddObjectProperty("Contrato_AreaTrabalhoDes_N", gxTv_SdtContrato_Contrato_areatrabalhodes_N, false);
            AddObjectProperty("Contratada_PessoaNom_N", gxTv_SdtContrato_Contratada_pessoanom_N, false);
            AddObjectProperty("Contratada_PessoaCNPJ_N", gxTv_SdtContrato_Contratada_pessoacnpj_N, false);
            AddObjectProperty("Contrato_NumeroAta_N", gxTv_SdtContrato_Contrato_numeroata_N, false);
            AddObjectProperty("Contrato_DataInicioTA_N", gxTv_SdtContrato_Contrato_datainiciota_N, false);
            AddObjectProperty("Contrato_DataFimTA_N", gxTv_SdtContrato_Contrato_datafimta_N, false);
            AddObjectProperty("Contrato_AceitaPFFS_N", gxTv_SdtContrato_Contrato_aceitapffs_N, false);
            AddObjectProperty("Contrato_PrepostoCod_N", gxTv_SdtContrato_Contrato_prepostocod_N, false);
            AddObjectProperty("Contrato_PrepostoPesCod_N", gxTv_SdtContrato_Contrato_prepostopescod_N, false);
            AddObjectProperty("Contrato_PrepostoNom_N", gxTv_SdtContrato_Contrato_prepostonom_N, false);
            AddObjectProperty("Contrato_PrdFtrCada_N", gxTv_SdtContrato_Contrato_prdftrcada_N, false);
            AddObjectProperty("Contrato_LmtFtr_N", gxTv_SdtContrato_Contrato_lmtftr_N, false);
            AddObjectProperty("Contrato_PrdFtrIni_N", gxTv_SdtContrato_Contrato_prdftrini_N, false);
            AddObjectProperty("Contrato_PrdFtrFim_N", gxTv_SdtContrato_Contrato_prdftrfim_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Contrato_Codigo" )]
      [  XmlElement( ElementName = "Contrato_Codigo"   )]
      public int gxTpr_Contrato_codigo
      {
         get {
            return gxTv_SdtContrato_Contrato_codigo ;
         }

         set {
            if ( gxTv_SdtContrato_Contrato_codigo != value )
            {
               gxTv_SdtContrato_Mode = "INS";
               this.gxTv_SdtContrato_Contrato_codigo_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_areatrabalhodes_Z_SetNull( );
               this.gxTv_SdtContrato_Contratada_codigo_Z_SetNull( );
               this.gxTv_SdtContrato_Contratada_pessoacod_Z_SetNull( );
               this.gxTv_SdtContrato_Contratada_pessoanom_Z_SetNull( );
               this.gxTv_SdtContrato_Contratada_pessoacnpj_Z_SetNull( );
               this.gxTv_SdtContrato_Contratada_sigla_Z_SetNull( );
               this.gxTv_SdtContrato_Contratada_tipofabrica_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_numero_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_numeroata_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_ano_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_unidadecontratacao_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_quantidade_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datavigenciainicio_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datavigenciatermino_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datainiciota_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datafimta_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datapublicacaodou_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datatermino_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_dataassinatura_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datapedidoreajuste_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_dataterminoata_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_datafimadaptacao_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_valor_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_valorundcntatual_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_diaspagto_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_calculodivergencia_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_indicedivergencia_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_aceitapffs_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_ativo_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_prepostocod_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_prepostopescod_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_prepostonom_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_prdftrcada_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_lmtftr_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_prdftrini_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_prdftrfim_Z_SetNull( );
               this.gxTv_SdtContrato_Contrato_identificacao_Z_SetNull( );
            }
            gxTv_SdtContrato_Contrato_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Contrato_AreaTrabalhoCod"   )]
      public int gxTpr_Contrato_areatrabalhocod
      {
         get {
            return gxTv_SdtContrato_Contrato_areatrabalhocod ;
         }

         set {
            gxTv_SdtContrato_Contrato_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_AreaTrabalhoDes" )]
      [  XmlElement( ElementName = "Contrato_AreaTrabalhoDes"   )]
      public String gxTpr_Contrato_areatrabalhodes
      {
         get {
            return gxTv_SdtContrato_Contrato_areatrabalhodes ;
         }

         set {
            gxTv_SdtContrato_Contrato_areatrabalhodes_N = 0;
            gxTv_SdtContrato_Contrato_areatrabalhodes = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_areatrabalhodes_SetNull( )
      {
         gxTv_SdtContrato_Contrato_areatrabalhodes_N = 1;
         gxTv_SdtContrato_Contrato_areatrabalhodes = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_areatrabalhodes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Codigo" )]
      [  XmlElement( ElementName = "Contratada_Codigo"   )]
      public int gxTpr_Contratada_codigo
      {
         get {
            return gxTv_SdtContrato_Contratada_codigo ;
         }

         set {
            gxTv_SdtContrato_Contratada_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaCod" )]
      [  XmlElement( ElementName = "Contratada_PessoaCod"   )]
      public int gxTpr_Contratada_pessoacod
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoacod ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaNom" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom"   )]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoanom ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoanom_N = 0;
            gxTv_SdtContrato_Contratada_pessoanom = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_pessoanom_SetNull( )
      {
         gxTv_SdtContrato_Contratada_pessoanom_N = 1;
         gxTv_SdtContrato_Contratada_pessoanom = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_pessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ"   )]
      public String gxTpr_Contratada_pessoacnpj
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoacnpj ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoacnpj_N = 0;
            gxTv_SdtContrato_Contratada_pessoacnpj = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_pessoacnpj_SetNull( )
      {
         gxTv_SdtContrato_Contratada_pessoacnpj_N = 1;
         gxTv_SdtContrato_Contratada_pessoacnpj = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_pessoacnpj_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Sigla" )]
      [  XmlElement( ElementName = "Contratada_Sigla"   )]
      public String gxTpr_Contratada_sigla
      {
         get {
            return gxTv_SdtContrato_Contratada_sigla ;
         }

         set {
            gxTv_SdtContrato_Contratada_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_TipoFabrica" )]
      [  XmlElement( ElementName = "Contratada_TipoFabrica"   )]
      public String gxTpr_Contratada_tipofabrica
      {
         get {
            return gxTv_SdtContrato_Contratada_tipofabrica ;
         }

         set {
            gxTv_SdtContrato_Contratada_tipofabrica = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_Numero" )]
      [  XmlElement( ElementName = "Contrato_Numero"   )]
      public String gxTpr_Contrato_numero
      {
         get {
            return gxTv_SdtContrato_Contrato_numero ;
         }

         set {
            gxTv_SdtContrato_Contrato_numero = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_NumeroAta" )]
      [  XmlElement( ElementName = "Contrato_NumeroAta"   )]
      public String gxTpr_Contrato_numeroata
      {
         get {
            return gxTv_SdtContrato_Contrato_numeroata ;
         }

         set {
            gxTv_SdtContrato_Contrato_numeroata_N = 0;
            gxTv_SdtContrato_Contrato_numeroata = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_numeroata_SetNull( )
      {
         gxTv_SdtContrato_Contrato_numeroata_N = 1;
         gxTv_SdtContrato_Contrato_numeroata = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_numeroata_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Ano" )]
      [  XmlElement( ElementName = "Contrato_Ano"   )]
      public short gxTpr_Contrato_ano
      {
         get {
            return gxTv_SdtContrato_Contrato_ano ;
         }

         set {
            gxTv_SdtContrato_Contrato_ano = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_Objeto" )]
      [  XmlElement( ElementName = "Contrato_Objeto"   )]
      public String gxTpr_Contrato_objeto
      {
         get {
            return gxTv_SdtContrato_Contrato_objeto ;
         }

         set {
            gxTv_SdtContrato_Contrato_objeto = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_UnidadeContratacao" )]
      [  XmlElement( ElementName = "Contrato_UnidadeContratacao"   )]
      public short gxTpr_Contrato_unidadecontratacao
      {
         get {
            return gxTv_SdtContrato_Contrato_unidadecontratacao ;
         }

         set {
            gxTv_SdtContrato_Contrato_unidadecontratacao = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_Quantidade" )]
      [  XmlElement( ElementName = "Contrato_Quantidade"   )]
      public int gxTpr_Contrato_quantidade
      {
         get {
            return gxTv_SdtContrato_Contrato_quantidade ;
         }

         set {
            gxTv_SdtContrato_Contrato_quantidade = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DataVigenciaInicio" )]
      [  XmlElement( ElementName = "Contrato_DataVigenciaInicio"  , IsNullable=true )]
      public string gxTpr_Contrato_datavigenciainicio_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datavigenciainicio == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datavigenciainicio).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datavigenciainicio = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datavigenciainicio = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datavigenciainicio
      {
         get {
            return gxTv_SdtContrato_Contrato_datavigenciainicio ;
         }

         set {
            gxTv_SdtContrato_Contrato_datavigenciainicio = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DataVigenciaTermino" )]
      [  XmlElement( ElementName = "Contrato_DataVigenciaTermino"  , IsNullable=true )]
      public string gxTpr_Contrato_datavigenciatermino_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datavigenciatermino == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datavigenciatermino).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datavigenciatermino = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datavigenciatermino = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datavigenciatermino
      {
         get {
            return gxTv_SdtContrato_Contrato_datavigenciatermino ;
         }

         set {
            gxTv_SdtContrato_Contrato_datavigenciatermino = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DataInicioTA" )]
      [  XmlElement( ElementName = "Contrato_DataInicioTA"  , IsNullable=true )]
      public string gxTpr_Contrato_datainiciota_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datainiciota == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datainiciota).value ;
         }

         set {
            gxTv_SdtContrato_Contrato_datainiciota_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datainiciota = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datainiciota = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datainiciota
      {
         get {
            return gxTv_SdtContrato_Contrato_datainiciota ;
         }

         set {
            gxTv_SdtContrato_Contrato_datainiciota_N = 0;
            gxTv_SdtContrato_Contrato_datainiciota = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datainiciota_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datainiciota_N = 1;
         gxTv_SdtContrato_Contrato_datainiciota = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datainiciota_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataFimTA" )]
      [  XmlElement( ElementName = "Contrato_DataFimTA"  , IsNullable=true )]
      public string gxTpr_Contrato_datafimta_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datafimta == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datafimta).value ;
         }

         set {
            gxTv_SdtContrato_Contrato_datafimta_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datafimta = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datafimta = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datafimta
      {
         get {
            return gxTv_SdtContrato_Contrato_datafimta ;
         }

         set {
            gxTv_SdtContrato_Contrato_datafimta_N = 0;
            gxTv_SdtContrato_Contrato_datafimta = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datafimta_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datafimta_N = 1;
         gxTv_SdtContrato_Contrato_datafimta = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datafimta_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataPublicacaoDOU" )]
      [  XmlElement( ElementName = "Contrato_DataPublicacaoDOU"  , IsNullable=true )]
      public string gxTpr_Contrato_datapublicacaodou_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datapublicacaodou == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datapublicacaodou).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datapublicacaodou = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datapublicacaodou = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datapublicacaodou
      {
         get {
            return gxTv_SdtContrato_Contrato_datapublicacaodou ;
         }

         set {
            gxTv_SdtContrato_Contrato_datapublicacaodou = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DataTermino" )]
      [  XmlElement( ElementName = "Contrato_DataTermino"  , IsNullable=true )]
      public string gxTpr_Contrato_datatermino_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datatermino == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datatermino).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datatermino = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datatermino = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datatermino
      {
         get {
            return gxTv_SdtContrato_Contrato_datatermino ;
         }

         set {
            gxTv_SdtContrato_Contrato_datatermino = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datatermino_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datatermino = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datatermino_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataAssinatura" )]
      [  XmlElement( ElementName = "Contrato_DataAssinatura"  , IsNullable=true )]
      public string gxTpr_Contrato_dataassinatura_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_dataassinatura == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_dataassinatura).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_dataassinatura = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_dataassinatura = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_dataassinatura
      {
         get {
            return gxTv_SdtContrato_Contrato_dataassinatura ;
         }

         set {
            gxTv_SdtContrato_Contrato_dataassinatura = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DataPedidoReajuste" )]
      [  XmlElement( ElementName = "Contrato_DataPedidoReajuste"  , IsNullable=true )]
      public string gxTpr_Contrato_datapedidoreajuste_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datapedidoreajuste == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datapedidoreajuste).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datapedidoreajuste = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datapedidoreajuste = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datapedidoreajuste
      {
         get {
            return gxTv_SdtContrato_Contrato_datapedidoreajuste ;
         }

         set {
            gxTv_SdtContrato_Contrato_datapedidoreajuste = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DataTerminoAta" )]
      [  XmlElement( ElementName = "Contrato_DataTerminoAta"  , IsNullable=true )]
      public string gxTpr_Contrato_dataterminoata_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_dataterminoata == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_dataterminoata).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_dataterminoata = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_dataterminoata = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_dataterminoata
      {
         get {
            return gxTv_SdtContrato_Contrato_dataterminoata ;
         }

         set {
            gxTv_SdtContrato_Contrato_dataterminoata = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DataFimAdaptacao" )]
      [  XmlElement( ElementName = "Contrato_DataFimAdaptacao"  , IsNullable=true )]
      public string gxTpr_Contrato_datafimadaptacao_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datafimadaptacao == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datafimadaptacao).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datafimadaptacao = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datafimadaptacao = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datafimadaptacao
      {
         get {
            return gxTv_SdtContrato_Contrato_datafimadaptacao ;
         }

         set {
            gxTv_SdtContrato_Contrato_datafimadaptacao = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_Valor" )]
      [  XmlElement( ElementName = "Contrato_Valor"   )]
      public double gxTpr_Contrato_valor_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_valor) ;
         }

         set {
            gxTv_SdtContrato_Contrato_valor = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valor
      {
         get {
            return gxTv_SdtContrato_Contrato_valor ;
         }

         set {
            gxTv_SdtContrato_Contrato_valor = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_ValorUnidadeContratacao" )]
      [  XmlElement( ElementName = "Contrato_ValorUnidadeContratacao"   )]
      public double gxTpr_Contrato_valorunidadecontratacao_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_valorunidadecontratacao) ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorunidadecontratacao = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valorunidadecontratacao
      {
         get {
            return gxTv_SdtContrato_Contrato_valorunidadecontratacao ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorunidadecontratacao = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_ValorUndCntAtual" )]
      [  XmlElement( ElementName = "Contrato_ValorUndCntAtual"   )]
      public double gxTpr_Contrato_valorundcntatual_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_valorundcntatual) ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorundcntatual = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valorundcntatual
      {
         get {
            return gxTv_SdtContrato_Contrato_valorundcntatual ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorundcntatual = (decimal)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_valorundcntatual_SetNull( )
      {
         gxTv_SdtContrato_Contrato_valorundcntatual = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_valorundcntatual_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_RegrasPagto" )]
      [  XmlElement( ElementName = "Contrato_RegrasPagto"   )]
      public String gxTpr_Contrato_regraspagto
      {
         get {
            return gxTv_SdtContrato_Contrato_regraspagto ;
         }

         set {
            gxTv_SdtContrato_Contrato_regraspagto = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_DiasPagto" )]
      [  XmlElement( ElementName = "Contrato_DiasPagto"   )]
      public short gxTpr_Contrato_diaspagto
      {
         get {
            return gxTv_SdtContrato_Contrato_diaspagto ;
         }

         set {
            gxTv_SdtContrato_Contrato_diaspagto = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_CalculoDivergencia" )]
      [  XmlElement( ElementName = "Contrato_CalculoDivergencia"   )]
      public String gxTpr_Contrato_calculodivergencia
      {
         get {
            return gxTv_SdtContrato_Contrato_calculodivergencia ;
         }

         set {
            gxTv_SdtContrato_Contrato_calculodivergencia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_IndiceDivergencia" )]
      [  XmlElement( ElementName = "Contrato_IndiceDivergencia"   )]
      public double gxTpr_Contrato_indicedivergencia_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_indicedivergencia) ;
         }

         set {
            gxTv_SdtContrato_Contrato_indicedivergencia = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_indicedivergencia
      {
         get {
            return gxTv_SdtContrato_Contrato_indicedivergencia ;
         }

         set {
            gxTv_SdtContrato_Contrato_indicedivergencia = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_AceitaPFFS" )]
      [  XmlElement( ElementName = "Contrato_AceitaPFFS"   )]
      public bool gxTpr_Contrato_aceitapffs
      {
         get {
            return gxTv_SdtContrato_Contrato_aceitapffs ;
         }

         set {
            gxTv_SdtContrato_Contrato_aceitapffs_N = 0;
            gxTv_SdtContrato_Contrato_aceitapffs = value;
         }

      }

      public void gxTv_SdtContrato_Contrato_aceitapffs_SetNull( )
      {
         gxTv_SdtContrato_Contrato_aceitapffs_N = 1;
         gxTv_SdtContrato_Contrato_aceitapffs = false;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_aceitapffs_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Ativo" )]
      [  XmlElement( ElementName = "Contrato_Ativo"   )]
      public bool gxTpr_Contrato_ativo
      {
         get {
            return gxTv_SdtContrato_Contrato_ativo ;
         }

         set {
            gxTv_SdtContrato_Contrato_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Contrato_PrepostoCod" )]
      [  XmlElement( ElementName = "Contrato_PrepostoCod"   )]
      public int gxTpr_Contrato_prepostocod
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostocod ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostocod_N = 0;
            gxTv_SdtContrato_Contrato_prepostocod = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostocod_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostocod_N = 1;
         gxTv_SdtContrato_Contrato_prepostocod = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoPesCod" )]
      [  XmlElement( ElementName = "Contrato_PrepostoPesCod"   )]
      public int gxTpr_Contrato_prepostopescod
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostopescod ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostopescod_N = 0;
            gxTv_SdtContrato_Contrato_prepostopescod = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostopescod_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostopescod_N = 1;
         gxTv_SdtContrato_Contrato_prepostopescod = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostopescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoNom" )]
      [  XmlElement( ElementName = "Contrato_PrepostoNom"   )]
      public String gxTpr_Contrato_prepostonom
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostonom ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostonom_N = 0;
            gxTv_SdtContrato_Contrato_prepostonom = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostonom_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostonom_N = 1;
         gxTv_SdtContrato_Contrato_prepostonom = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostonom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrCada" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrCada"   )]
      public String gxTpr_Contrato_prdftrcada
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrcada ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrcada_N = 0;
            gxTv_SdtContrato_Contrato_prdftrcada = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrcada_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrcada_N = 1;
         gxTv_SdtContrato_Contrato_prdftrcada = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrcada_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_LmtFtr" )]
      [  XmlElement( ElementName = "Contrato_LmtFtr"   )]
      public double gxTpr_Contrato_lmtftr_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_lmtftr) ;
         }

         set {
            gxTv_SdtContrato_Contrato_lmtftr_N = 0;
            gxTv_SdtContrato_Contrato_lmtftr = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_lmtftr
      {
         get {
            return gxTv_SdtContrato_Contrato_lmtftr ;
         }

         set {
            gxTv_SdtContrato_Contrato_lmtftr_N = 0;
            gxTv_SdtContrato_Contrato_lmtftr = (decimal)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_lmtftr_SetNull( )
      {
         gxTv_SdtContrato_Contrato_lmtftr_N = 1;
         gxTv_SdtContrato_Contrato_lmtftr = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_lmtftr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrIni" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrIni"   )]
      public short gxTpr_Contrato_prdftrini
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrini ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrini_N = 0;
            gxTv_SdtContrato_Contrato_prdftrini = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrini_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrini_N = 1;
         gxTv_SdtContrato_Contrato_prdftrini = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrini_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrFim" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrFim"   )]
      public short gxTpr_Contrato_prdftrfim
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrfim ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrfim_N = 0;
            gxTv_SdtContrato_Contrato_prdftrfim = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrfim_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrfim_N = 1;
         gxTv_SdtContrato_Contrato_prdftrfim = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrfim_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Identificacao" )]
      [  XmlElement( ElementName = "Contrato_Identificacao"   )]
      public String gxTpr_Contrato_identificacao
      {
         get {
            return gxTv_SdtContrato_Contrato_identificacao ;
         }

         set {
            gxTv_SdtContrato_Contrato_identificacao = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_identificacao_SetNull( )
      {
         gxTv_SdtContrato_Contrato_identificacao = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_identificacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContrato_Mode ;
         }

         set {
            gxTv_SdtContrato_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Mode_SetNull( )
      {
         gxTv_SdtContrato_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContrato_Initialized ;
         }

         set {
            gxTv_SdtContrato_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Initialized_SetNull( )
      {
         gxTv_SdtContrato_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Codigo_Z" )]
      [  XmlElement( ElementName = "Contrato_Codigo_Z"   )]
      public int gxTpr_Contrato_codigo_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_codigo_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_codigo_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Contrato_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contrato_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_AreaTrabalhoDes_Z" )]
      [  XmlElement( ElementName = "Contrato_AreaTrabalhoDes_Z"   )]
      public String gxTpr_Contrato_areatrabalhodes_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_areatrabalhodes_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_areatrabalhodes_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_areatrabalhodes_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_areatrabalhodes_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_areatrabalhodes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Codigo_Z" )]
      [  XmlElement( ElementName = "Contratada_Codigo_Z"   )]
      public int gxTpr_Contratada_codigo_Z
      {
         get {
            return gxTv_SdtContrato_Contratada_codigo_Z ;
         }

         set {
            gxTv_SdtContrato_Contratada_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_codigo_Z_SetNull( )
      {
         gxTv_SdtContrato_Contratada_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCod_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaCod_Z"   )]
      public int gxTpr_Contratada_pessoacod_Z
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoacod_Z ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_pessoacod_Z_SetNull( )
      {
         gxTv_SdtContrato_Contratada_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaNom_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom_Z"   )]
      public String gxTpr_Contratada_pessoanom_Z
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoanom_Z ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_pessoanom_Z_SetNull( )
      {
         gxTv_SdtContrato_Contratada_pessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_pessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ_Z"   )]
      public String gxTpr_Contratada_pessoacnpj_Z
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoacnpj_Z ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoacnpj_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_pessoacnpj_Z_SetNull( )
      {
         gxTv_SdtContrato_Contratada_pessoacnpj_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_pessoacnpj_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Sigla_Z" )]
      [  XmlElement( ElementName = "Contratada_Sigla_Z"   )]
      public String gxTpr_Contratada_sigla_Z
      {
         get {
            return gxTv_SdtContrato_Contratada_sigla_Z ;
         }

         set {
            gxTv_SdtContrato_Contratada_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_sigla_Z_SetNull( )
      {
         gxTv_SdtContrato_Contratada_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_TipoFabrica_Z" )]
      [  XmlElement( ElementName = "Contratada_TipoFabrica_Z"   )]
      public String gxTpr_Contratada_tipofabrica_Z
      {
         get {
            return gxTv_SdtContrato_Contratada_tipofabrica_Z ;
         }

         set {
            gxTv_SdtContrato_Contratada_tipofabrica_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_tipofabrica_Z_SetNull( )
      {
         gxTv_SdtContrato_Contratada_tipofabrica_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_tipofabrica_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Numero_Z" )]
      [  XmlElement( ElementName = "Contrato_Numero_Z"   )]
      public String gxTpr_Contrato_numero_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_numero_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_numero_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_numero_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_numero_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_numero_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_NumeroAta_Z" )]
      [  XmlElement( ElementName = "Contrato_NumeroAta_Z"   )]
      public String gxTpr_Contrato_numeroata_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_numeroata_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_numeroata_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_numeroata_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_numeroata_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_numeroata_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Ano_Z" )]
      [  XmlElement( ElementName = "Contrato_Ano_Z"   )]
      public short gxTpr_Contrato_ano_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_ano_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_ano_Z = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_ano_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_ano_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_ano_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_UnidadeContratacao_Z" )]
      [  XmlElement( ElementName = "Contrato_UnidadeContratacao_Z"   )]
      public short gxTpr_Contrato_unidadecontratacao_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_unidadecontratacao_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_unidadecontratacao_Z = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_unidadecontratacao_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_unidadecontratacao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_unidadecontratacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Quantidade_Z" )]
      [  XmlElement( ElementName = "Contrato_Quantidade_Z"   )]
      public int gxTpr_Contrato_quantidade_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_quantidade_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_quantidade_Z = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_quantidade_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_quantidade_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_quantidade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataVigenciaInicio_Z" )]
      [  XmlElement( ElementName = "Contrato_DataVigenciaInicio_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datavigenciainicio_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datavigenciainicio_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datavigenciainicio_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datavigenciainicio_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datavigenciainicio_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datavigenciainicio_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datavigenciainicio_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datavigenciainicio_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datavigenciainicio_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datavigenciainicio_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datavigenciainicio_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataVigenciaTermino_Z" )]
      [  XmlElement( ElementName = "Contrato_DataVigenciaTermino_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datavigenciatermino_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datavigenciatermino_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datavigenciatermino_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datavigenciatermino_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datavigenciatermino_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datavigenciatermino_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datavigenciatermino_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datavigenciatermino_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datavigenciatermino_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datavigenciatermino_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datavigenciatermino_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataInicioTA_Z" )]
      [  XmlElement( ElementName = "Contrato_DataInicioTA_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datainiciota_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datainiciota_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datainiciota_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datainiciota_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datainiciota_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datainiciota_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datainiciota_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datainiciota_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datainiciota_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datainiciota_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datainiciota_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataFimTA_Z" )]
      [  XmlElement( ElementName = "Contrato_DataFimTA_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datafimta_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datafimta_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datafimta_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datafimta_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datafimta_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datafimta_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datafimta_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datafimta_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datafimta_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datafimta_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datafimta_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataPublicacaoDOU_Z" )]
      [  XmlElement( ElementName = "Contrato_DataPublicacaoDOU_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datapublicacaodou_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datapublicacaodou_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datapublicacaodou_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datapublicacaodou_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datapublicacaodou_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datapublicacaodou_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datapublicacaodou_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datapublicacaodou_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datapublicacaodou_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datapublicacaodou_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datapublicacaodou_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataTermino_Z" )]
      [  XmlElement( ElementName = "Contrato_DataTermino_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datatermino_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datatermino_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datatermino_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datatermino_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datatermino_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datatermino_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datatermino_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datatermino_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datatermino_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datatermino_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datatermino_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataAssinatura_Z" )]
      [  XmlElement( ElementName = "Contrato_DataAssinatura_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_dataassinatura_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_dataassinatura_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_dataassinatura_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_dataassinatura_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_dataassinatura_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_dataassinatura_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_dataassinatura_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_dataassinatura_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_dataassinatura_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_dataassinatura_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_dataassinatura_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataPedidoReajuste_Z" )]
      [  XmlElement( ElementName = "Contrato_DataPedidoReajuste_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datapedidoreajuste_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datapedidoreajuste_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datapedidoreajuste_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datapedidoreajuste_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datapedidoreajuste_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datapedidoreajuste_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datapedidoreajuste_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataTerminoAta_Z" )]
      [  XmlElement( ElementName = "Contrato_DataTerminoAta_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_dataterminoata_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_dataterminoata_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_dataterminoata_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_dataterminoata_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_dataterminoata_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_dataterminoata_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_dataterminoata_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_dataterminoata_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_dataterminoata_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_dataterminoata_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_dataterminoata_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataFimAdaptacao_Z" )]
      [  XmlElement( ElementName = "Contrato_DataFimAdaptacao_Z"  , IsNullable=true )]
      public string gxTpr_Contrato_datafimadaptacao_Z_Nullable
      {
         get {
            if ( gxTv_SdtContrato_Contrato_datafimadaptacao_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContrato_Contrato_datafimadaptacao_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContrato_Contrato_datafimadaptacao_Z = DateTime.MinValue;
            else
               gxTv_SdtContrato_Contrato_datafimadaptacao_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contrato_datafimadaptacao_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_datafimadaptacao_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_datafimadaptacao_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datafimadaptacao_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datafimadaptacao_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datafimadaptacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Valor_Z" )]
      [  XmlElement( ElementName = "Contrato_Valor_Z"   )]
      public double gxTpr_Contrato_valor_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_valor_Z) ;
         }

         set {
            gxTv_SdtContrato_Contrato_valor_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valor_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_valor_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_valor_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_valor_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_valor_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_valor_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_ValorUnidadeContratacao_Z" )]
      [  XmlElement( ElementName = "Contrato_ValorUnidadeContratacao_Z"   )]
      public double gxTpr_Contrato_valorunidadecontratacao_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z) ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valorunidadecontratacao_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_ValorUndCntAtual_Z" )]
      [  XmlElement( ElementName = "Contrato_ValorUndCntAtual_Z"   )]
      public double gxTpr_Contrato_valorundcntatual_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_valorundcntatual_Z) ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorundcntatual_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_valorundcntatual_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_valorundcntatual_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_valorundcntatual_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_valorundcntatual_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_valorundcntatual_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_valorundcntatual_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DiasPagto_Z" )]
      [  XmlElement( ElementName = "Contrato_DiasPagto_Z"   )]
      public short gxTpr_Contrato_diaspagto_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_diaspagto_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_diaspagto_Z = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_diaspagto_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_diaspagto_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_diaspagto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_CalculoDivergencia_Z" )]
      [  XmlElement( ElementName = "Contrato_CalculoDivergencia_Z"   )]
      public String gxTpr_Contrato_calculodivergencia_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_calculodivergencia_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_calculodivergencia_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_calculodivergencia_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_calculodivergencia_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_calculodivergencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_IndiceDivergencia_Z" )]
      [  XmlElement( ElementName = "Contrato_IndiceDivergencia_Z"   )]
      public double gxTpr_Contrato_indicedivergencia_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_indicedivergencia_Z) ;
         }

         set {
            gxTv_SdtContrato_Contrato_indicedivergencia_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_indicedivergencia_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_indicedivergencia_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_indicedivergencia_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_indicedivergencia_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_indicedivergencia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_indicedivergencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_AceitaPFFS_Z" )]
      [  XmlElement( ElementName = "Contrato_AceitaPFFS_Z"   )]
      public bool gxTpr_Contrato_aceitapffs_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_aceitapffs_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_aceitapffs_Z = value;
         }

      }

      public void gxTv_SdtContrato_Contrato_aceitapffs_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_aceitapffs_Z = false;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_aceitapffs_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Ativo_Z" )]
      [  XmlElement( ElementName = "Contrato_Ativo_Z"   )]
      public bool gxTpr_Contrato_ativo_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_ativo_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_ativo_Z = value;
         }

      }

      public void gxTv_SdtContrato_Contrato_ativo_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoCod_Z" )]
      [  XmlElement( ElementName = "Contrato_PrepostoCod_Z"   )]
      public int gxTpr_Contrato_prepostocod_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostocod_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostocod_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoPesCod_Z" )]
      [  XmlElement( ElementName = "Contrato_PrepostoPesCod_Z"   )]
      public int gxTpr_Contrato_prepostopescod_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostopescod_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostopescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostopescod_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostopescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostopescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoNom_Z" )]
      [  XmlElement( ElementName = "Contrato_PrepostoNom_Z"   )]
      public String gxTpr_Contrato_prepostonom_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostonom_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostonom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostonom_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostonom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostonom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrCada_Z" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrCada_Z"   )]
      public String gxTpr_Contrato_prdftrcada_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrcada_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrcada_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrcada_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrcada_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrcada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_LmtFtr_Z" )]
      [  XmlElement( ElementName = "Contrato_LmtFtr_Z"   )]
      public double gxTpr_Contrato_lmtftr_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContrato_Contrato_lmtftr_Z) ;
         }

         set {
            gxTv_SdtContrato_Contrato_lmtftr_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contrato_lmtftr_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_lmtftr_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_lmtftr_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_lmtftr_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_lmtftr_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_lmtftr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrIni_Z" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrIni_Z"   )]
      public short gxTpr_Contrato_prdftrini_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrini_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrini_Z = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrini_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrini_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrini_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrFim_Z" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrFim_Z"   )]
      public short gxTpr_Contrato_prdftrfim_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrfim_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrfim_Z = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrfim_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrfim_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrfim_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Identificacao_Z" )]
      [  XmlElement( ElementName = "Contrato_Identificacao_Z"   )]
      public String gxTpr_Contrato_identificacao_Z
      {
         get {
            return gxTv_SdtContrato_Contrato_identificacao_Z ;
         }

         set {
            gxTv_SdtContrato_Contrato_identificacao_Z = (String)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_identificacao_Z_SetNull( )
      {
         gxTv_SdtContrato_Contrato_identificacao_Z = "";
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_identificacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_Codigo_N" )]
      [  XmlElement( ElementName = "Contrato_Codigo_N"   )]
      public short gxTpr_Contrato_codigo_N
      {
         get {
            return gxTv_SdtContrato_Contrato_codigo_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_codigo_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_AreaTrabalhoDes_N" )]
      [  XmlElement( ElementName = "Contrato_AreaTrabalhoDes_N"   )]
      public short gxTpr_Contrato_areatrabalhodes_N
      {
         get {
            return gxTv_SdtContrato_Contrato_areatrabalhodes_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_areatrabalhodes_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_areatrabalhodes_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_areatrabalhodes_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_areatrabalhodes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaNom_N" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom_N"   )]
      public short gxTpr_Contratada_pessoanom_N
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoanom_N ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_pessoanom_N_SetNull( )
      {
         gxTv_SdtContrato_Contratada_pessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_pessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ_N" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ_N"   )]
      public short gxTpr_Contratada_pessoacnpj_N
      {
         get {
            return gxTv_SdtContrato_Contratada_pessoacnpj_N ;
         }

         set {
            gxTv_SdtContrato_Contratada_pessoacnpj_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contratada_pessoacnpj_N_SetNull( )
      {
         gxTv_SdtContrato_Contratada_pessoacnpj_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contratada_pessoacnpj_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_NumeroAta_N" )]
      [  XmlElement( ElementName = "Contrato_NumeroAta_N"   )]
      public short gxTpr_Contrato_numeroata_N
      {
         get {
            return gxTv_SdtContrato_Contrato_numeroata_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_numeroata_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_numeroata_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_numeroata_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_numeroata_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataInicioTA_N" )]
      [  XmlElement( ElementName = "Contrato_DataInicioTA_N"   )]
      public short gxTpr_Contrato_datainiciota_N
      {
         get {
            return gxTv_SdtContrato_Contrato_datainiciota_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_datainiciota_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datainiciota_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datainiciota_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datainiciota_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_DataFimTA_N" )]
      [  XmlElement( ElementName = "Contrato_DataFimTA_N"   )]
      public short gxTpr_Contrato_datafimta_N
      {
         get {
            return gxTv_SdtContrato_Contrato_datafimta_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_datafimta_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_datafimta_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_datafimta_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_datafimta_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_AceitaPFFS_N" )]
      [  XmlElement( ElementName = "Contrato_AceitaPFFS_N"   )]
      public short gxTpr_Contrato_aceitapffs_N
      {
         get {
            return gxTv_SdtContrato_Contrato_aceitapffs_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_aceitapffs_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_aceitapffs_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_aceitapffs_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_aceitapffs_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoCod_N" )]
      [  XmlElement( ElementName = "Contrato_PrepostoCod_N"   )]
      public short gxTpr_Contrato_prepostocod_N
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostocod_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostocod_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoPesCod_N" )]
      [  XmlElement( ElementName = "Contrato_PrepostoPesCod_N"   )]
      public short gxTpr_Contrato_prepostopescod_N
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostopescod_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostopescod_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostopescod_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostopescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostopescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrepostoNom_N" )]
      [  XmlElement( ElementName = "Contrato_PrepostoNom_N"   )]
      public short gxTpr_Contrato_prepostonom_N
      {
         get {
            return gxTv_SdtContrato_Contrato_prepostonom_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_prepostonom_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prepostonom_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prepostonom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prepostonom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrCada_N" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrCada_N"   )]
      public short gxTpr_Contrato_prdftrcada_N
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrcada_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrcada_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrcada_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrcada_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrcada_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_LmtFtr_N" )]
      [  XmlElement( ElementName = "Contrato_LmtFtr_N"   )]
      public short gxTpr_Contrato_lmtftr_N
      {
         get {
            return gxTv_SdtContrato_Contrato_lmtftr_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_lmtftr_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_lmtftr_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_lmtftr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_lmtftr_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrIni_N" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrIni_N"   )]
      public short gxTpr_Contrato_prdftrini_N
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrini_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrini_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrini_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrini_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrini_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contrato_PrdFtrFim_N" )]
      [  XmlElement( ElementName = "Contrato_PrdFtrFim_N"   )]
      public short gxTpr_Contrato_prdftrfim_N
      {
         get {
            return gxTv_SdtContrato_Contrato_prdftrfim_N ;
         }

         set {
            gxTv_SdtContrato_Contrato_prdftrfim_N = (short)(value);
         }

      }

      public void gxTv_SdtContrato_Contrato_prdftrfim_N_SetNull( )
      {
         gxTv_SdtContrato_Contrato_prdftrfim_N = 0;
         return  ;
      }

      public bool gxTv_SdtContrato_Contrato_prdftrfim_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContrato_Contrato_areatrabalhodes = "";
         gxTv_SdtContrato_Contratada_pessoanom = "";
         gxTv_SdtContrato_Contratada_pessoacnpj = "";
         gxTv_SdtContrato_Contratada_sigla = "";
         gxTv_SdtContrato_Contratada_tipofabrica = "S";
         gxTv_SdtContrato_Contrato_numero = "";
         gxTv_SdtContrato_Contrato_numeroata = "";
         gxTv_SdtContrato_Contrato_objeto = "";
         gxTv_SdtContrato_Contrato_datavigenciainicio = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datavigenciatermino = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datainiciota = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimta = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapublicacaodou = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datatermino = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataassinatura = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapedidoreajuste = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataterminoata = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimadaptacao = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_regraspagto = "";
         gxTv_SdtContrato_Contrato_calculodivergencia = "B";
         gxTv_SdtContrato_Contrato_prepostonom = "";
         gxTv_SdtContrato_Contrato_prdftrcada = "";
         gxTv_SdtContrato_Contrato_identificacao = "";
         gxTv_SdtContrato_Mode = "";
         gxTv_SdtContrato_Contrato_areatrabalhodes_Z = "";
         gxTv_SdtContrato_Contratada_pessoanom_Z = "";
         gxTv_SdtContrato_Contratada_pessoacnpj_Z = "";
         gxTv_SdtContrato_Contratada_sigla_Z = "";
         gxTv_SdtContrato_Contratada_tipofabrica_Z = "";
         gxTv_SdtContrato_Contrato_numero_Z = "";
         gxTv_SdtContrato_Contrato_numeroata_Z = "";
         gxTv_SdtContrato_Contrato_datavigenciainicio_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datavigenciatermino_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datainiciota_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimta_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapublicacaodou_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datatermino_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataassinatura_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datapedidoreajuste_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_dataterminoata_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_datafimadaptacao_Z = DateTime.MinValue;
         gxTv_SdtContrato_Contrato_calculodivergencia_Z = "";
         gxTv_SdtContrato_Contrato_prepostonom_Z = "";
         gxTv_SdtContrato_Contrato_prdftrcada_Z = "";
         gxTv_SdtContrato_Contrato_identificacao_Z = "";
         gxTv_SdtContrato_Contrato_diaspagto = 30;
         gxTv_SdtContrato_Contrato_indicedivergencia = (decimal)(10);
         gxTv_SdtContrato_Contrato_ativo = true;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contrato", "GeneXus.Programs.contrato_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContrato_Contrato_ano ;
      private short gxTv_SdtContrato_Contrato_unidadecontratacao ;
      private short gxTv_SdtContrato_Contrato_diaspagto ;
      private short gxTv_SdtContrato_Contrato_prdftrini ;
      private short gxTv_SdtContrato_Contrato_prdftrfim ;
      private short gxTv_SdtContrato_Initialized ;
      private short gxTv_SdtContrato_Contrato_ano_Z ;
      private short gxTv_SdtContrato_Contrato_unidadecontratacao_Z ;
      private short gxTv_SdtContrato_Contrato_diaspagto_Z ;
      private short gxTv_SdtContrato_Contrato_prdftrini_Z ;
      private short gxTv_SdtContrato_Contrato_prdftrfim_Z ;
      private short gxTv_SdtContrato_Contrato_codigo_N ;
      private short gxTv_SdtContrato_Contrato_areatrabalhodes_N ;
      private short gxTv_SdtContrato_Contratada_pessoanom_N ;
      private short gxTv_SdtContrato_Contratada_pessoacnpj_N ;
      private short gxTv_SdtContrato_Contrato_numeroata_N ;
      private short gxTv_SdtContrato_Contrato_datainiciota_N ;
      private short gxTv_SdtContrato_Contrato_datafimta_N ;
      private short gxTv_SdtContrato_Contrato_aceitapffs_N ;
      private short gxTv_SdtContrato_Contrato_prepostocod_N ;
      private short gxTv_SdtContrato_Contrato_prepostopescod_N ;
      private short gxTv_SdtContrato_Contrato_prepostonom_N ;
      private short gxTv_SdtContrato_Contrato_prdftrcada_N ;
      private short gxTv_SdtContrato_Contrato_lmtftr_N ;
      private short gxTv_SdtContrato_Contrato_prdftrini_N ;
      private short gxTv_SdtContrato_Contrato_prdftrfim_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContrato_Contrato_codigo ;
      private int gxTv_SdtContrato_Contrato_areatrabalhocod ;
      private int gxTv_SdtContrato_Contratada_codigo ;
      private int gxTv_SdtContrato_Contratada_pessoacod ;
      private int gxTv_SdtContrato_Contrato_quantidade ;
      private int gxTv_SdtContrato_Contrato_prepostocod ;
      private int gxTv_SdtContrato_Contrato_prepostopescod ;
      private int gxTv_SdtContrato_Contrato_codigo_Z ;
      private int gxTv_SdtContrato_Contrato_areatrabalhocod_Z ;
      private int gxTv_SdtContrato_Contratada_codigo_Z ;
      private int gxTv_SdtContrato_Contratada_pessoacod_Z ;
      private int gxTv_SdtContrato_Contrato_quantidade_Z ;
      private int gxTv_SdtContrato_Contrato_prepostocod_Z ;
      private int gxTv_SdtContrato_Contrato_prepostopescod_Z ;
      private decimal gxTv_SdtContrato_Contrato_valor ;
      private decimal gxTv_SdtContrato_Contrato_valorunidadecontratacao ;
      private decimal gxTv_SdtContrato_Contrato_valorundcntatual ;
      private decimal gxTv_SdtContrato_Contrato_indicedivergencia ;
      private decimal gxTv_SdtContrato_Contrato_lmtftr ;
      private decimal gxTv_SdtContrato_Contrato_valor_Z ;
      private decimal gxTv_SdtContrato_Contrato_valorunidadecontratacao_Z ;
      private decimal gxTv_SdtContrato_Contrato_valorundcntatual_Z ;
      private decimal gxTv_SdtContrato_Contrato_indicedivergencia_Z ;
      private decimal gxTv_SdtContrato_Contrato_lmtftr_Z ;
      private String gxTv_SdtContrato_Contratada_pessoanom ;
      private String gxTv_SdtContrato_Contratada_sigla ;
      private String gxTv_SdtContrato_Contratada_tipofabrica ;
      private String gxTv_SdtContrato_Contrato_numero ;
      private String gxTv_SdtContrato_Contrato_numeroata ;
      private String gxTv_SdtContrato_Contrato_calculodivergencia ;
      private String gxTv_SdtContrato_Contrato_prepostonom ;
      private String gxTv_SdtContrato_Contrato_prdftrcada ;
      private String gxTv_SdtContrato_Mode ;
      private String gxTv_SdtContrato_Contratada_pessoanom_Z ;
      private String gxTv_SdtContrato_Contratada_sigla_Z ;
      private String gxTv_SdtContrato_Contratada_tipofabrica_Z ;
      private String gxTv_SdtContrato_Contrato_numero_Z ;
      private String gxTv_SdtContrato_Contrato_numeroata_Z ;
      private String gxTv_SdtContrato_Contrato_calculodivergencia_Z ;
      private String gxTv_SdtContrato_Contrato_prepostonom_Z ;
      private String gxTv_SdtContrato_Contrato_prdftrcada_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContrato_Contrato_datavigenciainicio ;
      private DateTime gxTv_SdtContrato_Contrato_datavigenciatermino ;
      private DateTime gxTv_SdtContrato_Contrato_datainiciota ;
      private DateTime gxTv_SdtContrato_Contrato_datafimta ;
      private DateTime gxTv_SdtContrato_Contrato_datapublicacaodou ;
      private DateTime gxTv_SdtContrato_Contrato_datatermino ;
      private DateTime gxTv_SdtContrato_Contrato_dataassinatura ;
      private DateTime gxTv_SdtContrato_Contrato_datapedidoreajuste ;
      private DateTime gxTv_SdtContrato_Contrato_dataterminoata ;
      private DateTime gxTv_SdtContrato_Contrato_datafimadaptacao ;
      private DateTime gxTv_SdtContrato_Contrato_datavigenciainicio_Z ;
      private DateTime gxTv_SdtContrato_Contrato_datavigenciatermino_Z ;
      private DateTime gxTv_SdtContrato_Contrato_datainiciota_Z ;
      private DateTime gxTv_SdtContrato_Contrato_datafimta_Z ;
      private DateTime gxTv_SdtContrato_Contrato_datapublicacaodou_Z ;
      private DateTime gxTv_SdtContrato_Contrato_datatermino_Z ;
      private DateTime gxTv_SdtContrato_Contrato_dataassinatura_Z ;
      private DateTime gxTv_SdtContrato_Contrato_datapedidoreajuste_Z ;
      private DateTime gxTv_SdtContrato_Contrato_dataterminoata_Z ;
      private DateTime gxTv_SdtContrato_Contrato_datafimadaptacao_Z ;
      private bool gxTv_SdtContrato_Contrato_aceitapffs ;
      private bool gxTv_SdtContrato_Contrato_ativo ;
      private bool gxTv_SdtContrato_Contrato_aceitapffs_Z ;
      private bool gxTv_SdtContrato_Contrato_ativo_Z ;
      private String gxTv_SdtContrato_Contrato_objeto ;
      private String gxTv_SdtContrato_Contrato_regraspagto ;
      private String gxTv_SdtContrato_Contrato_areatrabalhodes ;
      private String gxTv_SdtContrato_Contratada_pessoacnpj ;
      private String gxTv_SdtContrato_Contrato_identificacao ;
      private String gxTv_SdtContrato_Contrato_areatrabalhodes_Z ;
      private String gxTv_SdtContrato_Contratada_pessoacnpj_Z ;
      private String gxTv_SdtContrato_Contrato_identificacao_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Contrato", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContrato_RESTInterface : GxGenericCollectionItem<SdtContrato>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContrato_RESTInterface( ) : base()
      {
      }

      public SdtContrato_RESTInterface( SdtContrato psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Contrato_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contrato_codigo
      {
         get {
            return sdt.gxTpr_Contrato_codigo ;
         }

         set {
            sdt.gxTpr_Contrato_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_AreaTrabalhoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contrato_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contrato_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contrato_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_AreaTrabalhoDes" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contrato_areatrabalhodes
      {
         get {
            return sdt.gxTpr_Contrato_areatrabalhodes ;
         }

         set {
            sdt.gxTpr_Contrato_areatrabalhodes = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_Codigo" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_codigo
      {
         get {
            return sdt.gxTpr_Contratada_codigo ;
         }

         set {
            sdt.gxTpr_Contratada_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_pessoacod
      {
         get {
            return sdt.gxTpr_Contratada_pessoacod ;
         }

         set {
            sdt.gxTpr_Contratada_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaNom" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_pessoanom) ;
         }

         set {
            sdt.gxTpr_Contratada_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_PessoaCNPJ" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Contratada_pessoacnpj
      {
         get {
            return sdt.gxTpr_Contratada_pessoacnpj ;
         }

         set {
            sdt.gxTpr_Contratada_pessoacnpj = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_Sigla" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Contratada_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_sigla) ;
         }

         set {
            sdt.gxTpr_Contratada_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_TipoFabrica" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contratada_tipofabrica
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_tipofabrica) ;
         }

         set {
            sdt.gxTpr_Contratada_tipofabrica = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_Numero" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Contrato_numero
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_numero) ;
         }

         set {
            sdt.gxTpr_Contrato_numero = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_NumeroAta" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Contrato_numeroata
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_numeroata) ;
         }

         set {
            sdt.gxTpr_Contrato_numeroata = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_Ano" , Order = 11 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contrato_ano
      {
         get {
            return sdt.gxTpr_Contrato_ano ;
         }

         set {
            sdt.gxTpr_Contrato_ano = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_Objeto" , Order = 12 )]
      public String gxTpr_Contrato_objeto
      {
         get {
            return sdt.gxTpr_Contrato_objeto ;
         }

         set {
            sdt.gxTpr_Contrato_objeto = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_UnidadeContratacao" , Order = 13 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contrato_unidadecontratacao
      {
         get {
            return sdt.gxTpr_Contrato_unidadecontratacao ;
         }

         set {
            sdt.gxTpr_Contrato_unidadecontratacao = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_Quantidade" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Contrato_quantidade
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contrato_quantidade), 9, 0)) ;
         }

         set {
            sdt.gxTpr_Contrato_quantidade = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Contrato_DataVigenciaInicio" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datavigenciainicio
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datavigenciainicio) ;
         }

         set {
            sdt.gxTpr_Contrato_datavigenciainicio = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataVigenciaTermino" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datavigenciatermino
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datavigenciatermino) ;
         }

         set {
            sdt.gxTpr_Contrato_datavigenciatermino = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataInicioTA" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datainiciota
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datainiciota) ;
         }

         set {
            sdt.gxTpr_Contrato_datainiciota = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataFimTA" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datafimta
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datafimta) ;
         }

         set {
            sdt.gxTpr_Contrato_datafimta = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataPublicacaoDOU" , Order = 19 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datapublicacaodou
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datapublicacaodou) ;
         }

         set {
            sdt.gxTpr_Contrato_datapublicacaodou = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataTermino" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datatermino
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datatermino) ;
         }

         set {
            sdt.gxTpr_Contrato_datatermino = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataAssinatura" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Contrato_dataassinatura
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_dataassinatura) ;
         }

         set {
            sdt.gxTpr_Contrato_dataassinatura = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataPedidoReajuste" , Order = 22 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datapedidoreajuste
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datapedidoreajuste) ;
         }

         set {
            sdt.gxTpr_Contrato_datapedidoreajuste = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataTerminoAta" , Order = 23 )]
      [GxSeudo()]
      public String gxTpr_Contrato_dataterminoata
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_dataterminoata) ;
         }

         set {
            sdt.gxTpr_Contrato_dataterminoata = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_DataFimAdaptacao" , Order = 24 )]
      [GxSeudo()]
      public String gxTpr_Contrato_datafimadaptacao
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contrato_datafimadaptacao) ;
         }

         set {
            sdt.gxTpr_Contrato_datafimadaptacao = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Contrato_Valor" , Order = 25 )]
      [GxSeudo()]
      public String gxTpr_Contrato_valor
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contrato_valor, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contrato_valor = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contrato_ValorUnidadeContratacao" , Order = 26 )]
      [GxSeudo()]
      public String gxTpr_Contrato_valorunidadecontratacao
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contrato_valorunidadecontratacao, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contrato_valorunidadecontratacao = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contrato_ValorUndCntAtual" , Order = 27 )]
      [GxSeudo()]
      public String gxTpr_Contrato_valorundcntatual
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contrato_valorundcntatual, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contrato_valorundcntatual = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contrato_RegrasPagto" , Order = 28 )]
      public String gxTpr_Contrato_regraspagto
      {
         get {
            return sdt.gxTpr_Contrato_regraspagto ;
         }

         set {
            sdt.gxTpr_Contrato_regraspagto = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_DiasPagto" , Order = 29 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contrato_diaspagto
      {
         get {
            return sdt.gxTpr_Contrato_diaspagto ;
         }

         set {
            sdt.gxTpr_Contrato_diaspagto = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_CalculoDivergencia" , Order = 30 )]
      [GxSeudo()]
      public String gxTpr_Contrato_calculodivergencia
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_calculodivergencia) ;
         }

         set {
            sdt.gxTpr_Contrato_calculodivergencia = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_IndiceDivergencia" , Order = 31 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Contrato_indicedivergencia
      {
         get {
            return sdt.gxTpr_Contrato_indicedivergencia ;
         }

         set {
            sdt.gxTpr_Contrato_indicedivergencia = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_AceitaPFFS" , Order = 32 )]
      [GxSeudo()]
      public bool gxTpr_Contrato_aceitapffs
      {
         get {
            return sdt.gxTpr_Contrato_aceitapffs ;
         }

         set {
            sdt.gxTpr_Contrato_aceitapffs = value;
         }

      }

      [DataMember( Name = "Contrato_Ativo" , Order = 33 )]
      [GxSeudo()]
      public bool gxTpr_Contrato_ativo
      {
         get {
            return sdt.gxTpr_Contrato_ativo ;
         }

         set {
            sdt.gxTpr_Contrato_ativo = value;
         }

      }

      [DataMember( Name = "Contrato_PrepostoCod" , Order = 34 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contrato_prepostocod
      {
         get {
            return sdt.gxTpr_Contrato_prepostocod ;
         }

         set {
            sdt.gxTpr_Contrato_prepostocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_PrepostoPesCod" , Order = 35 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contrato_prepostopescod
      {
         get {
            return sdt.gxTpr_Contrato_prepostopescod ;
         }

         set {
            sdt.gxTpr_Contrato_prepostopescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_PrepostoNom" , Order = 36 )]
      [GxSeudo()]
      public String gxTpr_Contrato_prepostonom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_prepostonom) ;
         }

         set {
            sdt.gxTpr_Contrato_prepostonom = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_PrdFtrCada" , Order = 37 )]
      [GxSeudo()]
      public String gxTpr_Contrato_prdftrcada
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_prdftrcada) ;
         }

         set {
            sdt.gxTpr_Contrato_prdftrcada = (String)(value);
         }

      }

      [DataMember( Name = "Contrato_LmtFtr" , Order = 38 )]
      [GxSeudo()]
      public String gxTpr_Contrato_lmtftr
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contrato_lmtftr, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contrato_lmtftr = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contrato_PrdFtrIni" , Order = 39 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contrato_prdftrini
      {
         get {
            return sdt.gxTpr_Contrato_prdftrini ;
         }

         set {
            sdt.gxTpr_Contrato_prdftrini = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_PrdFtrFim" , Order = 40 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contrato_prdftrfim
      {
         get {
            return sdt.gxTpr_Contrato_prdftrfim ;
         }

         set {
            sdt.gxTpr_Contrato_prdftrfim = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_Identificacao" , Order = 41 )]
      [GxSeudo()]
      public String gxTpr_Contrato_identificacao
      {
         get {
            return sdt.gxTpr_Contrato_identificacao ;
         }

         set {
            sdt.gxTpr_Contrato_identificacao = (String)(value);
         }

      }

      public SdtContrato sdt
      {
         get {
            return (SdtContrato)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContrato() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 99 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
