/*
               File: Tributo
        Description: Tributo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:14.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tributo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TRIBUTO_CONTRATADACOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATRIBUTO_CONTRATADACOD43182( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1654Tributo_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1654Tributo_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1654Tributo_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynTributo_ContratadaCod.Name = "TRIBUTO_CONTRATADACOD";
         dynTributo_ContratadaCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tributo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTributo_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tributo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tributo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynTributo_ContratadaCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynTributo_ContratadaCod.ItemCount > 0 )
         {
            A1654Tributo_ContratadaCod = (int)(NumberUtil.Val( dynTributo_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1654Tributo_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_43182( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_43182e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_43182( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_43182( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_43182e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Tributo", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Tributo.htm");
            wb_table3_28_43182( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_43182e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_43182e( true) ;
         }
         else
         {
            wb_table1_2_43182e( false) ;
         }
      }

      protected void wb_table3_28_43182( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_43182( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_43182e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tributo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tributo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_43182e( true) ;
         }
         else
         {
            wb_table3_28_43182e( false) ;
         }
      }

      protected void wb_table4_34_43182( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_codigo_Internalname, "Codigo", "", "", lblTextblocktributo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1653Tributo_Codigo), 6, 0, ",", "")), ((edtTributo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1653Tributo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1653Tributo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_contratadacod_Internalname, "Nome", "", "", lblTextblocktributo_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTributo_ContratadaCod, dynTributo_ContratadaCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0)), 1, dynTributo_ContratadaCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTributo_ContratadaCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_Tributo.htm");
            dynTributo_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTributo_ContratadaCod_Internalname, "Values", (String)(dynTributo_ContratadaCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_pis_Internalname, "PIS", "", "", lblTextblocktributo_pis_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_PIS_Internalname, StringUtil.LTrim( StringUtil.NToC( A1655Tributo_PIS, 6, 2, ",", "")), ((edtTributo_PIS_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1655Tributo_PIS, "ZZ9.99")) : context.localUtil.Format( A1655Tributo_PIS, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_PIS_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_PIS_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_cofins_Internalname, "Cofins", "", "", lblTextblocktributo_cofins_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_Cofins_Internalname, StringUtil.LTrim( StringUtil.NToC( A1656Tributo_Cofins, 6, 2, ",", "")), ((edtTributo_Cofins_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1656Tributo_Cofins, "ZZ9.99")) : context.localUtil.Format( A1656Tributo_Cofins, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_Cofins_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_Cofins_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_ir_Internalname, "IR", "", "", lblTextblocktributo_ir_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_IR_Internalname, StringUtil.LTrim( StringUtil.NToC( A1657Tributo_IR, 6, 2, ",", "")), ((edtTributo_IR_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1657Tributo_IR, "ZZ9.99")) : context.localUtil.Format( A1657Tributo_IR, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_IR_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_IR_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_inss_Internalname, "INSS", "", "", lblTextblocktributo_inss_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_INSS_Internalname, StringUtil.LTrim( StringUtil.NToC( A1658Tributo_INSS, 6, 2, ",", "")), ((edtTributo_INSS_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1658Tributo_INSS, "ZZ9.99")) : context.localUtil.Format( A1658Tributo_INSS, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_INSS_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_INSS_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_csll_Internalname, "CSLL", "", "", lblTextblocktributo_csll_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_CSLL_Internalname, StringUtil.LTrim( StringUtil.NToC( A1659Tributo_CSLL, 6, 2, ",", "")), ((edtTributo_CSLL_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1659Tributo_CSLL, "ZZ9.99")) : context.localUtil.Format( A1659Tributo_CSLL, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_CSLL_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_CSLL_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_iss_Internalname, "ISS", "", "", lblTextblocktributo_iss_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_ISS_Internalname, StringUtil.LTrim( StringUtil.NToC( A1660Tributo_ISS, 6, 2, ",", "")), ((edtTributo_ISS_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1660Tributo_ISS, "ZZ9.99")) : context.localUtil.Format( A1660Tributo_ISS, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_ISS_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_ISS_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktributo_issareter_Internalname, "a Reter", "", "", lblTextblocktributo_issareter_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTributo_ISSaReter_Internalname, StringUtil.LTrim( StringUtil.NToC( A1661Tributo_ISSaReter, 6, 2, ",", "")), ((edtTributo_ISSaReter_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1661Tributo_ISSaReter, "ZZ9.99")) : context.localUtil.Format( A1661Tributo_ISSaReter, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTributo_ISSaReter_Jsonclick, 0, "Attribute", "", "", "", 1, edtTributo_ISSaReter_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Tributo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_43182e( true) ;
         }
         else
         {
            wb_table4_34_43182e( false) ;
         }
      }

      protected void wb_table2_5_43182( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tributo.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_43182e( true) ;
         }
         else
         {
            wb_table2_5_43182e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1653Tributo_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
               }
               else
               {
                  A1653Tributo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTributo_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
               }
               dynTributo_ContratadaCod.CurrentValue = cgiGet( dynTributo_ContratadaCod_Internalname);
               A1654Tributo_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynTributo_ContratadaCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1654Tributo_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_PIS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_PIS_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_PIS");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_PIS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1655Tributo_PIS = 0;
                  n1655Tributo_PIS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1655Tributo_PIS", StringUtil.LTrim( StringUtil.Str( A1655Tributo_PIS, 6, 2)));
               }
               else
               {
                  A1655Tributo_PIS = context.localUtil.CToN( cgiGet( edtTributo_PIS_Internalname), ",", ".");
                  n1655Tributo_PIS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1655Tributo_PIS", StringUtil.LTrim( StringUtil.Str( A1655Tributo_PIS, 6, 2)));
               }
               n1655Tributo_PIS = ((Convert.ToDecimal(0)==A1655Tributo_PIS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_Cofins_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_Cofins_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_COFINS");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_Cofins_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1656Tributo_Cofins = 0;
                  n1656Tributo_Cofins = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1656Tributo_Cofins", StringUtil.LTrim( StringUtil.Str( A1656Tributo_Cofins, 6, 2)));
               }
               else
               {
                  A1656Tributo_Cofins = context.localUtil.CToN( cgiGet( edtTributo_Cofins_Internalname), ",", ".");
                  n1656Tributo_Cofins = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1656Tributo_Cofins", StringUtil.LTrim( StringUtil.Str( A1656Tributo_Cofins, 6, 2)));
               }
               n1656Tributo_Cofins = ((Convert.ToDecimal(0)==A1656Tributo_Cofins) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_IR_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_IR_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_IR");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_IR_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1657Tributo_IR = 0;
                  n1657Tributo_IR = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1657Tributo_IR", StringUtil.LTrim( StringUtil.Str( A1657Tributo_IR, 6, 2)));
               }
               else
               {
                  A1657Tributo_IR = context.localUtil.CToN( cgiGet( edtTributo_IR_Internalname), ",", ".");
                  n1657Tributo_IR = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1657Tributo_IR", StringUtil.LTrim( StringUtil.Str( A1657Tributo_IR, 6, 2)));
               }
               n1657Tributo_IR = ((Convert.ToDecimal(0)==A1657Tributo_IR) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_INSS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_INSS_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_INSS");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_INSS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1658Tributo_INSS = 0;
                  n1658Tributo_INSS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1658Tributo_INSS", StringUtil.LTrim( StringUtil.Str( A1658Tributo_INSS, 6, 2)));
               }
               else
               {
                  A1658Tributo_INSS = context.localUtil.CToN( cgiGet( edtTributo_INSS_Internalname), ",", ".");
                  n1658Tributo_INSS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1658Tributo_INSS", StringUtil.LTrim( StringUtil.Str( A1658Tributo_INSS, 6, 2)));
               }
               n1658Tributo_INSS = ((Convert.ToDecimal(0)==A1658Tributo_INSS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_CSLL_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_CSLL_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_CSLL");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_CSLL_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1659Tributo_CSLL = 0;
                  n1659Tributo_CSLL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1659Tributo_CSLL", StringUtil.LTrim( StringUtil.Str( A1659Tributo_CSLL, 6, 2)));
               }
               else
               {
                  A1659Tributo_CSLL = context.localUtil.CToN( cgiGet( edtTributo_CSLL_Internalname), ",", ".");
                  n1659Tributo_CSLL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1659Tributo_CSLL", StringUtil.LTrim( StringUtil.Str( A1659Tributo_CSLL, 6, 2)));
               }
               n1659Tributo_CSLL = ((Convert.ToDecimal(0)==A1659Tributo_CSLL) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_ISS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_ISS_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_ISS");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_ISS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1660Tributo_ISS = 0;
                  n1660Tributo_ISS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1660Tributo_ISS", StringUtil.LTrim( StringUtil.Str( A1660Tributo_ISS, 6, 2)));
               }
               else
               {
                  A1660Tributo_ISS = context.localUtil.CToN( cgiGet( edtTributo_ISS_Internalname), ",", ".");
                  n1660Tributo_ISS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1660Tributo_ISS", StringUtil.LTrim( StringUtil.Str( A1660Tributo_ISS, 6, 2)));
               }
               n1660Tributo_ISS = ((Convert.ToDecimal(0)==A1660Tributo_ISS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTributo_ISSaReter_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTributo_ISSaReter_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIBUTO_ISSARETER");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_ISSaReter_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1661Tributo_ISSaReter = 0;
                  n1661Tributo_ISSaReter = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1661Tributo_ISSaReter", StringUtil.LTrim( StringUtil.Str( A1661Tributo_ISSaReter, 6, 2)));
               }
               else
               {
                  A1661Tributo_ISSaReter = context.localUtil.CToN( cgiGet( edtTributo_ISSaReter_Internalname), ",", ".");
                  n1661Tributo_ISSaReter = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1661Tributo_ISSaReter", StringUtil.LTrim( StringUtil.Str( A1661Tributo_ISSaReter, 6, 2)));
               }
               n1661Tributo_ISSaReter = ((Convert.ToDecimal(0)==A1661Tributo_ISSaReter) ? true : false);
               /* Read saved values. */
               Z1653Tributo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1653Tributo_Codigo"), ",", "."));
               Z1655Tributo_PIS = context.localUtil.CToN( cgiGet( "Z1655Tributo_PIS"), ",", ".");
               n1655Tributo_PIS = ((Convert.ToDecimal(0)==A1655Tributo_PIS) ? true : false);
               Z1656Tributo_Cofins = context.localUtil.CToN( cgiGet( "Z1656Tributo_Cofins"), ",", ".");
               n1656Tributo_Cofins = ((Convert.ToDecimal(0)==A1656Tributo_Cofins) ? true : false);
               Z1657Tributo_IR = context.localUtil.CToN( cgiGet( "Z1657Tributo_IR"), ",", ".");
               n1657Tributo_IR = ((Convert.ToDecimal(0)==A1657Tributo_IR) ? true : false);
               Z1658Tributo_INSS = context.localUtil.CToN( cgiGet( "Z1658Tributo_INSS"), ",", ".");
               n1658Tributo_INSS = ((Convert.ToDecimal(0)==A1658Tributo_INSS) ? true : false);
               Z1659Tributo_CSLL = context.localUtil.CToN( cgiGet( "Z1659Tributo_CSLL"), ",", ".");
               n1659Tributo_CSLL = ((Convert.ToDecimal(0)==A1659Tributo_CSLL) ? true : false);
               Z1660Tributo_ISS = context.localUtil.CToN( cgiGet( "Z1660Tributo_ISS"), ",", ".");
               n1660Tributo_ISS = ((Convert.ToDecimal(0)==A1660Tributo_ISS) ? true : false);
               Z1661Tributo_ISSaReter = context.localUtil.CToN( cgiGet( "Z1661Tributo_ISSaReter"), ",", ".");
               n1661Tributo_ISSaReter = ((Convert.ToDecimal(0)==A1661Tributo_ISSaReter) ? true : false);
               Z1654Tributo_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "Z1654Tributo_ContratadaCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1653Tributo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll43182( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes43182( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption430( )
      {
      }

      protected void ZM43182( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1655Tributo_PIS = T00433_A1655Tributo_PIS[0];
               Z1656Tributo_Cofins = T00433_A1656Tributo_Cofins[0];
               Z1657Tributo_IR = T00433_A1657Tributo_IR[0];
               Z1658Tributo_INSS = T00433_A1658Tributo_INSS[0];
               Z1659Tributo_CSLL = T00433_A1659Tributo_CSLL[0];
               Z1660Tributo_ISS = T00433_A1660Tributo_ISS[0];
               Z1661Tributo_ISSaReter = T00433_A1661Tributo_ISSaReter[0];
               Z1654Tributo_ContratadaCod = T00433_A1654Tributo_ContratadaCod[0];
            }
            else
            {
               Z1655Tributo_PIS = A1655Tributo_PIS;
               Z1656Tributo_Cofins = A1656Tributo_Cofins;
               Z1657Tributo_IR = A1657Tributo_IR;
               Z1658Tributo_INSS = A1658Tributo_INSS;
               Z1659Tributo_CSLL = A1659Tributo_CSLL;
               Z1660Tributo_ISS = A1660Tributo_ISS;
               Z1661Tributo_ISSaReter = A1661Tributo_ISSaReter;
               Z1654Tributo_ContratadaCod = A1654Tributo_ContratadaCod;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1653Tributo_Codigo = A1653Tributo_Codigo;
            Z1655Tributo_PIS = A1655Tributo_PIS;
            Z1656Tributo_Cofins = A1656Tributo_Cofins;
            Z1657Tributo_IR = A1657Tributo_IR;
            Z1658Tributo_INSS = A1658Tributo_INSS;
            Z1659Tributo_CSLL = A1659Tributo_CSLL;
            Z1660Tributo_ISS = A1660Tributo_ISS;
            Z1661Tributo_ISSaReter = A1661Tributo_ISSaReter;
            Z1654Tributo_ContratadaCod = A1654Tributo_ContratadaCod;
         }
      }

      protected void standaloneNotModal( )
      {
         GXATRIBUTO_CONTRATADACOD_html43182( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load43182( )
      {
         /* Using cursor T00435 */
         pr_default.execute(3, new Object[] {A1653Tributo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound182 = 1;
            A1655Tributo_PIS = T00435_A1655Tributo_PIS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1655Tributo_PIS", StringUtil.LTrim( StringUtil.Str( A1655Tributo_PIS, 6, 2)));
            n1655Tributo_PIS = T00435_n1655Tributo_PIS[0];
            A1656Tributo_Cofins = T00435_A1656Tributo_Cofins[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1656Tributo_Cofins", StringUtil.LTrim( StringUtil.Str( A1656Tributo_Cofins, 6, 2)));
            n1656Tributo_Cofins = T00435_n1656Tributo_Cofins[0];
            A1657Tributo_IR = T00435_A1657Tributo_IR[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1657Tributo_IR", StringUtil.LTrim( StringUtil.Str( A1657Tributo_IR, 6, 2)));
            n1657Tributo_IR = T00435_n1657Tributo_IR[0];
            A1658Tributo_INSS = T00435_A1658Tributo_INSS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1658Tributo_INSS", StringUtil.LTrim( StringUtil.Str( A1658Tributo_INSS, 6, 2)));
            n1658Tributo_INSS = T00435_n1658Tributo_INSS[0];
            A1659Tributo_CSLL = T00435_A1659Tributo_CSLL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1659Tributo_CSLL", StringUtil.LTrim( StringUtil.Str( A1659Tributo_CSLL, 6, 2)));
            n1659Tributo_CSLL = T00435_n1659Tributo_CSLL[0];
            A1660Tributo_ISS = T00435_A1660Tributo_ISS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1660Tributo_ISS", StringUtil.LTrim( StringUtil.Str( A1660Tributo_ISS, 6, 2)));
            n1660Tributo_ISS = T00435_n1660Tributo_ISS[0];
            A1661Tributo_ISSaReter = T00435_A1661Tributo_ISSaReter[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1661Tributo_ISSaReter", StringUtil.LTrim( StringUtil.Str( A1661Tributo_ISSaReter, 6, 2)));
            n1661Tributo_ISSaReter = T00435_n1661Tributo_ISSaReter[0];
            A1654Tributo_ContratadaCod = T00435_A1654Tributo_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1654Tributo_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0)));
            ZM43182( -2) ;
         }
         pr_default.close(3);
         OnLoadActions43182( ) ;
      }

      protected void OnLoadActions43182( )
      {
      }

      protected void CheckExtendedTable43182( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00434 */
         pr_default.execute(2, new Object[] {A1654Tributo_ContratadaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tributo_Contratada'.", "ForeignKeyNotFound", 1, "TRIBUTO_CONTRATADACOD");
            AnyError = 1;
            GX_FocusControl = dynTributo_ContratadaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors43182( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A1654Tributo_ContratadaCod )
      {
         /* Using cursor T00436 */
         pr_default.execute(4, new Object[] {A1654Tributo_ContratadaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tributo_Contratada'.", "ForeignKeyNotFound", 1, "TRIBUTO_CONTRATADACOD");
            AnyError = 1;
            GX_FocusControl = dynTributo_ContratadaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey43182( )
      {
         /* Using cursor T00437 */
         pr_default.execute(5, new Object[] {A1653Tributo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound182 = 1;
         }
         else
         {
            RcdFound182 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00433 */
         pr_default.execute(1, new Object[] {A1653Tributo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM43182( 2) ;
            RcdFound182 = 1;
            A1653Tributo_Codigo = T00433_A1653Tributo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
            A1655Tributo_PIS = T00433_A1655Tributo_PIS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1655Tributo_PIS", StringUtil.LTrim( StringUtil.Str( A1655Tributo_PIS, 6, 2)));
            n1655Tributo_PIS = T00433_n1655Tributo_PIS[0];
            A1656Tributo_Cofins = T00433_A1656Tributo_Cofins[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1656Tributo_Cofins", StringUtil.LTrim( StringUtil.Str( A1656Tributo_Cofins, 6, 2)));
            n1656Tributo_Cofins = T00433_n1656Tributo_Cofins[0];
            A1657Tributo_IR = T00433_A1657Tributo_IR[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1657Tributo_IR", StringUtil.LTrim( StringUtil.Str( A1657Tributo_IR, 6, 2)));
            n1657Tributo_IR = T00433_n1657Tributo_IR[0];
            A1658Tributo_INSS = T00433_A1658Tributo_INSS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1658Tributo_INSS", StringUtil.LTrim( StringUtil.Str( A1658Tributo_INSS, 6, 2)));
            n1658Tributo_INSS = T00433_n1658Tributo_INSS[0];
            A1659Tributo_CSLL = T00433_A1659Tributo_CSLL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1659Tributo_CSLL", StringUtil.LTrim( StringUtil.Str( A1659Tributo_CSLL, 6, 2)));
            n1659Tributo_CSLL = T00433_n1659Tributo_CSLL[0];
            A1660Tributo_ISS = T00433_A1660Tributo_ISS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1660Tributo_ISS", StringUtil.LTrim( StringUtil.Str( A1660Tributo_ISS, 6, 2)));
            n1660Tributo_ISS = T00433_n1660Tributo_ISS[0];
            A1661Tributo_ISSaReter = T00433_A1661Tributo_ISSaReter[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1661Tributo_ISSaReter", StringUtil.LTrim( StringUtil.Str( A1661Tributo_ISSaReter, 6, 2)));
            n1661Tributo_ISSaReter = T00433_n1661Tributo_ISSaReter[0];
            A1654Tributo_ContratadaCod = T00433_A1654Tributo_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1654Tributo_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0)));
            Z1653Tributo_Codigo = A1653Tributo_Codigo;
            sMode182 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load43182( ) ;
            if ( AnyError == 1 )
            {
               RcdFound182 = 0;
               InitializeNonKey43182( ) ;
            }
            Gx_mode = sMode182;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound182 = 0;
            InitializeNonKey43182( ) ;
            sMode182 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode182;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey43182( ) ;
         if ( RcdFound182 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound182 = 0;
         /* Using cursor T00438 */
         pr_default.execute(6, new Object[] {A1653Tributo_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00438_A1653Tributo_Codigo[0] < A1653Tributo_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00438_A1653Tributo_Codigo[0] > A1653Tributo_Codigo ) ) )
            {
               A1653Tributo_Codigo = T00438_A1653Tributo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
               RcdFound182 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound182 = 0;
         /* Using cursor T00439 */
         pr_default.execute(7, new Object[] {A1653Tributo_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00439_A1653Tributo_Codigo[0] > A1653Tributo_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00439_A1653Tributo_Codigo[0] < A1653Tributo_Codigo ) ) )
            {
               A1653Tributo_Codigo = T00439_A1653Tributo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
               RcdFound182 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey43182( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTributo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert43182( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound182 == 1 )
            {
               if ( A1653Tributo_Codigo != Z1653Tributo_Codigo )
               {
                  A1653Tributo_Codigo = Z1653Tributo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TRIBUTO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTributo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTributo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update43182( ) ;
                  GX_FocusControl = edtTributo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1653Tributo_Codigo != Z1653Tributo_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtTributo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert43182( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TRIBUTO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTributo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtTributo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert43182( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1653Tributo_Codigo != Z1653Tributo_Codigo )
         {
            A1653Tributo_Codigo = Z1653Tributo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TRIBUTO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTributo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTributo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound182 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "TRIBUTO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTributo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = dynTributo_ContratadaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart43182( ) ;
         if ( RcdFound182 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynTributo_ContratadaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd43182( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound182 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynTributo_ContratadaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound182 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynTributo_ContratadaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart43182( ) ;
         if ( RcdFound182 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound182 != 0 )
            {
               ScanNext43182( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = dynTributo_ContratadaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd43182( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency43182( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00432 */
            pr_default.execute(0, new Object[] {A1653Tributo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tributo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1655Tributo_PIS != T00432_A1655Tributo_PIS[0] ) || ( Z1656Tributo_Cofins != T00432_A1656Tributo_Cofins[0] ) || ( Z1657Tributo_IR != T00432_A1657Tributo_IR[0] ) || ( Z1658Tributo_INSS != T00432_A1658Tributo_INSS[0] ) || ( Z1659Tributo_CSLL != T00432_A1659Tributo_CSLL[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1660Tributo_ISS != T00432_A1660Tributo_ISS[0] ) || ( Z1661Tributo_ISSaReter != T00432_A1661Tributo_ISSaReter[0] ) || ( Z1654Tributo_ContratadaCod != T00432_A1654Tributo_ContratadaCod[0] ) )
            {
               if ( Z1655Tributo_PIS != T00432_A1655Tributo_PIS[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_PIS");
                  GXUtil.WriteLogRaw("Old: ",Z1655Tributo_PIS);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1655Tributo_PIS[0]);
               }
               if ( Z1656Tributo_Cofins != T00432_A1656Tributo_Cofins[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_Cofins");
                  GXUtil.WriteLogRaw("Old: ",Z1656Tributo_Cofins);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1656Tributo_Cofins[0]);
               }
               if ( Z1657Tributo_IR != T00432_A1657Tributo_IR[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_IR");
                  GXUtil.WriteLogRaw("Old: ",Z1657Tributo_IR);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1657Tributo_IR[0]);
               }
               if ( Z1658Tributo_INSS != T00432_A1658Tributo_INSS[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_INSS");
                  GXUtil.WriteLogRaw("Old: ",Z1658Tributo_INSS);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1658Tributo_INSS[0]);
               }
               if ( Z1659Tributo_CSLL != T00432_A1659Tributo_CSLL[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_CSLL");
                  GXUtil.WriteLogRaw("Old: ",Z1659Tributo_CSLL);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1659Tributo_CSLL[0]);
               }
               if ( Z1660Tributo_ISS != T00432_A1660Tributo_ISS[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_ISS");
                  GXUtil.WriteLogRaw("Old: ",Z1660Tributo_ISS);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1660Tributo_ISS[0]);
               }
               if ( Z1661Tributo_ISSaReter != T00432_A1661Tributo_ISSaReter[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_ISSaReter");
                  GXUtil.WriteLogRaw("Old: ",Z1661Tributo_ISSaReter);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1661Tributo_ISSaReter[0]);
               }
               if ( Z1654Tributo_ContratadaCod != T00432_A1654Tributo_ContratadaCod[0] )
               {
                  GXUtil.WriteLog("tributo:[seudo value changed for attri]"+"Tributo_ContratadaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1654Tributo_ContratadaCod);
                  GXUtil.WriteLogRaw("Current: ",T00432_A1654Tributo_ContratadaCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Tributo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert43182( )
      {
         BeforeValidate43182( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable43182( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM43182( 0) ;
            CheckOptimisticConcurrency43182( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm43182( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert43182( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004310 */
                     pr_default.execute(8, new Object[] {n1655Tributo_PIS, A1655Tributo_PIS, n1656Tributo_Cofins, A1656Tributo_Cofins, n1657Tributo_IR, A1657Tributo_IR, n1658Tributo_INSS, A1658Tributo_INSS, n1659Tributo_CSLL, A1659Tributo_CSLL, n1660Tributo_ISS, A1660Tributo_ISS, n1661Tributo_ISSaReter, A1661Tributo_ISSaReter, A1654Tributo_ContratadaCod});
                     A1653Tributo_Codigo = T004310_A1653Tributo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Tributo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption430( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load43182( ) ;
            }
            EndLevel43182( ) ;
         }
         CloseExtendedTableCursors43182( ) ;
      }

      protected void Update43182( )
      {
         BeforeValidate43182( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable43182( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency43182( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm43182( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate43182( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004311 */
                     pr_default.execute(9, new Object[] {n1655Tributo_PIS, A1655Tributo_PIS, n1656Tributo_Cofins, A1656Tributo_Cofins, n1657Tributo_IR, A1657Tributo_IR, n1658Tributo_INSS, A1658Tributo_INSS, n1659Tributo_CSLL, A1659Tributo_CSLL, n1660Tributo_ISS, A1660Tributo_ISS, n1661Tributo_ISSaReter, A1661Tributo_ISSaReter, A1654Tributo_ContratadaCod, A1653Tributo_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Tributo") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tributo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate43182( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption430( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel43182( ) ;
         }
         CloseExtendedTableCursors43182( ) ;
      }

      protected void DeferredUpdate43182( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate43182( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency43182( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls43182( ) ;
            AfterConfirm43182( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete43182( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004312 */
                  pr_default.execute(10, new Object[] {A1653Tributo_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Tributo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound182 == 0 )
                        {
                           InitAll43182( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption430( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode182 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel43182( ) ;
         Gx_mode = sMode182;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls43182( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel43182( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete43182( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Tributo");
            if ( AnyError == 0 )
            {
               ConfirmValues430( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Tributo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart43182( )
      {
         /* Using cursor T004313 */
         pr_default.execute(11);
         RcdFound182 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound182 = 1;
            A1653Tributo_Codigo = T004313_A1653Tributo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext43182( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound182 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound182 = 1;
            A1653Tributo_Codigo = T004313_A1653Tributo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd43182( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm43182( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert43182( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate43182( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete43182( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete43182( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate43182( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes43182( )
      {
         edtTributo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_Codigo_Enabled), 5, 0)));
         dynTributo_ContratadaCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTributo_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTributo_ContratadaCod.Enabled), 5, 0)));
         edtTributo_PIS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_PIS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_PIS_Enabled), 5, 0)));
         edtTributo_Cofins_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_Cofins_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_Cofins_Enabled), 5, 0)));
         edtTributo_IR_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_IR_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_IR_Enabled), 5, 0)));
         edtTributo_INSS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_INSS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_INSS_Enabled), 5, 0)));
         edtTributo_CSLL_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_CSLL_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_CSLL_Enabled), 5, 0)));
         edtTributo_ISS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_ISS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_ISS_Enabled), 5, 0)));
         edtTributo_ISSaReter_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTributo_ISSaReter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTributo_ISSaReter_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues430( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117291562");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tributo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1653Tributo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1653Tributo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1655Tributo_PIS", StringUtil.LTrim( StringUtil.NToC( Z1655Tributo_PIS, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1656Tributo_Cofins", StringUtil.LTrim( StringUtil.NToC( Z1656Tributo_Cofins, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1657Tributo_IR", StringUtil.LTrim( StringUtil.NToC( Z1657Tributo_IR, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1658Tributo_INSS", StringUtil.LTrim( StringUtil.NToC( Z1658Tributo_INSS, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1659Tributo_CSLL", StringUtil.LTrim( StringUtil.NToC( Z1659Tributo_CSLL, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1660Tributo_ISS", StringUtil.LTrim( StringUtil.NToC( Z1660Tributo_ISS, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1661Tributo_ISSaReter", StringUtil.LTrim( StringUtil.NToC( Z1661Tributo_ISSaReter, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1654Tributo_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1654Tributo_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tributo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Tributo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tributo" ;
      }

      protected void InitializeNonKey43182( )
      {
         A1654Tributo_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1654Tributo_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0)));
         A1655Tributo_PIS = 0;
         n1655Tributo_PIS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1655Tributo_PIS", StringUtil.LTrim( StringUtil.Str( A1655Tributo_PIS, 6, 2)));
         n1655Tributo_PIS = ((Convert.ToDecimal(0)==A1655Tributo_PIS) ? true : false);
         A1656Tributo_Cofins = 0;
         n1656Tributo_Cofins = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1656Tributo_Cofins", StringUtil.LTrim( StringUtil.Str( A1656Tributo_Cofins, 6, 2)));
         n1656Tributo_Cofins = ((Convert.ToDecimal(0)==A1656Tributo_Cofins) ? true : false);
         A1657Tributo_IR = 0;
         n1657Tributo_IR = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1657Tributo_IR", StringUtil.LTrim( StringUtil.Str( A1657Tributo_IR, 6, 2)));
         n1657Tributo_IR = ((Convert.ToDecimal(0)==A1657Tributo_IR) ? true : false);
         A1658Tributo_INSS = 0;
         n1658Tributo_INSS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1658Tributo_INSS", StringUtil.LTrim( StringUtil.Str( A1658Tributo_INSS, 6, 2)));
         n1658Tributo_INSS = ((Convert.ToDecimal(0)==A1658Tributo_INSS) ? true : false);
         A1659Tributo_CSLL = 0;
         n1659Tributo_CSLL = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1659Tributo_CSLL", StringUtil.LTrim( StringUtil.Str( A1659Tributo_CSLL, 6, 2)));
         n1659Tributo_CSLL = ((Convert.ToDecimal(0)==A1659Tributo_CSLL) ? true : false);
         A1660Tributo_ISS = 0;
         n1660Tributo_ISS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1660Tributo_ISS", StringUtil.LTrim( StringUtil.Str( A1660Tributo_ISS, 6, 2)));
         n1660Tributo_ISS = ((Convert.ToDecimal(0)==A1660Tributo_ISS) ? true : false);
         A1661Tributo_ISSaReter = 0;
         n1661Tributo_ISSaReter = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1661Tributo_ISSaReter", StringUtil.LTrim( StringUtil.Str( A1661Tributo_ISSaReter, 6, 2)));
         n1661Tributo_ISSaReter = ((Convert.ToDecimal(0)==A1661Tributo_ISSaReter) ? true : false);
         Z1655Tributo_PIS = 0;
         Z1656Tributo_Cofins = 0;
         Z1657Tributo_IR = 0;
         Z1658Tributo_INSS = 0;
         Z1659Tributo_CSLL = 0;
         Z1660Tributo_ISS = 0;
         Z1661Tributo_ISSaReter = 0;
         Z1654Tributo_ContratadaCod = 0;
      }

      protected void InitAll43182( )
      {
         A1653Tributo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1653Tributo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1653Tributo_Codigo), 6, 0)));
         InitializeNonKey43182( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117291568");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tributo.js", "?20203117291568");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocktributo_codigo_Internalname = "TEXTBLOCKTRIBUTO_CODIGO";
         edtTributo_Codigo_Internalname = "TRIBUTO_CODIGO";
         lblTextblocktributo_contratadacod_Internalname = "TEXTBLOCKTRIBUTO_CONTRATADACOD";
         dynTributo_ContratadaCod_Internalname = "TRIBUTO_CONTRATADACOD";
         lblTextblocktributo_pis_Internalname = "TEXTBLOCKTRIBUTO_PIS";
         edtTributo_PIS_Internalname = "TRIBUTO_PIS";
         lblTextblocktributo_cofins_Internalname = "TEXTBLOCKTRIBUTO_COFINS";
         edtTributo_Cofins_Internalname = "TRIBUTO_COFINS";
         lblTextblocktributo_ir_Internalname = "TEXTBLOCKTRIBUTO_IR";
         edtTributo_IR_Internalname = "TRIBUTO_IR";
         lblTextblocktributo_inss_Internalname = "TEXTBLOCKTRIBUTO_INSS";
         edtTributo_INSS_Internalname = "TRIBUTO_INSS";
         lblTextblocktributo_csll_Internalname = "TEXTBLOCKTRIBUTO_CSLL";
         edtTributo_CSLL_Internalname = "TRIBUTO_CSLL";
         lblTextblocktributo_iss_Internalname = "TEXTBLOCKTRIBUTO_ISS";
         edtTributo_ISS_Internalname = "TRIBUTO_ISS";
         lblTextblocktributo_issareter_Internalname = "TEXTBLOCKTRIBUTO_ISSARETER";
         edtTributo_ISSaReter_Internalname = "TRIBUTO_ISSARETER";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tributo";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtTributo_ISSaReter_Jsonclick = "";
         edtTributo_ISSaReter_Enabled = 1;
         edtTributo_ISS_Jsonclick = "";
         edtTributo_ISS_Enabled = 1;
         edtTributo_CSLL_Jsonclick = "";
         edtTributo_CSLL_Enabled = 1;
         edtTributo_INSS_Jsonclick = "";
         edtTributo_INSS_Enabled = 1;
         edtTributo_IR_Jsonclick = "";
         edtTributo_IR_Enabled = 1;
         edtTributo_Cofins_Jsonclick = "";
         edtTributo_Cofins_Enabled = 1;
         edtTributo_PIS_Jsonclick = "";
         edtTributo_PIS_Enabled = 1;
         dynTributo_ContratadaCod_Jsonclick = "";
         dynTributo_ContratadaCod.Enabled = 1;
         edtTributo_Codigo_Jsonclick = "";
         edtTributo_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATRIBUTO_CONTRATADACOD43182( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATRIBUTO_CONTRATADACOD_data43182( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATRIBUTO_CONTRATADACOD_html43182( )
      {
         int gxdynajaxvalue ;
         GXDLATRIBUTO_CONTRATADACOD_data43182( ) ;
         gxdynajaxindex = 1;
         dynTributo_ContratadaCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTributo_ContratadaCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATRIBUTO_CONTRATADACOD_data43182( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T004314 */
         pr_default.execute(12);
         while ( (pr_default.getStatus(12) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004314_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004314_A41Contratada_PessoaNom[0]));
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = dynTributo_ContratadaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Tributo_codigo( int GX_Parm1 ,
                                        decimal GX_Parm2 ,
                                        decimal GX_Parm3 ,
                                        decimal GX_Parm4 ,
                                        decimal GX_Parm5 ,
                                        decimal GX_Parm6 ,
                                        decimal GX_Parm7 ,
                                        decimal GX_Parm8 ,
                                        GXCombobox dynGX_Parm9 )
      {
         A1653Tributo_Codigo = GX_Parm1;
         A1655Tributo_PIS = GX_Parm2;
         n1655Tributo_PIS = false;
         A1656Tributo_Cofins = GX_Parm3;
         n1656Tributo_Cofins = false;
         A1657Tributo_IR = GX_Parm4;
         n1657Tributo_IR = false;
         A1658Tributo_INSS = GX_Parm5;
         n1658Tributo_INSS = false;
         A1659Tributo_CSLL = GX_Parm6;
         n1659Tributo_CSLL = false;
         A1660Tributo_ISS = GX_Parm7;
         n1660Tributo_ISS = false;
         A1661Tributo_ISSaReter = GX_Parm8;
         n1661Tributo_ISSaReter = false;
         dynTributo_ContratadaCod = dynGX_Parm9;
         A1654Tributo_ContratadaCod = (int)(NumberUtil.Val( dynTributo_ContratadaCod.CurrentValue, "."));
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         GXATRIBUTO_CONTRATADACOD_html43182( ) ;
         dynTributo_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0));
         if ( dynTributo_ContratadaCod.ItemCount > 0 )
         {
            A1654Tributo_ContratadaCod = (int)(NumberUtil.Val( dynTributo_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0))), "."));
         }
         dynTributo_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1654Tributo_ContratadaCod), 6, 0));
         isValidOutput.Add(dynTributo_ContratadaCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1655Tributo_PIS, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1656Tributo_Cofins, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1657Tributo_IR, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1658Tributo_INSS, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1659Tributo_CSLL, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1660Tributo_ISS, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1661Tributo_ISSaReter, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1653Tributo_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1654Tributo_ContratadaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1655Tributo_PIS, 6, 2, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1656Tributo_Cofins, 6, 2, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1657Tributo_IR, 6, 2, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1658Tributo_INSS, 6, 2, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1659Tributo_CSLL, 6, 2, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1660Tributo_ISS, 6, 2, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1661Tributo_ISSaReter, 6, 2, ",", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tributo_contratadacod( GXCombobox dynGX_Parm1 )
      {
         dynTributo_ContratadaCod = dynGX_Parm1;
         A1654Tributo_ContratadaCod = (int)(NumberUtil.Val( dynTributo_ContratadaCod.CurrentValue, "."));
         /* Using cursor T004315 */
         pr_default.execute(13, new Object[] {A1654Tributo_ContratadaCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tributo_Contratada'.", "ForeignKeyNotFound", 1, "TRIBUTO_CONTRATADACOD");
            AnyError = 1;
            GX_FocusControl = dynTributo_ContratadaCod_Internalname;
         }
         pr_default.close(13);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocktributo_codigo_Jsonclick = "";
         lblTextblocktributo_contratadacod_Jsonclick = "";
         lblTextblocktributo_pis_Jsonclick = "";
         lblTextblocktributo_cofins_Jsonclick = "";
         lblTextblocktributo_ir_Jsonclick = "";
         lblTextblocktributo_inss_Jsonclick = "";
         lblTextblocktributo_csll_Jsonclick = "";
         lblTextblocktributo_iss_Jsonclick = "";
         lblTextblocktributo_issareter_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T00435_A1653Tributo_Codigo = new int[1] ;
         T00435_A1655Tributo_PIS = new decimal[1] ;
         T00435_n1655Tributo_PIS = new bool[] {false} ;
         T00435_A1656Tributo_Cofins = new decimal[1] ;
         T00435_n1656Tributo_Cofins = new bool[] {false} ;
         T00435_A1657Tributo_IR = new decimal[1] ;
         T00435_n1657Tributo_IR = new bool[] {false} ;
         T00435_A1658Tributo_INSS = new decimal[1] ;
         T00435_n1658Tributo_INSS = new bool[] {false} ;
         T00435_A1659Tributo_CSLL = new decimal[1] ;
         T00435_n1659Tributo_CSLL = new bool[] {false} ;
         T00435_A1660Tributo_ISS = new decimal[1] ;
         T00435_n1660Tributo_ISS = new bool[] {false} ;
         T00435_A1661Tributo_ISSaReter = new decimal[1] ;
         T00435_n1661Tributo_ISSaReter = new bool[] {false} ;
         T00435_A1654Tributo_ContratadaCod = new int[1] ;
         T00434_A1654Tributo_ContratadaCod = new int[1] ;
         T00436_A1654Tributo_ContratadaCod = new int[1] ;
         T00437_A1653Tributo_Codigo = new int[1] ;
         T00433_A1653Tributo_Codigo = new int[1] ;
         T00433_A1655Tributo_PIS = new decimal[1] ;
         T00433_n1655Tributo_PIS = new bool[] {false} ;
         T00433_A1656Tributo_Cofins = new decimal[1] ;
         T00433_n1656Tributo_Cofins = new bool[] {false} ;
         T00433_A1657Tributo_IR = new decimal[1] ;
         T00433_n1657Tributo_IR = new bool[] {false} ;
         T00433_A1658Tributo_INSS = new decimal[1] ;
         T00433_n1658Tributo_INSS = new bool[] {false} ;
         T00433_A1659Tributo_CSLL = new decimal[1] ;
         T00433_n1659Tributo_CSLL = new bool[] {false} ;
         T00433_A1660Tributo_ISS = new decimal[1] ;
         T00433_n1660Tributo_ISS = new bool[] {false} ;
         T00433_A1661Tributo_ISSaReter = new decimal[1] ;
         T00433_n1661Tributo_ISSaReter = new bool[] {false} ;
         T00433_A1654Tributo_ContratadaCod = new int[1] ;
         sMode182 = "";
         T00438_A1653Tributo_Codigo = new int[1] ;
         T00439_A1653Tributo_Codigo = new int[1] ;
         T00432_A1653Tributo_Codigo = new int[1] ;
         T00432_A1655Tributo_PIS = new decimal[1] ;
         T00432_n1655Tributo_PIS = new bool[] {false} ;
         T00432_A1656Tributo_Cofins = new decimal[1] ;
         T00432_n1656Tributo_Cofins = new bool[] {false} ;
         T00432_A1657Tributo_IR = new decimal[1] ;
         T00432_n1657Tributo_IR = new bool[] {false} ;
         T00432_A1658Tributo_INSS = new decimal[1] ;
         T00432_n1658Tributo_INSS = new bool[] {false} ;
         T00432_A1659Tributo_CSLL = new decimal[1] ;
         T00432_n1659Tributo_CSLL = new bool[] {false} ;
         T00432_A1660Tributo_ISS = new decimal[1] ;
         T00432_n1660Tributo_ISS = new bool[] {false} ;
         T00432_A1661Tributo_ISSaReter = new decimal[1] ;
         T00432_n1661Tributo_ISSaReter = new bool[] {false} ;
         T00432_A1654Tributo_ContratadaCod = new int[1] ;
         T004310_A1653Tributo_Codigo = new int[1] ;
         T004313_A1653Tributo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004314_A40Contratada_PessoaCod = new int[1] ;
         T004314_A39Contratada_Codigo = new int[1] ;
         T004314_A41Contratada_PessoaNom = new String[] {""} ;
         T004314_n41Contratada_PessoaNom = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T004315_A1654Tributo_ContratadaCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tributo__default(),
            new Object[][] {
                new Object[] {
               T00432_A1653Tributo_Codigo, T00432_A1655Tributo_PIS, T00432_n1655Tributo_PIS, T00432_A1656Tributo_Cofins, T00432_n1656Tributo_Cofins, T00432_A1657Tributo_IR, T00432_n1657Tributo_IR, T00432_A1658Tributo_INSS, T00432_n1658Tributo_INSS, T00432_A1659Tributo_CSLL,
               T00432_n1659Tributo_CSLL, T00432_A1660Tributo_ISS, T00432_n1660Tributo_ISS, T00432_A1661Tributo_ISSaReter, T00432_n1661Tributo_ISSaReter, T00432_A1654Tributo_ContratadaCod
               }
               , new Object[] {
               T00433_A1653Tributo_Codigo, T00433_A1655Tributo_PIS, T00433_n1655Tributo_PIS, T00433_A1656Tributo_Cofins, T00433_n1656Tributo_Cofins, T00433_A1657Tributo_IR, T00433_n1657Tributo_IR, T00433_A1658Tributo_INSS, T00433_n1658Tributo_INSS, T00433_A1659Tributo_CSLL,
               T00433_n1659Tributo_CSLL, T00433_A1660Tributo_ISS, T00433_n1660Tributo_ISS, T00433_A1661Tributo_ISSaReter, T00433_n1661Tributo_ISSaReter, T00433_A1654Tributo_ContratadaCod
               }
               , new Object[] {
               T00434_A1654Tributo_ContratadaCod
               }
               , new Object[] {
               T00435_A1653Tributo_Codigo, T00435_A1655Tributo_PIS, T00435_n1655Tributo_PIS, T00435_A1656Tributo_Cofins, T00435_n1656Tributo_Cofins, T00435_A1657Tributo_IR, T00435_n1657Tributo_IR, T00435_A1658Tributo_INSS, T00435_n1658Tributo_INSS, T00435_A1659Tributo_CSLL,
               T00435_n1659Tributo_CSLL, T00435_A1660Tributo_ISS, T00435_n1660Tributo_ISS, T00435_A1661Tributo_ISSaReter, T00435_n1661Tributo_ISSaReter, T00435_A1654Tributo_ContratadaCod
               }
               , new Object[] {
               T00436_A1654Tributo_ContratadaCod
               }
               , new Object[] {
               T00437_A1653Tributo_Codigo
               }
               , new Object[] {
               T00438_A1653Tributo_Codigo
               }
               , new Object[] {
               T00439_A1653Tributo_Codigo
               }
               , new Object[] {
               T004310_A1653Tributo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004313_A1653Tributo_Codigo
               }
               , new Object[] {
               T004314_A40Contratada_PessoaCod, T004314_A39Contratada_Codigo, T004314_A41Contratada_PessoaNom, T004314_n41Contratada_PessoaNom
               }
               , new Object[] {
               T004315_A1654Tributo_ContratadaCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound182 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1653Tributo_Codigo ;
      private int Z1654Tributo_ContratadaCod ;
      private int A1654Tributo_ContratadaCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1653Tributo_Codigo ;
      private int edtTributo_Codigo_Enabled ;
      private int edtTributo_PIS_Enabled ;
      private int edtTributo_Cofins_Enabled ;
      private int edtTributo_IR_Enabled ;
      private int edtTributo_INSS_Enabled ;
      private int edtTributo_CSLL_Enabled ;
      private int edtTributo_ISS_Enabled ;
      private int edtTributo_ISSaReter_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1655Tributo_PIS ;
      private decimal Z1656Tributo_Cofins ;
      private decimal Z1657Tributo_IR ;
      private decimal Z1658Tributo_INSS ;
      private decimal Z1659Tributo_CSLL ;
      private decimal Z1660Tributo_ISS ;
      private decimal Z1661Tributo_ISSaReter ;
      private decimal A1655Tributo_PIS ;
      private decimal A1656Tributo_Cofins ;
      private decimal A1657Tributo_IR ;
      private decimal A1658Tributo_INSS ;
      private decimal A1659Tributo_CSLL ;
      private decimal A1660Tributo_ISS ;
      private decimal A1661Tributo_ISSaReter ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTributo_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocktributo_codigo_Internalname ;
      private String lblTextblocktributo_codigo_Jsonclick ;
      private String edtTributo_Codigo_Jsonclick ;
      private String lblTextblocktributo_contratadacod_Internalname ;
      private String lblTextblocktributo_contratadacod_Jsonclick ;
      private String dynTributo_ContratadaCod_Internalname ;
      private String dynTributo_ContratadaCod_Jsonclick ;
      private String lblTextblocktributo_pis_Internalname ;
      private String lblTextblocktributo_pis_Jsonclick ;
      private String edtTributo_PIS_Internalname ;
      private String edtTributo_PIS_Jsonclick ;
      private String lblTextblocktributo_cofins_Internalname ;
      private String lblTextblocktributo_cofins_Jsonclick ;
      private String edtTributo_Cofins_Internalname ;
      private String edtTributo_Cofins_Jsonclick ;
      private String lblTextblocktributo_ir_Internalname ;
      private String lblTextblocktributo_ir_Jsonclick ;
      private String edtTributo_IR_Internalname ;
      private String edtTributo_IR_Jsonclick ;
      private String lblTextblocktributo_inss_Internalname ;
      private String lblTextblocktributo_inss_Jsonclick ;
      private String edtTributo_INSS_Internalname ;
      private String edtTributo_INSS_Jsonclick ;
      private String lblTextblocktributo_csll_Internalname ;
      private String lblTextblocktributo_csll_Jsonclick ;
      private String edtTributo_CSLL_Internalname ;
      private String edtTributo_CSLL_Jsonclick ;
      private String lblTextblocktributo_iss_Internalname ;
      private String lblTextblocktributo_iss_Jsonclick ;
      private String edtTributo_ISS_Internalname ;
      private String edtTributo_ISS_Jsonclick ;
      private String lblTextblocktributo_issareter_Internalname ;
      private String lblTextblocktributo_issareter_Jsonclick ;
      private String edtTributo_ISSaReter_Internalname ;
      private String edtTributo_ISSaReter_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode182 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1655Tributo_PIS ;
      private bool n1656Tributo_Cofins ;
      private bool n1657Tributo_IR ;
      private bool n1658Tributo_INSS ;
      private bool n1659Tributo_CSLL ;
      private bool n1660Tributo_ISS ;
      private bool n1661Tributo_ISSaReter ;
      private bool Gx_longc ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynTributo_ContratadaCod ;
      private IDataStoreProvider pr_default ;
      private int[] T00435_A1653Tributo_Codigo ;
      private decimal[] T00435_A1655Tributo_PIS ;
      private bool[] T00435_n1655Tributo_PIS ;
      private decimal[] T00435_A1656Tributo_Cofins ;
      private bool[] T00435_n1656Tributo_Cofins ;
      private decimal[] T00435_A1657Tributo_IR ;
      private bool[] T00435_n1657Tributo_IR ;
      private decimal[] T00435_A1658Tributo_INSS ;
      private bool[] T00435_n1658Tributo_INSS ;
      private decimal[] T00435_A1659Tributo_CSLL ;
      private bool[] T00435_n1659Tributo_CSLL ;
      private decimal[] T00435_A1660Tributo_ISS ;
      private bool[] T00435_n1660Tributo_ISS ;
      private decimal[] T00435_A1661Tributo_ISSaReter ;
      private bool[] T00435_n1661Tributo_ISSaReter ;
      private int[] T00435_A1654Tributo_ContratadaCod ;
      private int[] T00434_A1654Tributo_ContratadaCod ;
      private int[] T00436_A1654Tributo_ContratadaCod ;
      private int[] T00437_A1653Tributo_Codigo ;
      private int[] T00433_A1653Tributo_Codigo ;
      private decimal[] T00433_A1655Tributo_PIS ;
      private bool[] T00433_n1655Tributo_PIS ;
      private decimal[] T00433_A1656Tributo_Cofins ;
      private bool[] T00433_n1656Tributo_Cofins ;
      private decimal[] T00433_A1657Tributo_IR ;
      private bool[] T00433_n1657Tributo_IR ;
      private decimal[] T00433_A1658Tributo_INSS ;
      private bool[] T00433_n1658Tributo_INSS ;
      private decimal[] T00433_A1659Tributo_CSLL ;
      private bool[] T00433_n1659Tributo_CSLL ;
      private decimal[] T00433_A1660Tributo_ISS ;
      private bool[] T00433_n1660Tributo_ISS ;
      private decimal[] T00433_A1661Tributo_ISSaReter ;
      private bool[] T00433_n1661Tributo_ISSaReter ;
      private int[] T00433_A1654Tributo_ContratadaCod ;
      private int[] T00438_A1653Tributo_Codigo ;
      private int[] T00439_A1653Tributo_Codigo ;
      private int[] T00432_A1653Tributo_Codigo ;
      private decimal[] T00432_A1655Tributo_PIS ;
      private bool[] T00432_n1655Tributo_PIS ;
      private decimal[] T00432_A1656Tributo_Cofins ;
      private bool[] T00432_n1656Tributo_Cofins ;
      private decimal[] T00432_A1657Tributo_IR ;
      private bool[] T00432_n1657Tributo_IR ;
      private decimal[] T00432_A1658Tributo_INSS ;
      private bool[] T00432_n1658Tributo_INSS ;
      private decimal[] T00432_A1659Tributo_CSLL ;
      private bool[] T00432_n1659Tributo_CSLL ;
      private decimal[] T00432_A1660Tributo_ISS ;
      private bool[] T00432_n1660Tributo_ISS ;
      private decimal[] T00432_A1661Tributo_ISSaReter ;
      private bool[] T00432_n1661Tributo_ISSaReter ;
      private int[] T00432_A1654Tributo_ContratadaCod ;
      private int[] T004310_A1653Tributo_Codigo ;
      private int[] T004313_A1653Tributo_Codigo ;
      private int[] T004314_A40Contratada_PessoaCod ;
      private int[] T004314_A39Contratada_Codigo ;
      private String[] T004314_A41Contratada_PessoaNom ;
      private bool[] T004314_n41Contratada_PessoaNom ;
      private int[] T004315_A1654Tributo_ContratadaCod ;
      private GXWebForm Form ;
   }

   public class tributo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00435 ;
          prmT00435 = new Object[] {
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00434 ;
          prmT00434 = new Object[] {
          new Object[] {"@Tributo_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00436 ;
          prmT00436 = new Object[] {
          new Object[] {"@Tributo_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00437 ;
          prmT00437 = new Object[] {
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00433 ;
          prmT00433 = new Object[] {
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00438 ;
          prmT00438 = new Object[] {
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00439 ;
          prmT00439 = new Object[] {
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00432 ;
          prmT00432 = new Object[] {
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004310 ;
          prmT004310 = new Object[] {
          new Object[] {"@Tributo_PIS",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_Cofins",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_IR",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_INSS",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_CSLL",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_ISS",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_ISSaReter",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004311 ;
          prmT004311 = new Object[] {
          new Object[] {"@Tributo_PIS",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_Cofins",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_IR",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_INSS",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_CSLL",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_ISS",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_ISSaReter",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Tributo_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004312 ;
          prmT004312 = new Object[] {
          new Object[] {"@Tributo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004313 ;
          prmT004313 = new Object[] {
          } ;
          Object[] prmT004314 ;
          prmT004314 = new Object[] {
          } ;
          Object[] prmT004315 ;
          prmT004315 = new Object[] {
          new Object[] {"@Tributo_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00432", "SELECT [Tributo_Codigo], [Tributo_PIS], [Tributo_Cofins], [Tributo_IR], [Tributo_INSS], [Tributo_CSLL], [Tributo_ISS], [Tributo_ISSaReter], [Tributo_ContratadaCod] AS Tributo_ContratadaCod FROM [Tributo] WITH (UPDLOCK) WHERE [Tributo_Codigo] = @Tributo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00432,1,0,true,false )
             ,new CursorDef("T00433", "SELECT [Tributo_Codigo], [Tributo_PIS], [Tributo_Cofins], [Tributo_IR], [Tributo_INSS], [Tributo_CSLL], [Tributo_ISS], [Tributo_ISSaReter], [Tributo_ContratadaCod] AS Tributo_ContratadaCod FROM [Tributo] WITH (NOLOCK) WHERE [Tributo_Codigo] = @Tributo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00433,1,0,true,false )
             ,new CursorDef("T00434", "SELECT [Contratada_Codigo] AS Tributo_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Tributo_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00434,1,0,true,false )
             ,new CursorDef("T00435", "SELECT TM1.[Tributo_Codigo], TM1.[Tributo_PIS], TM1.[Tributo_Cofins], TM1.[Tributo_IR], TM1.[Tributo_INSS], TM1.[Tributo_CSLL], TM1.[Tributo_ISS], TM1.[Tributo_ISSaReter], TM1.[Tributo_ContratadaCod] AS Tributo_ContratadaCod FROM [Tributo] TM1 WITH (NOLOCK) WHERE TM1.[Tributo_Codigo] = @Tributo_Codigo ORDER BY TM1.[Tributo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00435,100,0,true,false )
             ,new CursorDef("T00436", "SELECT [Contratada_Codigo] AS Tributo_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Tributo_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00436,1,0,true,false )
             ,new CursorDef("T00437", "SELECT [Tributo_Codigo] FROM [Tributo] WITH (NOLOCK) WHERE [Tributo_Codigo] = @Tributo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00437,1,0,true,false )
             ,new CursorDef("T00438", "SELECT TOP 1 [Tributo_Codigo] FROM [Tributo] WITH (NOLOCK) WHERE ( [Tributo_Codigo] > @Tributo_Codigo) ORDER BY [Tributo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00438,1,0,true,true )
             ,new CursorDef("T00439", "SELECT TOP 1 [Tributo_Codigo] FROM [Tributo] WITH (NOLOCK) WHERE ( [Tributo_Codigo] < @Tributo_Codigo) ORDER BY [Tributo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00439,1,0,true,true )
             ,new CursorDef("T004310", "INSERT INTO [Tributo]([Tributo_PIS], [Tributo_Cofins], [Tributo_IR], [Tributo_INSS], [Tributo_CSLL], [Tributo_ISS], [Tributo_ISSaReter], [Tributo_ContratadaCod]) VALUES(@Tributo_PIS, @Tributo_Cofins, @Tributo_IR, @Tributo_INSS, @Tributo_CSLL, @Tributo_ISS, @Tributo_ISSaReter, @Tributo_ContratadaCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004310)
             ,new CursorDef("T004311", "UPDATE [Tributo] SET [Tributo_PIS]=@Tributo_PIS, [Tributo_Cofins]=@Tributo_Cofins, [Tributo_IR]=@Tributo_IR, [Tributo_INSS]=@Tributo_INSS, [Tributo_CSLL]=@Tributo_CSLL, [Tributo_ISS]=@Tributo_ISS, [Tributo_ISSaReter]=@Tributo_ISSaReter, [Tributo_ContratadaCod]=@Tributo_ContratadaCod  WHERE [Tributo_Codigo] = @Tributo_Codigo", GxErrorMask.GX_NOMASK,prmT004311)
             ,new CursorDef("T004312", "DELETE FROM [Tributo]  WHERE [Tributo_Codigo] = @Tributo_Codigo", GxErrorMask.GX_NOMASK,prmT004312)
             ,new CursorDef("T004313", "SELECT [Tributo_Codigo] FROM [Tributo] WITH (NOLOCK) ORDER BY [Tributo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004313,100,0,true,false )
             ,new CursorDef("T004314", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004314,0,0,true,false )
             ,new CursorDef("T004315", "SELECT [Contratada_Codigo] AS Tributo_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Tributo_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004315,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                stmt.SetParameter(9, (int)parms[15]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
