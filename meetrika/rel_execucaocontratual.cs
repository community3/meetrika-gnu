/*
               File: REL_ExecucaoContratual
        Description: Stub for REL_ExecucaoContratual
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:13.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_execucaocontratual : GXProcedure
   {
      public rel_execucaocontratual( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_execucaocontratual( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Codigo ,
                           DateTime aP3_Vigencia ,
                           DateTime aP4_Vigencia_To ,
                           String aP5_Tipo )
      {
         this.AV2AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV4Codigo = aP2_Codigo;
         this.AV5Vigencia = aP3_Vigencia;
         this.AV6Vigencia_To = aP4_Vigencia_To;
         this.AV7Tipo = aP5_Tipo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Codigo ,
                                 DateTime aP3_Vigencia ,
                                 DateTime aP4_Vigencia_To ,
                                 String aP5_Tipo )
      {
         rel_execucaocontratual objrel_execucaocontratual;
         objrel_execucaocontratual = new rel_execucaocontratual();
         objrel_execucaocontratual.AV2AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objrel_execucaocontratual.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         objrel_execucaocontratual.AV4Codigo = aP2_Codigo;
         objrel_execucaocontratual.AV5Vigencia = aP3_Vigencia;
         objrel_execucaocontratual.AV6Vigencia_To = aP4_Vigencia_To;
         objrel_execucaocontratual.AV7Tipo = aP5_Tipo;
         objrel_execucaocontratual.context.SetSubmitInitialConfig(context);
         objrel_execucaocontratual.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_execucaocontratual);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_execucaocontratual)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2AreaTrabalho_Codigo,(int)AV3Contratada_Codigo,(int)AV4Codigo,(DateTime)AV5Vigencia,(DateTime)AV6Vigencia_To,(String)AV7Tipo} ;
         ClassLoader.Execute("arel_execucaocontratual","GeneXus.Programs.arel_execucaocontratual", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 6 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2AreaTrabalho_Codigo ;
      private int AV3Contratada_Codigo ;
      private int AV4Codigo ;
      private String AV7Tipo ;
      private DateTime AV5Vigencia ;
      private DateTime AV6Vigencia_To ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
