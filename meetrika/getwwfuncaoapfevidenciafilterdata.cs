/*
               File: GetWWFuncaoAPFEvidenciaFilterData
        Description: Get WWFuncao APFEvidencia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:3.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwfuncaoapfevidenciafilterdata : GXProcedure
   {
      public getwwfuncaoapfevidenciafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwfuncaoapfevidenciafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwfuncaoapfevidenciafilterdata objgetwwfuncaoapfevidenciafilterdata;
         objgetwwfuncaoapfevidenciafilterdata = new getwwfuncaoapfevidenciafilterdata();
         objgetwwfuncaoapfevidenciafilterdata.AV20DDOName = aP0_DDOName;
         objgetwwfuncaoapfevidenciafilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwfuncaoapfevidenciafilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwfuncaoapfevidenciafilterdata.AV24OptionsJson = "" ;
         objgetwwfuncaoapfevidenciafilterdata.AV27OptionsDescJson = "" ;
         objgetwwfuncaoapfevidenciafilterdata.AV29OptionIndexesJson = "" ;
         objgetwwfuncaoapfevidenciafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwfuncaoapfevidenciafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwfuncaoapfevidenciafilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwfuncaoapfevidenciafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFEVIDENCIA_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFEVIDENCIA_NOMEARQOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFEVIDENCIA_TIPOARQOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWFuncaoAPFEvidenciaGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWFuncaoAPFEvidenciaGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWFuncaoAPFEvidenciaGridState"), "");
         }
         AV49GXV1 = 1;
         while ( AV49GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV49GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
            {
               AV10TFFuncaoAPFEvidencia_Descricao = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL") == 0 )
            {
               AV11TFFuncaoAPFEvidencia_Descricao_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
            {
               AV12TFFuncaoAPFEvidencia_NomeArq = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV13TFFuncaoAPFEvidencia_NomeArq_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ") == 0 )
            {
               AV14TFFuncaoAPFEvidencia_TipoArq = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL") == 0 )
            {
               AV15TFFuncaoAPFEvidencia_TipoArq_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DATA") == 0 )
            {
               AV16TFFuncaoAPFEvidencia_Data = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV17TFFuncaoAPFEvidencia_Data_To = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV49GXV1 = (int)(AV49GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
            {
               AV37DynamicFiltersOperator1 = AV35GridStateDynamicFilter.gxTpr_Operator;
               AV38FuncaoAPFEvidencia_Descricao1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
               {
                  AV41DynamicFiltersOperator2 = AV35GridStateDynamicFilter.gxTpr_Operator;
                  AV42FuncaoAPFEvidencia_Descricao2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
                  {
                     AV45DynamicFiltersOperator3 = AV35GridStateDynamicFilter.gxTpr_Operator;
                     AV46FuncaoAPFEvidencia_Descricao3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPFEVIDENCIA_DESCRICAOOPTIONS' Routine */
         AV10TFFuncaoAPFEvidencia_Descricao = AV18SearchTxt;
         AV11TFFuncaoAPFEvidencia_Descricao_Sel = "";
         AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV38FuncaoAPFEvidencia_Descricao1;
         AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV41DynamicFiltersOperator2;
         AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV42FuncaoAPFEvidencia_Descricao2;
         AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV45DynamicFiltersOperator3;
         AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV46FuncaoAPFEvidencia_Descricao3;
         AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV10TFFuncaoAPFEvidencia_Descricao;
         AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV11TFFuncaoAPFEvidencia_Descricao_Sel;
         AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV12TFFuncaoAPFEvidencia_NomeArq;
         AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV13TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV14TFFuncaoAPFEvidencia_TipoArq;
         AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV15TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV16TFFuncaoAPFEvidencia_Data;
         AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV17TFFuncaoAPFEvidencia_Data_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                              AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                              AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                              AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                              AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                              AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                              AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                              AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                              AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                              AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                              AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                              AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                              AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                              AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                              AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                              AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                              AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                              AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              A411FuncaoAPFEvidencia_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao), "%", "");
         lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = StringUtil.PadR( StringUtil.RTrim( AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq), 50, "%");
         lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq), 10, "%");
         /* Using cursor P00M12 */
         pr_default.execute(0, new Object[] {lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao, AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel, lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq, AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel, lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq, AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel, AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data, AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKM12 = false;
            A407FuncaoAPFEvidencia_Descricao = P00M12_A407FuncaoAPFEvidencia_Descricao[0];
            n407FuncaoAPFEvidencia_Descricao = P00M12_n407FuncaoAPFEvidencia_Descricao[0];
            A411FuncaoAPFEvidencia_Data = P00M12_A411FuncaoAPFEvidencia_Data[0];
            A410FuncaoAPFEvidencia_TipoArq = P00M12_A410FuncaoAPFEvidencia_TipoArq[0];
            n410FuncaoAPFEvidencia_TipoArq = P00M12_n410FuncaoAPFEvidencia_TipoArq[0];
            A409FuncaoAPFEvidencia_NomeArq = P00M12_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P00M12_n409FuncaoAPFEvidencia_NomeArq[0];
            A406FuncaoAPFEvidencia_Codigo = P00M12_A406FuncaoAPFEvidencia_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00M12_A407FuncaoAPFEvidencia_Descricao[0], A407FuncaoAPFEvidencia_Descricao) == 0 ) )
            {
               BRKM12 = false;
               A406FuncaoAPFEvidencia_Codigo = P00M12_A406FuncaoAPFEvidencia_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKM12 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A407FuncaoAPFEvidencia_Descricao)) )
            {
               AV22Option = A407FuncaoAPFEvidencia_Descricao;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKM12 )
            {
               BRKM12 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAOAPFEVIDENCIA_NOMEARQOPTIONS' Routine */
         AV12TFFuncaoAPFEvidencia_NomeArq = AV18SearchTxt;
         AV13TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV38FuncaoAPFEvidencia_Descricao1;
         AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV41DynamicFiltersOperator2;
         AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV42FuncaoAPFEvidencia_Descricao2;
         AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV45DynamicFiltersOperator3;
         AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV46FuncaoAPFEvidencia_Descricao3;
         AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV10TFFuncaoAPFEvidencia_Descricao;
         AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV11TFFuncaoAPFEvidencia_Descricao_Sel;
         AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV12TFFuncaoAPFEvidencia_NomeArq;
         AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV13TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV14TFFuncaoAPFEvidencia_TipoArq;
         AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV15TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV16TFFuncaoAPFEvidencia_Data;
         AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV17TFFuncaoAPFEvidencia_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                              AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                              AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                              AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                              AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                              AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                              AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                              AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                              AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                              AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                              AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                              AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                              AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                              AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                              AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                              AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                              AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                              AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              A411FuncaoAPFEvidencia_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao), "%", "");
         lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = StringUtil.PadR( StringUtil.RTrim( AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq), 50, "%");
         lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq), 10, "%");
         /* Using cursor P00M13 */
         pr_default.execute(1, new Object[] {lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao, AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel, lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq, AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel, lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq, AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel, AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data, AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKM14 = false;
            A409FuncaoAPFEvidencia_NomeArq = P00M13_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P00M13_n409FuncaoAPFEvidencia_NomeArq[0];
            A411FuncaoAPFEvidencia_Data = P00M13_A411FuncaoAPFEvidencia_Data[0];
            A410FuncaoAPFEvidencia_TipoArq = P00M13_A410FuncaoAPFEvidencia_TipoArq[0];
            n410FuncaoAPFEvidencia_TipoArq = P00M13_n410FuncaoAPFEvidencia_TipoArq[0];
            A407FuncaoAPFEvidencia_Descricao = P00M13_A407FuncaoAPFEvidencia_Descricao[0];
            n407FuncaoAPFEvidencia_Descricao = P00M13_n407FuncaoAPFEvidencia_Descricao[0];
            A406FuncaoAPFEvidencia_Codigo = P00M13_A406FuncaoAPFEvidencia_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00M13_A409FuncaoAPFEvidencia_NomeArq[0], A409FuncaoAPFEvidencia_NomeArq) == 0 ) )
            {
               BRKM14 = false;
               A406FuncaoAPFEvidencia_Codigo = P00M13_A406FuncaoAPFEvidencia_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKM14 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq)) )
            {
               AV22Option = A409FuncaoAPFEvidencia_NomeArq;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKM14 )
            {
               BRKM14 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADFUNCAOAPFEVIDENCIA_TIPOARQOPTIONS' Routine */
         AV14TFFuncaoAPFEvidencia_TipoArq = AV18SearchTxt;
         AV15TFFuncaoAPFEvidencia_TipoArq_Sel = "";
         AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV38FuncaoAPFEvidencia_Descricao1;
         AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV41DynamicFiltersOperator2;
         AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV42FuncaoAPFEvidencia_Descricao2;
         AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV45DynamicFiltersOperator3;
         AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV46FuncaoAPFEvidencia_Descricao3;
         AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV10TFFuncaoAPFEvidencia_Descricao;
         AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV11TFFuncaoAPFEvidencia_Descricao_Sel;
         AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV12TFFuncaoAPFEvidencia_NomeArq;
         AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV13TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV14TFFuncaoAPFEvidencia_TipoArq;
         AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV15TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV16TFFuncaoAPFEvidencia_Data;
         AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV17TFFuncaoAPFEvidencia_Data_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                              AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                              AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                              AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                              AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                              AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                              AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                              AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                              AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                              AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                              AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                              AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                              AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                              AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                              AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                              AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                              AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                              AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              A411FuncaoAPFEvidencia_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao), "%", "");
         lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = StringUtil.PadR( StringUtil.RTrim( AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq), 50, "%");
         lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq), 10, "%");
         /* Using cursor P00M14 */
         pr_default.execute(2, new Object[] {lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao, AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel, lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq, AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel, lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq, AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel, AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data, AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKM16 = false;
            A410FuncaoAPFEvidencia_TipoArq = P00M14_A410FuncaoAPFEvidencia_TipoArq[0];
            n410FuncaoAPFEvidencia_TipoArq = P00M14_n410FuncaoAPFEvidencia_TipoArq[0];
            A411FuncaoAPFEvidencia_Data = P00M14_A411FuncaoAPFEvidencia_Data[0];
            A409FuncaoAPFEvidencia_NomeArq = P00M14_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P00M14_n409FuncaoAPFEvidencia_NomeArq[0];
            A407FuncaoAPFEvidencia_Descricao = P00M14_A407FuncaoAPFEvidencia_Descricao[0];
            n407FuncaoAPFEvidencia_Descricao = P00M14_n407FuncaoAPFEvidencia_Descricao[0];
            A406FuncaoAPFEvidencia_Codigo = P00M14_A406FuncaoAPFEvidencia_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00M14_A410FuncaoAPFEvidencia_TipoArq[0], A410FuncaoAPFEvidencia_TipoArq) == 0 ) )
            {
               BRKM16 = false;
               A406FuncaoAPFEvidencia_Codigo = P00M14_A406FuncaoAPFEvidencia_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKM16 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq)) )
            {
               AV22Option = A410FuncaoAPFEvidencia_TipoArq;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKM16 )
            {
               BRKM16 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoAPFEvidencia_Descricao = "";
         AV11TFFuncaoAPFEvidencia_Descricao_Sel = "";
         AV12TFFuncaoAPFEvidencia_NomeArq = "";
         AV13TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         AV14TFFuncaoAPFEvidencia_TipoArq = "";
         AV15TFFuncaoAPFEvidencia_TipoArq_Sel = "";
         AV16TFFuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         AV17TFFuncaoAPFEvidencia_Data_To = (DateTime)(DateTime.MinValue);
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV38FuncaoAPFEvidencia_Descricao1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV42FuncaoAPFEvidencia_Descricao2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV46FuncaoAPFEvidencia_Descricao3 = "";
         AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = "";
         AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = "";
         AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = "";
         AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = "";
         AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = "";
         AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = "";
         AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = "";
         AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = "";
         AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = "";
         AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = "";
         AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = "";
         AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = "";
         AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = (DateTime)(DateTime.MinValue);
         AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = "";
         lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = "";
         lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = "";
         lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = "";
         lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = "";
         lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = "";
         A407FuncaoAPFEvidencia_Descricao = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         A410FuncaoAPFEvidencia_TipoArq = "";
         A411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         P00M12_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         P00M12_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         P00M12_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00M12_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         P00M12_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         P00M12_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P00M12_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P00M12_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         AV22Option = "";
         P00M13_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P00M13_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P00M13_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00M13_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         P00M13_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         P00M13_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         P00M13_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         P00M13_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         P00M14_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         P00M14_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         P00M14_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00M14_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P00M14_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P00M14_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         P00M14_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         P00M14_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwfuncaoapfevidenciafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00M12_A407FuncaoAPFEvidencia_Descricao, P00M12_n407FuncaoAPFEvidencia_Descricao, P00M12_A411FuncaoAPFEvidencia_Data, P00M12_A410FuncaoAPFEvidencia_TipoArq, P00M12_n410FuncaoAPFEvidencia_TipoArq, P00M12_A409FuncaoAPFEvidencia_NomeArq, P00M12_n409FuncaoAPFEvidencia_NomeArq, P00M12_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               P00M13_A409FuncaoAPFEvidencia_NomeArq, P00M13_n409FuncaoAPFEvidencia_NomeArq, P00M13_A411FuncaoAPFEvidencia_Data, P00M13_A410FuncaoAPFEvidencia_TipoArq, P00M13_n410FuncaoAPFEvidencia_TipoArq, P00M13_A407FuncaoAPFEvidencia_Descricao, P00M13_n407FuncaoAPFEvidencia_Descricao, P00M13_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               P00M14_A410FuncaoAPFEvidencia_TipoArq, P00M14_n410FuncaoAPFEvidencia_TipoArq, P00M14_A411FuncaoAPFEvidencia_Data, P00M14_A409FuncaoAPFEvidencia_NomeArq, P00M14_n409FuncaoAPFEvidencia_NomeArq, P00M14_A407FuncaoAPFEvidencia_Descricao, P00M14_n407FuncaoAPFEvidencia_Descricao, P00M14_A406FuncaoAPFEvidencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV37DynamicFiltersOperator1 ;
      private short AV41DynamicFiltersOperator2 ;
      private short AV45DynamicFiltersOperator3 ;
      private short AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ;
      private short AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ;
      private short AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ;
      private int AV49GXV1 ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private long AV30count ;
      private String AV12TFFuncaoAPFEvidencia_NomeArq ;
      private String AV13TFFuncaoAPFEvidencia_NomeArq_Sel ;
      private String AV14TFFuncaoAPFEvidencia_TipoArq ;
      private String AV15TFFuncaoAPFEvidencia_TipoArq_Sel ;
      private String AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ;
      private String AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ;
      private String AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ;
      private String AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ;
      private String scmdbuf ;
      private String lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ;
      private String lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String A410FuncaoAPFEvidencia_TipoArq ;
      private DateTime AV16TFFuncaoAPFEvidencia_Data ;
      private DateTime AV17TFFuncaoAPFEvidencia_Data_To ;
      private DateTime AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ;
      private DateTime AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ;
      private DateTime A411FuncaoAPFEvidencia_Data ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ;
      private bool AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ;
      private bool BRKM12 ;
      private bool n407FuncaoAPFEvidencia_Descricao ;
      private bool n410FuncaoAPFEvidencia_TipoArq ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool BRKM14 ;
      private bool BRKM16 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV38FuncaoAPFEvidencia_Descricao1 ;
      private String AV42FuncaoAPFEvidencia_Descricao2 ;
      private String AV46FuncaoAPFEvidencia_Descricao3 ;
      private String AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ;
      private String AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ;
      private String AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ;
      private String lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ;
      private String lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ;
      private String lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ;
      private String A407FuncaoAPFEvidencia_Descricao ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV10TFFuncaoAPFEvidencia_Descricao ;
      private String AV11TFFuncaoAPFEvidencia_Descricao_Sel ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ;
      private String AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ;
      private String AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ;
      private String AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ;
      private String AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ;
      private String lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00M12_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] P00M12_n407FuncaoAPFEvidencia_Descricao ;
      private DateTime[] P00M12_A411FuncaoAPFEvidencia_Data ;
      private String[] P00M12_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] P00M12_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] P00M12_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P00M12_n409FuncaoAPFEvidencia_NomeArq ;
      private int[] P00M12_A406FuncaoAPFEvidencia_Codigo ;
      private String[] P00M13_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P00M13_n409FuncaoAPFEvidencia_NomeArq ;
      private DateTime[] P00M13_A411FuncaoAPFEvidencia_Data ;
      private String[] P00M13_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] P00M13_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] P00M13_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] P00M13_n407FuncaoAPFEvidencia_Descricao ;
      private int[] P00M13_A406FuncaoAPFEvidencia_Codigo ;
      private String[] P00M14_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] P00M14_n410FuncaoAPFEvidencia_TipoArq ;
      private DateTime[] P00M14_A411FuncaoAPFEvidencia_Data ;
      private String[] P00M14_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P00M14_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] P00M14_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] P00M14_n407FuncaoAPFEvidencia_Descricao ;
      private int[] P00M14_A406FuncaoAPFEvidencia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwfuncaoapfevidenciafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00M12( IGxContext context ,
                                             String AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                             bool AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                             short AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                             String AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                             bool AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                             String AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                             short AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                             String AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                             String AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                             String AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                             String AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                             String AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                             String AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                             String AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                             DateTime AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                             DateTime AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             DateTime A411FuncaoAPFEvidencia_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [14] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] = @AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] = @AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] like @lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_NomeArq] like @lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] = @AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_NomeArq] = @AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] like @lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_TipoArq] like @lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] = @AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_TipoArq] = @AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Data] >= @AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Data] >= @AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Data] <= @AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Data] <= @AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPFEvidencia_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00M13( IGxContext context ,
                                             String AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                             bool AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                             short AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                             String AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                             bool AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                             String AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                             short AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                             String AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                             String AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                             String AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                             String AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                             String AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                             String AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                             String AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                             DateTime AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                             DateTime AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             DateTime A411FuncaoAPFEvidencia_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [14] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] = @AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] = @AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] like @lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_NomeArq] like @lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] = @AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_NomeArq] = @AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] like @lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_TipoArq] like @lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] = @AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_TipoArq] = @AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Data] >= @AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Data] >= @AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Data] <= @AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Data] <= @AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPFEvidencia_NomeArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00M14( IGxContext context ,
                                             String AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                             bool AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                             short AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                             String AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                             bool AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                             String AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                             short AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                             String AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                             String AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                             String AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                             String AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                             String AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                             String AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                             String AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                             DateTime AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                             DateTime AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             DateTime A411FuncaoAPFEvidencia_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [14] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV52WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV54WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV56WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV58WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV60WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like '%' + @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like '%' + @lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] like @lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] = @AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Descricao] = @AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] like @lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_NomeArq] like @lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] = @AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_NomeArq] = @AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] like @lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_TipoArq] like @lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] = @AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_TipoArq] = @AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Data] >= @AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Data] >= @AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Data] <= @AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoAPFEvidencia_Data] <= @AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPFEvidencia_TipoArq]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00M12(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] );
               case 1 :
                     return conditional_P00M13(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] );
               case 2 :
                     return conditional_P00M14(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00M12 ;
          prmP00M12 = new Object[] {
          new Object[] {"@lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00M13 ;
          prmP00M13 = new Object[] {
          new Object[] {"@lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00M14 ;
          prmP00M14 = new Object[] {
          new Object[] {"@lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV53WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV57WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV62WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV63WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV67WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV68WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV69WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00M12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00M12,100,0,true,false )
             ,new CursorDef("P00M13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00M13,100,0,true,false )
             ,new CursorDef("P00M14", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00M14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwfuncaoapfevidenciafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwfuncaoapfevidenciafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwfuncaoapfevidenciafilterdata") )
          {
             return  ;
          }
          getwwfuncaoapfevidenciafilterdata worker = new getwwfuncaoapfevidenciafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
