/*
               File: ContratoServicosVnc
        Description: Regra de Vinculo entre Servi�os
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:59.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosvnc : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELCONTRATO_CODIGO") == 0 )
         {
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvSELCONTRATO_CODIGO2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELSERVICO_CODIGO") == 0 )
         {
            AV20SelContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvSELSERVICO_CODIGO2V114( AV20SelContrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel16"+"_"+"CONTRATOSERVICOSVNC_GESTOR") == 0 )
         {
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX16ASACONTRATOSERVICOSVNC_GESTOR2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_40") == 0 )
         {
            A917ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_40( A917ContratoSrvVnc_Codigo, A1088ContratoSrvVnc_PrestadoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_41") == 0 )
         {
            A917ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_41( A917ContratoSrvVnc_Codigo, A1088ContratoSrvVnc_PrestadoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_42") == 0 )
         {
            A915ContratoSrvVnc_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A915ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_42( A915ContratoSrvVnc_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_44") == 0 )
         {
            A933ContratoSrvVnc_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n933ContratoSrvVnc_ContratoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A933ContratoSrvVnc_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A933ContratoSrvVnc_ContratoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_44( A933ContratoSrvVnc_ContratoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_45") == 0 )
         {
            A921ContratoSrvVnc_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n921ContratoSrvVnc_ServicoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A921ContratoSrvVnc_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A921ContratoSrvVnc_ServicoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_45( A921ContratoSrvVnc_ServicoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_43") == 0 )
         {
            A1589ContratoSrvVnc_SrvVncCntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1589ContratoSrvVnc_SrvVncCntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_43( A1589ContratoSrvVnc_SrvVncCntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_46") == 0 )
         {
            A1628ContratoSrvVnc_SrvVncCntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1628ContratoSrvVnc_SrvVncCntCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1628ContratoSrvVnc_SrvVncCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1628ContratoSrvVnc_SrvVncCntCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_46( A1628ContratoSrvVnc_SrvVncCntCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_47") == 0 )
         {
            A923ContratoSrvVnc_ServicoVncCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n923ContratoSrvVnc_ServicoVncCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A923ContratoSrvVnc_ServicoVncCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A923ContratoSrvVnc_ServicoVncCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_47( A923ContratoSrvVnc_ServicoVncCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoSrvVnc_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSRVVNC_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoSrvVnc_Codigo), "ZZZZZ9")));
               AV16ContratoSrvVnc_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContratoSrvVnc_CntSrvCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratoSrvVnc_DoStatusDmn.Name = "CONTRATOSRVVNC_DOSTATUSDMN";
         cmbContratoSrvVnc_DoStatusDmn.WebTags = "";
         cmbContratoSrvVnc_DoStatusDmn.addItem("", "(Qualquer um)", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("B", "Stand by", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("S", "Solicitada", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("E", "Em An�lise", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("A", "Em execu��o", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("R", "Resolvida", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("C", "Conferida", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("D", "Rejeitada", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("H", "Homologada", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("O", "Aceite", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("P", "A Pagar", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("L", "Liquidada", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("X", "Cancelada", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("N", "N�o Faturada", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("J", "Planejamento", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("I", "An�lise Planejamento", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("T", "Validacao T�cnica", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("Q", "Validacao Qualidade", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("G", "Em Homologa��o", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("M", "Valida��o Mensura��o", 0);
         cmbContratoSrvVnc_DoStatusDmn.addItem("U", "Rascunho", 0);
         if ( cmbContratoSrvVnc_DoStatusDmn.ItemCount > 0 )
         {
            A1800ContratoSrvVnc_DoStatusDmn = cmbContratoSrvVnc_DoStatusDmn.getValidValue(A1800ContratoSrvVnc_DoStatusDmn);
            n1800ContratoSrvVnc_DoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
         }
         cmbContratoSrvVnc_StatusDmn.Name = "CONTRATOSRVVNC_STATUSDMN";
         cmbContratoSrvVnc_StatusDmn.WebTags = "";
         cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("D", "Rejeitada", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
         cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
         if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
         {
            A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
            n1084ContratoSrvVnc_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
         }
         cmbContratoSrvVnc_PrestadoraCod.Name = "CONTRATOSRVVNC_PRESTADORACOD";
         cmbContratoSrvVnc_PrestadoraCod.WebTags = "";
         cmbContratoSrvVnc_PrestadoraCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhuma)", 0);
         if ( cmbContratoSrvVnc_PrestadoraCod.ItemCount > 0 )
         {
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cmbContratoSrvVnc_PrestadoraCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0))), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
         }
         cmbContratoSrvVnc_SrvVncStatus.Name = "CONTRATOSRVVNC_SRVVNCSTATUS";
         cmbContratoSrvVnc_SrvVncStatus.WebTags = "";
         cmbContratoSrvVnc_SrvVncStatus.addItem("B", "Stand by", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("S", "Solicitada", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("E", "Em An�lise", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("A", "Em execu��o", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("R", "Resolvida", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("C", "Conferida", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("D", "Rejeitada", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("H", "Homologada", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("O", "Aceite", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("P", "A Pagar", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("L", "Liquidada", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("X", "Cancelada", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("N", "N�o Faturada", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("J", "Planejamento", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("I", "An�lise Planejamento", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("T", "Validacao T�cnica", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("Q", "Validacao Qualidade", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("G", "Em Homologa��o", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("M", "Valida��o Mensura��o", 0);
         cmbContratoSrvVnc_SrvVncStatus.addItem("U", "Rascunho", 0);
         if ( cmbContratoSrvVnc_SrvVncStatus.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus)) )
            {
               A1663ContratoSrvVnc_SrvVncStatus = "S";
               n1663ContratoSrvVnc_SrvVncStatus = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
            }
            A1663ContratoSrvVnc_SrvVncStatus = cmbContratoSrvVnc_SrvVncStatus.getValidValue(A1663ContratoSrvVnc_SrvVncStatus);
            n1663ContratoSrvVnc_SrvVncStatus = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
         }
         dynavSelcontrato_codigo.Name = "vSELCONTRATO_CODIGO";
         dynavSelcontrato_codigo.WebTags = "";
         dynavSelservico_codigo.Name = "vSELSERVICO_CODIGO";
         dynavSelservico_codigo.WebTags = "";
         cmbContratoSrvVnc_VincularCom.Name = "CONTRATOSRVVNC_VINCULARCOM";
         cmbContratoSrvVnc_VincularCom.WebTags = "";
         cmbContratoSrvVnc_VincularCom.addItem("D", "Quem dispara", 0);
         cmbContratoSrvVnc_VincularCom.addItem("O", "A Origem", 0);
         if ( cmbContratoSrvVnc_VincularCom.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom)) )
            {
               A1745ContratoSrvVnc_VincularCom = "D";
               n1745ContratoSrvVnc_VincularCom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
            }
            A1745ContratoSrvVnc_VincularCom = cmbContratoSrvVnc_VincularCom.getValidValue(A1745ContratoSrvVnc_VincularCom);
            n1745ContratoSrvVnc_VincularCom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
         }
         cmbContratoSrvVnc_SrvVncRef.Name = "CONTRATOSRVVNC_SRVVNCREF";
         cmbContratoSrvVnc_SrvVncRef.WebTags = "";
         cmbContratoSrvVnc_SrvVncRef.addItem("0", "N� da OS Vinculada", 0);
         cmbContratoSrvVnc_SrvVncRef.addItem("1", "Refer�ncia da OS Vinculada", 0);
         if ( cmbContratoSrvVnc_SrvVncRef.ItemCount > 0 )
         {
            A1821ContratoSrvVnc_SrvVncRef = (short)(NumberUtil.Val( cmbContratoSrvVnc_SrvVncRef.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0))), "."));
            n1821ContratoSrvVnc_SrvVncRef = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
         }
         cmbContratoServicosVnc_Gestor.Name = "CONTRATOSERVICOSVNC_GESTOR";
         cmbContratoServicosVnc_Gestor.WebTags = "";
         cmbContratoServicosVnc_Gestor.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
         cmbContratoServicosVnc_Gestor.addItem("700000", "Respons�vel atual", 0);
         cmbContratoServicosVnc_Gestor.addItem("800000", "Criador da OS", 0);
         cmbContratoServicosVnc_Gestor.addItem("900000", "Gestor do contrato", 0);
         if ( cmbContratoServicosVnc_Gestor.ItemCount > 0 )
         {
            A1438ContratoServicosVnc_Gestor = (int)(NumberUtil.Val( cmbContratoServicosVnc_Gestor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0))), "."));
            n1438ContratoServicosVnc_Gestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
         }
         cmbContratoSrvVnc_NovoStatusDmn.Name = "CONTRATOSRVVNC_NOVOSTATUSDMN";
         cmbContratoSrvVnc_NovoStatusDmn.WebTags = "";
         cmbContratoSrvVnc_NovoStatusDmn.addItem("", "(N�o mudar)", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("B", "Stand by", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("S", "Solicitada", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("E", "Em An�lise", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("A", "Em execu��o", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("R", "Resolvida", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("C", "Conferida", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("D", "Rejeitada", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("H", "Homologada", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("O", "Aceite", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("P", "A Pagar", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("L", "Liquidada", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("X", "Cancelada", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("N", "N�o Faturada", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("J", "Planejamento", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("I", "An�lise Planejamento", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("T", "Validacao T�cnica", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("Q", "Validacao Qualidade", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("G", "Em Homologa��o", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("M", "Valida��o Mensura��o", 0);
         cmbContratoSrvVnc_NovoStatusDmn.addItem("U", "Rascunho", 0);
         if ( cmbContratoSrvVnc_NovoStatusDmn.ItemCount > 0 )
         {
            A1743ContratoSrvVnc_NovoStatusDmn = cmbContratoSrvVnc_NovoStatusDmn.getValidValue(A1743ContratoSrvVnc_NovoStatusDmn);
            n1743ContratoSrvVnc_NovoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
         }
         cmbContratoSrvVnc_NovoRspDmn.Name = "CONTRATOSRVVNC_NOVORSPDMN";
         cmbContratoSrvVnc_NovoRspDmn.WebTags = "";
         cmbContratoSrvVnc_NovoRspDmn.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(N�o mudar)", 0);
         cmbContratoSrvVnc_NovoRspDmn.addItem("910000", "Criador da SS", 0);
         cmbContratoSrvVnc_NovoRspDmn.addItem("920000", ".Criador da OS", 0);
         if ( cmbContratoSrvVnc_NovoRspDmn.ItemCount > 0 )
         {
            A1801ContratoSrvVnc_NovoRspDmn = (int)(NumberUtil.Val( cmbContratoSrvVnc_NovoRspDmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0))), "."));
            n1801ContratoSrvVnc_NovoRspDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
         }
         cmbContratoServicosVnc_ClonaSrvOri.Name = "CONTRATOSERVICOSVNC_CLONASRVORI";
         cmbContratoServicosVnc_ClonaSrvOri.WebTags = "";
         cmbContratoServicosVnc_ClonaSrvOri.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratoServicosVnc_ClonaSrvOri.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratoServicosVnc_ClonaSrvOri.ItemCount > 0 )
         {
            A1145ContratoServicosVnc_ClonaSrvOri = StringUtil.StrToBool( cmbContratoServicosVnc_ClonaSrvOri.getValidValue(StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri)));
            n1145ContratoServicosVnc_ClonaSrvOri = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
         }
         cmbContratoServicosVnc_ClonarLink.Name = "CONTRATOSERVICOSVNC_CLONARLINK";
         cmbContratoServicosVnc_ClonarLink.WebTags = "";
         cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratoServicosVnc_ClonarLink.ItemCount > 0 )
         {
            A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cmbContratoServicosVnc_ClonarLink.getValidValue(StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink)));
            n1818ContratoServicosVnc_ClonarLink = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
         }
         chkContratoServicosVnc_NaoClonaInfo.Name = "CONTRATOSERVICOSVNC_NAOCLONAINFO";
         chkContratoServicosVnc_NaoClonaInfo.WebTags = "";
         chkContratoServicosVnc_NaoClonaInfo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosVnc_NaoClonaInfo_Internalname, "TitleCaption", chkContratoServicosVnc_NaoClonaInfo.Caption);
         chkContratoServicosVnc_NaoClonaInfo.CheckedValue = "false";
         cmbContratoSrvVnc_SemCusto.Name = "CONTRATOSRVVNC_SEMCUSTO";
         cmbContratoSrvVnc_SemCusto.WebTags = "";
         cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
         {
            A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
            n1090ContratoSrvVnc_SemCusto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
         }
         chkContratoServicosVnc_Ativo.Name = "CONTRATOSERVICOSVNC_ATIVO";
         chkContratoServicosVnc_Ativo.WebTags = "";
         chkContratoServicosVnc_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosVnc_Ativo_Internalname, "TitleCaption", chkContratoServicosVnc_Ativo.Caption);
         chkContratoServicosVnc_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Regra de Vinculo entre Servi�os", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoServicosVnc_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicosvnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosvnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoSrvVnc_Codigo ,
                           ref int aP2_ContratoSrvVnc_CntSrvCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoSrvVnc_Codigo = aP1_ContratoSrvVnc_Codigo;
         this.AV16ContratoSrvVnc_CntSrvCod = aP2_ContratoSrvVnc_CntSrvCod;
         executePrivate();
         aP2_ContratoSrvVnc_CntSrvCod=this.AV16ContratoSrvVnc_CntSrvCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoSrvVnc_DoStatusDmn = new GXCombobox();
         cmbContratoSrvVnc_StatusDmn = new GXCombobox();
         cmbContratoSrvVnc_PrestadoraCod = new GXCombobox();
         cmbContratoSrvVnc_SrvVncStatus = new GXCombobox();
         dynavSelcontrato_codigo = new GXCombobox();
         dynavSelservico_codigo = new GXCombobox();
         cmbContratoSrvVnc_VincularCom = new GXCombobox();
         cmbContratoSrvVnc_SrvVncRef = new GXCombobox();
         cmbContratoServicosVnc_Gestor = new GXCombobox();
         cmbContratoSrvVnc_NovoStatusDmn = new GXCombobox();
         cmbContratoSrvVnc_NovoRspDmn = new GXCombobox();
         cmbContratoServicosVnc_ClonaSrvOri = new GXCombobox();
         cmbContratoServicosVnc_ClonarLink = new GXCombobox();
         chkContratoServicosVnc_NaoClonaInfo = new GXCheckbox();
         cmbContratoSrvVnc_SemCusto = new GXCombobox();
         chkContratoServicosVnc_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoSrvVnc_DoStatusDmn.ItemCount > 0 )
         {
            A1800ContratoSrvVnc_DoStatusDmn = cmbContratoSrvVnc_DoStatusDmn.getValidValue(A1800ContratoSrvVnc_DoStatusDmn);
            n1800ContratoSrvVnc_DoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
         }
         if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
         {
            A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
            n1084ContratoSrvVnc_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
         }
         if ( cmbContratoSrvVnc_PrestadoraCod.ItemCount > 0 )
         {
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cmbContratoSrvVnc_PrestadoraCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0))), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
         }
         if ( cmbContratoSrvVnc_SrvVncStatus.ItemCount > 0 )
         {
            A1663ContratoSrvVnc_SrvVncStatus = cmbContratoSrvVnc_SrvVncStatus.getValidValue(A1663ContratoSrvVnc_SrvVncStatus);
            n1663ContratoSrvVnc_SrvVncStatus = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
         }
         if ( dynavSelcontrato_codigo.ItemCount > 0 )
         {
            AV20SelContrato_Codigo = (int)(NumberUtil.Val( dynavSelcontrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
         }
         if ( dynavSelservico_codigo.ItemCount > 0 )
         {
            AV22SelServico_Codigo = (int)(NumberUtil.Val( dynavSelservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
         }
         if ( cmbContratoSrvVnc_VincularCom.ItemCount > 0 )
         {
            A1745ContratoSrvVnc_VincularCom = cmbContratoSrvVnc_VincularCom.getValidValue(A1745ContratoSrvVnc_VincularCom);
            n1745ContratoSrvVnc_VincularCom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
         }
         if ( cmbContratoSrvVnc_SrvVncRef.ItemCount > 0 )
         {
            A1821ContratoSrvVnc_SrvVncRef = (short)(NumberUtil.Val( cmbContratoSrvVnc_SrvVncRef.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0))), "."));
            n1821ContratoSrvVnc_SrvVncRef = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
         }
         if ( cmbContratoServicosVnc_Gestor.ItemCount > 0 )
         {
            A1438ContratoServicosVnc_Gestor = (int)(NumberUtil.Val( cmbContratoServicosVnc_Gestor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0))), "."));
            n1438ContratoServicosVnc_Gestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
         }
         if ( cmbContratoSrvVnc_NovoStatusDmn.ItemCount > 0 )
         {
            A1743ContratoSrvVnc_NovoStatusDmn = cmbContratoSrvVnc_NovoStatusDmn.getValidValue(A1743ContratoSrvVnc_NovoStatusDmn);
            n1743ContratoSrvVnc_NovoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
         }
         if ( cmbContratoSrvVnc_NovoRspDmn.ItemCount > 0 )
         {
            A1801ContratoSrvVnc_NovoRspDmn = (int)(NumberUtil.Val( cmbContratoSrvVnc_NovoRspDmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0))), "."));
            n1801ContratoSrvVnc_NovoRspDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
         }
         if ( cmbContratoServicosVnc_ClonaSrvOri.ItemCount > 0 )
         {
            A1145ContratoServicosVnc_ClonaSrvOri = StringUtil.StrToBool( cmbContratoServicosVnc_ClonaSrvOri.getValidValue(StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri)));
            n1145ContratoServicosVnc_ClonaSrvOri = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
         }
         if ( cmbContratoServicosVnc_ClonarLink.ItemCount > 0 )
         {
            A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cmbContratoServicosVnc_ClonarLink.getValidValue(StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink)));
            n1818ContratoServicosVnc_ClonarLink = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
         }
         if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
         {
            A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
            n1090ContratoSrvVnc_SemCusto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2V114( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2V114e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoSrvVnc_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ",", "")), ((edtContratoSrvVnc_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoSrvVnc_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoSrvVnc_Codigo_Visible, edtContratoSrvVnc_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosVnc.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2V114( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2V114( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2V114e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_107_2V114( true) ;
         }
         return  ;
      }

      protected void wb_table3_107_2V114e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2V114e( true) ;
         }
         else
         {
            wb_table1_2_2V114e( false) ;
         }
      }

      protected void wb_table3_107_2V114( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_107_2V114e( true) ;
         }
         else
         {
            wb_table3_107_2V114e( false) ;
         }
      }

      protected void wb_table2_5_2V114( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2V114( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2V114e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2V114e( true) ;
         }
         else
         {
            wb_table2_5_2V114e( false) ;
         }
      }

      protected void wb_table4_13_2V114( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_servicosigla_Internalname, "Servi�o", "", "", lblTextblockcontratosrvvnc_servicosigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoSrvVnc_ServicoSigla_Internalname, StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A922ContratoSrvVnc_ServicoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoSrvVnc_ServicoSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoSrvVnc_ServicoSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoservicosvnc_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosVnc_Descricao_Internalname, A2108ContratoServicosVnc_Descricao, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosVnc_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosVnc_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_dostatusdmn_Internalname, "Do Status", "", "", lblTextblockcontratosrvvnc_dostatusdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_DoStatusDmn, cmbContratoSrvVnc_DoStatusDmn_Internalname, StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn), 1, cmbContratoSrvVnc_DoStatusDmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoSrvVnc_DoStatusDmn.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_DoStatusDmn.CurrentValue = StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_DoStatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_DoStatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_statusdmn_Internalname, "Para o Status", "", "", lblTextblockcontratosrvvnc_statusdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_StatusDmn, cmbContratoSrvVnc_StatusDmn_Internalname, StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn), 1, cmbContratoSrvVnc_StatusDmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoSrvVnc_StatusDmn.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_StatusDmn.CurrentValue = StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_StatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_StatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_prestadoracod_Internalname, "A��o", "", "", lblTextblockcontratosrvvnc_prestadoracod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_PrestadoraCod, cmbContratoSrvVnc_PrestadoraCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)), 1, cmbContratoSrvVnc_PrestadoraCod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"ECONTRATOSRVVNC_PRESTADORACOD.CLICK."+"'", "int", "", 1, cmbContratoSrvVnc_PrestadoraCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_PrestadoraCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_PrestadoraCod_Internalname, "Values", (String)(cmbContratoSrvVnc_PrestadoraCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_srvvncstatus_Internalname, "Com o Status", "", "", lblTextblockcontratosrvvnc_srvvncstatus_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_SrvVncStatus, cmbContratoSrvVnc_SrvVncStatus_Internalname, StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus), 1, cmbContratoSrvVnc_SrvVncStatus_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoSrvVnc_SrvVncStatus.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_SrvVncStatus.CurrentValue = StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SrvVncStatus_Internalname, "Values", (String)(cmbContratoSrvVnc_SrvVncStatus.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockselcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockselcontrato_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockselcontrato_codigo_Visible, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelcontrato_codigo, dynavSelcontrato_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)), 1, dynavSelcontrato_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSELCONTRATO_CODIGO.CLICK."+"'", "int", "", dynavSelcontrato_codigo.Visible, dynavSelcontrato_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_ContratoServicosVnc.htm");
            dynavSelcontrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontrato_codigo_Internalname, "Values", (String)(dynavSelcontrato_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockselservico_codigo_Internalname, "Demanda", "", "", lblTextblockselservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockselservico_codigo_Visible, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelservico_codigo, dynavSelservico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)), 1, dynavSelservico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavSelservico_codigo.Visible, dynavSelservico_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_ContratoServicosVnc.htm");
            dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Values", (String)(dynavSelservico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_vincularcom_Internalname, lblTextblockcontratosrvvnc_vincularcom_Caption, "", "", lblTextblockcontratosrvvnc_vincularcom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_VincularCom, cmbContratoSrvVnc_VincularCom_Internalname, StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom), 1, cmbContratoSrvVnc_VincularCom_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoSrvVnc_VincularCom.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_VincularCom.CurrentValue = StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_VincularCom_Internalname, "Values", (String)(cmbContratoSrvVnc_VincularCom.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_srvvncref_Internalname, "Refer�ncia", "", "", lblTextblockcontratosrvvnc_srvvncref_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_SrvVncRef, cmbContratoSrvVnc_SrvVncRef_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)), 1, cmbContratoSrvVnc_SrvVncRef_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoSrvVnc_SrvVncRef.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_SrvVncRef.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SrvVncRef_Internalname, "Values", (String)(cmbContratoSrvVnc_SrvVncRef.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_gestor_Internalname, "Emissor", "", "", lblTextblockcontratoservicosvnc_gestor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosVnc_Gestor, cmbContratoServicosVnc_Gestor_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)), 1, cmbContratoServicosVnc_Gestor_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoServicosVnc_Gestor.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoServicosVnc_Gestor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_Gestor_Internalname, "Values", (String)(cmbContratoServicosVnc_Gestor.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_novostatusdmn_Internalname, "Deixando-a no Status", "", "", lblTextblockcontratosrvvnc_novostatusdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_NovoStatusDmn, cmbContratoSrvVnc_NovoStatusDmn_Internalname, StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn), 1, cmbContratoSrvVnc_NovoStatusDmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoSrvVnc_NovoStatusDmn.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_NovoStatusDmn.CurrentValue = StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoStatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_NovoStatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_novorspdmn_Internalname, "Deixando-a com", "", "", lblTextblockcontratosrvvnc_novorspdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_NovoRspDmn, cmbContratoSrvVnc_NovoRspDmn_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)), 1, cmbContratoSrvVnc_NovoRspDmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoSrvVnc_NovoRspDmn.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_NovoRspDmn.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoRspDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_NovoRspDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_clonasrvori_Internalname, "Clonar resultado", "", "", lblTextblockcontratoservicosvnc_clonasrvori_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosVnc_ClonaSrvOri, cmbContratoServicosVnc_ClonaSrvOri_Internalname, StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri), 1, cmbContratoServicosVnc_ClonaSrvOri_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratoServicosVnc_ClonaSrvOri.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoServicosVnc_ClonaSrvOri.CurrentValue = StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_ClonaSrvOri_Internalname, "Values", (String)(cmbContratoServicosVnc_ClonaSrvOri.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_clonarlink_Internalname, "Clonar link", "", "", lblTextblockcontratoservicosvnc_clonarlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosVnc_ClonarLink, cmbContratoServicosVnc_ClonarLink_Internalname, StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink), 1, cmbContratoServicosVnc_ClonarLink_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratoServicosVnc_ClonarLink.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoServicosVnc_ClonarLink.CurrentValue = StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_ClonarLink_Internalname, "Values", (String)(cmbContratoServicosVnc_ClonarLink.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_naoclonainfo_Internalname, "N�o clonar info", "", "", lblTextblockcontratoservicosvnc_naoclonainfo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_88_2V114( true) ;
         }
         return  ;
      }

      protected void wb_table5_88_2V114e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_semcusto_Internalname, "Sem custo", "", "", lblTextblockcontratosrvvnc_semcusto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_SemCusto, cmbContratoSrvVnc_SemCusto_Internalname, StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto), 1, cmbContratoSrvVnc_SemCusto_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratoSrvVnc_SemCusto.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", "", true, "HLP_ContratoServicosVnc.htm");
            cmbContratoSrvVnc_SemCusto.CurrentValue = StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SemCusto_Internalname, "Values", (String)(cmbContratoSrvVnc_SemCusto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_ativo_Internalname, "Ativa?", "", "", lblTextblockcontratoservicosvnc_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontratoservicosvnc_ativo_Visible, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicosVnc_Ativo_Internalname, StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo), "", "", chkContratoServicosVnc_Ativo.Visible, chkContratoServicosVnc_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2V114e( true) ;
         }
         else
         {
            wb_table4_13_2V114e( false) ;
         }
      }

      protected void wb_table5_88_2V114( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname, tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicosVnc_NaoClonaInfo_Internalname, StringUtil.BoolToStr( A1437ContratoServicosVnc_NaoClonaInfo), "", "", 1, chkContratoServicosVnc_NaoClonaInfo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosvnc_naoclonainfo_righttext_Internalname, "�(T�tulo, observa��o, evid�ncias)", "", "", lblContratoservicosvnc_naoclonainfo_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_88_2V114e( true) ;
         }
         else
         {
            wb_table5_88_2V114e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112V2 */
         E112V2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A922ContratoSrvVnc_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoSrvVnc_ServicoSigla_Internalname));
               n922ContratoSrvVnc_ServicoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
               A2108ContratoServicosVnc_Descricao = StringUtil.Upper( cgiGet( edtContratoServicosVnc_Descricao_Internalname));
               n2108ContratoServicosVnc_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2108ContratoServicosVnc_Descricao", A2108ContratoServicosVnc_Descricao);
               n2108ContratoServicosVnc_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2108ContratoServicosVnc_Descricao)) ? true : false);
               cmbContratoSrvVnc_DoStatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_DoStatusDmn_Internalname);
               A1800ContratoSrvVnc_DoStatusDmn = cgiGet( cmbContratoSrvVnc_DoStatusDmn_Internalname);
               n1800ContratoSrvVnc_DoStatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
               n1800ContratoSrvVnc_DoStatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn)) ? true : false);
               cmbContratoSrvVnc_StatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
               A1084ContratoSrvVnc_StatusDmn = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
               n1084ContratoSrvVnc_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
               n1084ContratoSrvVnc_StatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn)) ? true : false);
               cmbContratoSrvVnc_PrestadoraCod.CurrentValue = cgiGet( cmbContratoSrvVnc_PrestadoraCod_Internalname);
               A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cgiGet( cmbContratoSrvVnc_PrestadoraCod_Internalname), "."));
               n1088ContratoSrvVnc_PrestadoraCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
               n1088ContratoSrvVnc_PrestadoraCod = ((0==A1088ContratoSrvVnc_PrestadoraCod) ? true : false);
               cmbContratoSrvVnc_SrvVncStatus.CurrentValue = cgiGet( cmbContratoSrvVnc_SrvVncStatus_Internalname);
               A1663ContratoSrvVnc_SrvVncStatus = cgiGet( cmbContratoSrvVnc_SrvVncStatus_Internalname);
               n1663ContratoSrvVnc_SrvVncStatus = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
               n1663ContratoSrvVnc_SrvVncStatus = (String.IsNullOrEmpty(StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus)) ? true : false);
               dynavSelcontrato_codigo.CurrentValue = cgiGet( dynavSelcontrato_codigo_Internalname);
               AV20SelContrato_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelcontrato_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
               dynavSelservico_codigo.CurrentValue = cgiGet( dynavSelservico_codigo_Internalname);
               AV22SelServico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelservico_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
               cmbContratoSrvVnc_VincularCom.CurrentValue = cgiGet( cmbContratoSrvVnc_VincularCom_Internalname);
               A1745ContratoSrvVnc_VincularCom = cgiGet( cmbContratoSrvVnc_VincularCom_Internalname);
               n1745ContratoSrvVnc_VincularCom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
               n1745ContratoSrvVnc_VincularCom = (String.IsNullOrEmpty(StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom)) ? true : false);
               cmbContratoSrvVnc_SrvVncRef.CurrentValue = cgiGet( cmbContratoSrvVnc_SrvVncRef_Internalname);
               A1821ContratoSrvVnc_SrvVncRef = (short)(NumberUtil.Val( cgiGet( cmbContratoSrvVnc_SrvVncRef_Internalname), "."));
               n1821ContratoSrvVnc_SrvVncRef = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
               n1821ContratoSrvVnc_SrvVncRef = ((0==A1821ContratoSrvVnc_SrvVncRef) ? true : false);
               cmbContratoServicosVnc_Gestor.CurrentValue = cgiGet( cmbContratoServicosVnc_Gestor_Internalname);
               A1438ContratoServicosVnc_Gestor = (int)(NumberUtil.Val( cgiGet( cmbContratoServicosVnc_Gestor_Internalname), "."));
               n1438ContratoServicosVnc_Gestor = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
               n1438ContratoServicosVnc_Gestor = ((0==A1438ContratoServicosVnc_Gestor) ? true : false);
               cmbContratoSrvVnc_NovoStatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_NovoStatusDmn_Internalname);
               A1743ContratoSrvVnc_NovoStatusDmn = cgiGet( cmbContratoSrvVnc_NovoStatusDmn_Internalname);
               n1743ContratoSrvVnc_NovoStatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
               n1743ContratoSrvVnc_NovoStatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn)) ? true : false);
               cmbContratoSrvVnc_NovoRspDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_NovoRspDmn_Internalname);
               A1801ContratoSrvVnc_NovoRspDmn = (int)(NumberUtil.Val( cgiGet( cmbContratoSrvVnc_NovoRspDmn_Internalname), "."));
               n1801ContratoSrvVnc_NovoRspDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
               n1801ContratoSrvVnc_NovoRspDmn = ((0==A1801ContratoSrvVnc_NovoRspDmn) ? true : false);
               cmbContratoServicosVnc_ClonaSrvOri.CurrentValue = cgiGet( cmbContratoServicosVnc_ClonaSrvOri_Internalname);
               A1145ContratoServicosVnc_ClonaSrvOri = StringUtil.StrToBool( cgiGet( cmbContratoServicosVnc_ClonaSrvOri_Internalname));
               n1145ContratoServicosVnc_ClonaSrvOri = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
               n1145ContratoServicosVnc_ClonaSrvOri = ((false==A1145ContratoServicosVnc_ClonaSrvOri) ? true : false);
               cmbContratoServicosVnc_ClonarLink.CurrentValue = cgiGet( cmbContratoServicosVnc_ClonarLink_Internalname);
               A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cgiGet( cmbContratoServicosVnc_ClonarLink_Internalname));
               n1818ContratoServicosVnc_ClonarLink = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
               n1818ContratoServicosVnc_ClonarLink = ((false==A1818ContratoServicosVnc_ClonarLink) ? true : false);
               A1437ContratoServicosVnc_NaoClonaInfo = StringUtil.StrToBool( cgiGet( chkContratoServicosVnc_NaoClonaInfo_Internalname));
               n1437ContratoServicosVnc_NaoClonaInfo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1437ContratoServicosVnc_NaoClonaInfo", A1437ContratoServicosVnc_NaoClonaInfo);
               n1437ContratoServicosVnc_NaoClonaInfo = ((false==A1437ContratoServicosVnc_NaoClonaInfo) ? true : false);
               cmbContratoSrvVnc_SemCusto.CurrentValue = cgiGet( cmbContratoSrvVnc_SemCusto_Internalname);
               A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cgiGet( cmbContratoSrvVnc_SemCusto_Internalname));
               n1090ContratoSrvVnc_SemCusto = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
               n1090ContratoSrvVnc_SemCusto = ((false==A1090ContratoSrvVnc_SemCusto) ? true : false);
               A1453ContratoServicosVnc_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicosVnc_Ativo_Internalname));
               n1453ContratoServicosVnc_Ativo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
               n1453ContratoServicosVnc_Ativo = ((false==A1453ContratoServicosVnc_Ativo) ? true : false);
               A917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
               /* Read saved values. */
               Z917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z917ContratoSrvVnc_Codigo"), ",", "."));
               Z1801ContratoSrvVnc_NovoRspDmn = (int)(context.localUtil.CToN( cgiGet( "Z1801ContratoSrvVnc_NovoRspDmn"), ",", "."));
               n1801ContratoSrvVnc_NovoRspDmn = ((0==A1801ContratoSrvVnc_NovoRspDmn) ? true : false);
               Z1743ContratoSrvVnc_NovoStatusDmn = cgiGet( "Z1743ContratoSrvVnc_NovoStatusDmn");
               n1743ContratoSrvVnc_NovoStatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn)) ? true : false);
               Z2108ContratoServicosVnc_Descricao = cgiGet( "Z2108ContratoServicosVnc_Descricao");
               n2108ContratoServicosVnc_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2108ContratoServicosVnc_Descricao)) ? true : false);
               Z1663ContratoSrvVnc_SrvVncStatus = cgiGet( "Z1663ContratoSrvVnc_SrvVncStatus");
               n1663ContratoSrvVnc_SrvVncStatus = (String.IsNullOrEmpty(StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus)) ? true : false);
               Z1800ContratoSrvVnc_DoStatusDmn = cgiGet( "Z1800ContratoSrvVnc_DoStatusDmn");
               n1800ContratoSrvVnc_DoStatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn)) ? true : false);
               Z1084ContratoSrvVnc_StatusDmn = cgiGet( "Z1084ContratoSrvVnc_StatusDmn");
               n1084ContratoSrvVnc_StatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn)) ? true : false);
               Z1745ContratoSrvVnc_VincularCom = cgiGet( "Z1745ContratoSrvVnc_VincularCom");
               n1745ContratoSrvVnc_VincularCom = (String.IsNullOrEmpty(StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom)) ? true : false);
               Z1821ContratoSrvVnc_SrvVncRef = (short)(context.localUtil.CToN( cgiGet( "Z1821ContratoSrvVnc_SrvVncRef"), ",", "."));
               n1821ContratoSrvVnc_SrvVncRef = ((0==A1821ContratoSrvVnc_SrvVncRef) ? true : false);
               Z1088ContratoSrvVnc_PrestadoraCod = (int)(context.localUtil.CToN( cgiGet( "Z1088ContratoSrvVnc_PrestadoraCod"), ",", "."));
               n1088ContratoSrvVnc_PrestadoraCod = ((0==A1088ContratoSrvVnc_PrestadoraCod) ? true : false);
               Z1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cgiGet( "Z1090ContratoSrvVnc_SemCusto"));
               n1090ContratoSrvVnc_SemCusto = ((false==A1090ContratoSrvVnc_SemCusto) ? true : false);
               Z1145ContratoServicosVnc_ClonaSrvOri = StringUtil.StrToBool( cgiGet( "Z1145ContratoServicosVnc_ClonaSrvOri"));
               n1145ContratoServicosVnc_ClonaSrvOri = ((false==A1145ContratoServicosVnc_ClonaSrvOri) ? true : false);
               Z1437ContratoServicosVnc_NaoClonaInfo = StringUtil.StrToBool( cgiGet( "Z1437ContratoServicosVnc_NaoClonaInfo"));
               n1437ContratoServicosVnc_NaoClonaInfo = ((false==A1437ContratoServicosVnc_NaoClonaInfo) ? true : false);
               Z1438ContratoServicosVnc_Gestor = (int)(context.localUtil.CToN( cgiGet( "Z1438ContratoServicosVnc_Gestor"), ",", "."));
               n1438ContratoServicosVnc_Gestor = ((0==A1438ContratoServicosVnc_Gestor) ? true : false);
               Z1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cgiGet( "Z1818ContratoServicosVnc_ClonarLink"));
               n1818ContratoServicosVnc_ClonarLink = ((false==A1818ContratoServicosVnc_ClonarLink) ? true : false);
               Z1453ContratoServicosVnc_Ativo = StringUtil.StrToBool( cgiGet( "Z1453ContratoServicosVnc_Ativo"));
               n1453ContratoServicosVnc_Ativo = ((false==A1453ContratoServicosVnc_Ativo) ? true : false);
               Z915ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z915ContratoSrvVnc_CntSrvCod"), ",", "."));
               Z1589ContratoSrvVnc_SrvVncCntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1589ContratoSrvVnc_SrvVncCntSrvCod"), ",", "."));
               n1589ContratoSrvVnc_SrvVncCntSrvCod = ((0==A1589ContratoSrvVnc_SrvVncCntSrvCod) ? true : false);
               A915ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z915ContratoSrvVnc_CntSrvCod"), ",", "."));
               A1589ContratoSrvVnc_SrvVncCntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1589ContratoSrvVnc_SrvVncCntSrvCod"), ",", "."));
               n1589ContratoSrvVnc_SrvVncCntSrvCod = false;
               n1589ContratoSrvVnc_SrvVncCntSrvCod = ((0==A1589ContratoSrvVnc_SrvVncCntSrvCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N915ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "N915ContratoSrvVnc_CntSrvCod"), ",", "."));
               N1589ContratoSrvVnc_SrvVncCntSrvCod = (int)(context.localUtil.CToN( cgiGet( "N1589ContratoSrvVnc_SrvVncCntSrvCod"), ",", "."));
               n1589ContratoSrvVnc_SrvVncCntSrvCod = ((0==A1589ContratoSrvVnc_SrvVncCntSrvCod) ? true : false);
               AV7ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSRVVNC_CODIGO"), ",", "."));
               AV11Insert_ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOSRVVNC_CNTSRVCOD"), ",", "."));
               A915ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_CNTSRVCOD"), ",", "."));
               AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOSRVVNC_SRVVNCCNTSRVCOD"), ",", "."));
               A1589ContratoSrvVnc_SrvVncCntSrvCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_SRVVNCCNTSRVCOD"), ",", "."));
               n1589ContratoSrvVnc_SrvVncCntSrvCod = ((0==A1589ContratoSrvVnc_SrvVncCntSrvCod) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1092ContratoSrvVnc_PrestadoraPesNom = cgiGet( "CONTRATOSRVVNC_PRESTADORAPESNOM");
               n1092ContratoSrvVnc_PrestadoraPesNom = false;
               A1744ContratoSrvVnc_PrestadoraTpFab = cgiGet( "CONTRATOSRVVNC_PRESTADORATPFAB");
               n1744ContratoSrvVnc_PrestadoraTpFab = false;
               A933ContratoSrvVnc_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_CONTRATOCOD"), ",", "."));
               A921ContratoSrvVnc_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_SERVICOCOD"), ",", "."));
               A1629ContratoSrvVnc_SrvVncTpVnc = cgiGet( "CONTRATOSRVVNC_SRVVNCTPVNC");
               n1629ContratoSrvVnc_SrvVncTpVnc = false;
               A1630ContratoSrvVnc_SrvVncTpDias = cgiGet( "CONTRATOSRVVNC_SRVVNCTPDIAS");
               n1630ContratoSrvVnc_SrvVncTpDias = false;
               A1642ContratoSrvVnc_SrvVncVlrUndCnt = context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_SRVVNCVLRUNDCNT"), ",", ".");
               n1642ContratoSrvVnc_SrvVncVlrUndCnt = false;
               A1651ContratoSrvVnc_SrvVncPrzInc = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_SRVVNCPRZINC"), ",", "."));
               n1651ContratoSrvVnc_SrvVncPrzInc = false;
               A1628ContratoSrvVnc_SrvVncCntCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_SRVVNCCNTCOD"), ",", "."));
               A923ContratoSrvVnc_ServicoVncCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_SERVICOVNCCOD"), ",", "."));
               A1662ContratoSrvVnc_ContratoAreaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSRVVNC_CONTRATOAREACOD"), ",", "."));
               n1662ContratoSrvVnc_ContratoAreaCod = false;
               A1631ContratoSrvVnc_SrvVncCntNum = cgiGet( "CONTRATOSRVVNC_SRVVNCCNTNUM");
               n1631ContratoSrvVnc_SrvVncCntNum = false;
               A924ContratoSrvVnc_ServicoVncSigla = cgiGet( "CONTRATOSRVVNC_SERVICOVNCSIGLA");
               n924ContratoSrvVnc_ServicoVncSigla = false;
               AV28Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosVnc";
               A917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A915ContratoSrvVnc_CntSrvCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A917ContratoSrvVnc_Codigo != Z917ContratoSrvVnc_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[SecurityCheckFailed value for]"+"ContratoSrvVnc_Codigo:"+context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratoservicosvnc:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoservicosvnc:[SecurityCheckFailed value for]"+"ContratoSrvVnc_CntSrvCod:"+context.localUtil.Format( (decimal)(A915ContratoSrvVnc_CntSrvCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A917ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode114 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode114;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound114 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2V0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOSRVVNC_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratoSrvVnc_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112V2 */
                           E112V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122V2 */
                           E122V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTRATOSRVVNC_PRESTADORACOD.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E132V2 */
                           E132V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VSELCONTRATO_CODIGO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E142V2 */
                           E142V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122V2 */
            E122V2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2V114( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2V114( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontrato_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelcontrato_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelservico_codigo.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2V0( )
      {
         BeforeValidate2V114( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2V114( ) ;
            }
            else
            {
               CheckExtendedTable2V114( ) ;
               CloseExtendedTableCursors2V114( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2V0( )
      {
      }

      protected void E112V2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV28Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV29GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GXV1), 8, 0)));
            while ( AV29GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV29GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoSrvVnc_CntSrvCod") == 0 )
               {
                  AV11Insert_ContratoSrvVnc_CntSrvCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoSrvVnc_CntSrvCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoSrvVnc_SrvVncCntSrvCod") == 0 )
               {
                  AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod), 6, 0)));
               }
               AV29GXV1 = (int)(AV29GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GXV1), 8, 0)));
            }
         }
         edtContratoSrvVnc_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSrvVnc_Codigo_Visible), 5, 0)));
         new prc_variaveiscontratoservicosvnc(context ).execute( ref  AV7ContratoSrvVnc_Codigo, ref  AV22SelServico_Codigo, ref  AV20SelContrato_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoSrvVnc_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSRVVNC_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoSrvVnc_Codigo), "ZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
         /* Execute user subroutine: 'CARREGAACOES' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E122V2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV16ContratoSrvVnc_CntSrvCod) + "," + UrlEncode(StringUtil.RTrim("Regras"));
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV16ContratoSrvVnc_CntSrvCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         if ( false )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwcontratoservicosvnc.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {(int)AV16ContratoSrvVnc_CntSrvCod});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E132V2( )
      {
         /* ContratoSrvVnc_PrestadoraCod_Click Routine */
         if ( A1088ContratoSrvVnc_PrestadoraCod > 0 )
         {
            GXt_int1 = AV20SelContrato_Codigo;
            new prc_contratodoprestador(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            AV20SelContrato_Codigo = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
         }
         else
         {
            AV20SelContrato_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
         }
         /* Execute user subroutine: 'SERVICODOCONTRATO' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         dynavSelcontrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontrato_codigo_Internalname, "Values", dynavSelcontrato_codigo.ToJavascriptSource());
         dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Values", dynavSelservico_codigo.ToJavascriptSource());
      }

      protected void E142V2( )
      {
         /* Selcontrato_codigo_Click Routine */
         /* Execute user subroutine: 'SERVICODOCONTRATO' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Values", dynavSelservico_codigo.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'CARREGAACOES' Routine */
         GXt_int1 = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         new prc_carregacontratadas(context ).execute( ref  GXt_int1, out  AV24Codigos, out  AV26Descricoes, out  AV27TipoFabrica) ;
         AV8WWPContext.gxTpr_Areatrabalho_codigo = GXt_int1;
         cmbContratoSrvVnc_PrestadoraCod.addItem("910000", "Voltar para o criador da SS", 0);
         cmbContratoSrvVnc_PrestadoraCod.addItem("920000", "Voltar para o criador da OS", 0);
         AV25i = 1;
         while ( AV25i <= AV24Codigos.Count )
         {
            AV23Descricao = ((String)AV26Descricoes.Item(AV25i));
            if ( StringUtil.StrCmp(AV27TipoFabrica.GetString(AV25i), "I") == 0 )
            {
               AV23Descricao = AV23Descricao + " (Tarefa)";
            }
            else
            {
               AV23Descricao = AV23Descricao + " (OS)";
            }
            cmbContratoSrvVnc_PrestadoraCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV24Codigos.GetNumeric(AV25i)), 6, 0)), AV23Descricao, 0);
            AV25i = (short)(AV25i+1);
         }
      }

      protected void S122( )
      {
         /* 'SERVICODOCONTRATO' Routine */
         if ( AV20SelContrato_Codigo > 0 )
         {
            GXt_int1 = AV22SelServico_Codigo;
            new prc_servicodocontrato(context ).execute( ref  AV20SelContrato_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
            AV22SelServico_Codigo = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
         }
      }

      protected void ZM2V114( short GX_JID )
      {
         if ( ( GX_JID == 39 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1801ContratoSrvVnc_NovoRspDmn = T002V3_A1801ContratoSrvVnc_NovoRspDmn[0];
               Z1743ContratoSrvVnc_NovoStatusDmn = T002V3_A1743ContratoSrvVnc_NovoStatusDmn[0];
               Z2108ContratoServicosVnc_Descricao = T002V3_A2108ContratoServicosVnc_Descricao[0];
               Z1663ContratoSrvVnc_SrvVncStatus = T002V3_A1663ContratoSrvVnc_SrvVncStatus[0];
               Z1800ContratoSrvVnc_DoStatusDmn = T002V3_A1800ContratoSrvVnc_DoStatusDmn[0];
               Z1084ContratoSrvVnc_StatusDmn = T002V3_A1084ContratoSrvVnc_StatusDmn[0];
               Z1745ContratoSrvVnc_VincularCom = T002V3_A1745ContratoSrvVnc_VincularCom[0];
               Z1821ContratoSrvVnc_SrvVncRef = T002V3_A1821ContratoSrvVnc_SrvVncRef[0];
               Z1088ContratoSrvVnc_PrestadoraCod = T002V3_A1088ContratoSrvVnc_PrestadoraCod[0];
               Z1090ContratoSrvVnc_SemCusto = T002V3_A1090ContratoSrvVnc_SemCusto[0];
               Z1145ContratoServicosVnc_ClonaSrvOri = T002V3_A1145ContratoServicosVnc_ClonaSrvOri[0];
               Z1437ContratoServicosVnc_NaoClonaInfo = T002V3_A1437ContratoServicosVnc_NaoClonaInfo[0];
               Z1438ContratoServicosVnc_Gestor = T002V3_A1438ContratoServicosVnc_Gestor[0];
               Z1818ContratoServicosVnc_ClonarLink = T002V3_A1818ContratoServicosVnc_ClonarLink[0];
               Z1453ContratoServicosVnc_Ativo = T002V3_A1453ContratoServicosVnc_Ativo[0];
               Z915ContratoSrvVnc_CntSrvCod = T002V3_A915ContratoSrvVnc_CntSrvCod[0];
               Z1589ContratoSrvVnc_SrvVncCntSrvCod = T002V3_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            }
            else
            {
               Z1801ContratoSrvVnc_NovoRspDmn = A1801ContratoSrvVnc_NovoRspDmn;
               Z1743ContratoSrvVnc_NovoStatusDmn = A1743ContratoSrvVnc_NovoStatusDmn;
               Z2108ContratoServicosVnc_Descricao = A2108ContratoServicosVnc_Descricao;
               Z1663ContratoSrvVnc_SrvVncStatus = A1663ContratoSrvVnc_SrvVncStatus;
               Z1800ContratoSrvVnc_DoStatusDmn = A1800ContratoSrvVnc_DoStatusDmn;
               Z1084ContratoSrvVnc_StatusDmn = A1084ContratoSrvVnc_StatusDmn;
               Z1745ContratoSrvVnc_VincularCom = A1745ContratoSrvVnc_VincularCom;
               Z1821ContratoSrvVnc_SrvVncRef = A1821ContratoSrvVnc_SrvVncRef;
               Z1088ContratoSrvVnc_PrestadoraCod = A1088ContratoSrvVnc_PrestadoraCod;
               Z1090ContratoSrvVnc_SemCusto = A1090ContratoSrvVnc_SemCusto;
               Z1145ContratoServicosVnc_ClonaSrvOri = A1145ContratoServicosVnc_ClonaSrvOri;
               Z1437ContratoServicosVnc_NaoClonaInfo = A1437ContratoServicosVnc_NaoClonaInfo;
               Z1438ContratoServicosVnc_Gestor = A1438ContratoServicosVnc_Gestor;
               Z1818ContratoServicosVnc_ClonarLink = A1818ContratoServicosVnc_ClonarLink;
               Z1453ContratoServicosVnc_Ativo = A1453ContratoServicosVnc_Ativo;
               Z915ContratoSrvVnc_CntSrvCod = A915ContratoSrvVnc_CntSrvCod;
               Z1589ContratoSrvVnc_SrvVncCntSrvCod = A1589ContratoSrvVnc_SrvVncCntSrvCod;
            }
         }
         if ( GX_JID == -39 )
         {
            Z917ContratoSrvVnc_Codigo = A917ContratoSrvVnc_Codigo;
            Z1801ContratoSrvVnc_NovoRspDmn = A1801ContratoSrvVnc_NovoRspDmn;
            Z1743ContratoSrvVnc_NovoStatusDmn = A1743ContratoSrvVnc_NovoStatusDmn;
            Z2108ContratoServicosVnc_Descricao = A2108ContratoServicosVnc_Descricao;
            Z1663ContratoSrvVnc_SrvVncStatus = A1663ContratoSrvVnc_SrvVncStatus;
            Z1800ContratoSrvVnc_DoStatusDmn = A1800ContratoSrvVnc_DoStatusDmn;
            Z1084ContratoSrvVnc_StatusDmn = A1084ContratoSrvVnc_StatusDmn;
            Z1745ContratoSrvVnc_VincularCom = A1745ContratoSrvVnc_VincularCom;
            Z1821ContratoSrvVnc_SrvVncRef = A1821ContratoSrvVnc_SrvVncRef;
            Z1088ContratoSrvVnc_PrestadoraCod = A1088ContratoSrvVnc_PrestadoraCod;
            Z1090ContratoSrvVnc_SemCusto = A1090ContratoSrvVnc_SemCusto;
            Z1145ContratoServicosVnc_ClonaSrvOri = A1145ContratoServicosVnc_ClonaSrvOri;
            Z1437ContratoServicosVnc_NaoClonaInfo = A1437ContratoServicosVnc_NaoClonaInfo;
            Z1438ContratoServicosVnc_Gestor = A1438ContratoServicosVnc_Gestor;
            Z1818ContratoServicosVnc_ClonarLink = A1818ContratoServicosVnc_ClonarLink;
            Z1453ContratoServicosVnc_Ativo = A1453ContratoServicosVnc_Ativo;
            Z915ContratoSrvVnc_CntSrvCod = A915ContratoSrvVnc_CntSrvCod;
            Z1589ContratoSrvVnc_SrvVncCntSrvCod = A1589ContratoSrvVnc_SrvVncCntSrvCod;
            Z933ContratoSrvVnc_ContratoCod = A933ContratoSrvVnc_ContratoCod;
            Z921ContratoSrvVnc_ServicoCod = A921ContratoSrvVnc_ServicoCod;
            Z1662ContratoSrvVnc_ContratoAreaCod = A1662ContratoSrvVnc_ContratoAreaCod;
            Z922ContratoSrvVnc_ServicoSigla = A922ContratoSrvVnc_ServicoSigla;
            Z1629ContratoSrvVnc_SrvVncTpVnc = A1629ContratoSrvVnc_SrvVncTpVnc;
            Z1630ContratoSrvVnc_SrvVncTpDias = A1630ContratoSrvVnc_SrvVncTpDias;
            Z1642ContratoSrvVnc_SrvVncVlrUndCnt = A1642ContratoSrvVnc_SrvVncVlrUndCnt;
            Z1651ContratoSrvVnc_SrvVncPrzInc = A1651ContratoSrvVnc_SrvVncPrzInc;
            Z1628ContratoSrvVnc_SrvVncCntCod = A1628ContratoSrvVnc_SrvVncCntCod;
            Z923ContratoSrvVnc_ServicoVncCod = A923ContratoSrvVnc_ServicoVncCod;
            Z1631ContratoSrvVnc_SrvVncCntNum = A1631ContratoSrvVnc_SrvVncCntNum;
            Z924ContratoSrvVnc_ServicoVncSigla = A924ContratoSrvVnc_ServicoVncSigla;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratoSrvVnc_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSrvVnc_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV28Pgmname = "ContratoServicosVnc";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Pgmname", AV28Pgmname);
         edtContratoSrvVnc_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSrvVnc_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoSrvVnc_Codigo) )
         {
            A917ContratoSrvVnc_Codigo = AV7ContratoSrvVnc_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
         }
         GXVvSELSERVICO_CODIGO_html2V114( AV20SelContrato_Codigo) ;
      }

      protected void standaloneModal( )
      {
         chkContratoServicosVnc_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosVnc_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicosVnc_Ativo.Visible), 5, 0)));
         lblTextblockcontratoservicosvnc_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosvnc_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosvnc_ativo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod) )
         {
            A1589ContratoSrvVnc_SrvVncCntSrvCod = AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod;
            n1589ContratoSrvVnc_SrvVncCntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoSrvVnc_CntSrvCod) )
         {
            A915ContratoSrvVnc_CntSrvCod = AV11Insert_ContratoSrvVnc_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A915ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1453ContratoServicosVnc_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A1453ContratoServicosVnc_Ativo = true;
            n1453ContratoServicosVnc_Ativo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom)) && ( Gx_BScreen == 0 ) )
         {
            A1745ContratoSrvVnc_VincularCom = "D";
            n1745ContratoSrvVnc_VincularCom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus)) && ( Gx_BScreen == 0 ) )
         {
            A1663ContratoSrvVnc_SrvVncStatus = "S";
            n1663ContratoSrvVnc_SrvVncStatus = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002V11 */
            pr_default.execute(5, new Object[] {n1589ContratoSrvVnc_SrvVncCntSrvCod, A1589ContratoSrvVnc_SrvVncCntSrvCod});
            A1629ContratoSrvVnc_SrvVncTpVnc = T002V11_A1629ContratoSrvVnc_SrvVncTpVnc[0];
            n1629ContratoSrvVnc_SrvVncTpVnc = T002V11_n1629ContratoSrvVnc_SrvVncTpVnc[0];
            A1630ContratoSrvVnc_SrvVncTpDias = T002V11_A1630ContratoSrvVnc_SrvVncTpDias[0];
            n1630ContratoSrvVnc_SrvVncTpDias = T002V11_n1630ContratoSrvVnc_SrvVncTpDias[0];
            A1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V11_A1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            n1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V11_n1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            A1651ContratoSrvVnc_SrvVncPrzInc = T002V11_A1651ContratoSrvVnc_SrvVncPrzInc[0];
            n1651ContratoSrvVnc_SrvVncPrzInc = T002V11_n1651ContratoSrvVnc_SrvVncPrzInc[0];
            A1628ContratoSrvVnc_SrvVncCntCod = T002V11_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = T002V11_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = T002V11_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = T002V11_n923ContratoSrvVnc_ServicoVncCod[0];
            pr_default.close(5);
            /* Using cursor T002V14 */
            pr_default.execute(8, new Object[] {n1628ContratoSrvVnc_SrvVncCntCod, A1628ContratoSrvVnc_SrvVncCntCod});
            A1631ContratoSrvVnc_SrvVncCntNum = T002V14_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = T002V14_n1631ContratoSrvVnc_SrvVncCntNum[0];
            pr_default.close(8);
            /* Using cursor T002V15 */
            pr_default.execute(9, new Object[] {n923ContratoSrvVnc_ServicoVncCod, A923ContratoSrvVnc_ServicoVncCod});
            A924ContratoSrvVnc_ServicoVncSigla = T002V15_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = T002V15_n924ContratoSrvVnc_ServicoVncSigla[0];
            pr_default.close(9);
            /* Using cursor T002V10 */
            pr_default.execute(4, new Object[] {A915ContratoSrvVnc_CntSrvCod});
            A933ContratoSrvVnc_ContratoCod = T002V10_A933ContratoSrvVnc_ContratoCod[0];
            n933ContratoSrvVnc_ContratoCod = T002V10_n933ContratoSrvVnc_ContratoCod[0];
            A921ContratoSrvVnc_ServicoCod = T002V10_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = T002V10_n921ContratoSrvVnc_ServicoCod[0];
            pr_default.close(4);
            /* Using cursor T002V12 */
            pr_default.execute(6, new Object[] {n933ContratoSrvVnc_ContratoCod, A933ContratoSrvVnc_ContratoCod});
            A1662ContratoSrvVnc_ContratoAreaCod = T002V12_A1662ContratoSrvVnc_ContratoAreaCod[0];
            n1662ContratoSrvVnc_ContratoAreaCod = T002V12_n1662ContratoSrvVnc_ContratoAreaCod[0];
            pr_default.close(6);
            /* Using cursor T002V13 */
            pr_default.execute(7, new Object[] {n921ContratoSrvVnc_ServicoCod, A921ContratoSrvVnc_ServicoCod});
            A922ContratoSrvVnc_ServicoSigla = T002V13_A922ContratoSrvVnc_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
            n922ContratoSrvVnc_ServicoSigla = T002V13_n922ContratoSrvVnc_ServicoSigla[0];
            pr_default.close(7);
         }
      }

      protected void Load2V114( )
      {
         /* Using cursor T002V16 */
         pr_default.execute(10, new Object[] {A917ContratoSrvVnc_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound114 = 1;
            A1801ContratoSrvVnc_NovoRspDmn = T002V16_A1801ContratoSrvVnc_NovoRspDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            n1801ContratoSrvVnc_NovoRspDmn = T002V16_n1801ContratoSrvVnc_NovoRspDmn[0];
            A1743ContratoSrvVnc_NovoStatusDmn = T002V16_A1743ContratoSrvVnc_NovoStatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            n1743ContratoSrvVnc_NovoStatusDmn = T002V16_n1743ContratoSrvVnc_NovoStatusDmn[0];
            A2108ContratoServicosVnc_Descricao = T002V16_A2108ContratoServicosVnc_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2108ContratoServicosVnc_Descricao", A2108ContratoServicosVnc_Descricao);
            n2108ContratoServicosVnc_Descricao = T002V16_n2108ContratoServicosVnc_Descricao[0];
            A922ContratoSrvVnc_ServicoSigla = T002V16_A922ContratoSrvVnc_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
            n922ContratoSrvVnc_ServicoSigla = T002V16_n922ContratoSrvVnc_ServicoSigla[0];
            A1631ContratoSrvVnc_SrvVncCntNum = T002V16_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = T002V16_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = T002V16_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = T002V16_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1629ContratoSrvVnc_SrvVncTpVnc = T002V16_A1629ContratoSrvVnc_SrvVncTpVnc[0];
            n1629ContratoSrvVnc_SrvVncTpVnc = T002V16_n1629ContratoSrvVnc_SrvVncTpVnc[0];
            A1630ContratoSrvVnc_SrvVncTpDias = T002V16_A1630ContratoSrvVnc_SrvVncTpDias[0];
            n1630ContratoSrvVnc_SrvVncTpDias = T002V16_n1630ContratoSrvVnc_SrvVncTpDias[0];
            A1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V16_A1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            n1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V16_n1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            A1651ContratoSrvVnc_SrvVncPrzInc = T002V16_A1651ContratoSrvVnc_SrvVncPrzInc[0];
            n1651ContratoSrvVnc_SrvVncPrzInc = T002V16_n1651ContratoSrvVnc_SrvVncPrzInc[0];
            A1663ContratoSrvVnc_SrvVncStatus = T002V16_A1663ContratoSrvVnc_SrvVncStatus[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
            n1663ContratoSrvVnc_SrvVncStatus = T002V16_n1663ContratoSrvVnc_SrvVncStatus[0];
            A1800ContratoSrvVnc_DoStatusDmn = T002V16_A1800ContratoSrvVnc_DoStatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
            n1800ContratoSrvVnc_DoStatusDmn = T002V16_n1800ContratoSrvVnc_DoStatusDmn[0];
            A1084ContratoSrvVnc_StatusDmn = T002V16_A1084ContratoSrvVnc_StatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
            n1084ContratoSrvVnc_StatusDmn = T002V16_n1084ContratoSrvVnc_StatusDmn[0];
            A1745ContratoSrvVnc_VincularCom = T002V16_A1745ContratoSrvVnc_VincularCom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
            n1745ContratoSrvVnc_VincularCom = T002V16_n1745ContratoSrvVnc_VincularCom[0];
            A1821ContratoSrvVnc_SrvVncRef = T002V16_A1821ContratoSrvVnc_SrvVncRef[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
            n1821ContratoSrvVnc_SrvVncRef = T002V16_n1821ContratoSrvVnc_SrvVncRef[0];
            A1088ContratoSrvVnc_PrestadoraCod = T002V16_A1088ContratoSrvVnc_PrestadoraCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            n1088ContratoSrvVnc_PrestadoraCod = T002V16_n1088ContratoSrvVnc_PrestadoraCod[0];
            A1090ContratoSrvVnc_SemCusto = T002V16_A1090ContratoSrvVnc_SemCusto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
            n1090ContratoSrvVnc_SemCusto = T002V16_n1090ContratoSrvVnc_SemCusto[0];
            A1145ContratoServicosVnc_ClonaSrvOri = T002V16_A1145ContratoServicosVnc_ClonaSrvOri[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
            n1145ContratoServicosVnc_ClonaSrvOri = T002V16_n1145ContratoServicosVnc_ClonaSrvOri[0];
            A1437ContratoServicosVnc_NaoClonaInfo = T002V16_A1437ContratoServicosVnc_NaoClonaInfo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1437ContratoServicosVnc_NaoClonaInfo", A1437ContratoServicosVnc_NaoClonaInfo);
            n1437ContratoServicosVnc_NaoClonaInfo = T002V16_n1437ContratoServicosVnc_NaoClonaInfo[0];
            A1438ContratoServicosVnc_Gestor = T002V16_A1438ContratoServicosVnc_Gestor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
            n1438ContratoServicosVnc_Gestor = T002V16_n1438ContratoServicosVnc_Gestor[0];
            A1818ContratoServicosVnc_ClonarLink = T002V16_A1818ContratoServicosVnc_ClonarLink[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
            n1818ContratoServicosVnc_ClonarLink = T002V16_n1818ContratoServicosVnc_ClonarLink[0];
            A1453ContratoServicosVnc_Ativo = T002V16_A1453ContratoServicosVnc_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
            n1453ContratoServicosVnc_Ativo = T002V16_n1453ContratoServicosVnc_Ativo[0];
            A915ContratoSrvVnc_CntSrvCod = T002V16_A915ContratoSrvVnc_CntSrvCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = T002V16_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = T002V16_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A933ContratoSrvVnc_ContratoCod = T002V16_A933ContratoSrvVnc_ContratoCod[0];
            n933ContratoSrvVnc_ContratoCod = T002V16_n933ContratoSrvVnc_ContratoCod[0];
            A1662ContratoSrvVnc_ContratoAreaCod = T002V16_A1662ContratoSrvVnc_ContratoAreaCod[0];
            n1662ContratoSrvVnc_ContratoAreaCod = T002V16_n1662ContratoSrvVnc_ContratoAreaCod[0];
            A921ContratoSrvVnc_ServicoCod = T002V16_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = T002V16_n921ContratoSrvVnc_ServicoCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = T002V16_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = T002V16_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = T002V16_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = T002V16_n923ContratoSrvVnc_ServicoVncCod[0];
            ZM2V114( -39) ;
         }
         pr_default.close(10);
         OnLoadActions2V114( ) ;
      }

      protected void OnLoadActions2V114( )
      {
         /* Using cursor T002V6 */
         pr_default.execute(2, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = T002V6_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = T002V6_n1092ContratoSrvVnc_PrestadoraPesNom[0];
         }
         else
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = "";
            n1092ContratoSrvVnc_PrestadoraPesNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1092ContratoSrvVnc_PrestadoraPesNom", A1092ContratoSrvVnc_PrestadoraPesNom);
         }
         pr_default.close(2);
         /* Using cursor T002V9 */
         pr_default.execute(3, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = T002V9_A1744ContratoSrvVnc_PrestadoraTpFab[0];
            n1744ContratoSrvVnc_PrestadoraTpFab = T002V9_n1744ContratoSrvVnc_PrestadoraTpFab[0];
         }
         else
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = "";
            n1744ContratoSrvVnc_PrestadoraTpFab = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1744ContratoSrvVnc_PrestadoraTpFab", A1744ContratoSrvVnc_PrestadoraTpFab);
         }
         pr_default.close(3);
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            A1743ContratoSrvVnc_NovoStatusDmn = "";
            n1743ContratoSrvVnc_NovoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            n1743ContratoSrvVnc_NovoStatusDmn = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               A1743ContratoSrvVnc_NovoStatusDmn = A1663ContratoSrvVnc_SrvVncStatus;
               n1743ContratoSrvVnc_NovoStatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            }
         }
         GXVvSELCONTRATO_CODIGO_html2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( A1589ContratoSrvVnc_SrvVncCntSrvCod > 0 ) )
         {
            AV22SelServico_Codigo = A1589ContratoSrvVnc_SrvVncCntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
         }
         else
         {
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0] )
            {
               AV22SelServico_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  AV22SelServico_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
               }
            }
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0] )
         {
            AV20SelContrato_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               AV20SelContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
            }
         }
         GXt_char2 = "";
         new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
         if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ! ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
         {
            cmbContratoServicosVnc_Gestor.addItem("900000", "Gestor do Contrato", 0);
            n1438ContratoServicosVnc_Gestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
         }
         else
         {
            GXt_char2 = "";
            new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
            {
               cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
               n1438ContratoServicosVnc_Gestor = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
                  n1438ContratoServicosVnc_Gestor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
               }
            }
         }
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            A1801ContratoSrvVnc_NovoRspDmn = 0;
            n1801ContratoSrvVnc_NovoRspDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            n1801ContratoSrvVnc_NovoRspDmn = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               A1801ContratoSrvVnc_NovoRspDmn = A1088ContratoSrvVnc_PrestadoraCod;
               n1801ContratoSrvVnc_NovoRspDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            }
         }
         cmbContratoSrvVnc_NovoRspDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoRspDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoRspDmn.Enabled), 5, 0)));
         cmbContratoSrvVnc_NovoStatusDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoStatusDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoStatusDmn.Enabled), 5, 0)));
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            lblTextblockcontratosrvvnc_vincularcom_Caption = "Vinculada com";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratosrvvnc_vincularcom_Internalname, "Caption", lblTextblockcontratosrvvnc_vincularcom_Caption);
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               lblTextblockcontratosrvvnc_vincularcom_Caption = "Voltar";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratosrvvnc_vincularcom_Internalname, "Caption", lblTextblockcontratosrvvnc_vincularcom_Caption);
            }
         }
         lblTextblockselcontrato_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockselcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockselcontrato_codigo_Visible), 5, 0)));
         dynavSelcontrato_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelcontrato_codigo.Visible), 5, 0)));
         lblTextblockselservico_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockselservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockselservico_codigo_Visible), 5, 0)));
         dynavSelservico_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelservico_codigo.Visible), 5, 0)));
      }

      protected void CheckExtendedTable2V114( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( A1088ContratoSrvVnc_PrestadoraCod < 900000 ) && ! new prc_temgestor(context).executeUdp( ref  AV20SelContrato_Codigo) )
         {
            GX_msglist.addItem("O contrato selecionado n�o tem Preposto/Gestor estabelecido!", 1, "CONTRATOSRVVNC_PRESTADORACOD");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_PrestadoraCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002V6 */
         pr_default.execute(2, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = T002V6_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = T002V6_n1092ContratoSrvVnc_PrestadoraPesNom[0];
         }
         else
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = "";
            n1092ContratoSrvVnc_PrestadoraPesNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1092ContratoSrvVnc_PrestadoraPesNom", A1092ContratoSrvVnc_PrestadoraPesNom);
         }
         pr_default.close(2);
         /* Using cursor T002V9 */
         pr_default.execute(3, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = T002V9_A1744ContratoSrvVnc_PrestadoraTpFab[0];
            n1744ContratoSrvVnc_PrestadoraTpFab = T002V9_n1744ContratoSrvVnc_PrestadoraTpFab[0];
         }
         else
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = "";
            n1744ContratoSrvVnc_PrestadoraTpFab = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1744ContratoSrvVnc_PrestadoraTpFab", A1744ContratoSrvVnc_PrestadoraTpFab);
         }
         pr_default.close(3);
         if ( ! ( ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "B") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "S") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "E") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "A") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "R") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "C") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "D") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "H") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "O") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "P") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "L") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "X") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "N") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "J") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "I") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "T") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "Q") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "G") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "M") == 0 ) || ( StringUtil.StrCmp(A1663ContratoSrvVnc_SrvVncStatus, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus)) ) )
         {
            GX_msglist.addItem("Campo Com o Status fora do intervalo", "OutOfRange", 1, "CONTRATOSRVVNC_SRVVNCSTATUS");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_SrvVncStatus_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            A1743ContratoSrvVnc_NovoStatusDmn = "";
            n1743ContratoSrvVnc_NovoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            n1743ContratoSrvVnc_NovoStatusDmn = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               A1743ContratoSrvVnc_NovoStatusDmn = A1663ContratoSrvVnc_SrvVncStatus;
               n1743ContratoSrvVnc_NovoStatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus)) )
         {
            GX_msglist.addItem("Com o Status � obrigat�rio.", 1, "CONTRATOSRVVNC_SRVVNCSTATUS");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_SrvVncStatus_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "B") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "E") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "A") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "C") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "H") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "X") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "N") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "J") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "I") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "T") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "Q") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "G") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "M") == 0 ) || ( StringUtil.StrCmp(A1800ContratoSrvVnc_DoStatusDmn, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn)) ) )
         {
            GX_msglist.addItem("Campo Do status fora do intervalo", "OutOfRange", 1, "CONTRATOSRVVNC_DOSTATUSDMN");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_DoStatusDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "B") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "E") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "A") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "C") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "H") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "X") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "N") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "J") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "I") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "T") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "Q") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "G") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "M") == 0 ) || ( StringUtil.StrCmp(A1084ContratoSrvVnc_StatusDmn, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn)) ) )
         {
            GX_msglist.addItem("Campo No status fora do intervalo", "OutOfRange", 1, "CONTRATOSRVVNC_STATUSDMN");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_StatusDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn)) )
         {
            GX_msglist.addItem("No status � obrigat�rio.", 1, "CONTRATOSRVVNC_STATUSDMN");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_StatusDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom)) )
         {
            GX_msglist.addItem("Vinculada com � obrigat�rio.", 1, "CONTRATOSRVVNC_VINCULARCOM");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_VincularCom_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "B") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "E") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "A") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "C") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "H") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "X") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "N") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "J") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "I") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "T") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "Q") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "G") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "M") == 0 ) || ( StringUtil.StrCmp(A1743ContratoSrvVnc_NovoStatusDmn, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn)) ) )
         {
            GX_msglist.addItem("Campo Deixando-a no Status fora do intervalo", "OutOfRange", 1, "CONTRATOSRVVNC_NOVOSTATUSDMN");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_NovoStatusDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GXVvSELCONTRATO_CODIGO_html2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( A1589ContratoSrvVnc_SrvVncCntSrvCod > 0 ) )
         {
            AV22SelServico_Codigo = A1589ContratoSrvVnc_SrvVncCntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
         }
         else
         {
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0] )
            {
               AV22SelServico_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  AV22SelServico_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
               }
            }
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0] )
         {
            AV20SelContrato_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               AV20SelContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
            }
         }
         GXt_char2 = "";
         new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
         if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ! ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
         {
            cmbContratoServicosVnc_Gestor.addItem("900000", "Gestor do Contrato", 0);
            n1438ContratoServicosVnc_Gestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
         }
         else
         {
            GXt_char2 = "";
            new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
            {
               cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
               n1438ContratoServicosVnc_Gestor = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
                  n1438ContratoServicosVnc_Gestor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
               }
            }
         }
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            A1801ContratoSrvVnc_NovoRspDmn = 0;
            n1801ContratoSrvVnc_NovoRspDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            n1801ContratoSrvVnc_NovoRspDmn = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               A1801ContratoSrvVnc_NovoRspDmn = A1088ContratoSrvVnc_PrestadoraCod;
               n1801ContratoSrvVnc_NovoRspDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            }
         }
         cmbContratoSrvVnc_NovoRspDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoRspDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoRspDmn.Enabled), 5, 0)));
         cmbContratoSrvVnc_NovoStatusDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoStatusDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoStatusDmn.Enabled), 5, 0)));
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            lblTextblockcontratosrvvnc_vincularcom_Caption = "Vinculada com";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratosrvvnc_vincularcom_Internalname, "Caption", lblTextblockcontratosrvvnc_vincularcom_Caption);
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               lblTextblockcontratosrvvnc_vincularcom_Caption = "Voltar";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratosrvvnc_vincularcom_Internalname, "Caption", lblTextblockcontratosrvvnc_vincularcom_Caption);
            }
         }
         lblTextblockselcontrato_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockselcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockselcontrato_codigo_Visible), 5, 0)));
         dynavSelcontrato_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelcontrato_codigo.Visible), 5, 0)));
         lblTextblockselservico_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockselservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockselservico_codigo_Visible), 5, 0)));
         dynavSelservico_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelservico_codigo.Visible), 5, 0)));
         if ( (0==A1088ContratoSrvVnc_PrestadoraCod) )
         {
            GX_msglist.addItem("A��o � obrigat�rio.", 1, "CONTRATOSRVVNC_PRESTADORACOD");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_PrestadoraCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A1438ContratoServicosVnc_Gestor) )
         {
            GX_msglist.addItem("Emissor � obrigat�rio.", 1, "CONTRATOSERVICOSVNC_GESTOR");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicosVnc_Gestor_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002V10 */
         pr_default.execute(4, new Object[] {A915ContratoSrvVnc_CntSrvCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_ContratoServicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A933ContratoSrvVnc_ContratoCod = T002V10_A933ContratoSrvVnc_ContratoCod[0];
         n933ContratoSrvVnc_ContratoCod = T002V10_n933ContratoSrvVnc_ContratoCod[0];
         A921ContratoSrvVnc_ServicoCod = T002V10_A921ContratoSrvVnc_ServicoCod[0];
         n921ContratoSrvVnc_ServicoCod = T002V10_n921ContratoSrvVnc_ServicoCod[0];
         pr_default.close(4);
         /* Using cursor T002V12 */
         pr_default.execute(6, new Object[] {n933ContratoSrvVnc_ContratoCod, A933ContratoSrvVnc_ContratoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_ContratoServicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1662ContratoSrvVnc_ContratoAreaCod = T002V12_A1662ContratoSrvVnc_ContratoAreaCod[0];
         n1662ContratoSrvVnc_ContratoAreaCod = T002V12_n1662ContratoSrvVnc_ContratoAreaCod[0];
         pr_default.close(6);
         /* Using cursor T002V13 */
         pr_default.execute(7, new Object[] {n921ContratoSrvVnc_ServicoCod, A921ContratoSrvVnc_ServicoCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_ContratoServicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A922ContratoSrvVnc_ServicoSigla = T002V13_A922ContratoSrvVnc_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
         n922ContratoSrvVnc_ServicoSigla = T002V13_n922ContratoSrvVnc_ServicoSigla[0];
         pr_default.close(7);
         /* Using cursor T002V11 */
         pr_default.execute(5, new Object[] {n1589ContratoSrvVnc_SrvVncCntSrvCod, A1589ContratoSrvVnc_SrvVncCntSrvCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1589ContratoSrvVnc_SrvVncCntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_Srv Vnc Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1629ContratoSrvVnc_SrvVncTpVnc = T002V11_A1629ContratoSrvVnc_SrvVncTpVnc[0];
         n1629ContratoSrvVnc_SrvVncTpVnc = T002V11_n1629ContratoSrvVnc_SrvVncTpVnc[0];
         A1630ContratoSrvVnc_SrvVncTpDias = T002V11_A1630ContratoSrvVnc_SrvVncTpDias[0];
         n1630ContratoSrvVnc_SrvVncTpDias = T002V11_n1630ContratoSrvVnc_SrvVncTpDias[0];
         A1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V11_A1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
         n1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V11_n1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
         A1651ContratoSrvVnc_SrvVncPrzInc = T002V11_A1651ContratoSrvVnc_SrvVncPrzInc[0];
         n1651ContratoSrvVnc_SrvVncPrzInc = T002V11_n1651ContratoSrvVnc_SrvVncPrzInc[0];
         A1628ContratoSrvVnc_SrvVncCntCod = T002V11_A1628ContratoSrvVnc_SrvVncCntCod[0];
         n1628ContratoSrvVnc_SrvVncCntCod = T002V11_n1628ContratoSrvVnc_SrvVncCntCod[0];
         A923ContratoSrvVnc_ServicoVncCod = T002V11_A923ContratoSrvVnc_ServicoVncCod[0];
         n923ContratoSrvVnc_ServicoVncCod = T002V11_n923ContratoSrvVnc_ServicoVncCod[0];
         pr_default.close(5);
         /* Using cursor T002V14 */
         pr_default.execute(8, new Object[] {n1628ContratoSrvVnc_SrvVncCntCod, A1628ContratoSrvVnc_SrvVncCntCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A1628ContratoSrvVnc_SrvVncCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_Srv Vnc Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1631ContratoSrvVnc_SrvVncCntNum = T002V14_A1631ContratoSrvVnc_SrvVncCntNum[0];
         n1631ContratoSrvVnc_SrvVncCntNum = T002V14_n1631ContratoSrvVnc_SrvVncCntNum[0];
         pr_default.close(8);
         /* Using cursor T002V15 */
         pr_default.execute(9, new Object[] {n923ContratoSrvVnc_ServicoVncCod, A923ContratoSrvVnc_ServicoVncCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A923ContratoSrvVnc_ServicoVncCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_Srv Vnc Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A924ContratoSrvVnc_ServicoVncSigla = T002V15_A924ContratoSrvVnc_ServicoVncSigla[0];
         n924ContratoSrvVnc_ServicoVncSigla = T002V15_n924ContratoSrvVnc_ServicoVncSigla[0];
         pr_default.close(9);
      }

      protected void CloseExtendedTableCursors2V114( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(5);
         pr_default.close(8);
         pr_default.close(9);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_40( int A917ContratoSrvVnc_Codigo ,
                                int A1088ContratoSrvVnc_PrestadoraCod )
      {
         /* Using cursor T002V19 */
         pr_default.execute(11, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = T002V19_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = T002V19_n1092ContratoSrvVnc_PrestadoraPesNom[0];
         }
         else
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = "";
            n1092ContratoSrvVnc_PrestadoraPesNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1092ContratoSrvVnc_PrestadoraPesNom", A1092ContratoSrvVnc_PrestadoraPesNom);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_41( int A917ContratoSrvVnc_Codigo ,
                                int A1088ContratoSrvVnc_PrestadoraCod )
      {
         /* Using cursor T002V22 */
         pr_default.execute(12, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(12) != 101) )
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = T002V22_A1744ContratoSrvVnc_PrestadoraTpFab[0];
            n1744ContratoSrvVnc_PrestadoraTpFab = T002V22_n1744ContratoSrvVnc_PrestadoraTpFab[0];
         }
         else
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = "";
            n1744ContratoSrvVnc_PrestadoraTpFab = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1744ContratoSrvVnc_PrestadoraTpFab", A1744ContratoSrvVnc_PrestadoraTpFab);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1744ContratoSrvVnc_PrestadoraTpFab))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_42( int A915ContratoSrvVnc_CntSrvCod )
      {
         /* Using cursor T002V23 */
         pr_default.execute(13, new Object[] {A915ContratoSrvVnc_CntSrvCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_ContratoServicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A933ContratoSrvVnc_ContratoCod = T002V23_A933ContratoSrvVnc_ContratoCod[0];
         n933ContratoSrvVnc_ContratoCod = T002V23_n933ContratoSrvVnc_ContratoCod[0];
         A921ContratoSrvVnc_ServicoCod = T002V23_A921ContratoSrvVnc_ServicoCod[0];
         n921ContratoSrvVnc_ServicoCod = T002V23_n921ContratoSrvVnc_ServicoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A933ContratoSrvVnc_ContratoCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A921ContratoSrvVnc_ServicoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_44( int A933ContratoSrvVnc_ContratoCod )
      {
         /* Using cursor T002V24 */
         pr_default.execute(14, new Object[] {n933ContratoSrvVnc_ContratoCod, A933ContratoSrvVnc_ContratoCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_ContratoServicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1662ContratoSrvVnc_ContratoAreaCod = T002V24_A1662ContratoSrvVnc_ContratoAreaCod[0];
         n1662ContratoSrvVnc_ContratoAreaCod = T002V24_n1662ContratoSrvVnc_ContratoAreaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1662ContratoSrvVnc_ContratoAreaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_45( int A921ContratoSrvVnc_ServicoCod )
      {
         /* Using cursor T002V25 */
         pr_default.execute(15, new Object[] {n921ContratoSrvVnc_ServicoCod, A921ContratoSrvVnc_ServicoCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_ContratoServicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A922ContratoSrvVnc_ServicoSigla = T002V25_A922ContratoSrvVnc_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
         n922ContratoSrvVnc_ServicoSigla = T002V25_n922ContratoSrvVnc_ServicoSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_43( int A1589ContratoSrvVnc_SrvVncCntSrvCod )
      {
         /* Using cursor T002V26 */
         pr_default.execute(16, new Object[] {n1589ContratoSrvVnc_SrvVncCntSrvCod, A1589ContratoSrvVnc_SrvVncCntSrvCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A1589ContratoSrvVnc_SrvVncCntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_Srv Vnc Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1629ContratoSrvVnc_SrvVncTpVnc = T002V26_A1629ContratoSrvVnc_SrvVncTpVnc[0];
         n1629ContratoSrvVnc_SrvVncTpVnc = T002V26_n1629ContratoSrvVnc_SrvVncTpVnc[0];
         A1630ContratoSrvVnc_SrvVncTpDias = T002V26_A1630ContratoSrvVnc_SrvVncTpDias[0];
         n1630ContratoSrvVnc_SrvVncTpDias = T002V26_n1630ContratoSrvVnc_SrvVncTpDias[0];
         A1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V26_A1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
         n1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V26_n1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
         A1651ContratoSrvVnc_SrvVncPrzInc = T002V26_A1651ContratoSrvVnc_SrvVncPrzInc[0];
         n1651ContratoSrvVnc_SrvVncPrzInc = T002V26_n1651ContratoSrvVnc_SrvVncPrzInc[0];
         A1628ContratoSrvVnc_SrvVncCntCod = T002V26_A1628ContratoSrvVnc_SrvVncCntCod[0];
         n1628ContratoSrvVnc_SrvVncCntCod = T002V26_n1628ContratoSrvVnc_SrvVncCntCod[0];
         A923ContratoSrvVnc_ServicoVncCod = T002V26_A923ContratoSrvVnc_ServicoVncCod[0];
         n923ContratoSrvVnc_ServicoVncCod = T002V26_n923ContratoSrvVnc_ServicoVncCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1629ContratoSrvVnc_SrvVncTpVnc))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1630ContratoSrvVnc_SrvVncTpDias))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A1642ContratoSrvVnc_SrvVncVlrUndCnt, 18, 5, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1651ContratoSrvVnc_SrvVncPrzInc), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1628ContratoSrvVnc_SrvVncCntCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A923ContratoSrvVnc_ServicoVncCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void gxLoad_46( int A1628ContratoSrvVnc_SrvVncCntCod )
      {
         /* Using cursor T002V27 */
         pr_default.execute(17, new Object[] {n1628ContratoSrvVnc_SrvVncCntCod, A1628ContratoSrvVnc_SrvVncCntCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A1628ContratoSrvVnc_SrvVncCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_Srv Vnc Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1631ContratoSrvVnc_SrvVncCntNum = T002V27_A1631ContratoSrvVnc_SrvVncCntNum[0];
         n1631ContratoSrvVnc_SrvVncCntNum = T002V27_n1631ContratoSrvVnc_SrvVncCntNum[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void gxLoad_47( int A923ContratoSrvVnc_ServicoVncCod )
      {
         /* Using cursor T002V28 */
         pr_default.execute(18, new Object[] {n923ContratoSrvVnc_ServicoVncCod, A923ContratoSrvVnc_ServicoVncCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            if ( ! ( (0==A923ContratoSrvVnc_ServicoVncCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Vnc_Srv Vnc Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A924ContratoSrvVnc_ServicoVncSigla = T002V28_A924ContratoSrvVnc_ServicoVncSigla[0];
         n924ContratoSrvVnc_ServicoVncSigla = T002V28_n924ContratoSrvVnc_ServicoVncSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void GetKey2V114( )
      {
         /* Using cursor T002V29 */
         pr_default.execute(19, new Object[] {A917ContratoSrvVnc_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound114 = 1;
         }
         else
         {
            RcdFound114 = 0;
         }
         pr_default.close(19);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002V3 */
         pr_default.execute(1, new Object[] {A917ContratoSrvVnc_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2V114( 39) ;
            RcdFound114 = 1;
            A917ContratoSrvVnc_Codigo = T002V3_A917ContratoSrvVnc_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
            A1801ContratoSrvVnc_NovoRspDmn = T002V3_A1801ContratoSrvVnc_NovoRspDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            n1801ContratoSrvVnc_NovoRspDmn = T002V3_n1801ContratoSrvVnc_NovoRspDmn[0];
            A1743ContratoSrvVnc_NovoStatusDmn = T002V3_A1743ContratoSrvVnc_NovoStatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            n1743ContratoSrvVnc_NovoStatusDmn = T002V3_n1743ContratoSrvVnc_NovoStatusDmn[0];
            A2108ContratoServicosVnc_Descricao = T002V3_A2108ContratoServicosVnc_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2108ContratoServicosVnc_Descricao", A2108ContratoServicosVnc_Descricao);
            n2108ContratoServicosVnc_Descricao = T002V3_n2108ContratoServicosVnc_Descricao[0];
            A1663ContratoSrvVnc_SrvVncStatus = T002V3_A1663ContratoSrvVnc_SrvVncStatus[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
            n1663ContratoSrvVnc_SrvVncStatus = T002V3_n1663ContratoSrvVnc_SrvVncStatus[0];
            A1800ContratoSrvVnc_DoStatusDmn = T002V3_A1800ContratoSrvVnc_DoStatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
            n1800ContratoSrvVnc_DoStatusDmn = T002V3_n1800ContratoSrvVnc_DoStatusDmn[0];
            A1084ContratoSrvVnc_StatusDmn = T002V3_A1084ContratoSrvVnc_StatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
            n1084ContratoSrvVnc_StatusDmn = T002V3_n1084ContratoSrvVnc_StatusDmn[0];
            A1745ContratoSrvVnc_VincularCom = T002V3_A1745ContratoSrvVnc_VincularCom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
            n1745ContratoSrvVnc_VincularCom = T002V3_n1745ContratoSrvVnc_VincularCom[0];
            A1821ContratoSrvVnc_SrvVncRef = T002V3_A1821ContratoSrvVnc_SrvVncRef[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
            n1821ContratoSrvVnc_SrvVncRef = T002V3_n1821ContratoSrvVnc_SrvVncRef[0];
            A1088ContratoSrvVnc_PrestadoraCod = T002V3_A1088ContratoSrvVnc_PrestadoraCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            n1088ContratoSrvVnc_PrestadoraCod = T002V3_n1088ContratoSrvVnc_PrestadoraCod[0];
            A1090ContratoSrvVnc_SemCusto = T002V3_A1090ContratoSrvVnc_SemCusto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
            n1090ContratoSrvVnc_SemCusto = T002V3_n1090ContratoSrvVnc_SemCusto[0];
            A1145ContratoServicosVnc_ClonaSrvOri = T002V3_A1145ContratoServicosVnc_ClonaSrvOri[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
            n1145ContratoServicosVnc_ClonaSrvOri = T002V3_n1145ContratoServicosVnc_ClonaSrvOri[0];
            A1437ContratoServicosVnc_NaoClonaInfo = T002V3_A1437ContratoServicosVnc_NaoClonaInfo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1437ContratoServicosVnc_NaoClonaInfo", A1437ContratoServicosVnc_NaoClonaInfo);
            n1437ContratoServicosVnc_NaoClonaInfo = T002V3_n1437ContratoServicosVnc_NaoClonaInfo[0];
            A1438ContratoServicosVnc_Gestor = T002V3_A1438ContratoServicosVnc_Gestor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
            n1438ContratoServicosVnc_Gestor = T002V3_n1438ContratoServicosVnc_Gestor[0];
            A1818ContratoServicosVnc_ClonarLink = T002V3_A1818ContratoServicosVnc_ClonarLink[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
            n1818ContratoServicosVnc_ClonarLink = T002V3_n1818ContratoServicosVnc_ClonarLink[0];
            A1453ContratoServicosVnc_Ativo = T002V3_A1453ContratoServicosVnc_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
            n1453ContratoServicosVnc_Ativo = T002V3_n1453ContratoServicosVnc_Ativo[0];
            A915ContratoSrvVnc_CntSrvCod = T002V3_A915ContratoSrvVnc_CntSrvCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = T002V3_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            Z917ContratoSrvVnc_Codigo = A917ContratoSrvVnc_Codigo;
            sMode114 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2V114( ) ;
            if ( AnyError == 1 )
            {
               RcdFound114 = 0;
               InitializeNonKey2V114( ) ;
            }
            Gx_mode = sMode114;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound114 = 0;
            InitializeNonKey2V114( ) ;
            sMode114 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode114;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2V114( ) ;
         if ( RcdFound114 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound114 = 0;
         /* Using cursor T002V30 */
         pr_default.execute(20, new Object[] {A917ContratoSrvVnc_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            while ( (pr_default.getStatus(20) != 101) && ( ( T002V30_A917ContratoSrvVnc_Codigo[0] < A917ContratoSrvVnc_Codigo ) ) )
            {
               pr_default.readNext(20);
            }
            if ( (pr_default.getStatus(20) != 101) && ( ( T002V30_A917ContratoSrvVnc_Codigo[0] > A917ContratoSrvVnc_Codigo ) ) )
            {
               A917ContratoSrvVnc_Codigo = T002V30_A917ContratoSrvVnc_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
               RcdFound114 = 1;
            }
         }
         pr_default.close(20);
      }

      protected void move_previous( )
      {
         RcdFound114 = 0;
         /* Using cursor T002V31 */
         pr_default.execute(21, new Object[] {A917ContratoSrvVnc_Codigo});
         if ( (pr_default.getStatus(21) != 101) )
         {
            while ( (pr_default.getStatus(21) != 101) && ( ( T002V31_A917ContratoSrvVnc_Codigo[0] > A917ContratoSrvVnc_Codigo ) ) )
            {
               pr_default.readNext(21);
            }
            if ( (pr_default.getStatus(21) != 101) && ( ( T002V31_A917ContratoSrvVnc_Codigo[0] < A917ContratoSrvVnc_Codigo ) ) )
            {
               A917ContratoSrvVnc_Codigo = T002V31_A917ContratoSrvVnc_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
               RcdFound114 = 1;
            }
         }
         pr_default.close(21);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2V114( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoServicosVnc_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2V114( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound114 == 1 )
            {
               if ( A917ContratoSrvVnc_Codigo != Z917ContratoSrvVnc_Codigo )
               {
                  A917ContratoSrvVnc_Codigo = Z917ContratoSrvVnc_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSRVVNC_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoSrvVnc_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoServicosVnc_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2V114( ) ;
                  GX_FocusControl = edtContratoServicosVnc_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A917ContratoSrvVnc_Codigo != Z917ContratoSrvVnc_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoServicosVnc_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2V114( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSRVVNC_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoSrvVnc_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoServicosVnc_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2V114( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A917ContratoSrvVnc_Codigo != Z917ContratoSrvVnc_Codigo )
         {
            A917ContratoSrvVnc_Codigo = Z917ContratoSrvVnc_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSRVVNC_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoSrvVnc_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoServicosVnc_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2V114( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002V2 */
            pr_default.execute(0, new Object[] {A917ContratoSrvVnc_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosVnc"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1801ContratoSrvVnc_NovoRspDmn != T002V2_A1801ContratoSrvVnc_NovoRspDmn[0] ) || ( StringUtil.StrCmp(Z1743ContratoSrvVnc_NovoStatusDmn, T002V2_A1743ContratoSrvVnc_NovoStatusDmn[0]) != 0 ) || ( StringUtil.StrCmp(Z2108ContratoServicosVnc_Descricao, T002V2_A2108ContratoServicosVnc_Descricao[0]) != 0 ) || ( StringUtil.StrCmp(Z1663ContratoSrvVnc_SrvVncStatus, T002V2_A1663ContratoSrvVnc_SrvVncStatus[0]) != 0 ) || ( StringUtil.StrCmp(Z1800ContratoSrvVnc_DoStatusDmn, T002V2_A1800ContratoSrvVnc_DoStatusDmn[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1084ContratoSrvVnc_StatusDmn, T002V2_A1084ContratoSrvVnc_StatusDmn[0]) != 0 ) || ( StringUtil.StrCmp(Z1745ContratoSrvVnc_VincularCom, T002V2_A1745ContratoSrvVnc_VincularCom[0]) != 0 ) || ( Z1821ContratoSrvVnc_SrvVncRef != T002V2_A1821ContratoSrvVnc_SrvVncRef[0] ) || ( Z1088ContratoSrvVnc_PrestadoraCod != T002V2_A1088ContratoSrvVnc_PrestadoraCod[0] ) || ( Z1090ContratoSrvVnc_SemCusto != T002V2_A1090ContratoSrvVnc_SemCusto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1145ContratoServicosVnc_ClonaSrvOri != T002V2_A1145ContratoServicosVnc_ClonaSrvOri[0] ) || ( Z1437ContratoServicosVnc_NaoClonaInfo != T002V2_A1437ContratoServicosVnc_NaoClonaInfo[0] ) || ( Z1438ContratoServicosVnc_Gestor != T002V2_A1438ContratoServicosVnc_Gestor[0] ) || ( Z1818ContratoServicosVnc_ClonarLink != T002V2_A1818ContratoServicosVnc_ClonarLink[0] ) || ( Z1453ContratoServicosVnc_Ativo != T002V2_A1453ContratoServicosVnc_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z915ContratoSrvVnc_CntSrvCod != T002V2_A915ContratoSrvVnc_CntSrvCod[0] ) || ( Z1589ContratoSrvVnc_SrvVncCntSrvCod != T002V2_A1589ContratoSrvVnc_SrvVncCntSrvCod[0] ) )
            {
               if ( Z1801ContratoSrvVnc_NovoRspDmn != T002V2_A1801ContratoSrvVnc_NovoRspDmn[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_NovoRspDmn");
                  GXUtil.WriteLogRaw("Old: ",Z1801ContratoSrvVnc_NovoRspDmn);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1801ContratoSrvVnc_NovoRspDmn[0]);
               }
               if ( StringUtil.StrCmp(Z1743ContratoSrvVnc_NovoStatusDmn, T002V2_A1743ContratoSrvVnc_NovoStatusDmn[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_NovoStatusDmn");
                  GXUtil.WriteLogRaw("Old: ",Z1743ContratoSrvVnc_NovoStatusDmn);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1743ContratoSrvVnc_NovoStatusDmn[0]);
               }
               if ( StringUtil.StrCmp(Z2108ContratoServicosVnc_Descricao, T002V2_A2108ContratoServicosVnc_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoServicosVnc_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z2108ContratoServicosVnc_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A2108ContratoServicosVnc_Descricao[0]);
               }
               if ( StringUtil.StrCmp(Z1663ContratoSrvVnc_SrvVncStatus, T002V2_A1663ContratoSrvVnc_SrvVncStatus[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_SrvVncStatus");
                  GXUtil.WriteLogRaw("Old: ",Z1663ContratoSrvVnc_SrvVncStatus);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1663ContratoSrvVnc_SrvVncStatus[0]);
               }
               if ( StringUtil.StrCmp(Z1800ContratoSrvVnc_DoStatusDmn, T002V2_A1800ContratoSrvVnc_DoStatusDmn[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_DoStatusDmn");
                  GXUtil.WriteLogRaw("Old: ",Z1800ContratoSrvVnc_DoStatusDmn);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1800ContratoSrvVnc_DoStatusDmn[0]);
               }
               if ( StringUtil.StrCmp(Z1084ContratoSrvVnc_StatusDmn, T002V2_A1084ContratoSrvVnc_StatusDmn[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_StatusDmn");
                  GXUtil.WriteLogRaw("Old: ",Z1084ContratoSrvVnc_StatusDmn);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1084ContratoSrvVnc_StatusDmn[0]);
               }
               if ( StringUtil.StrCmp(Z1745ContratoSrvVnc_VincularCom, T002V2_A1745ContratoSrvVnc_VincularCom[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_VincularCom");
                  GXUtil.WriteLogRaw("Old: ",Z1745ContratoSrvVnc_VincularCom);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1745ContratoSrvVnc_VincularCom[0]);
               }
               if ( Z1821ContratoSrvVnc_SrvVncRef != T002V2_A1821ContratoSrvVnc_SrvVncRef[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_SrvVncRef");
                  GXUtil.WriteLogRaw("Old: ",Z1821ContratoSrvVnc_SrvVncRef);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1821ContratoSrvVnc_SrvVncRef[0]);
               }
               if ( Z1088ContratoSrvVnc_PrestadoraCod != T002V2_A1088ContratoSrvVnc_PrestadoraCod[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_PrestadoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z1088ContratoSrvVnc_PrestadoraCod);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1088ContratoSrvVnc_PrestadoraCod[0]);
               }
               if ( Z1090ContratoSrvVnc_SemCusto != T002V2_A1090ContratoSrvVnc_SemCusto[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_SemCusto");
                  GXUtil.WriteLogRaw("Old: ",Z1090ContratoSrvVnc_SemCusto);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1090ContratoSrvVnc_SemCusto[0]);
               }
               if ( Z1145ContratoServicosVnc_ClonaSrvOri != T002V2_A1145ContratoServicosVnc_ClonaSrvOri[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoServicosVnc_ClonaSrvOri");
                  GXUtil.WriteLogRaw("Old: ",Z1145ContratoServicosVnc_ClonaSrvOri);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1145ContratoServicosVnc_ClonaSrvOri[0]);
               }
               if ( Z1437ContratoServicosVnc_NaoClonaInfo != T002V2_A1437ContratoServicosVnc_NaoClonaInfo[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoServicosVnc_NaoClonaInfo");
                  GXUtil.WriteLogRaw("Old: ",Z1437ContratoServicosVnc_NaoClonaInfo);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1437ContratoServicosVnc_NaoClonaInfo[0]);
               }
               if ( Z1438ContratoServicosVnc_Gestor != T002V2_A1438ContratoServicosVnc_Gestor[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoServicosVnc_Gestor");
                  GXUtil.WriteLogRaw("Old: ",Z1438ContratoServicosVnc_Gestor);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1438ContratoServicosVnc_Gestor[0]);
               }
               if ( Z1818ContratoServicosVnc_ClonarLink != T002V2_A1818ContratoServicosVnc_ClonarLink[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoServicosVnc_ClonarLink");
                  GXUtil.WriteLogRaw("Old: ",Z1818ContratoServicosVnc_ClonarLink);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1818ContratoServicosVnc_ClonarLink[0]);
               }
               if ( Z1453ContratoServicosVnc_Ativo != T002V2_A1453ContratoServicosVnc_Ativo[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoServicosVnc_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1453ContratoServicosVnc_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1453ContratoServicosVnc_Ativo[0]);
               }
               if ( Z915ContratoSrvVnc_CntSrvCod != T002V2_A915ContratoSrvVnc_CntSrvCod[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_CntSrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z915ContratoSrvVnc_CntSrvCod);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A915ContratoSrvVnc_CntSrvCod[0]);
               }
               if ( Z1589ContratoSrvVnc_SrvVncCntSrvCod != T002V2_A1589ContratoSrvVnc_SrvVncCntSrvCod[0] )
               {
                  GXUtil.WriteLog("contratoservicosvnc:[seudo value changed for attri]"+"ContratoSrvVnc_SrvVncCntSrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z1589ContratoSrvVnc_SrvVncCntSrvCod);
                  GXUtil.WriteLogRaw("Current: ",T002V2_A1589ContratoSrvVnc_SrvVncCntSrvCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosVnc"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2V114( )
      {
         BeforeValidate2V114( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2V114( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2V114( 0) ;
            CheckOptimisticConcurrency2V114( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2V114( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2V114( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002V32 */
                     pr_default.execute(22, new Object[] {n1801ContratoSrvVnc_NovoRspDmn, A1801ContratoSrvVnc_NovoRspDmn, n1743ContratoSrvVnc_NovoStatusDmn, A1743ContratoSrvVnc_NovoStatusDmn, n2108ContratoServicosVnc_Descricao, A2108ContratoServicosVnc_Descricao, n1663ContratoSrvVnc_SrvVncStatus, A1663ContratoSrvVnc_SrvVncStatus, n1800ContratoSrvVnc_DoStatusDmn, A1800ContratoSrvVnc_DoStatusDmn, n1084ContratoSrvVnc_StatusDmn, A1084ContratoSrvVnc_StatusDmn, n1745ContratoSrvVnc_VincularCom, A1745ContratoSrvVnc_VincularCom, n1821ContratoSrvVnc_SrvVncRef, A1821ContratoSrvVnc_SrvVncRef, n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod, n1090ContratoSrvVnc_SemCusto, A1090ContratoSrvVnc_SemCusto, n1145ContratoServicosVnc_ClonaSrvOri, A1145ContratoServicosVnc_ClonaSrvOri, n1437ContratoServicosVnc_NaoClonaInfo, A1437ContratoServicosVnc_NaoClonaInfo, n1438ContratoServicosVnc_Gestor, A1438ContratoServicosVnc_Gestor, n1818ContratoServicosVnc_ClonarLink, A1818ContratoServicosVnc_ClonarLink, n1453ContratoServicosVnc_Ativo, A1453ContratoServicosVnc_Ativo, A915ContratoSrvVnc_CntSrvCod, n1589ContratoSrvVnc_SrvVncCntSrvCod, A1589ContratoSrvVnc_SrvVncCntSrvCod});
                     A917ContratoSrvVnc_Codigo = T002V32_A917ContratoSrvVnc_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
                     pr_default.close(22);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosVnc") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2V0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2V114( ) ;
            }
            EndLevel2V114( ) ;
         }
         CloseExtendedTableCursors2V114( ) ;
      }

      protected void Update2V114( )
      {
         BeforeValidate2V114( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2V114( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2V114( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2V114( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2V114( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002V33 */
                     pr_default.execute(23, new Object[] {n1801ContratoSrvVnc_NovoRspDmn, A1801ContratoSrvVnc_NovoRspDmn, n1743ContratoSrvVnc_NovoStatusDmn, A1743ContratoSrvVnc_NovoStatusDmn, n2108ContratoServicosVnc_Descricao, A2108ContratoServicosVnc_Descricao, n1663ContratoSrvVnc_SrvVncStatus, A1663ContratoSrvVnc_SrvVncStatus, n1800ContratoSrvVnc_DoStatusDmn, A1800ContratoSrvVnc_DoStatusDmn, n1084ContratoSrvVnc_StatusDmn, A1084ContratoSrvVnc_StatusDmn, n1745ContratoSrvVnc_VincularCom, A1745ContratoSrvVnc_VincularCom, n1821ContratoSrvVnc_SrvVncRef, A1821ContratoSrvVnc_SrvVncRef, n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod, n1090ContratoSrvVnc_SemCusto, A1090ContratoSrvVnc_SemCusto, n1145ContratoServicosVnc_ClonaSrvOri, A1145ContratoServicosVnc_ClonaSrvOri, n1437ContratoServicosVnc_NaoClonaInfo, A1437ContratoServicosVnc_NaoClonaInfo, n1438ContratoServicosVnc_Gestor, A1438ContratoServicosVnc_Gestor, n1818ContratoServicosVnc_ClonarLink, A1818ContratoServicosVnc_ClonarLink, n1453ContratoServicosVnc_Ativo, A1453ContratoServicosVnc_Ativo, A915ContratoSrvVnc_CntSrvCod, n1589ContratoSrvVnc_SrvVncCntSrvCod, A1589ContratoSrvVnc_SrvVncCntSrvCod, A917ContratoSrvVnc_Codigo});
                     pr_default.close(23);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosVnc") ;
                     if ( (pr_default.getStatus(23) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosVnc"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2V114( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2V114( ) ;
         }
         CloseExtendedTableCursors2V114( ) ;
      }

      protected void DeferredUpdate2V114( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2V114( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2V114( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2V114( ) ;
            AfterConfirm2V114( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2V114( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002V34 */
                  pr_default.execute(24, new Object[] {A917ContratoSrvVnc_Codigo});
                  pr_default.close(24);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosVnc") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode114 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2V114( ) ;
         Gx_mode = sMode114;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2V114( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002V37 */
            pr_default.execute(25, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
            if ( (pr_default.getStatus(25) != 101) )
            {
               A1092ContratoSrvVnc_PrestadoraPesNom = T002V37_A1092ContratoSrvVnc_PrestadoraPesNom[0];
               n1092ContratoSrvVnc_PrestadoraPesNom = T002V37_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            }
            else
            {
               A1092ContratoSrvVnc_PrestadoraPesNom = "";
               n1092ContratoSrvVnc_PrestadoraPesNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1092ContratoSrvVnc_PrestadoraPesNom", A1092ContratoSrvVnc_PrestadoraPesNom);
            }
            pr_default.close(25);
            /* Using cursor T002V40 */
            pr_default.execute(26, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
            if ( (pr_default.getStatus(26) != 101) )
            {
               A1744ContratoSrvVnc_PrestadoraTpFab = T002V40_A1744ContratoSrvVnc_PrestadoraTpFab[0];
               n1744ContratoSrvVnc_PrestadoraTpFab = T002V40_n1744ContratoSrvVnc_PrestadoraTpFab[0];
            }
            else
            {
               A1744ContratoSrvVnc_PrestadoraTpFab = "";
               n1744ContratoSrvVnc_PrestadoraTpFab = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1744ContratoSrvVnc_PrestadoraTpFab", A1744ContratoSrvVnc_PrestadoraTpFab);
            }
            pr_default.close(26);
            GXVvSELCONTRATO_CODIGO_html2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
            cmbContratoSrvVnc_NovoRspDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoRspDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoRspDmn.Enabled), 5, 0)));
            cmbContratoSrvVnc_NovoStatusDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoStatusDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoStatusDmn.Enabled), 5, 0)));
            if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
            {
               lblTextblockcontratosrvvnc_vincularcom_Caption = "Vinculada com";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratosrvvnc_vincularcom_Internalname, "Caption", lblTextblockcontratosrvvnc_vincularcom_Caption);
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  lblTextblockcontratosrvvnc_vincularcom_Caption = "Voltar";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratosrvvnc_vincularcom_Internalname, "Caption", lblTextblockcontratosrvvnc_vincularcom_Caption);
               }
            }
            lblTextblockselcontrato_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockselcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockselcontrato_codigo_Visible), 5, 0)));
            dynavSelcontrato_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelcontrato_codigo.Visible), 5, 0)));
            lblTextblockselservico_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockselservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockselservico_codigo_Visible), 5, 0)));
            dynavSelservico_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelservico_codigo.Visible), 5, 0)));
            /* Using cursor T002V41 */
            pr_default.execute(27, new Object[] {A915ContratoSrvVnc_CntSrvCod});
            A933ContratoSrvVnc_ContratoCod = T002V41_A933ContratoSrvVnc_ContratoCod[0];
            n933ContratoSrvVnc_ContratoCod = T002V41_n933ContratoSrvVnc_ContratoCod[0];
            A921ContratoSrvVnc_ServicoCod = T002V41_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = T002V41_n921ContratoSrvVnc_ServicoCod[0];
            pr_default.close(27);
            /* Using cursor T002V42 */
            pr_default.execute(28, new Object[] {n933ContratoSrvVnc_ContratoCod, A933ContratoSrvVnc_ContratoCod});
            A1662ContratoSrvVnc_ContratoAreaCod = T002V42_A1662ContratoSrvVnc_ContratoAreaCod[0];
            n1662ContratoSrvVnc_ContratoAreaCod = T002V42_n1662ContratoSrvVnc_ContratoAreaCod[0];
            pr_default.close(28);
            /* Using cursor T002V43 */
            pr_default.execute(29, new Object[] {n921ContratoSrvVnc_ServicoCod, A921ContratoSrvVnc_ServicoCod});
            A922ContratoSrvVnc_ServicoSigla = T002V43_A922ContratoSrvVnc_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
            n922ContratoSrvVnc_ServicoSigla = T002V43_n922ContratoSrvVnc_ServicoSigla[0];
            pr_default.close(29);
            /* Using cursor T002V44 */
            pr_default.execute(30, new Object[] {n1589ContratoSrvVnc_SrvVncCntSrvCod, A1589ContratoSrvVnc_SrvVncCntSrvCod});
            A1629ContratoSrvVnc_SrvVncTpVnc = T002V44_A1629ContratoSrvVnc_SrvVncTpVnc[0];
            n1629ContratoSrvVnc_SrvVncTpVnc = T002V44_n1629ContratoSrvVnc_SrvVncTpVnc[0];
            A1630ContratoSrvVnc_SrvVncTpDias = T002V44_A1630ContratoSrvVnc_SrvVncTpDias[0];
            n1630ContratoSrvVnc_SrvVncTpDias = T002V44_n1630ContratoSrvVnc_SrvVncTpDias[0];
            A1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V44_A1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            n1642ContratoSrvVnc_SrvVncVlrUndCnt = T002V44_n1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            A1651ContratoSrvVnc_SrvVncPrzInc = T002V44_A1651ContratoSrvVnc_SrvVncPrzInc[0];
            n1651ContratoSrvVnc_SrvVncPrzInc = T002V44_n1651ContratoSrvVnc_SrvVncPrzInc[0];
            A1628ContratoSrvVnc_SrvVncCntCod = T002V44_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = T002V44_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = T002V44_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = T002V44_n923ContratoSrvVnc_ServicoVncCod[0];
            pr_default.close(30);
            /* Using cursor T002V45 */
            pr_default.execute(31, new Object[] {n1628ContratoSrvVnc_SrvVncCntCod, A1628ContratoSrvVnc_SrvVncCntCod});
            A1631ContratoSrvVnc_SrvVncCntNum = T002V45_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = T002V45_n1631ContratoSrvVnc_SrvVncCntNum[0];
            pr_default.close(31);
            /* Using cursor T002V46 */
            pr_default.execute(32, new Object[] {n923ContratoSrvVnc_ServicoVncCod, A923ContratoSrvVnc_ServicoVncCod});
            A924ContratoSrvVnc_ServicoVncSigla = T002V46_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = T002V46_n924ContratoSrvVnc_ServicoVncSigla[0];
            pr_default.close(32);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( A1589ContratoSrvVnc_SrvVncCntSrvCod > 0 ) )
            {
               AV22SelServico_Codigo = A1589ContratoSrvVnc_SrvVncCntSrvCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
            }
            else
            {
               if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0] )
               {
                  AV22SelServico_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
               }
               else
               {
                  if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
                  {
                     AV22SelServico_Codigo = 0;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SelServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0)));
                  }
               }
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0] )
            {
               AV20SelContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  AV20SelContrato_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SelContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0)));
               }
            }
         }
      }

      protected void EndLevel2V114( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2V114( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(25);
            pr_default.close(26);
            pr_default.close(27);
            pr_default.close(30);
            pr_default.close(28);
            pr_default.close(29);
            pr_default.close(31);
            pr_default.close(32);
            context.CommitDataStores( "ContratoServicosVnc");
            if ( AnyError == 0 )
            {
               ConfirmValues2V0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(25);
            pr_default.close(26);
            pr_default.close(27);
            pr_default.close(30);
            pr_default.close(28);
            pr_default.close(29);
            pr_default.close(31);
            pr_default.close(32);
            context.RollbackDataStores( "ContratoServicosVnc");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2V114( )
      {
         /* Scan By routine */
         /* Using cursor T002V47 */
         pr_default.execute(33);
         RcdFound114 = 0;
         if ( (pr_default.getStatus(33) != 101) )
         {
            RcdFound114 = 1;
            A917ContratoSrvVnc_Codigo = T002V47_A917ContratoSrvVnc_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2V114( )
      {
         /* Scan next routine */
         pr_default.readNext(33);
         RcdFound114 = 0;
         if ( (pr_default.getStatus(33) != 101) )
         {
            RcdFound114 = 1;
            A917ContratoSrvVnc_Codigo = T002V47_A917ContratoSrvVnc_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2V114( )
      {
         pr_default.close(33);
      }

      protected void AfterConfirm2V114( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2V114( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2V114( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2V114( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2V114( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2V114( )
      {
         /* Before Validate Rules */
         if ( AV22SelServico_Codigo > 0 )
         {
            A1589ContratoSrvVnc_SrvVncCntSrvCod = AV22SelServico_Codigo;
            n1589ContratoSrvVnc_SrvVncCntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0)));
         }
         else
         {
            if ( (0==AV22SelServico_Codigo) )
            {
               A1589ContratoSrvVnc_SrvVncCntSrvCod = 0;
               n1589ContratoSrvVnc_SrvVncCntSrvCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0)));
               n1589ContratoSrvVnc_SrvVncCntSrvCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0)));
            }
         }
      }

      protected void DisableAttributes2V114( )
      {
         edtContratoSrvVnc_ServicoSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_ServicoSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSrvVnc_ServicoSigla_Enabled), 5, 0)));
         edtContratoServicosVnc_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosVnc_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosVnc_Descricao_Enabled), 5, 0)));
         cmbContratoSrvVnc_DoStatusDmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_DoStatusDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_DoStatusDmn.Enabled), 5, 0)));
         cmbContratoSrvVnc_StatusDmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_StatusDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_StatusDmn.Enabled), 5, 0)));
         cmbContratoSrvVnc_PrestadoraCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_PrestadoraCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_PrestadoraCod.Enabled), 5, 0)));
         cmbContratoSrvVnc_SrvVncStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SrvVncStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_SrvVncStatus.Enabled), 5, 0)));
         dynavSelcontrato_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontrato_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelcontrato_codigo.Enabled), 5, 0)));
         dynavSelservico_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelservico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSelservico_codigo.Enabled), 5, 0)));
         cmbContratoSrvVnc_VincularCom.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_VincularCom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_VincularCom.Enabled), 5, 0)));
         cmbContratoSrvVnc_SrvVncRef.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SrvVncRef_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_SrvVncRef.Enabled), 5, 0)));
         cmbContratoServicosVnc_Gestor.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_Gestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosVnc_Gestor.Enabled), 5, 0)));
         cmbContratoSrvVnc_NovoStatusDmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoStatusDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoStatusDmn.Enabled), 5, 0)));
         cmbContratoSrvVnc_NovoRspDmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_NovoRspDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_NovoRspDmn.Enabled), 5, 0)));
         cmbContratoServicosVnc_ClonaSrvOri.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_ClonaSrvOri_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosVnc_ClonaSrvOri.Enabled), 5, 0)));
         cmbContratoServicosVnc_ClonarLink.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_ClonarLink_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosVnc_ClonarLink.Enabled), 5, 0)));
         chkContratoServicosVnc_NaoClonaInfo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosVnc_NaoClonaInfo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicosVnc_NaoClonaInfo.Enabled), 5, 0)));
         cmbContratoSrvVnc_SemCusto.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SemCusto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoSrvVnc_SemCusto.Enabled), 5, 0)));
         chkContratoServicosVnc_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosVnc_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicosVnc_Ativo.Enabled), 5, 0)));
         edtContratoSrvVnc_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSrvVnc_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2V0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122110385");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV16ContratoSrvVnc_CntSrvCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z917ContratoSrvVnc_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1801ContratoSrvVnc_NovoRspDmn), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1743ContratoSrvVnc_NovoStatusDmn", StringUtil.RTrim( Z1743ContratoSrvVnc_NovoStatusDmn));
         GxWebStd.gx_hidden_field( context, "Z2108ContratoServicosVnc_Descricao", Z2108ContratoServicosVnc_Descricao);
         GxWebStd.gx_hidden_field( context, "Z1663ContratoSrvVnc_SrvVncStatus", StringUtil.RTrim( Z1663ContratoSrvVnc_SrvVncStatus));
         GxWebStd.gx_hidden_field( context, "Z1800ContratoSrvVnc_DoStatusDmn", StringUtil.RTrim( Z1800ContratoSrvVnc_DoStatusDmn));
         GxWebStd.gx_hidden_field( context, "Z1084ContratoSrvVnc_StatusDmn", StringUtil.RTrim( Z1084ContratoSrvVnc_StatusDmn));
         GxWebStd.gx_hidden_field( context, "Z1745ContratoSrvVnc_VincularCom", StringUtil.RTrim( Z1745ContratoSrvVnc_VincularCom));
         GxWebStd.gx_hidden_field( context, "Z1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1821ContratoSrvVnc_SrvVncRef), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1088ContratoSrvVnc_PrestadoraCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1090ContratoSrvVnc_SemCusto", Z1090ContratoSrvVnc_SemCusto);
         GxWebStd.gx_boolean_hidden_field( context, "Z1145ContratoServicosVnc_ClonaSrvOri", Z1145ContratoServicosVnc_ClonaSrvOri);
         GxWebStd.gx_boolean_hidden_field( context, "Z1437ContratoServicosVnc_NaoClonaInfo", Z1437ContratoServicosVnc_NaoClonaInfo);
         GxWebStd.gx_hidden_field( context, "Z1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1438ContratoServicosVnc_Gestor), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1818ContratoServicosVnc_ClonarLink", Z1818ContratoServicosVnc_ClonarLink);
         GxWebStd.gx_boolean_hidden_field( context, "Z1453ContratoServicosVnc_Ativo", Z1453ContratoServicosVnc_Ativo);
         GxWebStd.gx_hidden_field( context, "Z915ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z915ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N915ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSRVVNC_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoSrvVnc_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOSRVVNC_SRVVNCCNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SRVVNCCNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_PRESTADORAPESNOM", StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_PRESTADORATPFAB", StringUtil.RTrim( A1744ContratoSrvVnc_PrestadoraTpFab));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A933ContratoSrvVnc_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A921ContratoSrvVnc_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SRVVNCTPVNC", StringUtil.RTrim( A1629ContratoSrvVnc_SrvVncTpVnc));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SRVVNCTPDIAS", StringUtil.RTrim( A1630ContratoSrvVnc_SrvVncTpDias));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SRVVNCVLRUNDCNT", StringUtil.LTrim( StringUtil.NToC( A1642ContratoSrvVnc_SrvVncVlrUndCnt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SRVVNCPRZINC", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1651ContratoSrvVnc_SrvVncPrzInc), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SRVVNCCNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1628ContratoSrvVnc_SrvVncCntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SERVICOVNCCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A923ContratoSrvVnc_ServicoVncCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_CONTRATOAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1662ContratoSrvVnc_ContratoAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SRVVNCCNTNUM", StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SERVICOVNCSIGLA", StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV28Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSRVVNC_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoSrvVnc_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosVnc";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A915ContratoSrvVnc_CntSrvCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosvnc:[SendSecurityCheck value for]"+"ContratoSrvVnc_Codigo:"+context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratoservicosvnc:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoservicosvnc:[SendSecurityCheck value for]"+"ContratoSrvVnc_CntSrvCod:"+context.localUtil.Format( (decimal)(A915ContratoSrvVnc_CntSrvCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV16ContratoSrvVnc_CntSrvCod) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosVnc" ;
      }

      public override String GetPgmdesc( )
      {
         return "Regra de Vinculo entre Servi�os" ;
      }

      protected void InitializeNonKey2V114( )
      {
         A915ContratoSrvVnc_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A915ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0)));
         A1589ContratoSrvVnc_SrvVncCntSrvCod = 0;
         n1589ContratoSrvVnc_SrvVncCntSrvCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1589ContratoSrvVnc_SrvVncCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1589ContratoSrvVnc_SrvVncCntSrvCod), 6, 0)));
         A1801ContratoSrvVnc_NovoRspDmn = 0;
         n1801ContratoSrvVnc_NovoRspDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
         n1801ContratoSrvVnc_NovoRspDmn = ((0==A1801ContratoSrvVnc_NovoRspDmn) ? true : false);
         A1743ContratoSrvVnc_NovoStatusDmn = "";
         n1743ContratoSrvVnc_NovoStatusDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
         n1743ContratoSrvVnc_NovoStatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn)) ? true : false);
         A1092ContratoSrvVnc_PrestadoraPesNom = "";
         n1092ContratoSrvVnc_PrestadoraPesNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1092ContratoSrvVnc_PrestadoraPesNom", A1092ContratoSrvVnc_PrestadoraPesNom);
         A1744ContratoSrvVnc_PrestadoraTpFab = "";
         n1744ContratoSrvVnc_PrestadoraTpFab = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1744ContratoSrvVnc_PrestadoraTpFab", A1744ContratoSrvVnc_PrestadoraTpFab);
         A2108ContratoServicosVnc_Descricao = "";
         n2108ContratoServicosVnc_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2108ContratoServicosVnc_Descricao", A2108ContratoServicosVnc_Descricao);
         n2108ContratoServicosVnc_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2108ContratoServicosVnc_Descricao)) ? true : false);
         A933ContratoSrvVnc_ContratoCod = 0;
         n933ContratoSrvVnc_ContratoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A933ContratoSrvVnc_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A933ContratoSrvVnc_ContratoCod), 6, 0)));
         A1662ContratoSrvVnc_ContratoAreaCod = 0;
         n1662ContratoSrvVnc_ContratoAreaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1662ContratoSrvVnc_ContratoAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1662ContratoSrvVnc_ContratoAreaCod), 6, 0)));
         A921ContratoSrvVnc_ServicoCod = 0;
         n921ContratoSrvVnc_ServicoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A921ContratoSrvVnc_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A921ContratoSrvVnc_ServicoCod), 6, 0)));
         A922ContratoSrvVnc_ServicoSigla = "";
         n922ContratoSrvVnc_ServicoSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
         A1628ContratoSrvVnc_SrvVncCntCod = 0;
         n1628ContratoSrvVnc_SrvVncCntCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1628ContratoSrvVnc_SrvVncCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1628ContratoSrvVnc_SrvVncCntCod), 6, 0)));
         A1631ContratoSrvVnc_SrvVncCntNum = "";
         n1631ContratoSrvVnc_SrvVncCntNum = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1631ContratoSrvVnc_SrvVncCntNum", A1631ContratoSrvVnc_SrvVncCntNum);
         A923ContratoSrvVnc_ServicoVncCod = 0;
         n923ContratoSrvVnc_ServicoVncCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A923ContratoSrvVnc_ServicoVncCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A923ContratoSrvVnc_ServicoVncCod), 6, 0)));
         A924ContratoSrvVnc_ServicoVncSigla = "";
         n924ContratoSrvVnc_ServicoVncSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A924ContratoSrvVnc_ServicoVncSigla", A924ContratoSrvVnc_ServicoVncSigla);
         A1629ContratoSrvVnc_SrvVncTpVnc = "";
         n1629ContratoSrvVnc_SrvVncTpVnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1629ContratoSrvVnc_SrvVncTpVnc", A1629ContratoSrvVnc_SrvVncTpVnc);
         A1630ContratoSrvVnc_SrvVncTpDias = "";
         n1630ContratoSrvVnc_SrvVncTpDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1630ContratoSrvVnc_SrvVncTpDias", A1630ContratoSrvVnc_SrvVncTpDias);
         A1642ContratoSrvVnc_SrvVncVlrUndCnt = 0;
         n1642ContratoSrvVnc_SrvVncVlrUndCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1642ContratoSrvVnc_SrvVncVlrUndCnt", StringUtil.LTrim( StringUtil.Str( A1642ContratoSrvVnc_SrvVncVlrUndCnt, 18, 5)));
         A1651ContratoSrvVnc_SrvVncPrzInc = 0;
         n1651ContratoSrvVnc_SrvVncPrzInc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1651ContratoSrvVnc_SrvVncPrzInc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1651ContratoSrvVnc_SrvVncPrzInc), 4, 0)));
         A1800ContratoSrvVnc_DoStatusDmn = "";
         n1800ContratoSrvVnc_DoStatusDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
         n1800ContratoSrvVnc_DoStatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn)) ? true : false);
         A1084ContratoSrvVnc_StatusDmn = "";
         n1084ContratoSrvVnc_StatusDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
         n1084ContratoSrvVnc_StatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn)) ? true : false);
         A1821ContratoSrvVnc_SrvVncRef = 0;
         n1821ContratoSrvVnc_SrvVncRef = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
         n1821ContratoSrvVnc_SrvVncRef = ((0==A1821ContratoSrvVnc_SrvVncRef) ? true : false);
         A1088ContratoSrvVnc_PrestadoraCod = 0;
         n1088ContratoSrvVnc_PrestadoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
         n1088ContratoSrvVnc_PrestadoraCod = ((0==A1088ContratoSrvVnc_PrestadoraCod) ? true : false);
         A1090ContratoSrvVnc_SemCusto = false;
         n1090ContratoSrvVnc_SemCusto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
         n1090ContratoSrvVnc_SemCusto = ((false==A1090ContratoSrvVnc_SemCusto) ? true : false);
         A1145ContratoServicosVnc_ClonaSrvOri = false;
         n1145ContratoServicosVnc_ClonaSrvOri = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
         n1145ContratoServicosVnc_ClonaSrvOri = ((false==A1145ContratoServicosVnc_ClonaSrvOri) ? true : false);
         A1437ContratoServicosVnc_NaoClonaInfo = false;
         n1437ContratoServicosVnc_NaoClonaInfo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1437ContratoServicosVnc_NaoClonaInfo", A1437ContratoServicosVnc_NaoClonaInfo);
         n1437ContratoServicosVnc_NaoClonaInfo = ((false==A1437ContratoServicosVnc_NaoClonaInfo) ? true : false);
         A1438ContratoServicosVnc_Gestor = 0;
         n1438ContratoServicosVnc_Gestor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
         n1438ContratoServicosVnc_Gestor = ((0==A1438ContratoServicosVnc_Gestor) ? true : false);
         A1818ContratoServicosVnc_ClonarLink = false;
         n1818ContratoServicosVnc_ClonarLink = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
         n1818ContratoServicosVnc_ClonarLink = ((false==A1818ContratoServicosVnc_ClonarLink) ? true : false);
         A1663ContratoSrvVnc_SrvVncStatus = "S";
         n1663ContratoSrvVnc_SrvVncStatus = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
         A1745ContratoSrvVnc_VincularCom = "D";
         n1745ContratoSrvVnc_VincularCom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
         A1453ContratoServicosVnc_Ativo = true;
         n1453ContratoServicosVnc_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
         Z1801ContratoSrvVnc_NovoRspDmn = 0;
         Z1743ContratoSrvVnc_NovoStatusDmn = "";
         Z2108ContratoServicosVnc_Descricao = "";
         Z1663ContratoSrvVnc_SrvVncStatus = "";
         Z1800ContratoSrvVnc_DoStatusDmn = "";
         Z1084ContratoSrvVnc_StatusDmn = "";
         Z1745ContratoSrvVnc_VincularCom = "";
         Z1821ContratoSrvVnc_SrvVncRef = 0;
         Z1088ContratoSrvVnc_PrestadoraCod = 0;
         Z1090ContratoSrvVnc_SemCusto = false;
         Z1145ContratoServicosVnc_ClonaSrvOri = false;
         Z1437ContratoServicosVnc_NaoClonaInfo = false;
         Z1438ContratoServicosVnc_Gestor = 0;
         Z1818ContratoServicosVnc_ClonarLink = false;
         Z1453ContratoServicosVnc_Ativo = false;
         Z915ContratoSrvVnc_CntSrvCod = 0;
         Z1589ContratoSrvVnc_SrvVncCntSrvCod = 0;
      }

      protected void InitAll2V114( )
      {
         A917ContratoSrvVnc_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
         InitializeNonKey2V114( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1453ContratoServicosVnc_Ativo = i1453ContratoServicosVnc_Ativo;
         n1453ContratoServicosVnc_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
         A1745ContratoSrvVnc_VincularCom = i1745ContratoSrvVnc_VincularCom;
         n1745ContratoSrvVnc_VincularCom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
         A1663ContratoSrvVnc_SrvVncStatus = i1663ContratoSrvVnc_SrvVncStatus;
         n1663ContratoSrvVnc_SrvVncStatus = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122110431");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicosvnc.js", "?20203122110432");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratosrvvnc_servicosigla_Internalname = "TEXTBLOCKCONTRATOSRVVNC_SERVICOSIGLA";
         edtContratoSrvVnc_ServicoSigla_Internalname = "CONTRATOSRVVNC_SERVICOSIGLA";
         lblTextblockcontratoservicosvnc_descricao_Internalname = "TEXTBLOCKCONTRATOSERVICOSVNC_DESCRICAO";
         edtContratoServicosVnc_Descricao_Internalname = "CONTRATOSERVICOSVNC_DESCRICAO";
         lblTextblockcontratosrvvnc_dostatusdmn_Internalname = "TEXTBLOCKCONTRATOSRVVNC_DOSTATUSDMN";
         cmbContratoSrvVnc_DoStatusDmn_Internalname = "CONTRATOSRVVNC_DOSTATUSDMN";
         lblTextblockcontratosrvvnc_statusdmn_Internalname = "TEXTBLOCKCONTRATOSRVVNC_STATUSDMN";
         cmbContratoSrvVnc_StatusDmn_Internalname = "CONTRATOSRVVNC_STATUSDMN";
         lblTextblockcontratosrvvnc_prestadoracod_Internalname = "TEXTBLOCKCONTRATOSRVVNC_PRESTADORACOD";
         cmbContratoSrvVnc_PrestadoraCod_Internalname = "CONTRATOSRVVNC_PRESTADORACOD";
         lblTextblockcontratosrvvnc_srvvncstatus_Internalname = "TEXTBLOCKCONTRATOSRVVNC_SRVVNCSTATUS";
         cmbContratoSrvVnc_SrvVncStatus_Internalname = "CONTRATOSRVVNC_SRVVNCSTATUS";
         lblTextblockselcontrato_codigo_Internalname = "TEXTBLOCKSELCONTRATO_CODIGO";
         dynavSelcontrato_codigo_Internalname = "vSELCONTRATO_CODIGO";
         lblTextblockselservico_codigo_Internalname = "TEXTBLOCKSELSERVICO_CODIGO";
         dynavSelservico_codigo_Internalname = "vSELSERVICO_CODIGO";
         lblTextblockcontratosrvvnc_vincularcom_Internalname = "TEXTBLOCKCONTRATOSRVVNC_VINCULARCOM";
         cmbContratoSrvVnc_VincularCom_Internalname = "CONTRATOSRVVNC_VINCULARCOM";
         lblTextblockcontratosrvvnc_srvvncref_Internalname = "TEXTBLOCKCONTRATOSRVVNC_SRVVNCREF";
         cmbContratoSrvVnc_SrvVncRef_Internalname = "CONTRATOSRVVNC_SRVVNCREF";
         lblTextblockcontratoservicosvnc_gestor_Internalname = "TEXTBLOCKCONTRATOSERVICOSVNC_GESTOR";
         cmbContratoServicosVnc_Gestor_Internalname = "CONTRATOSERVICOSVNC_GESTOR";
         lblTextblockcontratosrvvnc_novostatusdmn_Internalname = "TEXTBLOCKCONTRATOSRVVNC_NOVOSTATUSDMN";
         cmbContratoSrvVnc_NovoStatusDmn_Internalname = "CONTRATOSRVVNC_NOVOSTATUSDMN";
         lblTextblockcontratosrvvnc_novorspdmn_Internalname = "TEXTBLOCKCONTRATOSRVVNC_NOVORSPDMN";
         cmbContratoSrvVnc_NovoRspDmn_Internalname = "CONTRATOSRVVNC_NOVORSPDMN";
         lblTextblockcontratoservicosvnc_clonasrvori_Internalname = "TEXTBLOCKCONTRATOSERVICOSVNC_CLONASRVORI";
         cmbContratoServicosVnc_ClonaSrvOri_Internalname = "CONTRATOSERVICOSVNC_CLONASRVORI";
         lblTextblockcontratoservicosvnc_clonarlink_Internalname = "TEXTBLOCKCONTRATOSERVICOSVNC_CLONARLINK";
         cmbContratoServicosVnc_ClonarLink_Internalname = "CONTRATOSERVICOSVNC_CLONARLINK";
         lblTextblockcontratoservicosvnc_naoclonainfo_Internalname = "TEXTBLOCKCONTRATOSERVICOSVNC_NAOCLONAINFO";
         chkContratoServicosVnc_NaoClonaInfo_Internalname = "CONTRATOSERVICOSVNC_NAOCLONAINFO";
         lblContratoservicosvnc_naoclonainfo_righttext_Internalname = "CONTRATOSERVICOSVNC_NAOCLONAINFO_RIGHTTEXT";
         tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname = "TABLEMERGEDCONTRATOSERVICOSVNC_NAOCLONAINFO";
         lblTextblockcontratosrvvnc_semcusto_Internalname = "TEXTBLOCKCONTRATOSRVVNC_SEMCUSTO";
         cmbContratoSrvVnc_SemCusto_Internalname = "CONTRATOSRVVNC_SEMCUSTO";
         lblTextblockcontratoservicosvnc_ativo_Internalname = "TEXTBLOCKCONTRATOSERVICOSVNC_ATIVO";
         chkContratoServicosVnc_Ativo_Internalname = "CONTRATOSERVICOSVNC_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoSrvVnc_Codigo_Internalname = "CONTRATOSRVVNC_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Regra de disparo autom�tico";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Regra de Vinculo entre Servi�os";
         chkContratoServicosVnc_NaoClonaInfo.Enabled = 1;
         chkContratoServicosVnc_Ativo.Enabled = 1;
         chkContratoServicosVnc_Ativo.Visible = 1;
         lblTextblockcontratoservicosvnc_ativo_Visible = 1;
         cmbContratoSrvVnc_SemCusto_Jsonclick = "";
         cmbContratoSrvVnc_SemCusto.Enabled = 1;
         cmbContratoServicosVnc_ClonarLink_Jsonclick = "";
         cmbContratoServicosVnc_ClonarLink.Enabled = 1;
         cmbContratoServicosVnc_ClonaSrvOri_Jsonclick = "";
         cmbContratoServicosVnc_ClonaSrvOri.Enabled = 1;
         cmbContratoSrvVnc_NovoRspDmn_Jsonclick = "";
         cmbContratoSrvVnc_NovoRspDmn.Enabled = 1;
         cmbContratoSrvVnc_NovoStatusDmn_Jsonclick = "";
         cmbContratoSrvVnc_NovoStatusDmn.Enabled = 1;
         cmbContratoServicosVnc_Gestor_Jsonclick = "";
         cmbContratoServicosVnc_Gestor.Enabled = 1;
         cmbContratoSrvVnc_SrvVncRef_Jsonclick = "";
         cmbContratoSrvVnc_SrvVncRef.Enabled = 1;
         cmbContratoSrvVnc_VincularCom_Jsonclick = "";
         cmbContratoSrvVnc_VincularCom.Enabled = 1;
         lblTextblockcontratosrvvnc_vincularcom_Caption = "Vinculada com";
         dynavSelservico_codigo_Jsonclick = "";
         dynavSelservico_codigo.Enabled = 1;
         dynavSelservico_codigo.Visible = 1;
         lblTextblockselservico_codigo_Visible = 1;
         dynavSelcontrato_codigo_Jsonclick = "";
         dynavSelcontrato_codigo.Enabled = 1;
         dynavSelcontrato_codigo.Visible = 1;
         lblTextblockselcontrato_codigo_Visible = 1;
         cmbContratoSrvVnc_SrvVncStatus_Jsonclick = "";
         cmbContratoSrvVnc_SrvVncStatus.Enabled = 1;
         cmbContratoSrvVnc_PrestadoraCod_Jsonclick = "";
         cmbContratoSrvVnc_PrestadoraCod.Enabled = 1;
         cmbContratoSrvVnc_StatusDmn_Jsonclick = "";
         cmbContratoSrvVnc_StatusDmn.Enabled = 1;
         cmbContratoSrvVnc_DoStatusDmn_Jsonclick = "";
         cmbContratoSrvVnc_DoStatusDmn.Enabled = 1;
         edtContratoServicosVnc_Descricao_Jsonclick = "";
         edtContratoServicosVnc_Descricao_Enabled = 1;
         edtContratoSrvVnc_ServicoSigla_Jsonclick = "";
         edtContratoSrvVnc_ServicoSigla_Enabled = 0;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratoSrvVnc_Codigo_Jsonclick = "";
         edtContratoSrvVnc_Codigo_Enabled = 0;
         edtContratoSrvVnc_Codigo_Visible = 1;
         chkContratoServicosVnc_Ativo.Caption = "";
         chkContratoServicosVnc_NaoClonaInfo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXVvSELCONTRATO_CODIGO_html2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvSELCONTRATO_CODIGO2V114( int A1088ContratoSrvVnc_PrestadoraCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELCONTRATO_CODIGO_data2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELCONTRATO_CODIGO_html2V114( int A1088ContratoSrvVnc_PrestadoraCod )
      {
         int gxdynajaxvalue ;
         GXDLVvSELCONTRATO_CODIGO_data2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
         gxdynajaxindex = 1;
         dynavSelcontrato_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelcontrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvSELCONTRATO_CODIGO_data2V114( int A1088ContratoSrvVnc_PrestadoraCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002V48 */
         pr_default.execute(34, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         while ( (pr_default.getStatus(34) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002V48_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002V48_A77Contrato_Numero[0]));
            pr_default.readNext(34);
         }
         pr_default.close(34);
      }

      protected void GXDLVvSELSERVICO_CODIGO2V114( int AV20SelContrato_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELSERVICO_CODIGO_data2V114( AV20SelContrato_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELSERVICO_CODIGO_html2V114( int AV20SelContrato_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSELSERVICO_CODIGO_data2V114( AV20SelContrato_Codigo) ;
         gxdynajaxindex = 1;
         dynavSelservico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelservico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvSELSERVICO_CODIGO_data2V114( int AV20SelContrato_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T002V49 */
         pr_default.execute(35, new Object[] {AV20SelContrato_Codigo});
         while ( (pr_default.getStatus(35) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002V49_A160ContratoServicos_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002V49_A605Servico_Sigla[0]));
            pr_default.readNext(35);
         }
         pr_default.close(35);
      }

      protected void GX16ASACONTRATOSERVICOSVNC_GESTOR2V114( int A1088ContratoSrvVnc_PrestadoraCod )
      {
         GXt_char2 = "";
         new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
         if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ! ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
         {
            cmbContratoServicosVnc_Gestor.addItem("900000", "Gestor do Contrato", 0);
            n1438ContratoServicosVnc_Gestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
         }
         else
         {
            GXt_char2 = "";
            new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
            {
               cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
               n1438ContratoServicosVnc_Gestor = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
                  n1438ContratoServicosVnc_Gestor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
               }
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contratosrvvnc_prestadoracod( int GX_Parm1 ,
                                                      GXCombobox cmbGX_Parm2 ,
                                                      String GX_Parm3 ,
                                                      String GX_Parm4 ,
                                                      GXCombobox cmbGX_Parm5 ,
                                                      GXCombobox cmbGX_Parm6 ,
                                                      GXCombobox dynGX_Parm7 )
      {
         A917ContratoSrvVnc_Codigo = GX_Parm1;
         cmbContratoSrvVnc_PrestadoraCod = cmbGX_Parm2;
         A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cmbContratoSrvVnc_PrestadoraCod.CurrentValue, "."));
         n1088ContratoSrvVnc_PrestadoraCod = false;
         A1092ContratoSrvVnc_PrestadoraPesNom = GX_Parm3;
         n1092ContratoSrvVnc_PrestadoraPesNom = false;
         A1744ContratoSrvVnc_PrestadoraTpFab = GX_Parm4;
         n1744ContratoSrvVnc_PrestadoraTpFab = false;
         cmbContratoServicosVnc_Gestor = cmbGX_Parm5;
         A1438ContratoServicosVnc_Gestor = (int)(NumberUtil.Val( cmbContratoServicosVnc_Gestor.CurrentValue, "."));
         n1438ContratoServicosVnc_Gestor = false;
         cmbContratoServicosVnc_Gestor.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0));
         cmbContratoSrvVnc_NovoRspDmn = cmbGX_Parm6;
         A1801ContratoSrvVnc_NovoRspDmn = (int)(NumberUtil.Val( cmbContratoSrvVnc_NovoRspDmn.CurrentValue, "."));
         n1801ContratoSrvVnc_NovoRspDmn = false;
         cmbContratoSrvVnc_NovoRspDmn.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0));
         dynavSelcontrato_codigo = dynGX_Parm7;
         AV20SelContrato_Codigo = (int)(NumberUtil.Val( dynavSelcontrato_codigo.CurrentValue, "."));
         /* Using cursor T002V52 */
         pr_default.execute(36, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(36) != 101) )
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = T002V52_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = T002V52_n1092ContratoSrvVnc_PrestadoraPesNom[0];
         }
         else
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = "";
            n1092ContratoSrvVnc_PrestadoraPesNom = false;
         }
         pr_default.close(36);
         /* Using cursor T002V55 */
         pr_default.execute(37, new Object[] {n1088ContratoSrvVnc_PrestadoraCod, A1088ContratoSrvVnc_PrestadoraCod});
         if ( (pr_default.getStatus(37) != 101) )
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = T002V55_A1744ContratoSrvVnc_PrestadoraTpFab[0];
            n1744ContratoSrvVnc_PrestadoraTpFab = T002V55_n1744ContratoSrvVnc_PrestadoraTpFab[0];
         }
         else
         {
            A1744ContratoSrvVnc_PrestadoraTpFab = "";
            n1744ContratoSrvVnc_PrestadoraTpFab = false;
         }
         pr_default.close(37);
         GXt_char2 = "";
         new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
         if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ! ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
         {
            cmbContratoServicosVnc_Gestor.addItem("900000", "Gestor do Contrato", 0);
            n1438ContratoServicosVnc_Gestor = false;
            cmbContratoServicosVnc_Gestor.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0));
         }
         else
         {
            GXt_char2 = "";
            new prc_tipofabrica(context ).execute( ref  A1088ContratoSrvVnc_PrestadoraCod, out  GXt_char2) ;
            if ( ( A1088ContratoSrvVnc_PrestadoraCod > 0 ) && ( A1088ContratoSrvVnc_PrestadoraCod < 910000 ) && ( StringUtil.StrCmp(GXt_char2, "I") == 0 ) )
            {
               cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
               n1438ContratoServicosVnc_Gestor = false;
               cmbContratoServicosVnc_Gestor.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0));
            }
            else
            {
               if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
               {
                  cmbContratoServicosVnc_Gestor.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(900000), 6, 0)));
                  n1438ContratoServicosVnc_Gestor = false;
                  cmbContratoServicosVnc_Gestor.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0));
               }
            }
         }
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            A1801ContratoSrvVnc_NovoRspDmn = 0;
            n1801ContratoSrvVnc_NovoRspDmn = false;
            cmbContratoSrvVnc_NovoRspDmn.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0));
            n1801ContratoSrvVnc_NovoRspDmn = true;
            cmbContratoSrvVnc_NovoRspDmn.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0));
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               A1801ContratoSrvVnc_NovoRspDmn = A1088ContratoSrvVnc_PrestadoraCod;
               n1801ContratoSrvVnc_NovoRspDmn = false;
               cmbContratoSrvVnc_NovoRspDmn.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0));
            }
         }
         cmbContratoSrvVnc_NovoRspDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         cmbContratoSrvVnc_NovoStatusDmn.Enabled = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         if ( A1088ContratoSrvVnc_PrestadoraCod < 910000 )
         {
            lblTextblockcontratosrvvnc_vincularcom_Caption = "Vinculada com";
         }
         else
         {
            if ( A1088ContratoSrvVnc_PrestadoraCod >= 910000 )
            {
               lblTextblockcontratosrvvnc_vincularcom_Caption = "Voltar";
            }
         }
         lblTextblockselcontrato_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         dynavSelcontrato_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         lblTextblockselservico_codigo_Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         dynavSelservico_codigo.Visible = ((A1088ContratoSrvVnc_PrestadoraCod<910000) ? 1 : 0);
         if ( (0==A1088ContratoSrvVnc_PrestadoraCod) )
         {
            GX_msglist.addItem("A��o � obrigat�rio.", 1, "CONTRATOSRVVNC_PRESTADORACOD");
            AnyError = 1;
            GX_FocusControl = cmbContratoSrvVnc_PrestadoraCod_Internalname;
         }
         GXVvSELCONTRATO_CODIGO_html2V114( A1088ContratoSrvVnc_PrestadoraCod) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1092ContratoSrvVnc_PrestadoraPesNom = "";
            n1092ContratoSrvVnc_PrestadoraPesNom = false;
            A1744ContratoSrvVnc_PrestadoraTpFab = "";
            n1744ContratoSrvVnc_PrestadoraTpFab = false;
         }
         dynavSelcontrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0));
         isValidOutput.Add(StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom));
         isValidOutput.Add(StringUtil.RTrim( A1744ContratoSrvVnc_PrestadoraTpFab));
         cmbContratoServicosVnc_Gestor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0));
         isValidOutput.Add(cmbContratoServicosVnc_Gestor);
         cmbContratoSrvVnc_NovoRspDmn.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0));
         isValidOutput.Add(cmbContratoSrvVnc_NovoRspDmn);
         isValidOutput.Add(cmbContratoSrvVnc_NovoRspDmn.Enabled);
         isValidOutput.Add(cmbContratoSrvVnc_NovoStatusDmn.Enabled);
         isValidOutput.Add(lblTextblockcontratosrvvnc_vincularcom_Caption);
         isValidOutput.Add(lblTextblockselcontrato_codigo_Visible);
         isValidOutput.Add(dynavSelcontrato_codigo.Visible);
         isValidOutput.Add(lblTextblockselservico_codigo_Visible);
         isValidOutput.Add(dynavSelservico_codigo.Visible);
         if ( dynavSelcontrato_codigo.ItemCount > 0 )
         {
            AV20SelContrato_Codigo = (int)(NumberUtil.Val( dynavSelcontrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0))), "."));
         }
         dynavSelcontrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20SelContrato_Codigo), 6, 0));
         isValidOutput.Add(dynavSelcontrato_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Selcontrato_codigo( GXCombobox cmbGX_Parm1 ,
                                             GXCombobox dynGX_Parm2 ,
                                             GXCombobox dynGX_Parm3 )
      {
         cmbContratoSrvVnc_PrestadoraCod = cmbGX_Parm1;
         A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cmbContratoSrvVnc_PrestadoraCod.CurrentValue, "."));
         n1088ContratoSrvVnc_PrestadoraCod = false;
         dynavSelcontrato_codigo = dynGX_Parm2;
         AV20SelContrato_Codigo = (int)(NumberUtil.Val( dynavSelcontrato_codigo.CurrentValue, "."));
         dynavSelservico_codigo = dynGX_Parm3;
         AV22SelServico_Codigo = (int)(NumberUtil.Val( dynavSelservico_codigo.CurrentValue, "."));
         if ( ( A1088ContratoSrvVnc_PrestadoraCod < 900000 ) && ! new prc_temgestor(context).executeUdp( ref  AV20SelContrato_Codigo) )
         {
            GX_msglist.addItem("O contrato selecionado n�o tem Preposto/Gestor estabelecido!", 1, "vSELCONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynavSelcontrato_codigo_Internalname;
         }
         GXVvSELSERVICO_CODIGO_html2V114( AV20SelContrato_Codigo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0));
         if ( dynavSelservico_codigo.ItemCount > 0 )
         {
            AV22SelServico_Codigo = (int)(NumberUtil.Val( dynavSelservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0))), "."));
         }
         dynavSelservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22SelServico_Codigo), 6, 0));
         isValidOutput.Add(dynavSelservico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoSrvVnc_Codigo',fld:'vCONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV16ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122V2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null},{av:'AV16ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("CONTRATOSRVVNC_PRESTADORACOD.CLICK","{handler:'E132V2',iparms:[{av:'A1088ContratoSrvVnc_PrestadoraCod',fld:'CONTRATOSRVVNC_PRESTADORACOD',pic:'ZZZZZ9',nv:0},{av:'AV20SelContrato_Codigo',fld:'vSELCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20SelContrato_Codigo',fld:'vSELCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22SelServico_Codigo',fld:'vSELSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSELCONTRATO_CODIGO.CLICK","{handler:'E142V2',iparms:[{av:'AV20SelContrato_Codigo',fld:'vSELCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22SelServico_Codigo',fld:'vSELSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(27);
         pr_default.close(30);
         pr_default.close(28);
         pr_default.close(29);
         pr_default.close(31);
         pr_default.close(32);
         pr_default.close(36);
         pr_default.close(25);
         pr_default.close(37);
         pr_default.close(26);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1743ContratoSrvVnc_NovoStatusDmn = "";
         Z2108ContratoServicosVnc_Descricao = "";
         Z1663ContratoSrvVnc_SrvVncStatus = "";
         Z1800ContratoSrvVnc_DoStatusDmn = "";
         Z1084ContratoSrvVnc_StatusDmn = "";
         Z1745ContratoSrvVnc_VincularCom = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1800ContratoSrvVnc_DoStatusDmn = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A1663ContratoSrvVnc_SrvVncStatus = "";
         A1745ContratoSrvVnc_VincularCom = "";
         A1743ContratoSrvVnc_NovoStatusDmn = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockcontratosrvvnc_servicosigla_Jsonclick = "";
         A922ContratoSrvVnc_ServicoSigla = "";
         lblTextblockcontratoservicosvnc_descricao_Jsonclick = "";
         A2108ContratoServicosVnc_Descricao = "";
         lblTextblockcontratosrvvnc_dostatusdmn_Jsonclick = "";
         lblTextblockcontratosrvvnc_statusdmn_Jsonclick = "";
         lblTextblockcontratosrvvnc_prestadoracod_Jsonclick = "";
         lblTextblockcontratosrvvnc_srvvncstatus_Jsonclick = "";
         lblTextblockselcontrato_codigo_Jsonclick = "";
         lblTextblockselservico_codigo_Jsonclick = "";
         lblTextblockcontratosrvvnc_vincularcom_Jsonclick = "";
         lblTextblockcontratosrvvnc_srvvncref_Jsonclick = "";
         lblTextblockcontratoservicosvnc_gestor_Jsonclick = "";
         lblTextblockcontratosrvvnc_novostatusdmn_Jsonclick = "";
         lblTextblockcontratosrvvnc_novorspdmn_Jsonclick = "";
         lblTextblockcontratoservicosvnc_clonasrvori_Jsonclick = "";
         lblTextblockcontratoservicosvnc_clonarlink_Jsonclick = "";
         lblTextblockcontratoservicosvnc_naoclonainfo_Jsonclick = "";
         lblTextblockcontratosrvvnc_semcusto_Jsonclick = "";
         lblTextblockcontratoservicosvnc_ativo_Jsonclick = "";
         lblContratoservicosvnc_naoclonainfo_righttext_Jsonclick = "";
         A1092ContratoSrvVnc_PrestadoraPesNom = "";
         A1744ContratoSrvVnc_PrestadoraTpFab = "";
         A1629ContratoSrvVnc_SrvVncTpVnc = "";
         A1630ContratoSrvVnc_SrvVncTpDias = "";
         A1631ContratoSrvVnc_SrvVncCntNum = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         AV28Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode114 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV24Codigos = new GxSimpleCollection();
         AV26Descricoes = new GxSimpleCollection();
         AV27TipoFabrica = new GxSimpleCollection();
         AV23Descricao = "";
         Z922ContratoSrvVnc_ServicoSigla = "";
         Z1629ContratoSrvVnc_SrvVncTpVnc = "";
         Z1630ContratoSrvVnc_SrvVncTpDias = "";
         Z1631ContratoSrvVnc_SrvVncCntNum = "";
         Z924ContratoSrvVnc_ServicoVncSigla = "";
         T002V11_A1629ContratoSrvVnc_SrvVncTpVnc = new String[] {""} ;
         T002V11_n1629ContratoSrvVnc_SrvVncTpVnc = new bool[] {false} ;
         T002V11_A1630ContratoSrvVnc_SrvVncTpDias = new String[] {""} ;
         T002V11_n1630ContratoSrvVnc_SrvVncTpDias = new bool[] {false} ;
         T002V11_A1642ContratoSrvVnc_SrvVncVlrUndCnt = new decimal[1] ;
         T002V11_n1642ContratoSrvVnc_SrvVncVlrUndCnt = new bool[] {false} ;
         T002V11_A1651ContratoSrvVnc_SrvVncPrzInc = new short[1] ;
         T002V11_n1651ContratoSrvVnc_SrvVncPrzInc = new bool[] {false} ;
         T002V11_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         T002V11_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         T002V11_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         T002V11_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         T002V14_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         T002V14_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         T002V15_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         T002V15_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         T002V10_A933ContratoSrvVnc_ContratoCod = new int[1] ;
         T002V10_n933ContratoSrvVnc_ContratoCod = new bool[] {false} ;
         T002V10_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         T002V10_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         T002V12_A1662ContratoSrvVnc_ContratoAreaCod = new int[1] ;
         T002V12_n1662ContratoSrvVnc_ContratoAreaCod = new bool[] {false} ;
         T002V13_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         T002V13_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         T002V16_A917ContratoSrvVnc_Codigo = new int[1] ;
         T002V16_A1801ContratoSrvVnc_NovoRspDmn = new int[1] ;
         T002V16_n1801ContratoSrvVnc_NovoRspDmn = new bool[] {false} ;
         T002V16_A1743ContratoSrvVnc_NovoStatusDmn = new String[] {""} ;
         T002V16_n1743ContratoSrvVnc_NovoStatusDmn = new bool[] {false} ;
         T002V16_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         T002V16_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         T002V16_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         T002V16_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         T002V16_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         T002V16_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         T002V16_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         T002V16_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         T002V16_A1629ContratoSrvVnc_SrvVncTpVnc = new String[] {""} ;
         T002V16_n1629ContratoSrvVnc_SrvVncTpVnc = new bool[] {false} ;
         T002V16_A1630ContratoSrvVnc_SrvVncTpDias = new String[] {""} ;
         T002V16_n1630ContratoSrvVnc_SrvVncTpDias = new bool[] {false} ;
         T002V16_A1642ContratoSrvVnc_SrvVncVlrUndCnt = new decimal[1] ;
         T002V16_n1642ContratoSrvVnc_SrvVncVlrUndCnt = new bool[] {false} ;
         T002V16_A1651ContratoSrvVnc_SrvVncPrzInc = new short[1] ;
         T002V16_n1651ContratoSrvVnc_SrvVncPrzInc = new bool[] {false} ;
         T002V16_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         T002V16_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         T002V16_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         T002V16_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         T002V16_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         T002V16_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         T002V16_A1745ContratoSrvVnc_VincularCom = new String[] {""} ;
         T002V16_n1745ContratoSrvVnc_VincularCom = new bool[] {false} ;
         T002V16_A1821ContratoSrvVnc_SrvVncRef = new short[1] ;
         T002V16_n1821ContratoSrvVnc_SrvVncRef = new bool[] {false} ;
         T002V16_A1088ContratoSrvVnc_PrestadoraCod = new int[1] ;
         T002V16_n1088ContratoSrvVnc_PrestadoraCod = new bool[] {false} ;
         T002V16_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         T002V16_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         T002V16_A1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         T002V16_n1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         T002V16_A1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         T002V16_n1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         T002V16_A1438ContratoServicosVnc_Gestor = new int[1] ;
         T002V16_n1438ContratoServicosVnc_Gestor = new bool[] {false} ;
         T002V16_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         T002V16_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         T002V16_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         T002V16_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         T002V16_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         T002V16_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         T002V16_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         T002V16_A933ContratoSrvVnc_ContratoCod = new int[1] ;
         T002V16_n933ContratoSrvVnc_ContratoCod = new bool[] {false} ;
         T002V16_A1662ContratoSrvVnc_ContratoAreaCod = new int[1] ;
         T002V16_n1662ContratoSrvVnc_ContratoAreaCod = new bool[] {false} ;
         T002V16_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         T002V16_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         T002V16_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         T002V16_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         T002V16_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         T002V16_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         T002V6_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         T002V6_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         T002V9_A1744ContratoSrvVnc_PrestadoraTpFab = new String[] {""} ;
         T002V9_n1744ContratoSrvVnc_PrestadoraTpFab = new bool[] {false} ;
         T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         T002V19_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         T002V19_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         T002V22_A1744ContratoSrvVnc_PrestadoraTpFab = new String[] {""} ;
         T002V22_n1744ContratoSrvVnc_PrestadoraTpFab = new bool[] {false} ;
         T002V23_A933ContratoSrvVnc_ContratoCod = new int[1] ;
         T002V23_n933ContratoSrvVnc_ContratoCod = new bool[] {false} ;
         T002V23_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         T002V23_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         T002V24_A1662ContratoSrvVnc_ContratoAreaCod = new int[1] ;
         T002V24_n1662ContratoSrvVnc_ContratoAreaCod = new bool[] {false} ;
         T002V25_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         T002V25_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         T002V26_A1629ContratoSrvVnc_SrvVncTpVnc = new String[] {""} ;
         T002V26_n1629ContratoSrvVnc_SrvVncTpVnc = new bool[] {false} ;
         T002V26_A1630ContratoSrvVnc_SrvVncTpDias = new String[] {""} ;
         T002V26_n1630ContratoSrvVnc_SrvVncTpDias = new bool[] {false} ;
         T002V26_A1642ContratoSrvVnc_SrvVncVlrUndCnt = new decimal[1] ;
         T002V26_n1642ContratoSrvVnc_SrvVncVlrUndCnt = new bool[] {false} ;
         T002V26_A1651ContratoSrvVnc_SrvVncPrzInc = new short[1] ;
         T002V26_n1651ContratoSrvVnc_SrvVncPrzInc = new bool[] {false} ;
         T002V26_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         T002V26_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         T002V26_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         T002V26_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         T002V27_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         T002V27_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         T002V28_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         T002V28_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         T002V29_A917ContratoSrvVnc_Codigo = new int[1] ;
         T002V3_A917ContratoSrvVnc_Codigo = new int[1] ;
         T002V3_A1801ContratoSrvVnc_NovoRspDmn = new int[1] ;
         T002V3_n1801ContratoSrvVnc_NovoRspDmn = new bool[] {false} ;
         T002V3_A1743ContratoSrvVnc_NovoStatusDmn = new String[] {""} ;
         T002V3_n1743ContratoSrvVnc_NovoStatusDmn = new bool[] {false} ;
         T002V3_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         T002V3_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         T002V3_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         T002V3_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         T002V3_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         T002V3_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         T002V3_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         T002V3_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         T002V3_A1745ContratoSrvVnc_VincularCom = new String[] {""} ;
         T002V3_n1745ContratoSrvVnc_VincularCom = new bool[] {false} ;
         T002V3_A1821ContratoSrvVnc_SrvVncRef = new short[1] ;
         T002V3_n1821ContratoSrvVnc_SrvVncRef = new bool[] {false} ;
         T002V3_A1088ContratoSrvVnc_PrestadoraCod = new int[1] ;
         T002V3_n1088ContratoSrvVnc_PrestadoraCod = new bool[] {false} ;
         T002V3_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         T002V3_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         T002V3_A1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         T002V3_n1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         T002V3_A1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         T002V3_n1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         T002V3_A1438ContratoServicosVnc_Gestor = new int[1] ;
         T002V3_n1438ContratoServicosVnc_Gestor = new bool[] {false} ;
         T002V3_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         T002V3_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         T002V3_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         T002V3_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         T002V3_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         T002V3_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         T002V30_A917ContratoSrvVnc_Codigo = new int[1] ;
         T002V31_A917ContratoSrvVnc_Codigo = new int[1] ;
         T002V2_A917ContratoSrvVnc_Codigo = new int[1] ;
         T002V2_A1801ContratoSrvVnc_NovoRspDmn = new int[1] ;
         T002V2_n1801ContratoSrvVnc_NovoRspDmn = new bool[] {false} ;
         T002V2_A1743ContratoSrvVnc_NovoStatusDmn = new String[] {""} ;
         T002V2_n1743ContratoSrvVnc_NovoStatusDmn = new bool[] {false} ;
         T002V2_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         T002V2_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         T002V2_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         T002V2_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         T002V2_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         T002V2_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         T002V2_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         T002V2_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         T002V2_A1745ContratoSrvVnc_VincularCom = new String[] {""} ;
         T002V2_n1745ContratoSrvVnc_VincularCom = new bool[] {false} ;
         T002V2_A1821ContratoSrvVnc_SrvVncRef = new short[1] ;
         T002V2_n1821ContratoSrvVnc_SrvVncRef = new bool[] {false} ;
         T002V2_A1088ContratoSrvVnc_PrestadoraCod = new int[1] ;
         T002V2_n1088ContratoSrvVnc_PrestadoraCod = new bool[] {false} ;
         T002V2_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         T002V2_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         T002V2_A1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         T002V2_n1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         T002V2_A1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         T002V2_n1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         T002V2_A1438ContratoServicosVnc_Gestor = new int[1] ;
         T002V2_n1438ContratoServicosVnc_Gestor = new bool[] {false} ;
         T002V2_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         T002V2_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         T002V2_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         T002V2_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         T002V2_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         T002V2_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         T002V2_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         T002V32_A917ContratoSrvVnc_Codigo = new int[1] ;
         T002V37_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         T002V37_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         T002V40_A1744ContratoSrvVnc_PrestadoraTpFab = new String[] {""} ;
         T002V40_n1744ContratoSrvVnc_PrestadoraTpFab = new bool[] {false} ;
         T002V41_A933ContratoSrvVnc_ContratoCod = new int[1] ;
         T002V41_n933ContratoSrvVnc_ContratoCod = new bool[] {false} ;
         T002V41_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         T002V41_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         T002V42_A1662ContratoSrvVnc_ContratoAreaCod = new int[1] ;
         T002V42_n1662ContratoSrvVnc_ContratoAreaCod = new bool[] {false} ;
         T002V43_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         T002V43_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         T002V44_A1629ContratoSrvVnc_SrvVncTpVnc = new String[] {""} ;
         T002V44_n1629ContratoSrvVnc_SrvVncTpVnc = new bool[] {false} ;
         T002V44_A1630ContratoSrvVnc_SrvVncTpDias = new String[] {""} ;
         T002V44_n1630ContratoSrvVnc_SrvVncTpDias = new bool[] {false} ;
         T002V44_A1642ContratoSrvVnc_SrvVncVlrUndCnt = new decimal[1] ;
         T002V44_n1642ContratoSrvVnc_SrvVncVlrUndCnt = new bool[] {false} ;
         T002V44_A1651ContratoSrvVnc_SrvVncPrzInc = new short[1] ;
         T002V44_n1651ContratoSrvVnc_SrvVncPrzInc = new bool[] {false} ;
         T002V44_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         T002V44_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         T002V44_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         T002V44_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         T002V45_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         T002V45_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         T002V46_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         T002V46_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         T002V47_A917ContratoSrvVnc_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i1745ContratoSrvVnc_VincularCom = "";
         i1663ContratoSrvVnc_SrvVncStatus = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002V48_A74Contrato_Codigo = new int[1] ;
         T002V48_A77Contrato_Numero = new String[] {""} ;
         T002V48_A39Contratada_Codigo = new int[1] ;
         T002V49_A155Servico_Codigo = new int[1] ;
         T002V49_A160ContratoServicos_Codigo = new int[1] ;
         T002V49_A605Servico_Sigla = new String[] {""} ;
         T002V52_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         T002V52_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         T002V55_A1744ContratoSrvVnc_PrestadoraTpFab = new String[] {""} ;
         T002V55_n1744ContratoSrvVnc_PrestadoraTpFab = new bool[] {false} ;
         GXt_char2 = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosvnc__default(),
            new Object[][] {
                new Object[] {
               T002V2_A917ContratoSrvVnc_Codigo, T002V2_A1801ContratoSrvVnc_NovoRspDmn, T002V2_n1801ContratoSrvVnc_NovoRspDmn, T002V2_A1743ContratoSrvVnc_NovoStatusDmn, T002V2_n1743ContratoSrvVnc_NovoStatusDmn, T002V2_A2108ContratoServicosVnc_Descricao, T002V2_n2108ContratoServicosVnc_Descricao, T002V2_A1663ContratoSrvVnc_SrvVncStatus, T002V2_n1663ContratoSrvVnc_SrvVncStatus, T002V2_A1800ContratoSrvVnc_DoStatusDmn,
               T002V2_n1800ContratoSrvVnc_DoStatusDmn, T002V2_A1084ContratoSrvVnc_StatusDmn, T002V2_n1084ContratoSrvVnc_StatusDmn, T002V2_A1745ContratoSrvVnc_VincularCom, T002V2_n1745ContratoSrvVnc_VincularCom, T002V2_A1821ContratoSrvVnc_SrvVncRef, T002V2_n1821ContratoSrvVnc_SrvVncRef, T002V2_A1088ContratoSrvVnc_PrestadoraCod, T002V2_n1088ContratoSrvVnc_PrestadoraCod, T002V2_A1090ContratoSrvVnc_SemCusto,
               T002V2_n1090ContratoSrvVnc_SemCusto, T002V2_A1145ContratoServicosVnc_ClonaSrvOri, T002V2_n1145ContratoServicosVnc_ClonaSrvOri, T002V2_A1437ContratoServicosVnc_NaoClonaInfo, T002V2_n1437ContratoServicosVnc_NaoClonaInfo, T002V2_A1438ContratoServicosVnc_Gestor, T002V2_n1438ContratoServicosVnc_Gestor, T002V2_A1818ContratoServicosVnc_ClonarLink, T002V2_n1818ContratoServicosVnc_ClonarLink, T002V2_A1453ContratoServicosVnc_Ativo,
               T002V2_n1453ContratoServicosVnc_Ativo, T002V2_A915ContratoSrvVnc_CntSrvCod, T002V2_A1589ContratoSrvVnc_SrvVncCntSrvCod, T002V2_n1589ContratoSrvVnc_SrvVncCntSrvCod
               }
               , new Object[] {
               T002V3_A917ContratoSrvVnc_Codigo, T002V3_A1801ContratoSrvVnc_NovoRspDmn, T002V3_n1801ContratoSrvVnc_NovoRspDmn, T002V3_A1743ContratoSrvVnc_NovoStatusDmn, T002V3_n1743ContratoSrvVnc_NovoStatusDmn, T002V3_A2108ContratoServicosVnc_Descricao, T002V3_n2108ContratoServicosVnc_Descricao, T002V3_A1663ContratoSrvVnc_SrvVncStatus, T002V3_n1663ContratoSrvVnc_SrvVncStatus, T002V3_A1800ContratoSrvVnc_DoStatusDmn,
               T002V3_n1800ContratoSrvVnc_DoStatusDmn, T002V3_A1084ContratoSrvVnc_StatusDmn, T002V3_n1084ContratoSrvVnc_StatusDmn, T002V3_A1745ContratoSrvVnc_VincularCom, T002V3_n1745ContratoSrvVnc_VincularCom, T002V3_A1821ContratoSrvVnc_SrvVncRef, T002V3_n1821ContratoSrvVnc_SrvVncRef, T002V3_A1088ContratoSrvVnc_PrestadoraCod, T002V3_n1088ContratoSrvVnc_PrestadoraCod, T002V3_A1090ContratoSrvVnc_SemCusto,
               T002V3_n1090ContratoSrvVnc_SemCusto, T002V3_A1145ContratoServicosVnc_ClonaSrvOri, T002V3_n1145ContratoServicosVnc_ClonaSrvOri, T002V3_A1437ContratoServicosVnc_NaoClonaInfo, T002V3_n1437ContratoServicosVnc_NaoClonaInfo, T002V3_A1438ContratoServicosVnc_Gestor, T002V3_n1438ContratoServicosVnc_Gestor, T002V3_A1818ContratoServicosVnc_ClonarLink, T002V3_n1818ContratoServicosVnc_ClonarLink, T002V3_A1453ContratoServicosVnc_Ativo,
               T002V3_n1453ContratoServicosVnc_Ativo, T002V3_A915ContratoSrvVnc_CntSrvCod, T002V3_A1589ContratoSrvVnc_SrvVncCntSrvCod, T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod
               }
               , new Object[] {
               T002V6_A1092ContratoSrvVnc_PrestadoraPesNom, T002V6_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               T002V9_A1744ContratoSrvVnc_PrestadoraTpFab, T002V9_n1744ContratoSrvVnc_PrestadoraTpFab
               }
               , new Object[] {
               T002V10_A933ContratoSrvVnc_ContratoCod, T002V10_n933ContratoSrvVnc_ContratoCod, T002V10_A921ContratoSrvVnc_ServicoCod, T002V10_n921ContratoSrvVnc_ServicoCod
               }
               , new Object[] {
               T002V11_A1629ContratoSrvVnc_SrvVncTpVnc, T002V11_n1629ContratoSrvVnc_SrvVncTpVnc, T002V11_A1630ContratoSrvVnc_SrvVncTpDias, T002V11_n1630ContratoSrvVnc_SrvVncTpDias, T002V11_A1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V11_n1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V11_A1651ContratoSrvVnc_SrvVncPrzInc, T002V11_n1651ContratoSrvVnc_SrvVncPrzInc, T002V11_A1628ContratoSrvVnc_SrvVncCntCod, T002V11_n1628ContratoSrvVnc_SrvVncCntCod,
               T002V11_A923ContratoSrvVnc_ServicoVncCod, T002V11_n923ContratoSrvVnc_ServicoVncCod
               }
               , new Object[] {
               T002V12_A1662ContratoSrvVnc_ContratoAreaCod, T002V12_n1662ContratoSrvVnc_ContratoAreaCod
               }
               , new Object[] {
               T002V13_A922ContratoSrvVnc_ServicoSigla, T002V13_n922ContratoSrvVnc_ServicoSigla
               }
               , new Object[] {
               T002V14_A1631ContratoSrvVnc_SrvVncCntNum, T002V14_n1631ContratoSrvVnc_SrvVncCntNum
               }
               , new Object[] {
               T002V15_A924ContratoSrvVnc_ServicoVncSigla, T002V15_n924ContratoSrvVnc_ServicoVncSigla
               }
               , new Object[] {
               T002V16_A917ContratoSrvVnc_Codigo, T002V16_A1801ContratoSrvVnc_NovoRspDmn, T002V16_n1801ContratoSrvVnc_NovoRspDmn, T002V16_A1743ContratoSrvVnc_NovoStatusDmn, T002V16_n1743ContratoSrvVnc_NovoStatusDmn, T002V16_A2108ContratoServicosVnc_Descricao, T002V16_n2108ContratoServicosVnc_Descricao, T002V16_A922ContratoSrvVnc_ServicoSigla, T002V16_n922ContratoSrvVnc_ServicoSigla, T002V16_A1631ContratoSrvVnc_SrvVncCntNum,
               T002V16_n1631ContratoSrvVnc_SrvVncCntNum, T002V16_A924ContratoSrvVnc_ServicoVncSigla, T002V16_n924ContratoSrvVnc_ServicoVncSigla, T002V16_A1629ContratoSrvVnc_SrvVncTpVnc, T002V16_n1629ContratoSrvVnc_SrvVncTpVnc, T002V16_A1630ContratoSrvVnc_SrvVncTpDias, T002V16_n1630ContratoSrvVnc_SrvVncTpDias, T002V16_A1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V16_n1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V16_A1651ContratoSrvVnc_SrvVncPrzInc,
               T002V16_n1651ContratoSrvVnc_SrvVncPrzInc, T002V16_A1663ContratoSrvVnc_SrvVncStatus, T002V16_n1663ContratoSrvVnc_SrvVncStatus, T002V16_A1800ContratoSrvVnc_DoStatusDmn, T002V16_n1800ContratoSrvVnc_DoStatusDmn, T002V16_A1084ContratoSrvVnc_StatusDmn, T002V16_n1084ContratoSrvVnc_StatusDmn, T002V16_A1745ContratoSrvVnc_VincularCom, T002V16_n1745ContratoSrvVnc_VincularCom, T002V16_A1821ContratoSrvVnc_SrvVncRef,
               T002V16_n1821ContratoSrvVnc_SrvVncRef, T002V16_A1088ContratoSrvVnc_PrestadoraCod, T002V16_n1088ContratoSrvVnc_PrestadoraCod, T002V16_A1090ContratoSrvVnc_SemCusto, T002V16_n1090ContratoSrvVnc_SemCusto, T002V16_A1145ContratoServicosVnc_ClonaSrvOri, T002V16_n1145ContratoServicosVnc_ClonaSrvOri, T002V16_A1437ContratoServicosVnc_NaoClonaInfo, T002V16_n1437ContratoServicosVnc_NaoClonaInfo, T002V16_A1438ContratoServicosVnc_Gestor,
               T002V16_n1438ContratoServicosVnc_Gestor, T002V16_A1818ContratoServicosVnc_ClonarLink, T002V16_n1818ContratoServicosVnc_ClonarLink, T002V16_A1453ContratoServicosVnc_Ativo, T002V16_n1453ContratoServicosVnc_Ativo, T002V16_A915ContratoSrvVnc_CntSrvCod, T002V16_A1589ContratoSrvVnc_SrvVncCntSrvCod, T002V16_n1589ContratoSrvVnc_SrvVncCntSrvCod, T002V16_A933ContratoSrvVnc_ContratoCod, T002V16_n933ContratoSrvVnc_ContratoCod,
               T002V16_A1662ContratoSrvVnc_ContratoAreaCod, T002V16_n1662ContratoSrvVnc_ContratoAreaCod, T002V16_A921ContratoSrvVnc_ServicoCod, T002V16_n921ContratoSrvVnc_ServicoCod, T002V16_A1628ContratoSrvVnc_SrvVncCntCod, T002V16_n1628ContratoSrvVnc_SrvVncCntCod, T002V16_A923ContratoSrvVnc_ServicoVncCod, T002V16_n923ContratoSrvVnc_ServicoVncCod
               }
               , new Object[] {
               T002V19_A1092ContratoSrvVnc_PrestadoraPesNom, T002V19_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               T002V22_A1744ContratoSrvVnc_PrestadoraTpFab, T002V22_n1744ContratoSrvVnc_PrestadoraTpFab
               }
               , new Object[] {
               T002V23_A933ContratoSrvVnc_ContratoCod, T002V23_n933ContratoSrvVnc_ContratoCod, T002V23_A921ContratoSrvVnc_ServicoCod, T002V23_n921ContratoSrvVnc_ServicoCod
               }
               , new Object[] {
               T002V24_A1662ContratoSrvVnc_ContratoAreaCod, T002V24_n1662ContratoSrvVnc_ContratoAreaCod
               }
               , new Object[] {
               T002V25_A922ContratoSrvVnc_ServicoSigla, T002V25_n922ContratoSrvVnc_ServicoSigla
               }
               , new Object[] {
               T002V26_A1629ContratoSrvVnc_SrvVncTpVnc, T002V26_n1629ContratoSrvVnc_SrvVncTpVnc, T002V26_A1630ContratoSrvVnc_SrvVncTpDias, T002V26_n1630ContratoSrvVnc_SrvVncTpDias, T002V26_A1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V26_n1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V26_A1651ContratoSrvVnc_SrvVncPrzInc, T002V26_n1651ContratoSrvVnc_SrvVncPrzInc, T002V26_A1628ContratoSrvVnc_SrvVncCntCod, T002V26_n1628ContratoSrvVnc_SrvVncCntCod,
               T002V26_A923ContratoSrvVnc_ServicoVncCod, T002V26_n923ContratoSrvVnc_ServicoVncCod
               }
               , new Object[] {
               T002V27_A1631ContratoSrvVnc_SrvVncCntNum, T002V27_n1631ContratoSrvVnc_SrvVncCntNum
               }
               , new Object[] {
               T002V28_A924ContratoSrvVnc_ServicoVncSigla, T002V28_n924ContratoSrvVnc_ServicoVncSigla
               }
               , new Object[] {
               T002V29_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               T002V30_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               T002V31_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               T002V32_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002V37_A1092ContratoSrvVnc_PrestadoraPesNom, T002V37_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               T002V40_A1744ContratoSrvVnc_PrestadoraTpFab, T002V40_n1744ContratoSrvVnc_PrestadoraTpFab
               }
               , new Object[] {
               T002V41_A933ContratoSrvVnc_ContratoCod, T002V41_n933ContratoSrvVnc_ContratoCod, T002V41_A921ContratoSrvVnc_ServicoCod, T002V41_n921ContratoSrvVnc_ServicoCod
               }
               , new Object[] {
               T002V42_A1662ContratoSrvVnc_ContratoAreaCod, T002V42_n1662ContratoSrvVnc_ContratoAreaCod
               }
               , new Object[] {
               T002V43_A922ContratoSrvVnc_ServicoSigla, T002V43_n922ContratoSrvVnc_ServicoSigla
               }
               , new Object[] {
               T002V44_A1629ContratoSrvVnc_SrvVncTpVnc, T002V44_n1629ContratoSrvVnc_SrvVncTpVnc, T002V44_A1630ContratoSrvVnc_SrvVncTpDias, T002V44_n1630ContratoSrvVnc_SrvVncTpDias, T002V44_A1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V44_n1642ContratoSrvVnc_SrvVncVlrUndCnt, T002V44_A1651ContratoSrvVnc_SrvVncPrzInc, T002V44_n1651ContratoSrvVnc_SrvVncPrzInc, T002V44_A1628ContratoSrvVnc_SrvVncCntCod, T002V44_n1628ContratoSrvVnc_SrvVncCntCod,
               T002V44_A923ContratoSrvVnc_ServicoVncCod, T002V44_n923ContratoSrvVnc_ServicoVncCod
               }
               , new Object[] {
               T002V45_A1631ContratoSrvVnc_SrvVncCntNum, T002V45_n1631ContratoSrvVnc_SrvVncCntNum
               }
               , new Object[] {
               T002V46_A924ContratoSrvVnc_ServicoVncSigla, T002V46_n924ContratoSrvVnc_ServicoVncSigla
               }
               , new Object[] {
               T002V47_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               T002V48_A74Contrato_Codigo, T002V48_A77Contrato_Numero, T002V48_A39Contratada_Codigo
               }
               , new Object[] {
               T002V49_A155Servico_Codigo, T002V49_A160ContratoServicos_Codigo, T002V49_A605Servico_Sigla
               }
               , new Object[] {
               T002V52_A1092ContratoSrvVnc_PrestadoraPesNom, T002V52_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               T002V55_A1744ContratoSrvVnc_PrestadoraTpFab, T002V55_n1744ContratoSrvVnc_PrestadoraTpFab
               }
            }
         );
         Z1453ContratoServicosVnc_Ativo = true;
         n1453ContratoServicosVnc_Ativo = false;
         A1453ContratoServicosVnc_Ativo = true;
         n1453ContratoServicosVnc_Ativo = false;
         i1453ContratoServicosVnc_Ativo = true;
         n1453ContratoServicosVnc_Ativo = false;
         Z1745ContratoSrvVnc_VincularCom = "D";
         n1745ContratoSrvVnc_VincularCom = false;
         A1745ContratoSrvVnc_VincularCom = "D";
         n1745ContratoSrvVnc_VincularCom = false;
         i1745ContratoSrvVnc_VincularCom = "D";
         n1745ContratoSrvVnc_VincularCom = false;
         Z1663ContratoSrvVnc_SrvVncStatus = "S";
         n1663ContratoSrvVnc_SrvVncStatus = false;
         A1663ContratoSrvVnc_SrvVncStatus = "S";
         n1663ContratoSrvVnc_SrvVncStatus = false;
         i1663ContratoSrvVnc_SrvVncStatus = "S";
         n1663ContratoSrvVnc_SrvVncStatus = false;
         AV28Pgmname = "ContratoServicosVnc";
      }

      private short Z1821ContratoSrvVnc_SrvVncRef ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1821ContratoSrvVnc_SrvVncRef ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short A1651ContratoSrvVnc_SrvVncPrzInc ;
      private short RcdFound114 ;
      private short AV25i ;
      private short GX_JID ;
      private short Z1651ContratoSrvVnc_SrvVncPrzInc ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoSrvVnc_Codigo ;
      private int wcpOAV16ContratoSrvVnc_CntSrvCod ;
      private int Z917ContratoSrvVnc_Codigo ;
      private int Z1801ContratoSrvVnc_NovoRspDmn ;
      private int Z1088ContratoSrvVnc_PrestadoraCod ;
      private int Z1438ContratoServicosVnc_Gestor ;
      private int Z915ContratoSrvVnc_CntSrvCod ;
      private int Z1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int N915ContratoSrvVnc_CntSrvCod ;
      private int N1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1088ContratoSrvVnc_PrestadoraCod ;
      private int AV20SelContrato_Codigo ;
      private int A917ContratoSrvVnc_Codigo ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A933ContratoSrvVnc_ContratoCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1628ContratoSrvVnc_SrvVncCntCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int AV7ContratoSrvVnc_Codigo ;
      private int AV16ContratoSrvVnc_CntSrvCod ;
      private int A1438ContratoServicosVnc_Gestor ;
      private int A1801ContratoSrvVnc_NovoRspDmn ;
      private int trnEnded ;
      private int AV22SelServico_Codigo ;
      private int edtContratoSrvVnc_Codigo_Enabled ;
      private int edtContratoSrvVnc_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtContratoSrvVnc_ServicoSigla_Enabled ;
      private int edtContratoServicosVnc_Descricao_Enabled ;
      private int lblTextblockselcontrato_codigo_Visible ;
      private int lblTextblockselservico_codigo_Visible ;
      private int lblTextblockcontratoservicosvnc_ativo_Visible ;
      private int AV11Insert_ContratoSrvVnc_CntSrvCod ;
      private int AV17Insert_ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1662ContratoSrvVnc_ContratoAreaCod ;
      private int AV29GXV1 ;
      private int GXt_int1 ;
      private int Z933ContratoSrvVnc_ContratoCod ;
      private int Z921ContratoSrvVnc_ServicoCod ;
      private int Z1662ContratoSrvVnc_ContratoAreaCod ;
      private int Z1628ContratoSrvVnc_SrvVncCntCod ;
      private int Z923ContratoSrvVnc_ServicoVncCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal A1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private decimal Z1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1743ContratoSrvVnc_NovoStatusDmn ;
      private String Z1663ContratoSrvVnc_SrvVncStatus ;
      private String Z1800ContratoSrvVnc_DoStatusDmn ;
      private String Z1084ContratoSrvVnc_StatusDmn ;
      private String Z1745ContratoSrvVnc_VincularCom ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A1800ContratoSrvVnc_DoStatusDmn ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A1663ContratoSrvVnc_SrvVncStatus ;
      private String A1745ContratoSrvVnc_VincularCom ;
      private String A1743ContratoSrvVnc_NovoStatusDmn ;
      private String chkContratoServicosVnc_NaoClonaInfo_Internalname ;
      private String chkContratoServicosVnc_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoServicosVnc_Descricao_Internalname ;
      private String edtContratoSrvVnc_Codigo_Internalname ;
      private String edtContratoSrvVnc_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratosrvvnc_servicosigla_Internalname ;
      private String lblTextblockcontratosrvvnc_servicosigla_Jsonclick ;
      private String edtContratoSrvVnc_ServicoSigla_Internalname ;
      private String A922ContratoSrvVnc_ServicoSigla ;
      private String edtContratoSrvVnc_ServicoSigla_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_descricao_Internalname ;
      private String lblTextblockcontratoservicosvnc_descricao_Jsonclick ;
      private String edtContratoServicosVnc_Descricao_Jsonclick ;
      private String lblTextblockcontratosrvvnc_dostatusdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_dostatusdmn_Jsonclick ;
      private String cmbContratoSrvVnc_DoStatusDmn_Internalname ;
      private String cmbContratoSrvVnc_DoStatusDmn_Jsonclick ;
      private String lblTextblockcontratosrvvnc_statusdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_statusdmn_Jsonclick ;
      private String cmbContratoSrvVnc_StatusDmn_Internalname ;
      private String cmbContratoSrvVnc_StatusDmn_Jsonclick ;
      private String lblTextblockcontratosrvvnc_prestadoracod_Internalname ;
      private String lblTextblockcontratosrvvnc_prestadoracod_Jsonclick ;
      private String cmbContratoSrvVnc_PrestadoraCod_Internalname ;
      private String cmbContratoSrvVnc_PrestadoraCod_Jsonclick ;
      private String lblTextblockcontratosrvvnc_srvvncstatus_Internalname ;
      private String lblTextblockcontratosrvvnc_srvvncstatus_Jsonclick ;
      private String cmbContratoSrvVnc_SrvVncStatus_Internalname ;
      private String cmbContratoSrvVnc_SrvVncStatus_Jsonclick ;
      private String lblTextblockselcontrato_codigo_Internalname ;
      private String lblTextblockselcontrato_codigo_Jsonclick ;
      private String dynavSelcontrato_codigo_Internalname ;
      private String dynavSelcontrato_codigo_Jsonclick ;
      private String lblTextblockselservico_codigo_Internalname ;
      private String lblTextblockselservico_codigo_Jsonclick ;
      private String dynavSelservico_codigo_Internalname ;
      private String dynavSelservico_codigo_Jsonclick ;
      private String lblTextblockcontratosrvvnc_vincularcom_Internalname ;
      private String lblTextblockcontratosrvvnc_vincularcom_Caption ;
      private String lblTextblockcontratosrvvnc_vincularcom_Jsonclick ;
      private String cmbContratoSrvVnc_VincularCom_Internalname ;
      private String cmbContratoSrvVnc_VincularCom_Jsonclick ;
      private String lblTextblockcontratosrvvnc_srvvncref_Internalname ;
      private String lblTextblockcontratosrvvnc_srvvncref_Jsonclick ;
      private String cmbContratoSrvVnc_SrvVncRef_Internalname ;
      private String cmbContratoSrvVnc_SrvVncRef_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_gestor_Internalname ;
      private String lblTextblockcontratoservicosvnc_gestor_Jsonclick ;
      private String cmbContratoServicosVnc_Gestor_Internalname ;
      private String cmbContratoServicosVnc_Gestor_Jsonclick ;
      private String lblTextblockcontratosrvvnc_novostatusdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_novostatusdmn_Jsonclick ;
      private String cmbContratoSrvVnc_NovoStatusDmn_Internalname ;
      private String cmbContratoSrvVnc_NovoStatusDmn_Jsonclick ;
      private String lblTextblockcontratosrvvnc_novorspdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_novorspdmn_Jsonclick ;
      private String cmbContratoSrvVnc_NovoRspDmn_Internalname ;
      private String cmbContratoSrvVnc_NovoRspDmn_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_clonasrvori_Internalname ;
      private String lblTextblockcontratoservicosvnc_clonasrvori_Jsonclick ;
      private String cmbContratoServicosVnc_ClonaSrvOri_Internalname ;
      private String cmbContratoServicosVnc_ClonaSrvOri_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_clonarlink_Internalname ;
      private String lblTextblockcontratoservicosvnc_clonarlink_Jsonclick ;
      private String cmbContratoServicosVnc_ClonarLink_Internalname ;
      private String cmbContratoServicosVnc_ClonarLink_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_naoclonainfo_Internalname ;
      private String lblTextblockcontratoservicosvnc_naoclonainfo_Jsonclick ;
      private String lblTextblockcontratosrvvnc_semcusto_Internalname ;
      private String lblTextblockcontratosrvvnc_semcusto_Jsonclick ;
      private String cmbContratoSrvVnc_SemCusto_Internalname ;
      private String cmbContratoSrvVnc_SemCusto_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_ativo_Internalname ;
      private String lblTextblockcontratoservicosvnc_ativo_Jsonclick ;
      private String tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname ;
      private String lblContratoservicosvnc_naoclonainfo_righttext_Internalname ;
      private String lblContratoservicosvnc_naoclonainfo_righttext_Jsonclick ;
      private String A1092ContratoSrvVnc_PrestadoraPesNom ;
      private String A1744ContratoSrvVnc_PrestadoraTpFab ;
      private String A1629ContratoSrvVnc_SrvVncTpVnc ;
      private String A1630ContratoSrvVnc_SrvVncTpDias ;
      private String A1631ContratoSrvVnc_SrvVncCntNum ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String AV28Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode114 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z922ContratoSrvVnc_ServicoSigla ;
      private String Z1629ContratoSrvVnc_SrvVncTpVnc ;
      private String Z1630ContratoSrvVnc_SrvVncTpDias ;
      private String Z1631ContratoSrvVnc_SrvVncCntNum ;
      private String Z924ContratoSrvVnc_ServicoVncSigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i1745ContratoSrvVnc_VincularCom ;
      private String i1663ContratoSrvVnc_SrvVncStatus ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private String GXt_char2 ;
      private bool Z1090ContratoSrvVnc_SemCusto ;
      private bool Z1145ContratoServicosVnc_ClonaSrvOri ;
      private bool Z1437ContratoServicosVnc_NaoClonaInfo ;
      private bool Z1818ContratoServicosVnc_ClonarLink ;
      private bool Z1453ContratoServicosVnc_Ativo ;
      private bool entryPointCalled ;
      private bool n1088ContratoSrvVnc_PrestadoraCod ;
      private bool n933ContratoSrvVnc_ContratoCod ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n1628ContratoSrvVnc_SrvVncCntCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool toggleJsOutput ;
      private bool n1800ContratoSrvVnc_DoStatusDmn ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n1663ContratoSrvVnc_SrvVncStatus ;
      private bool n1745ContratoSrvVnc_VincularCom ;
      private bool n1821ContratoSrvVnc_SrvVncRef ;
      private bool n1438ContratoServicosVnc_Gestor ;
      private bool n1743ContratoSrvVnc_NovoStatusDmn ;
      private bool n1801ContratoSrvVnc_NovoRspDmn ;
      private bool A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool A1818ContratoServicosVnc_ClonarLink ;
      private bool n1818ContratoServicosVnc_ClonarLink ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool wbErr ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool n922ContratoSrvVnc_ServicoSigla ;
      private bool n2108ContratoServicosVnc_Descricao ;
      private bool n1437ContratoServicosVnc_NaoClonaInfo ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool n1744ContratoSrvVnc_PrestadoraTpFab ;
      private bool n1629ContratoSrvVnc_SrvVncTpVnc ;
      private bool n1630ContratoSrvVnc_SrvVncTpDias ;
      private bool n1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private bool n1651ContratoSrvVnc_SrvVncPrzInc ;
      private bool n1662ContratoSrvVnc_ContratoAreaCod ;
      private bool n1631ContratoSrvVnc_SrvVncCntNum ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i1453ContratoServicosVnc_Ativo ;
      private String Z2108ContratoServicosVnc_Descricao ;
      private String A2108ContratoServicosVnc_Descricao ;
      private String AV23Descricao ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ContratoSrvVnc_CntSrvCod ;
      private GXCombobox cmbContratoSrvVnc_DoStatusDmn ;
      private GXCombobox cmbContratoSrvVnc_StatusDmn ;
      private GXCombobox cmbContratoSrvVnc_PrestadoraCod ;
      private GXCombobox cmbContratoSrvVnc_SrvVncStatus ;
      private GXCombobox dynavSelcontrato_codigo ;
      private GXCombobox dynavSelservico_codigo ;
      private GXCombobox cmbContratoSrvVnc_VincularCom ;
      private GXCombobox cmbContratoSrvVnc_SrvVncRef ;
      private GXCombobox cmbContratoServicosVnc_Gestor ;
      private GXCombobox cmbContratoSrvVnc_NovoStatusDmn ;
      private GXCombobox cmbContratoSrvVnc_NovoRspDmn ;
      private GXCombobox cmbContratoServicosVnc_ClonaSrvOri ;
      private GXCombobox cmbContratoServicosVnc_ClonarLink ;
      private GXCheckbox chkContratoServicosVnc_NaoClonaInfo ;
      private GXCombobox cmbContratoSrvVnc_SemCusto ;
      private GXCheckbox chkContratoServicosVnc_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T002V11_A1629ContratoSrvVnc_SrvVncTpVnc ;
      private bool[] T002V11_n1629ContratoSrvVnc_SrvVncTpVnc ;
      private String[] T002V11_A1630ContratoSrvVnc_SrvVncTpDias ;
      private bool[] T002V11_n1630ContratoSrvVnc_SrvVncTpDias ;
      private decimal[] T002V11_A1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private bool[] T002V11_n1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private short[] T002V11_A1651ContratoSrvVnc_SrvVncPrzInc ;
      private bool[] T002V11_n1651ContratoSrvVnc_SrvVncPrzInc ;
      private int[] T002V11_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] T002V11_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] T002V11_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] T002V11_n923ContratoSrvVnc_ServicoVncCod ;
      private String[] T002V14_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] T002V14_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] T002V15_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] T002V15_n924ContratoSrvVnc_ServicoVncSigla ;
      private int[] T002V10_A933ContratoSrvVnc_ContratoCod ;
      private bool[] T002V10_n933ContratoSrvVnc_ContratoCod ;
      private int[] T002V10_A921ContratoSrvVnc_ServicoCod ;
      private bool[] T002V10_n921ContratoSrvVnc_ServicoCod ;
      private int[] T002V12_A1662ContratoSrvVnc_ContratoAreaCod ;
      private bool[] T002V12_n1662ContratoSrvVnc_ContratoAreaCod ;
      private String[] T002V13_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] T002V13_n922ContratoSrvVnc_ServicoSigla ;
      private int[] T002V16_A917ContratoSrvVnc_Codigo ;
      private int[] T002V16_A1801ContratoSrvVnc_NovoRspDmn ;
      private bool[] T002V16_n1801ContratoSrvVnc_NovoRspDmn ;
      private String[] T002V16_A1743ContratoSrvVnc_NovoStatusDmn ;
      private bool[] T002V16_n1743ContratoSrvVnc_NovoStatusDmn ;
      private String[] T002V16_A2108ContratoServicosVnc_Descricao ;
      private bool[] T002V16_n2108ContratoServicosVnc_Descricao ;
      private String[] T002V16_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] T002V16_n922ContratoSrvVnc_ServicoSigla ;
      private String[] T002V16_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] T002V16_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] T002V16_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] T002V16_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] T002V16_A1629ContratoSrvVnc_SrvVncTpVnc ;
      private bool[] T002V16_n1629ContratoSrvVnc_SrvVncTpVnc ;
      private String[] T002V16_A1630ContratoSrvVnc_SrvVncTpDias ;
      private bool[] T002V16_n1630ContratoSrvVnc_SrvVncTpDias ;
      private decimal[] T002V16_A1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private bool[] T002V16_n1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private short[] T002V16_A1651ContratoSrvVnc_SrvVncPrzInc ;
      private bool[] T002V16_n1651ContratoSrvVnc_SrvVncPrzInc ;
      private String[] T002V16_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] T002V16_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] T002V16_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] T002V16_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] T002V16_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] T002V16_n1084ContratoSrvVnc_StatusDmn ;
      private String[] T002V16_A1745ContratoSrvVnc_VincularCom ;
      private bool[] T002V16_n1745ContratoSrvVnc_VincularCom ;
      private short[] T002V16_A1821ContratoSrvVnc_SrvVncRef ;
      private bool[] T002V16_n1821ContratoSrvVnc_SrvVncRef ;
      private int[] T002V16_A1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] T002V16_n1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] T002V16_A1090ContratoSrvVnc_SemCusto ;
      private bool[] T002V16_n1090ContratoSrvVnc_SemCusto ;
      private bool[] T002V16_A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] T002V16_n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] T002V16_A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] T002V16_n1437ContratoServicosVnc_NaoClonaInfo ;
      private int[] T002V16_A1438ContratoServicosVnc_Gestor ;
      private bool[] T002V16_n1438ContratoServicosVnc_Gestor ;
      private bool[] T002V16_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] T002V16_n1818ContratoServicosVnc_ClonarLink ;
      private bool[] T002V16_A1453ContratoServicosVnc_Ativo ;
      private bool[] T002V16_n1453ContratoServicosVnc_Ativo ;
      private int[] T002V16_A915ContratoSrvVnc_CntSrvCod ;
      private int[] T002V16_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] T002V16_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] T002V16_A933ContratoSrvVnc_ContratoCod ;
      private bool[] T002V16_n933ContratoSrvVnc_ContratoCod ;
      private int[] T002V16_A1662ContratoSrvVnc_ContratoAreaCod ;
      private bool[] T002V16_n1662ContratoSrvVnc_ContratoAreaCod ;
      private int[] T002V16_A921ContratoSrvVnc_ServicoCod ;
      private bool[] T002V16_n921ContratoSrvVnc_ServicoCod ;
      private int[] T002V16_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] T002V16_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] T002V16_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] T002V16_n923ContratoSrvVnc_ServicoVncCod ;
      private String[] T002V6_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] T002V6_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private String[] T002V9_A1744ContratoSrvVnc_PrestadoraTpFab ;
      private bool[] T002V9_n1744ContratoSrvVnc_PrestadoraTpFab ;
      private bool[] T002V3_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private String[] T002V19_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] T002V19_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private String[] T002V22_A1744ContratoSrvVnc_PrestadoraTpFab ;
      private bool[] T002V22_n1744ContratoSrvVnc_PrestadoraTpFab ;
      private int[] T002V23_A933ContratoSrvVnc_ContratoCod ;
      private bool[] T002V23_n933ContratoSrvVnc_ContratoCod ;
      private int[] T002V23_A921ContratoSrvVnc_ServicoCod ;
      private bool[] T002V23_n921ContratoSrvVnc_ServicoCod ;
      private int[] T002V24_A1662ContratoSrvVnc_ContratoAreaCod ;
      private bool[] T002V24_n1662ContratoSrvVnc_ContratoAreaCod ;
      private String[] T002V25_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] T002V25_n922ContratoSrvVnc_ServicoSigla ;
      private String[] T002V26_A1629ContratoSrvVnc_SrvVncTpVnc ;
      private bool[] T002V26_n1629ContratoSrvVnc_SrvVncTpVnc ;
      private String[] T002V26_A1630ContratoSrvVnc_SrvVncTpDias ;
      private bool[] T002V26_n1630ContratoSrvVnc_SrvVncTpDias ;
      private decimal[] T002V26_A1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private bool[] T002V26_n1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private short[] T002V26_A1651ContratoSrvVnc_SrvVncPrzInc ;
      private bool[] T002V26_n1651ContratoSrvVnc_SrvVncPrzInc ;
      private int[] T002V26_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] T002V26_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] T002V26_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] T002V26_n923ContratoSrvVnc_ServicoVncCod ;
      private String[] T002V27_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] T002V27_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] T002V28_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] T002V28_n924ContratoSrvVnc_ServicoVncSigla ;
      private int[] T002V29_A917ContratoSrvVnc_Codigo ;
      private int[] T002V3_A917ContratoSrvVnc_Codigo ;
      private int[] T002V3_A1801ContratoSrvVnc_NovoRspDmn ;
      private bool[] T002V3_n1801ContratoSrvVnc_NovoRspDmn ;
      private String[] T002V3_A1743ContratoSrvVnc_NovoStatusDmn ;
      private bool[] T002V3_n1743ContratoSrvVnc_NovoStatusDmn ;
      private String[] T002V3_A2108ContratoServicosVnc_Descricao ;
      private bool[] T002V3_n2108ContratoServicosVnc_Descricao ;
      private String[] T002V3_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] T002V3_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] T002V3_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] T002V3_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] T002V3_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] T002V3_n1084ContratoSrvVnc_StatusDmn ;
      private String[] T002V3_A1745ContratoSrvVnc_VincularCom ;
      private bool[] T002V3_n1745ContratoSrvVnc_VincularCom ;
      private short[] T002V3_A1821ContratoSrvVnc_SrvVncRef ;
      private bool[] T002V3_n1821ContratoSrvVnc_SrvVncRef ;
      private int[] T002V3_A1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] T002V3_n1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] T002V3_A1090ContratoSrvVnc_SemCusto ;
      private bool[] T002V3_n1090ContratoSrvVnc_SemCusto ;
      private bool[] T002V3_A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] T002V3_n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] T002V3_A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] T002V3_n1437ContratoServicosVnc_NaoClonaInfo ;
      private int[] T002V3_A1438ContratoServicosVnc_Gestor ;
      private bool[] T002V3_n1438ContratoServicosVnc_Gestor ;
      private bool[] T002V3_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] T002V3_n1818ContratoServicosVnc_ClonarLink ;
      private bool[] T002V3_A1453ContratoServicosVnc_Ativo ;
      private bool[] T002V3_n1453ContratoServicosVnc_Ativo ;
      private int[] T002V3_A915ContratoSrvVnc_CntSrvCod ;
      private int[] T002V3_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] T002V30_A917ContratoSrvVnc_Codigo ;
      private int[] T002V31_A917ContratoSrvVnc_Codigo ;
      private int[] T002V2_A917ContratoSrvVnc_Codigo ;
      private int[] T002V2_A1801ContratoSrvVnc_NovoRspDmn ;
      private bool[] T002V2_n1801ContratoSrvVnc_NovoRspDmn ;
      private String[] T002V2_A1743ContratoSrvVnc_NovoStatusDmn ;
      private bool[] T002V2_n1743ContratoSrvVnc_NovoStatusDmn ;
      private String[] T002V2_A2108ContratoServicosVnc_Descricao ;
      private bool[] T002V2_n2108ContratoServicosVnc_Descricao ;
      private String[] T002V2_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] T002V2_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] T002V2_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] T002V2_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] T002V2_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] T002V2_n1084ContratoSrvVnc_StatusDmn ;
      private String[] T002V2_A1745ContratoSrvVnc_VincularCom ;
      private bool[] T002V2_n1745ContratoSrvVnc_VincularCom ;
      private short[] T002V2_A1821ContratoSrvVnc_SrvVncRef ;
      private bool[] T002V2_n1821ContratoSrvVnc_SrvVncRef ;
      private int[] T002V2_A1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] T002V2_n1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] T002V2_A1090ContratoSrvVnc_SemCusto ;
      private bool[] T002V2_n1090ContratoSrvVnc_SemCusto ;
      private bool[] T002V2_A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] T002V2_n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] T002V2_A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] T002V2_n1437ContratoServicosVnc_NaoClonaInfo ;
      private int[] T002V2_A1438ContratoServicosVnc_Gestor ;
      private bool[] T002V2_n1438ContratoServicosVnc_Gestor ;
      private bool[] T002V2_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] T002V2_n1818ContratoServicosVnc_ClonarLink ;
      private bool[] T002V2_A1453ContratoServicosVnc_Ativo ;
      private bool[] T002V2_n1453ContratoServicosVnc_Ativo ;
      private int[] T002V2_A915ContratoSrvVnc_CntSrvCod ;
      private int[] T002V2_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] T002V2_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] T002V32_A917ContratoSrvVnc_Codigo ;
      private String[] T002V37_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] T002V37_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private String[] T002V40_A1744ContratoSrvVnc_PrestadoraTpFab ;
      private bool[] T002V40_n1744ContratoSrvVnc_PrestadoraTpFab ;
      private int[] T002V41_A933ContratoSrvVnc_ContratoCod ;
      private bool[] T002V41_n933ContratoSrvVnc_ContratoCod ;
      private int[] T002V41_A921ContratoSrvVnc_ServicoCod ;
      private bool[] T002V41_n921ContratoSrvVnc_ServicoCod ;
      private int[] T002V42_A1662ContratoSrvVnc_ContratoAreaCod ;
      private bool[] T002V42_n1662ContratoSrvVnc_ContratoAreaCod ;
      private String[] T002V43_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] T002V43_n922ContratoSrvVnc_ServicoSigla ;
      private String[] T002V44_A1629ContratoSrvVnc_SrvVncTpVnc ;
      private bool[] T002V44_n1629ContratoSrvVnc_SrvVncTpVnc ;
      private String[] T002V44_A1630ContratoSrvVnc_SrvVncTpDias ;
      private bool[] T002V44_n1630ContratoSrvVnc_SrvVncTpDias ;
      private decimal[] T002V44_A1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private bool[] T002V44_n1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private short[] T002V44_A1651ContratoSrvVnc_SrvVncPrzInc ;
      private bool[] T002V44_n1651ContratoSrvVnc_SrvVncPrzInc ;
      private int[] T002V44_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] T002V44_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] T002V44_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] T002V44_n923ContratoSrvVnc_ServicoVncCod ;
      private String[] T002V45_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] T002V45_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] T002V46_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] T002V46_n924ContratoSrvVnc_ServicoVncSigla ;
      private int[] T002V47_A917ContratoSrvVnc_Codigo ;
      private int[] T002V48_A74Contrato_Codigo ;
      private String[] T002V48_A77Contrato_Numero ;
      private int[] T002V48_A39Contratada_Codigo ;
      private int[] T002V49_A155Servico_Codigo ;
      private int[] T002V49_A160ContratoServicos_Codigo ;
      private String[] T002V49_A605Servico_Sigla ;
      private String[] T002V52_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] T002V52_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private String[] T002V55_A1744ContratoSrvVnc_PrestadoraTpFab ;
      private bool[] T002V55_n1744ContratoSrvVnc_PrestadoraTpFab ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV24Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27TipoFabrica ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Descricoes ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratoservicosvnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002V16 ;
          prmT002V16 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT002V16 ;
          cmdBufferT002V16=" SELECT TM1.[ContratoSrvVnc_Codigo], TM1.[ContratoSrvVnc_NovoRspDmn], TM1.[ContratoSrvVnc_NovoStatusDmn], TM1.[ContratoServicosVnc_Descricao], T4.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, T6.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T7.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T5.[ContratoServicos_TipoVnc] AS ContratoSrvVnc_SrvVncTpVnc, T5.[ContratoServicos_PrazoTpDias] AS ContratoSrvVnc_SrvVncTpDias, T5.[Servico_VlrUnidadeContratada] AS ContratoSrvVnc_SrvVncVlrUndCnt, T5.[ContratoServicos_PrazoInicio] AS ContratoSrvVnc_SrvVncPrzInc, TM1.[ContratoSrvVnc_SrvVncStatus], TM1.[ContratoSrvVnc_DoStatusDmn], TM1.[ContratoSrvVnc_StatusDmn], TM1.[ContratoSrvVnc_VincularCom], TM1.[ContratoSrvVnc_SrvVncRef], TM1.[ContratoSrvVnc_PrestadoraCod], TM1.[ContratoSrvVnc_SemCusto], TM1.[ContratoServicosVnc_ClonaSrvOri], TM1.[ContratoServicosVnc_NaoClonaInfo], TM1.[ContratoServicosVnc_Gestor], TM1.[ContratoServicosVnc_ClonarLink], TM1.[ContratoServicosVnc_Ativo], TM1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, TM1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_ContratoCod, T3.[Contrato_AreaTrabalhoCod] AS ContratoSrvVnc_ContratoAreaCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T5.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T5.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod FROM (((((([ContratoServicosVnc] TM1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = TM1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = TM1.[ContratoSrvVnc_SrvVncCntSrvCod]) "
          + " LEFT JOIN [Contrato] T6 WITH (NOLOCK) ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) LEFT JOIN [Servico] T7 WITH (NOLOCK) ON T7.[Servico_Codigo] = T5.[Servico_Codigo]) WHERE TM1.[ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo ORDER BY TM1.[ContratoSrvVnc_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT002V6 ;
          prmT002V6 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V9 ;
          prmT002V9 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V10 ;
          prmT002V10 = new Object[] {
          new Object[] {"@ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V12 ;
          prmT002V12 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V13 ;
          prmT002V13 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V11 ;
          prmT002V11 = new Object[] {
          new Object[] {"@ContratoSrvVnc_SrvVncCntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V14 ;
          prmT002V14 = new Object[] {
          new Object[] {"@ContratoSrvVnc_SrvVncCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V15 ;
          prmT002V15 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ServicoVncCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V19 ;
          prmT002V19 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V22 ;
          prmT002V22 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V23 ;
          prmT002V23 = new Object[] {
          new Object[] {"@ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V24 ;
          prmT002V24 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V25 ;
          prmT002V25 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V26 ;
          prmT002V26 = new Object[] {
          new Object[] {"@ContratoSrvVnc_SrvVncCntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V27 ;
          prmT002V27 = new Object[] {
          new Object[] {"@ContratoSrvVnc_SrvVncCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V28 ;
          prmT002V28 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ServicoVncCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V29 ;
          prmT002V29 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V3 ;
          prmT002V3 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V30 ;
          prmT002V30 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V31 ;
          prmT002V31 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V2 ;
          prmT002V2 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V32 ;
          prmT002V32 = new Object[] {
          new Object[] {"@ContratoSrvVnc_NovoRspDmn",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_NovoStatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_DoStatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_VincularCom",SqlDbType.Char,20,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncRef",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_SemCusto",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_ClonaSrvOri",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_NaoClonaInfo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_Gestor",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosVnc_ClonarLink",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncCntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V33 ;
          prmT002V33 = new Object[] {
          new Object[] {"@ContratoSrvVnc_NovoRspDmn",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_NovoStatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_DoStatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoSrvVnc_VincularCom",SqlDbType.Char,20,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncRef",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_SemCusto",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_ClonaSrvOri",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_NaoClonaInfo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_Gestor",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosVnc_ClonarLink",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicosVnc_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_SrvVncCntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V34 ;
          prmT002V34 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V37 ;
          prmT002V37 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V40 ;
          prmT002V40 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V41 ;
          prmT002V41 = new Object[] {
          new Object[] {"@ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V42 ;
          prmT002V42 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V43 ;
          prmT002V43 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V44 ;
          prmT002V44 = new Object[] {
          new Object[] {"@ContratoSrvVnc_SrvVncCntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V45 ;
          prmT002V45 = new Object[] {
          new Object[] {"@ContratoSrvVnc_SrvVncCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V46 ;
          prmT002V46 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ServicoVncCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V47 ;
          prmT002V47 = new Object[] {
          } ;
          Object[] prmT002V48 ;
          prmT002V48 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V49 ;
          prmT002V49 = new Object[] {
          new Object[] {"@AV20SelContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V52 ;
          prmT002V52 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002V55 ;
          prmT002V55 = new Object[] {
          new Object[] {"@ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002V2", "SELECT [ContratoSrvVnc_Codigo], [ContratoSrvVnc_NovoRspDmn], [ContratoSrvVnc_NovoStatusDmn], [ContratoServicosVnc_Descricao], [ContratoSrvVnc_SrvVncStatus], [ContratoSrvVnc_DoStatusDmn], [ContratoSrvVnc_StatusDmn], [ContratoSrvVnc_VincularCom], [ContratoSrvVnc_SrvVncRef], [ContratoSrvVnc_PrestadoraCod], [ContratoSrvVnc_SemCusto], [ContratoServicosVnc_ClonaSrvOri], [ContratoServicosVnc_NaoClonaInfo], [ContratoServicosVnc_Gestor], [ContratoServicosVnc_ClonarLink], [ContratoServicosVnc_Ativo], [ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, [ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod FROM [ContratoServicosVnc] WITH (UPDLOCK) WHERE [ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V2,1,0,true,false )
             ,new CursorDef("T002V3", "SELECT [ContratoSrvVnc_Codigo], [ContratoSrvVnc_NovoRspDmn], [ContratoSrvVnc_NovoStatusDmn], [ContratoServicosVnc_Descricao], [ContratoSrvVnc_SrvVncStatus], [ContratoSrvVnc_DoStatusDmn], [ContratoSrvVnc_StatusDmn], [ContratoSrvVnc_VincularCom], [ContratoSrvVnc_SrvVncRef], [ContratoSrvVnc_PrestadoraCod], [ContratoSrvVnc_SemCusto], [ContratoServicosVnc_ClonaSrvOri], [ContratoServicosVnc_NaoClonaInfo], [ContratoServicosVnc_Gestor], [ContratoServicosVnc_ClonarLink], [ContratoServicosVnc_Ativo], [ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, [ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V3,1,0,true,false )
             ,new CursorDef("T002V6", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_Sigla], '') WHEN @ContratoSrvVnc_PrestadoraCod = 910000 THEN 'Criador da SS' WHEN @ContratoSrvVnc_PrestadoraCod = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V6,1,0,true,false )
             ,new CursorDef("T002V9", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraTpFab], '') AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_TipoFabrica], '') WHEN @ContratoSrvVnc_PrestadoraCod >= 910000 THEN 'I' END AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT [Contratada_TipoFabrica] AS Contratada_TipoFabrica, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V9,1,0,true,false )
             ,new CursorDef("T002V10", "SELECT [Contrato_Codigo] AS ContratoSrvVnc_ContratoCod, [Servico_Codigo] AS ContratoSrvVnc_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoSrvVnc_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V10,1,0,true,false )
             ,new CursorDef("T002V11", "SELECT [ContratoServicos_TipoVnc] AS ContratoSrvVnc_SrvVncTpVnc, [ContratoServicos_PrazoTpDias] AS ContratoSrvVnc_SrvVncTpDias, [Servico_VlrUnidadeContratada] AS ContratoSrvVnc_SrvVncVlrUndCnt, [ContratoServicos_PrazoInicio] AS ContratoSrvVnc_SrvVncPrzInc, [Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, [Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoSrvVnc_SrvVncCntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V11,1,0,true,false )
             ,new CursorDef("T002V12", "SELECT [Contrato_AreaTrabalhoCod] AS ContratoSrvVnc_ContratoAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSrvVnc_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V12,1,0,true,false )
             ,new CursorDef("T002V13", "SELECT [Servico_Sigla] AS ContratoSrvVnc_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoSrvVnc_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V13,1,0,true,false )
             ,new CursorDef("T002V14", "SELECT [Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSrvVnc_SrvVncCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V14,1,0,true,false )
             ,new CursorDef("T002V15", "SELECT [Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoSrvVnc_ServicoVncCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V15,1,0,true,false )
             ,new CursorDef("T002V16", cmdBufferT002V16,true, GxErrorMask.GX_NOMASK, false, this,prmT002V16,100,0,true,false )
             ,new CursorDef("T002V19", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_Sigla], '') WHEN @ContratoSrvVnc_PrestadoraCod = 910000 THEN 'Criador da SS' WHEN @ContratoSrvVnc_PrestadoraCod = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V19,1,0,true,false )
             ,new CursorDef("T002V22", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraTpFab], '') AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_TipoFabrica], '') WHEN @ContratoSrvVnc_PrestadoraCod >= 910000 THEN 'I' END AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT [Contratada_TipoFabrica] AS Contratada_TipoFabrica, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V22,1,0,true,false )
             ,new CursorDef("T002V23", "SELECT [Contrato_Codigo] AS ContratoSrvVnc_ContratoCod, [Servico_Codigo] AS ContratoSrvVnc_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoSrvVnc_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V23,1,0,true,false )
             ,new CursorDef("T002V24", "SELECT [Contrato_AreaTrabalhoCod] AS ContratoSrvVnc_ContratoAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSrvVnc_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V24,1,0,true,false )
             ,new CursorDef("T002V25", "SELECT [Servico_Sigla] AS ContratoSrvVnc_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoSrvVnc_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V25,1,0,true,false )
             ,new CursorDef("T002V26", "SELECT [ContratoServicos_TipoVnc] AS ContratoSrvVnc_SrvVncTpVnc, [ContratoServicos_PrazoTpDias] AS ContratoSrvVnc_SrvVncTpDias, [Servico_VlrUnidadeContratada] AS ContratoSrvVnc_SrvVncVlrUndCnt, [ContratoServicos_PrazoInicio] AS ContratoSrvVnc_SrvVncPrzInc, [Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, [Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoSrvVnc_SrvVncCntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V26,1,0,true,false )
             ,new CursorDef("T002V27", "SELECT [Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSrvVnc_SrvVncCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V27,1,0,true,false )
             ,new CursorDef("T002V28", "SELECT [Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoSrvVnc_ServicoVncCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V28,1,0,true,false )
             ,new CursorDef("T002V29", "SELECT [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002V29,1,0,true,false )
             ,new CursorDef("T002V30", "SELECT TOP 1 [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE ( [ContratoSrvVnc_Codigo] > @ContratoSrvVnc_Codigo) ORDER BY [ContratoSrvVnc_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002V30,1,0,true,true )
             ,new CursorDef("T002V31", "SELECT TOP 1 [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE ( [ContratoSrvVnc_Codigo] < @ContratoSrvVnc_Codigo) ORDER BY [ContratoSrvVnc_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002V31,1,0,true,true )
             ,new CursorDef("T002V32", "INSERT INTO [ContratoServicosVnc]([ContratoSrvVnc_NovoRspDmn], [ContratoSrvVnc_NovoStatusDmn], [ContratoServicosVnc_Descricao], [ContratoSrvVnc_SrvVncStatus], [ContratoSrvVnc_DoStatusDmn], [ContratoSrvVnc_StatusDmn], [ContratoSrvVnc_VincularCom], [ContratoSrvVnc_SrvVncRef], [ContratoSrvVnc_PrestadoraCod], [ContratoSrvVnc_SemCusto], [ContratoServicosVnc_ClonaSrvOri], [ContratoServicosVnc_NaoClonaInfo], [ContratoServicosVnc_Gestor], [ContratoServicosVnc_ClonarLink], [ContratoServicosVnc_Ativo], [ContratoSrvVnc_CntSrvCod], [ContratoSrvVnc_SrvVncCntSrvCod]) VALUES(@ContratoSrvVnc_NovoRspDmn, @ContratoSrvVnc_NovoStatusDmn, @ContratoServicosVnc_Descricao, @ContratoSrvVnc_SrvVncStatus, @ContratoSrvVnc_DoStatusDmn, @ContratoSrvVnc_StatusDmn, @ContratoSrvVnc_VincularCom, @ContratoSrvVnc_SrvVncRef, @ContratoSrvVnc_PrestadoraCod, @ContratoSrvVnc_SemCusto, @ContratoServicosVnc_ClonaSrvOri, @ContratoServicosVnc_NaoClonaInfo, @ContratoServicosVnc_Gestor, @ContratoServicosVnc_ClonarLink, @ContratoServicosVnc_Ativo, @ContratoSrvVnc_CntSrvCod, @ContratoSrvVnc_SrvVncCntSrvCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002V32)
             ,new CursorDef("T002V33", "UPDATE [ContratoServicosVnc] SET [ContratoSrvVnc_NovoRspDmn]=@ContratoSrvVnc_NovoRspDmn, [ContratoSrvVnc_NovoStatusDmn]=@ContratoSrvVnc_NovoStatusDmn, [ContratoServicosVnc_Descricao]=@ContratoServicosVnc_Descricao, [ContratoSrvVnc_SrvVncStatus]=@ContratoSrvVnc_SrvVncStatus, [ContratoSrvVnc_DoStatusDmn]=@ContratoSrvVnc_DoStatusDmn, [ContratoSrvVnc_StatusDmn]=@ContratoSrvVnc_StatusDmn, [ContratoSrvVnc_VincularCom]=@ContratoSrvVnc_VincularCom, [ContratoSrvVnc_SrvVncRef]=@ContratoSrvVnc_SrvVncRef, [ContratoSrvVnc_PrestadoraCod]=@ContratoSrvVnc_PrestadoraCod, [ContratoSrvVnc_SemCusto]=@ContratoSrvVnc_SemCusto, [ContratoServicosVnc_ClonaSrvOri]=@ContratoServicosVnc_ClonaSrvOri, [ContratoServicosVnc_NaoClonaInfo]=@ContratoServicosVnc_NaoClonaInfo, [ContratoServicosVnc_Gestor]=@ContratoServicosVnc_Gestor, [ContratoServicosVnc_ClonarLink]=@ContratoServicosVnc_ClonarLink, [ContratoServicosVnc_Ativo]=@ContratoServicosVnc_Ativo, [ContratoSrvVnc_CntSrvCod]=@ContratoSrvVnc_CntSrvCod, [ContratoSrvVnc_SrvVncCntSrvCod]=@ContratoSrvVnc_SrvVncCntSrvCod  WHERE [ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo", GxErrorMask.GX_NOMASK,prmT002V33)
             ,new CursorDef("T002V34", "DELETE FROM [ContratoServicosVnc]  WHERE [ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo", GxErrorMask.GX_NOMASK,prmT002V34)
             ,new CursorDef("T002V37", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_Sigla], '') WHEN @ContratoSrvVnc_PrestadoraCod = 910000 THEN 'Criador da SS' WHEN @ContratoSrvVnc_PrestadoraCod = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V37,1,0,true,false )
             ,new CursorDef("T002V40", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraTpFab], '') AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_TipoFabrica], '') WHEN @ContratoSrvVnc_PrestadoraCod >= 910000 THEN 'I' END AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT [Contratada_TipoFabrica] AS Contratada_TipoFabrica, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V40,1,0,true,false )
             ,new CursorDef("T002V41", "SELECT [Contrato_Codigo] AS ContratoSrvVnc_ContratoCod, [Servico_Codigo] AS ContratoSrvVnc_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoSrvVnc_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V41,1,0,true,false )
             ,new CursorDef("T002V42", "SELECT [Contrato_AreaTrabalhoCod] AS ContratoSrvVnc_ContratoAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSrvVnc_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V42,1,0,true,false )
             ,new CursorDef("T002V43", "SELECT [Servico_Sigla] AS ContratoSrvVnc_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoSrvVnc_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V43,1,0,true,false )
             ,new CursorDef("T002V44", "SELECT [ContratoServicos_TipoVnc] AS ContratoSrvVnc_SrvVncTpVnc, [ContratoServicos_PrazoTpDias] AS ContratoSrvVnc_SrvVncTpDias, [Servico_VlrUnidadeContratada] AS ContratoSrvVnc_SrvVncVlrUndCnt, [ContratoServicos_PrazoInicio] AS ContratoSrvVnc_SrvVncPrzInc, [Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, [Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoSrvVnc_SrvVncCntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V44,1,0,true,false )
             ,new CursorDef("T002V45", "SELECT [Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSrvVnc_SrvVncCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V45,1,0,true,false )
             ,new CursorDef("T002V46", "SELECT [Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoSrvVnc_ServicoVncCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V46,1,0,true,false )
             ,new CursorDef("T002V47", "SELECT [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) ORDER BY [ContratoSrvVnc_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002V47,100,0,true,false )
             ,new CursorDef("T002V48", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ORDER BY [Contrato_Numero] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V48,0,0,true,false )
             ,new CursorDef("T002V49", "SELECT T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE T1.[Contrato_Codigo] = @AV20SelContrato_Codigo ORDER BY T2.[Servico_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V49,0,0,true,false )
             ,new CursorDef("T002V52", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_Sigla], '') WHEN @ContratoSrvVnc_PrestadoraCod = 910000 THEN 'Criador da SS' WHEN @ContratoSrvVnc_PrestadoraCod = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom FROM (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V52,1,0,true,false )
             ,new CursorDef("T002V55", "SELECT COALESCE( T1.[ContratoSrvVnc_PrestadoraTpFab], '') AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT CASE  WHEN @ContratoSrvVnc_PrestadoraCod < 910000 THEN COALESCE( T2.[Contratada_TipoFabrica], '') WHEN @ContratoSrvVnc_PrestadoraCod >= 910000 THEN 'I' END AS ContratoSrvVnc_PrestadoraTpFab FROM (SELECT [Contratada_TipoFabrica] AS Contratada_TipoFabrica, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoSrvVnc_PrestadoraCod ) T2 ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT002V55,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((bool[]) buf[21])[0] = rslt.getBool(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((bool[]) buf[23])[0] = rslt.getBool(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((bool[]) buf[27])[0] = rslt.getBool(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((bool[]) buf[29])[0] = rslt.getBool(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((bool[]) buf[21])[0] = rslt.getBool(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((bool[]) buf[23])[0] = rslt.getBool(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((bool[]) buf[27])[0] = rslt.getBool(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((bool[]) buf[29])[0] = rslt.getBool(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 20) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((short[]) buf[29])[0] = rslt.getShort(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((bool[]) buf[33])[0] = rslt.getBool(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((bool[]) buf[35])[0] = rslt.getBool(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((bool[]) buf[37])[0] = rslt.getBool(20) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(20);
                ((int[]) buf[39])[0] = rslt.getInt(21) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(21);
                ((bool[]) buf[41])[0] = rslt.getBool(22) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(22);
                ((bool[]) buf[43])[0] = rslt.getBool(23) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(23);
                ((int[]) buf[45])[0] = rslt.getInt(24) ;
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((int[]) buf[48])[0] = rslt.getInt(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                ((int[]) buf[50])[0] = rslt.getInt(27) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(27);
                ((int[]) buf[52])[0] = rslt.getInt(28) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(28);
                ((int[]) buf[54])[0] = rslt.getInt(29) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(29);
                ((int[]) buf[56])[0] = rslt.getInt(30) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(30);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                return;
             case 31 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 32 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
             case 36 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 37 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(11, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 15 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(15, (bool)parms[29]);
                }
                stmt.SetParameter(16, (int)parms[30]);
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[32]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(11, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 15 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(15, (bool)parms[29]);
                }
                stmt.SetParameter(16, (int)parms[30]);
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[32]);
                }
                stmt.SetParameter(18, (int)parms[33]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
