/*
               File: ContagemResultadoEvidenciaGeneral
        Description: Contagem Resultado Evidencia General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:47.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoevidenciageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadoevidenciageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadoevidenciageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultadoEvidencia_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.A586ContagemResultadoEvidencia_Codigo = aP1_ContagemResultadoEvidencia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A586ContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A456ContagemResultado_Codigo,(int)A586ContagemResultadoEvidencia_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PABZ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ContagemResultadoEvidenciaGeneral";
               context.Gx_err = 0;
               WSBZ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Evidencia General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117214793");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoevidenciageneral.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode("" +A586ContagemResultadoEvidencia_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEVIDENCIA_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormBZ2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadoevidenciageneral.js", "?20203117214795");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoEvidenciaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Evidencia General" ;
      }

      protected void WBBZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadoevidenciageneral.aspx");
            }
            wb_table1_2_BZ2( true) ;
         }
         else
         {
            wb_table1_2_BZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultado_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoEvidencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoEvidencia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoEvidencia_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoEvidenciaGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTBZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Evidencia General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPBZ0( ) ;
            }
         }
      }

      protected void WSBZ2( )
      {
         STARTBZ2( ) ;
         EVTBZ2( ) ;
      }

      protected void EVTBZ2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11BZ2 */
                                    E11BZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12BZ2 */
                                    E12BZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13BZ2 */
                                    E13BZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14BZ2 */
                                    E14BZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormBZ2( ) ;
            }
         }
      }

      protected void PABZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ContagemResultadoEvidenciaGeneral";
         context.Gx_err = 0;
      }

      protected void RFBZ2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00BZ2 */
            pr_default.execute(0, new Object[] {A586ContagemResultadoEvidencia_Codigo, A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A589ContagemResultadoEvidencia_NomeArq = H00BZ2_A589ContagemResultadoEvidencia_NomeArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
               n589ContagemResultadoEvidencia_NomeArq = H00BZ2_n589ContagemResultadoEvidencia_NomeArq[0];
               edtContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
               A590ContagemResultadoEvidencia_TipoArq = H00BZ2_A590ContagemResultadoEvidencia_TipoArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
               n590ContagemResultadoEvidencia_TipoArq = H00BZ2_n590ContagemResultadoEvidencia_TipoArq[0];
               edtContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
               A591ContagemResultadoEvidencia_Data = H00BZ2_A591ContagemResultadoEvidencia_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A591ContagemResultadoEvidencia_Data", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEVIDENCIA_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99")));
               n591ContagemResultadoEvidencia_Data = H00BZ2_n591ContagemResultadoEvidencia_Data[0];
               A588ContagemResultadoEvidencia_Arquivo = H00BZ2_A588ContagemResultadoEvidencia_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
               n588ContagemResultadoEvidencia_Arquivo = H00BZ2_n588ContagemResultadoEvidencia_Arquivo[0];
               /* Execute user event: E12BZ2 */
               E12BZ2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBBZ0( ) ;
         }
      }

      protected void STRUPBZ0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ContagemResultadoEvidenciaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11BZ2 */
         E11BZ2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A588ContagemResultadoEvidencia_Arquivo = cgiGet( edtContagemResultadoEvidencia_Arquivo_Internalname);
            n588ContagemResultadoEvidencia_Arquivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
            A591ContagemResultadoEvidencia_Data = context.localUtil.CToT( cgiGet( edtContagemResultadoEvidencia_Data_Internalname), 0);
            n591ContagemResultadoEvidencia_Data = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A591ContagemResultadoEvidencia_Data", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEVIDENCIA_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99")));
            /* Read saved values. */
            wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
            wcpOA586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA586ContagemResultadoEvidencia_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11BZ2 */
         E11BZ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11BZ2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12BZ2( )
      {
         /* Load Routine */
         edtContagemResultado_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Visible), 5, 0)));
         edtContagemResultadoEvidencia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoEvidencia_Codigo_Visible), 5, 0)));
      }

      protected void E13BZ2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagemresultadoevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A586ContagemResultadoEvidencia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14BZ2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagemresultadoevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A586ContagemResultadoEvidencia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoEvidencia";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultado_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultadoEvidencia_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8ContagemResultadoEvidencia_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_BZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_BZ2( true) ;
         }
         else
         {
            wb_table2_8_BZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_BZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_30_BZ2( true) ;
         }
         else
         {
            wb_table3_30_BZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_30_BZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BZ2e( true) ;
         }
         else
         {
            wb_table1_2_BZ2e( false) ;
         }
      }

      protected void wb_table3_30_BZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_30_BZ2e( true) ;
         }
         else
         {
            wb_table3_30_BZ2e( false) ;
         }
      }

      protected void wb_table2_8_BZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoevidencia_arquivo_Internalname, "Arquivo", "", "", lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCellView'>") ;
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            edtContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            edtContagemResultadoEvidencia_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
            edtContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) )
            {
               gxblobfileaux.Source = A588ContagemResultadoEvidencia_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtContagemResultadoEvidencia_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtContagemResultadoEvidencia_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A588ContagemResultadoEvidencia_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n588ContagemResultadoEvidencia_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
                  edtContagemResultadoEvidencia_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtContagemResultadoEvidencia_Arquivo_Internalname, StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo), context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Filetype)) ? A588ContagemResultadoEvidencia_Arquivo : edtContagemResultadoEvidencia_Arquivo_Filetype)) : edtContagemResultadoEvidencia_Arquivo_Contenttype), true, "", edtContagemResultadoEvidencia_Arquivo_Parameters, 0, 0, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtContagemResultadoEvidencia_Arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+"", "", "", "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoevidencia_nomearq_Internalname, "Nome do Arquivo", "", "", lblTextblockcontagemresultadoevidencia_nomearq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoEvidencia_NomeArq_Internalname, StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq), StringUtil.RTrim( context.localUtil.Format( A589ContagemResultadoEvidencia_NomeArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoEvidencia_NomeArq_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoevidencia_tipoarq_Internalname, "Tipo", "", "", lblTextblockcontagemresultadoevidencia_tipoarq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoEvidencia_TipoArq_Internalname, StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq), StringUtil.RTrim( context.localUtil.Format( A590ContagemResultadoEvidencia_TipoArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoEvidencia_TipoArq_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoevidencia_data_Internalname, "Upload", "", "", lblTextblockcontagemresultadoevidencia_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoEvidencia_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoEvidencia_Data_Internalname, context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoEvidencia_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoEvidencia_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoEvidenciaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_BZ2e( true) ;
         }
         else
         {
            wb_table2_8_BZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         A586ContagemResultadoEvidencia_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABZ2( ) ;
         WSBZ2( ) ;
         WEBZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA456ContagemResultado_Codigo = (String)((String)getParm(obj,0));
         sCtrlA586ContagemResultadoEvidencia_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PABZ2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadoevidenciageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PABZ2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A586ContagemResultadoEvidencia_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
         }
         wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
         wcpOA586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA586ContagemResultadoEvidencia_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A456ContagemResultado_Codigo != wcpOA456ContagemResultado_Codigo ) || ( A586ContagemResultadoEvidencia_Codigo != wcpOA586ContagemResultadoEvidencia_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         wcpOA586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA456ContagemResultado_Codigo = cgiGet( sPrefix+"A456ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA456ContagemResultado_Codigo) > 0 )
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA456ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A456ContagemResultado_Codigo_PARM"), ",", "."));
         }
         sCtrlA586ContagemResultadoEvidencia_Codigo = cgiGet( sPrefix+"A586ContagemResultadoEvidencia_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA586ContagemResultadoEvidencia_Codigo) > 0 )
         {
            A586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA586ContagemResultadoEvidencia_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
         }
         else
         {
            A586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A586ContagemResultadoEvidencia_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PABZ2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSBZ2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSBZ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A586ContagemResultadoEvidencia_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA586ContagemResultadoEvidencia_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A586ContagemResultadoEvidencia_Codigo_CTRL", StringUtil.RTrim( sCtrlA586ContagemResultadoEvidencia_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEBZ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117214835");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadoevidenciageneral.js", "?20203117214835");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultadoevidencia_arquivo_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEVIDENCIA_ARQUIVO";
         edtContagemResultadoEvidencia_Arquivo_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ARQUIVO";
         lblTextblockcontagemresultadoevidencia_nomearq_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         edtContagemResultadoEvidencia_NomeArq_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         lblTextblockcontagemresultadoevidencia_tipoarq_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEVIDENCIA_TIPOARQ";
         edtContagemResultadoEvidencia_TipoArq_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_TIPOARQ";
         lblTextblockcontagemresultadoevidencia_data_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEVIDENCIA_DATA";
         edtContagemResultadoEvidencia_Data_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DATA";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO";
         edtContagemResultadoEvidencia_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultadoEvidencia_Data_Jsonclick = "";
         edtContagemResultadoEvidencia_TipoArq_Jsonclick = "";
         edtContagemResultadoEvidencia_NomeArq_Jsonclick = "";
         edtContagemResultadoEvidencia_Arquivo_Jsonclick = "";
         edtContagemResultadoEvidencia_Arquivo_Parameters = "";
         edtContagemResultadoEvidencia_Arquivo_Contenttype = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContagemResultadoEvidencia_Arquivo_Filetype = "";
         edtContagemResultadoEvidencia_Codigo_Jsonclick = "";
         edtContagemResultadoEvidencia_Codigo_Visible = 1;
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13BZ2',iparms:[{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14BZ2',iparms:[{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00BZ2_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00BZ2_A456ContagemResultado_Codigo = new int[1] ;
         H00BZ2_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00BZ2_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00BZ2_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00BZ2_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00BZ2_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00BZ2_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         H00BZ2_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         H00BZ2_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         A589ContagemResultadoEvidencia_NomeArq = "";
         edtContagemResultadoEvidencia_Arquivo_Filename = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblockcontagemresultadoevidencia_nomearq_Jsonclick = "";
         lblTextblockcontagemresultadoevidencia_tipoarq_Jsonclick = "";
         lblTextblockcontagemresultadoevidencia_data_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA456ContagemResultado_Codigo = "";
         sCtrlA586ContagemResultadoEvidencia_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoevidenciageneral__default(),
            new Object[][] {
                new Object[] {
               H00BZ2_A586ContagemResultadoEvidencia_Codigo, H00BZ2_A456ContagemResultado_Codigo, H00BZ2_A589ContagemResultadoEvidencia_NomeArq, H00BZ2_n589ContagemResultadoEvidencia_NomeArq, H00BZ2_A590ContagemResultadoEvidencia_TipoArq, H00BZ2_n590ContagemResultadoEvidencia_TipoArq, H00BZ2_A591ContagemResultadoEvidencia_Data, H00BZ2_n591ContagemResultadoEvidencia_Data, H00BZ2_A588ContagemResultadoEvidencia_Arquivo, H00BZ2_n588ContagemResultadoEvidencia_Arquivo
               }
            }
         );
         AV15Pgmname = "ContagemResultadoEvidenciaGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ContagemResultadoEvidenciaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A456ContagemResultado_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int wcpOA586ContagemResultadoEvidencia_Codigo ;
      private int edtContagemResultado_Codigo_Visible ;
      private int edtContagemResultadoEvidencia_Codigo_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContagemResultado_Codigo ;
      private int AV8ContagemResultadoEvidencia_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultadoEvidencia_Codigo_Internalname ;
      private String edtContagemResultadoEvidencia_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String edtContagemResultadoEvidencia_Arquivo_Filename ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String edtContagemResultadoEvidencia_Arquivo_Filetype ;
      private String edtContagemResultadoEvidencia_Arquivo_Internalname ;
      private String edtContagemResultadoEvidencia_Data_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_arquivo_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick ;
      private String edtContagemResultadoEvidencia_Arquivo_Contenttype ;
      private String edtContagemResultadoEvidencia_Arquivo_Parameters ;
      private String edtContagemResultadoEvidencia_Arquivo_Jsonclick ;
      private String lblTextblockcontagemresultadoevidencia_nomearq_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_nomearq_Jsonclick ;
      private String edtContagemResultadoEvidencia_NomeArq_Internalname ;
      private String edtContagemResultadoEvidencia_NomeArq_Jsonclick ;
      private String lblTextblockcontagemresultadoevidencia_tipoarq_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_tipoarq_Jsonclick ;
      private String edtContagemResultadoEvidencia_TipoArq_Internalname ;
      private String edtContagemResultadoEvidencia_TipoArq_Jsonclick ;
      private String lblTextblockcontagemresultadoevidencia_data_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_data_Jsonclick ;
      private String edtContagemResultadoEvidencia_Data_Jsonclick ;
      private String sCtrlA456ContagemResultado_Codigo ;
      private String sCtrlA586ContagemResultadoEvidencia_Codigo ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool returnInSub ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private GxFile gxblobfileaux ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00BZ2_A586ContagemResultadoEvidencia_Codigo ;
      private int[] H00BZ2_A456ContagemResultado_Codigo ;
      private String[] H00BZ2_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00BZ2_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] H00BZ2_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00BZ2_n590ContagemResultadoEvidencia_TipoArq ;
      private DateTime[] H00BZ2_A591ContagemResultadoEvidencia_Data ;
      private bool[] H00BZ2_n591ContagemResultadoEvidencia_Data ;
      private String[] H00BZ2_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] H00BZ2_n588ContagemResultadoEvidencia_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class contagemresultadoevidenciageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BZ2 ;
          prmH00BZ2 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BZ2", "SELECT [ContagemResultadoEvidencia_Codigo], [ContagemResultado_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE ([ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo) AND ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) ORDER BY [ContagemResultadoEvidencia_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BZ2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getBLOBFile(6, rslt.getString(4, 10), rslt.getString(3, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
