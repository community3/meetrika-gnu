/*
               File: GetProjeto_ProjetoDesenvolvimentoWCFilterData
        Description: Get Projeto_Projeto Desenvolvimento WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:19.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getprojeto_projetodesenvolvimentowcfilterdata : GXProcedure
   {
      public getprojeto_projetodesenvolvimentowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getprojeto_projetodesenvolvimentowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getprojeto_projetodesenvolvimentowcfilterdata objgetprojeto_projetodesenvolvimentowcfilterdata;
         objgetprojeto_projetodesenvolvimentowcfilterdata = new getprojeto_projetodesenvolvimentowcfilterdata();
         objgetprojeto_projetodesenvolvimentowcfilterdata.AV14DDOName = aP0_DDOName;
         objgetprojeto_projetodesenvolvimentowcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetprojeto_projetodesenvolvimentowcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetprojeto_projetodesenvolvimentowcfilterdata.AV18OptionsJson = "" ;
         objgetprojeto_projetodesenvolvimentowcfilterdata.AV21OptionsDescJson = "" ;
         objgetprojeto_projetodesenvolvimentowcfilterdata.AV23OptionIndexesJson = "" ;
         objgetprojeto_projetodesenvolvimentowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetprojeto_projetodesenvolvimentowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetprojeto_projetodesenvolvimentowcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getprojeto_projetodesenvolvimentowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADPROJETODESENVOLVIMENTO_SISTEMANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("Projeto_ProjetoDesenvolvimentoWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Projeto_ProjetoDesenvolvimentoWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("Projeto_ProjetoDesenvolvimentoWCGridState"), "");
         }
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV35GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFPROJETODESENVOLVIMENTO_SISTEMANOM") == 0 )
            {
               AV10TFProjetoDesenvolvimento_SistemaNom = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL") == 0 )
            {
               AV11TFProjetoDesenvolvimento_SistemaNom_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&PROJETODESENVOLVIMENTO_PROJETOCOD") == 0 )
            {
               AV32ProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV35GXV1 = (int)(AV35GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV30DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 )
            {
               AV31ProjetoDesenvolvimento_SistemaNom1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPROJETODESENVOLVIMENTO_SISTEMANOMOPTIONS' Routine */
         AV10TFProjetoDesenvolvimento_SistemaNom = AV12SearchTxt;
         AV11TFProjetoDesenvolvimento_SistemaNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV30DynamicFiltersSelector1 ,
                                              AV31ProjetoDesenvolvimento_SistemaNom1 ,
                                              AV11TFProjetoDesenvolvimento_SistemaNom_Sel ,
                                              AV10TFProjetoDesenvolvimento_SistemaNom ,
                                              A671ProjetoDesenvolvimento_SistemaNom ,
                                              AV32ProjetoDesenvolvimento_ProjetoCod ,
                                              A669ProjetoDesenvolvimento_ProjetoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV31ProjetoDesenvolvimento_SistemaNom1 = StringUtil.Concat( StringUtil.RTrim( AV31ProjetoDesenvolvimento_SistemaNom1), "%", "");
         lV10TFProjetoDesenvolvimento_SistemaNom = StringUtil.Concat( StringUtil.RTrim( AV10TFProjetoDesenvolvimento_SistemaNom), "%", "");
         /* Using cursor P00O62 */
         pr_default.execute(0, new Object[] {AV32ProjetoDesenvolvimento_ProjetoCod, lV31ProjetoDesenvolvimento_SistemaNom1, lV10TFProjetoDesenvolvimento_SistemaNom, AV11TFProjetoDesenvolvimento_SistemaNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKO62 = false;
            A670ProjetoDesenvolvimento_SistemaCod = P00O62_A670ProjetoDesenvolvimento_SistemaCod[0];
            A669ProjetoDesenvolvimento_ProjetoCod = P00O62_A669ProjetoDesenvolvimento_ProjetoCod[0];
            A671ProjetoDesenvolvimento_SistemaNom = P00O62_A671ProjetoDesenvolvimento_SistemaNom[0];
            n671ProjetoDesenvolvimento_SistemaNom = P00O62_n671ProjetoDesenvolvimento_SistemaNom[0];
            A671ProjetoDesenvolvimento_SistemaNom = P00O62_A671ProjetoDesenvolvimento_SistemaNom[0];
            n671ProjetoDesenvolvimento_SistemaNom = P00O62_n671ProjetoDesenvolvimento_SistemaNom[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00O62_A669ProjetoDesenvolvimento_ProjetoCod[0] == A669ProjetoDesenvolvimento_ProjetoCod ) && ( P00O62_A670ProjetoDesenvolvimento_SistemaCod[0] == A670ProjetoDesenvolvimento_SistemaCod ) )
            {
               BRKO62 = false;
               AV24count = (long)(AV24count+1);
               BRKO62 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A671ProjetoDesenvolvimento_SistemaNom)) )
            {
               AV16Option = A671ProjetoDesenvolvimento_SistemaNom;
               AV15InsertIndex = 1;
               while ( ( AV15InsertIndex <= AV17Options.Count ) && ( StringUtil.StrCmp(((String)AV17Options.Item(AV15InsertIndex)), AV16Option) < 0 ) )
               {
                  AV15InsertIndex = (int)(AV15InsertIndex+1);
               }
               AV17Options.Add(AV16Option, AV15InsertIndex);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), AV15InsertIndex);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKO62 )
            {
               BRKO62 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFProjetoDesenvolvimento_SistemaNom = "";
         AV11TFProjetoDesenvolvimento_SistemaNom_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV30DynamicFiltersSelector1 = "";
         AV31ProjetoDesenvolvimento_SistemaNom1 = "";
         scmdbuf = "";
         lV31ProjetoDesenvolvimento_SistemaNom1 = "";
         lV10TFProjetoDesenvolvimento_SistemaNom = "";
         A671ProjetoDesenvolvimento_SistemaNom = "";
         P00O62_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         P00O62_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         P00O62_A671ProjetoDesenvolvimento_SistemaNom = new String[] {""} ;
         P00O62_n671ProjetoDesenvolvimento_SistemaNom = new bool[] {false} ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getprojeto_projetodesenvolvimentowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00O62_A670ProjetoDesenvolvimento_SistemaCod, P00O62_A669ProjetoDesenvolvimento_ProjetoCod, P00O62_A671ProjetoDesenvolvimento_SistemaNom, P00O62_n671ProjetoDesenvolvimento_SistemaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV35GXV1 ;
      private int AV32ProjetoDesenvolvimento_ProjetoCod ;
      private int A669ProjetoDesenvolvimento_ProjetoCod ;
      private int A670ProjetoDesenvolvimento_SistemaCod ;
      private int AV15InsertIndex ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKO62 ;
      private bool n671ProjetoDesenvolvimento_SistemaNom ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFProjetoDesenvolvimento_SistemaNom ;
      private String AV11TFProjetoDesenvolvimento_SistemaNom_Sel ;
      private String AV30DynamicFiltersSelector1 ;
      private String AV31ProjetoDesenvolvimento_SistemaNom1 ;
      private String lV31ProjetoDesenvolvimento_SistemaNom1 ;
      private String lV10TFProjetoDesenvolvimento_SistemaNom ;
      private String A671ProjetoDesenvolvimento_SistemaNom ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00O62_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] P00O62_A669ProjetoDesenvolvimento_ProjetoCod ;
      private String[] P00O62_A671ProjetoDesenvolvimento_SistemaNom ;
      private bool[] P00O62_n671ProjetoDesenvolvimento_SistemaNom ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getprojeto_projetodesenvolvimentowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00O62( IGxContext context ,
                                             String AV30DynamicFiltersSelector1 ,
                                             String AV31ProjetoDesenvolvimento_SistemaNom1 ,
                                             String AV11TFProjetoDesenvolvimento_SistemaNom_Sel ,
                                             String AV10TFProjetoDesenvolvimento_SistemaNom ,
                                             String A671ProjetoDesenvolvimento_SistemaNom ,
                                             int AV32ProjetoDesenvolvimento_ProjetoCod ,
                                             int A669ProjetoDesenvolvimento_ProjetoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [4] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod, T1.[ProjetoDesenvolvimento_ProjetoCod], T2.[Sistema_Nome] AS ProjetoDesenvolvimento_SistemaNom FROM ([ProjetoDesenvolvimento] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ProjetoDesenvolvimento_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ProjetoDesenvolvimento_ProjetoCod] = @AV32ProjetoDesenvolvimento_ProjetoCod)";
         if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ProjetoDesenvolvimento_SistemaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV31ProjetoDesenvolvimento_SistemaNom1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFProjetoDesenvolvimento_SistemaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFProjetoDesenvolvimento_SistemaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV10TFProjetoDesenvolvimento_SistemaNom)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFProjetoDesenvolvimento_SistemaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV11TFProjetoDesenvolvimento_SistemaNom_Sel)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ProjetoDesenvolvimento_ProjetoCod], T1.[ProjetoDesenvolvimento_SistemaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00O62(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00O62 ;
          prmP00O62 = new Object[] {
          new Object[] {"@AV32ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV31ProjetoDesenvolvimento_SistemaNom1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV10TFProjetoDesenvolvimento_SistemaNom",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFProjetoDesenvolvimento_SistemaNom_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00O62", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00O62,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getprojeto_projetodesenvolvimentowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getprojeto_projetodesenvolvimentowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getprojeto_projetodesenvolvimentowcfilterdata") )
          {
             return  ;
          }
          getprojeto_projetodesenvolvimentowcfilterdata worker = new getprojeto_projetodesenvolvimentowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
