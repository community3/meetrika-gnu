/*
               File: type_SdtGAMUserAttributeMultiValues
        Description: GAMUserAttributeMultiValues
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:14.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMUserAttributeMultiValues : GxUserType, IGxExternalObject
   {
      public SdtGAMUserAttributeMultiValues( )
      {
         initialize();
      }

      public SdtGAMUserAttributeMultiValues( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMUserAttributeMultiValues_externalReference == null )
         {
            GAMUserAttributeMultiValues_externalReference = new Artech.Security.GAMUserAttributeMultiValues(context);
         }
         returntostring = "";
         returntostring = (String)(GAMUserAttributeMultiValues_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Id
      {
         get {
            if ( GAMUserAttributeMultiValues_externalReference == null )
            {
               GAMUserAttributeMultiValues_externalReference = new Artech.Security.GAMUserAttributeMultiValues(context);
            }
            return GAMUserAttributeMultiValues_externalReference.Id ;
         }

         set {
            if ( GAMUserAttributeMultiValues_externalReference == null )
            {
               GAMUserAttributeMultiValues_externalReference = new Artech.Security.GAMUserAttributeMultiValues(context);
            }
            GAMUserAttributeMultiValues_externalReference.Id = value;
         }

      }

      public String gxTpr_Value
      {
         get {
            if ( GAMUserAttributeMultiValues_externalReference == null )
            {
               GAMUserAttributeMultiValues_externalReference = new Artech.Security.GAMUserAttributeMultiValues(context);
            }
            return GAMUserAttributeMultiValues_externalReference.Value ;
         }

         set {
            if ( GAMUserAttributeMultiValues_externalReference == null )
            {
               GAMUserAttributeMultiValues_externalReference = new Artech.Security.GAMUserAttributeMultiValues(context);
            }
            GAMUserAttributeMultiValues_externalReference.Value = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMUserAttributeMultiValues_externalReference == null )
            {
               GAMUserAttributeMultiValues_externalReference = new Artech.Security.GAMUserAttributeMultiValues(context);
            }
            return GAMUserAttributeMultiValues_externalReference ;
         }

         set {
            GAMUserAttributeMultiValues_externalReference = (Artech.Security.GAMUserAttributeMultiValues)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMUserAttributeMultiValues GAMUserAttributeMultiValues_externalReference=null ;
   }

}
