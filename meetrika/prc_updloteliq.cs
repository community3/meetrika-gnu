/*
               File: PRC_UpdLoteLiq
        Description: Upd Lote Liquidacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:18.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updloteliq : GXProcedure
   {
      public prc_updloteliq( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updloteliq( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Lote_Codigo ,
                           ref String aP1_Lote_LiqBanco ,
                           DateTime aP2_Lote_LiqData ,
                           decimal aP3_Lote_LiqValor )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         this.AV8Lote_LiqBanco = aP1_Lote_LiqBanco;
         this.AV9Lote_LiqData = aP2_Lote_LiqData;
         this.AV10Lote_LiqValor = aP3_Lote_LiqValor;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.A596Lote_Codigo;
         aP1_Lote_LiqBanco=this.AV8Lote_LiqBanco;
      }

      public void executeSubmit( ref int aP0_Lote_Codigo ,
                                 ref String aP1_Lote_LiqBanco ,
                                 DateTime aP2_Lote_LiqData ,
                                 decimal aP3_Lote_LiqValor )
      {
         prc_updloteliq objprc_updloteliq;
         objprc_updloteliq = new prc_updloteliq();
         objprc_updloteliq.A596Lote_Codigo = aP0_Lote_Codigo;
         objprc_updloteliq.AV8Lote_LiqBanco = aP1_Lote_LiqBanco;
         objprc_updloteliq.AV9Lote_LiqData = aP2_Lote_LiqData;
         objprc_updloteliq.AV10Lote_LiqValor = aP3_Lote_LiqValor;
         objprc_updloteliq.context.SetSubmitInitialConfig(context);
         objprc_updloteliq.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updloteliq);
         aP0_Lote_Codigo=this.A596Lote_Codigo;
         aP1_Lote_LiqBanco=this.AV8Lote_LiqBanco;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updloteliq)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00502 */
         pr_default.execute(0, new Object[] {n675Lote_LiqBanco, AV8Lote_LiqBanco, n676Lote_LiqData, AV9Lote_LiqData, n677Lote_LiqValor, AV10Lote_LiqValor, A596Lote_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
         dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         A675Lote_LiqBanco = "";
         A676Lote_LiqData = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updloteliq__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A596Lote_Codigo ;
      private decimal AV10Lote_LiqValor ;
      private decimal A677Lote_LiqValor ;
      private String AV8Lote_LiqBanco ;
      private String A675Lote_LiqBanco ;
      private DateTime AV9Lote_LiqData ;
      private DateTime A676Lote_LiqData ;
      private bool n675Lote_LiqBanco ;
      private bool n676Lote_LiqData ;
      private bool n677Lote_LiqValor ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Lote_Codigo ;
      private String aP1_Lote_LiqBanco ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_updloteliq__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00502 ;
          prmP00502 = new Object[] {
          new Object[] {"@Lote_LiqBanco",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_LiqData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_LiqValor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00502", "UPDATE [Lote] SET [Lote_LiqBanco]=@Lote_LiqBanco, [Lote_LiqData]=@Lote_LiqData, [Lote_LiqValor]=@Lote_LiqValor  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00502)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
       }
    }

 }

}
