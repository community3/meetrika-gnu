/*
               File: type_SdtSDT_WP_OS
        Description: SDT_WP_OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 9:44:20.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_WP_OS" )]
   [XmlType(TypeName =  "SDT_WP_OS" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_WP_OS : GxUserType
   {
      public SdtSDT_WP_OS( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj = "";
         gxTv_SdtSDT_WP_OS_Calculodivergencia = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_demanda = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_descricao = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_link = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom = "";
         gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor = "";
         gxTv_SdtSDT_WP_OS_Modulo_nome = "";
         gxTv_SdtSDT_WP_OS_Modulo_sigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_observacao = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref = "";
         gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prztp = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_datincta = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_referencia = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante = "";
         gxTv_SdtSDT_WP_OS_Usuario_cargouonom = "";
         gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome = "";
         gxTv_SdtSDT_WP_OS_Contratoservicos_localexec = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise = DateTime.MinValue;
      }

      public SdtSDT_WP_OS( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WP_OS deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WP_OS)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WP_OS obj ;
         obj = this;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Contagemresultado_datadmn = deserialized.gxTpr_Contagemresultado_datadmn;
         obj.gxTpr_Contagemresultado_datainicio = deserialized.gxTpr_Contagemresultado_datainicio;
         obj.gxTpr_Contagemresultado_dataentrega = deserialized.gxTpr_Contagemresultado_dataentrega;
         obj.gxTpr_Contagemresultado_horaentrega = deserialized.gxTpr_Contagemresultado_horaentrega;
         obj.gxTpr_Contagemresultado_dataprevista = deserialized.gxTpr_Contagemresultado_dataprevista;
         obj.gxTpr_Contagemresultado_dataexecucao = deserialized.gxTpr_Contagemresultado_dataexecucao;
         obj.gxTpr_Contagemresultado_dataentregareal = deserialized.gxTpr_Contagemresultado_dataentregareal;
         obj.gxTpr_Contagemresultado_datahomologacao = deserialized.gxTpr_Contagemresultado_datahomologacao;
         obj.gxTpr_Contagemresultado_prazoinicialdias = deserialized.gxTpr_Contagemresultado_prazoinicialdias;
         obj.gxTpr_Contagemresultado_prazomaisdias = deserialized.gxTpr_Contagemresultado_prazomaisdias;
         obj.gxTpr_Contagemresultado_contratadacod = deserialized.gxTpr_Contagemresultado_contratadacod;
         obj.gxTpr_Contagemresultado_contratadapessoacod = deserialized.gxTpr_Contagemresultado_contratadapessoacod;
         obj.gxTpr_Contagemresultado_contratadapessoanom = deserialized.gxTpr_Contagemresultado_contratadapessoanom;
         obj.gxTpr_Contagemresultado_contratadasigla = deserialized.gxTpr_Contagemresultado_contratadasigla;
         obj.gxTpr_Contagemresultado_contratadatipofab = deserialized.gxTpr_Contagemresultado_contratadatipofab;
         obj.gxTpr_Contagemresultado_contratadalogo = deserialized.gxTpr_Contagemresultado_contratadalogo;
         obj.gxTpr_Contagemresultado_contratadalogo_gxi = deserialized.gxTpr_Contagemresultado_contratadalogo_gxi;
         obj.gxTpr_Contagemresultado_contratadacnpj = deserialized.gxTpr_Contagemresultado_contratadacnpj;
         obj.gxTpr_Contratada_areatrabalhocod = deserialized.gxTpr_Contratada_areatrabalhocod;
         obj.gxTpr_Indicedivergencia = deserialized.gxTpr_Indicedivergencia;
         obj.gxTpr_Calculodivergencia = deserialized.gxTpr_Calculodivergencia;
         obj.gxTpr_Contagemresultado_contratadaorigemcod = deserialized.gxTpr_Contagemresultado_contratadaorigemcod;
         obj.gxTpr_Contagemresultado_contratadaorigemsigla = deserialized.gxTpr_Contagemresultado_contratadaorigemsigla;
         obj.gxTpr_Contagemresultado_contratadaorigempescod = deserialized.gxTpr_Contagemresultado_contratadaorigempescod;
         obj.gxTpr_Contagemresultado_contratadaorigempesnom = deserialized.gxTpr_Contagemresultado_contratadaorigempesnom;
         obj.gxTpr_Contagemresultado_contratadaorigemareacod = deserialized.gxTpr_Contagemresultado_contratadaorigemareacod;
         obj.gxTpr_Contagemresultado_contratadaorigemusasistema = deserialized.gxTpr_Contagemresultado_contratadaorigemusasistema;
         obj.gxTpr_Contagemresultado_contadorfscod = deserialized.gxTpr_Contagemresultado_contadorfscod;
         obj.gxTpr_Contagemresultado_crfspessoacod = deserialized.gxTpr_Contagemresultado_crfspessoacod;
         obj.gxTpr_Contagemresultado_contadorfsnom = deserialized.gxTpr_Contagemresultado_contadorfsnom;
         obj.gxTpr_Contagemresultado_demanda = deserialized.gxTpr_Contagemresultado_demanda;
         obj.gxTpr_Contagemresultado_dmnsrvprst = deserialized.gxTpr_Contagemresultado_dmnsrvprst;
         obj.gxTpr_Contagemresultado_demanda_order = deserialized.gxTpr_Contagemresultado_demanda_order;
         obj.gxTpr_Contagemresultado_erros = deserialized.gxTpr_Contagemresultado_erros;
         obj.gxTpr_Contagemresultado_demandafm = deserialized.gxTpr_Contagemresultado_demandafm;
         obj.gxTpr_Contagemresultado_demandafm_order = deserialized.gxTpr_Contagemresultado_demandafm_order;
         obj.gxTpr_Contagemresultado_ss = deserialized.gxTpr_Contagemresultado_ss;
         obj.gxTpr_Contagemresultado_osfsosfm = deserialized.gxTpr_Contagemresultado_osfsosfm;
         obj.gxTpr_Contagemresultado_osmanual = deserialized.gxTpr_Contagemresultado_osmanual;
         obj.gxTpr_Contagemresultado_descricao = deserialized.gxTpr_Contagemresultado_descricao;
         obj.gxTpr_Contagemresultado_link = deserialized.gxTpr_Contagemresultado_link;
         obj.gxTpr_Contagemresultado_sistemacod = deserialized.gxTpr_Contagemresultado_sistemacod;
         obj.gxTpr_Contagemresultado_sistemanom = deserialized.gxTpr_Contagemresultado_sistemanom;
         obj.gxTpr_Contagemresultado_sistemaareacod = deserialized.gxTpr_Contagemresultado_sistemaareacod;
         obj.gxTpr_Contagemrresultado_sistemasigla = deserialized.gxTpr_Contagemrresultado_sistemasigla;
         obj.gxTpr_Contagemresultado_sistemacoord = deserialized.gxTpr_Contagemresultado_sistemacoord;
         obj.gxTpr_Contagemresultado_sistemaativo = deserialized.gxTpr_Contagemresultado_sistemaativo;
         obj.gxTpr_Contagemresultado_sistemagestor = deserialized.gxTpr_Contagemresultado_sistemagestor;
         obj.gxTpr_Modulo_codigo = deserialized.gxTpr_Modulo_codigo;
         obj.gxTpr_Modulo_nome = deserialized.gxTpr_Modulo_nome;
         obj.gxTpr_Modulo_sigla = deserialized.gxTpr_Modulo_sigla;
         obj.gxTpr_Contagemresultado_naocnfdmncod = deserialized.gxTpr_Contagemresultado_naocnfdmncod;
         obj.gxTpr_Contagemresultado_naocnfdmnnom = deserialized.gxTpr_Contagemresultado_naocnfdmnnom;
         obj.gxTpr_Contagemresultado_naocnfdmngls = deserialized.gxTpr_Contagemresultado_naocnfdmngls;
         obj.gxTpr_Contagemresultado_statusdmn = deserialized.gxTpr_Contagemresultado_statusdmn;
         obj.gxTpr_Contagemresultado_statusultcnt = deserialized.gxTpr_Contagemresultado_statusultcnt;
         obj.gxTpr_Contagemresultado_statusdmnvnc = deserialized.gxTpr_Contagemresultado_statusdmnvnc;
         obj.gxTpr_Contagemresultado_dataultcnt = deserialized.gxTpr_Contagemresultado_dataultcnt;
         obj.gxTpr_Contagemresultado_horaultcnt = deserialized.gxTpr_Contagemresultado_horaultcnt;
         obj.gxTpr_Contagemresultado_cntcomnaocnf = deserialized.gxTpr_Contagemresultado_cntcomnaocnf;
         obj.gxTpr_Contagemresultado_pffinal = deserialized.gxTpr_Contagemresultado_pffinal;
         obj.gxTpr_Contagemresultado_contadorfm = deserialized.gxTpr_Contagemresultado_contadorfm;
         obj.gxTpr_Contagemresultado_pfbfmultima = deserialized.gxTpr_Contagemresultado_pfbfmultima;
         obj.gxTpr_Contagemresultado_pflfmultima = deserialized.gxTpr_Contagemresultado_pflfmultima;
         obj.gxTpr_Contagemresultado_pfbfsultima = deserialized.gxTpr_Contagemresultado_pfbfsultima;
         obj.gxTpr_Contagemresultado_cstuntultima = deserialized.gxTpr_Contagemresultado_cstuntultima;
         obj.gxTpr_Contagemresultado_pflfsultima = deserialized.gxTpr_Contagemresultado_pflfsultima;
         obj.gxTpr_Contagemresultado_deflatorcnt = deserialized.gxTpr_Contagemresultado_deflatorcnt;
         obj.gxTpr_Contagemresultado_servico = deserialized.gxTpr_Contagemresultado_servico;
         obj.gxTpr_Contagemresultado_cntsrvcod = deserialized.gxTpr_Contagemresultado_cntsrvcod;
         obj.gxTpr_Contagemresultado_cntsrvals = deserialized.gxTpr_Contagemresultado_cntsrvals;
         obj.gxTpr_Contagemresultado_servicogrupo = deserialized.gxTpr_Contagemresultado_servicogrupo;
         obj.gxTpr_Contagemresultado_servicosigla = deserialized.gxTpr_Contagemresultado_servicosigla;
         obj.gxTpr_Contagemresultado_servicotela = deserialized.gxTpr_Contagemresultado_servicotela;
         obj.gxTpr_Contagemresultado_servicoativo = deserialized.gxTpr_Contagemresultado_servicoativo;
         obj.gxTpr_Contagemresultado_serviconaorqratr = deserialized.gxTpr_Contagemresultado_serviconaorqratr;
         obj.gxTpr_Contagemresultado_servicoss = deserialized.gxTpr_Contagemresultado_servicoss;
         obj.gxTpr_Contagemresultado_servicosssgl = deserialized.gxTpr_Contagemresultado_servicosssgl;
         obj.gxTpr_Contagemresultado_srvlnhnegcod = deserialized.gxTpr_Contagemresultado_srvlnhnegcod;
         obj.gxTpr_Contagemresultado_ehvalidacao = deserialized.gxTpr_Contagemresultado_ehvalidacao;
         obj.gxTpr_Contagemresultado_esforcototal = deserialized.gxTpr_Contagemresultado_esforcototal;
         obj.gxTpr_Contagemresultado_esforcosoma = deserialized.gxTpr_Contagemresultado_esforcosoma;
         obj.gxTpr_Contagemresultado_observacao = deserialized.gxTpr_Contagemresultado_observacao;
         obj.gxTpr_Contagemresultado_evidencia = deserialized.gxTpr_Contagemresultado_evidencia;
         obj.gxTpr_Contagemresultado_datacadastro = deserialized.gxTpr_Contagemresultado_datacadastro;
         obj.gxTpr_Contagemresultado_owner = deserialized.gxTpr_Contagemresultado_owner;
         obj.gxTpr_Contagemresultado_owner_identificao = deserialized.gxTpr_Contagemresultado_owner_identificao;
         obj.gxTpr_Contagemresultado_contratantedoowner = deserialized.gxTpr_Contagemresultado_contratantedoowner;
         obj.gxTpr_Contagemresultado_contratadadoowner = deserialized.gxTpr_Contagemresultado_contratadadoowner;
         obj.gxTpr_Contagemresultado_responsavel = deserialized.gxTpr_Contagemresultado_responsavel;
         obj.gxTpr_Contagemresultado_responsavelpesscod = deserialized.gxTpr_Contagemresultado_responsavelpesscod;
         obj.gxTpr_Contagemresultado_responsavelpessnome = deserialized.gxTpr_Contagemresultado_responsavelpessnome;
         obj.gxTpr_Contagemresultado_contratadadoresponsavel = deserialized.gxTpr_Contagemresultado_contratadadoresponsavel;
         obj.gxTpr_Contagemresultado_contratantedoresponsavel = deserialized.gxTpr_Contagemresultado_contratantedoresponsavel;
         obj.gxTpr_Contagemresultado_valorpf = deserialized.gxTpr_Contagemresultado_valorpf;
         obj.gxTpr_Contagemresultado_custo = deserialized.gxTpr_Contagemresultado_custo;
         obj.gxTpr_Contagemresultado_osvinculada = deserialized.gxTpr_Contagemresultado_osvinculada;
         obj.gxTpr_Contagemresultado_dmnvinculada = deserialized.gxTpr_Contagemresultado_dmnvinculada;
         obj.gxTpr_Contagemresultado_dmnvinculadaref = deserialized.gxTpr_Contagemresultado_dmnvinculadaref;
         obj.gxTpr_Contagemresultado_pfbfsimp = deserialized.gxTpr_Contagemresultado_pfbfsimp;
         obj.gxTpr_Contagemresultado_pflfsimp = deserialized.gxTpr_Contagemresultado_pflfsimp;
         obj.gxTpr_Contagemresultado_pbfinal = deserialized.gxTpr_Contagemresultado_pbfinal;
         obj.gxTpr_Contagemresultado_plfinal = deserialized.gxTpr_Contagemresultado_plfinal;
         obj.gxTpr_Contagemresultado_liqlogcod = deserialized.gxTpr_Contagemresultado_liqlogcod;
         obj.gxTpr_Contagemresultado_liquidada = deserialized.gxTpr_Contagemresultado_liquidada;
         obj.gxTpr_Contagemresultadoliqlog_data = deserialized.gxTpr_Contagemresultadoliqlog_data;
         obj.gxTpr_Contagemresultado_fncusrcod = deserialized.gxTpr_Contagemresultado_fncusrcod;
         obj.gxTpr_Contagemresultado_agrupador = deserialized.gxTpr_Contagemresultado_agrupador;
         obj.gxTpr_Contagemresultado_cntsrvprrcod = deserialized.gxTpr_Contagemresultado_cntsrvprrcod;
         obj.gxTpr_Contagemresultado_cntsrvprrnome = deserialized.gxTpr_Contagemresultado_cntsrvprrnome;
         obj.gxTpr_Contagemresultado_cntsrvprrprz = deserialized.gxTpr_Contagemresultado_cntsrvprrprz;
         obj.gxTpr_Contagemresultado_cntsrvprrcst = deserialized.gxTpr_Contagemresultado_cntsrvprrcst;
         obj.gxTpr_Contagemresultado_temdpnhmlg = deserialized.gxTpr_Contagemresultado_temdpnhmlg;
         obj.gxTpr_Contagemresultado_tempndhmlg = deserialized.gxTpr_Contagemresultado_tempndhmlg;
         obj.gxTpr_Contagemresultado_tmpestanl = deserialized.gxTpr_Contagemresultado_tmpestanl;
         obj.gxTpr_Contagemresultado_tmpestexc = deserialized.gxTpr_Contagemresultado_tmpestexc;
         obj.gxTpr_Contagemresultado_tmpestcrr = deserialized.gxTpr_Contagemresultado_tmpestcrr;
         obj.gxTpr_Contagemresultado_inicioanl = deserialized.gxTpr_Contagemresultado_inicioanl;
         obj.gxTpr_Contagemresultado_fimanl = deserialized.gxTpr_Contagemresultado_fimanl;
         obj.gxTpr_Contagemresultado_inicioexc = deserialized.gxTpr_Contagemresultado_inicioexc;
         obj.gxTpr_Contagemresultado_fimexc = deserialized.gxTpr_Contagemresultado_fimexc;
         obj.gxTpr_Contagemresultado_iniciocrr = deserialized.gxTpr_Contagemresultado_iniciocrr;
         obj.gxTpr_Contagemresultado_fimcrr = deserialized.gxTpr_Contagemresultado_fimcrr;
         obj.gxTpr_Contagemresultado_evento = deserialized.gxTpr_Contagemresultado_evento;
         obj.gxTpr_Contagemresultado_projetocod = deserialized.gxTpr_Contagemresultado_projetocod;
         obj.gxTpr_Contagemresultado_cntsrvtpvnc = deserialized.gxTpr_Contagemresultado_cntsrvtpvnc;
         obj.gxTpr_Contagemresultado_cntsrvftrm = deserialized.gxTpr_Contagemresultado_cntsrvftrm;
         obj.gxTpr_Contagemresultado_cntsrvprc = deserialized.gxTpr_Contagemresultado_cntsrvprc;
         obj.gxTpr_Contagemresultado_cntsrvvlrundcnt = deserialized.gxTpr_Contagemresultado_cntsrvvlrundcnt;
         obj.gxTpr_Contagemresultado_cntsrvsttpgmfnc = deserialized.gxTpr_Contagemresultado_cntsrvsttpgmfnc;
         obj.gxTpr_Contagemresultado_cntsrvinddvr = deserialized.gxTpr_Contagemresultado_cntsrvinddvr;
         obj.gxTpr_Contagemresultado_cntsrvqtduntcns = deserialized.gxTpr_Contagemresultado_cntsrvqtduntcns;
         obj.gxTpr_Contagemresultado_cntsrvundcnt = deserialized.gxTpr_Contagemresultado_cntsrvundcnt;
         obj.gxTpr_Contagemresultado_cntsrvundcntsgl = deserialized.gxTpr_Contagemresultado_cntsrvundcntsgl;
         obj.gxTpr_Contagemresultado_cntsrvmmn = deserialized.gxTpr_Contagemresultado_cntsrvmmn;
         obj.gxTpr_Contagemresultado_przanl = deserialized.gxTpr_Contagemresultado_przanl;
         obj.gxTpr_Contagemresultado_przexc = deserialized.gxTpr_Contagemresultado_przexc;
         obj.gxTpr_Contagemresultado_przcrr = deserialized.gxTpr_Contagemresultado_przcrr;
         obj.gxTpr_Contagemresultado_przrsp = deserialized.gxTpr_Contagemresultado_przrsp;
         obj.gxTpr_Contagemresultado_przgrt = deserialized.gxTpr_Contagemresultado_przgrt;
         obj.gxTpr_Contagemresultado_prztp = deserialized.gxTpr_Contagemresultado_prztp;
         obj.gxTpr_Contagemresultado_prztpdias = deserialized.gxTpr_Contagemresultado_prztpdias;
         obj.gxTpr_Contagemresultado_cntcod = deserialized.gxTpr_Contagemresultado_cntcod;
         obj.gxTpr_Contagemresultado_cntnum = deserialized.gxTpr_Contagemresultado_cntnum;
         obj.gxTpr_Contagemresultado_cntprpcod = deserialized.gxTpr_Contagemresultado_cntprpcod;
         obj.gxTpr_Contagemresultado_cntprppescod = deserialized.gxTpr_Contagemresultado_cntprppescod;
         obj.gxTpr_Contagemresultado_cntprppesnom = deserialized.gxTpr_Contagemresultado_cntprppesnom;
         obj.gxTpr_Contagemresultado_cntinddvr = deserialized.gxTpr_Contagemresultado_cntinddvr;
         obj.gxTpr_Contagemresultado_cntclcdvr = deserialized.gxTpr_Contagemresultado_cntclcdvr;
         obj.gxTpr_Contagemresultado_cntvlrundcnt = deserialized.gxTpr_Contagemresultado_cntvlrundcnt;
         obj.gxTpr_Contagemresultado_cntprdftrcada = deserialized.gxTpr_Contagemresultado_cntprdftrcada;
         obj.gxTpr_Contagemresultado_cntprdftrini = deserialized.gxTpr_Contagemresultado_cntprdftrini;
         obj.gxTpr_Contagemresultado_cntprdftrfim = deserialized.gxTpr_Contagemresultado_cntprdftrfim;
         obj.gxTpr_Contagemresultado_cntlmtftr = deserialized.gxTpr_Contagemresultado_cntlmtftr;
         obj.gxTpr_Contagemresultado_datvgninc = deserialized.gxTpr_Contagemresultado_datvgninc;
         obj.gxTpr_Contagemresultado_datincta = deserialized.gxTpr_Contagemresultado_datincta;
         obj.gxTpr_Contagemresultado_serviconome = deserialized.gxTpr_Contagemresultado_serviconome;
         obj.gxTpr_Contagemresultado_servicotphrrq = deserialized.gxTpr_Contagemresultado_servicotphrrq;
         obj.gxTpr_Contagemresultado_codsrvvnc = deserialized.gxTpr_Contagemresultado_codsrvvnc;
         obj.gxTpr_Contagemresultado_siglasrvvnc = deserialized.gxTpr_Contagemresultado_siglasrvvnc;
         obj.gxTpr_Contagemresultado_codsrvssvnc = deserialized.gxTpr_Contagemresultado_codsrvssvnc;
         obj.gxTpr_Contagemresultado_cntsrvvnccod = deserialized.gxTpr_Contagemresultado_cntsrvvnccod;
         obj.gxTpr_Contagemresultado_cntvnccod = deserialized.gxTpr_Contagemresultado_cntvnccod;
         obj.gxTpr_Contagemresultado_glsindvalor = deserialized.gxTpr_Contagemresultado_glsindvalor;
         obj.gxTpr_Contagemresultado_tiporegistro = deserialized.gxTpr_Contagemresultado_tiporegistro;
         obj.gxTpr_Contagemresultado_uoowner = deserialized.gxTpr_Contagemresultado_uoowner;
         obj.gxTpr_Contagemresultado_referencia = deserialized.gxTpr_Contagemresultado_referencia;
         obj.gxTpr_Contagemresultado_restricoes = deserialized.gxTpr_Contagemresultado_restricoes;
         obj.gxTpr_Contagemresultado_prioridadeprevista = deserialized.gxTpr_Contagemresultado_prioridadeprevista;
         obj.gxTpr_Contagemresultado_przinc = deserialized.gxTpr_Contagemresultado_przinc;
         obj.gxTpr_Contagemresultado_temproposta = deserialized.gxTpr_Contagemresultado_temproposta;
         obj.gxTpr_Contagemresultado_combinada = deserialized.gxTpr_Contagemresultado_combinada;
         obj.gxTpr_Contagemresultado_entrega = deserialized.gxTpr_Contagemresultado_entrega;
         obj.gxTpr_Contagemresultado_semcusto = deserialized.gxTpr_Contagemresultado_semcusto;
         obj.gxTpr_Contagemresultado_vlrcnc = deserialized.gxTpr_Contagemresultado_vlrcnc;
         obj.gxTpr_Contagemresultado_pfcnc = deserialized.gxTpr_Contagemresultado_pfcnc;
         obj.gxTpr_Contagemresultado_dataprvpgm = deserialized.gxTpr_Contagemresultado_dataprvpgm;
         obj.gxTpr_Contagemresultado_quantidadesolicitada = deserialized.gxTpr_Contagemresultado_quantidadesolicitada;
         obj.gxTpr_Contagemresultado_requisitadoporcontratante = deserialized.gxTpr_Contagemresultado_requisitadoporcontratante;
         obj.gxTpr_Contagemresultado_requisitante = deserialized.gxTpr_Contagemresultado_requisitante;
         obj.gxTpr_Usuario_cargouonom = deserialized.gxTpr_Usuario_cargouonom;
         obj.gxTpr_Contratoservicos_undcntnome = deserialized.gxTpr_Contratoservicos_undcntnome;
         obj.gxTpr_Contratoservicos_qntuntcns = deserialized.gxTpr_Contratoservicos_qntuntcns;
         obj.gxTpr_Contratoservicosprazo_complexidade = deserialized.gxTpr_Contratoservicosprazo_complexidade;
         obj.gxTpr_Contratoservicosprioridade_codigo = deserialized.gxTpr_Contratoservicosprioridade_codigo;
         obj.gxTpr_Contratoservicos_localexec = deserialized.gxTpr_Contratoservicos_localexec;
         obj.gxTpr_Contagemresultado_prazoanalise = deserialized.gxTpr_Contagemresultado_prazoanalise;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataDmn") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataInicio") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataEntrega") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_HoraEntrega") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataPrevista") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataExecucao") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataEntregaReal") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataHomologacao") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoInicialDias") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_prazoinicialdias = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoMaisDias") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_prazomaisdias = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaPessoaCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaPessoaNom") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaSigla") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaTipoFab") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaLogo") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaLogo_GXI") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCNPJ") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contratada_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "IndiceDivergencia") )
               {
                  gxTv_SdtSDT_WP_OS_Indicedivergencia = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CalculoDivergencia") )
               {
                  gxTv_SdtSDT_WP_OS_Calculodivergencia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemSigla") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemPesCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemPesNom") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemAreaCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemareacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemUsaSistema") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemusasistema = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFSCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFSPessoaCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_crfspessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFSNom") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_demanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DmnSrvPrst") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda_ORDER") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_demanda_order = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Erros") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_erros = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_ORDER") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm_order = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SS") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_ss = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OsFsOsFm") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSManual") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_osmanual = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Descricao") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Link") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaNom") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contagemresultado_SistemaAreaCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaareacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemrResultado_SistemaSigla") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaCoord") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaAtivo") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaGestor") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo") )
               {
                  gxTv_SdtSDT_WP_OS_Modulo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Nome") )
               {
                  gxTv_SdtSDT_WP_OS_Modulo_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Sigla") )
               {
                  gxTv_SdtSDT_WP_OS_Modulo_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmncod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnNom") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnGls") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmngls = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusUltCnt") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_statusultcnt = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmnVnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataUltCnt") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_HoraUltCnt") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntComNaoCnf") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntcomnaocnf = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFFinal") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFM") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfm = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFMUltima") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFMUltima") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFSUltima") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CstUntUltima") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFSUltima") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DeflatorCnt") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Servico") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servico = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvAls") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoGrupo") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servicogrupo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSigla") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoTela") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoAtivo") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servicoativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoNaoRqrAtr") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_serviconaorqratr = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSS") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servicoss = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSSSgl") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SrvLnhNegCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_srvlnhnegcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_EhValidacao") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_ehvalidacao = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_EsforcoTotal") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_EsforcoSoma") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_esforcosoma = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Observacao") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_observacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Evidencia") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataCadastro") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Owner") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Owner_Identificao") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratanteDoOwner") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoowner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaDoOwner") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoowner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Responsavel") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_responsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ResponsavelPessCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpesscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ResponsavelPessNome") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaDoResponsavel") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoresponsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratanteDoResponsavel") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoresponsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ValorPF") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Custo") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_custo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_osvinculada = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DmnVinculada") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DmnVinculadaRef") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFSImp") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFSImp") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PBFinal") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PLFinal") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_LiqLogCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_liqlogcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Liquidada") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_liquidada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoLiqLog_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_FncUsrCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_fncusrcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Agrupador") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvPrrCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvPrrNome") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvPrrPrz") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvPrrCst") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TemDpnHmlg") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_temdpnhmlg = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TemPndHmlg") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_tempndhmlg = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TmpEstAnl") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestanl = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TmpEstExc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestexc = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TmpEstCrr") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestcrr = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_InicioAnl") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_FimAnl") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_InicioExc") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_FimExc") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_InicioCrr") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_FimCrr") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Evento") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_evento = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ProjetoCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_projetocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvTpVnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvFtrm") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvPrc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvVlrUndCnt") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvSttPgmFnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvIndDvr") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvQtdUntCns") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvUndCnt") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcnt = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvUndCntSgl") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvMmn") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzAnl") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_przanl = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzExc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_przexc = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzCrr") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_przcrr = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzRsp") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_przrsp = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzGrt") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_przgrt = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzTp") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_prztp = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzTpDias") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntNum") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntPrpCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntprpcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntPrpPesCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntPrpPesNom") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntIndDvr") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntClcDvr") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntVlrUndCnt") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntPrdFtrCada") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntPrdFtrIni") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrini = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntPrdFtrFim") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrfim = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntLmtFtr") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DatVgnInc") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DatIncTA") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datincta = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_datincta = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoNome") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoTpHrrq") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_servicotphrrq = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CodSrvVnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvvnc = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SiglaSrvVnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CodSrvSSVnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvssvnc = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvVncCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvnccod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntVncCod") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_cntvnccod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_GlsIndValor") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TipoRegistro") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_tiporegistro = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_UOOwner") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_uoowner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Referencia") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_referencia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Restricoes") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrioridadePrevista") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrzInc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_przinc = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TemProposta") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_temproposta = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Combinada") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_combinada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Entrega") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_entrega = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SemCusto") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_semcusto = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_VlrCnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFCnc") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataPrvPgm") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_QuantidadeSolicitada") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_RequisitadoPorContratante") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_requisitadoporcontratante = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Requisitante") )
               {
                  gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUONom") )
               {
                  gxTv_SdtSDT_WP_OS_Usuario_cargouonom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntNome") )
               {
                  gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_QntUntCns") )
               {
                  gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrazo_Complexidade") )
               {
                  gxTv_SdtSDT_WP_OS_Contratoservicosprazo_complexidade = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Codigo") )
               {
                  gxTv_SdtSDT_WP_OS_Contratoservicosprioridade_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_LocalExec") )
               {
                  gxTv_SdtSDT_WP_OS_Contratoservicos_localexec = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoAnalise") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_WP_OS";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataDmn");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataDmn", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataInicio");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataInicio", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataEntrega");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataEntrega", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega) )
         {
            oWriter.WriteStartElement("ContagemResultado_HoraEntrega");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_HoraEntrega", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataPrevista");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataPrevista", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataExecucao");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataExecucao", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataEntregaReal");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataEntregaReal", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataHomologacao");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataHomologacao", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultado_PrazoInicialDias", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_prazoinicialdias), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrazoMaisDias", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_prazomaisdias), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaPessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaPessoaNom", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaSigla", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaTipoFab", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaLogo", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaLogo_GXI", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaCNPJ", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contratada_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("IndiceDivergencia", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Indicedivergencia, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("CalculoDivergencia", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Calculodivergencia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemSigla", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemPesNom", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemAreaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemareacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemUsaSistema", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemusasistema)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContadorFSCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CrFSPessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_crfspessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContadorFSNom", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Demanda", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_demanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DmnSrvPrst", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Demanda_ORDER", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_demanda_order), 18, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Erros", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_erros), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DemandaFM", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DemandaFM_ORDER", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm_order), 18, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SS", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_ss), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_OsFsOsFm", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_OSManual", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_osmanual)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Descricao", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Link", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SistemaNom", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contagemresultado_SistemaAreaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaareacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemrResultado_SistemaSigla", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SistemaCoord", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SistemaAtivo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SistemaGestor", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Modulo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Modulo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Modulo_Nome", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Modulo_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Modulo_Sigla", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Modulo_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_NaoCnfDmnCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmncod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_NaoCnfDmnNom", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_NaoCnfDmnGls", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmngls)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_StatusDmn", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_StatusUltCnt", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_statusultcnt), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_StatusDmnVnc", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataUltCnt");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataUltCnt", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultado_HoraUltCnt", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntComNaoCnf", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntcomnaocnf), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFFinal", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContadorFM", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfm), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFBFMUltima", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFLFMUltima", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFBFSUltima", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CstUntUltima", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFLFSUltima", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DeflatorCnt", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt, 6, 3)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Servico", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_servico), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvAls", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoGrupo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_servicogrupo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoSigla", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoTela", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoAtivo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_servicoativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoNaoRqrAtr", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_serviconaorqratr)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoSS", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_servicoss), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoSSSgl", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SrvLnhNegCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_srvlnhnegcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_EhValidacao", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_ehvalidacao)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_EsforcoTotal", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_EsforcoSoma", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_esforcosoma), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Observacao", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_observacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Evidencia", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataCadastro");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataCadastro", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultado_Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Owner_Identificao", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratanteDoOwner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoowner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaDoOwner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoowner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Responsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_responsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ResponsavelPessCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpesscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ResponsavelPessNome", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaDoResponsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoresponsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratanteDoResponsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoresponsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ValorPF", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Custo", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_custo, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_OSVinculada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_osvinculada), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DmnVinculada", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DmnVinculadaRef", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFBFSImp", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFLFSImp", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PBFinal", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PLFinal", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_LiqLogCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_liqlogcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Liquidada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_liquidada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data) )
         {
            oWriter.WriteStartElement("ContagemResultadoLiqLog_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoLiqLog_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultado_FncUsrCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_fncusrcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Agrupador", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvPrrCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvPrrNome", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvPrrPrz", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvPrrCst", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_TemDpnHmlg", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_temdpnhmlg)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_TemPndHmlg", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_tempndhmlg)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_TmpEstAnl", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestanl), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_TmpEstExc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestexc), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_TmpEstCrr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestcrr), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl) )
         {
            oWriter.WriteStartElement("ContagemResultado_InicioAnl");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_InicioAnl", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl) )
         {
            oWriter.WriteStartElement("ContagemResultado_FimAnl");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_FimAnl", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc) )
         {
            oWriter.WriteStartElement("ContagemResultado_InicioExc");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_InicioExc", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc) )
         {
            oWriter.WriteStartElement("ContagemResultado_FimExc");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_FimExc", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr) )
         {
            oWriter.WriteStartElement("ContagemResultado_InicioCrr");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_InicioCrr", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr) )
         {
            oWriter.WriteStartElement("ContagemResultado_FimCrr");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_FimCrr", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultado_Evento", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_evento), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ProjetoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_projetocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvTpVnc", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvFtrm", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvPrc", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc, 7, 3)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvVlrUndCnt", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvSttPgmFnc", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvIndDvr", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvQtdUntCns", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns, 9, 4)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvUndCnt", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcnt), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvUndCntSgl", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvMmn", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzAnl", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_przanl), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzExc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_przexc), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzCrr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_przcrr), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzRsp", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_przrsp), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzGrt", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_przgrt), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzTp", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_prztp));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzTpDias", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntNum", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntPrpCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntprpcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntPrpPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntPrpPesNom", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntIndDvr", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntClcDvr", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntVlrUndCnt", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntPrdFtrCada", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntPrdFtrIni", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrini), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntPrdFtrFim", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrfim), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntLmtFtr", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc) )
         {
            oWriter.WriteStartElement("ContagemResultado_DatVgnInc");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DatVgnInc", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_datincta) )
         {
            oWriter.WriteStartElement("ContagemResultado_DatIncTA");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datincta)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datincta)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datincta)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DatIncTA", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultado_ServicoNome", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ServicoTpHrrq", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_servicotphrrq), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CodSrvVnc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvvnc), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SiglaSrvVnc", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CodSrvSSVnc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvssvnc), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvVncCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvnccod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_CntVncCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_cntvnccod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_GlsIndValor", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_TipoRegistro", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_tiporegistro), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_UOOwner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_uoowner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Referencia", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_referencia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Restricoes", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrioridadePrevista", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PrzInc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_przinc), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_TemProposta", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_temproposta)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Combinada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_combinada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Entrega", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_entrega), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_SemCusto", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_semcusto)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_VlrCnc", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_PFCnc", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataPrvPgm");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataPrvPgm", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultado_QuantidadeSolicitada", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada, 9, 4)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_RequisitadoPorContratante", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_WP_OS_Contagemresultado_requisitadoporcontratante)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Requisitante", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_CargoUONom", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Usuario_cargouonom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicos_UndCntNome", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicos_QntUntCns", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns, 9, 4)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrazo_Complexidade", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contratoservicosprazo_complexidade), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contratoservicosprioridade_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicos_LocalExec", StringUtil.RTrim( gxTv_SdtSDT_WP_OS_Contratoservicos_localexec));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise) )
         {
            oWriter.WriteStartElement("ContagemResultado_PrazoAnalise");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_PrazoAnalise", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtSDT_WP_OS_Contagemresultado_codigo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataDmn", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataInicio", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataEntrega", sDateCnv, false);
         datetime_STZ = gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_HoraEntrega", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataPrevista", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataExecucao", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataEntregaReal", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataHomologacao", sDateCnv, false);
         AddObjectProperty("ContagemResultado_PrazoInicialDias", gxTv_SdtSDT_WP_OS_Contagemresultado_prazoinicialdias, false);
         AddObjectProperty("ContagemResultado_PrazoMaisDias", gxTv_SdtSDT_WP_OS_Contagemresultado_prazomaisdias, false);
         AddObjectProperty("ContagemResultado_ContratadaCod", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacod, false);
         AddObjectProperty("ContagemResultado_ContratadaPessoaCod", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoacod, false);
         AddObjectProperty("ContagemResultado_ContratadaPessoaNom", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom, false);
         AddObjectProperty("ContagemResultado_ContratadaSigla", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla, false);
         AddObjectProperty("ContagemResultado_ContratadaTipoFab", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab, false);
         AddObjectProperty("ContagemResultado_ContratadaLogo", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo, false);
         AddObjectProperty("ContagemResultado_ContratadaLogo_GXI", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi, false);
         AddObjectProperty("ContagemResultado_ContratadaCNPJ", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj, false);
         AddObjectProperty("Contratada_AreaTrabalhoCod", gxTv_SdtSDT_WP_OS_Contratada_areatrabalhocod, false);
         AddObjectProperty("IndiceDivergencia", gxTv_SdtSDT_WP_OS_Indicedivergencia, false);
         AddObjectProperty("CalculoDivergencia", gxTv_SdtSDT_WP_OS_Calculodivergencia, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemCod", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemcod, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemSigla", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemPesCod", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempescod, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemPesNom", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemAreaCod", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemareacod, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemUsaSistema", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemusasistema, false);
         AddObjectProperty("ContagemResultado_ContadorFSCod", gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfscod, false);
         AddObjectProperty("ContagemResultado_CrFSPessoaCod", gxTv_SdtSDT_WP_OS_Contagemresultado_crfspessoacod, false);
         AddObjectProperty("ContagemResultado_ContadorFSNom", gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom, false);
         AddObjectProperty("ContagemResultado_Demanda", gxTv_SdtSDT_WP_OS_Contagemresultado_demanda, false);
         AddObjectProperty("ContagemResultado_DmnSrvPrst", gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst, false);
         AddObjectProperty("ContagemResultado_Demanda_ORDER", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_demanda_order), 18, 0)), false);
         AddObjectProperty("ContagemResultado_Erros", gxTv_SdtSDT_WP_OS_Contagemresultado_erros, false);
         AddObjectProperty("ContagemResultado_DemandaFM", gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm, false);
         AddObjectProperty("ContagemResultado_DemandaFM_ORDER", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm_order), 18, 0)), false);
         AddObjectProperty("ContagemResultado_SS", gxTv_SdtSDT_WP_OS_Contagemresultado_ss, false);
         AddObjectProperty("ContagemResultado_OsFsOsFm", gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm, false);
         AddObjectProperty("ContagemResultado_OSManual", gxTv_SdtSDT_WP_OS_Contagemresultado_osmanual, false);
         AddObjectProperty("ContagemResultado_Descricao", gxTv_SdtSDT_WP_OS_Contagemresultado_descricao, false);
         AddObjectProperty("ContagemResultado_Link", gxTv_SdtSDT_WP_OS_Contagemresultado_link, false);
         AddObjectProperty("ContagemResultado_SistemaCod", gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacod, false);
         AddObjectProperty("ContagemResultado_SistemaNom", gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom, false);
         AddObjectProperty("Contagemresultado_SistemaAreaCod", gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaareacod, false);
         AddObjectProperty("ContagemrResultado_SistemaSigla", gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla, false);
         AddObjectProperty("ContagemResultado_SistemaCoord", gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord, false);
         AddObjectProperty("ContagemResultado_SistemaAtivo", gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaativo, false);
         AddObjectProperty("ContagemResultado_SistemaGestor", gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor, false);
         AddObjectProperty("Modulo_Codigo", gxTv_SdtSDT_WP_OS_Modulo_codigo, false);
         AddObjectProperty("Modulo_Nome", gxTv_SdtSDT_WP_OS_Modulo_nome, false);
         AddObjectProperty("Modulo_Sigla", gxTv_SdtSDT_WP_OS_Modulo_sigla, false);
         AddObjectProperty("ContagemResultado_NaoCnfDmnCod", gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmncod, false);
         AddObjectProperty("ContagemResultado_NaoCnfDmnNom", gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom, false);
         AddObjectProperty("ContagemResultado_NaoCnfDmnGls", gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmngls, false);
         AddObjectProperty("ContagemResultado_StatusDmn", gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn, false);
         AddObjectProperty("ContagemResultado_StatusUltCnt", gxTv_SdtSDT_WP_OS_Contagemresultado_statusultcnt, false);
         AddObjectProperty("ContagemResultado_StatusDmnVnc", gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataUltCnt", sDateCnv, false);
         AddObjectProperty("ContagemResultado_HoraUltCnt", gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt, false);
         AddObjectProperty("ContagemResultado_CntComNaoCnf", gxTv_SdtSDT_WP_OS_Contagemresultado_cntcomnaocnf, false);
         AddObjectProperty("ContagemResultado_PFFinal", gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal, false);
         AddObjectProperty("ContagemResultado_ContadorFM", gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfm, false);
         AddObjectProperty("ContagemResultado_PFBFMUltima", gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima, false);
         AddObjectProperty("ContagemResultado_PFLFMUltima", gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima, false);
         AddObjectProperty("ContagemResultado_PFBFSUltima", gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima, false);
         AddObjectProperty("ContagemResultado_CstUntUltima", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima, 18, 5)), false);
         AddObjectProperty("ContagemResultado_PFLFSUltima", gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima, false);
         AddObjectProperty("ContagemResultado_DeflatorCnt", gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt, false);
         AddObjectProperty("ContagemResultado_Servico", gxTv_SdtSDT_WP_OS_Contagemresultado_servico, false);
         AddObjectProperty("ContagemResultado_CntSrvCod", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvcod, false);
         AddObjectProperty("ContagemResultado_CntSrvAls", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals, false);
         AddObjectProperty("ContagemResultado_ServicoGrupo", gxTv_SdtSDT_WP_OS_Contagemresultado_servicogrupo, false);
         AddObjectProperty("ContagemResultado_ServicoSigla", gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla, false);
         AddObjectProperty("ContagemResultado_ServicoTela", gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela, false);
         AddObjectProperty("ContagemResultado_ServicoAtivo", gxTv_SdtSDT_WP_OS_Contagemresultado_servicoativo, false);
         AddObjectProperty("ContagemResultado_ServicoNaoRqrAtr", gxTv_SdtSDT_WP_OS_Contagemresultado_serviconaorqratr, false);
         AddObjectProperty("ContagemResultado_ServicoSS", gxTv_SdtSDT_WP_OS_Contagemresultado_servicoss, false);
         AddObjectProperty("ContagemResultado_ServicoSSSgl", gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl, false);
         AddObjectProperty("ContagemResultado_SrvLnhNegCod", gxTv_SdtSDT_WP_OS_Contagemresultado_srvlnhnegcod, false);
         AddObjectProperty("ContagemResultado_EhValidacao", gxTv_SdtSDT_WP_OS_Contagemresultado_ehvalidacao, false);
         AddObjectProperty("ContagemResultado_EsforcoTotal", gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal, false);
         AddObjectProperty("ContagemResultado_EsforcoSoma", gxTv_SdtSDT_WP_OS_Contagemresultado_esforcosoma, false);
         AddObjectProperty("ContagemResultado_Observacao", gxTv_SdtSDT_WP_OS_Contagemresultado_observacao, false);
         AddObjectProperty("ContagemResultado_Evidencia", gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataCadastro", sDateCnv, false);
         AddObjectProperty("ContagemResultado_Owner", gxTv_SdtSDT_WP_OS_Contagemresultado_owner, false);
         AddObjectProperty("ContagemResultado_Owner_Identificao", gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao, false);
         AddObjectProperty("ContagemResultado_ContratanteDoOwner", gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoowner, false);
         AddObjectProperty("ContagemResultado_ContratadaDoOwner", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoowner, false);
         AddObjectProperty("ContagemResultado_Responsavel", gxTv_SdtSDT_WP_OS_Contagemresultado_responsavel, false);
         AddObjectProperty("ContagemResultado_ResponsavelPessCod", gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpesscod, false);
         AddObjectProperty("ContagemResultado_ResponsavelPessNome", gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome, false);
         AddObjectProperty("ContagemResultado_ContratadaDoResponsavel", gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoresponsavel, false);
         AddObjectProperty("ContagemResultado_ContratanteDoResponsavel", gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoresponsavel, false);
         AddObjectProperty("ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf, 18, 5)), false);
         AddObjectProperty("ContagemResultado_Custo", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_custo, 18, 5)), false);
         AddObjectProperty("ContagemResultado_OSVinculada", gxTv_SdtSDT_WP_OS_Contagemresultado_osvinculada, false);
         AddObjectProperty("ContagemResultado_DmnVinculada", gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada, false);
         AddObjectProperty("ContagemResultado_DmnVinculadaRef", gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref, false);
         AddObjectProperty("ContagemResultado_PFBFSImp", gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp, false);
         AddObjectProperty("ContagemResultado_PFLFSImp", gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp, false);
         AddObjectProperty("ContagemResultado_PBFinal", gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal, false);
         AddObjectProperty("ContagemResultado_PLFinal", gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal, false);
         AddObjectProperty("ContagemResultado_LiqLogCod", gxTv_SdtSDT_WP_OS_Contagemresultado_liqlogcod, false);
         AddObjectProperty("ContagemResultado_Liquidada", gxTv_SdtSDT_WP_OS_Contagemresultado_liquidada, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoLiqLog_Data", sDateCnv, false);
         AddObjectProperty("ContagemResultado_FncUsrCod", gxTv_SdtSDT_WP_OS_Contagemresultado_fncusrcod, false);
         AddObjectProperty("ContagemResultado_Agrupador", gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador, false);
         AddObjectProperty("ContagemResultado_CntSrvPrrCod", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcod, false);
         AddObjectProperty("ContagemResultado_CntSrvPrrNome", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome, false);
         AddObjectProperty("ContagemResultado_CntSrvPrrPrz", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz, false);
         AddObjectProperty("ContagemResultado_CntSrvPrrCst", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst, false);
         AddObjectProperty("ContagemResultado_TemDpnHmlg", gxTv_SdtSDT_WP_OS_Contagemresultado_temdpnhmlg, false);
         AddObjectProperty("ContagemResultado_TemPndHmlg", gxTv_SdtSDT_WP_OS_Contagemresultado_tempndhmlg, false);
         AddObjectProperty("ContagemResultado_TmpEstAnl", gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestanl, false);
         AddObjectProperty("ContagemResultado_TmpEstExc", gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestexc, false);
         AddObjectProperty("ContagemResultado_TmpEstCrr", gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestcrr, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_InicioAnl", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_FimAnl", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_InicioExc", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_FimExc", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_InicioCrr", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_FimCrr", sDateCnv, false);
         AddObjectProperty("ContagemResultado_Evento", gxTv_SdtSDT_WP_OS_Contagemresultado_evento, false);
         AddObjectProperty("ContagemResultado_ProjetoCod", gxTv_SdtSDT_WP_OS_Contagemresultado_projetocod, false);
         AddObjectProperty("ContagemResultado_CntSrvTpVnc", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc, false);
         AddObjectProperty("ContagemResultado_CntSrvFtrm", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm, false);
         AddObjectProperty("ContagemResultado_CntSrvPrc", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc, false);
         AddObjectProperty("ContagemResultado_CntSrvVlrUndCnt", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt, 18, 5)), false);
         AddObjectProperty("ContagemResultado_CntSrvSttPgmFnc", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc, false);
         AddObjectProperty("ContagemResultado_CntSrvIndDvr", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr, false);
         AddObjectProperty("ContagemResultado_CntSrvQtdUntCns", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns, false);
         AddObjectProperty("ContagemResultado_CntSrvUndCnt", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcnt, false);
         AddObjectProperty("ContagemResultado_CntSrvUndCntSgl", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl, false);
         AddObjectProperty("ContagemResultado_CntSrvMmn", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn, false);
         AddObjectProperty("ContagemResultado_PrzAnl", gxTv_SdtSDT_WP_OS_Contagemresultado_przanl, false);
         AddObjectProperty("ContagemResultado_PrzExc", gxTv_SdtSDT_WP_OS_Contagemresultado_przexc, false);
         AddObjectProperty("ContagemResultado_PrzCrr", gxTv_SdtSDT_WP_OS_Contagemresultado_przcrr, false);
         AddObjectProperty("ContagemResultado_PrzRsp", gxTv_SdtSDT_WP_OS_Contagemresultado_przrsp, false);
         AddObjectProperty("ContagemResultado_PrzGrt", gxTv_SdtSDT_WP_OS_Contagemresultado_przgrt, false);
         AddObjectProperty("ContagemResultado_PrzTp", gxTv_SdtSDT_WP_OS_Contagemresultado_prztp, false);
         AddObjectProperty("ContagemResultado_PrzTpDias", gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias, false);
         AddObjectProperty("ContagemResultado_CntCod", gxTv_SdtSDT_WP_OS_Contagemresultado_cntcod, false);
         AddObjectProperty("ContagemResultado_CntNum", gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum, false);
         AddObjectProperty("ContagemResultado_CntPrpCod", gxTv_SdtSDT_WP_OS_Contagemresultado_cntprpcod, false);
         AddObjectProperty("ContagemResultado_CntPrpPesCod", gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppescod, false);
         AddObjectProperty("ContagemResultado_CntPrpPesNom", gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom, false);
         AddObjectProperty("ContagemResultado_CntIndDvr", gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr, false);
         AddObjectProperty("ContagemResultado_CntClcDvr", gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr, false);
         AddObjectProperty("ContagemResultado_CntVlrUndCnt", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt, 18, 5)), false);
         AddObjectProperty("ContagemResultado_CntPrdFtrCada", gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada, false);
         AddObjectProperty("ContagemResultado_CntPrdFtrIni", gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrini, false);
         AddObjectProperty("ContagemResultado_CntPrdFtrFim", gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrfim, false);
         AddObjectProperty("ContagemResultado_CntLmtFtr", gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DatVgnInc", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_datincta)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_datincta)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_datincta)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DatIncTA", sDateCnv, false);
         AddObjectProperty("ContagemResultado_ServicoNome", gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome, false);
         AddObjectProperty("ContagemResultado_ServicoTpHrrq", gxTv_SdtSDT_WP_OS_Contagemresultado_servicotphrrq, false);
         AddObjectProperty("ContagemResultado_CodSrvVnc", gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvvnc, false);
         AddObjectProperty("ContagemResultado_SiglaSrvVnc", gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc, false);
         AddObjectProperty("ContagemResultado_CodSrvSSVnc", gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvssvnc, false);
         AddObjectProperty("ContagemResultado_CntSrvVncCod", gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvnccod, false);
         AddObjectProperty("ContagemResultado_CntVncCod", gxTv_SdtSDT_WP_OS_Contagemresultado_cntvnccod, false);
         AddObjectProperty("ContagemResultado_GlsIndValor", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor, 18, 5)), false);
         AddObjectProperty("ContagemResultado_TipoRegistro", gxTv_SdtSDT_WP_OS_Contagemresultado_tiporegistro, false);
         AddObjectProperty("ContagemResultado_UOOwner", gxTv_SdtSDT_WP_OS_Contagemresultado_uoowner, false);
         AddObjectProperty("ContagemResultado_Referencia", gxTv_SdtSDT_WP_OS_Contagemresultado_referencia, false);
         AddObjectProperty("ContagemResultado_Restricoes", gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes, false);
         AddObjectProperty("ContagemResultado_PrioridadePrevista", gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista, false);
         AddObjectProperty("ContagemResultado_PrzInc", gxTv_SdtSDT_WP_OS_Contagemresultado_przinc, false);
         AddObjectProperty("ContagemResultado_TemProposta", gxTv_SdtSDT_WP_OS_Contagemresultado_temproposta, false);
         AddObjectProperty("ContagemResultado_Combinada", gxTv_SdtSDT_WP_OS_Contagemresultado_combinada, false);
         AddObjectProperty("ContagemResultado_Entrega", gxTv_SdtSDT_WP_OS_Contagemresultado_entrega, false);
         AddObjectProperty("ContagemResultado_SemCusto", gxTv_SdtSDT_WP_OS_Contagemresultado_semcusto, false);
         AddObjectProperty("ContagemResultado_VlrCnc", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc, 18, 5)), false);
         AddObjectProperty("ContagemResultado_PFCnc", gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataPrvPgm", sDateCnv, false);
         AddObjectProperty("ContagemResultado_QuantidadeSolicitada", gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada, false);
         AddObjectProperty("ContagemResultado_RequisitadoPorContratante", gxTv_SdtSDT_WP_OS_Contagemresultado_requisitadoporcontratante, false);
         AddObjectProperty("ContagemResultado_Requisitante", gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante, false);
         AddObjectProperty("Usuario_CargoUONom", gxTv_SdtSDT_WP_OS_Usuario_cargouonom, false);
         AddObjectProperty("ContratoServicos_UndCntNome", gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome, false);
         AddObjectProperty("ContratoServicos_QntUntCns", gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns, false);
         AddObjectProperty("ContratoServicosPrazo_Complexidade", gxTv_SdtSDT_WP_OS_Contratoservicosprazo_complexidade, false);
         AddObjectProperty("ContratoServicosPrioridade_Codigo", gxTv_SdtSDT_WP_OS_Contratoservicosprioridade_codigo, false);
         AddObjectProperty("ContratoServicos_LocalExec", gxTv_SdtSDT_WP_OS_Contratoservicos_localexec, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_PrazoAnalise", sDateCnv, false);
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_codigo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataDmn" )]
      [  XmlElement( ElementName = "ContagemResultado_DataDmn"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datadmn_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datadmn
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataInicio" )]
      [  XmlElement( ElementName = "ContagemResultado_DataInicio"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datainicio_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datainicio
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataEntrega" )]
      [  XmlElement( ElementName = "ContagemResultado_DataEntrega"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataentrega_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataentrega
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_HoraEntrega" )]
      [  XmlElement( ElementName = "ContagemResultado_HoraEntrega"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_horaentrega_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_horaentrega
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataPrevista" )]
      [  XmlElement( ElementName = "ContagemResultado_DataPrevista"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataprevista_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataprevista
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataExecucao" )]
      [  XmlElement( ElementName = "ContagemResultado_DataExecucao"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataexecucao_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataexecucao
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataEntregaReal" )]
      [  XmlElement( ElementName = "ContagemResultado_DataEntregaReal"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataentregareal_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataentregareal
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataHomologacao" )]
      [  XmlElement( ElementName = "ContagemResultado_DataHomologacao"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datahomologacao_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datahomologacao
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoInicialDias" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoInicialDias"   )]
      public short gxTpr_Contagemresultado_prazoinicialdias
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_prazoinicialdias ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_prazoinicialdias = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoMaisDias" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoMaisDias"   )]
      public short gxTpr_Contagemresultado_prazomaisdias
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_prazomaisdias ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_prazomaisdias = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCod"   )]
      public int gxTpr_Contagemresultado_contratadacod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaPessoaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaPessoaCod"   )]
      public int gxTpr_Contagemresultado_contratadapessoacod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoacod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaPessoaNom" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaPessoaNom"   )]
      public String gxTpr_Contagemresultado_contratadapessoanom
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaSigla" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaSigla"   )]
      public String gxTpr_Contagemresultado_contratadasigla
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaTipoFab" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaTipoFab"   )]
      public String gxTpr_Contagemresultado_contratadatipofab
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaLogo" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaLogo"   )]
      [GxUpload()]
      public String gxTpr_Contagemresultado_contratadalogo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaLogo_GXI" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaLogo_GXI"   )]
      public String gxTpr_Contagemresultado_contratadalogo_gxi
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCNPJ" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCNPJ"   )]
      public String gxTpr_Contagemresultado_contratadacnpj
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod"   )]
      public int gxTpr_Contratada_areatrabalhocod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contratada_areatrabalhocod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contratada_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "IndiceDivergencia" )]
      [  XmlElement( ElementName = "IndiceDivergencia"   )]
      public double gxTpr_Indicedivergencia_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Indicedivergencia) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Indicedivergencia = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Indicedivergencia
      {
         get {
            return gxTv_SdtSDT_WP_OS_Indicedivergencia ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Indicedivergencia = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "CalculoDivergencia" )]
      [  XmlElement( ElementName = "CalculoDivergencia"   )]
      public String gxTpr_Calculodivergencia
      {
         get {
            return gxTv_SdtSDT_WP_OS_Calculodivergencia ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Calculodivergencia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod"   )]
      public int gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemSigla" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemSigla"   )]
      public String gxTpr_Contagemresultado_contratadaorigemsigla
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemPesCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemPesCod"   )]
      public int gxTpr_Contagemresultado_contratadaorigempescod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempescod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempescod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemPesNom" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemPesNom"   )]
      public String gxTpr_Contagemresultado_contratadaorigempesnom
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemAreaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemAreaCod"   )]
      public int gxTpr_Contagemresultado_contratadaorigemareacod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemareacod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemareacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemUsaSistema" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemUsaSistema"   )]
      public bool gxTpr_Contagemresultado_contratadaorigemusasistema
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemusasistema ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemusasistema = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFSCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFSCod"   )]
      public int gxTpr_Contagemresultado_contadorfscod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfscod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CrFSPessoaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFSPessoaCod"   )]
      public int gxTpr_Contagemresultado_crfspessoacod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_crfspessoacod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_crfspessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFSNom" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFSNom"   )]
      public String gxTpr_Contagemresultado_contadorfsnom
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda"   )]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_demanda ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_demanda = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DmnSrvPrst" )]
      [  XmlElement( ElementName = "ContagemResultado_DmnSrvPrst"   )]
      public String gxTpr_Contagemresultado_dmnsrvprst
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda_ORDER" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda_ORDER"   )]
      public long gxTpr_Contagemresultado_demanda_order
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_demanda_order ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_demanda_order = (long)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Erros" )]
      [  XmlElement( ElementName = "ContagemResultado_Erros"   )]
      public short gxTpr_Contagemresultado_erros
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_erros ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_erros = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM"   )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_ORDER" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_ORDER"   )]
      public long gxTpr_Contagemresultado_demandafm_order
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm_order ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm_order = (long)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SS" )]
      [  XmlElement( ElementName = "ContagemResultado_SS"   )]
      public int gxTpr_Contagemresultado_ss
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_ss ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_ss = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_OsFsOsFm" )]
      [  XmlElement( ElementName = "ContagemResultado_OsFsOsFm"   )]
      public String gxTpr_Contagemresultado_osfsosfm
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_OSManual" )]
      [  XmlElement( ElementName = "ContagemResultado_OSManual"   )]
      public bool gxTpr_Contagemresultado_osmanual
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_osmanual ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_osmanual = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Descricao" )]
      [  XmlElement( ElementName = "ContagemResultado_Descricao"   )]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_descricao ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Link" )]
      [  XmlElement( ElementName = "ContagemResultado_Link"   )]
      public String gxTpr_Contagemresultado_link
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_link ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaCod"   )]
      public int gxTpr_Contagemresultado_sistemacod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaNom" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaNom"   )]
      public String gxTpr_Contagemresultado_sistemanom
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contagemresultado_SistemaAreaCod" )]
      [  XmlElement( ElementName = "Contagemresultado_SistemaAreaCod"   )]
      public int gxTpr_Contagemresultado_sistemaareacod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaareacod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaareacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemrResultado_SistemaSigla" )]
      [  XmlElement( ElementName = "ContagemrResultado_SistemaSigla"   )]
      public String gxTpr_Contagemrresultado_sistemasigla
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaCoord" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaCoord"   )]
      public String gxTpr_Contagemresultado_sistemacoord
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaAtivo" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaAtivo"   )]
      public bool gxTpr_Contagemresultado_sistemaativo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaativo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaativo = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaGestor" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaGestor"   )]
      public String gxTpr_Contagemresultado_sistemagestor
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo_Codigo" )]
      [  XmlElement( ElementName = "Modulo_Codigo"   )]
      public int gxTpr_Modulo_codigo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Modulo_codigo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Modulo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo_Nome" )]
      [  XmlElement( ElementName = "Modulo_Nome"   )]
      public String gxTpr_Modulo_nome
      {
         get {
            return gxTv_SdtSDT_WP_OS_Modulo_nome ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Modulo_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo_Sigla" )]
      [  XmlElement( ElementName = "Modulo_Sigla"   )]
      public String gxTpr_Modulo_sigla
      {
         get {
            return gxTv_SdtSDT_WP_OS_Modulo_sigla ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Modulo_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnCod" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnCod"   )]
      public int gxTpr_Contagemresultado_naocnfdmncod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmncod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmncod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnNom" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnNom"   )]
      public String gxTpr_Contagemresultado_naocnfdmnnom
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnGls" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnGls"   )]
      public bool gxTpr_Contagemresultado_naocnfdmngls
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmngls ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmngls = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn"   )]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_StatusUltCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusUltCnt"   )]
      public short gxTpr_Contagemresultado_statusultcnt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_statusultcnt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_statusultcnt = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmnVnc" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmnVnc"   )]
      public String gxTpr_Contagemresultado_statusdmnvnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataUltCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_DataUltCnt"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataultcnt_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataultcnt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_HoraUltCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_HoraUltCnt"   )]
      public String gxTpr_Contagemresultado_horaultcnt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntComNaoCnf" )]
      [  XmlElement( ElementName = "ContagemResultado_CntComNaoCnf"   )]
      public int gxTpr_Contagemresultado_cntcomnaocnf
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntcomnaocnf ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntcomnaocnf = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFFinal" )]
      [  XmlElement( ElementName = "ContagemResultado_PFFinal"   )]
      public double gxTpr_Contagemresultado_pffinal_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pffinal
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFM" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFM"   )]
      public int gxTpr_Contagemresultado_contadorfm
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfm ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfm = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFMUltima" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFMUltima"   )]
      public double gxTpr_Contagemresultado_pfbfmultima_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfbfmultima
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFMUltima" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFMUltima"   )]
      public double gxTpr_Contagemresultado_pflfmultima_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfmultima
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFSUltima" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFSUltima"   )]
      public double gxTpr_Contagemresultado_pfbfsultima_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfbfsultima
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CstUntUltima" )]
      [  XmlElement( ElementName = "ContagemResultado_CstUntUltima"   )]
      public double gxTpr_Contagemresultado_cstuntultima_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cstuntultima
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFSUltima" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFSUltima"   )]
      public double gxTpr_Contagemresultado_pflfsultima_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfsultima
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DeflatorCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_DeflatorCnt"   )]
      public double gxTpr_Contagemresultado_deflatorcnt_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_deflatorcnt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Servico" )]
      [  XmlElement( ElementName = "ContagemResultado_Servico"   )]
      public int gxTpr_Contagemresultado_servico
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servico ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servico = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod"   )]
      public int gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvAls" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvAls"   )]
      public String gxTpr_Contagemresultado_cntsrvals
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoGrupo" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoGrupo"   )]
      public int gxTpr_Contagemresultado_servicogrupo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servicogrupo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servicogrupo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSigla" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSigla"   )]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoTela" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoTela"   )]
      public String gxTpr_Contagemresultado_servicotela
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoAtivo" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoAtivo"   )]
      public bool gxTpr_Contagemresultado_servicoativo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servicoativo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servicoativo = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoNaoRqrAtr" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoNaoRqrAtr"   )]
      public bool gxTpr_Contagemresultado_serviconaorqratr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_serviconaorqratr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_serviconaorqratr = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSS" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSS"   )]
      public int gxTpr_Contagemresultado_servicoss
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servicoss ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servicoss = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSSSgl" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSSSgl"   )]
      public String gxTpr_Contagemresultado_servicosssgl
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SrvLnhNegCod" )]
      [  XmlElement( ElementName = "ContagemResultado_SrvLnhNegCod"   )]
      public int gxTpr_Contagemresultado_srvlnhnegcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_srvlnhnegcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_srvlnhnegcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_EhValidacao" )]
      [  XmlElement( ElementName = "ContagemResultado_EhValidacao"   )]
      public bool gxTpr_Contagemresultado_ehvalidacao
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_ehvalidacao ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_ehvalidacao = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_EsforcoTotal" )]
      [  XmlElement( ElementName = "ContagemResultado_EsforcoTotal"   )]
      public String gxTpr_Contagemresultado_esforcototal
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_EsforcoSoma" )]
      [  XmlElement( ElementName = "ContagemResultado_EsforcoSoma"   )]
      public int gxTpr_Contagemresultado_esforcosoma
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_esforcosoma ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_esforcosoma = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Observacao" )]
      [  XmlElement( ElementName = "ContagemResultado_Observacao"   )]
      public String gxTpr_Contagemresultado_observacao
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_observacao ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_observacao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Evidencia" )]
      [  XmlElement( ElementName = "ContagemResultado_Evidencia"   )]
      public String gxTpr_Contagemresultado_evidencia
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataCadastro" )]
      [  XmlElement( ElementName = "ContagemResultado_DataCadastro"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datacadastro_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datacadastro
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Owner" )]
      [  XmlElement( ElementName = "ContagemResultado_Owner"   )]
      public int gxTpr_Contagemresultado_owner
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_owner ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_owner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Owner_Identificao" )]
      [  XmlElement( ElementName = "ContagemResultado_Owner_Identificao"   )]
      public String gxTpr_Contagemresultado_owner_identificao
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratanteDoOwner" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratanteDoOwner"   )]
      public int gxTpr_Contagemresultado_contratantedoowner
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoowner ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoowner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaDoOwner" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaDoOwner"   )]
      public int gxTpr_Contagemresultado_contratadadoowner
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoowner ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoowner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Responsavel" )]
      [  XmlElement( ElementName = "ContagemResultado_Responsavel"   )]
      public int gxTpr_Contagemresultado_responsavel
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_responsavel ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_responsavel = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ResponsavelPessCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ResponsavelPessCod"   )]
      public int gxTpr_Contagemresultado_responsavelpesscod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpesscod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpesscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ResponsavelPessNome" )]
      [  XmlElement( ElementName = "ContagemResultado_ResponsavelPessNome"   )]
      public String gxTpr_Contagemresultado_responsavelpessnome
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaDoResponsavel" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaDoResponsavel"   )]
      public int gxTpr_Contagemresultado_contratadadoresponsavel
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoresponsavel ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoresponsavel = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratanteDoResponsavel" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratanteDoResponsavel"   )]
      public int gxTpr_Contagemresultado_contratantedoresponsavel
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoresponsavel ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoresponsavel = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ValorPF" )]
      [  XmlElement( ElementName = "ContagemResultado_ValorPF"   )]
      public double gxTpr_Contagemresultado_valorpf_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_valorpf
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Custo" )]
      [  XmlElement( ElementName = "ContagemResultado_Custo"   )]
      public double gxTpr_Contagemresultado_custo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_custo) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_custo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_custo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_custo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_custo = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada"   )]
      public int gxTpr_Contagemresultado_osvinculada
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_osvinculada ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_osvinculada = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DmnVinculada" )]
      [  XmlElement( ElementName = "ContagemResultado_DmnVinculada"   )]
      public String gxTpr_Contagemresultado_dmnvinculada
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DmnVinculadaRef" )]
      [  XmlElement( ElementName = "ContagemResultado_DmnVinculadaRef"   )]
      public String gxTpr_Contagemresultado_dmnvinculadaref
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFSImp" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFSImp"   )]
      public double gxTpr_Contagemresultado_pfbfsimp_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfbfsimp
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFSImp" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFSImp"   )]
      public double gxTpr_Contagemresultado_pflfsimp_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfsimp
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PBFinal" )]
      [  XmlElement( ElementName = "ContagemResultado_PBFinal"   )]
      public double gxTpr_Contagemresultado_pbfinal_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pbfinal
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PLFinal" )]
      [  XmlElement( ElementName = "ContagemResultado_PLFinal"   )]
      public double gxTpr_Contagemresultado_plfinal_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_plfinal
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_LiqLogCod" )]
      [  XmlElement( ElementName = "ContagemResultado_LiqLogCod"   )]
      public int gxTpr_Contagemresultado_liqlogcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_liqlogcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_liqlogcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Liquidada" )]
      [  XmlElement( ElementName = "ContagemResultado_Liquidada"   )]
      public bool gxTpr_Contagemresultado_liquidada
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_liquidada ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_liquidada = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoLiqLog_Data" )]
      [  XmlElement( ElementName = "ContagemResultadoLiqLog_Data"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoliqlog_data_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoliqlog_data
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_FncUsrCod" )]
      [  XmlElement( ElementName = "ContagemResultado_FncUsrCod"   )]
      public int gxTpr_Contagemresultado_fncusrcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_fncusrcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_fncusrcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Agrupador" )]
      [  XmlElement( ElementName = "ContagemResultado_Agrupador"   )]
      public String gxTpr_Contagemresultado_agrupador
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvPrrCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvPrrCod"   )]
      public int gxTpr_Contagemresultado_cntsrvprrcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvPrrNome" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvPrrNome"   )]
      public String gxTpr_Contagemresultado_cntsrvprrnome
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvPrrPrz" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvPrrPrz"   )]
      public double gxTpr_Contagemresultado_cntsrvprrprz_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntsrvprrprz
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvPrrCst" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvPrrCst"   )]
      public double gxTpr_Contagemresultado_cntsrvprrcst_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntsrvprrcst
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_TemDpnHmlg" )]
      [  XmlElement( ElementName = "ContagemResultado_TemDpnHmlg"   )]
      public bool gxTpr_Contagemresultado_temdpnhmlg
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_temdpnhmlg ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_temdpnhmlg = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_TemPndHmlg" )]
      [  XmlElement( ElementName = "ContagemResultado_TemPndHmlg"   )]
      public bool gxTpr_Contagemresultado_tempndhmlg
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_tempndhmlg ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_tempndhmlg = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_TmpEstAnl" )]
      [  XmlElement( ElementName = "ContagemResultado_TmpEstAnl"   )]
      public int gxTpr_Contagemresultado_tmpestanl
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestanl ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestanl = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_TmpEstExc" )]
      [  XmlElement( ElementName = "ContagemResultado_TmpEstExc"   )]
      public int gxTpr_Contagemresultado_tmpestexc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestexc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestexc = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_TmpEstCrr" )]
      [  XmlElement( ElementName = "ContagemResultado_TmpEstCrr"   )]
      public int gxTpr_Contagemresultado_tmpestcrr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestcrr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestcrr = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_InicioAnl" )]
      [  XmlElement( ElementName = "ContagemResultado_InicioAnl"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_inicioanl_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_inicioanl
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_FimAnl" )]
      [  XmlElement( ElementName = "ContagemResultado_FimAnl"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_fimanl_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_fimanl
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_InicioExc" )]
      [  XmlElement( ElementName = "ContagemResultado_InicioExc"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_inicioexc_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_inicioexc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_FimExc" )]
      [  XmlElement( ElementName = "ContagemResultado_FimExc"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_fimexc_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_fimexc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_InicioCrr" )]
      [  XmlElement( ElementName = "ContagemResultado_InicioCrr"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_iniciocrr_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_iniciocrr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_FimCrr" )]
      [  XmlElement( ElementName = "ContagemResultado_FimCrr"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_fimcrr_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_fimcrr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Evento" )]
      [  XmlElement( ElementName = "ContagemResultado_Evento"   )]
      public short gxTpr_Contagemresultado_evento
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_evento ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_evento = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ProjetoCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ProjetoCod"   )]
      public int gxTpr_Contagemresultado_projetocod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_projetocod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_projetocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvTpVnc" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvTpVnc"   )]
      public String gxTpr_Contagemresultado_cntsrvtpvnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvFtrm" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvFtrm"   )]
      public String gxTpr_Contagemresultado_cntsrvftrm
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvPrc" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvPrc"   )]
      public double gxTpr_Contagemresultado_cntsrvprc_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntsrvprc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvVlrUndCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvVlrUndCnt"   )]
      public double gxTpr_Contagemresultado_cntsrvvlrundcnt_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntsrvvlrundcnt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvSttPgmFnc" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvSttPgmFnc"   )]
      public String gxTpr_Contagemresultado_cntsrvsttpgmfnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvIndDvr" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvIndDvr"   )]
      public double gxTpr_Contagemresultado_cntsrvinddvr_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntsrvinddvr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvQtdUntCns" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvQtdUntCns"   )]
      public double gxTpr_Contagemresultado_cntsrvqtduntcns_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntsrvqtduntcns
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvUndCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvUndCnt"   )]
      public int gxTpr_Contagemresultado_cntsrvundcnt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcnt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcnt = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvUndCntSgl" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvUndCntSgl"   )]
      public String gxTpr_Contagemresultado_cntsrvundcntsgl
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvMmn" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvMmn"   )]
      public String gxTpr_Contagemresultado_cntsrvmmn
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzAnl" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzAnl"   )]
      public short gxTpr_Contagemresultado_przanl
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_przanl ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_przanl = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzExc" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzExc"   )]
      public short gxTpr_Contagemresultado_przexc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_przexc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_przexc = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzCrr" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzCrr"   )]
      public short gxTpr_Contagemresultado_przcrr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_przcrr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_przcrr = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzRsp" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzRsp"   )]
      public short gxTpr_Contagemresultado_przrsp
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_przrsp ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_przrsp = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzGrt" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzGrt"   )]
      public short gxTpr_Contagemresultado_przgrt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_przgrt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_przgrt = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzTp" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzTp"   )]
      public String gxTpr_Contagemresultado_prztp
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_prztp ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_prztp = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzTpDias" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzTpDias"   )]
      public String gxTpr_Contagemresultado_prztpdias
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntCod"   )]
      public int gxTpr_Contagemresultado_cntcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntNum" )]
      [  XmlElement( ElementName = "ContagemResultado_CntNum"   )]
      public String gxTpr_Contagemresultado_cntnum
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntPrpCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntPrpCod"   )]
      public int gxTpr_Contagemresultado_cntprpcod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntprpcod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntprpcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntPrpPesCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntPrpPesCod"   )]
      public int gxTpr_Contagemresultado_cntprppescod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppescod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppescod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntPrpPesNom" )]
      [  XmlElement( ElementName = "ContagemResultado_CntPrpPesNom"   )]
      public String gxTpr_Contagemresultado_cntprppesnom
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntIndDvr" )]
      [  XmlElement( ElementName = "ContagemResultado_CntIndDvr"   )]
      public double gxTpr_Contagemresultado_cntinddvr_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntinddvr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntClcDvr" )]
      [  XmlElement( ElementName = "ContagemResultado_CntClcDvr"   )]
      public String gxTpr_Contagemresultado_cntclcdvr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntVlrUndCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_CntVlrUndCnt"   )]
      public double gxTpr_Contagemresultado_cntvlrundcnt_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntvlrundcnt
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntPrdFtrCada" )]
      [  XmlElement( ElementName = "ContagemResultado_CntPrdFtrCada"   )]
      public String gxTpr_Contagemresultado_cntprdftrcada
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntPrdFtrIni" )]
      [  XmlElement( ElementName = "ContagemResultado_CntPrdFtrIni"   )]
      public short gxTpr_Contagemresultado_cntprdftrini
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrini ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrini = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntPrdFtrFim" )]
      [  XmlElement( ElementName = "ContagemResultado_CntPrdFtrFim"   )]
      public short gxTpr_Contagemresultado_cntprdftrfim
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrfim ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrfim = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntLmtFtr" )]
      [  XmlElement( ElementName = "ContagemResultado_CntLmtFtr"   )]
      public double gxTpr_Contagemresultado_cntlmtftr_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cntlmtftr
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DatVgnInc" )]
      [  XmlElement( ElementName = "ContagemResultado_DatVgnInc"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datvgninc_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datvgninc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DatIncTA" )]
      [  XmlElement( ElementName = "ContagemResultado_DatIncTA"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datincta_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_datincta == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_datincta).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_datincta = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_datincta = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datincta
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_datincta ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_datincta = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoNome" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoNome"   )]
      public String gxTpr_Contagemresultado_serviconome
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoTpHrrq" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoTpHrrq"   )]
      public short gxTpr_Contagemresultado_servicotphrrq
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_servicotphrrq ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_servicotphrrq = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CodSrvVnc" )]
      [  XmlElement( ElementName = "ContagemResultado_CodSrvVnc"   )]
      public int gxTpr_Contagemresultado_codsrvvnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvvnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvvnc = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SiglaSrvVnc" )]
      [  XmlElement( ElementName = "ContagemResultado_SiglaSrvVnc"   )]
      public String gxTpr_Contagemresultado_siglasrvvnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CodSrvSSVnc" )]
      [  XmlElement( ElementName = "ContagemResultado_CodSrvSSVnc"   )]
      public int gxTpr_Contagemresultado_codsrvssvnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvssvnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvssvnc = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvVncCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvVncCod"   )]
      public int gxTpr_Contagemresultado_cntsrvvnccod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvnccod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvnccod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntVncCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntVncCod"   )]
      public int gxTpr_Contagemresultado_cntvnccod
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_cntvnccod ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_cntvnccod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_GlsIndValor" )]
      [  XmlElement( ElementName = "ContagemResultado_GlsIndValor"   )]
      public double gxTpr_Contagemresultado_glsindvalor_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_glsindvalor
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_TipoRegistro" )]
      [  XmlElement( ElementName = "ContagemResultado_TipoRegistro"   )]
      public short gxTpr_Contagemresultado_tiporegistro
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_tiporegistro ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_tiporegistro = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_UOOwner" )]
      [  XmlElement( ElementName = "ContagemResultado_UOOwner"   )]
      public int gxTpr_Contagemresultado_uoowner
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_uoowner ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_uoowner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Referencia" )]
      [  XmlElement( ElementName = "ContagemResultado_Referencia"   )]
      public String gxTpr_Contagemresultado_referencia
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_referencia ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_referencia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Restricoes" )]
      [  XmlElement( ElementName = "ContagemResultado_Restricoes"   )]
      public String gxTpr_Contagemresultado_restricoes
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrioridadePrevista" )]
      [  XmlElement( ElementName = "ContagemResultado_PrioridadePrevista"   )]
      public String gxTpr_Contagemresultado_prioridadeprevista
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrzInc" )]
      [  XmlElement( ElementName = "ContagemResultado_PrzInc"   )]
      public short gxTpr_Contagemresultado_przinc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_przinc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_przinc = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_TemProposta" )]
      [  XmlElement( ElementName = "ContagemResultado_TemProposta"   )]
      public bool gxTpr_Contagemresultado_temproposta
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_temproposta ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_temproposta = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Combinada" )]
      [  XmlElement( ElementName = "ContagemResultado_Combinada"   )]
      public bool gxTpr_Contagemresultado_combinada
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_combinada ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_combinada = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Entrega" )]
      [  XmlElement( ElementName = "ContagemResultado_Entrega"   )]
      public short gxTpr_Contagemresultado_entrega
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_entrega ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_entrega = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_SemCusto" )]
      [  XmlElement( ElementName = "ContagemResultado_SemCusto"   )]
      public bool gxTpr_Contagemresultado_semcusto
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_semcusto ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_semcusto = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_VlrCnc" )]
      [  XmlElement( ElementName = "ContagemResultado_VlrCnc"   )]
      public double gxTpr_Contagemresultado_vlrcnc_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_vlrcnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFCnc" )]
      [  XmlElement( ElementName = "ContagemResultado_PFCnc"   )]
      public double gxTpr_Contagemresultado_pfcnc_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfcnc
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataPrvPgm" )]
      [  XmlElement( ElementName = "ContagemResultado_DataPrvPgm"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataprvpgm_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataprvpgm
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_QuantidadeSolicitada" )]
      [  XmlElement( ElementName = "ContagemResultado_QuantidadeSolicitada"   )]
      public double gxTpr_Contagemresultado_quantidadesolicitada_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_quantidadesolicitada
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_RequisitadoPorContratante" )]
      [  XmlElement( ElementName = "ContagemResultado_RequisitadoPorContratante"   )]
      public bool gxTpr_Contagemresultado_requisitadoporcontratante
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_requisitadoporcontratante ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_requisitadoporcontratante = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Requisitante" )]
      [  XmlElement( ElementName = "ContagemResultado_Requisitante"   )]
      public String gxTpr_Contagemresultado_requisitante
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_CargoUONom" )]
      [  XmlElement( ElementName = "Usuario_CargoUONom"   )]
      public String gxTpr_Usuario_cargouonom
      {
         get {
            return gxTv_SdtSDT_WP_OS_Usuario_cargouonom ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Usuario_cargouonom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntNome" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntNome"   )]
      public String gxTpr_Contratoservicos_undcntnome
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_QntUntCns" )]
      [  XmlElement( ElementName = "ContratoServicos_QntUntCns"   )]
      public double gxTpr_Contratoservicos_qntuntcns_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns) ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicos_qntuntcns
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosPrazo_Complexidade" )]
      [  XmlElement( ElementName = "ContratoServicosPrazo_Complexidade"   )]
      public short gxTpr_Contratoservicosprazo_complexidade
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contratoservicosprazo_complexidade ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contratoservicosprazo_complexidade = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Codigo" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Codigo"   )]
      public int gxTpr_Contratoservicosprioridade_codigo
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contratoservicosprioridade_codigo ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contratoservicosprioridade_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_LocalExec" )]
      [  XmlElement( ElementName = "ContratoServicos_LocalExec"   )]
      public String gxTpr_Contratoservicos_localexec
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contratoservicos_localexec ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contratoservicos_localexec = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoAnalise" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoAnalise"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_prazoanalise_Nullable
      {
         get {
            if ( gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise = DateTime.MinValue;
            else
               gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_prazoanalise
      {
         get {
            return gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise ;
         }

         set {
            gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise = (DateTime)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab = "S";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj = "";
         gxTv_SdtSDT_WP_OS_Calculodivergencia = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_demanda = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_descricao = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_link = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom = "";
         gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor = "";
         gxTv_SdtSDT_WP_OS_Modulo_nome = "";
         gxTv_SdtSDT_WP_OS_Modulo_sigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_observacao = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro = DateTimeUtil.ServerNow( (IGxContext)(context), "DEFAULT");
         gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref = "";
         gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm = "B";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prztp = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr = "B";
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_datincta = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_referencia = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante = "";
         gxTv_SdtSDT_WP_OS_Usuario_cargouonom = "";
         gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome = "";
         gxTv_SdtSDT_WP_OS_Contratoservicos_localexec = "";
         gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise = DateTime.MinValue;
         gxTv_SdtSDT_WP_OS_Contagemresultado_ss = 0;
         gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaativo = true;
         gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmngls = false;
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicoativo = true;
         gxTv_SdtSDT_WP_OS_Contagemresultado_ehvalidacao = false;
         gxTv_SdtSDT_WP_OS_Contagemresultado_evento = 1;
         gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr = (decimal)(10);
         gxTv_SdtSDT_WP_OS_Contagemresultado_servicotphrrq = 1;
         gxTv_SdtSDT_WP_OS_Contagemresultado_tiporegistro = 1;
         gxTv_SdtSDT_WP_OS_Contagemresultado_przinc = 1;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_prazoinicialdias ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_prazomaisdias ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_erros ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_statusultcnt ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_evento ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_przanl ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_przexc ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_przcrr ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_przrsp ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_przgrt ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrini ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrfim ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_servicotphrrq ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_tiporegistro ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_przinc ;
      protected short gxTv_SdtSDT_WP_OS_Contagemresultado_entrega ;
      protected short gxTv_SdtSDT_WP_OS_Contratoservicosprazo_complexidade ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_codigo ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoacod ;
      protected int gxTv_SdtSDT_WP_OS_Contratada_areatrabalhocod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempescod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemareacod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfscod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_crfspessoacod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_ss ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaareacod ;
      protected int gxTv_SdtSDT_WP_OS_Modulo_codigo ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmncod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntcomnaocnf ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfm ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_servico ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_servicogrupo ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_servicoss ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_srvlnhnegcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_esforcosoma ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_owner ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoowner ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoowner ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_responsavel ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpesscod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratadadoresponsavel ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_contratantedoresponsavel ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_osvinculada ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_liqlogcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_fncusrcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestanl ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestexc ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_tmpestcrr ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_projetocod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcnt ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntprpcod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppescod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvvnc ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_codsrvssvnc ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvnccod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_cntvnccod ;
      protected int gxTv_SdtSDT_WP_OS_Contagemresultado_uoowner ;
      protected int gxTv_SdtSDT_WP_OS_Contratoservicosprioridade_codigo ;
      protected long gxTv_SdtSDT_WP_OS_Contagemresultado_demanda_order ;
      protected long gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm_order ;
      protected decimal gxTv_SdtSDT_WP_OS_Indicedivergencia ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pffinal ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfmultima ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pflfmultima ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsultima ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cstuntultima ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsultima ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_deflatorcnt ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_valorpf ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_custo ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pfbfsimp ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pflfsimp ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pbfinal ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_plfinal ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrprz ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrcst ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprc ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvvlrundcnt ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvinddvr ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvqtduntcns ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntinddvr ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntvlrundcnt ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_cntlmtftr ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_glsindvalor ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_vlrcnc ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_pfcnc ;
      protected decimal gxTv_SdtSDT_WP_OS_Contagemresultado_quantidadesolicitada ;
      protected decimal gxTv_SdtSDT_WP_OS_Contratoservicos_qntuntcns ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadapessoanom ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadasigla ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadatipofab ;
      protected String gxTv_SdtSDT_WP_OS_Calculodivergencia ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemsigla ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigempesnom ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contadorfsnom ;
      protected String gxTv_SdtSDT_WP_OS_Contagemrresultado_sistemasigla ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_sistemagestor ;
      protected String gxTv_SdtSDT_WP_OS_Modulo_nome ;
      protected String gxTv_SdtSDT_WP_OS_Modulo_sigla ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmnnom ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmn ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_statusdmnvnc ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_horaultcnt ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvals ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_servicosigla ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_servicotela ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_servicosssgl ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_esforcototal ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_responsavelpessnome ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_agrupador ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvprrnome ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvtpvnc ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvsttpgmfnc ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvundcntsgl ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvmmn ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_prztp ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_prztpdias ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntnum ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntprppesnom ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntclcdvr ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntprdftrcada ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_serviconome ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_siglasrvvnc ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_requisitante ;
      protected String gxTv_SdtSDT_WP_OS_Usuario_cargouonom ;
      protected String gxTv_SdtSDT_WP_OS_Contratoservicos_undcntnome ;
      protected String gxTv_SdtSDT_WP_OS_Contratoservicos_localexec ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_horaentrega ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_dataprevista ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_dataexecucao ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_dataentregareal ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_datahomologacao ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_datacadastro ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultadoliqlog_data ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_inicioanl ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_fimanl ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_inicioexc ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_fimexc ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_iniciocrr ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_fimcrr ;
      protected DateTime datetime_STZ ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_datadmn ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_datainicio ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_dataentrega ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_dataultcnt ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_datvgninc ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_datincta ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_dataprvpgm ;
      protected DateTime gxTv_SdtSDT_WP_OS_Contagemresultado_prazoanalise ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_contratadaorigemusasistema ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_osmanual ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_sistemaativo ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_naocnfdmngls ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_servicoativo ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_serviconaorqratr ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_ehvalidacao ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_liquidada ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_temdpnhmlg ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_tempndhmlg ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_temproposta ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_combinada ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_semcusto ;
      protected bool gxTv_SdtSDT_WP_OS_Contagemresultado_requisitadoporcontratante ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_link ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_observacao ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_evidencia ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo_gxi ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadacnpj ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_demanda ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_dmnsrvprst ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_demandafm ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_osfsosfm ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_descricao ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_sistemanom ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_sistemacoord ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_owner_identificao ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculada ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_dmnvinculadaref ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_cntsrvftrm ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_referencia ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_restricoes ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_prioridadeprevista ;
      protected String gxTv_SdtSDT_WP_OS_Contagemresultado_contratadalogo ;
   }

   [DataContract(Name = @"SDT_WP_OS", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_WP_OS_RESTInterface : GxGenericCollectionItem<SdtSDT_WP_OS>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WP_OS_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WP_OS_RESTInterface( SdtSDT_WP_OS psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DataDmn" , Order = 1 )]
      public String gxTpr_Contagemresultado_datadmn
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datadmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datadmn = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DataInicio" , Order = 2 )]
      public String gxTpr_Contagemresultado_datainicio
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datainicio) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datainicio = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DataEntrega" , Order = 3 )]
      public String gxTpr_Contagemresultado_dataentrega
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_dataentrega) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataentrega = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_HoraEntrega" , Order = 4 )]
      public String gxTpr_Contagemresultado_horaentrega
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_horaentrega) ;
         }

         set {
            GXt_dtime1 = DateTimeUtil.ResetDate(DateTimeUtil.CToT2( (String)(value)));
            sdt.gxTpr_Contagemresultado_horaentrega = GXt_dtime1;
         }

      }

      [DataMember( Name = "ContagemResultado_DataPrevista" , Order = 5 )]
      public String gxTpr_Contagemresultado_dataprevista
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_dataprevista) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataprevista = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DataExecucao" , Order = 6 )]
      public String gxTpr_Contagemresultado_dataexecucao
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_dataexecucao) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataexecucao = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DataEntregaReal" , Order = 7 )]
      public String gxTpr_Contagemresultado_dataentregareal
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_dataentregareal) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataentregareal = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DataHomologacao" , Order = 8 )]
      public String gxTpr_Contagemresultado_datahomologacao
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_datahomologacao) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datahomologacao = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_PrazoInicialDias" , Order = 9 )]
      public Nullable<short> gxTpr_Contagemresultado_prazoinicialdias
      {
         get {
            return sdt.gxTpr_Contagemresultado_prazoinicialdias ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prazoinicialdias = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PrazoMaisDias" , Order = 10 )]
      public Nullable<short> gxTpr_Contagemresultado_prazomaisdias
      {
         get {
            return sdt.gxTpr_Contagemresultado_prazomaisdias ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prazomaisdias = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaCod" , Order = 11 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaPessoaCod" , Order = 12 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadapessoacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadapessoacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadapessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaPessoaNom" , Order = 13 )]
      public String gxTpr_Contagemresultado_contratadapessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadapessoanom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadapessoanom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaSigla" , Order = 14 )]
      public String gxTpr_Contagemresultado_contratadasigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadasigla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadasigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaTipoFab" , Order = 15 )]
      public String gxTpr_Contagemresultado_contratadatipofab
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadatipofab) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadatipofab = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaLogo" , Order = 16 )]
      [GxUpload()]
      public String gxTpr_Contagemresultado_contratadalogo
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadalogo)) ? PathUtil.RelativePath( sdt.gxTpr_Contagemresultado_contratadalogo) : StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadalogo_gxi)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadalogo = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaCNPJ" , Order = 18 )]
      public String gxTpr_Contagemresultado_contratadacnpj
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadacnpj ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadacnpj = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_AreaTrabalhoCod" , Order = 19 )]
      public Nullable<int> gxTpr_Contratada_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contratada_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contratada_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "IndiceDivergencia" , Order = 20 )]
      public Nullable<decimal> gxTpr_Indicedivergencia
      {
         get {
            return sdt.gxTpr_Indicedivergencia ;
         }

         set {
            sdt.gxTpr_Indicedivergencia = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "CalculoDivergencia" , Order = 21 )]
      public String gxTpr_Calculodivergencia
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Calculodivergencia) ;
         }

         set {
            sdt.gxTpr_Calculodivergencia = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemCod" , Order = 22 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigemcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemSigla" , Order = 23 )]
      public String gxTpr_Contagemresultado_contratadaorigemsigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadaorigemsigla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemsigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemPesCod" , Order = 24 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadaorigempescod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigempescod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigempescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemPesNom" , Order = 25 )]
      public String gxTpr_Contagemresultado_contratadaorigempesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadaorigempesnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigempesnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemAreaCod" , Order = 26 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadaorigemareacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigemareacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemareacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemUsaSistema" , Order = 27 )]
      public bool gxTpr_Contagemresultado_contratadaorigemusasistema
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigemusasistema ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemusasistema = value;
         }

      }

      [DataMember( Name = "ContagemResultado_ContadorFSCod" , Order = 28 )]
      public Nullable<int> gxTpr_Contagemresultado_contadorfscod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contadorfscod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contadorfscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CrFSPessoaCod" , Order = 29 )]
      public Nullable<int> gxTpr_Contagemresultado_crfspessoacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_crfspessoacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_crfspessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContadorFSNom" , Order = 30 )]
      public String gxTpr_Contagemresultado_contadorfsnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contadorfsnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contadorfsnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Demanda" , Order = 31 )]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return sdt.gxTpr_Contagemresultado_demanda ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demanda = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DmnSrvPrst" , Order = 32 )]
      public String gxTpr_Contagemresultado_dmnsrvprst
      {
         get {
            return sdt.gxTpr_Contagemresultado_dmnsrvprst ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dmnsrvprst = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Demanda_ORDER" , Order = 33 )]
      public String gxTpr_Contagemresultado_demanda_order
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_demanda_order), 18, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demanda_order = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_Erros" , Order = 34 )]
      public Nullable<short> gxTpr_Contagemresultado_erros
      {
         get {
            return sdt.gxTpr_Contagemresultado_erros ;
         }

         set {
            sdt.gxTpr_Contagemresultado_erros = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DemandaFM" , Order = 35 )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return sdt.gxTpr_Contagemresultado_demandafm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demandafm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DemandaFM_ORDER" , Order = 36 )]
      public String gxTpr_Contagemresultado_demandafm_order
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_demandafm_order), 18, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demandafm_order = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_SS" , Order = 37 )]
      public String gxTpr_Contagemresultado_ss
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_ss), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_ss = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_OsFsOsFm" , Order = 38 )]
      public String gxTpr_Contagemresultado_osfsosfm
      {
         get {
            return sdt.gxTpr_Contagemresultado_osfsosfm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osfsosfm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_OSManual" , Order = 39 )]
      public bool gxTpr_Contagemresultado_osmanual
      {
         get {
            return sdt.gxTpr_Contagemresultado_osmanual ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osmanual = value;
         }

      }

      [DataMember( Name = "ContagemResultado_Descricao" , Order = 40 )]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return sdt.gxTpr_Contagemresultado_descricao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Link" , Order = 41 )]
      public String gxTpr_Contagemresultado_link
      {
         get {
            return sdt.gxTpr_Contagemresultado_link ;
         }

         set {
            sdt.gxTpr_Contagemresultado_link = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_SistemaCod" , Order = 42 )]
      public Nullable<int> gxTpr_Contagemresultado_sistemacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_sistemacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_SistemaNom" , Order = 43 )]
      public String gxTpr_Contagemresultado_sistemanom
      {
         get {
            return sdt.gxTpr_Contagemresultado_sistemanom ;
         }

         set {
            sdt.gxTpr_Contagemresultado_sistemanom = (String)(value);
         }

      }

      [DataMember( Name = "Contagemresultado_SistemaAreaCod" , Order = 44 )]
      public Nullable<int> gxTpr_Contagemresultado_sistemaareacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_sistemaareacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_sistemaareacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemrResultado_SistemaSigla" , Order = 45 )]
      public String gxTpr_Contagemrresultado_sistemasigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemrresultado_sistemasigla) ;
         }

         set {
            sdt.gxTpr_Contagemrresultado_sistemasigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_SistemaCoord" , Order = 46 )]
      public String gxTpr_Contagemresultado_sistemacoord
      {
         get {
            return sdt.gxTpr_Contagemresultado_sistemacoord ;
         }

         set {
            sdt.gxTpr_Contagemresultado_sistemacoord = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_SistemaAtivo" , Order = 47 )]
      public bool gxTpr_Contagemresultado_sistemaativo
      {
         get {
            return sdt.gxTpr_Contagemresultado_sistemaativo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_sistemaativo = value;
         }

      }

      [DataMember( Name = "ContagemResultado_SistemaGestor" , Order = 48 )]
      public String gxTpr_Contagemresultado_sistemagestor
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_sistemagestor) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_sistemagestor = (String)(value);
         }

      }

      [DataMember( Name = "Modulo_Codigo" , Order = 49 )]
      public Nullable<int> gxTpr_Modulo_codigo
      {
         get {
            return sdt.gxTpr_Modulo_codigo ;
         }

         set {
            sdt.gxTpr_Modulo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Modulo_Nome" , Order = 50 )]
      public String gxTpr_Modulo_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Modulo_nome) ;
         }

         set {
            sdt.gxTpr_Modulo_nome = (String)(value);
         }

      }

      [DataMember( Name = "Modulo_Sigla" , Order = 51 )]
      public String gxTpr_Modulo_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Modulo_sigla) ;
         }

         set {
            sdt.gxTpr_Modulo_sigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_NaoCnfDmnCod" , Order = 52 )]
      public Nullable<int> gxTpr_Contagemresultado_naocnfdmncod
      {
         get {
            return sdt.gxTpr_Contagemresultado_naocnfdmncod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_naocnfdmncod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_NaoCnfDmnNom" , Order = 53 )]
      public String gxTpr_Contagemresultado_naocnfdmnnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_naocnfdmnnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_naocnfdmnnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_NaoCnfDmnGls" , Order = 54 )]
      public bool gxTpr_Contagemresultado_naocnfdmngls
      {
         get {
            return sdt.gxTpr_Contagemresultado_naocnfdmngls ;
         }

         set {
            sdt.gxTpr_Contagemresultado_naocnfdmngls = value;
         }

      }

      [DataMember( Name = "ContagemResultado_StatusDmn" , Order = 55 )]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_statusdmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusdmn = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusUltCnt" , Order = 56 )]
      public Nullable<short> gxTpr_Contagemresultado_statusultcnt
      {
         get {
            return sdt.gxTpr_Contagemresultado_statusultcnt ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusultcnt = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusDmnVnc" , Order = 57 )]
      public String gxTpr_Contagemresultado_statusdmnvnc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_statusdmnvnc) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusdmnvnc = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DataUltCnt" , Order = 58 )]
      public String gxTpr_Contagemresultado_dataultcnt
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_dataultcnt) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataultcnt = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_HoraUltCnt" , Order = 59 )]
      public String gxTpr_Contagemresultado_horaultcnt
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_horaultcnt) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_horaultcnt = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntComNaoCnf" , Order = 60 )]
      public String gxTpr_Contagemresultado_cntcomnaocnf
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_cntcomnaocnf), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntcomnaocnf = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_PFFinal" , Order = 61 )]
      public String gxTpr_Contagemresultado_pffinal
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pffinal, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pffinal = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_ContadorFM" , Order = 62 )]
      public Nullable<int> gxTpr_Contagemresultado_contadorfm
      {
         get {
            return sdt.gxTpr_Contagemresultado_contadorfm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contadorfm = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PFBFMUltima" , Order = 63 )]
      public String gxTpr_Contagemresultado_pfbfmultima
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pfbfmultima, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pfbfmultima = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFMUltima" , Order = 64 )]
      public String gxTpr_Contagemresultado_pflfmultima
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfmultima, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfmultima = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFBFSUltima" , Order = 65 )]
      public String gxTpr_Contagemresultado_pfbfsultima
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pfbfsultima, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pfbfsultima = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_CstUntUltima" , Order = 66 )]
      public String gxTpr_Contagemresultado_cstuntultima
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_cstuntultima, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cstuntultima = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFSUltima" , Order = 67 )]
      public String gxTpr_Contagemresultado_pflfsultima
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfsultima, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfsultima = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_DeflatorCnt" , Order = 68 )]
      public Nullable<decimal> gxTpr_Contagemresultado_deflatorcnt
      {
         get {
            return sdt.gxTpr_Contagemresultado_deflatorcnt ;
         }

         set {
            sdt.gxTpr_Contagemresultado_deflatorcnt = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Servico" , Order = 69 )]
      public Nullable<int> gxTpr_Contagemresultado_servico
      {
         get {
            return sdt.gxTpr_Contagemresultado_servico ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servico = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvCod" , Order = 70 )]
      public Nullable<int> gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvAls" , Order = 71 )]
      public String gxTpr_Contagemresultado_cntsrvals
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntsrvals) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvals = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoGrupo" , Order = 72 )]
      public Nullable<int> gxTpr_Contagemresultado_servicogrupo
      {
         get {
            return sdt.gxTpr_Contagemresultado_servicogrupo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicogrupo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoSigla" , Order = 73 )]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_servicosigla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoTela" , Order = 74 )]
      public String gxTpr_Contagemresultado_servicotela
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_servicotela) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicotela = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoAtivo" , Order = 75 )]
      public bool gxTpr_Contagemresultado_servicoativo
      {
         get {
            return sdt.gxTpr_Contagemresultado_servicoativo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicoativo = value;
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoNaoRqrAtr" , Order = 76 )]
      public bool gxTpr_Contagemresultado_serviconaorqratr
      {
         get {
            return sdt.gxTpr_Contagemresultado_serviconaorqratr ;
         }

         set {
            sdt.gxTpr_Contagemresultado_serviconaorqratr = value;
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoSS" , Order = 77 )]
      public Nullable<int> gxTpr_Contagemresultado_servicoss
      {
         get {
            return sdt.gxTpr_Contagemresultado_servicoss ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicoss = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoSSSgl" , Order = 78 )]
      public String gxTpr_Contagemresultado_servicosssgl
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_servicosssgl) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicosssgl = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_SrvLnhNegCod" , Order = 79 )]
      public Nullable<int> gxTpr_Contagemresultado_srvlnhnegcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_srvlnhnegcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_srvlnhnegcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_EhValidacao" , Order = 80 )]
      public bool gxTpr_Contagemresultado_ehvalidacao
      {
         get {
            return sdt.gxTpr_Contagemresultado_ehvalidacao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_ehvalidacao = value;
         }

      }

      [DataMember( Name = "ContagemResultado_EsforcoTotal" , Order = 81 )]
      public String gxTpr_Contagemresultado_esforcototal
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_esforcototal) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_esforcototal = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_EsforcoSoma" , Order = 82 )]
      public String gxTpr_Contagemresultado_esforcosoma
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_esforcosoma), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_esforcosoma = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_Observacao" , Order = 83 )]
      public String gxTpr_Contagemresultado_observacao
      {
         get {
            return sdt.gxTpr_Contagemresultado_observacao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_observacao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Evidencia" , Order = 84 )]
      public String gxTpr_Contagemresultado_evidencia
      {
         get {
            return sdt.gxTpr_Contagemresultado_evidencia ;
         }

         set {
            sdt.gxTpr_Contagemresultado_evidencia = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DataCadastro" , Order = 85 )]
      public String gxTpr_Contagemresultado_datacadastro
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_datacadastro) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datacadastro = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_Owner" , Order = 86 )]
      public Nullable<int> gxTpr_Contagemresultado_owner
      {
         get {
            return sdt.gxTpr_Contagemresultado_owner ;
         }

         set {
            sdt.gxTpr_Contagemresultado_owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Owner_Identificao" , Order = 87 )]
      public String gxTpr_Contagemresultado_owner_identificao
      {
         get {
            return sdt.gxTpr_Contagemresultado_owner_identificao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_owner_identificao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratanteDoOwner" , Order = 88 )]
      public Nullable<int> gxTpr_Contagemresultado_contratantedoowner
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratantedoowner ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratantedoowner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaDoOwner" , Order = 89 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadadoowner
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadadoowner ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadadoowner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Responsavel" , Order = 90 )]
      public Nullable<int> gxTpr_Contagemresultado_responsavel
      {
         get {
            return sdt.gxTpr_Contagemresultado_responsavel ;
         }

         set {
            sdt.gxTpr_Contagemresultado_responsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ResponsavelPessCod" , Order = 91 )]
      public Nullable<int> gxTpr_Contagemresultado_responsavelpesscod
      {
         get {
            return sdt.gxTpr_Contagemresultado_responsavelpesscod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_responsavelpesscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ResponsavelPessNome" , Order = 92 )]
      public String gxTpr_Contagemresultado_responsavelpessnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_responsavelpessnome) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_responsavelpessnome = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaDoResponsavel" , Order = 93 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadadoresponsavel
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadadoresponsavel ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadadoresponsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratanteDoResponsavel" , Order = 94 )]
      public Nullable<int> gxTpr_Contagemresultado_contratantedoresponsavel
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratantedoresponsavel ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratantedoresponsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ValorPF" , Order = 95 )]
      public String gxTpr_Contagemresultado_valorpf
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_valorpf, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_valorpf = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_Custo" , Order = 96 )]
      public String gxTpr_Contagemresultado_custo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_custo, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_custo = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_OSVinculada" , Order = 97 )]
      public Nullable<int> gxTpr_Contagemresultado_osvinculada
      {
         get {
            return sdt.gxTpr_Contagemresultado_osvinculada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osvinculada = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DmnVinculada" , Order = 98 )]
      public String gxTpr_Contagemresultado_dmnvinculada
      {
         get {
            return sdt.gxTpr_Contagemresultado_dmnvinculada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dmnvinculada = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DmnVinculadaRef" , Order = 99 )]
      public String gxTpr_Contagemresultado_dmnvinculadaref
      {
         get {
            return sdt.gxTpr_Contagemresultado_dmnvinculadaref ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dmnvinculadaref = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PFBFSImp" , Order = 100 )]
      public String gxTpr_Contagemresultado_pfbfsimp
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pfbfsimp, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pfbfsimp = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFSImp" , Order = 101 )]
      public String gxTpr_Contagemresultado_pflfsimp
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfsimp, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfsimp = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PBFinal" , Order = 102 )]
      public String gxTpr_Contagemresultado_pbfinal
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pbfinal, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pbfinal = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PLFinal" , Order = 103 )]
      public String gxTpr_Contagemresultado_plfinal
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_plfinal, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_plfinal = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_LiqLogCod" , Order = 104 )]
      public Nullable<int> gxTpr_Contagemresultado_liqlogcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_liqlogcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_liqlogcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Liquidada" , Order = 105 )]
      public bool gxTpr_Contagemresultado_liquidada
      {
         get {
            return sdt.gxTpr_Contagemresultado_liquidada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_liquidada = value;
         }

      }

      [DataMember( Name = "ContagemResultadoLiqLog_Data" , Order = 106 )]
      public String gxTpr_Contagemresultadoliqlog_data
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadoliqlog_data) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoliqlog_data = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_FncUsrCod" , Order = 107 )]
      public Nullable<int> gxTpr_Contagemresultado_fncusrcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_fncusrcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_fncusrcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Agrupador" , Order = 108 )]
      public String gxTpr_Contagemresultado_agrupador
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_agrupador) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_agrupador = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvPrrCod" , Order = 109 )]
      public Nullable<int> gxTpr_Contagemresultado_cntsrvprrcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvprrcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvprrcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvPrrNome" , Order = 110 )]
      public String gxTpr_Contagemresultado_cntsrvprrnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntsrvprrnome) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvprrnome = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvPrrPrz" , Order = 111 )]
      public Nullable<decimal> gxTpr_Contagemresultado_cntsrvprrprz
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvprrprz ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvprrprz = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvPrrCst" , Order = 112 )]
      public Nullable<decimal> gxTpr_Contagemresultado_cntsrvprrcst
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvprrcst ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvprrcst = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_TemDpnHmlg" , Order = 113 )]
      public bool gxTpr_Contagemresultado_temdpnhmlg
      {
         get {
            return sdt.gxTpr_Contagemresultado_temdpnhmlg ;
         }

         set {
            sdt.gxTpr_Contagemresultado_temdpnhmlg = value;
         }

      }

      [DataMember( Name = "ContagemResultado_TemPndHmlg" , Order = 114 )]
      public bool gxTpr_Contagemresultado_tempndhmlg
      {
         get {
            return sdt.gxTpr_Contagemresultado_tempndhmlg ;
         }

         set {
            sdt.gxTpr_Contagemresultado_tempndhmlg = value;
         }

      }

      [DataMember( Name = "ContagemResultado_TmpEstAnl" , Order = 115 )]
      public String gxTpr_Contagemresultado_tmpestanl
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_tmpestanl), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_tmpestanl = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_TmpEstExc" , Order = 116 )]
      public String gxTpr_Contagemresultado_tmpestexc
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_tmpestexc), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_tmpestexc = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_TmpEstCrr" , Order = 117 )]
      public String gxTpr_Contagemresultado_tmpestcrr
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_tmpestcrr), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_tmpestcrr = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_InicioAnl" , Order = 118 )]
      public String gxTpr_Contagemresultado_inicioanl
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_inicioanl) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_inicioanl = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_FimAnl" , Order = 119 )]
      public String gxTpr_Contagemresultado_fimanl
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_fimanl) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_fimanl = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_InicioExc" , Order = 120 )]
      public String gxTpr_Contagemresultado_inicioexc
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_inicioexc) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_inicioexc = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_FimExc" , Order = 121 )]
      public String gxTpr_Contagemresultado_fimexc
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_fimexc) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_fimexc = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_InicioCrr" , Order = 122 )]
      public String gxTpr_Contagemresultado_iniciocrr
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_iniciocrr) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_iniciocrr = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_FimCrr" , Order = 123 )]
      public String gxTpr_Contagemresultado_fimcrr
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_fimcrr) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_fimcrr = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_Evento" , Order = 124 )]
      public Nullable<short> gxTpr_Contagemresultado_evento
      {
         get {
            return sdt.gxTpr_Contagemresultado_evento ;
         }

         set {
            sdt.gxTpr_Contagemresultado_evento = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ProjetoCod" , Order = 125 )]
      public Nullable<int> gxTpr_Contagemresultado_projetocod
      {
         get {
            return sdt.gxTpr_Contagemresultado_projetocod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_projetocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvTpVnc" , Order = 126 )]
      public String gxTpr_Contagemresultado_cntsrvtpvnc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntsrvtpvnc) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvtpvnc = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvFtrm" , Order = 127 )]
      public String gxTpr_Contagemresultado_cntsrvftrm
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvftrm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvftrm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvPrc" , Order = 128 )]
      public String gxTpr_Contagemresultado_cntsrvprc
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_cntsrvprc, 7, 3)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvprc = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvVlrUndCnt" , Order = 129 )]
      public String gxTpr_Contagemresultado_cntsrvvlrundcnt
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_cntsrvvlrundcnt, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvvlrundcnt = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvSttPgmFnc" , Order = 130 )]
      public String gxTpr_Contagemresultado_cntsrvsttpgmfnc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntsrvsttpgmfnc) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvsttpgmfnc = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvIndDvr" , Order = 131 )]
      public Nullable<decimal> gxTpr_Contagemresultado_cntsrvinddvr
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvinddvr ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvinddvr = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvQtdUntCns" , Order = 132 )]
      public String gxTpr_Contagemresultado_cntsrvqtduntcns
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_cntsrvqtduntcns, 9, 4)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvqtduntcns = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvUndCnt" , Order = 133 )]
      public Nullable<int> gxTpr_Contagemresultado_cntsrvundcnt
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvundcnt ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvundcnt = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvUndCntSgl" , Order = 134 )]
      public String gxTpr_Contagemresultado_cntsrvundcntsgl
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntsrvundcntsgl) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvundcntsgl = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvMmn" , Order = 135 )]
      public String gxTpr_Contagemresultado_cntsrvmmn
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntsrvmmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvmmn = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzAnl" , Order = 136 )]
      public Nullable<short> gxTpr_Contagemresultado_przanl
      {
         get {
            return sdt.gxTpr_Contagemresultado_przanl ;
         }

         set {
            sdt.gxTpr_Contagemresultado_przanl = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzExc" , Order = 137 )]
      public Nullable<short> gxTpr_Contagemresultado_przexc
      {
         get {
            return sdt.gxTpr_Contagemresultado_przexc ;
         }

         set {
            sdt.gxTpr_Contagemresultado_przexc = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzCrr" , Order = 138 )]
      public Nullable<short> gxTpr_Contagemresultado_przcrr
      {
         get {
            return sdt.gxTpr_Contagemresultado_przcrr ;
         }

         set {
            sdt.gxTpr_Contagemresultado_przcrr = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzRsp" , Order = 139 )]
      public Nullable<short> gxTpr_Contagemresultado_przrsp
      {
         get {
            return sdt.gxTpr_Contagemresultado_przrsp ;
         }

         set {
            sdt.gxTpr_Contagemresultado_przrsp = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzGrt" , Order = 140 )]
      public Nullable<short> gxTpr_Contagemresultado_przgrt
      {
         get {
            return sdt.gxTpr_Contagemresultado_przgrt ;
         }

         set {
            sdt.gxTpr_Contagemresultado_przgrt = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzTp" , Order = 141 )]
      public String gxTpr_Contagemresultado_prztp
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_prztp) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prztp = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzTpDias" , Order = 142 )]
      public String gxTpr_Contagemresultado_prztpdias
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_prztpdias) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prztpdias = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntCod" , Order = 143 )]
      public Nullable<int> gxTpr_Contagemresultado_cntcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntNum" , Order = 144 )]
      public String gxTpr_Contagemresultado_cntnum
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntnum) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntnum = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntPrpCod" , Order = 145 )]
      public Nullable<int> gxTpr_Contagemresultado_cntprpcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntprpcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntprpcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntPrpPesCod" , Order = 146 )]
      public Nullable<int> gxTpr_Contagemresultado_cntprppescod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntprppescod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntprppescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntPrpPesNom" , Order = 147 )]
      public String gxTpr_Contagemresultado_cntprppesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntprppesnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntprppesnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntIndDvr" , Order = 148 )]
      public Nullable<decimal> gxTpr_Contagemresultado_cntinddvr
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntinddvr ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntinddvr = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntClcDvr" , Order = 149 )]
      public String gxTpr_Contagemresultado_cntclcdvr
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntclcdvr) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntclcdvr = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntVlrUndCnt" , Order = 150 )]
      public String gxTpr_Contagemresultado_cntvlrundcnt
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_cntvlrundcnt, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntvlrundcnt = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_CntPrdFtrCada" , Order = 151 )]
      public String gxTpr_Contagemresultado_cntprdftrcada
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_cntprdftrcada) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntprdftrcada = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntPrdFtrIni" , Order = 152 )]
      public Nullable<short> gxTpr_Contagemresultado_cntprdftrini
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntprdftrini ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntprdftrini = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntPrdFtrFim" , Order = 153 )]
      public Nullable<short> gxTpr_Contagemresultado_cntprdftrfim
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntprdftrfim ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntprdftrfim = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntLmtFtr" , Order = 154 )]
      public String gxTpr_Contagemresultado_cntlmtftr
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_cntlmtftr, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntlmtftr = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_DatVgnInc" , Order = 155 )]
      public String gxTpr_Contagemresultado_datvgninc
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datvgninc) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datvgninc = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_DatIncTA" , Order = 156 )]
      public String gxTpr_Contagemresultado_datincta
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datincta) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datincta = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoNome" , Order = 157 )]
      public String gxTpr_Contagemresultado_serviconome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_serviconome) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_serviconome = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoTpHrrq" , Order = 158 )]
      public Nullable<short> gxTpr_Contagemresultado_servicotphrrq
      {
         get {
            return sdt.gxTpr_Contagemresultado_servicotphrrq ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicotphrrq = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CodSrvVnc" , Order = 159 )]
      public Nullable<int> gxTpr_Contagemresultado_codsrvvnc
      {
         get {
            return sdt.gxTpr_Contagemresultado_codsrvvnc ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codsrvvnc = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_SiglaSrvVnc" , Order = 160 )]
      public String gxTpr_Contagemresultado_siglasrvvnc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_siglasrvvnc) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_siglasrvvnc = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CodSrvSSVnc" , Order = 161 )]
      public Nullable<int> gxTpr_Contagemresultado_codsrvssvnc
      {
         get {
            return sdt.gxTpr_Contagemresultado_codsrvssvnc ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codsrvssvnc = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvVncCod" , Order = 162 )]
      public Nullable<int> gxTpr_Contagemresultado_cntsrvvnccod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvvnccod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvvnccod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CntVncCod" , Order = 163 )]
      public Nullable<int> gxTpr_Contagemresultado_cntvnccod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntvnccod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntvnccod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_GlsIndValor" , Order = 164 )]
      public String gxTpr_Contagemresultado_glsindvalor
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_glsindvalor, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_glsindvalor = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_TipoRegistro" , Order = 165 )]
      public Nullable<short> gxTpr_Contagemresultado_tiporegistro
      {
         get {
            return sdt.gxTpr_Contagemresultado_tiporegistro ;
         }

         set {
            sdt.gxTpr_Contagemresultado_tiporegistro = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_UOOwner" , Order = 166 )]
      public Nullable<int> gxTpr_Contagemresultado_uoowner
      {
         get {
            return sdt.gxTpr_Contagemresultado_uoowner ;
         }

         set {
            sdt.gxTpr_Contagemresultado_uoowner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Referencia" , Order = 167 )]
      public String gxTpr_Contagemresultado_referencia
      {
         get {
            return sdt.gxTpr_Contagemresultado_referencia ;
         }

         set {
            sdt.gxTpr_Contagemresultado_referencia = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Restricoes" , Order = 168 )]
      public String gxTpr_Contagemresultado_restricoes
      {
         get {
            return sdt.gxTpr_Contagemresultado_restricoes ;
         }

         set {
            sdt.gxTpr_Contagemresultado_restricoes = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PrioridadePrevista" , Order = 169 )]
      public String gxTpr_Contagemresultado_prioridadeprevista
      {
         get {
            return sdt.gxTpr_Contagemresultado_prioridadeprevista ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prioridadeprevista = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PrzInc" , Order = 170 )]
      public Nullable<short> gxTpr_Contagemresultado_przinc
      {
         get {
            return sdt.gxTpr_Contagemresultado_przinc ;
         }

         set {
            sdt.gxTpr_Contagemresultado_przinc = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_TemProposta" , Order = 171 )]
      public bool gxTpr_Contagemresultado_temproposta
      {
         get {
            return sdt.gxTpr_Contagemresultado_temproposta ;
         }

         set {
            sdt.gxTpr_Contagemresultado_temproposta = value;
         }

      }

      [DataMember( Name = "ContagemResultado_Combinada" , Order = 172 )]
      public bool gxTpr_Contagemresultado_combinada
      {
         get {
            return sdt.gxTpr_Contagemresultado_combinada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_combinada = value;
         }

      }

      [DataMember( Name = "ContagemResultado_Entrega" , Order = 173 )]
      public Nullable<short> gxTpr_Contagemresultado_entrega
      {
         get {
            return sdt.gxTpr_Contagemresultado_entrega ;
         }

         set {
            sdt.gxTpr_Contagemresultado_entrega = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_SemCusto" , Order = 174 )]
      public bool gxTpr_Contagemresultado_semcusto
      {
         get {
            return sdt.gxTpr_Contagemresultado_semcusto ;
         }

         set {
            sdt.gxTpr_Contagemresultado_semcusto = value;
         }

      }

      [DataMember( Name = "ContagemResultado_VlrCnc" , Order = 175 )]
      public String gxTpr_Contagemresultado_vlrcnc
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_vlrcnc, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_vlrcnc = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFCnc" , Order = 176 )]
      public String gxTpr_Contagemresultado_pfcnc
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pfcnc, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pfcnc = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_DataPrvPgm" , Order = 177 )]
      public String gxTpr_Contagemresultado_dataprvpgm
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_dataprvpgm) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataprvpgm = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_QuantidadeSolicitada" , Order = 178 )]
      public String gxTpr_Contagemresultado_quantidadesolicitada
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_quantidadesolicitada, 9, 4)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_quantidadesolicitada = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_RequisitadoPorContratante" , Order = 179 )]
      public bool gxTpr_Contagemresultado_requisitadoporcontratante
      {
         get {
            return sdt.gxTpr_Contagemresultado_requisitadoporcontratante ;
         }

         set {
            sdt.gxTpr_Contagemresultado_requisitadoporcontratante = value;
         }

      }

      [DataMember( Name = "ContagemResultado_Requisitante" , Order = 180 )]
      public String gxTpr_Contagemresultado_requisitante
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_requisitante) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_requisitante = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_CargoUONom" , Order = 181 )]
      public String gxTpr_Usuario_cargouonom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_cargouonom) ;
         }

         set {
            sdt.gxTpr_Usuario_cargouonom = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_UndCntNome" , Order = 182 )]
      public String gxTpr_Contratoservicos_undcntnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_undcntnome) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_undcntnome = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_QntUntCns" , Order = 183 )]
      public String gxTpr_Contratoservicos_qntuntcns
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicos_qntuntcns, 9, 4)) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_qntuntcns = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratoServicosPrazo_Complexidade" , Order = 184 )]
      public Nullable<short> gxTpr_Contratoservicosprazo_complexidade
      {
         get {
            return sdt.gxTpr_Contratoservicosprazo_complexidade ;
         }

         set {
            sdt.gxTpr_Contratoservicosprazo_complexidade = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_Codigo" , Order = 185 )]
      public Nullable<int> gxTpr_Contratoservicosprioridade_codigo
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_codigo ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_LocalExec" , Order = 186 )]
      public String gxTpr_Contratoservicos_localexec
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_localexec) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_localexec = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PrazoAnalise" , Order = 187 )]
      public String gxTpr_Contagemresultado_prazoanalise
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_prazoanalise) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prazoanalise = DateTimeUtil.CToD2( (String)(value));
         }

      }

      public SdtSDT_WP_OS sdt
      {
         get {
            return (SdtSDT_WP_OS)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WP_OS() ;
         }
      }

      protected DateTime GXt_dtime1 ;
   }

}
