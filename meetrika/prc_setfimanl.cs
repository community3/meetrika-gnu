/*
               File: PRC_SetFimAnl
        Description: Set Fim Anl
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:37.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_setfimanl : GXProcedure
   {
      public prc_setfimanl( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_setfimanl( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Codigo )
      {
         this.AV18Codigo = aP0_Codigo;
         initialize();
         executePrivate();
         aP0_Codigo=this.AV18Codigo;
      }

      public int executeUdp( )
      {
         this.AV18Codigo = aP0_Codigo;
         initialize();
         executePrivate();
         aP0_Codigo=this.AV18Codigo;
         return AV18Codigo ;
      }

      public void executeSubmit( ref int aP0_Codigo )
      {
         prc_setfimanl objprc_setfimanl;
         objprc_setfimanl = new prc_setfimanl();
         objprc_setfimanl.AV18Codigo = aP0_Codigo;
         objprc_setfimanl.context.SetSubmitInitialConfig(context);
         objprc_setfimanl.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_setfimanl);
         aP0_Codigo=this.AV18Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_setfimanl)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00B13 */
         pr_default.execute(0, new Object[] {AV18Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00B13_A456ContagemResultado_Codigo[0];
            A1521ContagemResultado_FimAnl = P00B13_A1521ContagemResultado_FimAnl[0];
            n1521ContagemResultado_FimAnl = P00B13_n1521ContagemResultado_FimAnl[0];
            A1520ContagemResultado_InicioAnl = P00B13_A1520ContagemResultado_InicioAnl[0];
            n1520ContagemResultado_InicioAnl = P00B13_n1520ContagemResultado_InicioAnl[0];
            A490ContagemResultado_ContratadaCod = P00B13_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00B13_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = P00B13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00B13_n601ContagemResultado_Servico[0];
            A1553ContagemResultado_CntSrvCod = P00B13_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00B13_n1553ContagemResultado_CntSrvCod[0];
            A1519ContagemResultado_TmpEstAnl = P00B13_A1519ContagemResultado_TmpEstAnl[0];
            n1519ContagemResultado_TmpEstAnl = P00B13_n1519ContagemResultado_TmpEstAnl[0];
            A584ContagemResultado_ContadorFM = P00B13_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00B13_n584ContagemResultado_ContadorFM[0];
            A584ContagemResultado_ContadorFM = P00B13_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00B13_n584ContagemResultado_ContadorFM[0];
            A601ContagemResultado_Servico = P00B13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00B13_n601ContagemResultado_Servico[0];
            A1521ContagemResultado_FimAnl = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1521ContagemResultado_FimAnl = false;
            if ( P00B13_n1520ContagemResultado_InicioAnl[0] )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00B14 */
               pr_default.execute(1, new Object[] {n1521ContagemResultado_FimAnl, A1521ContagemResultado_FimAnl, n1519ContagemResultado_TmpEstAnl, A1519ContagemResultado_TmpEstAnl, A456ContagemResultado_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
            }
            AV10Contratada = A490ContagemResultado_ContratadaCod;
            AV11Servico = A601ContagemResultado_Servico;
            AV12Contador = A584ContagemResultado_ContadorFM;
            AV23ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            GXt_int1 = AV9TmpEstAnl;
            GXt_int2 = (int)(DateTimeUtil.TDiff( A1521ContagemResultado_FimAnl, A1520ContagemResultado_InicioAnl));
            new prc_sstohhmm(context ).execute( ref  GXt_int2, ref  AV13Horas, out  GXt_int1) ;
            AV9TmpEstAnl = GXt_int1;
            A1519ContagemResultado_TmpEstAnl = (int)(A1519ContagemResultado_TmpEstAnl+AV9TmpEstAnl);
            n1519ContagemResultado_TmpEstAnl = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00B15 */
            pr_default.execute(2, new Object[] {n1521ContagemResultado_FimAnl, A1521ContagemResultado_FimAnl, n1519ContagemResultado_TmpEstAnl, A1519ContagemResultado_TmpEstAnl, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00B16 */
            pr_default.execute(3, new Object[] {n1521ContagemResultado_FimAnl, A1521ContagemResultado_FimAnl, n1519ContagemResultado_TmpEstAnl, A1519ContagemResultado_TmpEstAnl, A456ContagemResultado_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00B18 */
         pr_default.execute(4, new Object[] {AV23ContratoServicos_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A456ContagemResultado_Codigo = P00B18_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00B18_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00B18_n602ContagemResultado_OSVinculada[0];
            A1519ContagemResultado_TmpEstAnl = P00B18_A1519ContagemResultado_TmpEstAnl[0];
            n1519ContagemResultado_TmpEstAnl = P00B18_n1519ContagemResultado_TmpEstAnl[0];
            A1553ContagemResultado_CntSrvCod = P00B18_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00B18_n1553ContagemResultado_CntSrvCod[0];
            A40000GXC1 = P00B18_A40000GXC1[0];
            n40000GXC1 = P00B18_n40000GXC1[0];
            A40001GXC2 = P00B18_A40001GXC2[0];
            n40001GXC2 = P00B18_n40001GXC2[0];
            A40000GXC1 = P00B18_A40000GXC1[0];
            n40000GXC1 = P00B18_n40000GXC1[0];
            A40001GXC2 = P00B18_A40001GXC2[0];
            n40001GXC2 = P00B18_n40001GXC2[0];
            AV17Count = A40000GXC1;
            AV19Soma = A40001GXC2;
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( AV17Count > 0 )
         {
            AV15Media = (int)((AV19Soma+AV9TmpEstAnl)/ (decimal)(AV17Count));
         }
         else
         {
            AV15Media = AV9TmpEstAnl;
         }
         AV17Count = 0;
         /* Using cursor P00B111 */
         pr_default.execute(5, new Object[] {AV23ContratoServicos_Codigo, AV12Contador});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A456ContagemResultado_Codigo = P00B111_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00B111_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00B111_n602ContagemResultado_OSVinculada[0];
            A1519ContagemResultado_TmpEstAnl = P00B111_A1519ContagemResultado_TmpEstAnl[0];
            n1519ContagemResultado_TmpEstAnl = P00B111_n1519ContagemResultado_TmpEstAnl[0];
            A1553ContagemResultado_CntSrvCod = P00B111_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00B111_n1553ContagemResultado_CntSrvCod[0];
            A584ContagemResultado_ContadorFM = P00B111_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00B111_n584ContagemResultado_ContadorFM[0];
            A40000GXC1 = P00B111_A40000GXC1[0];
            n40000GXC1 = P00B111_n40000GXC1[0];
            A40001GXC2 = P00B111_A40001GXC2[0];
            n40001GXC2 = P00B111_n40001GXC2[0];
            A584ContagemResultado_ContadorFM = P00B111_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00B111_n584ContagemResultado_ContadorFM[0];
            A40000GXC1 = P00B111_A40000GXC1[0];
            n40000GXC1 = P00B111_n40000GXC1[0];
            A40001GXC2 = P00B111_A40001GXC2[0];
            n40001GXC2 = P00B111_n40001GXC2[0];
            AV17Count = A40000GXC1;
            AV19Soma = A40001GXC2;
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( AV17Count > 0 )
         {
            AV16MediaCr = (int)((AV19Soma+AV9TmpEstAnl)/ (decimal)(AV17Count));
         }
         else
         {
            AV16MediaCr = AV9TmpEstAnl;
         }
         /* Using cursor P00B112 */
         pr_default.execute(6, new Object[] {AV23ContratoServicos_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A160ContratoServicos_Codigo = P00B112_A160ContratoServicos_Codigo[0];
            A1516ContratoServicos_TmpEstAnl = P00B112_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = P00B112_n1516ContratoServicos_TmpEstAnl[0];
            A1516ContratoServicos_TmpEstAnl = AV15Media;
            n1516ContratoServicos_TmpEstAnl = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00B113 */
            pr_default.execute(7, new Object[] {n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, A160ContratoServicos_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
            if (true) break;
            /* Using cursor P00B114 */
            pr_default.execute(8, new Object[] {n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, A160ContratoServicos_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
         /* Using cursor P00B115 */
         pr_default.execute(9, new Object[] {AV10Contratada, AV12Contador});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = P00B115_A69ContratadaUsuario_UsuarioCod[0];
            A66ContratadaUsuario_ContratadaCod = P00B115_A66ContratadaUsuario_ContratadaCod[0];
            A1518ContratadaUsuario_TmpEstAnl = P00B115_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = P00B115_n1518ContratadaUsuario_TmpEstAnl[0];
            A1518ContratadaUsuario_TmpEstAnl = AV16MediaCr;
            n1518ContratadaUsuario_TmpEstAnl = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00B116 */
            pr_default.execute(10, new Object[] {n1518ContratadaUsuario_TmpEstAnl, A1518ContratadaUsuario_TmpEstAnl, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
            if (true) break;
            /* Using cursor P00B117 */
            pr_default.execute(11, new Object[] {n1518ContratadaUsuario_TmpEstAnl, A1518ContratadaUsuario_TmpEstAnl, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(9);
         /* Using cursor P00B118 */
         pr_default.execute(12, new Object[] {AV12Contador, AV11Servico});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A829UsuarioServicos_ServicoCod = P00B118_A829UsuarioServicos_ServicoCod[0];
            A828UsuarioServicos_UsuarioCod = P00B118_A828UsuarioServicos_UsuarioCod[0];
            A1517UsuarioServicos_TmpEstAnl = P00B118_A1517UsuarioServicos_TmpEstAnl[0];
            n1517UsuarioServicos_TmpEstAnl = P00B118_n1517UsuarioServicos_TmpEstAnl[0];
            A1517UsuarioServicos_TmpEstAnl = AV16MediaCr;
            n1517UsuarioServicos_TmpEstAnl = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00B119 */
            pr_default.execute(13, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
            pr_default.close(13);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
            if (true) break;
            /* Using cursor P00B120 */
            pr_default.execute(14, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(12);
         new prc_setinicioexc(context ).execute( ref  AV18Codigo) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00B13_A456ContagemResultado_Codigo = new int[1] ;
         P00B13_A1521ContagemResultado_FimAnl = new DateTime[] {DateTime.MinValue} ;
         P00B13_n1521ContagemResultado_FimAnl = new bool[] {false} ;
         P00B13_A1520ContagemResultado_InicioAnl = new DateTime[] {DateTime.MinValue} ;
         P00B13_n1520ContagemResultado_InicioAnl = new bool[] {false} ;
         P00B13_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00B13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00B13_A601ContagemResultado_Servico = new int[1] ;
         P00B13_n601ContagemResultado_Servico = new bool[] {false} ;
         P00B13_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00B13_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00B13_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00B13_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00B13_A584ContagemResultado_ContadorFM = new int[1] ;
         P00B13_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         A1521ContagemResultado_FimAnl = (DateTime)(DateTime.MinValue);
         A1520ContagemResultado_InicioAnl = (DateTime)(DateTime.MinValue);
         P00B18_A456ContagemResultado_Codigo = new int[1] ;
         P00B18_A602ContagemResultado_OSVinculada = new int[1] ;
         P00B18_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00B18_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00B18_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00B18_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00B18_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00B18_A40000GXC1 = new int[1] ;
         P00B18_n40000GXC1 = new bool[] {false} ;
         P00B18_A40001GXC2 = new int[1] ;
         P00B18_n40001GXC2 = new bool[] {false} ;
         P00B111_A456ContagemResultado_Codigo = new int[1] ;
         P00B111_A602ContagemResultado_OSVinculada = new int[1] ;
         P00B111_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00B111_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00B111_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00B111_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00B111_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00B111_A584ContagemResultado_ContadorFM = new int[1] ;
         P00B111_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         P00B111_A40000GXC1 = new int[1] ;
         P00B111_n40000GXC1 = new bool[] {false} ;
         P00B111_A40001GXC2 = new int[1] ;
         P00B111_n40001GXC2 = new bool[] {false} ;
         P00B112_A160ContratoServicos_Codigo = new int[1] ;
         P00B112_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         P00B112_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         P00B115_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00B115_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00B115_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         P00B115_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         P00B118_A829UsuarioServicos_ServicoCod = new int[1] ;
         P00B118_A828UsuarioServicos_UsuarioCod = new int[1] ;
         P00B118_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         P00B118_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_setfimanl__default(),
            new Object[][] {
                new Object[] {
               P00B13_A456ContagemResultado_Codigo, P00B13_A1521ContagemResultado_FimAnl, P00B13_n1521ContagemResultado_FimAnl, P00B13_A1520ContagemResultado_InicioAnl, P00B13_n1520ContagemResultado_InicioAnl, P00B13_A490ContagemResultado_ContratadaCod, P00B13_n490ContagemResultado_ContratadaCod, P00B13_A601ContagemResultado_Servico, P00B13_n601ContagemResultado_Servico, P00B13_A1553ContagemResultado_CntSrvCod,
               P00B13_n1553ContagemResultado_CntSrvCod, P00B13_A1519ContagemResultado_TmpEstAnl, P00B13_n1519ContagemResultado_TmpEstAnl, P00B13_A584ContagemResultado_ContadorFM, P00B13_n584ContagemResultado_ContadorFM
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00B18_A456ContagemResultado_Codigo, P00B18_A602ContagemResultado_OSVinculada, P00B18_n602ContagemResultado_OSVinculada, P00B18_A1519ContagemResultado_TmpEstAnl, P00B18_n1519ContagemResultado_TmpEstAnl, P00B18_A1553ContagemResultado_CntSrvCod, P00B18_n1553ContagemResultado_CntSrvCod, P00B18_A40000GXC1, P00B18_n40000GXC1, P00B18_A40001GXC2,
               P00B18_n40001GXC2
               }
               , new Object[] {
               P00B111_A456ContagemResultado_Codigo, P00B111_A602ContagemResultado_OSVinculada, P00B111_n602ContagemResultado_OSVinculada, P00B111_A1519ContagemResultado_TmpEstAnl, P00B111_n1519ContagemResultado_TmpEstAnl, P00B111_A1553ContagemResultado_CntSrvCod, P00B111_n1553ContagemResultado_CntSrvCod, P00B111_A584ContagemResultado_ContadorFM, P00B111_n584ContagemResultado_ContadorFM, P00B111_A40000GXC1,
               P00B111_n40000GXC1, P00B111_A40001GXC2, P00B111_n40001GXC2
               }
               , new Object[] {
               P00B112_A160ContratoServicos_Codigo, P00B112_A1516ContratoServicos_TmpEstAnl, P00B112_n1516ContratoServicos_TmpEstAnl
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00B115_A69ContratadaUsuario_UsuarioCod, P00B115_A66ContratadaUsuario_ContratadaCod, P00B115_A1518ContratadaUsuario_TmpEstAnl, P00B115_n1518ContratadaUsuario_TmpEstAnl
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00B118_A829UsuarioServicos_ServicoCod, P00B118_A828UsuarioServicos_UsuarioCod, P00B118_A1517UsuarioServicos_TmpEstAnl, P00B118_n1517UsuarioServicos_TmpEstAnl
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV18Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1519ContagemResultado_TmpEstAnl ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV10Contratada ;
      private int AV11Servico ;
      private int AV12Contador ;
      private int AV23ContratoServicos_Codigo ;
      private int AV9TmpEstAnl ;
      private int GXt_int1 ;
      private int GXt_int2 ;
      private int AV13Horas ;
      private int A602ContagemResultado_OSVinculada ;
      private int A40000GXC1 ;
      private int A40001GXC2 ;
      private int AV17Count ;
      private int AV19Soma ;
      private int AV15Media ;
      private int AV16MediaCr ;
      private int A160ContratoServicos_Codigo ;
      private int A1516ContratoServicos_TmpEstAnl ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1518ContratadaUsuario_TmpEstAnl ;
      private int A829UsuarioServicos_ServicoCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A1517UsuarioServicos_TmpEstAnl ;
      private String scmdbuf ;
      private DateTime A1521ContagemResultado_FimAnl ;
      private DateTime A1520ContagemResultado_InicioAnl ;
      private bool n1521ContagemResultado_FimAnl ;
      private bool n1520ContagemResultado_InicioAnl ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1519ContagemResultado_TmpEstAnl ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n40000GXC1 ;
      private bool n40001GXC2 ;
      private bool n1516ContratoServicos_TmpEstAnl ;
      private bool n1518ContratadaUsuario_TmpEstAnl ;
      private bool n1517UsuarioServicos_TmpEstAnl ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00B13_A456ContagemResultado_Codigo ;
      private DateTime[] P00B13_A1521ContagemResultado_FimAnl ;
      private bool[] P00B13_n1521ContagemResultado_FimAnl ;
      private DateTime[] P00B13_A1520ContagemResultado_InicioAnl ;
      private bool[] P00B13_n1520ContagemResultado_InicioAnl ;
      private int[] P00B13_A490ContagemResultado_ContratadaCod ;
      private bool[] P00B13_n490ContagemResultado_ContratadaCod ;
      private int[] P00B13_A601ContagemResultado_Servico ;
      private bool[] P00B13_n601ContagemResultado_Servico ;
      private int[] P00B13_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00B13_n1553ContagemResultado_CntSrvCod ;
      private int[] P00B13_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00B13_n1519ContagemResultado_TmpEstAnl ;
      private int[] P00B13_A584ContagemResultado_ContadorFM ;
      private bool[] P00B13_n584ContagemResultado_ContadorFM ;
      private int[] P00B18_A456ContagemResultado_Codigo ;
      private int[] P00B18_A602ContagemResultado_OSVinculada ;
      private bool[] P00B18_n602ContagemResultado_OSVinculada ;
      private int[] P00B18_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00B18_n1519ContagemResultado_TmpEstAnl ;
      private int[] P00B18_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00B18_n1553ContagemResultado_CntSrvCod ;
      private int[] P00B18_A40000GXC1 ;
      private bool[] P00B18_n40000GXC1 ;
      private int[] P00B18_A40001GXC2 ;
      private bool[] P00B18_n40001GXC2 ;
      private int[] P00B111_A456ContagemResultado_Codigo ;
      private int[] P00B111_A602ContagemResultado_OSVinculada ;
      private bool[] P00B111_n602ContagemResultado_OSVinculada ;
      private int[] P00B111_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00B111_n1519ContagemResultado_TmpEstAnl ;
      private int[] P00B111_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00B111_n1553ContagemResultado_CntSrvCod ;
      private int[] P00B111_A584ContagemResultado_ContadorFM ;
      private bool[] P00B111_n584ContagemResultado_ContadorFM ;
      private int[] P00B111_A40000GXC1 ;
      private bool[] P00B111_n40000GXC1 ;
      private int[] P00B111_A40001GXC2 ;
      private bool[] P00B111_n40001GXC2 ;
      private int[] P00B112_A160ContratoServicos_Codigo ;
      private int[] P00B112_A1516ContratoServicos_TmpEstAnl ;
      private bool[] P00B112_n1516ContratoServicos_TmpEstAnl ;
      private int[] P00B115_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00B115_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00B115_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] P00B115_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] P00B118_A829UsuarioServicos_ServicoCod ;
      private int[] P00B118_A828UsuarioServicos_UsuarioCod ;
      private int[] P00B118_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] P00B118_n1517UsuarioServicos_TmpEstAnl ;
   }

   public class prc_setfimanl__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00B13 ;
          prmP00B13 = new Object[] {
          new Object[] {"@AV18Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B14 ;
          prmP00B14 = new Object[] {
          new Object[] {"@ContagemResultado_FimAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B15 ;
          prmP00B15 = new Object[] {
          new Object[] {"@ContagemResultado_FimAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B16 ;
          prmP00B16 = new Object[] {
          new Object[] {"@ContagemResultado_FimAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B18 ;
          prmP00B18 = new Object[] {
          new Object[] {"@AV23ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B111 ;
          prmP00B111 = new Object[] {
          new Object[] {"@AV23ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Contador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B112 ;
          prmP00B112 = new Object[] {
          new Object[] {"@AV23ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B113 ;
          prmP00B113 = new Object[] {
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B114 ;
          prmP00B114 = new Object[] {
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B115 ;
          prmP00B115 = new Object[] {
          new Object[] {"@AV10Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Contador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B116 ;
          prmP00B116 = new Object[] {
          new Object[] {"@ContratadaUsuario_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B117 ;
          prmP00B117 = new Object[] {
          new Object[] {"@ContratadaUsuario_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B118 ;
          prmP00B118 = new Object[] {
          new Object[] {"@AV12Contador",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B119 ;
          prmP00B119 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B120 ;
          prmP00B120 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00B13", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_FimAnl], T1.[ContagemResultado_InicioAnl], T1.[ContagemResultado_ContratadaCod], T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_TmpEstAnl], COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV18Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B13,1,0,true,true )
             ,new CursorDef("P00B14", "UPDATE [ContagemResultado] SET [ContagemResultado_FimAnl]=@ContagemResultado_FimAnl, [ContagemResultado_TmpEstAnl]=@ContagemResultado_TmpEstAnl  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B14)
             ,new CursorDef("P00B15", "UPDATE [ContagemResultado] SET [ContagemResultado_FimAnl]=@ContagemResultado_FimAnl, [ContagemResultado_TmpEstAnl]=@ContagemResultado_TmpEstAnl  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B15)
             ,new CursorDef("P00B16", "UPDATE [ContagemResultado] SET [ContagemResultado_FimAnl]=@ContagemResultado_FimAnl, [ContagemResultado_TmpEstAnl]=@ContagemResultado_TmpEstAnl  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B16)
             ,new CursorDef("P00B18", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_TmpEstAnl], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, COALESCE( T2.[GXC1], 0) AS GXC1, COALESCE( T2.[GXC2], 0) AS GXC2 FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS GXC1, [ContagemResultado_Codigo], [ContagemResultado_OSVinculada], SUM([ContagemResultado_TmpEstAnl]) AS GXC2 FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo], [ContagemResultado_OSVinculada] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo] AND T2.[ContagemResultado_OSVinculada] = T1.[ContagemResultado_OSVinculada]) WHERE (T1.[ContagemResultado_CntSrvCod] = @AV23ContratoServicos_Codigo) AND (T1.[ContagemResultado_TmpEstAnl] > 0) ORDER BY T1.[ContagemResultado_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B18,100,0,false,false )
             ,new CursorDef("P00B111", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_TmpEstAnl], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T3.[GXC1], 0) AS GXC1, COALESCE( T3.[GXC2], 0) AS GXC2 FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT COUNT(*) AS GXC1, [ContagemResultado_Codigo], [ContagemResultado_OSVinculada], SUM([ContagemResultado_TmpEstAnl]) AS GXC2 FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo], [ContagemResultado_OSVinculada] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo] AND T3.[ContagemResultado_OSVinculada] = T1.[ContagemResultado_OSVinculada]) WHERE (T1.[ContagemResultado_CntSrvCod] = @AV23ContratoServicos_Codigo) AND (T1.[ContagemResultado_TmpEstAnl] > 0) AND (COALESCE( T2.[ContagemResultado_ContadorFM], 0) = @AV12Contador) ORDER BY T1.[ContagemResultado_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B111,100,0,false,false )
             ,new CursorDef("P00B112", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_TmpEstAnl] FROM [ContratoServicos] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @AV23ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B112,1,0,true,true )
             ,new CursorDef("P00B113", "UPDATE [ContratoServicos] SET [ContratoServicos_TmpEstAnl]=@ContratoServicos_TmpEstAnl  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B113)
             ,new CursorDef("P00B114", "UPDATE [ContratoServicos] SET [ContratoServicos_TmpEstAnl]=@ContratoServicos_TmpEstAnl  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B114)
             ,new CursorDef("P00B115", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_TmpEstAnl] FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV10Contratada and [ContratadaUsuario_UsuarioCod] = @AV12Contador ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B115,1,0,true,true )
             ,new CursorDef("P00B116", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_TmpEstAnl]=@ContratadaUsuario_TmpEstAnl  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B116)
             ,new CursorDef("P00B117", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_TmpEstAnl]=@ContratadaUsuario_TmpEstAnl  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B117)
             ,new CursorDef("P00B118", "SELECT TOP 1 [UsuarioServicos_ServicoCod], [UsuarioServicos_UsuarioCod], [UsuarioServicos_TmpEstAnl] FROM [UsuarioServicos] WITH (UPDLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV12Contador and [UsuarioServicos_ServicoCod] = @AV11Servico ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B118,1,0,true,true )
             ,new CursorDef("P00B119", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstAnl]=@UsuarioServicos_TmpEstAnl  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B119)
             ,new CursorDef("P00B120", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstAnl]=@UsuarioServicos_TmpEstAnl  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B120)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
       }
    }

 }

}
