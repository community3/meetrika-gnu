/*
               File: WWAgendaAtendimento
        Description:  Agenda Atendimento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:18:3.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwagendaatendimento : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwagendaatendimento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwagendaatendimento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_118 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_118_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_118_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV55DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)));
               AV16AgendaAtendimento_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
               AV17AgendaAtendimento_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
               AV56ContagemResultado_DemandaFM1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM1", AV56ContagemResultado_DemandaFM1);
               AV31ContagemResultado_Demanda1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_Demanda1", AV31ContagemResultado_Demanda1);
               AV61AgendaAtendimento_CodDmn1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61AgendaAtendimento_CodDmn1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61AgendaAtendimento_CodDmn1), 6, 0)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV57DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)));
               AV20AgendaAtendimento_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
               AV21AgendaAtendimento_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
               AV58ContagemResultado_DemandaFM2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DemandaFM2", AV58ContagemResultado_DemandaFM2);
               AV32ContagemResultado_Demanda2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Demanda2", AV32ContagemResultado_Demanda2);
               AV62AgendaAtendimento_CodDmn2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62AgendaAtendimento_CodDmn2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62AgendaAtendimento_CodDmn2), 6, 0)));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV59DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)));
               AV24AgendaAtendimento_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
               AV25AgendaAtendimento_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
               AV60ContagemResultado_DemandaFM3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_DemandaFM3", AV60ContagemResultado_DemandaFM3);
               AV33ContagemResultado_Demanda3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda3", AV33ContagemResultado_Demanda3);
               AV63AgendaAtendimento_CodDmn3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63AgendaAtendimento_CodDmn3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63AgendaAtendimento_CodDmn3), 6, 0)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV37TFAgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data", context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"));
               AV38TFAgendaAtendimento_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFAgendaAtendimento_Data_To", context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"));
               AV65TFAgendaAtendimento_CodDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0)));
               AV66TFAgendaAtendimento_CodDmn_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFAgendaAtendimento_CodDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0)));
               AV52TFContagemResultado_DemandaFM = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultado_DemandaFM", AV52TFContagemResultado_DemandaFM);
               AV53TFContagemResultado_DemandaFM_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultado_DemandaFM_Sel", AV53TFContagemResultado_DemandaFM_Sel);
               AV43TFContagemResultado_Demanda = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContagemResultado_Demanda", AV43TFContagemResultado_Demanda);
               AV44TFContagemResultado_Demanda_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultado_Demanda_Sel", AV44TFContagemResultado_Demanda_Sel);
               AV70TFContagemResultado_CntSrvAls = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_CntSrvAls", AV70TFContagemResultado_CntSrvAls);
               AV71TFContagemResultado_CntSrvAls_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_CntSrvAls_Sel", AV71TFContagemResultado_CntSrvAls_Sel);
               AV47TFAgendaAtendimento_QtdUnd = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5)));
               AV48TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace", AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace);
               AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace", AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace);
               AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
               AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace);
               AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace", AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace);
               AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace", AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace);
               AV68Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
               AV113Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1183AgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1184AgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               A1209AgendaAtendimento_CodDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAID2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTID2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122118441");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwagendaatendimento.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA_TO1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDAFM1", AV56ContagemResultado_DemandaFM1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA1", AV31ContagemResultado_Demanda1);
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_CODDMN1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61AgendaAtendimento_CodDmn1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA_TO2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDAFM2", AV58ContagemResultado_DemandaFM2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA2", AV32ContagemResultado_Demanda2);
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_CODDMN2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62AgendaAtendimento_CodDmn2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA_TO3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDAFM3", AV60ContagemResultado_DemandaFM3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA3", AV33ContagemResultado_Demanda3);
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_CODDMN3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63AgendaAtendimento_CodDmn3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_DATA", context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_DATA_TO", context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_CODDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_CODDMN_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM", AV52TFContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL", AV53TFContagemResultado_DemandaFM_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDA", AV43TFContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL", AV44TFContagemResultado_Demanda_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CNTSRVALS", StringUtil.RTrim( AV70TFContagemResultado_CntSrvAls));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CNTSRVALS_SEL", StringUtil.RTrim( AV71TFContagemResultado_CntSrvAls_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_QTDUND", StringUtil.LTrim( StringUtil.NToC( AV47TFAgendaAtendimento_QtdUnd, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_QTDUND_TO", StringUtil.LTrim( StringUtil.NToC( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_118", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_118), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAGENDAATENDIMENTO_DATATITLEFILTERDATA", AV36AgendaAtendimento_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAGENDAATENDIMENTO_DATATITLEFILTERDATA", AV36AgendaAtendimento_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA", AV64AgendaAtendimento_CodDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA", AV64AgendaAtendimento_CodDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV51ContagemResultado_DemandaFMTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV51ContagemResultado_DemandaFMTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA", AV42ContagemResultado_DemandaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA", AV42ContagemResultado_DemandaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA", AV69ContagemResultado_CntSrvAlsTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA", AV69ContagemResultado_CntSrvAlsTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA", AV46AgendaAtendimento_QtdUndTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA", AV46AgendaAtendimento_QtdUndTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV113Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Caption", StringUtil.RTrim( Ddo_agendaatendimento_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Tooltip", StringUtil.RTrim( Ddo_agendaatendimento_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Cls", StringUtil.RTrim( Ddo_agendaatendimento_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_agendaatendimento_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_agendaatendimento_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_agendaatendimento_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filtertype", StringUtil.RTrim( Ddo_agendaatendimento_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Sortasc", StringUtil.RTrim( Ddo_agendaatendimento_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Sortdsc", StringUtil.RTrim( Ddo_agendaatendimento_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_agendaatendimento_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_agendaatendimento_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_agendaatendimento_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_agendaatendimento_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Caption", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Tooltip", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Cls", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtext_set", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtextto_set", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Includesortasc", StringUtil.BoolToStr( Ddo_agendaatendimento_coddmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Includesortdsc", StringUtil.BoolToStr( Ddo_agendaatendimento_coddmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Sortedstatus", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Includefilter", StringUtil.BoolToStr( Ddo_agendaatendimento_coddmn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Filtertype", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Filterisrange", StringUtil.BoolToStr( Ddo_agendaatendimento_coddmn_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Includedatalist", StringUtil.BoolToStr( Ddo_agendaatendimento_coddmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Sortasc", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Sortdsc", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Cleanfilter", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Rangefilterfrom", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Rangefilterto", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Searchbuttontext", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Caption", StringUtil.RTrim( Ddo_contagemresultado_demanda_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_demanda_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Cls", StringUtil.RTrim( Ddo_contagemresultado_demanda_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_demanda_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_demanda_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_demanda_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_demanda_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_demanda_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_demanda_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Caption", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Cls", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_cntsrvals_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_cntsrvals_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_cntsrvals_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_cntsrvals_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_cntsrvals_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_cntsrvals_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Caption", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Tooltip", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Cls", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_set", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_set", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Dropdownoptionstype", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Includesortasc", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Includesortdsc", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Sortedstatus", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Includefilter", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Filtertype", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Filterisrange", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Includedatalist", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Sortasc", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Sortdsc", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Cleanfilter", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterfrom", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterto", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Searchbuttontext", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_agendaatendimento_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Activeeventkey", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtext_get", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtextto_get", StringUtil.RTrim( Ddo_agendaatendimento_coddmn_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_demanda_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_demanda_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CNTSRVALS_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_cntsrvals_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Activeeventkey", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_get", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_get", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEID2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTID2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwagendaatendimento.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWAgendaAtendimento" ;
      }

      public override String GetPgmdesc( )
      {
         return " Agenda Atendimento" ;
      }

      protected void WBID0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_ID2( true) ;
         }
         else
         {
            wb_table1_2_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(129, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(130, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfagendaatendimento_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_data_Internalname, context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"), context.localUtil.Format( AV37TFAgendaAtendimento_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavTfagendaatendimento_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfagendaatendimento_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfagendaatendimento_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_data_to_Internalname, context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"), context.localUtil.Format( AV38TFAgendaAtendimento_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavTfagendaatendimento_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfagendaatendimento_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_agendaatendimento_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_agendaatendimento_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_agendaatendimento_dataauxdate_Internalname, context.localUtil.Format(AV39DDO_AgendaAtendimento_DataAuxDate, "99/99/99"), context.localUtil.Format( AV39DDO_AgendaAtendimento_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_agendaatendimento_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_agendaatendimento_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_agendaatendimento_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_agendaatendimento_dataauxdateto_Internalname, context.localUtil.Format(AV40DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV40DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_agendaatendimento_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_agendaatendimento_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_coddmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65TFAgendaAtendimento_CodDmn), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_coddmn_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_coddmn_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_coddmn_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_coddmn_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_coddmn_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_Internalname, AV52TFContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV52TFContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_sel_Internalname, AV53TFContagemResultado_DemandaFM_Sel, StringUtil.RTrim( context.localUtil.Format( AV53TFContagemResultado_DemandaFM_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_sel_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demanda_Internalname, AV43TFContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV43TFContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demanda_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demanda_sel_Internalname, AV44TFContagemResultado_Demanda_Sel, StringUtil.RTrim( context.localUtil.Format( AV44TFContagemResultado_Demanda_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demanda_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demanda_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_cntsrvals_Internalname, StringUtil.RTrim( AV70TFContagemResultado_CntSrvAls), StringUtil.RTrim( context.localUtil.Format( AV70TFContagemResultado_CntSrvAls, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,142);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_cntsrvals_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_cntsrvals_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_cntsrvals_sel_Internalname, StringUtil.RTrim( AV71TFContagemResultado_CntSrvAls_Sel), StringUtil.RTrim( context.localUtil.Format( AV71TFContagemResultado_CntSrvAls_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_cntsrvals_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_cntsrvals_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_qtdund_Internalname, StringUtil.LTrim( StringUtil.NToC( AV47TFAgendaAtendimento_QtdUnd, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV47TFAgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_qtdund_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_qtdund_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_qtdund_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV48TFAgendaAtendimento_QtdUnd_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,145);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_qtdund_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_qtdund_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AGENDAATENDIMENTO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", 0, edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AGENDAATENDIMENTO_CODDMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Internalname, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", 0, edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DEMANDAFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", 0, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DEMANDAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", 0, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_CNTSRVALSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Internalname, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AGENDAATENDIMENTO_QTDUNDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_118_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAgendaAtendimento.htm");
         }
         wbLoad = true;
      }

      protected void STARTID2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Agenda Atendimento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPID0( ) ;
      }

      protected void WSID2( )
      {
         STARTID2( ) ;
         EVTID2( ) ;
      }

      protected void EVTID2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AGENDAATENDIMENTO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11ID2 */
                              E11ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AGENDAATENDIMENTO_CODDMN.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12ID2 */
                              E12ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13ID2 */
                              E13ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DEMANDA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14ID2 */
                              E14ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_CNTSRVALS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15ID2 */
                              E15ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AGENDAATENDIMENTO_QTDUND.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16ID2 */
                              E16ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17ID2 */
                              E17ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18ID2 */
                              E18ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19ID2 */
                              E19ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20ID2 */
                              E20ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21ID2 */
                              E21ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22ID2 */
                              E22ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23ID2 */
                              E23ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24ID2 */
                              E24ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25ID2 */
                              E25ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26ID2 */
                              E26ID2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
                              AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
                              AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
                              AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
                              AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
                              AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
                              AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
                              AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
                              AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
                              AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
                              AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
                              AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
                              AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
                              AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
                              AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
                              AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
                              AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
                              AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
                              AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
                              AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
                              AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
                              AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
                              AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
                              AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
                              AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
                              AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
                              AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
                              AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
                              AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
                              AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
                              AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
                              AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
                              AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
                              AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
                              AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
                              AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_118_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_118_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_118_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1182( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV111Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV112Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1183AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CntSrcCod_Internalname), ",", "."));
                              A1184AgendaAtendimento_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAgendaAtendimento_Data_Internalname), 0));
                              A1209AgendaAtendimento_CodDmn = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CodDmn_Internalname), ",", "."));
                              A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                              n493ContagemResultado_DemandaFM = false;
                              A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
                              n457ContagemResultado_Demanda = false;
                              A2008ContagemResultado_CntSrvAls = StringUtil.Upper( cgiGet( edtContagemResultado_CntSrvAls_Internalname));
                              n2008ContagemResultado_CntSrvAls = false;
                              A1186AgendaAtendimento_QtdUnd = context.localUtil.CToN( cgiGet( edtAgendaAtendimento_QtdUnd_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27ID2 */
                                    E27ID2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28ID2 */
                                    E28ID2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29ID2 */
                                    E29ID2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV55DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA1"), 0) != AV16AgendaAtendimento_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO1"), 0) != AV17AgendaAtendimento_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demandafm1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM1"), AV56ContagemResultado_DemandaFM1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demanda1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA1"), AV31ContagemResultado_Demanda1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_coddmn1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vAGENDAATENDIMENTO_CODDMN1"), ",", ".") != Convert.ToDecimal( AV61AgendaAtendimento_CodDmn1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV57DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA2"), 0) != AV20AgendaAtendimento_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO2"), 0) != AV21AgendaAtendimento_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demandafm2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM2"), AV58ContagemResultado_DemandaFM2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demanda2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA2"), AV32ContagemResultado_Demanda2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_coddmn2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vAGENDAATENDIMENTO_CODDMN2"), ",", ".") != Convert.ToDecimal( AV62AgendaAtendimento_CodDmn2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV59DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_data3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA3"), 0) != AV24AgendaAtendimento_Data3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_data_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO3"), 0) != AV25AgendaAtendimento_Data_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demandafm3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM3"), AV60ContagemResultado_DemandaFM3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demanda3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA3"), AV33ContagemResultado_Demanda3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Agendaatendimento_coddmn3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vAGENDAATENDIMENTO_CODDMN3"), ",", ".") != Convert.ToDecimal( AV63AgendaAtendimento_CodDmn3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfagendaatendimento_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA"), 0) != AV37TFAgendaAtendimento_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfagendaatendimento_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA_TO"), 0) != AV38TFAgendaAtendimento_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfagendaatendimento_coddmn Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CODDMN"), ",", ".") != Convert.ToDecimal( AV65TFAgendaAtendimento_CodDmn )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfagendaatendimento_coddmn_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CODDMN_TO"), ",", ".") != Convert.ToDecimal( AV66TFAgendaAtendimento_CodDmn_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_demandafm Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV52TFContagemResultado_DemandaFM) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_demandafm_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV53TFContagemResultado_DemandaFM_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_demanda Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA"), AV43TFContagemResultado_Demanda) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_demanda_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL"), AV44TFContagemResultado_Demanda_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_cntsrvals Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CNTSRVALS"), AV70TFContagemResultado_CntSrvAls) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_cntsrvals_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CNTSRVALS_SEL"), AV71TFContagemResultado_CntSrvAls_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfagendaatendimento_qtdund Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_QTDUND"), ",", ".") != AV47TFAgendaAtendimento_QtdUnd )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfagendaatendimento_qtdund_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_QTDUND_TO"), ",", ".") != AV48TFAgendaAtendimento_QtdUnd_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEID2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAID2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AGENDAATENDIMENTO_DATA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DEMANDAFM", "N� OS", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DEMANDA", "N� Refer�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("AGENDAATENDIMENTO_CODDMN", "ID", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV55DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AGENDAATENDIMENTO_DATA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DEMANDAFM", "N� OS", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DEMANDA", "N� Refer�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("AGENDAATENDIMENTO_CODDMN", "ID", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV57DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("AGENDAATENDIMENTO_DATA", "Data", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DEMANDAFM", "N� OS", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DEMANDA", "N� Refer�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("AGENDAATENDIMENTO_CODDMN", "ID", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV59DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1182( ) ;
         while ( nGXsfl_118_idx <= nRC_GXsfl_118 )
         {
            sendrow_1182( ) ;
            nGXsfl_118_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_118_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_118_idx+1));
            sGXsfl_118_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_118_idx), 4, 0)), 4, "0");
            SubsflControlProps_1182( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV55DynamicFiltersOperator1 ,
                                       DateTime AV16AgendaAtendimento_Data1 ,
                                       DateTime AV17AgendaAtendimento_Data_To1 ,
                                       String AV56ContagemResultado_DemandaFM1 ,
                                       String AV31ContagemResultado_Demanda1 ,
                                       int AV61AgendaAtendimento_CodDmn1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV57DynamicFiltersOperator2 ,
                                       DateTime AV20AgendaAtendimento_Data2 ,
                                       DateTime AV21AgendaAtendimento_Data_To2 ,
                                       String AV58ContagemResultado_DemandaFM2 ,
                                       String AV32ContagemResultado_Demanda2 ,
                                       int AV62AgendaAtendimento_CodDmn2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV59DynamicFiltersOperator3 ,
                                       DateTime AV24AgendaAtendimento_Data3 ,
                                       DateTime AV25AgendaAtendimento_Data_To3 ,
                                       String AV60ContagemResultado_DemandaFM3 ,
                                       String AV33ContagemResultado_Demanda3 ,
                                       int AV63AgendaAtendimento_CodDmn3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       DateTime AV37TFAgendaAtendimento_Data ,
                                       DateTime AV38TFAgendaAtendimento_Data_To ,
                                       int AV65TFAgendaAtendimento_CodDmn ,
                                       int AV66TFAgendaAtendimento_CodDmn_To ,
                                       String AV52TFContagemResultado_DemandaFM ,
                                       String AV53TFContagemResultado_DemandaFM_Sel ,
                                       String AV43TFContagemResultado_Demanda ,
                                       String AV44TFContagemResultado_Demanda_Sel ,
                                       String AV70TFContagemResultado_CntSrvAls ,
                                       String AV71TFContagemResultado_CntSrvAls_Sel ,
                                       decimal AV47TFAgendaAtendimento_QtdUnd ,
                                       decimal AV48TFAgendaAtendimento_QtdUnd_To ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace ,
                                       String AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace ,
                                       String AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ,
                                       String AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace ,
                                       String AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace ,
                                       String AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace ,
                                       int AV68Contratada_AreaTrabalhoCod ,
                                       String AV113Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1183AgendaAtendimento_CntSrcCod ,
                                       DateTime A1184AgendaAtendimento_Data ,
                                       int A1209AgendaAtendimento_CodDmn )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFID2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_CNTSRCCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AGENDAATENDIMENTO_CNTSRCCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_DATA", GetSecureSignedToken( "", A1184AgendaAtendimento_Data));
         GxWebStd.gx_hidden_field( context, "AGENDAATENDIMENTO_DATA", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_CODDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AGENDAATENDIMENTO_CODDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_QTDUND", GetSecureSignedToken( "", context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "AGENDAATENDIMENTO_QTDUND", StringUtil.LTrim( StringUtil.NToC( A1186AgendaAtendimento_QtdUnd, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV55DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV57DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV59DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFID2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV113Pgmname = "WWAgendaAtendimento";
         context.Gx_err = 0;
      }

      protected void RFID2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 118;
         /* Execute user event: E28ID2 */
         E28ID2 ();
         nGXsfl_118_idx = 1;
         sGXsfl_118_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_118_idx), 4, 0)), 4, "0");
         SubsflControlProps_1182( ) ;
         nGXsfl_118_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1182( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                                 AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                                 AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                                 AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                                 AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                                 AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                                 AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                                 AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                                 AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                                 AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                                 AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                                 AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                                 AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                                 AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                                 AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                                 AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                                 AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                                 AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                                 AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                                 AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                                 AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                                 AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                                 AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                                 AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                                 AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                                 AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                                 AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                                 AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                                 AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                                 AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                                 AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                                 AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                                 AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                                 AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                                 AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                                 A1184AgendaAtendimento_Data ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 A457ContagemResultado_Demanda ,
                                                 A1209AgendaAtendimento_CodDmn ,
                                                 A2008ContagemResultado_CntSrvAls ,
                                                 A1186AgendaAtendimento_QtdUnd ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
            lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
            lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
            lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
            lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
            lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
            lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
            lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
            lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
            lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
            lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
            lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
            lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm), "%", "");
            lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda), "%", "");
            lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = StringUtil.PadR( StringUtil.RTrim( AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals), 15, "%");
            /* Using cursor H00ID2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1, AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1, AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1, AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2, AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2, AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2, AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3, AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3, AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3, AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data, AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to, AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn, AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to, lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm, AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel, lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda, AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel, lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals, AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel, AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund, AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_118_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1553ContagemResultado_CntSrvCod = H00ID2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00ID2_n1553ContagemResultado_CntSrvCod[0];
               A490ContagemResultado_ContratadaCod = H00ID2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00ID2_n490ContagemResultado_ContratadaCod[0];
               A52Contratada_AreaTrabalhoCod = H00ID2_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00ID2_n52Contratada_AreaTrabalhoCod[0];
               A1186AgendaAtendimento_QtdUnd = H00ID2_A1186AgendaAtendimento_QtdUnd[0];
               A2008ContagemResultado_CntSrvAls = H00ID2_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = H00ID2_n2008ContagemResultado_CntSrvAls[0];
               A457ContagemResultado_Demanda = H00ID2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00ID2_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00ID2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00ID2_n493ContagemResultado_DemandaFM[0];
               A1209AgendaAtendimento_CodDmn = H00ID2_A1209AgendaAtendimento_CodDmn[0];
               A1184AgendaAtendimento_Data = H00ID2_A1184AgendaAtendimento_Data[0];
               A1183AgendaAtendimento_CntSrcCod = H00ID2_A1183AgendaAtendimento_CntSrcCod[0];
               A1553ContagemResultado_CntSrvCod = H00ID2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00ID2_n1553ContagemResultado_CntSrvCod[0];
               A490ContagemResultado_ContratadaCod = H00ID2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00ID2_n490ContagemResultado_ContratadaCod[0];
               A457ContagemResultado_Demanda = H00ID2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00ID2_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00ID2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00ID2_n493ContagemResultado_DemandaFM[0];
               A2008ContagemResultado_CntSrvAls = H00ID2_A2008ContagemResultado_CntSrvAls[0];
               n2008ContagemResultado_CntSrvAls = H00ID2_n2008ContagemResultado_CntSrvAls[0];
               A52Contratada_AreaTrabalhoCod = H00ID2_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00ID2_n52Contratada_AreaTrabalhoCod[0];
               /* Execute user event: E29ID2 */
               E29ID2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 118;
            WBID0( ) ;
         }
         nGXsfl_118_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
         AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
         AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
         AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
         AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
         AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
         AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
         AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
         AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
         AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                              AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                              AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                              AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                              AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                              AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                              AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                              AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                              AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                              AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                              AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                              AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                              AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                              AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                              AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                              AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                              AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                              AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                              AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                              AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                              AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                              AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                              AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                              AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                              AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                              AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                              AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                              AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                              AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                              AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                              AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                              AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                              AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                              AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                              AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                              A1184AgendaAtendimento_Data ,
                                              A493ContagemResultado_DemandaFM ,
                                              A457ContagemResultado_Demanda ,
                                              A1209AgendaAtendimento_CodDmn ,
                                              A2008ContagemResultado_CntSrvAls ,
                                              A1186AgendaAtendimento_QtdUnd ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm), "%", "");
         lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda), "%", "");
         lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = StringUtil.PadR( StringUtil.RTrim( AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals), 15, "%");
         /* Using cursor H00ID3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1, AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1, AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1, AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2, AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2, AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2, AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3, AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3, AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3, AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data, AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to, AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn, AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to, lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm, AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel, lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda, AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel, lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals, AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel, AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund, AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to});
         GRID_nRecordCount = H00ID3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
         AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
         AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
         AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
         AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
         AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
         AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
         AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
         AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
         AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
         AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
         AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
         AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
         AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
         AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
         AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
         AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
         AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
         AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
         AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
         AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
         AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
         AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
         AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
         AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
         AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
         AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
         AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
         AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
         AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
         AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
         AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
         AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
         AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
         AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
         AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
         AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
         AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
         AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
         AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
         AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
         AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
         AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
         AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
         AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
         AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         }
         return (int)(0) ;
      }

      protected void STRUPID0( )
      {
         /* Before Start, stand alone formulas. */
         AV113Pgmname = "WWAgendaAtendimento";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27ID2 */
         E27ID2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV50DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vAGENDAATENDIMENTO_DATATITLEFILTERDATA"), AV36AgendaAtendimento_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA"), AV64AgendaAtendimento_CodDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA"), AV51ContagemResultado_DemandaFMTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA"), AV42ContagemResultado_DemandaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA"), AV69ContagemResultado_CntSrvAlsTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA"), AV46AgendaAtendimento_QtdUndTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_AREATRABALHOCOD");
               GX_FocusControl = edtavContratada_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68Contratada_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV68Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV55DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data1"}), 1, "vAGENDAATENDIMENTO_DATA1");
               GX_FocusControl = edtavAgendaatendimento_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16AgendaAtendimento_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
            }
            else
            {
               AV16AgendaAtendimento_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data_To1"}), 1, "vAGENDAATENDIMENTO_DATA_TO1");
               GX_FocusControl = edtavAgendaatendimento_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17AgendaAtendimento_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
            }
            else
            {
               AV17AgendaAtendimento_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
            }
            AV56ContagemResultado_DemandaFM1 = cgiGet( edtavContagemresultado_demandafm1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM1", AV56ContagemResultado_DemandaFM1);
            AV31ContagemResultado_Demanda1 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_Demanda1", AV31ContagemResultado_Demanda1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAGENDAATENDIMENTO_CODDMN1");
               GX_FocusControl = edtavAgendaatendimento_coddmn1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61AgendaAtendimento_CodDmn1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61AgendaAtendimento_CodDmn1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61AgendaAtendimento_CodDmn1), 6, 0)));
            }
            else
            {
               AV61AgendaAtendimento_CodDmn1 = (int)(context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61AgendaAtendimento_CodDmn1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61AgendaAtendimento_CodDmn1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV57DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data2"}), 1, "vAGENDAATENDIMENTO_DATA2");
               GX_FocusControl = edtavAgendaatendimento_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20AgendaAtendimento_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
            }
            else
            {
               AV20AgendaAtendimento_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data_To2"}), 1, "vAGENDAATENDIMENTO_DATA_TO2");
               GX_FocusControl = edtavAgendaatendimento_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21AgendaAtendimento_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
            }
            else
            {
               AV21AgendaAtendimento_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
            }
            AV58ContagemResultado_DemandaFM2 = cgiGet( edtavContagemresultado_demandafm2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DemandaFM2", AV58ContagemResultado_DemandaFM2);
            AV32ContagemResultado_Demanda2 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Demanda2", AV32ContagemResultado_Demanda2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAGENDAATENDIMENTO_CODDMN2");
               GX_FocusControl = edtavAgendaatendimento_coddmn2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62AgendaAtendimento_CodDmn2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62AgendaAtendimento_CodDmn2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62AgendaAtendimento_CodDmn2), 6, 0)));
            }
            else
            {
               AV62AgendaAtendimento_CodDmn2 = (int)(context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62AgendaAtendimento_CodDmn2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62AgendaAtendimento_CodDmn2), 6, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV59DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data3"}), 1, "vAGENDAATENDIMENTO_DATA3");
               GX_FocusControl = edtavAgendaatendimento_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24AgendaAtendimento_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
            }
            else
            {
               AV24AgendaAtendimento_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data_To3"}), 1, "vAGENDAATENDIMENTO_DATA_TO3");
               GX_FocusControl = edtavAgendaatendimento_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25AgendaAtendimento_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
            }
            else
            {
               AV25AgendaAtendimento_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
            }
            AV60ContagemResultado_DemandaFM3 = cgiGet( edtavContagemresultado_demandafm3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_DemandaFM3", AV60ContagemResultado_DemandaFM3);
            AV33ContagemResultado_Demanda3 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda3", AV33ContagemResultado_Demanda3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAGENDAATENDIMENTO_CODDMN3");
               GX_FocusControl = edtavAgendaatendimento_coddmn3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63AgendaAtendimento_CodDmn3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63AgendaAtendimento_CodDmn3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63AgendaAtendimento_CodDmn3), 6, 0)));
            }
            else
            {
               AV63AgendaAtendimento_CodDmn3 = (int)(context.localUtil.CToN( cgiGet( edtavAgendaatendimento_coddmn3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63AgendaAtendimento_CodDmn3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63AgendaAtendimento_CodDmn3), 6, 0)));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfagendaatendimento_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAgenda Atendimento_Data"}), 1, "vTFAGENDAATENDIMENTO_DATA");
               GX_FocusControl = edtavTfagendaatendimento_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFAgendaAtendimento_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data", context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"));
            }
            else
            {
               AV37TFAgendaAtendimento_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfagendaatendimento_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data", context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfagendaatendimento_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAgenda Atendimento_Data_To"}), 1, "vTFAGENDAATENDIMENTO_DATA_TO");
               GX_FocusControl = edtavTfagendaatendimento_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFAgendaAtendimento_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFAgendaAtendimento_Data_To", context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"));
            }
            else
            {
               AV38TFAgendaAtendimento_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfagendaatendimento_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFAgendaAtendimento_Data_To", context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_agendaatendimento_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Agenda Atendimento_Data Aux Date"}), 1, "vDDO_AGENDAATENDIMENTO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_agendaatendimento_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39DDO_AgendaAtendimento_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_AgendaAtendimento_DataAuxDate", context.localUtil.Format(AV39DDO_AgendaAtendimento_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV39DDO_AgendaAtendimento_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_agendaatendimento_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_AgendaAtendimento_DataAuxDate", context.localUtil.Format(AV39DDO_AgendaAtendimento_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_agendaatendimento_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Agenda Atendimento_Data Aux Date To"}), 1, "vDDO_AGENDAATENDIMENTO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_agendaatendimento_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40DDO_AgendaAtendimento_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_AgendaAtendimento_DataAuxDateTo", context.localUtil.Format(AV40DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV40DDO_AgendaAtendimento_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_agendaatendimento_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_AgendaAtendimento_DataAuxDateTo", context.localUtil.Format(AV40DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_coddmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_coddmn_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_CODDMN");
               GX_FocusControl = edtavTfagendaatendimento_coddmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFAgendaAtendimento_CodDmn = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0)));
            }
            else
            {
               AV65TFAgendaAtendimento_CodDmn = (int)(context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_coddmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_coddmn_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_coddmn_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_CODDMN_TO");
               GX_FocusControl = edtavTfagendaatendimento_coddmn_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFAgendaAtendimento_CodDmn_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFAgendaAtendimento_CodDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0)));
            }
            else
            {
               AV66TFAgendaAtendimento_CodDmn_To = (int)(context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_coddmn_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFAgendaAtendimento_CodDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0)));
            }
            AV52TFContagemResultado_DemandaFM = cgiGet( edtavTfcontagemresultado_demandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultado_DemandaFM", AV52TFContagemResultado_DemandaFM);
            AV53TFContagemResultado_DemandaFM_Sel = cgiGet( edtavTfcontagemresultado_demandafm_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultado_DemandaFM_Sel", AV53TFContagemResultado_DemandaFM_Sel);
            AV43TFContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContagemResultado_Demanda", AV43TFContagemResultado_Demanda);
            AV44TFContagemResultado_Demanda_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_demanda_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultado_Demanda_Sel", AV44TFContagemResultado_Demanda_Sel);
            AV70TFContagemResultado_CntSrvAls = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_cntsrvals_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_CntSrvAls", AV70TFContagemResultado_CntSrvAls);
            AV71TFContagemResultado_CntSrvAls_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_cntsrvals_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_CntSrvAls_Sel", AV71TFContagemResultado_CntSrvAls_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_QTDUND");
               GX_FocusControl = edtavTfagendaatendimento_qtdund_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFAgendaAtendimento_QtdUnd = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5)));
            }
            else
            {
               AV47TFAgendaAtendimento_QtdUnd = context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_QTDUND_TO");
               GX_FocusControl = edtavTfagendaatendimento_qtdund_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFAgendaAtendimento_QtdUnd_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5)));
            }
            else
            {
               AV48TFAgendaAtendimento_QtdUnd_To = context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5)));
            }
            AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace = cgiGet( edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace", AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace);
            AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace = cgiGet( edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace", AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace);
            AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
            AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace);
            AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace", AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace);
            AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = cgiGet( edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace", AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_118 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_118"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_agendaatendimento_data_Caption = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Caption");
            Ddo_agendaatendimento_data_Tooltip = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Tooltip");
            Ddo_agendaatendimento_data_Cls = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Cls");
            Ddo_agendaatendimento_data_Filteredtext_set = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_set");
            Ddo_agendaatendimento_data_Filteredtextto_set = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_set");
            Ddo_agendaatendimento_data_Dropdownoptionstype = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Dropdownoptionstype");
            Ddo_agendaatendimento_data_Titlecontrolidtoreplace = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Titlecontrolidtoreplace");
            Ddo_agendaatendimento_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includesortasc"));
            Ddo_agendaatendimento_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includesortdsc"));
            Ddo_agendaatendimento_data_Sortedstatus = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Sortedstatus");
            Ddo_agendaatendimento_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includefilter"));
            Ddo_agendaatendimento_data_Filtertype = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filtertype");
            Ddo_agendaatendimento_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filterisrange"));
            Ddo_agendaatendimento_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includedatalist"));
            Ddo_agendaatendimento_data_Sortasc = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Sortasc");
            Ddo_agendaatendimento_data_Sortdsc = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Sortdsc");
            Ddo_agendaatendimento_data_Cleanfilter = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Cleanfilter");
            Ddo_agendaatendimento_data_Rangefilterfrom = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Rangefilterfrom");
            Ddo_agendaatendimento_data_Rangefilterto = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Rangefilterto");
            Ddo_agendaatendimento_data_Searchbuttontext = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Searchbuttontext");
            Ddo_agendaatendimento_coddmn_Caption = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Caption");
            Ddo_agendaatendimento_coddmn_Tooltip = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Tooltip");
            Ddo_agendaatendimento_coddmn_Cls = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Cls");
            Ddo_agendaatendimento_coddmn_Filteredtext_set = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtext_set");
            Ddo_agendaatendimento_coddmn_Filteredtextto_set = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtextto_set");
            Ddo_agendaatendimento_coddmn_Dropdownoptionstype = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Dropdownoptionstype");
            Ddo_agendaatendimento_coddmn_Titlecontrolidtoreplace = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Titlecontrolidtoreplace");
            Ddo_agendaatendimento_coddmn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Includesortasc"));
            Ddo_agendaatendimento_coddmn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Includesortdsc"));
            Ddo_agendaatendimento_coddmn_Sortedstatus = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Sortedstatus");
            Ddo_agendaatendimento_coddmn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Includefilter"));
            Ddo_agendaatendimento_coddmn_Filtertype = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Filtertype");
            Ddo_agendaatendimento_coddmn_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Filterisrange"));
            Ddo_agendaatendimento_coddmn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Includedatalist"));
            Ddo_agendaatendimento_coddmn_Sortasc = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Sortasc");
            Ddo_agendaatendimento_coddmn_Sortdsc = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Sortdsc");
            Ddo_agendaatendimento_coddmn_Cleanfilter = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Cleanfilter");
            Ddo_agendaatendimento_coddmn_Rangefilterfrom = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Rangefilterfrom");
            Ddo_agendaatendimento_coddmn_Rangefilterto = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Rangefilterto");
            Ddo_agendaatendimento_coddmn_Searchbuttontext = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Searchbuttontext");
            Ddo_contagemresultado_demandafm_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption");
            Ddo_contagemresultado_demandafm_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip");
            Ddo_contagemresultado_demandafm_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls");
            Ddo_contagemresultado_demandafm_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set");
            Ddo_contagemresultado_demandafm_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set");
            Ddo_contagemresultado_demandafm_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype");
            Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_demandafm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc"));
            Ddo_contagemresultado_demandafm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc"));
            Ddo_contagemresultado_demandafm_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortedstatus");
            Ddo_contagemresultado_demandafm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter"));
            Ddo_contagemresultado_demandafm_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype");
            Ddo_contagemresultado_demandafm_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange"));
            Ddo_contagemresultado_demandafm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist"));
            Ddo_contagemresultado_demandafm_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype");
            Ddo_contagemresultado_demandafm_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc");
            Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_demandafm_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortasc");
            Ddo_contagemresultado_demandafm_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortdsc");
            Ddo_contagemresultado_demandafm_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata");
            Ddo_contagemresultado_demandafm_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter");
            Ddo_contagemresultado_demandafm_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound");
            Ddo_contagemresultado_demandafm_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext");
            Ddo_contagemresultado_demanda_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Caption");
            Ddo_contagemresultado_demanda_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Tooltip");
            Ddo_contagemresultado_demanda_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Cls");
            Ddo_contagemresultado_demanda_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_set");
            Ddo_contagemresultado_demanda_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_set");
            Ddo_contagemresultado_demanda_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Dropdownoptionstype");
            Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Titlecontrolidtoreplace");
            Ddo_contagemresultado_demanda_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortasc"));
            Ddo_contagemresultado_demanda_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includesortdsc"));
            Ddo_contagemresultado_demanda_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Sortedstatus");
            Ddo_contagemresultado_demanda_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includefilter"));
            Ddo_contagemresultado_demanda_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filtertype");
            Ddo_contagemresultado_demanda_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filterisrange"));
            Ddo_contagemresultado_demanda_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Includedatalist"));
            Ddo_contagemresultado_demanda_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Datalisttype");
            Ddo_contagemresultado_demanda_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistproc");
            Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_demanda_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Sortasc");
            Ddo_contagemresultado_demanda_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Sortdsc");
            Ddo_contagemresultado_demanda_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Loadingdata");
            Ddo_contagemresultado_demanda_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Cleanfilter");
            Ddo_contagemresultado_demanda_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Noresultsfound");
            Ddo_contagemresultado_demanda_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Searchbuttontext");
            Ddo_contagemresultado_cntsrvals_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Caption");
            Ddo_contagemresultado_cntsrvals_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Tooltip");
            Ddo_contagemresultado_cntsrvals_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Cls");
            Ddo_contagemresultado_cntsrvals_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filteredtext_set");
            Ddo_contagemresultado_cntsrvals_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Selectedvalue_set");
            Ddo_contagemresultado_cntsrvals_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Dropdownoptionstype");
            Ddo_contagemresultado_cntsrvals_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Titlecontrolidtoreplace");
            Ddo_contagemresultado_cntsrvals_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includesortasc"));
            Ddo_contagemresultado_cntsrvals_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includesortdsc"));
            Ddo_contagemresultado_cntsrvals_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Sortedstatus");
            Ddo_contagemresultado_cntsrvals_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includefilter"));
            Ddo_contagemresultado_cntsrvals_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filtertype");
            Ddo_contagemresultado_cntsrvals_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filterisrange"));
            Ddo_contagemresultado_cntsrvals_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Includedatalist"));
            Ddo_contagemresultado_cntsrvals_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Datalisttype");
            Ddo_contagemresultado_cntsrvals_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Datalistproc");
            Ddo_contagemresultado_cntsrvals_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_cntsrvals_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Sortasc");
            Ddo_contagemresultado_cntsrvals_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Sortdsc");
            Ddo_contagemresultado_cntsrvals_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Loadingdata");
            Ddo_contagemresultado_cntsrvals_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Cleanfilter");
            Ddo_contagemresultado_cntsrvals_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Noresultsfound");
            Ddo_contagemresultado_cntsrvals_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Searchbuttontext");
            Ddo_agendaatendimento_qtdund_Caption = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Caption");
            Ddo_agendaatendimento_qtdund_Tooltip = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Tooltip");
            Ddo_agendaatendimento_qtdund_Cls = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Cls");
            Ddo_agendaatendimento_qtdund_Filteredtext_set = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_set");
            Ddo_agendaatendimento_qtdund_Filteredtextto_set = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_set");
            Ddo_agendaatendimento_qtdund_Dropdownoptionstype = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Dropdownoptionstype");
            Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Titlecontrolidtoreplace");
            Ddo_agendaatendimento_qtdund_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Includesortasc"));
            Ddo_agendaatendimento_qtdund_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Includesortdsc"));
            Ddo_agendaatendimento_qtdund_Sortedstatus = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Sortedstatus");
            Ddo_agendaatendimento_qtdund_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Includefilter"));
            Ddo_agendaatendimento_qtdund_Filtertype = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Filtertype");
            Ddo_agendaatendimento_qtdund_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Filterisrange"));
            Ddo_agendaatendimento_qtdund_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Includedatalist"));
            Ddo_agendaatendimento_qtdund_Sortasc = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Sortasc");
            Ddo_agendaatendimento_qtdund_Sortdsc = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Sortdsc");
            Ddo_agendaatendimento_qtdund_Cleanfilter = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Cleanfilter");
            Ddo_agendaatendimento_qtdund_Rangefilterfrom = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterfrom");
            Ddo_agendaatendimento_qtdund_Rangefilterto = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterto");
            Ddo_agendaatendimento_qtdund_Searchbuttontext = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Searchbuttontext");
            Ddo_agendaatendimento_data_Activeeventkey = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Activeeventkey");
            Ddo_agendaatendimento_data_Filteredtext_get = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_get");
            Ddo_agendaatendimento_data_Filteredtextto_get = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_get");
            Ddo_agendaatendimento_coddmn_Activeeventkey = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Activeeventkey");
            Ddo_agendaatendimento_coddmn_Filteredtext_get = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtext_get");
            Ddo_agendaatendimento_coddmn_Filteredtextto_get = cgiGet( "DDO_AGENDAATENDIMENTO_CODDMN_Filteredtextto_get");
            Ddo_contagemresultado_demandafm_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey");
            Ddo_contagemresultado_demandafm_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get");
            Ddo_contagemresultado_demandafm_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get");
            Ddo_contagemresultado_demanda_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Activeeventkey");
            Ddo_contagemresultado_demanda_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_get");
            Ddo_contagemresultado_demanda_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_get");
            Ddo_contagemresultado_cntsrvals_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Activeeventkey");
            Ddo_contagemresultado_cntsrvals_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Filteredtext_get");
            Ddo_contagemresultado_cntsrvals_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_CNTSRVALS_Selectedvalue_get");
            Ddo_agendaatendimento_qtdund_Activeeventkey = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Activeeventkey");
            Ddo_agendaatendimento_qtdund_Filteredtext_get = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_get");
            Ddo_agendaatendimento_qtdund_Filteredtextto_get = cgiGet( "DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV55DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA1"), 0) != AV16AgendaAtendimento_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO1"), 0) != AV17AgendaAtendimento_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM1"), AV56ContagemResultado_DemandaFM1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA1"), AV31ContagemResultado_Demanda1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vAGENDAATENDIMENTO_CODDMN1"), ",", ".") != Convert.ToDecimal( AV61AgendaAtendimento_CodDmn1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV57DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA2"), 0) != AV20AgendaAtendimento_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO2"), 0) != AV21AgendaAtendimento_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM2"), AV58ContagemResultado_DemandaFM2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA2"), AV32ContagemResultado_Demanda2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vAGENDAATENDIMENTO_CODDMN2"), ",", ".") != Convert.ToDecimal( AV62AgendaAtendimento_CodDmn2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV59DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA3"), 0) != AV24AgendaAtendimento_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO3"), 0) != AV25AgendaAtendimento_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM3"), AV60ContagemResultado_DemandaFM3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA3"), AV33ContagemResultado_Demanda3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vAGENDAATENDIMENTO_CODDMN3"), ",", ".") != Convert.ToDecimal( AV63AgendaAtendimento_CodDmn3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA"), 0) != AV37TFAgendaAtendimento_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA_TO"), 0) != AV38TFAgendaAtendimento_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CODDMN"), ",", ".") != Convert.ToDecimal( AV65TFAgendaAtendimento_CodDmn )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CODDMN_TO"), ",", ".") != Convert.ToDecimal( AV66TFAgendaAtendimento_CodDmn_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV52TFContagemResultado_DemandaFM) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV53TFContagemResultado_DemandaFM_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA"), AV43TFContagemResultado_Demanda) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL"), AV44TFContagemResultado_Demanda_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CNTSRVALS"), AV70TFContagemResultado_CntSrvAls) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CNTSRVALS_SEL"), AV71TFContagemResultado_CntSrvAls_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_QTDUND"), ",", ".") != AV47TFAgendaAtendimento_QtdUnd )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_QTDUND_TO"), ",", ".") != AV48TFAgendaAtendimento_QtdUnd_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27ID2 */
         E27ID2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27ID2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfagendaatendimento_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_data_Visible), 5, 0)));
         edtavTfagendaatendimento_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_data_to_Visible), 5, 0)));
         edtavTfagendaatendimento_coddmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_coddmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_coddmn_Visible), 5, 0)));
         edtavTfagendaatendimento_coddmn_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_coddmn_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_coddmn_to_Visible), 5, 0)));
         edtavTfcontagemresultado_demandafm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demandafm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_Visible), 5, 0)));
         edtavTfcontagemresultado_demandafm_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demandafm_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_demanda_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demanda_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demanda_Visible), 5, 0)));
         edtavTfcontagemresultado_demanda_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demanda_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demanda_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_cntsrvals_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_cntsrvals_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_cntsrvals_Visible), 5, 0)));
         edtavTfcontagemresultado_cntsrvals_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_cntsrvals_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_cntsrvals_sel_Visible), 5, 0)));
         edtavTfagendaatendimento_qtdund_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_qtdund_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_qtdund_Visible), 5, 0)));
         edtavTfagendaatendimento_qtdund_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_qtdund_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_qtdund_to_Visible), 5, 0)));
         Ddo_agendaatendimento_data_Titlecontrolidtoreplace = subGrid_Internalname+"_AgendaAtendimento_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "TitleControlIdToReplace", Ddo_agendaatendimento_data_Titlecontrolidtoreplace);
         AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace = Ddo_agendaatendimento_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace", AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace);
         edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_agendaatendimento_coddmn_Titlecontrolidtoreplace = subGrid_Internalname+"_AgendaAtendimento_CodDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "TitleControlIdToReplace", Ddo_agendaatendimento_coddmn_Titlecontrolidtoreplace);
         AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace = Ddo_agendaatendimento_coddmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace", AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace);
         edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DemandaFM";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace);
         AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_Demanda";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_demanda_Titlecontrolidtoreplace);
         AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace = Ddo_contagemresultado_demanda_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace);
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_cntsrvals_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_CntSrvAls";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_cntsrvals_Titlecontrolidtoreplace);
         AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace = Ddo_contagemresultado_cntsrvals_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace", AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace);
         edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace = subGrid_Internalname+"_AgendaAtendimento_QtdUnd";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "TitleControlIdToReplace", Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace);
         AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace", AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace);
         edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Agenda Atendimento";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "ID", 0);
         cmbavOrderedby.addItem("3", "N� OS", 0);
         cmbavOrderedby.addItem("4", "N� Refer�ncia", 0);
         cmbavOrderedby.addItem("5", "Contagem Resultado_Cnt Srv Als", 0);
         cmbavOrderedby.addItem("6", "Unidades", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV50DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV50DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         AV16AgendaAtendimento_Data1 = DateTimeUtil.DAdd(DateTimeUtil.ServerDate( context, "DEFAULT"),-((int)(7)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
         while ( DateTimeUtil.Dow( AV16AgendaAtendimento_Data1) > 2 )
         {
            AV16AgendaAtendimento_Data1 = DateTimeUtil.DAdd(AV16AgendaAtendimento_Data1,-((int)(1)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
         }
         AV17AgendaAtendimento_Data_To1 = DateTimeUtil.DAdd(DateTimeUtil.ServerDate( context, "DEFAULT"),+((int)(1)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
      }

      protected void E28ID2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV36AgendaAtendimento_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64AgendaAtendimento_CodDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContagemResultado_DemandaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContagemResultado_CntSrvAlsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46AgendaAtendimento_QtdUndTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            }
            if ( AV22DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAgendaAtendimento_Data_Titleformat = 2;
         edtAgendaAtendimento_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_Data_Internalname, "Title", edtAgendaAtendimento_Data_Title);
         edtAgendaAtendimento_CodDmn_Titleformat = 2;
         edtAgendaAtendimento_CodDmn_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "ID", AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CodDmn_Internalname, "Title", edtAgendaAtendimento_CodDmn_Title);
         edtContagemResultado_DemandaFM_Titleformat = 2;
         edtContagemResultado_DemandaFM_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� OS", AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DemandaFM_Internalname, "Title", edtContagemResultado_DemandaFM_Title);
         edtContagemResultado_Demanda_Titleformat = 2;
         edtContagemResultado_Demanda_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Refer�ncia", AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Demanda_Internalname, "Title", edtContagemResultado_Demanda_Title);
         edtContagemResultado_CntSrvAls_Titleformat = 2;
         edtContagemResultado_CntSrvAls_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contagem Resultado_Cnt Srv Als", AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_CntSrvAls_Internalname, "Title", edtContagemResultado_CntSrvAls_Title);
         edtAgendaAtendimento_QtdUnd_Titleformat = 2;
         edtAgendaAtendimento_QtdUnd_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidades", AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_QtdUnd_Internalname, "Title", edtAgendaAtendimento_QtdUnd_Title);
         AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV68Contratada_AreaTrabalhoCod;
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV55DynamicFiltersOperator1;
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV16AgendaAtendimento_Data1;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV17AgendaAtendimento_Data_To1;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV56ContagemResultado_DemandaFM1;
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV31ContagemResultado_Demanda1;
         AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV61AgendaAtendimento_CodDmn1;
         AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV57DynamicFiltersOperator2;
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV20AgendaAtendimento_Data2;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV21AgendaAtendimento_Data_To2;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV58ContagemResultado_DemandaFM2;
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV32ContagemResultado_Demanda2;
         AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV62AgendaAtendimento_CodDmn2;
         AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV24AgendaAtendimento_Data3;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV25AgendaAtendimento_Data_To3;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV60ContagemResultado_DemandaFM3;
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV33ContagemResultado_Demanda3;
         AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV63AgendaAtendimento_CodDmn3;
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV37TFAgendaAtendimento_Data;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV38TFAgendaAtendimento_Data_To;
         AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV65TFAgendaAtendimento_CodDmn;
         AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV66TFAgendaAtendimento_CodDmn_To;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV52TFContagemResultado_DemandaFM;
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV53TFContagemResultado_DemandaFM_Sel;
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV43TFContagemResultado_Demanda;
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV44TFContagemResultado_Demanda_Sel;
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV70TFContagemResultado_CntSrvAls;
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV71TFContagemResultado_CntSrvAls_Sel;
         AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV47TFAgendaAtendimento_QtdUnd;
         AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV48TFAgendaAtendimento_QtdUnd_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36AgendaAtendimento_DataTitleFilterData", AV36AgendaAtendimento_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64AgendaAtendimento_CodDmnTitleFilterData", AV64AgendaAtendimento_CodDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51ContagemResultado_DemandaFMTitleFilterData", AV51ContagemResultado_DemandaFMTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42ContagemResultado_DemandaTitleFilterData", AV42ContagemResultado_DemandaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69ContagemResultado_CntSrvAlsTitleFilterData", AV69ContagemResultado_CntSrvAlsTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46AgendaAtendimento_QtdUndTitleFilterData", AV46AgendaAtendimento_QtdUndTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11ID2( )
      {
         /* Ddo_agendaatendimento_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_agendaatendimento_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFAgendaAtendimento_Data = context.localUtil.CToD( Ddo_agendaatendimento_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data", context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"));
            AV38TFAgendaAtendimento_Data_To = context.localUtil.CToD( Ddo_agendaatendimento_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFAgendaAtendimento_Data_To", context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E12ID2( )
      {
         /* Ddo_agendaatendimento_coddmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_agendaatendimento_coddmn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_coddmn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "SortedStatus", Ddo_agendaatendimento_coddmn_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_coddmn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_coddmn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "SortedStatus", Ddo_agendaatendimento_coddmn_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_coddmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFAgendaAtendimento_CodDmn = (int)(NumberUtil.Val( Ddo_agendaatendimento_coddmn_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0)));
            AV66TFAgendaAtendimento_CodDmn_To = (int)(NumberUtil.Val( Ddo_agendaatendimento_coddmn_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFAgendaAtendimento_CodDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13ID2( )
      {
         /* Ddo_contagemresultado_demandafm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demandafm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demandafm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFContagemResultado_DemandaFM = Ddo_contagemresultado_demandafm_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultado_DemandaFM", AV52TFContagemResultado_DemandaFM);
            AV53TFContagemResultado_DemandaFM_Sel = Ddo_contagemresultado_demandafm_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultado_DemandaFM_Sel", AV53TFContagemResultado_DemandaFM_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14ID2( )
      {
         /* Ddo_contagemresultado_demanda_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demanda_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_demanda_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFContagemResultado_Demanda = Ddo_contagemresultado_demanda_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContagemResultado_Demanda", AV43TFContagemResultado_Demanda);
            AV44TFContagemResultado_Demanda_Sel = Ddo_contagemresultado_demanda_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultado_Demanda_Sel", AV44TFContagemResultado_Demanda_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15ID2( )
      {
         /* Ddo_contagemresultado_cntsrvals_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_cntsrvals_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_cntsrvals_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "SortedStatus", Ddo_contagemresultado_cntsrvals_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_cntsrvals_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultado_cntsrvals_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "SortedStatus", Ddo_contagemresultado_cntsrvals_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_cntsrvals_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFContagemResultado_CntSrvAls = Ddo_contagemresultado_cntsrvals_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_CntSrvAls", AV70TFContagemResultado_CntSrvAls);
            AV71TFContagemResultado_CntSrvAls_Sel = Ddo_contagemresultado_cntsrvals_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_CntSrvAls_Sel", AV71TFContagemResultado_CntSrvAls_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16ID2( )
      {
         /* Ddo_agendaatendimento_qtdund_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_agendaatendimento_qtdund_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_qtdund_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_qtdund_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_qtdund_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_qtdund_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFAgendaAtendimento_QtdUnd = NumberUtil.Val( Ddo_agendaatendimento_qtdund_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5)));
            AV48TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( Ddo_agendaatendimento_qtdund_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29ID2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("agendaatendimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1183AgendaAtendimento_CntSrcCod) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A1184AgendaAtendimento_Data)) + "," + UrlEncode("" +A1209AgendaAtendimento_CodDmn);
            AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV111Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV28Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV111Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("agendaatendimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1183AgendaAtendimento_CntSrcCod) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A1184AgendaAtendimento_Data)) + "," + UrlEncode("" +A1209AgendaAtendimento_CodDmn);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV112Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV112Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtAgendaAtendimento_Data_Link = formatLink("viewagendaatendimento.aspx") + "?" + UrlEncode("" +A1183AgendaAtendimento_CntSrcCod) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A1184AgendaAtendimento_Data)) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 118;
         }
         sendrow_1182( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_118_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(118, GridRow);
         }
      }

      protected void E17ID2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22ID2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18ID2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E23ID2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV55DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24ID2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19ID2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E25ID2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV57DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E20ID2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55DynamicFiltersOperator1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV56ContagemResultado_DemandaFM1, AV31ContagemResultado_Demanda1, AV61AgendaAtendimento_CodDmn1, AV19DynamicFiltersSelector2, AV57DynamicFiltersOperator2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV58ContagemResultado_DemandaFM2, AV32ContagemResultado_Demanda2, AV62AgendaAtendimento_CodDmn2, AV23DynamicFiltersSelector3, AV59DynamicFiltersOperator3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV60ContagemResultado_DemandaFM3, AV33ContagemResultado_Demanda3, AV63AgendaAtendimento_CodDmn3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV37TFAgendaAtendimento_Data, AV38TFAgendaAtendimento_Data_To, AV65TFAgendaAtendimento_CodDmn, AV66TFAgendaAtendimento_CodDmn_To, AV52TFContagemResultado_DemandaFM, AV53TFContagemResultado_DemandaFM_Sel, AV43TFContagemResultado_Demanda, AV44TFContagemResultado_Demanda_Sel, AV70TFContagemResultado_CntSrvAls, AV71TFContagemResultado_CntSrvAls_Sel, AV47TFAgendaAtendimento_QtdUnd, AV48TFAgendaAtendimento_QtdUnd_To, AV6WWPContext, AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace, AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace, AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace, AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV68Contratada_AreaTrabalhoCod, AV113Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E26ID2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV59DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E21ID2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_agendaatendimento_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
         Ddo_agendaatendimento_coddmn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "SortedStatus", Ddo_agendaatendimento_coddmn_Sortedstatus);
         Ddo_contagemresultado_demandafm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
         Ddo_contagemresultado_demanda_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
         Ddo_contagemresultado_cntsrvals_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "SortedStatus", Ddo_contagemresultado_cntsrvals_Sortedstatus);
         Ddo_agendaatendimento_qtdund_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_agendaatendimento_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_agendaatendimento_coddmn_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "SortedStatus", Ddo_agendaatendimento_coddmn_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contagemresultado_demandafm_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contagemresultado_demanda_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contagemresultado_cntsrvals_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "SortedStatus", Ddo_contagemresultado_cntsrvals_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_agendaatendimento_qtdund_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfiltersagendaatendimento_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data1_Visible), 5, 0)));
         edtavContagemresultado_demandafm1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm1_Visible), 5, 0)));
         edtavContagemresultado_demanda1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda1_Visible), 5, 0)));
         edtavAgendaatendimento_coddmn1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgendaatendimento_coddmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgendaatendimento_coddmn1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersagendaatendimento_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_CODDMN") == 0 )
         {
            edtavAgendaatendimento_coddmn1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgendaatendimento_coddmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgendaatendimento_coddmn1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfiltersagendaatendimento_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data2_Visible), 5, 0)));
         edtavContagemresultado_demandafm2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm2_Visible), 5, 0)));
         edtavContagemresultado_demanda2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda2_Visible), 5, 0)));
         edtavAgendaatendimento_coddmn2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgendaatendimento_coddmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgendaatendimento_coddmn2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersagendaatendimento_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_CODDMN") == 0 )
         {
            edtavAgendaatendimento_coddmn2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgendaatendimento_coddmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgendaatendimento_coddmn2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfiltersagendaatendimento_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data3_Visible), 5, 0)));
         edtavContagemresultado_demandafm3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm3_Visible), 5, 0)));
         edtavContagemresultado_demanda3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda3_Visible), 5, 0)));
         edtavAgendaatendimento_coddmn3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgendaatendimento_coddmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgendaatendimento_coddmn3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersagendaatendimento_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_CODDMN") == 0 )
         {
            edtavAgendaatendimento_coddmn3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgendaatendimento_coddmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgendaatendimento_coddmn3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20AgendaAtendimento_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
         AV21AgendaAtendimento_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24AgendaAtendimento_Data3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
         AV25AgendaAtendimento_Data_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV68Contratada_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
         AV37TFAgendaAtendimento_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data", context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"));
         Ddo_agendaatendimento_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "FilteredText_set", Ddo_agendaatendimento_data_Filteredtext_set);
         AV38TFAgendaAtendimento_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFAgendaAtendimento_Data_To", context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"));
         Ddo_agendaatendimento_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_data_Filteredtextto_set);
         AV65TFAgendaAtendimento_CodDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0)));
         Ddo_agendaatendimento_coddmn_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "FilteredText_set", Ddo_agendaatendimento_coddmn_Filteredtext_set);
         AV66TFAgendaAtendimento_CodDmn_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFAgendaAtendimento_CodDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0)));
         Ddo_agendaatendimento_coddmn_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_coddmn_Filteredtextto_set);
         AV52TFContagemResultado_DemandaFM = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultado_DemandaFM", AV52TFContagemResultado_DemandaFM);
         Ddo_contagemresultado_demandafm_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "FilteredText_set", Ddo_contagemresultado_demandafm_Filteredtext_set);
         AV53TFContagemResultado_DemandaFM_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultado_DemandaFM_Sel", AV53TFContagemResultado_DemandaFM_Sel);
         Ddo_contagemresultado_demandafm_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SelectedValue_set", Ddo_contagemresultado_demandafm_Selectedvalue_set);
         AV43TFContagemResultado_Demanda = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContagemResultado_Demanda", AV43TFContagemResultado_Demanda);
         Ddo_contagemresultado_demanda_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "FilteredText_set", Ddo_contagemresultado_demanda_Filteredtext_set);
         AV44TFContagemResultado_Demanda_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultado_Demanda_Sel", AV44TFContagemResultado_Demanda_Sel);
         Ddo_contagemresultado_demanda_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SelectedValue_set", Ddo_contagemresultado_demanda_Selectedvalue_set);
         AV70TFContagemResultado_CntSrvAls = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_CntSrvAls", AV70TFContagemResultado_CntSrvAls);
         Ddo_contagemresultado_cntsrvals_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "FilteredText_set", Ddo_contagemresultado_cntsrvals_Filteredtext_set);
         AV71TFContagemResultado_CntSrvAls_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_CntSrvAls_Sel", AV71TFContagemResultado_CntSrvAls_Sel);
         Ddo_contagemresultado_cntsrvals_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "SelectedValue_set", Ddo_contagemresultado_cntsrvals_Selectedvalue_set);
         AV47TFAgendaAtendimento_QtdUnd = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5)));
         Ddo_agendaatendimento_qtdund_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "FilteredText_set", Ddo_agendaatendimento_qtdund_Filteredtext_set);
         AV48TFAgendaAtendimento_QtdUnd_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5)));
         Ddo_agendaatendimento_qtdund_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_qtdund_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16AgendaAtendimento_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
         AV17AgendaAtendimento_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV113Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV113Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV113Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV114GXV1 = 1;
         while ( AV114GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV114GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV68Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_DATA") == 0 )
            {
               AV37TFAgendaAtendimento_Data = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data", context.localUtil.Format(AV37TFAgendaAtendimento_Data, "99/99/99"));
               AV38TFAgendaAtendimento_Data_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFAgendaAtendimento_Data_To", context.localUtil.Format(AV38TFAgendaAtendimento_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV37TFAgendaAtendimento_Data) )
               {
                  Ddo_agendaatendimento_data_Filteredtext_set = context.localUtil.DToC( AV37TFAgendaAtendimento_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "FilteredText_set", Ddo_agendaatendimento_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV38TFAgendaAtendimento_Data_To) )
               {
                  Ddo_agendaatendimento_data_Filteredtextto_set = context.localUtil.DToC( AV38TFAgendaAtendimento_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_CODDMN") == 0 )
            {
               AV65TFAgendaAtendimento_CodDmn = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0)));
               AV66TFAgendaAtendimento_CodDmn_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFAgendaAtendimento_CodDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0)));
               if ( ! (0==AV65TFAgendaAtendimento_CodDmn) )
               {
                  Ddo_agendaatendimento_coddmn_Filteredtext_set = StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "FilteredText_set", Ddo_agendaatendimento_coddmn_Filteredtext_set);
               }
               if ( ! (0==AV66TFAgendaAtendimento_CodDmn_To) )
               {
                  Ddo_agendaatendimento_coddmn_Filteredtextto_set = StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_coddmn_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_coddmn_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV52TFContagemResultado_DemandaFM = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultado_DemandaFM", AV52TFContagemResultado_DemandaFM);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContagemResultado_DemandaFM)) )
               {
                  Ddo_contagemresultado_demandafm_Filteredtext_set = AV52TFContagemResultado_DemandaFM;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "FilteredText_set", Ddo_contagemresultado_demandafm_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV53TFContagemResultado_DemandaFM_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultado_DemandaFM_Sel", AV53TFContagemResultado_DemandaFM_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContagemResultado_DemandaFM_Sel)) )
               {
                  Ddo_contagemresultado_demandafm_Selectedvalue_set = AV53TFContagemResultado_DemandaFM_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SelectedValue_set", Ddo_contagemresultado_demandafm_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV43TFContagemResultado_Demanda = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContagemResultado_Demanda", AV43TFContagemResultado_Demanda);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContagemResultado_Demanda)) )
               {
                  Ddo_contagemresultado_demanda_Filteredtext_set = AV43TFContagemResultado_Demanda;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "FilteredText_set", Ddo_contagemresultado_demanda_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV44TFContagemResultado_Demanda_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultado_Demanda_Sel", AV44TFContagemResultado_Demanda_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContagemResultado_Demanda_Sel)) )
               {
                  Ddo_contagemresultado_demanda_Selectedvalue_set = AV44TFContagemResultado_Demanda_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demanda_Internalname, "SelectedValue_set", Ddo_contagemresultado_demanda_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CNTSRVALS") == 0 )
            {
               AV70TFContagemResultado_CntSrvAls = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultado_CntSrvAls", AV70TFContagemResultado_CntSrvAls);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContagemResultado_CntSrvAls)) )
               {
                  Ddo_contagemresultado_cntsrvals_Filteredtext_set = AV70TFContagemResultado_CntSrvAls;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "FilteredText_set", Ddo_contagemresultado_cntsrvals_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CNTSRVALS_SEL") == 0 )
            {
               AV71TFContagemResultado_CntSrvAls_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_CntSrvAls_Sel", AV71TFContagemResultado_CntSrvAls_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContagemResultado_CntSrvAls_Sel)) )
               {
                  Ddo_contagemresultado_cntsrvals_Selectedvalue_set = AV71TFContagemResultado_CntSrvAls_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_cntsrvals_Internalname, "SelectedValue_set", Ddo_contagemresultado_cntsrvals_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_QTDUND") == 0 )
            {
               AV47TFAgendaAtendimento_QtdUnd = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5)));
               AV48TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV47TFAgendaAtendimento_QtdUnd) )
               {
                  Ddo_agendaatendimento_qtdund_Filteredtext_set = StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "FilteredText_set", Ddo_agendaatendimento_qtdund_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV48TFAgendaAtendimento_QtdUnd_To) )
               {
                  Ddo_agendaatendimento_qtdund_Filteredtextto_set = StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_qtdund_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_qtdund_Filteredtextto_set);
               }
            }
            AV114GXV1 = (int)(AV114GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 )
            {
               AV16AgendaAtendimento_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
               AV17AgendaAtendimento_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV55DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)));
               AV56ContagemResultado_DemandaFM1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM1", AV56ContagemResultado_DemandaFM1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV55DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)));
               AV31ContagemResultado_Demanda1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_Demanda1", AV31ContagemResultado_Demanda1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_CODDMN") == 0 )
            {
               AV61AgendaAtendimento_CodDmn1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61AgendaAtendimento_CodDmn1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61AgendaAtendimento_CodDmn1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 )
               {
                  AV20AgendaAtendimento_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
                  AV21AgendaAtendimento_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  AV57DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)));
                  AV58ContagemResultado_DemandaFM2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DemandaFM2", AV58ContagemResultado_DemandaFM2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
               {
                  AV57DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)));
                  AV32ContagemResultado_Demanda2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Demanda2", AV32ContagemResultado_Demanda2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_CODDMN") == 0 )
               {
                  AV62AgendaAtendimento_CodDmn2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62AgendaAtendimento_CodDmn2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62AgendaAtendimento_CodDmn2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 )
                  {
                     AV24AgendaAtendimento_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
                     AV25AgendaAtendimento_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
                  {
                     AV59DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)));
                     AV60ContagemResultado_DemandaFM3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_DemandaFM3", AV60ContagemResultado_DemandaFM3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                  {
                     AV59DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)));
                     AV33ContagemResultado_Demanda3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda3", AV33ContagemResultado_Demanda3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_CODDMN") == 0 )
                  {
                     AV63AgendaAtendimento_CodDmn3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63AgendaAtendimento_CodDmn3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63AgendaAtendimento_CodDmn3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV113Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV68Contratada_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV37TFAgendaAtendimento_Data) && (DateTime.MinValue==AV38TFAgendaAtendimento_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAGENDAATENDIMENTO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV37TFAgendaAtendimento_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV38TFAgendaAtendimento_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV65TFAgendaAtendimento_CodDmn) && (0==AV66TFAgendaAtendimento_CodDmn_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAGENDAATENDIMENTO_CODDMN";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV65TFAgendaAtendimento_CodDmn), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV66TFAgendaAtendimento_CodDmn_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContagemResultado_DemandaFM)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFContagemResultado_DemandaFM;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContagemResultado_DemandaFM_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFContagemResultado_DemandaFM_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContagemResultado_Demanda)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDA";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFContagemResultado_Demanda;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContagemResultado_Demanda_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFContagemResultado_Demanda_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContagemResultado_CntSrvAls)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CNTSRVALS";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFContagemResultado_CntSrvAls;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContagemResultado_CntSrvAls_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CNTSRVALS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFContagemResultado_CntSrvAls_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV47TFAgendaAtendimento_QtdUnd) && (Convert.ToDecimal(0)==AV48TFAgendaAtendimento_QtdUnd_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAGENDAATENDIMENTO_QTDUND";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV47TFAgendaAtendimento_QtdUnd, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV48TFAgendaAtendimento_QtdUnd_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV113Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16AgendaAtendimento_Data1) && (DateTime.MinValue==AV17AgendaAtendimento_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16AgendaAtendimento_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17AgendaAtendimento_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56ContagemResultado_DemandaFM1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV55DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultado_Demanda1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31ContagemResultado_Demanda1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV55DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ! (0==AV61AgendaAtendimento_CodDmn1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV61AgendaAtendimento_CodDmn1), 6, 0);
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV20AgendaAtendimento_Data2) && (DateTime.MinValue==AV21AgendaAtendimento_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20AgendaAtendimento_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21AgendaAtendimento_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContagemResultado_DemandaFM2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV58ContagemResultado_DemandaFM2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV57DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultado_Demanda2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV32ContagemResultado_Demanda2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV57DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ! (0==AV62AgendaAtendimento_CodDmn2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV62AgendaAtendimento_CodDmn2), 6, 0);
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV24AgendaAtendimento_Data3) && (DateTime.MinValue==AV25AgendaAtendimento_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24AgendaAtendimento_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25AgendaAtendimento_Data_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_DemandaFM3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV60ContagemResultado_DemandaFM3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV59DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultado_Demanda3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33ContagemResultado_Demanda3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV59DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ! (0==AV63AgendaAtendimento_CodDmn3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV63AgendaAtendimento_CodDmn3), 6, 0);
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV113Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "AgendaAtendimento";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_ID2( true) ;
         }
         else
         {
            wb_table2_8_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_115_ID2( true) ;
         }
         else
         {
            wb_table3_115_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table3_115_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_ID2e( true) ;
         }
         else
         {
            wb_table1_2_ID2e( false) ;
         }
      }

      protected void wb_table3_115_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"118\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Agenda Atendimento_Cnt Src Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAgendaAtendimento_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtAgendaAtendimento_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAgendaAtendimento_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAgendaAtendimento_CodDmn_Titleformat == 0 )
               {
                  context.SendWebValue( edtAgendaAtendimento_CodDmn_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAgendaAtendimento_CodDmn_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DemandaFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DemandaFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DemandaFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_Demanda_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_Demanda_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_Demanda_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_CntSrvAls_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_CntSrvAls_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_CntSrvAls_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAgendaAtendimento_QtdUnd_Titleformat == 0 )
               {
                  context.SendWebValue( edtAgendaAtendimento_QtdUnd_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAgendaAtendimento_QtdUnd_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAgendaAtendimento_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAgendaAtendimento_Data_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAgendaAtendimento_Data_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAgendaAtendimento_CodDmn_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAgendaAtendimento_CodDmn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DemandaFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A457ContagemResultado_Demanda);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_Demanda_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Demanda_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2008ContagemResultado_CntSrvAls));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_CntSrvAls_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_CntSrvAls_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1186AgendaAtendimento_QtdUnd, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAgendaAtendimento_QtdUnd_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAgendaAtendimento_QtdUnd_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 118 )
         {
            wbEnd = 0;
            nRC_GXsfl_118 = (short)(nGXsfl_118_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_115_ID2e( true) ;
         }
         else
         {
            wb_table3_115_ID2e( false) ;
         }
      }

      protected void wb_table2_8_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAgendaatendimentotitle_Internalname, "Agenda Atendimento", "", "", lblAgendaatendimentotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_13_ID2( true) ;
         }
         else
         {
            wb_table4_13_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_118_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWAgendaAtendimento.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_22_ID2( true) ;
         }
         else
         {
            wb_table5_22_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table5_22_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_ID2e( true) ;
         }
         else
         {
            wb_table2_8_ID2e( false) ;
         }
      }

      protected void wb_table5_22_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68Contratada_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV68Contratada_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_31_ID2( true) ;
         }
         else
         {
            wb_table6_31_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_22_ID2e( true) ;
         }
         else
         {
            wb_table5_22_ID2e( false) ;
         }
      }

      protected void wb_table6_31_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_118_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_WWAgendaAtendimento.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_40_ID2( true) ;
         }
         else
         {
            wb_table7_40_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table7_40_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_118_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "", true, "HLP_WWAgendaAtendimento.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_67_ID2( true) ;
         }
         else
         {
            wb_table8_67_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table8_67_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_118_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", "", true, "HLP_WWAgendaAtendimento.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_94_ID2( true) ;
         }
         else
         {
            wb_table9_94_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table9_94_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_ID2e( true) ;
         }
         else
         {
            wb_table6_31_ID2e( false) ;
         }
      }

      protected void wb_table9_94_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_118_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", "", true, "HLP_WWAgendaAtendimento.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_99_ID2( true) ;
         }
         else
         {
            wb_table10_99_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table10_99_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm3_Internalname, AV60ContagemResultado_DemandaFM3, StringUtil.RTrim( context.localUtil.Format( AV60ContagemResultado_DemandaFM3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm3_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda3_Internalname, AV33ContagemResultado_Demanda3, StringUtil.RTrim( context.localUtil.Format( AV33ContagemResultado_Demanda3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda3_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_coddmn3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63AgendaAtendimento_CodDmn3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63AgendaAtendimento_CodDmn3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_coddmn3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAgendaatendimento_coddmn3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_94_ID2e( true) ;
         }
         else
         {
            wb_table9_94_ID2e( false) ;
         }
      }

      protected void wb_table10_99_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersagendaatendimento_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data3_Internalname, context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"), context.localUtil.Format( AV24AgendaAtendimento_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data_to3_Internalname, context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"), context.localUtil.Format( AV25AgendaAtendimento_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_99_ID2e( true) ;
         }
         else
         {
            wb_table10_99_ID2e( false) ;
         }
      }

      protected void wb_table8_67_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_118_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_WWAgendaAtendimento.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_72_ID2( true) ;
         }
         else
         {
            wb_table11_72_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table11_72_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm2_Internalname, AV58ContagemResultado_DemandaFM2, StringUtil.RTrim( context.localUtil.Format( AV58ContagemResultado_DemandaFM2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm2_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda2_Internalname, AV32ContagemResultado_Demanda2, StringUtil.RTrim( context.localUtil.Format( AV32ContagemResultado_Demanda2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda2_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_coddmn2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62AgendaAtendimento_CodDmn2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62AgendaAtendimento_CodDmn2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_coddmn2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAgendaatendimento_coddmn2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_67_ID2e( true) ;
         }
         else
         {
            wb_table8_67_ID2e( false) ;
         }
      }

      protected void wb_table11_72_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersagendaatendimento_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data2_Internalname, context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"), context.localUtil.Format( AV20AgendaAtendimento_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data_to2_Internalname, context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"), context.localUtil.Format( AV21AgendaAtendimento_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_72_ID2e( true) ;
         }
         else
         {
            wb_table11_72_ID2e( false) ;
         }
      }

      protected void wb_table7_40_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_118_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_WWAgendaAtendimento.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_45_ID2( true) ;
         }
         else
         {
            wb_table12_45_ID2( false) ;
         }
         return  ;
      }

      protected void wb_table12_45_ID2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm1_Internalname, AV56ContagemResultado_DemandaFM1, StringUtil.RTrim( context.localUtil.Format( AV56ContagemResultado_DemandaFM1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm1_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda1_Internalname, AV31ContagemResultado_Demanda1, StringUtil.RTrim( context.localUtil.Format( AV31ContagemResultado_Demanda1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda1_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_118_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_coddmn1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61AgendaAtendimento_CodDmn1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61AgendaAtendimento_CodDmn1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_coddmn1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAgendaatendimento_coddmn1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_40_ID2e( true) ;
         }
         else
         {
            wb_table7_40_ID2e( false) ;
         }
      }

      protected void wb_table12_45_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersagendaatendimento_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data1_Internalname, context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"), context.localUtil.Format( AV16AgendaAtendimento_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_118_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data_to1_Internalname, context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"), context.localUtil.Format( AV17AgendaAtendimento_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_45_ID2e( true) ;
         }
         else
         {
            wb_table12_45_ID2e( false) ;
         }
      }

      protected void wb_table4_13_ID2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_ID2e( true) ;
         }
         else
         {
            wb_table4_13_ID2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAID2( ) ;
         WSID2( ) ;
         WEID2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221181381");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwagendaatendimento.js", "?202031221181381");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1182( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_118_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_118_idx;
         edtAgendaAtendimento_CntSrcCod_Internalname = "AGENDAATENDIMENTO_CNTSRCCOD_"+sGXsfl_118_idx;
         edtAgendaAtendimento_Data_Internalname = "AGENDAATENDIMENTO_DATA_"+sGXsfl_118_idx;
         edtAgendaAtendimento_CodDmn_Internalname = "AGENDAATENDIMENTO_CODDMN_"+sGXsfl_118_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_118_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_118_idx;
         edtContagemResultado_CntSrvAls_Internalname = "CONTAGEMRESULTADO_CNTSRVALS_"+sGXsfl_118_idx;
         edtAgendaAtendimento_QtdUnd_Internalname = "AGENDAATENDIMENTO_QTDUND_"+sGXsfl_118_idx;
      }

      protected void SubsflControlProps_fel_1182( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_118_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_118_fel_idx;
         edtAgendaAtendimento_CntSrcCod_Internalname = "AGENDAATENDIMENTO_CNTSRCCOD_"+sGXsfl_118_fel_idx;
         edtAgendaAtendimento_Data_Internalname = "AGENDAATENDIMENTO_DATA_"+sGXsfl_118_fel_idx;
         edtAgendaAtendimento_CodDmn_Internalname = "AGENDAATENDIMENTO_CODDMN_"+sGXsfl_118_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_118_fel_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_118_fel_idx;
         edtContagemResultado_CntSrvAls_Internalname = "CONTAGEMRESULTADO_CNTSRVALS_"+sGXsfl_118_fel_idx;
         edtAgendaAtendimento_QtdUnd_Internalname = "AGENDAATENDIMENTO_QTDUND_"+sGXsfl_118_fel_idx;
      }

      protected void sendrow_1182( )
      {
         SubsflControlProps_1182( ) ;
         WBID0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_118_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_118_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_118_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV111Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV111Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV112Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAgendaAtendimento_CntSrcCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAgendaAtendimento_CntSrcCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)118,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAgendaAtendimento_Data_Internalname,context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"),context.localUtil.Format( A1184AgendaAtendimento_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtAgendaAtendimento_Data_Link,(String)"",(String)"",(String)"",(String)edtAgendaAtendimento_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)118,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAgendaAtendimento_CodDmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAgendaAtendimento_CodDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)118,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)118,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)118,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_CntSrvAls_Internalname,StringUtil.RTrim( A2008ContagemResultado_CntSrvAls),StringUtil.RTrim( context.localUtil.Format( A2008ContagemResultado_CntSrvAls, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_CntSrvAls_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)118,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAgendaAtendimento_QtdUnd_Internalname,StringUtil.LTrim( StringUtil.NToC( A1186AgendaAtendimento_QtdUnd, 14, 5, ",", "")),context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAgendaAtendimento_QtdUnd_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)118,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_CNTSRCCOD"+"_"+sGXsfl_118_idx, GetSecureSignedToken( sGXsfl_118_idx, context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_DATA"+"_"+sGXsfl_118_idx, GetSecureSignedToken( sGXsfl_118_idx, A1184AgendaAtendimento_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_CODDMN"+"_"+sGXsfl_118_idx, GetSecureSignedToken( sGXsfl_118_idx, context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_QTDUND"+"_"+sGXsfl_118_idx, GetSecureSignedToken( sGXsfl_118_idx, context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_118_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_118_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_118_idx+1));
            sGXsfl_118_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_118_idx), 4, 0)), 4, "0");
            SubsflControlProps_1182( ) ;
         }
         /* End function sendrow_1182 */
      }

      protected void init_default_properties( )
      {
         lblAgendaatendimentotitle_Internalname = "AGENDAATENDIMENTOTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextcontratada_areatrabalhocod_Internalname = "FILTERTEXTCONTRATADA_AREATRABALHOCOD";
         edtavContratada_areatrabalhocod_Internalname = "vCONTRATADA_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavAgendaatendimento_data1_Internalname = "vAGENDAATENDIMENTO_DATA1";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Internalname = "DYNAMICFILTERSAGENDAATENDIMENTO_DATA_RANGEMIDDLETEXT1";
         edtavAgendaatendimento_data_to1_Internalname = "vAGENDAATENDIMENTO_DATA_TO1";
         tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1";
         edtavContagemresultado_demandafm1_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM1";
         edtavContagemresultado_demanda1_Internalname = "vCONTAGEMRESULTADO_DEMANDA1";
         edtavAgendaatendimento_coddmn1_Internalname = "vAGENDAATENDIMENTO_CODDMN1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavAgendaatendimento_data2_Internalname = "vAGENDAATENDIMENTO_DATA2";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Internalname = "DYNAMICFILTERSAGENDAATENDIMENTO_DATA_RANGEMIDDLETEXT2";
         edtavAgendaatendimento_data_to2_Internalname = "vAGENDAATENDIMENTO_DATA_TO2";
         tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2";
         edtavContagemresultado_demandafm2_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM2";
         edtavContagemresultado_demanda2_Internalname = "vCONTAGEMRESULTADO_DEMANDA2";
         edtavAgendaatendimento_coddmn2_Internalname = "vAGENDAATENDIMENTO_CODDMN2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavAgendaatendimento_data3_Internalname = "vAGENDAATENDIMENTO_DATA3";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Internalname = "DYNAMICFILTERSAGENDAATENDIMENTO_DATA_RANGEMIDDLETEXT3";
         edtavAgendaatendimento_data_to3_Internalname = "vAGENDAATENDIMENTO_DATA_TO3";
         tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3";
         edtavContagemresultado_demandafm3_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM3";
         edtavContagemresultado_demanda3_Internalname = "vCONTAGEMRESULTADO_DEMANDA3";
         edtavAgendaatendimento_coddmn3_Internalname = "vAGENDAATENDIMENTO_CODDMN3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtAgendaAtendimento_CntSrcCod_Internalname = "AGENDAATENDIMENTO_CNTSRCCOD";
         edtAgendaAtendimento_Data_Internalname = "AGENDAATENDIMENTO_DATA";
         edtAgendaAtendimento_CodDmn_Internalname = "AGENDAATENDIMENTO_CODDMN";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA";
         edtContagemResultado_CntSrvAls_Internalname = "CONTAGEMRESULTADO_CNTSRVALS";
         edtAgendaAtendimento_QtdUnd_Internalname = "AGENDAATENDIMENTO_QTDUND";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfagendaatendimento_data_Internalname = "vTFAGENDAATENDIMENTO_DATA";
         edtavTfagendaatendimento_data_to_Internalname = "vTFAGENDAATENDIMENTO_DATA_TO";
         edtavDdo_agendaatendimento_dataauxdate_Internalname = "vDDO_AGENDAATENDIMENTO_DATAAUXDATE";
         edtavDdo_agendaatendimento_dataauxdateto_Internalname = "vDDO_AGENDAATENDIMENTO_DATAAUXDATETO";
         divDdo_agendaatendimento_dataauxdates_Internalname = "DDO_AGENDAATENDIMENTO_DATAAUXDATES";
         edtavTfagendaatendimento_coddmn_Internalname = "vTFAGENDAATENDIMENTO_CODDMN";
         edtavTfagendaatendimento_coddmn_to_Internalname = "vTFAGENDAATENDIMENTO_CODDMN_TO";
         edtavTfcontagemresultado_demandafm_Internalname = "vTFCONTAGEMRESULTADO_DEMANDAFM";
         edtavTfcontagemresultado_demandafm_sel_Internalname = "vTFCONTAGEMRESULTADO_DEMANDAFM_SEL";
         edtavTfcontagemresultado_demanda_Internalname = "vTFCONTAGEMRESULTADO_DEMANDA";
         edtavTfcontagemresultado_demanda_sel_Internalname = "vTFCONTAGEMRESULTADO_DEMANDA_SEL";
         edtavTfcontagemresultado_cntsrvals_Internalname = "vTFCONTAGEMRESULTADO_CNTSRVALS";
         edtavTfcontagemresultado_cntsrvals_sel_Internalname = "vTFCONTAGEMRESULTADO_CNTSRVALS_SEL";
         edtavTfagendaatendimento_qtdund_Internalname = "vTFAGENDAATENDIMENTO_QTDUND";
         edtavTfagendaatendimento_qtdund_to_Internalname = "vTFAGENDAATENDIMENTO_QTDUND_TO";
         Ddo_agendaatendimento_data_Internalname = "DDO_AGENDAATENDIMENTO_DATA";
         edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname = "vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE";
         Ddo_agendaatendimento_coddmn_Internalname = "DDO_AGENDAATENDIMENTO_CODDMN";
         edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Internalname = "vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_demandafm_Internalname = "DDO_CONTAGEMRESULTADO_DEMANDAFM";
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_demanda_Internalname = "DDO_CONTAGEMRESULTADO_DEMANDA";
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_cntsrvals_Internalname = "DDO_CONTAGEMRESULTADO_CNTSRVALS";
         edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE";
         Ddo_agendaatendimento_qtdund_Internalname = "DDO_AGENDAATENDIMENTO_QTDUND";
         edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname = "vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAgendaAtendimento_QtdUnd_Jsonclick = "";
         edtContagemResultado_CntSrvAls_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtAgendaAtendimento_CodDmn_Jsonclick = "";
         edtAgendaAtendimento_Data_Jsonclick = "";
         edtAgendaAtendimento_CntSrcCod_Jsonclick = "";
         edtavAgendaatendimento_data_to1_Jsonclick = "";
         edtavAgendaatendimento_data1_Jsonclick = "";
         edtavAgendaatendimento_coddmn1_Jsonclick = "";
         edtavContagemresultado_demanda1_Jsonclick = "";
         edtavContagemresultado_demandafm1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavAgendaatendimento_data_to2_Jsonclick = "";
         edtavAgendaatendimento_data2_Jsonclick = "";
         edtavAgendaatendimento_coddmn2_Jsonclick = "";
         edtavContagemresultado_demanda2_Jsonclick = "";
         edtavContagemresultado_demandafm2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavAgendaatendimento_data_to3_Jsonclick = "";
         edtavAgendaatendimento_data3_Jsonclick = "";
         edtavAgendaatendimento_coddmn3_Jsonclick = "";
         edtavContagemresultado_demanda3_Jsonclick = "";
         edtavContagemresultado_demandafm3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavContratada_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtAgendaAtendimento_Data_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtAgendaAtendimento_QtdUnd_Titleformat = 0;
         edtContagemResultado_CntSrvAls_Titleformat = 0;
         edtContagemResultado_Demanda_Titleformat = 0;
         edtContagemResultado_DemandaFM_Titleformat = 0;
         edtAgendaAtendimento_CodDmn_Titleformat = 0;
         edtAgendaAtendimento_Data_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavAgendaatendimento_coddmn3_Visible = 1;
         edtavContagemresultado_demanda3_Visible = 1;
         edtavContagemresultado_demandafm3_Visible = 1;
         tblTablemergeddynamicfiltersagendaatendimento_data3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavAgendaatendimento_coddmn2_Visible = 1;
         edtavContagemresultado_demanda2_Visible = 1;
         edtavContagemresultado_demandafm2_Visible = 1;
         tblTablemergeddynamicfiltersagendaatendimento_data2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavAgendaatendimento_coddmn1_Visible = 1;
         edtavContagemresultado_demanda1_Visible = 1;
         edtavContagemresultado_demandafm1_Visible = 1;
         tblTablemergeddynamicfiltersagendaatendimento_data1_Visible = 1;
         edtAgendaAtendimento_QtdUnd_Title = "Unidades";
         edtContagemResultado_CntSrvAls_Title = "Contagem Resultado_Cnt Srv Als";
         edtContagemResultado_Demanda_Title = "N� Refer�ncia";
         edtContagemResultado_DemandaFM_Title = "N� OS";
         edtAgendaAtendimento_CodDmn_Title = "ID";
         edtAgendaAtendimento_Data_Title = "Data";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible = 1;
         edtavTfagendaatendimento_qtdund_to_Jsonclick = "";
         edtavTfagendaatendimento_qtdund_to_Visible = 1;
         edtavTfagendaatendimento_qtdund_Jsonclick = "";
         edtavTfagendaatendimento_qtdund_Visible = 1;
         edtavTfcontagemresultado_cntsrvals_sel_Jsonclick = "";
         edtavTfcontagemresultado_cntsrvals_sel_Visible = 1;
         edtavTfcontagemresultado_cntsrvals_Jsonclick = "";
         edtavTfcontagemresultado_cntsrvals_Visible = 1;
         edtavTfcontagemresultado_demanda_sel_Jsonclick = "";
         edtavTfcontagemresultado_demanda_sel_Visible = 1;
         edtavTfcontagemresultado_demanda_Jsonclick = "";
         edtavTfcontagemresultado_demanda_Visible = 1;
         edtavTfcontagemresultado_demandafm_sel_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_sel_Visible = 1;
         edtavTfcontagemresultado_demandafm_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_Visible = 1;
         edtavTfagendaatendimento_coddmn_to_Jsonclick = "";
         edtavTfagendaatendimento_coddmn_to_Visible = 1;
         edtavTfagendaatendimento_coddmn_Jsonclick = "";
         edtavTfagendaatendimento_coddmn_Visible = 1;
         edtavDdo_agendaatendimento_dataauxdateto_Jsonclick = "";
         edtavDdo_agendaatendimento_dataauxdate_Jsonclick = "";
         edtavTfagendaatendimento_data_to_Jsonclick = "";
         edtavTfagendaatendimento_data_to_Visible = 1;
         edtavTfagendaatendimento_data_Jsonclick = "";
         edtavTfagendaatendimento_data_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_agendaatendimento_qtdund_Searchbuttontext = "Pesquisar";
         Ddo_agendaatendimento_qtdund_Rangefilterto = "At�";
         Ddo_agendaatendimento_qtdund_Rangefilterfrom = "Desde";
         Ddo_agendaatendimento_qtdund_Cleanfilter = "Limpar pesquisa";
         Ddo_agendaatendimento_qtdund_Sortdsc = "Ordenar de Z � A";
         Ddo_agendaatendimento_qtdund_Sortasc = "Ordenar de A � Z";
         Ddo_agendaatendimento_qtdund_Includedatalist = Convert.ToBoolean( 0);
         Ddo_agendaatendimento_qtdund_Filterisrange = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Filtertype = "Numeric";
         Ddo_agendaatendimento_qtdund_Includefilter = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Includesortasc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace = "";
         Ddo_agendaatendimento_qtdund_Dropdownoptionstype = "GridTitleSettings";
         Ddo_agendaatendimento_qtdund_Cls = "ColumnSettings";
         Ddo_agendaatendimento_qtdund_Tooltip = "Op��es";
         Ddo_agendaatendimento_qtdund_Caption = "";
         Ddo_contagemresultado_cntsrvals_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_cntsrvals_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_cntsrvals_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_cntsrvals_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_cntsrvals_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_cntsrvals_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_cntsrvals_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_cntsrvals_Datalistproc = "GetWWAgendaAtendimentoFilterData";
         Ddo_contagemresultado_cntsrvals_Datalisttype = "Dynamic";
         Ddo_contagemresultado_cntsrvals_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_cntsrvals_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_cntsrvals_Filtertype = "Character";
         Ddo_contagemresultado_cntsrvals_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_cntsrvals_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_cntsrvals_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_cntsrvals_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_cntsrvals_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_cntsrvals_Cls = "ColumnSettings";
         Ddo_contagemresultado_cntsrvals_Tooltip = "Op��es";
         Ddo_contagemresultado_cntsrvals_Caption = "";
         Ddo_contagemresultado_demanda_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_demanda_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_demanda_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_demanda_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_demanda_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_demanda_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_demanda_Datalistproc = "GetWWAgendaAtendimentoFilterData";
         Ddo_contagemresultado_demanda_Datalisttype = "Dynamic";
         Ddo_contagemresultado_demanda_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demanda_Filtertype = "Character";
         Ddo_contagemresultado_demanda_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_demanda_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_demanda_Cls = "ColumnSettings";
         Ddo_contagemresultado_demanda_Tooltip = "Op��es";
         Ddo_contagemresultado_demanda_Caption = "";
         Ddo_contagemresultado_demandafm_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_demandafm_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_demandafm_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_demandafm_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_demandafm_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_demandafm_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_demandafm_Datalistproc = "GetWWAgendaAtendimentoFilterData";
         Ddo_contagemresultado_demandafm_Datalisttype = "Dynamic";
         Ddo_contagemresultado_demandafm_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demandafm_Filtertype = "Character";
         Ddo_contagemresultado_demandafm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_demandafm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_demandafm_Cls = "ColumnSettings";
         Ddo_contagemresultado_demandafm_Tooltip = "Op��es";
         Ddo_contagemresultado_demandafm_Caption = "";
         Ddo_agendaatendimento_coddmn_Searchbuttontext = "Pesquisar";
         Ddo_agendaatendimento_coddmn_Rangefilterto = "At�";
         Ddo_agendaatendimento_coddmn_Rangefilterfrom = "Desde";
         Ddo_agendaatendimento_coddmn_Cleanfilter = "Limpar pesquisa";
         Ddo_agendaatendimento_coddmn_Sortdsc = "Ordenar de Z � A";
         Ddo_agendaatendimento_coddmn_Sortasc = "Ordenar de A � Z";
         Ddo_agendaatendimento_coddmn_Includedatalist = Convert.ToBoolean( 0);
         Ddo_agendaatendimento_coddmn_Filterisrange = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_coddmn_Filtertype = "Numeric";
         Ddo_agendaatendimento_coddmn_Includefilter = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_coddmn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_coddmn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_coddmn_Titlecontrolidtoreplace = "";
         Ddo_agendaatendimento_coddmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_agendaatendimento_coddmn_Cls = "ColumnSettings";
         Ddo_agendaatendimento_coddmn_Tooltip = "Op��es";
         Ddo_agendaatendimento_coddmn_Caption = "";
         Ddo_agendaatendimento_data_Searchbuttontext = "Pesquisar";
         Ddo_agendaatendimento_data_Rangefilterto = "At�";
         Ddo_agendaatendimento_data_Rangefilterfrom = "Desde";
         Ddo_agendaatendimento_data_Cleanfilter = "Limpar pesquisa";
         Ddo_agendaatendimento_data_Sortdsc = "Ordenar de Z � A";
         Ddo_agendaatendimento_data_Sortasc = "Ordenar de A � Z";
         Ddo_agendaatendimento_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_agendaatendimento_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Filtertype = "Date";
         Ddo_agendaatendimento_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Titlecontrolidtoreplace = "";
         Ddo_agendaatendimento_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_agendaatendimento_data_Cls = "ColumnSettings";
         Ddo_agendaatendimento_data_Tooltip = "Op��es";
         Ddo_agendaatendimento_data_Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Agenda Atendimento";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36AgendaAtendimento_DataTitleFilterData',fld:'vAGENDAATENDIMENTO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV64AgendaAtendimento_CodDmnTitleFilterData',fld:'vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContagemResultado_DemandaTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContagemResultado_CntSrvAlsTitleFilterData',fld:'vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA',pic:'',nv:null},{av:'AV46AgendaAtendimento_QtdUndTitleFilterData',fld:'vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtAgendaAtendimento_Data_Titleformat',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Titleformat'},{av:'edtAgendaAtendimento_Data_Title',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Title'},{av:'edtAgendaAtendimento_CodDmn_Titleformat',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Titleformat'},{av:'edtAgendaAtendimento_CodDmn_Title',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Title'},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtContagemResultado_Demanda_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'edtContagemResultado_Demanda_Title',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{av:'edtContagemResultado_CntSrvAls_Titleformat',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Titleformat'},{av:'edtContagemResultado_CntSrvAls_Title',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Title'},{av:'edtAgendaAtendimento_QtdUnd_Titleformat',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Titleformat'},{av:'edtAgendaAtendimento_QtdUnd_Title',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("DDO_AGENDAATENDIMENTO_DATA.ONOPTIONCLICKED","{handler:'E11ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_agendaatendimento_data_Activeeventkey',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'ActiveEventKey'},{av:'Ddo_agendaatendimento_data_Filteredtext_get',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredText_get'},{av:'Ddo_agendaatendimento_data_Filteredtextto_get',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'Ddo_agendaatendimento_coddmn_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_cntsrvals_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AGENDAATENDIMENTO_CODDMN.ONOPTIONCLICKED","{handler:'E12ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_agendaatendimento_coddmn_Activeeventkey',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'ActiveEventKey'},{av:'Ddo_agendaatendimento_coddmn_Filteredtext_get',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'FilteredText_get'},{av:'Ddo_agendaatendimento_coddmn_Filteredtextto_get',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_agendaatendimento_coddmn_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'SortedStatus'},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_cntsrvals_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED","{handler:'E13ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultado_demandafm_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_demandafm_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_demandafm_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_coddmn_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_cntsrvals_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DEMANDA.ONOPTIONCLICKED","{handler:'E14ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultado_demanda_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_demanda_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_demanda_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_coddmn_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_cntsrvals_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_CNTSRVALS.ONOPTIONCLICKED","{handler:'E15ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultado_cntsrvals_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_cntsrvals_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_cntsrvals_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_cntsrvals_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SortedStatus'},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_coddmn_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AGENDAATENDIMENTO_QTDUND.ONOPTIONCLICKED","{handler:'E16ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_agendaatendimento_qtdund_Activeeventkey',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'ActiveEventKey'},{av:'Ddo_agendaatendimento_qtdund_Filteredtext_get',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'FilteredText_get'},{av:'Ddo_agendaatendimento_qtdund_Filteredtextto_get',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_coddmn_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_cntsrvals_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29ID2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtAgendaAtendimento_Data_Link',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22ID2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn2_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn3_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn1_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23ID2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn1_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24ID2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn2_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn3_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn1_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25ID2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn2_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn2_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn3_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn1_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26ID2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn3_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21ID2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'Ddo_agendaatendimento_data_Filteredtext_set',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredText_set'},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'Ddo_agendaatendimento_data_Filteredtextto_set',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredTextTo_set'},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'Ddo_agendaatendimento_coddmn_Filteredtext_set',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'FilteredText_set'},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_agendaatendimento_coddmn_Filteredtextto_set',ctrl:'DDO_AGENDAATENDIMENTO_CODDMN',prop:'FilteredTextTo_set'},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'Ddo_contagemresultado_demandafm_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'FilteredText_set'},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_demandafm_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SelectedValue_set'},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demanda_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'FilteredText_set'},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demanda_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SelectedValue_set'},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'Ddo_contagemresultado_cntsrvals_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'FilteredText_set'},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_cntsrvals_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_CNTSRVALS',prop:'SelectedValue_set'},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_agendaatendimento_qtdund_Filteredtext_set',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'FilteredText_set'},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_agendaatendimento_qtdund_Filteredtextto_set',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn1_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn2_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'edtavAgendaatendimento_coddmn3_Visible',ctrl:'vAGENDAATENDIMENTO_CODDMN3',prop:'Visible'}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36AgendaAtendimento_DataTitleFilterData',fld:'vAGENDAATENDIMENTO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV64AgendaAtendimento_CodDmnTitleFilterData',fld:'vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContagemResultado_DemandaTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContagemResultado_CntSrvAlsTitleFilterData',fld:'vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA',pic:'',nv:null},{av:'AV46AgendaAtendimento_QtdUndTitleFilterData',fld:'vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtAgendaAtendimento_Data_Titleformat',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Titleformat'},{av:'edtAgendaAtendimento_Data_Title',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Title'},{av:'edtAgendaAtendimento_CodDmn_Titleformat',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Titleformat'},{av:'edtAgendaAtendimento_CodDmn_Title',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Title'},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtContagemResultado_Demanda_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'edtContagemResultado_Demanda_Title',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{av:'edtContagemResultado_CntSrvAls_Titleformat',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Titleformat'},{av:'edtContagemResultado_CntSrvAls_Title',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Title'},{av:'edtAgendaAtendimento_QtdUnd_Titleformat',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Titleformat'},{av:'edtAgendaAtendimento_QtdUnd_Title',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36AgendaAtendimento_DataTitleFilterData',fld:'vAGENDAATENDIMENTO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV64AgendaAtendimento_CodDmnTitleFilterData',fld:'vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContagemResultado_DemandaTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContagemResultado_CntSrvAlsTitleFilterData',fld:'vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA',pic:'',nv:null},{av:'AV46AgendaAtendimento_QtdUndTitleFilterData',fld:'vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtAgendaAtendimento_Data_Titleformat',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Titleformat'},{av:'edtAgendaAtendimento_Data_Title',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Title'},{av:'edtAgendaAtendimento_CodDmn_Titleformat',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Titleformat'},{av:'edtAgendaAtendimento_CodDmn_Title',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Title'},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtContagemResultado_Demanda_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'edtContagemResultado_Demanda_Title',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{av:'edtContagemResultado_CntSrvAls_Titleformat',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Titleformat'},{av:'edtContagemResultado_CntSrvAls_Title',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Title'},{av:'edtAgendaAtendimento_QtdUnd_Titleformat',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Titleformat'},{av:'edtAgendaAtendimento_QtdUnd_Title',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36AgendaAtendimento_DataTitleFilterData',fld:'vAGENDAATENDIMENTO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV64AgendaAtendimento_CodDmnTitleFilterData',fld:'vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContagemResultado_DemandaTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContagemResultado_CntSrvAlsTitleFilterData',fld:'vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA',pic:'',nv:null},{av:'AV46AgendaAtendimento_QtdUndTitleFilterData',fld:'vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtAgendaAtendimento_Data_Titleformat',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Titleformat'},{av:'edtAgendaAtendimento_Data_Title',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Title'},{av:'edtAgendaAtendimento_CodDmn_Titleformat',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Titleformat'},{av:'edtAgendaAtendimento_CodDmn_Title',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Title'},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtContagemResultado_Demanda_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'edtContagemResultado_Demanda_Title',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{av:'edtContagemResultado_CntSrvAls_Titleformat',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Titleformat'},{av:'edtContagemResultado_CntSrvAls_Title',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Title'},{av:'edtAgendaAtendimento_QtdUnd_Titleformat',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Titleformat'},{av:'edtAgendaAtendimento_QtdUnd_Title',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CODDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CNTSRVALSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV31ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV61AgendaAtendimento_CodDmn1',fld:'vAGENDAATENDIMENTO_CODDMN1',pic:'ZZZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV58ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV32ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV62AgendaAtendimento_CodDmn2',fld:'vAGENDAATENDIMENTO_CODDMN2',pic:'ZZZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV60ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV63AgendaAtendimento_CodDmn3',fld:'vAGENDAATENDIMENTO_CODDMN3',pic:'ZZZZZ9',nv:0},{av:'AV37TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV38TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV65TFAgendaAtendimento_CodDmn',fld:'vTFAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV66TFAgendaAtendimento_CodDmn_To',fld:'vTFAGENDAATENDIMENTO_CODDMN_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV53TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV43TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV44TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV70TFContagemResultado_CntSrvAls',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS',pic:'@!',nv:''},{av:'AV71TFContagemResultado_CntSrvAls_Sel',fld:'vTFCONTAGEMRESULTADO_CNTSRVALS_SEL',pic:'@!',nv:''},{av:'AV47TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV113Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36AgendaAtendimento_DataTitleFilterData',fld:'vAGENDAATENDIMENTO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV64AgendaAtendimento_CodDmnTitleFilterData',fld:'vAGENDAATENDIMENTO_CODDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContagemResultado_DemandaTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContagemResultado_CntSrvAlsTitleFilterData',fld:'vCONTAGEMRESULTADO_CNTSRVALSTITLEFILTERDATA',pic:'',nv:null},{av:'AV46AgendaAtendimento_QtdUndTitleFilterData',fld:'vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV55DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV59DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtAgendaAtendimento_Data_Titleformat',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Titleformat'},{av:'edtAgendaAtendimento_Data_Title',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Title'},{av:'edtAgendaAtendimento_CodDmn_Titleformat',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Titleformat'},{av:'edtAgendaAtendimento_CodDmn_Title',ctrl:'AGENDAATENDIMENTO_CODDMN',prop:'Title'},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtContagemResultado_Demanda_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'edtContagemResultado_Demanda_Title',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{av:'edtContagemResultado_CntSrvAls_Titleformat',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Titleformat'},{av:'edtContagemResultado_CntSrvAls_Title',ctrl:'CONTAGEMRESULTADO_CNTSRVALS',prop:'Title'},{av:'edtAgendaAtendimento_QtdUnd_Titleformat',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Titleformat'},{av:'edtAgendaAtendimento_QtdUnd_Title',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_agendaatendimento_data_Activeeventkey = "";
         Ddo_agendaatendimento_data_Filteredtext_get = "";
         Ddo_agendaatendimento_data_Filteredtextto_get = "";
         Ddo_agendaatendimento_coddmn_Activeeventkey = "";
         Ddo_agendaatendimento_coddmn_Filteredtext_get = "";
         Ddo_agendaatendimento_coddmn_Filteredtextto_get = "";
         Ddo_contagemresultado_demandafm_Activeeventkey = "";
         Ddo_contagemresultado_demandafm_Filteredtext_get = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_get = "";
         Ddo_contagemresultado_demanda_Activeeventkey = "";
         Ddo_contagemresultado_demanda_Filteredtext_get = "";
         Ddo_contagemresultado_demanda_Selectedvalue_get = "";
         Ddo_contagemresultado_cntsrvals_Activeeventkey = "";
         Ddo_contagemresultado_cntsrvals_Filteredtext_get = "";
         Ddo_contagemresultado_cntsrvals_Selectedvalue_get = "";
         Ddo_agendaatendimento_qtdund_Activeeventkey = "";
         Ddo_agendaatendimento_qtdund_Filteredtext_get = "";
         Ddo_agendaatendimento_qtdund_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16AgendaAtendimento_Data1 = DateTime.MinValue;
         AV17AgendaAtendimento_Data_To1 = DateTime.MinValue;
         AV56ContagemResultado_DemandaFM1 = "";
         AV31ContagemResultado_Demanda1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV20AgendaAtendimento_Data2 = DateTime.MinValue;
         AV21AgendaAtendimento_Data_To2 = DateTime.MinValue;
         AV58ContagemResultado_DemandaFM2 = "";
         AV32ContagemResultado_Demanda2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV24AgendaAtendimento_Data3 = DateTime.MinValue;
         AV25AgendaAtendimento_Data_To3 = DateTime.MinValue;
         AV60ContagemResultado_DemandaFM3 = "";
         AV33ContagemResultado_Demanda3 = "";
         AV37TFAgendaAtendimento_Data = DateTime.MinValue;
         AV38TFAgendaAtendimento_Data_To = DateTime.MinValue;
         AV52TFContagemResultado_DemandaFM = "";
         AV53TFContagemResultado_DemandaFM_Sel = "";
         AV43TFContagemResultado_Demanda = "";
         AV44TFContagemResultado_Demanda_Sel = "";
         AV70TFContagemResultado_CntSrvAls = "";
         AV71TFContagemResultado_CntSrvAls_Sel = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace = "";
         AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace = "";
         AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "";
         AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace = "";
         AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace = "";
         AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = "";
         AV113Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV50DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36AgendaAtendimento_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64AgendaAtendimento_CodDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContagemResultado_DemandaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContagemResultado_CntSrvAlsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46AgendaAtendimento_QtdUndTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_agendaatendimento_data_Filteredtext_set = "";
         Ddo_agendaatendimento_data_Filteredtextto_set = "";
         Ddo_agendaatendimento_data_Sortedstatus = "";
         Ddo_agendaatendimento_coddmn_Filteredtext_set = "";
         Ddo_agendaatendimento_coddmn_Filteredtextto_set = "";
         Ddo_agendaatendimento_coddmn_Sortedstatus = "";
         Ddo_contagemresultado_demandafm_Filteredtext_set = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_set = "";
         Ddo_contagemresultado_demandafm_Sortedstatus = "";
         Ddo_contagemresultado_demanda_Filteredtext_set = "";
         Ddo_contagemresultado_demanda_Selectedvalue_set = "";
         Ddo_contagemresultado_demanda_Sortedstatus = "";
         Ddo_contagemresultado_cntsrvals_Filteredtext_set = "";
         Ddo_contagemresultado_cntsrvals_Selectedvalue_set = "";
         Ddo_contagemresultado_cntsrvals_Sortedstatus = "";
         Ddo_agendaatendimento_qtdund_Filteredtext_set = "";
         Ddo_agendaatendimento_qtdund_Filteredtextto_set = "";
         Ddo_agendaatendimento_qtdund_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV39DDO_AgendaAtendimento_DataAuxDate = DateTime.MinValue;
         AV40DDO_AgendaAtendimento_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = "";
         AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = DateTime.MinValue;
         AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = DateTime.MinValue;
         AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = "";
         AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = "";
         AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = "";
         AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = DateTime.MinValue;
         AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = DateTime.MinValue;
         AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = "";
         AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = "";
         AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = "";
         AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = DateTime.MinValue;
         AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = DateTime.MinValue;
         AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = "";
         AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = "";
         AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = DateTime.MinValue;
         AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = DateTime.MinValue;
         AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = "";
         AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = "";
         AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = "";
         AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = "";
         AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = "";
         AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = "";
         AV28Update = "";
         AV111Update_GXI = "";
         AV29Delete = "";
         AV112Delete_GXI = "";
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A2008ContagemResultado_CntSrvAls = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = "";
         lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = "";
         lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = "";
         lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = "";
         lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = "";
         lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = "";
         lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = "";
         lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = "";
         lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = "";
         H00ID2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00ID2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00ID2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00ID2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00ID2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00ID2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00ID2_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         H00ID2_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         H00ID2_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         H00ID2_A457ContagemResultado_Demanda = new String[] {""} ;
         H00ID2_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00ID2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00ID2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00ID2_A1209AgendaAtendimento_CodDmn = new int[1] ;
         H00ID2_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         H00ID2_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         H00ID3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblAgendaatendimentotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextcontratada_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwagendaatendimento__default(),
            new Object[][] {
                new Object[] {
               H00ID2_A1553ContagemResultado_CntSrvCod, H00ID2_n1553ContagemResultado_CntSrvCod, H00ID2_A490ContagemResultado_ContratadaCod, H00ID2_n490ContagemResultado_ContratadaCod, H00ID2_A52Contratada_AreaTrabalhoCod, H00ID2_n52Contratada_AreaTrabalhoCod, H00ID2_A1186AgendaAtendimento_QtdUnd, H00ID2_A2008ContagemResultado_CntSrvAls, H00ID2_n2008ContagemResultado_CntSrvAls, H00ID2_A457ContagemResultado_Demanda,
               H00ID2_n457ContagemResultado_Demanda, H00ID2_A493ContagemResultado_DemandaFM, H00ID2_n493ContagemResultado_DemandaFM, H00ID2_A1209AgendaAtendimento_CodDmn, H00ID2_A1184AgendaAtendimento_Data, H00ID2_A1183AgendaAtendimento_CntSrcCod
               }
               , new Object[] {
               H00ID3_AGRID_nRecordCount
               }
            }
         );
         AV113Pgmname = "WWAgendaAtendimento";
         /* GeneXus formulas. */
         AV113Pgmname = "WWAgendaAtendimento";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_118 ;
      private short nGXsfl_118_idx=1 ;
      private short AV13OrderedBy ;
      private short AV55DynamicFiltersOperator1 ;
      private short AV57DynamicFiltersOperator2 ;
      private short AV59DynamicFiltersOperator3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ;
      private short AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ;
      private short AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_118_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAgendaAtendimento_Data_Titleformat ;
      private short edtAgendaAtendimento_CodDmn_Titleformat ;
      private short edtContagemResultado_DemandaFM_Titleformat ;
      private short edtContagemResultado_Demanda_Titleformat ;
      private short edtContagemResultado_CntSrvAls_Titleformat ;
      private short edtAgendaAtendimento_QtdUnd_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV61AgendaAtendimento_CodDmn1 ;
      private int AV62AgendaAtendimento_CodDmn2 ;
      private int AV63AgendaAtendimento_CodDmn3 ;
      private int AV65TFAgendaAtendimento_CodDmn ;
      private int AV66TFAgendaAtendimento_CodDmn_To ;
      private int AV68Contratada_AreaTrabalhoCod ;
      private int A1183AgendaAtendimento_CntSrcCod ;
      private int A1209AgendaAtendimento_CodDmn ;
      private int Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_cntsrvals_Datalistupdateminimumcharacters ;
      private int edtavTfagendaatendimento_data_Visible ;
      private int edtavTfagendaatendimento_data_to_Visible ;
      private int edtavTfagendaatendimento_coddmn_Visible ;
      private int edtavTfagendaatendimento_coddmn_to_Visible ;
      private int edtavTfcontagemresultado_demandafm_Visible ;
      private int edtavTfcontagemresultado_demandafm_sel_Visible ;
      private int edtavTfcontagemresultado_demanda_Visible ;
      private int edtavTfcontagemresultado_demanda_sel_Visible ;
      private int edtavTfcontagemresultado_cntsrvals_Visible ;
      private int edtavTfcontagemresultado_cntsrvals_sel_Visible ;
      private int edtavTfagendaatendimento_qtdund_Visible ;
      private int edtavTfagendaatendimento_qtdund_to_Visible ;
      private int edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Visible ;
      private int edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible ;
      private int AV75WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod ;
      private int AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ;
      private int AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ;
      private int AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ;
      private int AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ;
      private int AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int edtavOrdereddsc_Visible ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfiltersagendaatendimento_data1_Visible ;
      private int edtavContagemresultado_demandafm1_Visible ;
      private int edtavContagemresultado_demanda1_Visible ;
      private int edtavAgendaatendimento_coddmn1_Visible ;
      private int tblTablemergeddynamicfiltersagendaatendimento_data2_Visible ;
      private int edtavContagemresultado_demandafm2_Visible ;
      private int edtavContagemresultado_demanda2_Visible ;
      private int edtavAgendaatendimento_coddmn2_Visible ;
      private int tblTablemergeddynamicfiltersagendaatendimento_data3_Visible ;
      private int edtavContagemresultado_demandafm3_Visible ;
      private int edtavContagemresultado_demanda3_Visible ;
      private int edtavAgendaatendimento_coddmn3_Visible ;
      private int AV114GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV47TFAgendaAtendimento_QtdUnd ;
      private decimal AV48TFAgendaAtendimento_QtdUnd_To ;
      private decimal AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ;
      private decimal AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ;
      private decimal A1186AgendaAtendimento_QtdUnd ;
      private String Ddo_agendaatendimento_data_Activeeventkey ;
      private String Ddo_agendaatendimento_data_Filteredtext_get ;
      private String Ddo_agendaatendimento_data_Filteredtextto_get ;
      private String Ddo_agendaatendimento_coddmn_Activeeventkey ;
      private String Ddo_agendaatendimento_coddmn_Filteredtext_get ;
      private String Ddo_agendaatendimento_coddmn_Filteredtextto_get ;
      private String Ddo_contagemresultado_demandafm_Activeeventkey ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_get ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_get ;
      private String Ddo_contagemresultado_demanda_Activeeventkey ;
      private String Ddo_contagemresultado_demanda_Filteredtext_get ;
      private String Ddo_contagemresultado_demanda_Selectedvalue_get ;
      private String Ddo_contagemresultado_cntsrvals_Activeeventkey ;
      private String Ddo_contagemresultado_cntsrvals_Filteredtext_get ;
      private String Ddo_contagemresultado_cntsrvals_Selectedvalue_get ;
      private String Ddo_agendaatendimento_qtdund_Activeeventkey ;
      private String Ddo_agendaatendimento_qtdund_Filteredtext_get ;
      private String Ddo_agendaatendimento_qtdund_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_118_idx="0001" ;
      private String AV70TFContagemResultado_CntSrvAls ;
      private String AV71TFContagemResultado_CntSrvAls_Sel ;
      private String AV113Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_agendaatendimento_data_Caption ;
      private String Ddo_agendaatendimento_data_Tooltip ;
      private String Ddo_agendaatendimento_data_Cls ;
      private String Ddo_agendaatendimento_data_Filteredtext_set ;
      private String Ddo_agendaatendimento_data_Filteredtextto_set ;
      private String Ddo_agendaatendimento_data_Dropdownoptionstype ;
      private String Ddo_agendaatendimento_data_Titlecontrolidtoreplace ;
      private String Ddo_agendaatendimento_data_Sortedstatus ;
      private String Ddo_agendaatendimento_data_Filtertype ;
      private String Ddo_agendaatendimento_data_Sortasc ;
      private String Ddo_agendaatendimento_data_Sortdsc ;
      private String Ddo_agendaatendimento_data_Cleanfilter ;
      private String Ddo_agendaatendimento_data_Rangefilterfrom ;
      private String Ddo_agendaatendimento_data_Rangefilterto ;
      private String Ddo_agendaatendimento_data_Searchbuttontext ;
      private String Ddo_agendaatendimento_coddmn_Caption ;
      private String Ddo_agendaatendimento_coddmn_Tooltip ;
      private String Ddo_agendaatendimento_coddmn_Cls ;
      private String Ddo_agendaatendimento_coddmn_Filteredtext_set ;
      private String Ddo_agendaatendimento_coddmn_Filteredtextto_set ;
      private String Ddo_agendaatendimento_coddmn_Dropdownoptionstype ;
      private String Ddo_agendaatendimento_coddmn_Titlecontrolidtoreplace ;
      private String Ddo_agendaatendimento_coddmn_Sortedstatus ;
      private String Ddo_agendaatendimento_coddmn_Filtertype ;
      private String Ddo_agendaatendimento_coddmn_Sortasc ;
      private String Ddo_agendaatendimento_coddmn_Sortdsc ;
      private String Ddo_agendaatendimento_coddmn_Cleanfilter ;
      private String Ddo_agendaatendimento_coddmn_Rangefilterfrom ;
      private String Ddo_agendaatendimento_coddmn_Rangefilterto ;
      private String Ddo_agendaatendimento_coddmn_Searchbuttontext ;
      private String Ddo_contagemresultado_demandafm_Caption ;
      private String Ddo_contagemresultado_demandafm_Tooltip ;
      private String Ddo_contagemresultado_demandafm_Cls ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_set ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_set ;
      private String Ddo_contagemresultado_demandafm_Dropdownoptionstype ;
      private String Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_demandafm_Sortedstatus ;
      private String Ddo_contagemresultado_demandafm_Filtertype ;
      private String Ddo_contagemresultado_demandafm_Datalisttype ;
      private String Ddo_contagemresultado_demandafm_Datalistproc ;
      private String Ddo_contagemresultado_demandafm_Sortasc ;
      private String Ddo_contagemresultado_demandafm_Sortdsc ;
      private String Ddo_contagemresultado_demandafm_Loadingdata ;
      private String Ddo_contagemresultado_demandafm_Cleanfilter ;
      private String Ddo_contagemresultado_demandafm_Noresultsfound ;
      private String Ddo_contagemresultado_demandafm_Searchbuttontext ;
      private String Ddo_contagemresultado_demanda_Caption ;
      private String Ddo_contagemresultado_demanda_Tooltip ;
      private String Ddo_contagemresultado_demanda_Cls ;
      private String Ddo_contagemresultado_demanda_Filteredtext_set ;
      private String Ddo_contagemresultado_demanda_Selectedvalue_set ;
      private String Ddo_contagemresultado_demanda_Dropdownoptionstype ;
      private String Ddo_contagemresultado_demanda_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_demanda_Sortedstatus ;
      private String Ddo_contagemresultado_demanda_Filtertype ;
      private String Ddo_contagemresultado_demanda_Datalisttype ;
      private String Ddo_contagemresultado_demanda_Datalistproc ;
      private String Ddo_contagemresultado_demanda_Sortasc ;
      private String Ddo_contagemresultado_demanda_Sortdsc ;
      private String Ddo_contagemresultado_demanda_Loadingdata ;
      private String Ddo_contagemresultado_demanda_Cleanfilter ;
      private String Ddo_contagemresultado_demanda_Noresultsfound ;
      private String Ddo_contagemresultado_demanda_Searchbuttontext ;
      private String Ddo_contagemresultado_cntsrvals_Caption ;
      private String Ddo_contagemresultado_cntsrvals_Tooltip ;
      private String Ddo_contagemresultado_cntsrvals_Cls ;
      private String Ddo_contagemresultado_cntsrvals_Filteredtext_set ;
      private String Ddo_contagemresultado_cntsrvals_Selectedvalue_set ;
      private String Ddo_contagemresultado_cntsrvals_Dropdownoptionstype ;
      private String Ddo_contagemresultado_cntsrvals_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_cntsrvals_Sortedstatus ;
      private String Ddo_contagemresultado_cntsrvals_Filtertype ;
      private String Ddo_contagemresultado_cntsrvals_Datalisttype ;
      private String Ddo_contagemresultado_cntsrvals_Datalistproc ;
      private String Ddo_contagemresultado_cntsrvals_Sortasc ;
      private String Ddo_contagemresultado_cntsrvals_Sortdsc ;
      private String Ddo_contagemresultado_cntsrvals_Loadingdata ;
      private String Ddo_contagemresultado_cntsrvals_Cleanfilter ;
      private String Ddo_contagemresultado_cntsrvals_Noresultsfound ;
      private String Ddo_contagemresultado_cntsrvals_Searchbuttontext ;
      private String Ddo_agendaatendimento_qtdund_Caption ;
      private String Ddo_agendaatendimento_qtdund_Tooltip ;
      private String Ddo_agendaatendimento_qtdund_Cls ;
      private String Ddo_agendaatendimento_qtdund_Filteredtext_set ;
      private String Ddo_agendaatendimento_qtdund_Filteredtextto_set ;
      private String Ddo_agendaatendimento_qtdund_Dropdownoptionstype ;
      private String Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace ;
      private String Ddo_agendaatendimento_qtdund_Sortedstatus ;
      private String Ddo_agendaatendimento_qtdund_Filtertype ;
      private String Ddo_agendaatendimento_qtdund_Sortasc ;
      private String Ddo_agendaatendimento_qtdund_Sortdsc ;
      private String Ddo_agendaatendimento_qtdund_Cleanfilter ;
      private String Ddo_agendaatendimento_qtdund_Rangefilterfrom ;
      private String Ddo_agendaatendimento_qtdund_Rangefilterto ;
      private String Ddo_agendaatendimento_qtdund_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfagendaatendimento_data_Internalname ;
      private String edtavTfagendaatendimento_data_Jsonclick ;
      private String edtavTfagendaatendimento_data_to_Internalname ;
      private String edtavTfagendaatendimento_data_to_Jsonclick ;
      private String divDdo_agendaatendimento_dataauxdates_Internalname ;
      private String edtavDdo_agendaatendimento_dataauxdate_Internalname ;
      private String edtavDdo_agendaatendimento_dataauxdate_Jsonclick ;
      private String edtavDdo_agendaatendimento_dataauxdateto_Internalname ;
      private String edtavDdo_agendaatendimento_dataauxdateto_Jsonclick ;
      private String edtavTfagendaatendimento_coddmn_Internalname ;
      private String edtavTfagendaatendimento_coddmn_Jsonclick ;
      private String edtavTfagendaatendimento_coddmn_to_Internalname ;
      private String edtavTfagendaatendimento_coddmn_to_Jsonclick ;
      private String edtavTfcontagemresultado_demandafm_Internalname ;
      private String edtavTfcontagemresultado_demandafm_Jsonclick ;
      private String edtavTfcontagemresultado_demandafm_sel_Internalname ;
      private String edtavTfcontagemresultado_demandafm_sel_Jsonclick ;
      private String edtavTfcontagemresultado_demanda_Internalname ;
      private String edtavTfcontagemresultado_demanda_Jsonclick ;
      private String edtavTfcontagemresultado_demanda_sel_Internalname ;
      private String edtavTfcontagemresultado_demanda_sel_Jsonclick ;
      private String edtavTfcontagemresultado_cntsrvals_Internalname ;
      private String edtavTfcontagemresultado_cntsrvals_Jsonclick ;
      private String edtavTfcontagemresultado_cntsrvals_sel_Internalname ;
      private String edtavTfcontagemresultado_cntsrvals_sel_Jsonclick ;
      private String edtavTfagendaatendimento_qtdund_Internalname ;
      private String edtavTfagendaatendimento_qtdund_Jsonclick ;
      private String edtavTfagendaatendimento_qtdund_to_Internalname ;
      private String edtavTfagendaatendimento_qtdund_to_Jsonclick ;
      private String edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_agendaatendimento_coddmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_cntsrvalstitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ;
      private String AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtAgendaAtendimento_CntSrcCod_Internalname ;
      private String edtAgendaAtendimento_Data_Internalname ;
      private String edtAgendaAtendimento_CodDmn_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String A2008ContagemResultado_CntSrvAls ;
      private String edtContagemResultado_CntSrvAls_Internalname ;
      private String edtAgendaAtendimento_QtdUnd_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavContratada_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavAgendaatendimento_data1_Internalname ;
      private String edtavAgendaatendimento_data_to1_Internalname ;
      private String edtavContagemresultado_demandafm1_Internalname ;
      private String edtavContagemresultado_demanda1_Internalname ;
      private String edtavAgendaatendimento_coddmn1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavAgendaatendimento_data2_Internalname ;
      private String edtavAgendaatendimento_data_to2_Internalname ;
      private String edtavContagemresultado_demandafm2_Internalname ;
      private String edtavContagemresultado_demanda2_Internalname ;
      private String edtavAgendaatendimento_coddmn2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavAgendaatendimento_data3_Internalname ;
      private String edtavAgendaatendimento_data_to3_Internalname ;
      private String edtavContagemresultado_demandafm3_Internalname ;
      private String edtavContagemresultado_demanda3_Internalname ;
      private String edtavAgendaatendimento_coddmn3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_agendaatendimento_data_Internalname ;
      private String Ddo_agendaatendimento_coddmn_Internalname ;
      private String Ddo_contagemresultado_demandafm_Internalname ;
      private String Ddo_contagemresultado_demanda_Internalname ;
      private String Ddo_contagemresultado_cntsrvals_Internalname ;
      private String Ddo_agendaatendimento_qtdund_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAgendaAtendimento_Data_Title ;
      private String edtAgendaAtendimento_CodDmn_Title ;
      private String edtContagemResultado_DemandaFM_Title ;
      private String edtContagemResultado_Demanda_Title ;
      private String edtContagemResultado_CntSrvAls_Title ;
      private String edtAgendaAtendimento_QtdUnd_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtAgendaAtendimento_Data_Link ;
      private String tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname ;
      private String tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname ;
      private String tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblAgendaatendimentotitle_Internalname ;
      private String lblAgendaatendimentotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Jsonclick ;
      private String edtavContratada_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultado_demandafm3_Jsonclick ;
      private String edtavContagemresultado_demanda3_Jsonclick ;
      private String edtavAgendaatendimento_coddmn3_Jsonclick ;
      private String edtavAgendaatendimento_data3_Jsonclick ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Jsonclick ;
      private String edtavAgendaatendimento_data_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultado_demandafm2_Jsonclick ;
      private String edtavContagemresultado_demanda2_Jsonclick ;
      private String edtavAgendaatendimento_coddmn2_Jsonclick ;
      private String edtavAgendaatendimento_data2_Jsonclick ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Jsonclick ;
      private String edtavAgendaatendimento_data_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultado_demandafm1_Jsonclick ;
      private String edtavContagemresultado_demanda1_Jsonclick ;
      private String edtavAgendaatendimento_coddmn1_Jsonclick ;
      private String edtavAgendaatendimento_data1_Jsonclick ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Jsonclick ;
      private String edtavAgendaatendimento_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_118_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAgendaAtendimento_CntSrcCod_Jsonclick ;
      private String edtAgendaAtendimento_Data_Jsonclick ;
      private String edtAgendaAtendimento_CodDmn_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemResultado_CntSrvAls_Jsonclick ;
      private String edtAgendaAtendimento_QtdUnd_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16AgendaAtendimento_Data1 ;
      private DateTime AV17AgendaAtendimento_Data_To1 ;
      private DateTime AV20AgendaAtendimento_Data2 ;
      private DateTime AV21AgendaAtendimento_Data_To2 ;
      private DateTime AV24AgendaAtendimento_Data3 ;
      private DateTime AV25AgendaAtendimento_Data_To3 ;
      private DateTime AV37TFAgendaAtendimento_Data ;
      private DateTime AV38TFAgendaAtendimento_Data_To ;
      private DateTime A1184AgendaAtendimento_Data ;
      private DateTime AV39DDO_AgendaAtendimento_DataAuxDate ;
      private DateTime AV40DDO_AgendaAtendimento_DataAuxDateTo ;
      private DateTime AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ;
      private DateTime AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ;
      private DateTime AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ;
      private DateTime AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ;
      private DateTime AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ;
      private DateTime AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ;
      private DateTime AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ;
      private DateTime AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Ddo_agendaatendimento_data_Includesortasc ;
      private bool Ddo_agendaatendimento_data_Includesortdsc ;
      private bool Ddo_agendaatendimento_data_Includefilter ;
      private bool Ddo_agendaatendimento_data_Filterisrange ;
      private bool Ddo_agendaatendimento_data_Includedatalist ;
      private bool Ddo_agendaatendimento_coddmn_Includesortasc ;
      private bool Ddo_agendaatendimento_coddmn_Includesortdsc ;
      private bool Ddo_agendaatendimento_coddmn_Includefilter ;
      private bool Ddo_agendaatendimento_coddmn_Filterisrange ;
      private bool Ddo_agendaatendimento_coddmn_Includedatalist ;
      private bool Ddo_contagemresultado_demandafm_Includesortasc ;
      private bool Ddo_contagemresultado_demandafm_Includesortdsc ;
      private bool Ddo_contagemresultado_demandafm_Includefilter ;
      private bool Ddo_contagemresultado_demandafm_Filterisrange ;
      private bool Ddo_contagemresultado_demandafm_Includedatalist ;
      private bool Ddo_contagemresultado_demanda_Includesortasc ;
      private bool Ddo_contagemresultado_demanda_Includesortdsc ;
      private bool Ddo_contagemresultado_demanda_Includefilter ;
      private bool Ddo_contagemresultado_demanda_Filterisrange ;
      private bool Ddo_contagemresultado_demanda_Includedatalist ;
      private bool Ddo_contagemresultado_cntsrvals_Includesortasc ;
      private bool Ddo_contagemresultado_cntsrvals_Includesortdsc ;
      private bool Ddo_contagemresultado_cntsrvals_Includefilter ;
      private bool Ddo_contagemresultado_cntsrvals_Filterisrange ;
      private bool Ddo_contagemresultado_cntsrvals_Includedatalist ;
      private bool Ddo_agendaatendimento_qtdund_Includesortasc ;
      private bool Ddo_agendaatendimento_qtdund_Includesortdsc ;
      private bool Ddo_agendaatendimento_qtdund_Includefilter ;
      private bool Ddo_agendaatendimento_qtdund_Filterisrange ;
      private bool Ddo_agendaatendimento_qtdund_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ;
      private bool AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n2008ContagemResultado_CntSrvAls ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV56ContagemResultado_DemandaFM1 ;
      private String AV31ContagemResultado_Demanda1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV58ContagemResultado_DemandaFM2 ;
      private String AV32ContagemResultado_Demanda2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV60ContagemResultado_DemandaFM3 ;
      private String AV33ContagemResultado_Demanda3 ;
      private String AV52TFContagemResultado_DemandaFM ;
      private String AV53TFContagemResultado_DemandaFM_Sel ;
      private String AV43TFContagemResultado_Demanda ;
      private String AV44TFContagemResultado_Demanda_Sel ;
      private String AV41ddo_AgendaAtendimento_DataTitleControlIdToReplace ;
      private String AV67ddo_AgendaAtendimento_CodDmnTitleControlIdToReplace ;
      private String AV54ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ;
      private String AV45ddo_ContagemResultado_DemandaTitleControlIdToReplace ;
      private String AV72ddo_ContagemResultado_CntSrvAlsTitleControlIdToReplace ;
      private String AV49ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace ;
      private String AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ;
      private String AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ;
      private String AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ;
      private String AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ;
      private String AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ;
      private String AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ;
      private String AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ;
      private String AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ;
      private String AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ;
      private String AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ;
      private String AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ;
      private String AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ;
      private String AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ;
      private String AV111Update_GXI ;
      private String AV112Delete_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ;
      private String lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ;
      private String lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ;
      private String lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ;
      private String lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ;
      private String lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ;
      private String lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ;
      private String lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00ID2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00ID2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00ID2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00ID2_n490ContagemResultado_ContratadaCod ;
      private int[] H00ID2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00ID2_n52Contratada_AreaTrabalhoCod ;
      private decimal[] H00ID2_A1186AgendaAtendimento_QtdUnd ;
      private String[] H00ID2_A2008ContagemResultado_CntSrvAls ;
      private bool[] H00ID2_n2008ContagemResultado_CntSrvAls ;
      private String[] H00ID2_A457ContagemResultado_Demanda ;
      private bool[] H00ID2_n457ContagemResultado_Demanda ;
      private String[] H00ID2_A493ContagemResultado_DemandaFM ;
      private bool[] H00ID2_n493ContagemResultado_DemandaFM ;
      private int[] H00ID2_A1209AgendaAtendimento_CodDmn ;
      private DateTime[] H00ID2_A1184AgendaAtendimento_Data ;
      private int[] H00ID2_A1183AgendaAtendimento_CntSrcCod ;
      private long[] H00ID3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36AgendaAtendimento_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64AgendaAtendimento_CodDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51ContagemResultado_DemandaFMTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42ContagemResultado_DemandaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69ContagemResultado_CntSrvAlsTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46AgendaAtendimento_QtdUndTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV50DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwagendaatendimento__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00ID2( IGxContext context ,
                                             String AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                             DateTime AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                             short AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                             String AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                             String AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                             int AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                             bool AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                             String AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                             DateTime AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                             DateTime AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                             short AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                             String AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                             String AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                             int AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                             bool AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                             String AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                             DateTime AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                             DateTime AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                             short AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                             String AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                             String AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                             int AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                             DateTime AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                             DateTime AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                             int AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                             int AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                             String AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                             String AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                             String AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                             String AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                             String AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                             String AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                             decimal AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                             decimal AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A457ContagemResultado_Demanda ,
                                             int A1209AgendaAtendimento_CodDmn ,
                                             String A2008ContagemResultado_CntSrvAls ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [45] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Contratada_AreaTrabalhoCod], T1.[AgendaAtendimento_QtdUnd], T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CntSrcCod]";
         sFromString = " FROM ((([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T4.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data)";
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to)";
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ! (0==AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] >= @AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn)";
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! (0==AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] <= @AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to)";
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] like @lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)";
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] = @AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)";
         }
         else
         {
            GXv_int2[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund)";
         }
         else
         {
            GXv_int2[38] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to)";
         }
         else
         {
            GXv_int2[39] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CodDmn]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CodDmn] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_DemandaFM]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_DemandaFM] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_Demanda]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_Demanda] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[ContratoServicos_Alias]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[ContratoServicos_Alias] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_QtdUnd]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_QtdUnd] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CodDmn]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00ID3( IGxContext context ,
                                             String AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                             DateTime AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                             short AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                             String AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                             String AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                             int AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                             bool AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                             String AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                             DateTime AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                             DateTime AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                             short AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                             String AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                             String AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                             int AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                             bool AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                             String AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                             DateTime AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                             DateTime AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                             short AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                             String AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                             String AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                             int AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                             DateTime AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                             DateTime AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                             int AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                             int AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                             String AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                             String AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                             String AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                             String AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                             String AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                             String AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                             decimal AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                             decimal AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A457ContagemResultado_Demanda ,
                                             int A1209AgendaAtendimento_CodDmn ,
                                             String A2008ContagemResultado_CntSrvAls ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [40] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T4.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV77WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV85WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV83WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV93WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( AV91WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV92WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3)";
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data)";
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to)";
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! (0==AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] >= @AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn)";
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! (0==AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] <= @AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to)";
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] like @lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)";
         }
         else
         {
            GXv_int4[36] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] = @AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)";
         }
         else
         {
            GXv_int4[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund)";
         }
         else
         {
            GXv_int4[38] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to)";
         }
         else
         {
            GXv_int4[39] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00ID2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (DateTime)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (short)dynConstraints[41] , (bool)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] );
               case 1 :
                     return conditional_H00ID3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (DateTime)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (short)dynConstraints[41] , (bool)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00ID2 ;
          prmH00ID2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn",SqlDbType.Int,6,0} ,
          new Object[] {"@AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals",SqlDbType.Char,15,0} ,
          new Object[] {"@AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00ID3 ;
          prmH00ID3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78WWAgendaAtendimentoDS_4_Agendaatendimento_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV79WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV82WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWAgendaAtendimentoDS_12_Agendaatendimento_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV88WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV90WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV94WWAgendaAtendimentoDS_20_Agendaatendimento_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV96WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV97WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV98WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV99WWAgendaAtendimentoDS_25_Tfagendaatendimento_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV100WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn",SqlDbType.Int,6,0} ,
          new Object[] {"@AV102WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV103WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV104WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV105WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV106WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV107WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals",SqlDbType.Char,15,0} ,
          new Object[] {"@AV108WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV109WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV110WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00ID2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ID2,11,0,true,false )
             ,new CursorDef("H00ID3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ID3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[83]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[84]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[85]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[86]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[87]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[88]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[89]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[78]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[79]);
                }
                return;
       }
    }

 }

}
