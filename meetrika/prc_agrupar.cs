/*
               File: PRC_Agrupar
        Description: Agrupar demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:44.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_agrupar : GXProcedure
   {
      public prc_agrupar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_agrupar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Agrupador )
      {
         this.AV10Agrupador = aP0_Agrupador;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Agrupador )
      {
         prc_agrupar objprc_agrupar;
         objprc_agrupar = new prc_agrupar();
         objprc_agrupar.AV10Agrupador = aP0_Agrupador;
         objprc_agrupar.context.SetSubmitInitialConfig(context);
         objprc_agrupar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_agrupar);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_agrupar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Codigos.FromXml(AV9WebSession.Get("Codigos"), "Collection");
         AV9WebSession.Remove("Codigos");
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A602ContagemResultado_OSVinculada ,
                                              AV8Codigos },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00782 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A602ContagemResultado_OSVinculada = P00782_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00782_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = P00782_A456ContagemResultado_Codigo[0];
            AV8Codigos.Add(A456ContagemResultado_Codigo, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV8Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00783 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A456ContagemResultado_Codigo = P00783_A456ContagemResultado_Codigo[0];
            A1046ContagemResultado_Agrupador = P00783_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00783_n1046ContagemResultado_Agrupador[0];
            A1046ContagemResultado_Agrupador = AV10Agrupador;
            n1046ContagemResultado_Agrupador = false;
            BatchSize = 100;
            pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp00784");
            /* Using cursor P00784 */
            pr_default.addRecord(2, new Object[] {n1046ContagemResultado_Agrupador, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
            {
               Executebatchp00784( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(1);
         }
         if ( pr_default.getBatchSize(2) > 0 )
         {
            Executebatchp00784( ) ;
         }
         pr_default.close(1);
         this.cleanup();
      }

      protected void Executebatchp00784( )
      {
         /* Using cursor P00784 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_Agrupar");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         AV8Codigos = new GxSimpleCollection();
         AV9WebSession = context.GetSession();
         scmdbuf = "";
         P00782_A602ContagemResultado_OSVinculada = new int[1] ;
         P00782_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00782_A456ContagemResultado_Codigo = new int[1] ;
         P00783_A456ContagemResultado_Codigo = new int[1] ;
         P00783_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00783_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         A1046ContagemResultado_Agrupador = "";
         P00784_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00784_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00784_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_agrupar__default(),
            new Object[][] {
                new Object[] {
               P00782_A602ContagemResultado_OSVinculada, P00782_n602ContagemResultado_OSVinculada, P00782_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00783_A456ContagemResultado_Codigo, P00783_A1046ContagemResultado_Agrupador, P00783_n1046ContagemResultado_Agrupador
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A602ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int BatchSize ;
      private String AV10Agrupador ;
      private String scmdbuf ;
      private String A1046ContagemResultado_Agrupador ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1046ContagemResultado_Agrupador ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00782_A602ContagemResultado_OSVinculada ;
      private bool[] P00782_n602ContagemResultado_OSVinculada ;
      private int[] P00782_A456ContagemResultado_Codigo ;
      private int[] P00783_A456ContagemResultado_Codigo ;
      private String[] P00783_A1046ContagemResultado_Agrupador ;
      private bool[] P00783_n1046ContagemResultado_Agrupador ;
      private String[] P00784_A1046ContagemResultado_Agrupador ;
      private bool[] P00784_n1046ContagemResultado_Agrupador ;
      private int[] P00784_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Codigos ;
   }

   public class prc_agrupar__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00782( IGxContext context ,
                                             int A602ContagemResultado_OSVinculada ,
                                             IGxCollection AV8Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_OSVinculada], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV8Codigos, "[ContagemResultado_OSVinculada] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      protected Object[] conditional_P00783( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV8Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Codigo], [ContagemResultado_Agrupador] FROM [ContagemResultado] WITH (UPDLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV8Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00782(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 1 :
                     return conditional_P00783(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00784 ;
          prmP00784 = new Object[] {
          new Object[] {"@ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00782 ;
          prmP00782 = new Object[] {
          } ;
          Object[] prmP00783 ;
          prmP00783 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00782", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00782,100,0,false,false )
             ,new CursorDef("P00783", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00783,1,0,true,false )
             ,new CursorDef("P00784", "UPDATE [ContagemResultado] SET [ContagemResultado_Agrupador]=@ContagemResultado_Agrupador  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00784)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
