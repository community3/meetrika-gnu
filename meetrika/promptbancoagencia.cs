/*
               File: PromptBancoAgencia
        Description: Selecione Banco Agencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:31:46.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptbancoagencia : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptbancoagencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptbancoagencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutBancoAgencia_Codigo ,
                           ref String aP1_InOutBancoAgencia_NomeAgencia ,
                           ref String aP2_InOutBancoAgencia_Agencia ,
                           ref String aP3_InOutBanco_Nome ,
                           ref String aP4_InOutMunicipio_Nome )
      {
         this.AV7InOutBancoAgencia_Codigo = aP0_InOutBancoAgencia_Codigo;
         this.AV36InOutBancoAgencia_NomeAgencia = aP1_InOutBancoAgencia_NomeAgencia;
         this.AV8InOutBancoAgencia_Agencia = aP2_InOutBancoAgencia_Agencia;
         this.AV37InOutBanco_Nome = aP3_InOutBanco_Nome;
         this.AV38InOutMunicipio_Nome = aP4_InOutMunicipio_Nome;
         executePrivate();
         aP0_InOutBancoAgencia_Codigo=this.AV7InOutBancoAgencia_Codigo;
         aP1_InOutBancoAgencia_NomeAgencia=this.AV36InOutBancoAgencia_NomeAgencia;
         aP2_InOutBancoAgencia_Agencia=this.AV8InOutBancoAgencia_Agencia;
         aP3_InOutBanco_Nome=this.AV37InOutBanco_Nome;
         aP4_InOutMunicipio_Nome=this.AV38InOutMunicipio_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_98 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_98_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_98_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17BancoAgencia_Agencia1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
               AV18Banco_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
               AV19Municipio_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23BancoAgencia_Agencia2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
               AV24Banco_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
               AV25Municipio_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
               AV27DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
               AV29BancoAgencia_Agencia3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
               AV30Banco_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
               AV31Municipio_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutBancoAgencia_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutBancoAgencia_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV36InOutBancoAgencia_NomeAgencia = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36InOutBancoAgencia_NomeAgencia", AV36InOutBancoAgencia_NomeAgencia);
                  AV8InOutBancoAgencia_Agencia = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutBancoAgencia_Agencia", AV8InOutBancoAgencia_Agencia);
                  AV37InOutBanco_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37InOutBanco_Nome", AV37InOutBanco_Nome);
                  AV38InOutMunicipio_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38InOutMunicipio_Nome", AV38InOutMunicipio_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA1N2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS1N2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE1N2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117314641");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptbancoagencia.aspx") + "?" + UrlEncode("" +AV7InOutBancoAgencia_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV36InOutBancoAgencia_NomeAgencia)) + "," + UrlEncode(StringUtil.RTrim(AV8InOutBancoAgencia_Agencia)) + "," + UrlEncode(StringUtil.RTrim(AV37InOutBanco_Nome)) + "," + UrlEncode(StringUtil.RTrim(AV38InOutMunicipio_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCOAGENCIA_AGENCIA1", StringUtil.RTrim( AV17BancoAgencia_Agencia1));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCO_NOME1", StringUtil.RTrim( AV18Banco_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME1", StringUtil.RTrim( AV19Municipio_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCOAGENCIA_AGENCIA2", StringUtil.RTrim( AV23BancoAgencia_Agencia2));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCO_NOME2", StringUtil.RTrim( AV24Banco_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME2", StringUtil.RTrim( AV25Municipio_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCOAGENCIA_AGENCIA3", StringUtil.RTrim( AV29BancoAgencia_Agencia3));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCO_NOME3", StringUtil.RTrim( AV30Banco_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME3", StringUtil.RTrim( AV31Municipio_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_98", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_98), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTBANCOAGENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutBancoAgencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTBANCOAGENCIA_NOMEAGENCIA", StringUtil.RTrim( AV36InOutBancoAgencia_NomeAgencia));
         GxWebStd.gx_hidden_field( context, "vINOUTBANCOAGENCIA_AGENCIA", StringUtil.RTrim( AV8InOutBancoAgencia_Agencia));
         GxWebStd.gx_hidden_field( context, "vINOUTBANCO_NOME", StringUtil.RTrim( AV37InOutBanco_Nome));
         GxWebStd.gx_hidden_field( context, "vINOUTMUNICIPIO_NOME", StringUtil.RTrim( AV38InOutMunicipio_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm1N2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptBancoAgencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Banco Agencia" ;
      }

      protected void WB1N0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_1N2( true) ;
         }
         else
         {
            wb_table1_2_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_98_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(111, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_98_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(112, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"");
         }
         wbLoad = true;
      }

      protected void START1N2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Banco Agencia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1N0( ) ;
      }

      protected void WS1N2( )
      {
         START1N2( ) ;
         EVT1N2( ) ;
      }

      protected void EVT1N2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111N2 */
                           E111N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121N2 */
                           E121N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E131N2 */
                           E131N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E141N2 */
                           E141N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E151N2 */
                           E151N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E161N2 */
                           E161N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOBTNNOVOBANCOAGENCIA'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E171N2 */
                           E171N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E181N2 */
                           E181N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E191N2 */
                           E191N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E201N2 */
                           E201N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E211N2 */
                           E211N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E221N2 */
                           E221N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_98_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
                           SubsflControlProps_982( ) ;
                           AV34Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV94Select_GXI : context.convertURL( context.PathToRelativeUrl( AV34Select))));
                           A17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBancoAgencia_Codigo_Internalname), ",", "."));
                           A27BancoAgencia_Agencia = cgiGet( edtBancoAgencia_Agencia_Internalname);
                           A28BancoAgencia_NomeAgencia = StringUtil.Upper( cgiGet( edtBancoAgencia_NomeAgencia_Internalname));
                           A18Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBanco_Codigo_Internalname), ",", "."));
                           A20Banco_Nome = StringUtil.Upper( cgiGet( edtBanco_Nome_Internalname));
                           A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
                           A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E231N2 */
                                 E231N2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E241N2 */
                                 E241N2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E251N2 */
                                 E251N2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Bancoagencia_agencia1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA1"), AV17BancoAgencia_Agencia1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Banco_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME1"), AV18Banco_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Municipio_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME1"), AV19Municipio_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Bancoagencia_agencia2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA2"), AV23BancoAgencia_Agencia2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Banco_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME2"), AV24Banco_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Municipio_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME2"), AV25Municipio_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Bancoagencia_agencia3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA3"), AV29BancoAgencia_Agencia3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Banco_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME3"), AV30Banco_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Municipio_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME3"), AV31Municipio_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E261N2 */
                                       E261N2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE1N2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm1N2( ) ;
            }
         }
      }

      protected void PA1N2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("BANCOAGENCIA_AGENCIA", "N�mero da Ag�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("BANCO_NOME", "Banco", 0);
            cmbavDynamicfiltersselector1.addItem("MUNICIPIO_NOME", "Nome do Munic�pio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("BANCOAGENCIA_AGENCIA", "N�mero da Ag�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("BANCO_NOME", "Banco", 0);
            cmbavDynamicfiltersselector2.addItem("MUNICIPIO_NOME", "Nome do Munic�pio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("BANCOAGENCIA_AGENCIA", "N�mero da Ag�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("BANCO_NOME", "Banco", 0);
            cmbavDynamicfiltersselector3.addItem("MUNICIPIO_NOME", "Nome do Munic�pio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_982( ) ;
         while ( nGXsfl_98_idx <= nRC_GXsfl_98 )
         {
            sendrow_982( ) ;
            nGXsfl_98_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_98_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_98_idx+1));
            sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
            SubsflControlProps_982( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17BancoAgencia_Agencia1 ,
                                       String AV18Banco_Nome1 ,
                                       String AV19Municipio_Nome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23BancoAgencia_Agencia2 ,
                                       String AV24Banco_Nome2 ,
                                       String AV25Municipio_Nome2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       short AV28DynamicFiltersOperator3 ,
                                       String AV29BancoAgencia_Agencia3 ,
                                       String AV30Banco_Nome3 ,
                                       String AV31Municipio_Nome3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF1N2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "BANCOAGENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_AGENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, ""))));
         GxWebStd.gx_hidden_field( context, "BANCOAGENCIA_AGENCIA", StringUtil.RTrim( A27BancoAgencia_Agencia));
         GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_NOMEAGENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!"))));
         GxWebStd.gx_hidden_field( context, "BANCOAGENCIA_NOMEAGENCIA", StringUtil.RTrim( A28BancoAgencia_NomeAgencia));
         GxWebStd.gx_hidden_field( context, "gxhash_BANCO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "BANCO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1N2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF1N2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 98;
         /* Execute user event: E241N2 */
         E241N2 ();
         nGXsfl_98_idx = 1;
         sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
         SubsflControlProps_982( ) ;
         nGXsfl_98_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_982( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17BancoAgencia_Agencia1 ,
                                                 AV18Banco_Nome1 ,
                                                 AV19Municipio_Nome1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23BancoAgencia_Agencia2 ,
                                                 AV24Banco_Nome2 ,
                                                 AV25Municipio_Nome2 ,
                                                 AV26DynamicFiltersEnabled3 ,
                                                 AV27DynamicFiltersSelector3 ,
                                                 AV28DynamicFiltersOperator3 ,
                                                 AV29BancoAgencia_Agencia3 ,
                                                 AV30Banco_Nome3 ,
                                                 AV31Municipio_Nome3 ,
                                                 A27BancoAgencia_Agencia ,
                                                 A20Banco_Nome ,
                                                 A26Municipio_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17BancoAgencia_Agencia1 = StringUtil.PadR( StringUtil.RTrim( AV17BancoAgencia_Agencia1), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
            lV17BancoAgencia_Agencia1 = StringUtil.PadR( StringUtil.RTrim( AV17BancoAgencia_Agencia1), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
            lV18Banco_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Banco_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
            lV18Banco_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Banco_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
            lV19Municipio_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19Municipio_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
            lV19Municipio_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19Municipio_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
            lV23BancoAgencia_Agencia2 = StringUtil.PadR( StringUtil.RTrim( AV23BancoAgencia_Agencia2), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
            lV23BancoAgencia_Agencia2 = StringUtil.PadR( StringUtil.RTrim( AV23BancoAgencia_Agencia2), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
            lV24Banco_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24Banco_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
            lV24Banco_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24Banco_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
            lV25Municipio_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Municipio_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
            lV25Municipio_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Municipio_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
            lV29BancoAgencia_Agencia3 = StringUtil.PadR( StringUtil.RTrim( AV29BancoAgencia_Agencia3), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
            lV29BancoAgencia_Agencia3 = StringUtil.PadR( StringUtil.RTrim( AV29BancoAgencia_Agencia3), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
            lV30Banco_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30Banco_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
            lV30Banco_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30Banco_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
            lV31Municipio_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31Municipio_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
            lV31Municipio_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31Municipio_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
            /* Using cursor H001N2 */
            pr_default.execute(0, new Object[] {lV17BancoAgencia_Agencia1, lV17BancoAgencia_Agencia1, lV18Banco_Nome1, lV18Banco_Nome1, lV19Municipio_Nome1, lV19Municipio_Nome1, lV23BancoAgencia_Agencia2, lV23BancoAgencia_Agencia2, lV24Banco_Nome2, lV24Banco_Nome2, lV25Municipio_Nome2, lV25Municipio_Nome2, lV29BancoAgencia_Agencia3, lV29BancoAgencia_Agencia3, lV30Banco_Nome3, lV30Banco_Nome3, lV31Municipio_Nome3, lV31Municipio_Nome3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_98_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A26Municipio_Nome = H001N2_A26Municipio_Nome[0];
               A25Municipio_Codigo = H001N2_A25Municipio_Codigo[0];
               A20Banco_Nome = H001N2_A20Banco_Nome[0];
               A18Banco_Codigo = H001N2_A18Banco_Codigo[0];
               A28BancoAgencia_NomeAgencia = H001N2_A28BancoAgencia_NomeAgencia[0];
               A27BancoAgencia_Agencia = H001N2_A27BancoAgencia_Agencia[0];
               A17BancoAgencia_Codigo = H001N2_A17BancoAgencia_Codigo[0];
               A26Municipio_Nome = H001N2_A26Municipio_Nome[0];
               A20Banco_Nome = H001N2_A20Banco_Nome[0];
               /* Execute user event: E251N2 */
               E251N2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 98;
            WB1N0( ) ;
         }
         nGXsfl_98_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17BancoAgencia_Agencia1 ,
                                              AV18Banco_Nome1 ,
                                              AV19Municipio_Nome1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23BancoAgencia_Agencia2 ,
                                              AV24Banco_Nome2 ,
                                              AV25Municipio_Nome2 ,
                                              AV26DynamicFiltersEnabled3 ,
                                              AV27DynamicFiltersSelector3 ,
                                              AV28DynamicFiltersOperator3 ,
                                              AV29BancoAgencia_Agencia3 ,
                                              AV30Banco_Nome3 ,
                                              AV31Municipio_Nome3 ,
                                              A27BancoAgencia_Agencia ,
                                              A20Banco_Nome ,
                                              A26Municipio_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17BancoAgencia_Agencia1 = StringUtil.PadR( StringUtil.RTrim( AV17BancoAgencia_Agencia1), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
         lV17BancoAgencia_Agencia1 = StringUtil.PadR( StringUtil.RTrim( AV17BancoAgencia_Agencia1), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
         lV18Banco_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Banco_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
         lV18Banco_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Banco_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
         lV19Municipio_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19Municipio_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
         lV19Municipio_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19Municipio_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
         lV23BancoAgencia_Agencia2 = StringUtil.PadR( StringUtil.RTrim( AV23BancoAgencia_Agencia2), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
         lV23BancoAgencia_Agencia2 = StringUtil.PadR( StringUtil.RTrim( AV23BancoAgencia_Agencia2), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
         lV24Banco_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24Banco_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
         lV24Banco_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24Banco_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
         lV25Municipio_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Municipio_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
         lV25Municipio_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25Municipio_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
         lV29BancoAgencia_Agencia3 = StringUtil.PadR( StringUtil.RTrim( AV29BancoAgencia_Agencia3), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
         lV29BancoAgencia_Agencia3 = StringUtil.PadR( StringUtil.RTrim( AV29BancoAgencia_Agencia3), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
         lV30Banco_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30Banco_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
         lV30Banco_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30Banco_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
         lV31Municipio_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31Municipio_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
         lV31Municipio_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31Municipio_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
         /* Using cursor H001N3 */
         pr_default.execute(1, new Object[] {lV17BancoAgencia_Agencia1, lV17BancoAgencia_Agencia1, lV18Banco_Nome1, lV18Banco_Nome1, lV19Municipio_Nome1, lV19Municipio_Nome1, lV23BancoAgencia_Agencia2, lV23BancoAgencia_Agencia2, lV24Banco_Nome2, lV24Banco_Nome2, lV25Municipio_Nome2, lV25Municipio_Nome2, lV29BancoAgencia_Agencia3, lV29BancoAgencia_Agencia3, lV30Banco_Nome3, lV30Banco_Nome3, lV31Municipio_Nome3, lV31Municipio_Nome3});
         GRID_nRecordCount = H001N3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return (int)(0) ;
      }

      protected void STRUP1N0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E231N2 */
         E231N2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17BancoAgencia_Agencia1 = cgiGet( edtavBancoagencia_agencia1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
            AV18Banco_Nome1 = StringUtil.Upper( cgiGet( edtavBanco_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
            AV19Municipio_Nome1 = StringUtil.Upper( cgiGet( edtavMunicipio_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23BancoAgencia_Agencia2 = cgiGet( edtavBancoagencia_agencia2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
            AV24Banco_Nome2 = StringUtil.Upper( cgiGet( edtavBanco_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
            AV25Municipio_Nome2 = StringUtil.Upper( cgiGet( edtavMunicipio_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            AV29BancoAgencia_Agencia3 = cgiGet( edtavBancoagencia_agencia3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
            AV30Banco_Nome3 = StringUtil.Upper( cgiGet( edtavBanco_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
            AV31Municipio_Nome3 = StringUtil.Upper( cgiGet( edtavMunicipio_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_98 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_98"), ",", "."));
            AV90GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV91GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA1"), AV17BancoAgencia_Agencia1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME1"), AV18Banco_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME1"), AV19Municipio_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA2"), AV23BancoAgencia_Agencia2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME2"), AV24Banco_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME2"), AV25Municipio_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA3"), AV29BancoAgencia_Agencia3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME3"), AV30Banco_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME3"), AV31Municipio_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E231N2 */
         E231N2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E231N2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Select Banco Agencia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "N�mero da Ag�ncia", 0);
         cmbavOrderedby.addItem("2", "Nome da Ag�ncia", 0);
         cmbavOrderedby.addItem("3", "C�digo", 0);
         cmbavOrderedby.addItem("4", "C�digo", 0);
         cmbavOrderedby.addItem("5", "Banco", 0);
         cmbavOrderedby.addItem("6", "C�digo", 0);
         cmbavOrderedby.addItem("7", "Munic�pio", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E241N2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV26DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         edtBancoAgencia_Codigo_Titleformat = 2;
         edtBancoAgencia_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Codigo_Internalname, "Title", edtBancoAgencia_Codigo_Title);
         edtBancoAgencia_Agencia_Titleformat = 2;
         edtBancoAgencia_Agencia_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N�mero da Ag�ncia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Agencia_Internalname, "Title", edtBancoAgencia_Agencia_Title);
         edtBancoAgencia_NomeAgencia_Titleformat = 2;
         edtBancoAgencia_NomeAgencia_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome da Ag�ncia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_NomeAgencia_Internalname, "Title", edtBancoAgencia_NomeAgencia_Title);
         edtBanco_Codigo_Titleformat = 2;
         edtBanco_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBanco_Codigo_Internalname, "Title", edtBanco_Codigo_Title);
         edtBanco_Nome_Titleformat = 2;
         edtBanco_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Banco", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBanco_Nome_Internalname, "Title", edtBanco_Nome_Title);
         edtMunicipio_Codigo_Titleformat = 2;
         edtMunicipio_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Title", edtMunicipio_Codigo_Title);
         edtMunicipio_Nome_Titleformat = 2;
         edtMunicipio_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Munic�pio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Title", edtMunicipio_Nome_Title);
         AV90GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90GridCurrentPage), 10, 0)));
         AV91GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91GridPageCount), 10, 0)));
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E111N2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV89PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV89PageToGo) ;
         }
      }

      private void E251N2( )
      {
         /* Grid_Load Routine */
         AV34Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV34Select);
         AV94Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 98;
         }
         sendrow_982( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_98_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(98, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E261N2 */
         E261N2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E261N2( )
      {
         /* Enter Routine */
         AV7InOutBancoAgencia_Codigo = A17BancoAgencia_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutBancoAgencia_Codigo), 6, 0)));
         AV36InOutBancoAgencia_NomeAgencia = A28BancoAgencia_NomeAgencia;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36InOutBancoAgencia_NomeAgencia", AV36InOutBancoAgencia_NomeAgencia);
         AV8InOutBancoAgencia_Agencia = A27BancoAgencia_Agencia;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutBancoAgencia_Agencia", AV8InOutBancoAgencia_Agencia);
         AV37InOutBanco_Nome = A20Banco_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37InOutBanco_Nome", AV37InOutBanco_Nome);
         AV38InOutMunicipio_Nome = A26Municipio_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38InOutMunicipio_Nome", AV38InOutMunicipio_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutBancoAgencia_Codigo,(String)AV36InOutBancoAgencia_NomeAgencia,(String)AV8InOutBancoAgencia_Agencia,(String)AV37InOutBanco_Nome,(String)AV38InOutMunicipio_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E121N2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E181N2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
      }

      protected void E131N2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E191N2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E201N2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
      }

      protected void E141N2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E211N2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E151N2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E221N2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E161N2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E171N2( )
      {
         /* 'DobtnNovoBancoAgencia' Routine */
         context.PopUp(formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavBancoagencia_agencia1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia1_Visible), 5, 0)));
         edtavBanco_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome1_Visible), 5, 0)));
         edtavMunicipio_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            edtavBancoagencia_agencia1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 )
         {
            edtavBanco_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavBancoagencia_agencia2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia2_Visible), 5, 0)));
         edtavBanco_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome2_Visible), 5, 0)));
         edtavMunicipio_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            edtavBancoagencia_agencia2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 )
         {
            edtavBanco_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavBancoagencia_agencia3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia3_Visible), 5, 0)));
         edtavBanco_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome3_Visible), 5, 0)));
         edtavMunicipio_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            edtavBancoagencia_agencia3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 )
         {
            edtavBanco_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23BancoAgencia_Agencia2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         AV29BancoAgencia_Agencia3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17BancoAgencia_Agencia1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17BancoAgencia_Agencia1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18Banco_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19Municipio_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23BancoAgencia_Agencia2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24Banco_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25Municipio_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV29BancoAgencia_Agencia3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV30Banco_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV31Municipio_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17BancoAgencia_Agencia1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17BancoAgencia_Agencia1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Banco_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Banco_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Municipio_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Municipio_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23BancoAgencia_Agencia2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23BancoAgencia_Agencia2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Banco_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Banco_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Municipio_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Municipio_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29BancoAgencia_Agencia3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV29BancoAgencia_Agencia3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Banco_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV30Banco_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Municipio_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Municipio_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1N2( true) ;
         }
         else
         {
            wb_table2_5_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_92_1N2( true) ;
         }
         else
         {
            wb_table3_92_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table3_92_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1N2e( true) ;
         }
         else
         {
            wb_table1_2_1N2e( false) ;
         }
      }

      protected void wb_table3_92_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_95_1N2( true) ;
         }
         else
         {
            wb_table4_95_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table4_95_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_92_1N2e( true) ;
         }
         else
         {
            wb_table3_92_1N2e( false) ;
         }
      }

      protected void wb_table4_95_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"98\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBancoAgencia_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtBancoAgencia_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBancoAgencia_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBancoAgencia_Agencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtBancoAgencia_Agencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBancoAgencia_Agencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBancoAgencia_NomeAgencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtBancoAgencia_NomeAgencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBancoAgencia_NomeAgencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBanco_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtBanco_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBanco_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBanco_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtBanco_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBanco_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBancoAgencia_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBancoAgencia_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A27BancoAgencia_Agencia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBancoAgencia_Agencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBancoAgencia_Agencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A28BancoAgencia_NomeAgencia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBancoAgencia_NomeAgencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBancoAgencia_NomeAgencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBanco_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBanco_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A20Banco_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBanco_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBanco_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A26Municipio_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 98 )
         {
            wbEnd = 0;
            nRC_GXsfl_98 = (short)(nGXsfl_98_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_95_1N2e( true) ;
         }
         else
         {
            wb_table4_95_1N2e( false) ;
         }
      }

      protected void wb_table2_5_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_8_1N2( true) ;
         }
         else
         {
            wb_table5_8_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table5_8_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_86_1N2( true) ;
         }
         else
         {
            wb_table6_86_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table6_86_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1N2e( true) ;
         }
         else
         {
            wb_table2_5_1N2e( false) ;
         }
      }

      protected void wb_table6_86_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnnovobancoagencia_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(98), 2, 0)+","+"null"+");", "Novo Banco x Ag�ncia", bttBtnbtnnovobancoagencia_Jsonclick, 5, "Clique aqui p/ criar Novo Banco x Ag�ncia", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBTNNOVOBANCOAGENCIA\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_86_1N2e( true) ;
         }
         else
         {
            wb_table6_86_1N2e( false) ;
         }
      }

      protected void wb_table5_8_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table7_11_1N2( true) ;
         }
         else
         {
            wb_table7_11_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table7_11_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_8_1N2e( true) ;
         }
         else
         {
            wb_table5_8_1N2e( false) ;
         }
      }

      protected void wb_table7_11_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_PromptBancoAgencia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_20_1N2( true) ;
         }
         else
         {
            wb_table8_20_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table8_20_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_11_1N2e( true) ;
         }
         else
         {
            wb_table7_11_1N2e( false) ;
         }
      }

      protected void wb_table8_20_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table9_25_1N2( true) ;
         }
         else
         {
            wb_table9_25_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table9_25_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_20_1N2e( true) ;
         }
         else
         {
            wb_table8_20_1N2e( false) ;
         }
      }

      protected void wb_table9_25_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_PromptBancoAgencia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_34_1N2( true) ;
         }
         else
         {
            wb_table10_34_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table10_34_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptBancoAgencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptBancoAgencia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_53_1N2( true) ;
         }
         else
         {
            wb_table11_53_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table11_53_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptBancoAgencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_PromptBancoAgencia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_72_1N2( true) ;
         }
         else
         {
            wb_table12_72_1N2( false) ;
         }
         return  ;
      }

      protected void wb_table12_72_1N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_25_1N2e( true) ;
         }
         else
         {
            wb_table9_25_1N2e( false) ;
         }
      }

      protected void wb_table12_72_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", "", true, "HLP_PromptBancoAgencia.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBancoagencia_agencia3_Internalname, StringUtil.RTrim( AV29BancoAgencia_Agencia3), StringUtil.RTrim( context.localUtil.Format( AV29BancoAgencia_Agencia3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBancoagencia_agencia3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBancoagencia_agencia3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBanco_nome3_Internalname, StringUtil.RTrim( AV30Banco_Nome3), StringUtil.RTrim( context.localUtil.Format( AV30Banco_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBanco_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBanco_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome3_Internalname, StringUtil.RTrim( AV31Municipio_Nome3), StringUtil.RTrim( context.localUtil.Format( AV31Municipio_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_72_1N2e( true) ;
         }
         else
         {
            wb_table12_72_1N2e( false) ;
         }
      }

      protected void wb_table11_53_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_PromptBancoAgencia.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBancoagencia_agencia2_Internalname, StringUtil.RTrim( AV23BancoAgencia_Agencia2), StringUtil.RTrim( context.localUtil.Format( AV23BancoAgencia_Agencia2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBancoagencia_agencia2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBancoagencia_agencia2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBanco_nome2_Internalname, StringUtil.RTrim( AV24Banco_Nome2), StringUtil.RTrim( context.localUtil.Format( AV24Banco_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBanco_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBanco_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome2_Internalname, StringUtil.RTrim( AV25Municipio_Nome2), StringUtil.RTrim( context.localUtil.Format( AV25Municipio_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_53_1N2e( true) ;
         }
         else
         {
            wb_table11_53_1N2e( false) ;
         }
      }

      protected void wb_table10_34_1N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_PromptBancoAgencia.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBancoagencia_agencia1_Internalname, StringUtil.RTrim( AV17BancoAgencia_Agencia1), StringUtil.RTrim( context.localUtil.Format( AV17BancoAgencia_Agencia1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBancoagencia_agencia1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBancoagencia_agencia1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBanco_nome1_Internalname, StringUtil.RTrim( AV18Banco_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Banco_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBanco_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBanco_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome1_Internalname, StringUtil.RTrim( AV19Municipio_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19Municipio_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_34_1N2e( true) ;
         }
         else
         {
            wb_table10_34_1N2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutBancoAgencia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutBancoAgencia_Codigo), 6, 0)));
         AV36InOutBancoAgencia_NomeAgencia = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36InOutBancoAgencia_NomeAgencia", AV36InOutBancoAgencia_NomeAgencia);
         AV8InOutBancoAgencia_Agencia = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutBancoAgencia_Agencia", AV8InOutBancoAgencia_Agencia);
         AV37InOutBanco_Nome = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37InOutBanco_Nome", AV37InOutBanco_Nome);
         AV38InOutMunicipio_Nome = (String)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38InOutMunicipio_Nome", AV38InOutMunicipio_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1N2( ) ;
         WS1N2( ) ;
         WE1N2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117314937");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptbancoagencia.js", "?20203117314938");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_982( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_98_idx;
         edtBancoAgencia_Codigo_Internalname = "BANCOAGENCIA_CODIGO_"+sGXsfl_98_idx;
         edtBancoAgencia_Agencia_Internalname = "BANCOAGENCIA_AGENCIA_"+sGXsfl_98_idx;
         edtBancoAgencia_NomeAgencia_Internalname = "BANCOAGENCIA_NOMEAGENCIA_"+sGXsfl_98_idx;
         edtBanco_Codigo_Internalname = "BANCO_CODIGO_"+sGXsfl_98_idx;
         edtBanco_Nome_Internalname = "BANCO_NOME_"+sGXsfl_98_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_98_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_98_idx;
      }

      protected void SubsflControlProps_fel_982( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_98_fel_idx;
         edtBancoAgencia_Codigo_Internalname = "BANCOAGENCIA_CODIGO_"+sGXsfl_98_fel_idx;
         edtBancoAgencia_Agencia_Internalname = "BANCOAGENCIA_AGENCIA_"+sGXsfl_98_fel_idx;
         edtBancoAgencia_NomeAgencia_Internalname = "BANCOAGENCIA_NOMEAGENCIA_"+sGXsfl_98_fel_idx;
         edtBanco_Codigo_Internalname = "BANCO_CODIGO_"+sGXsfl_98_fel_idx;
         edtBanco_Nome_Internalname = "BANCO_NOME_"+sGXsfl_98_fel_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_98_fel_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_98_fel_idx;
      }

      protected void sendrow_982( )
      {
         SubsflControlProps_982( ) ;
         WB1N0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_98_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_98_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_98_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 99,'',false,'',98)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV34Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV94Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV94Select_GXI : context.PathToRelativeUrl( AV34Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_98_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV34Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBancoAgencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBancoAgencia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBancoAgencia_Agencia_Internalname,StringUtil.RTrim( A27BancoAgencia_Agencia),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBancoAgencia_Agencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)-1,(bool)true,(String)"Agencia",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBancoAgencia_NomeAgencia_Internalname,StringUtil.RTrim( A28BancoAgencia_NomeAgencia),StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBancoAgencia_NomeAgencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBanco_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBanco_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBanco_Nome_Internalname,StringUtil.RTrim( A20Banco_Nome),StringUtil.RTrim( context.localUtil.Format( A20Banco_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBanco_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Nome_Internalname,StringUtil.RTrim( A26Municipio_Nome),StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_CODIGO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sGXsfl_98_idx, context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_AGENCIA"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sGXsfl_98_idx, StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_NOMEAGENCIA"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sGXsfl_98_idx, StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_BANCO_CODIGO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sGXsfl_98_idx, context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_CODIGO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sGXsfl_98_idx, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_98_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_98_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_98_idx+1));
            sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
            SubsflControlProps_982( ) ;
         }
         /* End function sendrow_982 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavBancoagencia_agencia1_Internalname = "vBANCOAGENCIA_AGENCIA1";
         edtavBanco_nome1_Internalname = "vBANCO_NOME1";
         edtavMunicipio_nome1_Internalname = "vMUNICIPIO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavBancoagencia_agencia2_Internalname = "vBANCOAGENCIA_AGENCIA2";
         edtavBanco_nome2_Internalname = "vBANCO_NOME2";
         edtavMunicipio_nome2_Internalname = "vMUNICIPIO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavBancoagencia_agencia3_Internalname = "vBANCOAGENCIA_AGENCIA3";
         edtavBanco_nome3_Internalname = "vBANCO_NOME3";
         edtavMunicipio_nome3_Internalname = "vMUNICIPIO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         bttBtnbtnnovobancoagencia_Internalname = "BTNBTNNOVOBANCOAGENCIA";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         edtavSelect_Internalname = "vSELECT";
         edtBancoAgencia_Codigo_Internalname = "BANCOAGENCIA_CODIGO";
         edtBancoAgencia_Agencia_Internalname = "BANCOAGENCIA_AGENCIA";
         edtBancoAgencia_NomeAgencia_Internalname = "BANCOAGENCIA_NOMEAGENCIA";
         edtBanco_Codigo_Internalname = "BANCO_CODIGO";
         edtBanco_Nome_Internalname = "BANCO_NOME";
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtMunicipio_Nome_Jsonclick = "";
         edtMunicipio_Codigo_Jsonclick = "";
         edtBanco_Nome_Jsonclick = "";
         edtBanco_Codigo_Jsonclick = "";
         edtBancoAgencia_NomeAgencia_Jsonclick = "";
         edtBancoAgencia_Agencia_Jsonclick = "";
         edtBancoAgencia_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavMunicipio_nome1_Jsonclick = "";
         edtavBanco_nome1_Jsonclick = "";
         edtavBancoagencia_agencia1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavMunicipio_nome2_Jsonclick = "";
         edtavBanco_nome2_Jsonclick = "";
         edtavBancoagencia_agencia2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavMunicipio_nome3_Jsonclick = "";
         edtavBanco_nome3_Jsonclick = "";
         edtavBancoagencia_agencia3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtMunicipio_Nome_Titleformat = 0;
         edtMunicipio_Codigo_Titleformat = 0;
         edtBanco_Nome_Titleformat = 0;
         edtBanco_Codigo_Titleformat = 0;
         edtBancoAgencia_NomeAgencia_Titleformat = 0;
         edtBancoAgencia_Agencia_Titleformat = 0;
         edtBancoAgencia_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavMunicipio_nome3_Visible = 1;
         edtavBanco_nome3_Visible = 1;
         edtavBancoagencia_agencia3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavMunicipio_nome2_Visible = 1;
         edtavBanco_nome2_Visible = 1;
         edtavBancoagencia_agencia2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavMunicipio_nome1_Visible = 1;
         edtavBanco_nome1_Visible = 1;
         edtavBancoagencia_agencia1_Visible = 1;
         edtMunicipio_Nome_Title = "Munic�pio";
         edtMunicipio_Codigo_Title = "C�digo";
         edtBanco_Nome_Title = "Banco";
         edtBanco_Codigo_Title = "C�digo";
         edtBancoAgencia_NomeAgencia_Title = "Nome da Ag�ncia";
         edtBancoAgencia_Agencia_Title = "N�mero da Ag�ncia";
         edtBancoAgencia_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Banco Agencia";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtBancoAgencia_Codigo_Titleformat',ctrl:'BANCOAGENCIA_CODIGO',prop:'Titleformat'},{av:'edtBancoAgencia_Codigo_Title',ctrl:'BANCOAGENCIA_CODIGO',prop:'Title'},{av:'edtBancoAgencia_Agencia_Titleformat',ctrl:'BANCOAGENCIA_AGENCIA',prop:'Titleformat'},{av:'edtBancoAgencia_Agencia_Title',ctrl:'BANCOAGENCIA_AGENCIA',prop:'Title'},{av:'edtBancoAgencia_NomeAgencia_Titleformat',ctrl:'BANCOAGENCIA_NOMEAGENCIA',prop:'Titleformat'},{av:'edtBancoAgencia_NomeAgencia_Title',ctrl:'BANCOAGENCIA_NOMEAGENCIA',prop:'Title'},{av:'edtBanco_Codigo_Titleformat',ctrl:'BANCO_CODIGO',prop:'Titleformat'},{av:'edtBanco_Codigo_Title',ctrl:'BANCO_CODIGO',prop:'Title'},{av:'edtBanco_Nome_Titleformat',ctrl:'BANCO_NOME',prop:'Titleformat'},{av:'edtBanco_Nome_Title',ctrl:'BANCO_NOME',prop:'Title'},{av:'edtMunicipio_Codigo_Titleformat',ctrl:'MUNICIPIO_CODIGO',prop:'Titleformat'},{av:'edtMunicipio_Codigo_Title',ctrl:'MUNICIPIO_CODIGO',prop:'Title'},{av:'edtMunicipio_Nome_Titleformat',ctrl:'MUNICIPIO_NOME',prop:'Titleformat'},{av:'edtMunicipio_Nome_Title',ctrl:'MUNICIPIO_NOME',prop:'Title'},{av:'AV90GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV91GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E111N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E251N2',iparms:[],oparms:[{av:'AV34Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E261N2',iparms:[{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A28BancoAgencia_NomeAgencia',fld:'BANCOAGENCIA_NOMEAGENCIA',pic:'@!',hsh:true,nv:''},{av:'A27BancoAgencia_Agencia',fld:'BANCOAGENCIA_AGENCIA',pic:'',hsh:true,nv:''},{av:'A20Banco_Nome',fld:'BANCO_NOME',pic:'@!',nv:''},{av:'A26Municipio_Nome',fld:'MUNICIPIO_NOME',pic:'@!',nv:''}],oparms:[{av:'AV7InOutBancoAgencia_Codigo',fld:'vINOUTBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36InOutBancoAgencia_NomeAgencia',fld:'vINOUTBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV8InOutBancoAgencia_Agencia',fld:'vINOUTBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV37InOutBanco_Nome',fld:'vINOUTBANCO_NOME',pic:'@!',nv:''},{av:'AV38InOutMunicipio_Nome',fld:'vINOUTMUNICIPIO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E121N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E181N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E131N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E191N2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E201N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E141N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E211N2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E151N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E221N2',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E161N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOBTNNOVOBANCOAGENCIA'","{handler:'E171N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV36InOutBancoAgencia_NomeAgencia = "";
         wcpOAV8InOutBancoAgencia_Agencia = "";
         wcpOAV37InOutBanco_Nome = "";
         wcpOAV38InOutMunicipio_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17BancoAgencia_Agencia1 = "";
         AV18Banco_Nome1 = "";
         AV19Municipio_Nome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23BancoAgencia_Agencia2 = "";
         AV24Banco_Nome2 = "";
         AV25Municipio_Nome2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV29BancoAgencia_Agencia3 = "";
         AV30Banco_Nome3 = "";
         AV31Municipio_Nome3 = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Select = "";
         AV94Select_GXI = "";
         A27BancoAgencia_Agencia = "";
         A28BancoAgencia_NomeAgencia = "";
         A20Banco_Nome = "";
         A26Municipio_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17BancoAgencia_Agencia1 = "";
         lV18Banco_Nome1 = "";
         lV19Municipio_Nome1 = "";
         lV23BancoAgencia_Agencia2 = "";
         lV24Banco_Nome2 = "";
         lV25Municipio_Nome2 = "";
         lV29BancoAgencia_Agencia3 = "";
         lV30Banco_Nome3 = "";
         lV31Municipio_Nome3 = "";
         H001N2_A26Municipio_Nome = new String[] {""} ;
         H001N2_A25Municipio_Codigo = new int[1] ;
         H001N2_A20Banco_Nome = new String[] {""} ;
         H001N2_A18Banco_Codigo = new int[1] ;
         H001N2_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         H001N2_A27BancoAgencia_Agencia = new String[] {""} ;
         H001N2_A17BancoAgencia_Codigo = new int[1] ;
         H001N3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         bttBtnbtnnovobancoagencia_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptbancoagencia__default(),
            new Object[][] {
                new Object[] {
               H001N2_A26Municipio_Nome, H001N2_A25Municipio_Codigo, H001N2_A20Banco_Nome, H001N2_A18Banco_Codigo, H001N2_A28BancoAgencia_NomeAgencia, H001N2_A27BancoAgencia_Agencia, H001N2_A17BancoAgencia_Codigo
               }
               , new Object[] {
               H001N3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_98 ;
      private short nGXsfl_98_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV28DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_98_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtBancoAgencia_Codigo_Titleformat ;
      private short edtBancoAgencia_Agencia_Titleformat ;
      private short edtBancoAgencia_NomeAgencia_Titleformat ;
      private short edtBanco_Codigo_Titleformat ;
      private short edtBanco_Nome_Titleformat ;
      private short edtMunicipio_Codigo_Titleformat ;
      private short edtMunicipio_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutBancoAgencia_Codigo ;
      private int wcpOAV7InOutBancoAgencia_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A17BancoAgencia_Codigo ;
      private int A18Banco_Codigo ;
      private int A25Municipio_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV89PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavBancoagencia_agencia1_Visible ;
      private int edtavBanco_nome1_Visible ;
      private int edtavMunicipio_nome1_Visible ;
      private int edtavBancoagencia_agencia2_Visible ;
      private int edtavBanco_nome2_Visible ;
      private int edtavMunicipio_nome2_Visible ;
      private int edtavBancoagencia_agencia3_Visible ;
      private int edtavBanco_nome3_Visible ;
      private int edtavMunicipio_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV90GridCurrentPage ;
      private long AV91GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV36InOutBancoAgencia_NomeAgencia ;
      private String AV8InOutBancoAgencia_Agencia ;
      private String AV37InOutBanco_Nome ;
      private String AV38InOutMunicipio_Nome ;
      private String wcpOAV36InOutBancoAgencia_NomeAgencia ;
      private String wcpOAV8InOutBancoAgencia_Agencia ;
      private String wcpOAV37InOutBanco_Nome ;
      private String wcpOAV38InOutMunicipio_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_98_idx="0001" ;
      private String AV17BancoAgencia_Agencia1 ;
      private String AV18Banco_Nome1 ;
      private String AV19Municipio_Nome1 ;
      private String AV23BancoAgencia_Agencia2 ;
      private String AV24Banco_Nome2 ;
      private String AV25Municipio_Nome2 ;
      private String AV29BancoAgencia_Agencia3 ;
      private String AV30Banco_Nome3 ;
      private String AV31Municipio_Nome3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtBancoAgencia_Codigo_Internalname ;
      private String A27BancoAgencia_Agencia ;
      private String edtBancoAgencia_Agencia_Internalname ;
      private String A28BancoAgencia_NomeAgencia ;
      private String edtBancoAgencia_NomeAgencia_Internalname ;
      private String edtBanco_Codigo_Internalname ;
      private String A20Banco_Nome ;
      private String edtBanco_Nome_Internalname ;
      private String edtMunicipio_Codigo_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17BancoAgencia_Agencia1 ;
      private String lV18Banco_Nome1 ;
      private String lV19Municipio_Nome1 ;
      private String lV23BancoAgencia_Agencia2 ;
      private String lV24Banco_Nome2 ;
      private String lV25Municipio_Nome2 ;
      private String lV29BancoAgencia_Agencia3 ;
      private String lV30Banco_Nome3 ;
      private String lV31Municipio_Nome3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavBancoagencia_agencia1_Internalname ;
      private String edtavBanco_nome1_Internalname ;
      private String edtavMunicipio_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavBancoagencia_agencia2_Internalname ;
      private String edtavBanco_nome2_Internalname ;
      private String edtavMunicipio_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavBancoagencia_agencia3_Internalname ;
      private String edtavBanco_nome3_Internalname ;
      private String edtavMunicipio_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtBancoAgencia_Codigo_Title ;
      private String edtBancoAgencia_Agencia_Title ;
      private String edtBancoAgencia_NomeAgencia_Title ;
      private String edtBanco_Codigo_Title ;
      private String edtBanco_Nome_Title ;
      private String edtMunicipio_Codigo_Title ;
      private String edtMunicipio_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String bttBtnbtnnovobancoagencia_Internalname ;
      private String bttBtnbtnnovobancoagencia_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavBancoagencia_agencia3_Jsonclick ;
      private String edtavBanco_nome3_Jsonclick ;
      private String edtavMunicipio_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavBancoagencia_agencia2_Jsonclick ;
      private String edtavBanco_nome2_Jsonclick ;
      private String edtavMunicipio_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavBancoagencia_agencia1_Jsonclick ;
      private String edtavBanco_nome1_Jsonclick ;
      private String edtavMunicipio_nome1_Jsonclick ;
      private String sGXsfl_98_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtBancoAgencia_Codigo_Jsonclick ;
      private String edtBancoAgencia_Agencia_Jsonclick ;
      private String edtBancoAgencia_NomeAgencia_Jsonclick ;
      private String edtBanco_Codigo_Jsonclick ;
      private String edtBanco_Nome_Jsonclick ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV94Select_GXI ;
      private String AV34Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutBancoAgencia_Codigo ;
      private String aP1_InOutBancoAgencia_NomeAgencia ;
      private String aP2_InOutBancoAgencia_Agencia ;
      private String aP3_InOutBanco_Nome ;
      private String aP4_InOutMunicipio_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H001N2_A26Municipio_Nome ;
      private int[] H001N2_A25Municipio_Codigo ;
      private String[] H001N2_A20Banco_Nome ;
      private int[] H001N2_A18Banco_Codigo ;
      private String[] H001N2_A28BancoAgencia_NomeAgencia ;
      private String[] H001N2_A27BancoAgencia_Agencia ;
      private int[] H001N2_A17BancoAgencia_Codigo ;
      private long[] H001N3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
   }

   public class promptbancoagencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H001N2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17BancoAgencia_Agencia1 ,
                                             String AV18Banco_Nome1 ,
                                             String AV19Municipio_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23BancoAgencia_Agencia2 ,
                                             String AV24Banco_Nome2 ,
                                             String AV25Municipio_Nome2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             String AV29BancoAgencia_Agencia3 ,
                                             String AV30Banco_Nome3 ,
                                             String AV31Municipio_Nome3 ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [23] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Municipio_Nome], T1.[Municipio_Codigo], T3.[Banco_Nome], T1.[Banco_Codigo], T1.[BancoAgencia_NomeAgencia], T1.[BancoAgencia_Agencia], T1.[BancoAgencia_Codigo]";
         sFromString = " FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Banco] T3 WITH (NOLOCK) ON T3.[Banco_Codigo] = T1.[Banco_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17BancoAgencia_Agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV17BancoAgencia_Agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV17BancoAgencia_Agencia1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17BancoAgencia_Agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV17BancoAgencia_Agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV17BancoAgencia_Agencia1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Banco_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV18Banco_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV18Banco_Nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Banco_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV18Banco_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV18Banco_Nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Municipio_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV19Municipio_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV19Municipio_Nome1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Municipio_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV19Municipio_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV19Municipio_Nome1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23BancoAgencia_Agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV23BancoAgencia_Agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV23BancoAgencia_Agencia2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23BancoAgencia_Agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV23BancoAgencia_Agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV23BancoAgencia_Agencia2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Banco_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV24Banco_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV24Banco_Nome2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Banco_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV24Banco_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV24Banco_Nome2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Municipio_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV25Municipio_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV25Municipio_Nome2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Municipio_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV25Municipio_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV25Municipio_Nome2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29BancoAgencia_Agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV29BancoAgencia_Agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV29BancoAgencia_Agencia3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29BancoAgencia_Agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV29BancoAgencia_Agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV29BancoAgencia_Agencia3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Banco_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV30Banco_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV30Banco_Nome3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Banco_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV30Banco_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV30Banco_Nome3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Municipio_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV31Municipio_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV31Municipio_Nome3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Municipio_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV31Municipio_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV31Municipio_Nome3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Agencia]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Agencia] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_NomeAgencia]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_NomeAgencia] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Banco_Codigo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Banco_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Banco_Nome]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Banco_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H001N3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17BancoAgencia_Agencia1 ,
                                             String AV18Banco_Nome1 ,
                                             String AV19Municipio_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23BancoAgencia_Agencia2 ,
                                             String AV24Banco_Nome2 ,
                                             String AV25Municipio_Nome2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             String AV29BancoAgencia_Agencia3 ,
                                             String AV30Banco_Nome3 ,
                                             String AV31Municipio_Nome3 ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [18] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Banco] T2 WITH (NOLOCK) ON T2.[Banco_Codigo] = T1.[Banco_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17BancoAgencia_Agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV17BancoAgencia_Agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV17BancoAgencia_Agencia1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17BancoAgencia_Agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV17BancoAgencia_Agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV17BancoAgencia_Agencia1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Banco_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV18Banco_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV18Banco_Nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Banco_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV18Banco_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV18Banco_Nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Municipio_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV19Municipio_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV19Municipio_Nome1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Municipio_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV19Municipio_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV19Municipio_Nome1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23BancoAgencia_Agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV23BancoAgencia_Agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV23BancoAgencia_Agencia2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23BancoAgencia_Agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV23BancoAgencia_Agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV23BancoAgencia_Agencia2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Banco_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV24Banco_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV24Banco_Nome2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Banco_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV24Banco_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV24Banco_Nome2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Municipio_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV25Municipio_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV25Municipio_Nome2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Municipio_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV25Municipio_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV25Municipio_Nome2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29BancoAgencia_Agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV29BancoAgencia_Agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV29BancoAgencia_Agencia3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29BancoAgencia_Agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV29BancoAgencia_Agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV29BancoAgencia_Agencia3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Banco_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV30Banco_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV30Banco_Nome3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Banco_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV30Banco_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV30Banco_Nome3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Municipio_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV31Municipio_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV31Municipio_Nome3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Municipio_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV31Municipio_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV31Municipio_Nome3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H001N2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H001N3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001N2 ;
          prmH001N2 = new Object[] {
          new Object[] {"@lV17BancoAgencia_Agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV17BancoAgencia_Agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18Banco_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Banco_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Municipio_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Municipio_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23BancoAgencia_Agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV23BancoAgencia_Agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV24Banco_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24Banco_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Municipio_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Municipio_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29BancoAgencia_Agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV29BancoAgencia_Agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV30Banco_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30Banco_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31Municipio_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31Municipio_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH001N3 ;
          prmH001N3 = new Object[] {
          new Object[] {"@lV17BancoAgencia_Agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV17BancoAgencia_Agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18Banco_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Banco_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Municipio_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Municipio_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23BancoAgencia_Agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV23BancoAgencia_Agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV24Banco_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24Banco_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Municipio_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25Municipio_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29BancoAgencia_Agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV29BancoAgencia_Agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV30Banco_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30Banco_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31Municipio_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31Municipio_Nome3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H001N2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001N2,11,0,true,false )
             ,new CursorDef("H001N3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001N3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 10) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                return;
       }
    }

 }

}
