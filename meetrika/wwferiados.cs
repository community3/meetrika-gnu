/*
               File: WWFeriados
        Description:  Feriados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:56:4.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwferiados : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwferiados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwferiados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkFeriado_Fixo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_95 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_95_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_95_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV30Ano1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano1), 4, 0)));
               AV16Feriado_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
               AV28Feriado_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data1", context.localUtil.Format(AV28Feriado_Data1, "99/99/99"));
               AV29Feriado_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Feriado_Data_To1", context.localUtil.Format(AV29Feriado_Data_To1, "99/99/99"));
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV31Ano2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Ano2), 4, 0)));
               AV19Feriado_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
               AV32Feriado_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data2", context.localUtil.Format(AV32Feriado_Data2, "99/99/99"));
               AV33Feriado_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Feriado_Data_To2", context.localUtil.Format(AV33Feriado_Data_To2, "99/99/99"));
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV34Ano3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Ano3), 4, 0)));
               AV22Feriado_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
               AV35Feriado_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data3", context.localUtil.Format(AV35Feriado_Data3, "99/99/99"));
               AV36Feriado_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Feriado_Data_To3", context.localUtil.Format(AV36Feriado_Data_To3, "99/99/99"));
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV40TFFeriado_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFeriado_Data", context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"));
               AV41TFFeriado_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFFeriado_Data_To", context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"));
               AV46TFFeriado_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFeriado_Nome", AV46TFFeriado_Nome);
               AV47TFFeriado_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Nome_Sel", AV47TFFeriado_Nome_Sel);
               AV50TFFeriado_Fixo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0));
               AV44ddo_Feriado_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Feriado_DataTitleControlIdToReplace", AV44ddo_Feriado_DataTitleControlIdToReplace);
               AV48ddo_Feriado_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Feriado_NomeTitleControlIdToReplace", AV48ddo_Feriado_NomeTitleControlIdToReplace);
               AV51ddo_Feriado_FixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Feriado_FixoTitleControlIdToReplace", AV51ddo_Feriado_FixoTitleControlIdToReplace);
               AV82Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               A1175Feriado_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAI92( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTI92( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311856454");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwferiados.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vANO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30Ano1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_NOME1", StringUtil.RTrim( AV16Feriado_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA1", context.localUtil.Format(AV28Feriado_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA_TO1", context.localUtil.Format(AV29Feriado_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vANO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31Ano2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_NOME2", StringUtil.RTrim( AV19Feriado_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA2", context.localUtil.Format(AV32Feriado_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA_TO2", context.localUtil.Format(AV33Feriado_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vANO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Ano3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_NOME3", StringUtil.RTrim( AV22Feriado_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA3", context.localUtil.Format(AV35Feriado_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFERIADO_DATA_TO3", context.localUtil.Format(AV36Feriado_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_DATA", context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_DATA_TO", context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_NOME", StringUtil.RTrim( AV46TFFeriado_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_NOME_SEL", StringUtil.RTrim( AV47TFFeriado_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFERIADO_FIXO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_95", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_95), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV52DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV52DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFERIADO_DATATITLEFILTERDATA", AV39Feriado_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFERIADO_DATATITLEFILTERDATA", AV39Feriado_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFERIADO_NOMETITLEFILTERDATA", AV45Feriado_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFERIADO_NOMETITLEFILTERDATA", AV45Feriado_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFERIADO_FIXOTITLEFILTERDATA", AV49Feriado_FixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFERIADO_FIXOTITLEFILTERDATA", AV49Feriado_FixoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV82Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Caption", StringUtil.RTrim( Ddo_feriado_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Tooltip", StringUtil.RTrim( Ddo_feriado_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Cls", StringUtil.RTrim( Ddo_feriado_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_feriado_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_feriado_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_feriado_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_feriado_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_feriado_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_feriado_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_feriado_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_feriado_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filtertype", StringUtil.RTrim( Ddo_feriado_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_feriado_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_feriado_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Sortasc", StringUtil.RTrim( Ddo_feriado_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Sortdsc", StringUtil.RTrim( Ddo_feriado_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_feriado_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_feriado_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_feriado_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_feriado_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Caption", StringUtil.RTrim( Ddo_feriado_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Tooltip", StringUtil.RTrim( Ddo_feriado_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Cls", StringUtil.RTrim( Ddo_feriado_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_feriado_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_feriado_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_feriado_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_feriado_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_feriado_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_feriado_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_feriado_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filtertype", StringUtil.RTrim( Ddo_feriado_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_feriado_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_feriado_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Datalisttype", StringUtil.RTrim( Ddo_feriado_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Datalistproc", StringUtil.RTrim( Ddo_feriado_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_feriado_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Loadingdata", StringUtil.RTrim( Ddo_feriado_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_feriado_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_feriado_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_feriado_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Caption", StringUtil.RTrim( Ddo_feriado_fixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Tooltip", StringUtil.RTrim( Ddo_feriado_fixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Cls", StringUtil.RTrim( Ddo_feriado_fixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Selectedvalue_set", StringUtil.RTrim( Ddo_feriado_fixo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_feriado_fixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_feriado_fixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includesortasc", StringUtil.BoolToStr( Ddo_feriado_fixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_feriado_fixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includefilter", StringUtil.BoolToStr( Ddo_feriado_fixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Includedatalist", StringUtil.BoolToStr( Ddo_feriado_fixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Datalisttype", StringUtil.RTrim( Ddo_feriado_fixo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Datalistfixedvalues", StringUtil.RTrim( Ddo_feriado_fixo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Cleanfilter", StringUtil.RTrim( Ddo_feriado_fixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Searchbuttontext", StringUtil.RTrim( Ddo_feriado_fixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_feriado_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_feriado_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_feriado_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_feriado_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_feriado_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_feriado_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Activeeventkey", StringUtil.RTrim( Ddo_feriado_fixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FERIADO_FIXO_Selectedvalue_get", StringUtil.RTrim( Ddo_feriado_fixo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEI92( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTI92( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwferiados.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWFeriados" ;
      }

      public override String GetPgmdesc( )
      {
         return " Feriados" ;
      }

      protected void WBI90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_I92( true) ;
         }
         else
         {
            wb_table1_2_I92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(107, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(108, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfferiado_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_data_Internalname, context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"), context.localUtil.Format( AV40TFFeriado_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavTfferiado_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfferiado_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfferiado_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_data_to_Internalname, context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"), context.localUtil.Format( AV41TFFeriado_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavTfferiado_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfferiado_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_feriado_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_feriado_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_feriado_dataauxdate_Internalname, context.localUtil.Format(AV42DDO_Feriado_DataAuxDate, "99/99/99"), context.localUtil.Format( AV42DDO_Feriado_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_feriado_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_feriado_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_feriado_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_feriado_dataauxdateto_Internalname, context.localUtil.Format(AV43DDO_Feriado_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV43DDO_Feriado_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_feriado_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_feriado_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_nome_Internalname, StringUtil.RTrim( AV46TFFeriado_Nome), StringUtil.RTrim( context.localUtil.Format( AV46TFFeriado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_nome_sel_Internalname, StringUtil.RTrim( AV47TFFeriado_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV47TFFeriado_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfferiado_fixo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50TFFeriado_Fixo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfferiado_fixo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfferiado_fixo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FERIADO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname, AV44ddo_Feriado_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_feriado_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFeriados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FERIADO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname, AV48ddo_Feriado_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_feriado_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFeriados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FERIADO_FIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname, AV51ddo_Feriado_FixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFeriados.htm");
         }
         wbLoad = true;
      }

      protected void STARTI92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Feriados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPI90( ) ;
      }

      protected void WSI92( )
      {
         STARTI92( ) ;
         EVTI92( ) ;
      }

      protected void EVTI92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11I92 */
                              E11I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FERIADO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12I92 */
                              E12I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FERIADO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13I92 */
                              E13I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FERIADO_FIXO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14I92 */
                              E14I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15I92 */
                              E15I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16I92 */
                              E16I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17I92 */
                              E17I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18I92 */
                              E18I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOIMPORTAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19I92 */
                              E19I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20I92 */
                              E20I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21I92 */
                              E21I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22I92 */
                              E22I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23I92 */
                              E23I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24I92 */
                              E24I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25I92 */
                              E25I92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_95_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
                              SubsflControlProps_952( ) ;
                              AV25Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV80Update_GXI : context.convertURL( context.PathToRelativeUrl( AV25Update))));
                              AV26Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV81Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV26Delete))));
                              A1175Feriado_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtFeriado_Data_Internalname), 0));
                              A1176Feriado_Nome = StringUtil.Upper( cgiGet( edtFeriado_Nome_Internalname));
                              A1195Feriado_Fixo = StringUtil.StrToBool( cgiGet( chkFeriado_Fixo_Internalname));
                              n1195Feriado_Fixo = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26I92 */
                                    E26I92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27I92 */
                                    E27I92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28I92 */
                                    E28I92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ano1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO1"), ",", ".") != Convert.ToDecimal( AV30Ano1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME1"), AV16Feriado_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA1"), 0) != AV28Feriado_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO1"), 0) != AV29Feriado_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ano2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO2"), ",", ".") != Convert.ToDecimal( AV31Ano2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME2"), AV19Feriado_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA2"), 0) != AV32Feriado_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO2"), 0) != AV33Feriado_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ano3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO3"), ",", ".") != Convert.ToDecimal( AV34Ano3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME3"), AV22Feriado_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_data3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA3"), 0) != AV35Feriado_Data3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Feriado_data_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO3"), 0) != AV36Feriado_Data_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfferiado_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA"), 0) != AV40TFFeriado_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfferiado_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA_TO"), 0) != AV41TFFeriado_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfferiado_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME"), AV46TFFeriado_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfferiado_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME_SEL"), AV47TFFeriado_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfferiado_fixo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFERIADO_FIXO_SEL"), ",", ".") != Convert.ToDecimal( AV50TFFeriado_Fixo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEI92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAI92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ANO", "Ano", 0);
            cmbavDynamicfiltersselector1.addItem("FERIADO_NOME", "Descri��o", 0);
            cmbavDynamicfiltersselector1.addItem("FERIADO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ANO", "Ano", 0);
            cmbavDynamicfiltersselector2.addItem("FERIADO_NOME", "Descri��o", 0);
            cmbavDynamicfiltersselector2.addItem("FERIADO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("ANO", "Ano", 0);
            cmbavDynamicfiltersselector3.addItem("FERIADO_NOME", "Descri��o", 0);
            cmbavDynamicfiltersselector3.addItem("FERIADO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            GXCCtl = "FERIADO_FIXO_" + sGXsfl_95_idx;
            chkFeriado_Fixo.Name = GXCCtl;
            chkFeriado_Fixo.WebTags = "";
            chkFeriado_Fixo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFeriado_Fixo_Internalname, "TitleCaption", chkFeriado_Fixo.Caption);
            chkFeriado_Fixo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_952( ) ;
         while ( nGXsfl_95_idx <= nRC_GXsfl_95 )
         {
            sendrow_952( ) ;
            nGXsfl_95_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_95_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_95_idx+1));
            sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
            SubsflControlProps_952( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV30Ano1 ,
                                       String AV16Feriado_Nome1 ,
                                       DateTime AV28Feriado_Data1 ,
                                       DateTime AV29Feriado_Data_To1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       short AV31Ano2 ,
                                       String AV19Feriado_Nome2 ,
                                       DateTime AV32Feriado_Data2 ,
                                       DateTime AV33Feriado_Data_To2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       short AV34Ano3 ,
                                       String AV22Feriado_Nome3 ,
                                       DateTime AV35Feriado_Data3 ,
                                       DateTime AV36Feriado_Data_To3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       bool AV14OrderedDsc ,
                                       DateTime AV40TFFeriado_Data ,
                                       DateTime AV41TFFeriado_Data_To ,
                                       String AV46TFFeriado_Nome ,
                                       String AV47TFFeriado_Nome_Sel ,
                                       short AV50TFFeriado_Fixo_Sel ,
                                       String AV44ddo_Feriado_DataTitleControlIdToReplace ,
                                       String AV48ddo_Feriado_NomeTitleControlIdToReplace ,
                                       String AV51ddo_Feriado_FixoTitleControlIdToReplace ,
                                       String AV82Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       DateTime A1175Feriado_Data )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFI92( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_DATA", GetSecureSignedToken( "", A1175Feriado_Data));
         GxWebStd.gx_hidden_field( context, "FERIADO_DATA", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1176Feriado_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "FERIADO_NOME", StringUtil.RTrim( A1176Feriado_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_FIXO", GetSecureSignedToken( "", A1195Feriado_Fixo));
         GxWebStd.gx_hidden_field( context, "FERIADO_FIXO", StringUtil.BoolToStr( A1195Feriado_Fixo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFI92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV82Pgmname = "WWFeriados";
         context.Gx_err = 0;
      }

      protected void RFI92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 95;
         /* Execute user event: E27I92 */
         E27I92 ();
         nGXsfl_95_idx = 1;
         sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
         SubsflControlProps_952( ) ;
         nGXsfl_95_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_952( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV58WWFeriadosDS_1_Dynamicfiltersselector1 ,
                                                 AV59WWFeriadosDS_2_Ano1 ,
                                                 AV60WWFeriadosDS_3_Feriado_nome1 ,
                                                 AV61WWFeriadosDS_4_Feriado_data1 ,
                                                 AV62WWFeriadosDS_5_Feriado_data_to1 ,
                                                 AV63WWFeriadosDS_6_Dynamicfiltersenabled2 ,
                                                 AV64WWFeriadosDS_7_Dynamicfiltersselector2 ,
                                                 AV65WWFeriadosDS_8_Ano2 ,
                                                 AV66WWFeriadosDS_9_Feriado_nome2 ,
                                                 AV67WWFeriadosDS_10_Feriado_data2 ,
                                                 AV68WWFeriadosDS_11_Feriado_data_to2 ,
                                                 AV69WWFeriadosDS_12_Dynamicfiltersenabled3 ,
                                                 AV70WWFeriadosDS_13_Dynamicfiltersselector3 ,
                                                 AV71WWFeriadosDS_14_Ano3 ,
                                                 AV72WWFeriadosDS_15_Feriado_nome3 ,
                                                 AV73WWFeriadosDS_16_Feriado_data3 ,
                                                 AV74WWFeriadosDS_17_Feriado_data_to3 ,
                                                 AV75WWFeriadosDS_18_Tfferiado_data ,
                                                 AV76WWFeriadosDS_19_Tfferiado_data_to ,
                                                 AV78WWFeriadosDS_21_Tfferiado_nome_sel ,
                                                 AV77WWFeriadosDS_20_Tfferiado_nome ,
                                                 AV79WWFeriadosDS_22_Tfferiado_fixo_sel ,
                                                 A1175Feriado_Data ,
                                                 A1176Feriado_Nome ,
                                                 A1195Feriado_Fixo ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            lV60WWFeriadosDS_3_Feriado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWFeriadosDS_3_Feriado_nome1), 50, "%");
            lV66WWFeriadosDS_9_Feriado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWFeriadosDS_9_Feriado_nome2), 50, "%");
            lV72WWFeriadosDS_15_Feriado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWFeriadosDS_15_Feriado_nome3), 50, "%");
            lV77WWFeriadosDS_20_Tfferiado_nome = StringUtil.PadR( StringUtil.RTrim( AV77WWFeriadosDS_20_Tfferiado_nome), 50, "%");
            /* Using cursor H00I92 */
            pr_default.execute(0, new Object[] {AV59WWFeriadosDS_2_Ano1, lV60WWFeriadosDS_3_Feriado_nome1, AV61WWFeriadosDS_4_Feriado_data1, AV62WWFeriadosDS_5_Feriado_data_to1, AV65WWFeriadosDS_8_Ano2, lV66WWFeriadosDS_9_Feriado_nome2, AV67WWFeriadosDS_10_Feriado_data2, AV68WWFeriadosDS_11_Feriado_data_to2, AV71WWFeriadosDS_14_Ano3, lV72WWFeriadosDS_15_Feriado_nome3, AV73WWFeriadosDS_16_Feriado_data3, AV74WWFeriadosDS_17_Feriado_data_to3, AV75WWFeriadosDS_18_Tfferiado_data, AV76WWFeriadosDS_19_Tfferiado_data_to, lV77WWFeriadosDS_20_Tfferiado_nome, AV78WWFeriadosDS_21_Tfferiado_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_95_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1195Feriado_Fixo = H00I92_A1195Feriado_Fixo[0];
               n1195Feriado_Fixo = H00I92_n1195Feriado_Fixo[0];
               A1176Feriado_Nome = H00I92_A1176Feriado_Nome[0];
               A1175Feriado_Data = H00I92_A1175Feriado_Data[0];
               /* Execute user event: E28I92 */
               E28I92 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 95;
            WBI90( ) ;
         }
         nGXsfl_95_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV59WWFeriadosDS_2_Ano1 = AV30Ano1;
         AV60WWFeriadosDS_3_Feriado_nome1 = AV16Feriado_Nome1;
         AV61WWFeriadosDS_4_Feriado_data1 = AV28Feriado_Data1;
         AV62WWFeriadosDS_5_Feriado_data_to1 = AV29Feriado_Data_To1;
         AV63WWFeriadosDS_6_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV65WWFeriadosDS_8_Ano2 = AV31Ano2;
         AV66WWFeriadosDS_9_Feriado_nome2 = AV19Feriado_Nome2;
         AV67WWFeriadosDS_10_Feriado_data2 = AV32Feriado_Data2;
         AV68WWFeriadosDS_11_Feriado_data_to2 = AV33Feriado_Data_To2;
         AV69WWFeriadosDS_12_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWFeriadosDS_14_Ano3 = AV34Ano3;
         AV72WWFeriadosDS_15_Feriado_nome3 = AV22Feriado_Nome3;
         AV73WWFeriadosDS_16_Feriado_data3 = AV35Feriado_Data3;
         AV74WWFeriadosDS_17_Feriado_data_to3 = AV36Feriado_Data_To3;
         AV75WWFeriadosDS_18_Tfferiado_data = AV40TFFeriado_Data;
         AV76WWFeriadosDS_19_Tfferiado_data_to = AV41TFFeriado_Data_To;
         AV77WWFeriadosDS_20_Tfferiado_nome = AV46TFFeriado_Nome;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = AV47TFFeriado_Nome_Sel;
         AV79WWFeriadosDS_22_Tfferiado_fixo_sel = AV50TFFeriado_Fixo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV58WWFeriadosDS_1_Dynamicfiltersselector1 ,
                                              AV59WWFeriadosDS_2_Ano1 ,
                                              AV60WWFeriadosDS_3_Feriado_nome1 ,
                                              AV61WWFeriadosDS_4_Feriado_data1 ,
                                              AV62WWFeriadosDS_5_Feriado_data_to1 ,
                                              AV63WWFeriadosDS_6_Dynamicfiltersenabled2 ,
                                              AV64WWFeriadosDS_7_Dynamicfiltersselector2 ,
                                              AV65WWFeriadosDS_8_Ano2 ,
                                              AV66WWFeriadosDS_9_Feriado_nome2 ,
                                              AV67WWFeriadosDS_10_Feriado_data2 ,
                                              AV68WWFeriadosDS_11_Feriado_data_to2 ,
                                              AV69WWFeriadosDS_12_Dynamicfiltersenabled3 ,
                                              AV70WWFeriadosDS_13_Dynamicfiltersselector3 ,
                                              AV71WWFeriadosDS_14_Ano3 ,
                                              AV72WWFeriadosDS_15_Feriado_nome3 ,
                                              AV73WWFeriadosDS_16_Feriado_data3 ,
                                              AV74WWFeriadosDS_17_Feriado_data_to3 ,
                                              AV75WWFeriadosDS_18_Tfferiado_data ,
                                              AV76WWFeriadosDS_19_Tfferiado_data_to ,
                                              AV78WWFeriadosDS_21_Tfferiado_nome_sel ,
                                              AV77WWFeriadosDS_20_Tfferiado_nome ,
                                              AV79WWFeriadosDS_22_Tfferiado_fixo_sel ,
                                              A1175Feriado_Data ,
                                              A1176Feriado_Nome ,
                                              A1195Feriado_Fixo ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV60WWFeriadosDS_3_Feriado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWFeriadosDS_3_Feriado_nome1), 50, "%");
         lV66WWFeriadosDS_9_Feriado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWFeriadosDS_9_Feriado_nome2), 50, "%");
         lV72WWFeriadosDS_15_Feriado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWFeriadosDS_15_Feriado_nome3), 50, "%");
         lV77WWFeriadosDS_20_Tfferiado_nome = StringUtil.PadR( StringUtil.RTrim( AV77WWFeriadosDS_20_Tfferiado_nome), 50, "%");
         /* Using cursor H00I93 */
         pr_default.execute(1, new Object[] {AV59WWFeriadosDS_2_Ano1, lV60WWFeriadosDS_3_Feriado_nome1, AV61WWFeriadosDS_4_Feriado_data1, AV62WWFeriadosDS_5_Feriado_data_to1, AV65WWFeriadosDS_8_Ano2, lV66WWFeriadosDS_9_Feriado_nome2, AV67WWFeriadosDS_10_Feriado_data2, AV68WWFeriadosDS_11_Feriado_data_to2, AV71WWFeriadosDS_14_Ano3, lV72WWFeriadosDS_15_Feriado_nome3, AV73WWFeriadosDS_16_Feriado_data3, AV74WWFeriadosDS_17_Feriado_data_to3, AV75WWFeriadosDS_18_Tfferiado_data, AV76WWFeriadosDS_19_Tfferiado_data_to, lV77WWFeriadosDS_20_Tfferiado_nome, AV78WWFeriadosDS_21_Tfferiado_nome_sel});
         GRID_nRecordCount = H00I93_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV59WWFeriadosDS_2_Ano1 = AV30Ano1;
         AV60WWFeriadosDS_3_Feriado_nome1 = AV16Feriado_Nome1;
         AV61WWFeriadosDS_4_Feriado_data1 = AV28Feriado_Data1;
         AV62WWFeriadosDS_5_Feriado_data_to1 = AV29Feriado_Data_To1;
         AV63WWFeriadosDS_6_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV65WWFeriadosDS_8_Ano2 = AV31Ano2;
         AV66WWFeriadosDS_9_Feriado_nome2 = AV19Feriado_Nome2;
         AV67WWFeriadosDS_10_Feriado_data2 = AV32Feriado_Data2;
         AV68WWFeriadosDS_11_Feriado_data_to2 = AV33Feriado_Data_To2;
         AV69WWFeriadosDS_12_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWFeriadosDS_14_Ano3 = AV34Ano3;
         AV72WWFeriadosDS_15_Feriado_nome3 = AV22Feriado_Nome3;
         AV73WWFeriadosDS_16_Feriado_data3 = AV35Feriado_Data3;
         AV74WWFeriadosDS_17_Feriado_data_to3 = AV36Feriado_Data_To3;
         AV75WWFeriadosDS_18_Tfferiado_data = AV40TFFeriado_Data;
         AV76WWFeriadosDS_19_Tfferiado_data_to = AV41TFFeriado_Data_To;
         AV77WWFeriadosDS_20_Tfferiado_nome = AV46TFFeriado_Nome;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = AV47TFFeriado_Nome_Sel;
         AV79WWFeriadosDS_22_Tfferiado_fixo_sel = AV50TFFeriado_Fixo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV59WWFeriadosDS_2_Ano1 = AV30Ano1;
         AV60WWFeriadosDS_3_Feriado_nome1 = AV16Feriado_Nome1;
         AV61WWFeriadosDS_4_Feriado_data1 = AV28Feriado_Data1;
         AV62WWFeriadosDS_5_Feriado_data_to1 = AV29Feriado_Data_To1;
         AV63WWFeriadosDS_6_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV65WWFeriadosDS_8_Ano2 = AV31Ano2;
         AV66WWFeriadosDS_9_Feriado_nome2 = AV19Feriado_Nome2;
         AV67WWFeriadosDS_10_Feriado_data2 = AV32Feriado_Data2;
         AV68WWFeriadosDS_11_Feriado_data_to2 = AV33Feriado_Data_To2;
         AV69WWFeriadosDS_12_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWFeriadosDS_14_Ano3 = AV34Ano3;
         AV72WWFeriadosDS_15_Feriado_nome3 = AV22Feriado_Nome3;
         AV73WWFeriadosDS_16_Feriado_data3 = AV35Feriado_Data3;
         AV74WWFeriadosDS_17_Feriado_data_to3 = AV36Feriado_Data_To3;
         AV75WWFeriadosDS_18_Tfferiado_data = AV40TFFeriado_Data;
         AV76WWFeriadosDS_19_Tfferiado_data_to = AV41TFFeriado_Data_To;
         AV77WWFeriadosDS_20_Tfferiado_nome = AV46TFFeriado_Nome;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = AV47TFFeriado_Nome_Sel;
         AV79WWFeriadosDS_22_Tfferiado_fixo_sel = AV50TFFeriado_Fixo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV59WWFeriadosDS_2_Ano1 = AV30Ano1;
         AV60WWFeriadosDS_3_Feriado_nome1 = AV16Feriado_Nome1;
         AV61WWFeriadosDS_4_Feriado_data1 = AV28Feriado_Data1;
         AV62WWFeriadosDS_5_Feriado_data_to1 = AV29Feriado_Data_To1;
         AV63WWFeriadosDS_6_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV65WWFeriadosDS_8_Ano2 = AV31Ano2;
         AV66WWFeriadosDS_9_Feriado_nome2 = AV19Feriado_Nome2;
         AV67WWFeriadosDS_10_Feriado_data2 = AV32Feriado_Data2;
         AV68WWFeriadosDS_11_Feriado_data_to2 = AV33Feriado_Data_To2;
         AV69WWFeriadosDS_12_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWFeriadosDS_14_Ano3 = AV34Ano3;
         AV72WWFeriadosDS_15_Feriado_nome3 = AV22Feriado_Nome3;
         AV73WWFeriadosDS_16_Feriado_data3 = AV35Feriado_Data3;
         AV74WWFeriadosDS_17_Feriado_data_to3 = AV36Feriado_Data_To3;
         AV75WWFeriadosDS_18_Tfferiado_data = AV40TFFeriado_Data;
         AV76WWFeriadosDS_19_Tfferiado_data_to = AV41TFFeriado_Data_To;
         AV77WWFeriadosDS_20_Tfferiado_nome = AV46TFFeriado_Nome;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = AV47TFFeriado_Nome_Sel;
         AV79WWFeriadosDS_22_Tfferiado_fixo_sel = AV50TFFeriado_Fixo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV59WWFeriadosDS_2_Ano1 = AV30Ano1;
         AV60WWFeriadosDS_3_Feriado_nome1 = AV16Feriado_Nome1;
         AV61WWFeriadosDS_4_Feriado_data1 = AV28Feriado_Data1;
         AV62WWFeriadosDS_5_Feriado_data_to1 = AV29Feriado_Data_To1;
         AV63WWFeriadosDS_6_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV65WWFeriadosDS_8_Ano2 = AV31Ano2;
         AV66WWFeriadosDS_9_Feriado_nome2 = AV19Feriado_Nome2;
         AV67WWFeriadosDS_10_Feriado_data2 = AV32Feriado_Data2;
         AV68WWFeriadosDS_11_Feriado_data_to2 = AV33Feriado_Data_To2;
         AV69WWFeriadosDS_12_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWFeriadosDS_14_Ano3 = AV34Ano3;
         AV72WWFeriadosDS_15_Feriado_nome3 = AV22Feriado_Nome3;
         AV73WWFeriadosDS_16_Feriado_data3 = AV35Feriado_Data3;
         AV74WWFeriadosDS_17_Feriado_data_to3 = AV36Feriado_Data_To3;
         AV75WWFeriadosDS_18_Tfferiado_data = AV40TFFeriado_Data;
         AV76WWFeriadosDS_19_Tfferiado_data_to = AV41TFFeriado_Data_To;
         AV77WWFeriadosDS_20_Tfferiado_nome = AV46TFFeriado_Nome;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = AV47TFFeriado_Nome_Sel;
         AV79WWFeriadosDS_22_Tfferiado_fixo_sel = AV50TFFeriado_Fixo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV59WWFeriadosDS_2_Ano1 = AV30Ano1;
         AV60WWFeriadosDS_3_Feriado_nome1 = AV16Feriado_Nome1;
         AV61WWFeriadosDS_4_Feriado_data1 = AV28Feriado_Data1;
         AV62WWFeriadosDS_5_Feriado_data_to1 = AV29Feriado_Data_To1;
         AV63WWFeriadosDS_6_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV65WWFeriadosDS_8_Ano2 = AV31Ano2;
         AV66WWFeriadosDS_9_Feriado_nome2 = AV19Feriado_Nome2;
         AV67WWFeriadosDS_10_Feriado_data2 = AV32Feriado_Data2;
         AV68WWFeriadosDS_11_Feriado_data_to2 = AV33Feriado_Data_To2;
         AV69WWFeriadosDS_12_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWFeriadosDS_14_Ano3 = AV34Ano3;
         AV72WWFeriadosDS_15_Feriado_nome3 = AV22Feriado_Nome3;
         AV73WWFeriadosDS_16_Feriado_data3 = AV35Feriado_Data3;
         AV74WWFeriadosDS_17_Feriado_data_to3 = AV36Feriado_Data_To3;
         AV75WWFeriadosDS_18_Tfferiado_data = AV40TFFeriado_Data;
         AV76WWFeriadosDS_19_Tfferiado_data_to = AV41TFFeriado_Data_To;
         AV77WWFeriadosDS_20_Tfferiado_nome = AV46TFFeriado_Nome;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = AV47TFFeriado_Nome_Sel;
         AV79WWFeriadosDS_22_Tfferiado_fixo_sel = AV50TFFeriado_Fixo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         }
         return (int)(0) ;
      }

      protected void STRUPI90( )
      {
         /* Before Start, stand alone formulas. */
         AV82Pgmname = "WWFeriados";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26I92 */
         E26I92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV52DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFERIADO_DATATITLEFILTERDATA"), AV39Feriado_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFERIADO_NOMETITLEFILTERDATA"), AV45Feriado_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFERIADO_FIXOTITLEFILTERDATA"), AV49Feriado_FixoTitleFilterData);
            /* Read variables values. */
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAno1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAno1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vANO1");
               GX_FocusControl = edtavAno1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30Ano1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano1), 4, 0)));
            }
            else
            {
               AV30Ano1 = (short)(context.localUtil.CToN( cgiGet( edtavAno1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano1), 4, 0)));
            }
            AV16Feriado_Nome1 = StringUtil.Upper( cgiGet( edtavFeriado_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data1"}), 1, "vFERIADO_DATA1");
               GX_FocusControl = edtavFeriado_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28Feriado_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data1", context.localUtil.Format(AV28Feriado_Data1, "99/99/99"));
            }
            else
            {
               AV28Feriado_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data1", context.localUtil.Format(AV28Feriado_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data_To1"}), 1, "vFERIADO_DATA_TO1");
               GX_FocusControl = edtavFeriado_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29Feriado_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Feriado_Data_To1", context.localUtil.Format(AV29Feriado_Data_To1, "99/99/99"));
            }
            else
            {
               AV29Feriado_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Feriado_Data_To1", context.localUtil.Format(AV29Feriado_Data_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAno2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAno2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vANO2");
               GX_FocusControl = edtavAno2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31Ano2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Ano2), 4, 0)));
            }
            else
            {
               AV31Ano2 = (short)(context.localUtil.CToN( cgiGet( edtavAno2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Ano2), 4, 0)));
            }
            AV19Feriado_Nome2 = StringUtil.Upper( cgiGet( edtavFeriado_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data2"}), 1, "vFERIADO_DATA2");
               GX_FocusControl = edtavFeriado_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32Feriado_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data2", context.localUtil.Format(AV32Feriado_Data2, "99/99/99"));
            }
            else
            {
               AV32Feriado_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data2", context.localUtil.Format(AV32Feriado_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data_To2"}), 1, "vFERIADO_DATA_TO2");
               GX_FocusControl = edtavFeriado_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33Feriado_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Feriado_Data_To2", context.localUtil.Format(AV33Feriado_Data_To2, "99/99/99"));
            }
            else
            {
               AV33Feriado_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Feriado_Data_To2", context.localUtil.Format(AV33Feriado_Data_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAno3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAno3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vANO3");
               GX_FocusControl = edtavAno3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Ano3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Ano3), 4, 0)));
            }
            else
            {
               AV34Ano3 = (short)(context.localUtil.CToN( cgiGet( edtavAno3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Ano3), 4, 0)));
            }
            AV22Feriado_Nome3 = StringUtil.Upper( cgiGet( edtavFeriado_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data3"}), 1, "vFERIADO_DATA3");
               GX_FocusControl = edtavFeriado_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35Feriado_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data3", context.localUtil.Format(AV35Feriado_Data3, "99/99/99"));
            }
            else
            {
               AV35Feriado_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data3", context.localUtil.Format(AV35Feriado_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavFeriado_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Feriado_Data_To3"}), 1, "vFERIADO_DATA_TO3");
               GX_FocusControl = edtavFeriado_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Feriado_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Feriado_Data_To3", context.localUtil.Format(AV36Feriado_Data_To3, "99/99/99"));
            }
            else
            {
               AV36Feriado_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavFeriado_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Feriado_Data_To3", context.localUtil.Format(AV36Feriado_Data_To3, "99/99/99"));
            }
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfferiado_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFFeriado_Data"}), 1, "vTFFERIADO_DATA");
               GX_FocusControl = edtavTfferiado_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFFeriado_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFeriado_Data", context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"));
            }
            else
            {
               AV40TFFeriado_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfferiado_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFeriado_Data", context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfferiado_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFFeriado_Data_To"}), 1, "vTFFERIADO_DATA_TO");
               GX_FocusControl = edtavTfferiado_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFFeriado_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFFeriado_Data_To", context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"));
            }
            else
            {
               AV41TFFeriado_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfferiado_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFFeriado_Data_To", context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_feriado_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Feriado_Data Aux Date"}), 1, "vDDO_FERIADO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_feriado_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42DDO_Feriado_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_Feriado_DataAuxDate", context.localUtil.Format(AV42DDO_Feriado_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV42DDO_Feriado_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_feriado_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_Feriado_DataAuxDate", context.localUtil.Format(AV42DDO_Feriado_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_feriado_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Feriado_Data Aux Date To"}), 1, "vDDO_FERIADO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_feriado_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43DDO_Feriado_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43DDO_Feriado_DataAuxDateTo", context.localUtil.Format(AV43DDO_Feriado_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV43DDO_Feriado_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_feriado_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43DDO_Feriado_DataAuxDateTo", context.localUtil.Format(AV43DDO_Feriado_DataAuxDateTo, "99/99/99"));
            }
            AV46TFFeriado_Nome = StringUtil.Upper( cgiGet( edtavTfferiado_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFeriado_Nome", AV46TFFeriado_Nome);
            AV47TFFeriado_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfferiado_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Nome_Sel", AV47TFFeriado_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfferiado_fixo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfferiado_fixo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFERIADO_FIXO_SEL");
               GX_FocusControl = edtavTfferiado_fixo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFFeriado_Fixo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0));
            }
            else
            {
               AV50TFFeriado_Fixo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfferiado_fixo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0));
            }
            AV44ddo_Feriado_DataTitleControlIdToReplace = cgiGet( edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Feriado_DataTitleControlIdToReplace", AV44ddo_Feriado_DataTitleControlIdToReplace);
            AV48ddo_Feriado_NomeTitleControlIdToReplace = cgiGet( edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Feriado_NomeTitleControlIdToReplace", AV48ddo_Feriado_NomeTitleControlIdToReplace);
            AV51ddo_Feriado_FixoTitleControlIdToReplace = cgiGet( edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Feriado_FixoTitleControlIdToReplace", AV51ddo_Feriado_FixoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_95 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_95"), ",", "."));
            AV54GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV55GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_feriado_data_Caption = cgiGet( "DDO_FERIADO_DATA_Caption");
            Ddo_feriado_data_Tooltip = cgiGet( "DDO_FERIADO_DATA_Tooltip");
            Ddo_feriado_data_Cls = cgiGet( "DDO_FERIADO_DATA_Cls");
            Ddo_feriado_data_Filteredtext_set = cgiGet( "DDO_FERIADO_DATA_Filteredtext_set");
            Ddo_feriado_data_Filteredtextto_set = cgiGet( "DDO_FERIADO_DATA_Filteredtextto_set");
            Ddo_feriado_data_Dropdownoptionstype = cgiGet( "DDO_FERIADO_DATA_Dropdownoptionstype");
            Ddo_feriado_data_Titlecontrolidtoreplace = cgiGet( "DDO_FERIADO_DATA_Titlecontrolidtoreplace");
            Ddo_feriado_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includesortasc"));
            Ddo_feriado_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includesortdsc"));
            Ddo_feriado_data_Sortedstatus = cgiGet( "DDO_FERIADO_DATA_Sortedstatus");
            Ddo_feriado_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includefilter"));
            Ddo_feriado_data_Filtertype = cgiGet( "DDO_FERIADO_DATA_Filtertype");
            Ddo_feriado_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Filterisrange"));
            Ddo_feriado_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_DATA_Includedatalist"));
            Ddo_feriado_data_Sortasc = cgiGet( "DDO_FERIADO_DATA_Sortasc");
            Ddo_feriado_data_Sortdsc = cgiGet( "DDO_FERIADO_DATA_Sortdsc");
            Ddo_feriado_data_Cleanfilter = cgiGet( "DDO_FERIADO_DATA_Cleanfilter");
            Ddo_feriado_data_Rangefilterfrom = cgiGet( "DDO_FERIADO_DATA_Rangefilterfrom");
            Ddo_feriado_data_Rangefilterto = cgiGet( "DDO_FERIADO_DATA_Rangefilterto");
            Ddo_feriado_data_Searchbuttontext = cgiGet( "DDO_FERIADO_DATA_Searchbuttontext");
            Ddo_feriado_nome_Caption = cgiGet( "DDO_FERIADO_NOME_Caption");
            Ddo_feriado_nome_Tooltip = cgiGet( "DDO_FERIADO_NOME_Tooltip");
            Ddo_feriado_nome_Cls = cgiGet( "DDO_FERIADO_NOME_Cls");
            Ddo_feriado_nome_Filteredtext_set = cgiGet( "DDO_FERIADO_NOME_Filteredtext_set");
            Ddo_feriado_nome_Selectedvalue_set = cgiGet( "DDO_FERIADO_NOME_Selectedvalue_set");
            Ddo_feriado_nome_Dropdownoptionstype = cgiGet( "DDO_FERIADO_NOME_Dropdownoptionstype");
            Ddo_feriado_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FERIADO_NOME_Titlecontrolidtoreplace");
            Ddo_feriado_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includesortasc"));
            Ddo_feriado_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includesortdsc"));
            Ddo_feriado_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includefilter"));
            Ddo_feriado_nome_Filtertype = cgiGet( "DDO_FERIADO_NOME_Filtertype");
            Ddo_feriado_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Filterisrange"));
            Ddo_feriado_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_NOME_Includedatalist"));
            Ddo_feriado_nome_Datalisttype = cgiGet( "DDO_FERIADO_NOME_Datalisttype");
            Ddo_feriado_nome_Datalistproc = cgiGet( "DDO_FERIADO_NOME_Datalistproc");
            Ddo_feriado_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FERIADO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_feriado_nome_Loadingdata = cgiGet( "DDO_FERIADO_NOME_Loadingdata");
            Ddo_feriado_nome_Cleanfilter = cgiGet( "DDO_FERIADO_NOME_Cleanfilter");
            Ddo_feriado_nome_Noresultsfound = cgiGet( "DDO_FERIADO_NOME_Noresultsfound");
            Ddo_feriado_nome_Searchbuttontext = cgiGet( "DDO_FERIADO_NOME_Searchbuttontext");
            Ddo_feriado_fixo_Caption = cgiGet( "DDO_FERIADO_FIXO_Caption");
            Ddo_feriado_fixo_Tooltip = cgiGet( "DDO_FERIADO_FIXO_Tooltip");
            Ddo_feriado_fixo_Cls = cgiGet( "DDO_FERIADO_FIXO_Cls");
            Ddo_feriado_fixo_Selectedvalue_set = cgiGet( "DDO_FERIADO_FIXO_Selectedvalue_set");
            Ddo_feriado_fixo_Dropdownoptionstype = cgiGet( "DDO_FERIADO_FIXO_Dropdownoptionstype");
            Ddo_feriado_fixo_Titlecontrolidtoreplace = cgiGet( "DDO_FERIADO_FIXO_Titlecontrolidtoreplace");
            Ddo_feriado_fixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includesortasc"));
            Ddo_feriado_fixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includesortdsc"));
            Ddo_feriado_fixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includefilter"));
            Ddo_feriado_fixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FERIADO_FIXO_Includedatalist"));
            Ddo_feriado_fixo_Datalisttype = cgiGet( "DDO_FERIADO_FIXO_Datalisttype");
            Ddo_feriado_fixo_Datalistfixedvalues = cgiGet( "DDO_FERIADO_FIXO_Datalistfixedvalues");
            Ddo_feriado_fixo_Cleanfilter = cgiGet( "DDO_FERIADO_FIXO_Cleanfilter");
            Ddo_feriado_fixo_Searchbuttontext = cgiGet( "DDO_FERIADO_FIXO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_feriado_data_Activeeventkey = cgiGet( "DDO_FERIADO_DATA_Activeeventkey");
            Ddo_feriado_data_Filteredtext_get = cgiGet( "DDO_FERIADO_DATA_Filteredtext_get");
            Ddo_feriado_data_Filteredtextto_get = cgiGet( "DDO_FERIADO_DATA_Filteredtextto_get");
            Ddo_feriado_nome_Activeeventkey = cgiGet( "DDO_FERIADO_NOME_Activeeventkey");
            Ddo_feriado_nome_Filteredtext_get = cgiGet( "DDO_FERIADO_NOME_Filteredtext_get");
            Ddo_feriado_nome_Selectedvalue_get = cgiGet( "DDO_FERIADO_NOME_Selectedvalue_get");
            Ddo_feriado_fixo_Activeeventkey = cgiGet( "DDO_FERIADO_FIXO_Activeeventkey");
            Ddo_feriado_fixo_Selectedvalue_get = cgiGet( "DDO_FERIADO_FIXO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO1"), ",", ".") != Convert.ToDecimal( AV30Ano1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME1"), AV16Feriado_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA1"), 0) != AV28Feriado_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO1"), 0) != AV29Feriado_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO2"), ",", ".") != Convert.ToDecimal( AV31Ano2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME2"), AV19Feriado_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA2"), 0) != AV32Feriado_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO2"), 0) != AV33Feriado_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vANO3"), ",", ".") != Convert.ToDecimal( AV34Ano3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFERIADO_NOME3"), AV22Feriado_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA3"), 0) != AV35Feriado_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vFERIADO_DATA_TO3"), 0) != AV36Feriado_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA"), 0) != AV40TFFeriado_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFFERIADO_DATA_TO"), 0) != AV41TFFeriado_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME"), AV46TFFeriado_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFERIADO_NOME_SEL"), AV47TFFeriado_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFERIADO_FIXO_SEL"), ",", ".") != Convert.ToDecimal( AV50TFFeriado_Fixo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26I92 */
         E26I92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26I92( )
      {
         /* Start Routine */
         subGrid_Rows = 15;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfferiado_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_data_Visible), 5, 0)));
         edtavTfferiado_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_data_to_Visible), 5, 0)));
         edtavTfferiado_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_nome_Visible), 5, 0)));
         edtavTfferiado_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_nome_sel_Visible), 5, 0)));
         edtavTfferiado_fixo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfferiado_fixo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfferiado_fixo_sel_Visible), 5, 0)));
         Ddo_feriado_data_Titlecontrolidtoreplace = subGrid_Internalname+"_Feriado_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "TitleControlIdToReplace", Ddo_feriado_data_Titlecontrolidtoreplace);
         AV44ddo_Feriado_DataTitleControlIdToReplace = Ddo_feriado_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Feriado_DataTitleControlIdToReplace", AV44ddo_Feriado_DataTitleControlIdToReplace);
         edtavDdo_feriado_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_feriado_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_feriado_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Feriado_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "TitleControlIdToReplace", Ddo_feriado_nome_Titlecontrolidtoreplace);
         AV48ddo_Feriado_NomeTitleControlIdToReplace = Ddo_feriado_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Feriado_NomeTitleControlIdToReplace", AV48ddo_Feriado_NomeTitleControlIdToReplace);
         edtavDdo_feriado_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_feriado_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_feriado_fixo_Titlecontrolidtoreplace = subGrid_Internalname+"_Feriado_Fixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "TitleControlIdToReplace", Ddo_feriado_fixo_Titlecontrolidtoreplace);
         AV51ddo_Feriado_FixoTitleControlIdToReplace = Ddo_feriado_fixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Feriado_FixoTitleControlIdToReplace", AV51ddo_Feriado_FixoTitleControlIdToReplace);
         edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Feriados";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV52DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV52DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         AV30Ano1 = (short)(DateTimeUtil.Year( DateTimeUtil.ServerDate( context, "DEFAULT")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano1), 4, 0)));
      }

      protected void E27I92( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV39Feriado_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45Feriado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Feriado_FixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFeriado_Data_Titleformat = 2;
         edtFeriado_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV44ddo_Feriado_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Data_Internalname, "Title", edtFeriado_Data_Title);
         edtFeriado_Nome_Titleformat = 2;
         edtFeriado_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV48ddo_Feriado_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Nome_Internalname, "Title", edtFeriado_Nome_Title);
         chkFeriado_Fixo_Titleformat = 2;
         chkFeriado_Fixo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fixo", AV51ddo_Feriado_FixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFeriado_Fixo_Internalname, "Title", chkFeriado_Fixo.Title.Text);
         AV54GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridCurrentPage), 10, 0)));
         AV55GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridPageCount), 10, 0)));
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV59WWFeriadosDS_2_Ano1 = AV30Ano1;
         AV60WWFeriadosDS_3_Feriado_nome1 = AV16Feriado_Nome1;
         AV61WWFeriadosDS_4_Feriado_data1 = AV28Feriado_Data1;
         AV62WWFeriadosDS_5_Feriado_data_to1 = AV29Feriado_Data_To1;
         AV63WWFeriadosDS_6_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV65WWFeriadosDS_8_Ano2 = AV31Ano2;
         AV66WWFeriadosDS_9_Feriado_nome2 = AV19Feriado_Nome2;
         AV67WWFeriadosDS_10_Feriado_data2 = AV32Feriado_Data2;
         AV68WWFeriadosDS_11_Feriado_data_to2 = AV33Feriado_Data_To2;
         AV69WWFeriadosDS_12_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV71WWFeriadosDS_14_Ano3 = AV34Ano3;
         AV72WWFeriadosDS_15_Feriado_nome3 = AV22Feriado_Nome3;
         AV73WWFeriadosDS_16_Feriado_data3 = AV35Feriado_Data3;
         AV74WWFeriadosDS_17_Feriado_data_to3 = AV36Feriado_Data_To3;
         AV75WWFeriadosDS_18_Tfferiado_data = AV40TFFeriado_Data;
         AV76WWFeriadosDS_19_Tfferiado_data_to = AV41TFFeriado_Data_To;
         AV77WWFeriadosDS_20_Tfferiado_nome = AV46TFFeriado_Nome;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = AV47TFFeriado_Nome_Sel;
         AV79WWFeriadosDS_22_Tfferiado_fixo_sel = AV50TFFeriado_Fixo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39Feriado_DataTitleFilterData", AV39Feriado_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45Feriado_NomeTitleFilterData", AV45Feriado_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49Feriado_FixoTitleFilterData", AV49Feriado_FixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11I92( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV53PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV53PageToGo) ;
         }
      }

      protected void E12I92( )
      {
         /* Ddo_feriado_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_feriado_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "SortedStatus", Ddo_feriado_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_feriado_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "SortedStatus", Ddo_feriado_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_feriado_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFFeriado_Data = context.localUtil.CToD( Ddo_feriado_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFeriado_Data", context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"));
            AV41TFFeriado_Data_To = context.localUtil.CToD( Ddo_feriado_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFFeriado_Data_To", context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13I92( )
      {
         /* Ddo_feriado_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_feriado_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFFeriado_Nome = Ddo_feriado_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFeriado_Nome", AV46TFFeriado_Nome);
            AV47TFFeriado_Nome_Sel = Ddo_feriado_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Nome_Sel", AV47TFFeriado_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14I92( )
      {
         /* Ddo_feriado_fixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_feriado_fixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFFeriado_Fixo_Sel = (short)(NumberUtil.Val( Ddo_feriado_fixo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      private void E28I92( )
      {
         /* Grid_Load Routine */
         AV25Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
         AV80Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("feriados.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A1175Feriado_Data));
         AV26Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
         AV81Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("feriados.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A1175Feriado_Data));
         edtFeriado_Nome_Link = formatLink("viewferiados.aspx") + "?" + UrlEncode(DateTimeUtil.FormatDateParm(A1175Feriado_Data)) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 95;
         }
         sendrow_952( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_95_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(95, GridRow);
         }
      }

      protected void E21I92( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15I92( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22I92( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23I92( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16I92( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24I92( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17I92( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV30Ano1, AV16Feriado_Nome1, AV28Feriado_Data1, AV29Feriado_Data_To1, AV18DynamicFiltersSelector2, AV31Ano2, AV19Feriado_Nome2, AV32Feriado_Data2, AV33Feriado_Data_To2, AV21DynamicFiltersSelector3, AV34Ano3, AV22Feriado_Nome3, AV35Feriado_Data3, AV36Feriado_Data_To3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV40TFFeriado_Data, AV41TFFeriado_Data_To, AV46TFFeriado_Nome, AV47TFFeriado_Nome_Sel, AV50TFFeriado_Fixo_Sel, AV44ddo_Feriado_DataTitleControlIdToReplace, AV48ddo_Feriado_NomeTitleControlIdToReplace, AV51ddo_Feriado_FixoTitleControlIdToReplace, AV82Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A1175Feriado_Data) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25I92( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18I92( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E19I92( )
      {
         /* 'DoImportar' Routine */
         context.PopUp(formatLink("webpanel1.aspx") , new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void E20I92( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("feriados.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode(DateTimeUtil.FormatDateParm(DateTime.MinValue));
         context.wjLocDisableFrm = 1;
      }

      protected void S212( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_feriado_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "SortedStatus", Ddo_feriado_data_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAno1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno1_Visible), 5, 0)));
         edtavFeriado_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome1_Visible), 5, 0)));
         tblTablemergeddynamicfiltersferiado_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 )
         {
            edtavAno1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 )
         {
            edtavFeriado_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersferiado_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAno2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno2_Visible), 5, 0)));
         edtavFeriado_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome2_Visible), 5, 0)));
         tblTablemergeddynamicfiltersferiado_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 )
         {
            edtavAno2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 )
         {
            edtavFeriado_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersferiado_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavAno3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno3_Visible), 5, 0)));
         edtavFeriado_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome3_Visible), 5, 0)));
         tblTablemergeddynamicfiltersferiado_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 )
         {
            edtavAno3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAno3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAno3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 )
         {
            edtavFeriado_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFeriado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFeriado_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersferiado_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersferiado_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersferiado_data3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV31Ano2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Ano2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV34Ano3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Ano3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV40TFFeriado_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFeriado_Data", context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"));
         Ddo_feriado_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "FilteredText_set", Ddo_feriado_data_Filteredtext_set);
         AV41TFFeriado_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFFeriado_Data_To", context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"));
         Ddo_feriado_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "FilteredTextTo_set", Ddo_feriado_data_Filteredtextto_set);
         AV46TFFeriado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFeriado_Nome", AV46TFFeriado_Nome);
         Ddo_feriado_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "FilteredText_set", Ddo_feriado_nome_Filteredtext_set);
         AV47TFFeriado_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Nome_Sel", AV47TFFeriado_Nome_Sel);
         Ddo_feriado_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "SelectedValue_set", Ddo_feriado_nome_Selectedvalue_set);
         AV50TFFeriado_Fixo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0));
         Ddo_feriado_fixo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "SelectedValue_set", Ddo_feriado_fixo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "ANO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV30Ano1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV82Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV82Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV27Session.Get(AV82Pgmname+"GridState"), "");
         }
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV83GXV1 = 1;
         while ( AV83GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV83GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFERIADO_DATA") == 0 )
            {
               AV40TFFeriado_Data = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFeriado_Data", context.localUtil.Format(AV40TFFeriado_Data, "99/99/99"));
               AV41TFFeriado_Data_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFFeriado_Data_To", context.localUtil.Format(AV41TFFeriado_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV40TFFeriado_Data) )
               {
                  Ddo_feriado_data_Filteredtext_set = context.localUtil.DToC( AV40TFFeriado_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "FilteredText_set", Ddo_feriado_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV41TFFeriado_Data_To) )
               {
                  Ddo_feriado_data_Filteredtextto_set = context.localUtil.DToC( AV41TFFeriado_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_data_Internalname, "FilteredTextTo_set", Ddo_feriado_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFERIADO_NOME") == 0 )
            {
               AV46TFFeriado_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFeriado_Nome", AV46TFFeriado_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFFeriado_Nome)) )
               {
                  Ddo_feriado_nome_Filteredtext_set = AV46TFFeriado_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "FilteredText_set", Ddo_feriado_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFERIADO_NOME_SEL") == 0 )
            {
               AV47TFFeriado_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFeriado_Nome_Sel", AV47TFFeriado_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFeriado_Nome_Sel)) )
               {
                  Ddo_feriado_nome_Selectedvalue_set = AV47TFFeriado_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_nome_Internalname, "SelectedValue_set", Ddo_feriado_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFERIADO_FIXO_SEL") == 0 )
            {
               AV50TFFeriado_Fixo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFFeriado_Fixo_Sel", StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0));
               if ( ! (0==AV50TFFeriado_Fixo_Sel) )
               {
                  Ddo_feriado_fixo_Selectedvalue_set = StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_feriado_fixo_Internalname, "SelectedValue_set", Ddo_feriado_fixo_Selectedvalue_set);
               }
            }
            AV83GXV1 = (int)(AV83GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 )
            {
               AV30Ano1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Ano1), 4, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 )
            {
               AV16Feriado_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Feriado_Nome1", AV16Feriado_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 )
            {
               AV28Feriado_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Feriado_Data1", context.localUtil.Format(AV28Feriado_Data1, "99/99/99"));
               AV29Feriado_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Feriado_Data_To1", context.localUtil.Format(AV29Feriado_Data_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 )
               {
                  AV31Ano2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Ano2), 4, 0)));
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 )
               {
                  AV19Feriado_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Feriado_Nome2", AV19Feriado_Nome2);
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 )
               {
                  AV32Feriado_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Feriado_Data2", context.localUtil.Format(AV32Feriado_Data2, "99/99/99"));
                  AV33Feriado_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Feriado_Data_To2", context.localUtil.Format(AV33Feriado_Data_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 )
                  {
                     AV34Ano3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Ano3), 4, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 )
                  {
                     AV22Feriado_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Feriado_Nome3", AV22Feriado_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 )
                  {
                     AV35Feriado_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Feriado_Data3", context.localUtil.Format(AV35Feriado_Data3, "99/99/99"));
                     AV36Feriado_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Feriado_Data_To3", context.localUtil.Format(AV36Feriado_Data_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV27Session.Get(AV82Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV40TFFeriado_Data) && (DateTime.MinValue==AV41TFFeriado_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV40TFFeriado_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV41TFFeriado_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFFeriado_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFFeriado_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFeriado_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFFeriado_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV50TFFeriado_Fixo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFERIADO_FIXO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV50TFFeriado_Fixo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV82Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ANO") == 0 ) && ! (0==AV30Ano1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV30Ano1), 4, 0);
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Feriado_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16Feriado_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FERIADO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV28Feriado_Data1) && (DateTime.MinValue==AV29Feriado_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV28Feriado_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV29Feriado_Data_To1, 2, "/");
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "ANO") == 0 ) && ! (0==AV31Ano2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV31Ano2), 4, 0);
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Feriado_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Feriado_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FERIADO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV32Feriado_Data2) && (DateTime.MinValue==AV33Feriado_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV32Feriado_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV33Feriado_Data_To2, 2, "/");
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "ANO") == 0 ) && ! (0==AV34Ano3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV34Ano3), 4, 0);
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Feriado_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Feriado_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FERIADO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV35Feriado_Data3) && (DateTime.MinValue==AV36Feriado_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV35Feriado_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV36Feriado_Data_To3, 2, "/");
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV82Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Feriados";
         AV27Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_I92( true) ;
         }
         else
         {
            wb_table2_8_I92( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_89_I92( true) ;
         }
         else
         {
            wb_table3_89_I92( false) ;
         }
         return  ;
      }

      protected void wb_table3_89_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_I92e( true) ;
         }
         else
         {
            wb_table1_2_I92e( false) ;
         }
      }

      protected void wb_table3_89_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 10, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_92_I92( true) ;
         }
         else
         {
            wb_table4_92_I92( false) ;
         }
         return  ;
      }

      protected void wb_table4_92_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnimportar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(95), 2, 0)+","+"null"+");", "Importar", bttBtnimportar_Jsonclick, 5, "Importar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOIMPORTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_89_I92e( true) ;
         }
         else
         {
            wb_table3_89_I92e( false) ;
         }
      }

      protected void wb_table4_92_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"95\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFeriado_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtFeriado_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFeriado_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFeriado_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFeriado_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFeriado_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkFeriado_Fixo_Titleformat == 0 )
               {
                  context.SendWebValue( chkFeriado_Fixo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkFeriado_Fixo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFeriado_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFeriado_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1176Feriado_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFeriado_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFeriado_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFeriado_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1195Feriado_Fixo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkFeriado_Fixo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkFeriado_Fixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 95 )
         {
            wbEnd = 0;
            nRC_GXsfl_95 = (short)(nGXsfl_95_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_92_I92e( true) ;
         }
         else
         {
            wb_table4_92_I92e( false) ;
         }
      }

      protected void wb_table2_8_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFeriadostitle_Internalname, "Feriados", "", "", lblFeriadostitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_I92( true) ;
         }
         else
         {
            wb_table5_13_I92( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_18_I92( true) ;
         }
         else
         {
            wb_table6_18_I92( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_I92e( true) ;
         }
         else
         {
            wb_table2_8_I92e( false) ;
         }
      }

      protected void wb_table6_18_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_23_I92( true) ;
         }
         else
         {
            wb_table7_23_I92( false) ;
         }
         return  ;
      }

      protected void wb_table7_23_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_I92e( true) ;
         }
         else
         {
            wb_table6_18_I92e( false) ;
         }
      }

      protected void wb_table7_23_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_95_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WWFeriados.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAno1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30Ano1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV30Ano1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAno1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAno1_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_WWFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFeriado_nome1_Internalname, StringUtil.RTrim( AV16Feriado_Nome1), StringUtil.RTrim( context.localUtil.Format( AV16Feriado_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFeriado_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFeriados.htm");
            wb_table8_34_I92( true) ;
         }
         else
         {
            wb_table8_34_I92( false) ;
         }
         return  ;
      }

      protected void wb_table8_34_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFeriados.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_95_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WWFeriados.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAno2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31Ano2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31Ano2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAno2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAno2_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_WWFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFeriado_nome2_Internalname, StringUtil.RTrim( AV19Feriado_Nome2), StringUtil.RTrim( context.localUtil.Format( AV19Feriado_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFeriado_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFeriados.htm");
            wb_table9_55_I92( true) ;
         }
         else
         {
            wb_table9_55_I92( false) ;
         }
         return  ;
      }

      protected void wb_table9_55_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFeriados.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_95_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_WWFeriados.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAno3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Ano3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34Ano3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAno3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAno3_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_WWFeriados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFeriado_nome3_Internalname, StringUtil.RTrim( AV22Feriado_Nome3), StringUtil.RTrim( context.localUtil.Format( AV22Feriado_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFeriado_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFeriados.htm");
            wb_table10_76_I92( true) ;
         }
         else
         {
            wb_table10_76_I92( false) ;
         }
         return  ;
      }

      protected void wb_table10_76_I92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_23_I92e( true) ;
         }
         else
         {
            wb_table7_23_I92e( false) ;
         }
      }

      protected void wb_table10_76_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersferiado_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersferiado_data3_Internalname, tblTablemergeddynamicfiltersferiado_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data3_Internalname, context.localUtil.Format(AV35Feriado_Data3, "99/99/99"), context.localUtil.Format( AV35Feriado_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersferiado_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersferiado_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data_to3_Internalname, context.localUtil.Format(AV36Feriado_Data_To3, "99/99/99"), context.localUtil.Format( AV36Feriado_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_76_I92e( true) ;
         }
         else
         {
            wb_table10_76_I92e( false) ;
         }
      }

      protected void wb_table9_55_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersferiado_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersferiado_data2_Internalname, tblTablemergeddynamicfiltersferiado_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data2_Internalname, context.localUtil.Format(AV32Feriado_Data2, "99/99/99"), context.localUtil.Format( AV32Feriado_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersferiado_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersferiado_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data_to2_Internalname, context.localUtil.Format(AV33Feriado_Data_To2, "99/99/99"), context.localUtil.Format( AV33Feriado_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_55_I92e( true) ;
         }
         else
         {
            wb_table9_55_I92e( false) ;
         }
      }

      protected void wb_table8_34_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersferiado_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersferiado_data1_Internalname, tblTablemergeddynamicfiltersferiado_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data1_Internalname, context.localUtil.Format(AV28Feriado_Data1, "99/99/99"), context.localUtil.Format( AV28Feriado_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersferiado_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersferiado_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_95_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavFeriado_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavFeriado_data_to1_Internalname, context.localUtil.Format(AV29Feriado_Data_To1, "99/99/99"), context.localUtil.Format( AV29Feriado_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFeriado_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFeriados.htm");
            GxWebStd.gx_bitmap( context, edtavFeriado_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_34_I92e( true) ;
         }
         else
         {
            wb_table8_34_I92e( false) ;
         }
      }

      protected void wb_table5_13_I92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFeriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_I92e( true) ;
         }
         else
         {
            wb_table5_13_I92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAI92( ) ;
         WSI92( ) ;
         WEI92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118561032");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwferiados.js", "?20203118561032");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_952( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_95_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_95_idx;
         edtFeriado_Data_Internalname = "FERIADO_DATA_"+sGXsfl_95_idx;
         edtFeriado_Nome_Internalname = "FERIADO_NOME_"+sGXsfl_95_idx;
         chkFeriado_Fixo_Internalname = "FERIADO_FIXO_"+sGXsfl_95_idx;
      }

      protected void SubsflControlProps_fel_952( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_95_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_95_fel_idx;
         edtFeriado_Data_Internalname = "FERIADO_DATA_"+sGXsfl_95_fel_idx;
         edtFeriado_Nome_Internalname = "FERIADO_NOME_"+sGXsfl_95_fel_idx;
         chkFeriado_Fixo_Internalname = "FERIADO_FIXO_"+sGXsfl_95_fel_idx;
      }

      protected void sendrow_952( )
      {
         SubsflControlProps_952( ) ;
         WBI90( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_95_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_95_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_95_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV25Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV80Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV80Update_GXI : context.PathToRelativeUrl( AV25Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV25Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV81Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV81Delete_GXI : context.PathToRelativeUrl( AV26Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFeriado_Data_Internalname,context.localUtil.Format(A1175Feriado_Data, "99/99/99"),context.localUtil.Format( A1175Feriado_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFeriado_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)95,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFeriado_Nome_Internalname,StringUtil.RTrim( A1176Feriado_Nome),StringUtil.RTrim( context.localUtil.Format( A1176Feriado_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFeriado_Nome_Link,(String)"",(String)"",(String)"",(String)edtFeriado_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)95,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkFeriado_Fixo_Internalname,StringUtil.BoolToStr( A1195Feriado_Fixo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_DATA"+"_"+sGXsfl_95_idx, GetSecureSignedToken( sGXsfl_95_idx, A1175Feriado_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_NOME"+"_"+sGXsfl_95_idx, GetSecureSignedToken( sGXsfl_95_idx, StringUtil.RTrim( context.localUtil.Format( A1176Feriado_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_FERIADO_FIXO"+"_"+sGXsfl_95_idx, GetSecureSignedToken( sGXsfl_95_idx, A1195Feriado_Fixo));
            GridContainer.AddRow(GridRow);
            nGXsfl_95_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_95_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_95_idx+1));
            sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
            SubsflControlProps_952( ) ;
         }
         /* End function sendrow_952 */
      }

      protected void init_default_properties( )
      {
         lblFeriadostitle_Internalname = "FERIADOSTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavAno1_Internalname = "vANO1";
         edtavFeriado_nome1_Internalname = "vFERIADO_NOME1";
         edtavFeriado_data1_Internalname = "vFERIADO_DATA1";
         lblDynamicfiltersferiado_data_rangemiddletext1_Internalname = "DYNAMICFILTERSFERIADO_DATA_RANGEMIDDLETEXT1";
         edtavFeriado_data_to1_Internalname = "vFERIADO_DATA_TO1";
         tblTablemergeddynamicfiltersferiado_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavAno2_Internalname = "vANO2";
         edtavFeriado_nome2_Internalname = "vFERIADO_NOME2";
         edtavFeriado_data2_Internalname = "vFERIADO_DATA2";
         lblDynamicfiltersferiado_data_rangemiddletext2_Internalname = "DYNAMICFILTERSFERIADO_DATA_RANGEMIDDLETEXT2";
         edtavFeriado_data_to2_Internalname = "vFERIADO_DATA_TO2";
         tblTablemergeddynamicfiltersferiado_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavAno3_Internalname = "vANO3";
         edtavFeriado_nome3_Internalname = "vFERIADO_NOME3";
         edtavFeriado_data3_Internalname = "vFERIADO_DATA3";
         lblDynamicfiltersferiado_data_rangemiddletext3_Internalname = "DYNAMICFILTERSFERIADO_DATA_RANGEMIDDLETEXT3";
         edtavFeriado_data_to3_Internalname = "vFERIADO_DATA_TO3";
         tblTablemergeddynamicfiltersferiado_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtFeriado_Data_Internalname = "FERIADO_DATA";
         edtFeriado_Nome_Internalname = "FERIADO_NOME";
         chkFeriado_Fixo_Internalname = "FERIADO_FIXO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         bttBtnimportar_Internalname = "BTNIMPORTAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         edtavTfferiado_data_Internalname = "vTFFERIADO_DATA";
         edtavTfferiado_data_to_Internalname = "vTFFERIADO_DATA_TO";
         edtavDdo_feriado_dataauxdate_Internalname = "vDDO_FERIADO_DATAAUXDATE";
         edtavDdo_feriado_dataauxdateto_Internalname = "vDDO_FERIADO_DATAAUXDATETO";
         divDdo_feriado_dataauxdates_Internalname = "DDO_FERIADO_DATAAUXDATES";
         edtavTfferiado_nome_Internalname = "vTFFERIADO_NOME";
         edtavTfferiado_nome_sel_Internalname = "vTFFERIADO_NOME_SEL";
         edtavTfferiado_fixo_sel_Internalname = "vTFFERIADO_FIXO_SEL";
         Ddo_feriado_data_Internalname = "DDO_FERIADO_DATA";
         edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname = "vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE";
         Ddo_feriado_nome_Internalname = "DDO_FERIADO_NOME";
         edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname = "vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_feriado_fixo_Internalname = "DDO_FERIADO_FIXO";
         edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname = "vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFeriado_Nome_Jsonclick = "";
         edtFeriado_Data_Jsonclick = "";
         edtavFeriado_data_to1_Jsonclick = "";
         edtavFeriado_data1_Jsonclick = "";
         edtavFeriado_data_to2_Jsonclick = "";
         edtavFeriado_data2_Jsonclick = "";
         edtavFeriado_data_to3_Jsonclick = "";
         edtavFeriado_data3_Jsonclick = "";
         edtavFeriado_nome3_Jsonclick = "";
         edtavAno3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavFeriado_nome2_Jsonclick = "";
         edtavAno2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavFeriado_nome1_Jsonclick = "";
         edtavAno1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFeriado_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkFeriado_Fixo_Titleformat = 0;
         edtFeriado_Nome_Titleformat = 0;
         edtFeriado_Data_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfiltersferiado_data3_Visible = 1;
         edtavFeriado_nome3_Visible = 1;
         edtavAno3_Visible = 1;
         tblTablemergeddynamicfiltersferiado_data2_Visible = 1;
         edtavFeriado_nome2_Visible = 1;
         edtavAno2_Visible = 1;
         tblTablemergeddynamicfiltersferiado_data1_Visible = 1;
         edtavFeriado_nome1_Visible = 1;
         edtavAno1_Visible = 1;
         chkFeriado_Fixo.Title.Text = "Fixo";
         edtFeriado_Nome_Title = "Descri��o";
         edtFeriado_Data_Title = "Data";
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkFeriado_Fixo.Caption = "";
         edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_feriado_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_feriado_datatitlecontrolidtoreplace_Visible = 1;
         edtavTfferiado_fixo_sel_Jsonclick = "";
         edtavTfferiado_fixo_sel_Visible = 1;
         edtavTfferiado_nome_sel_Jsonclick = "";
         edtavTfferiado_nome_sel_Visible = 1;
         edtavTfferiado_nome_Jsonclick = "";
         edtavTfferiado_nome_Visible = 1;
         edtavDdo_feriado_dataauxdateto_Jsonclick = "";
         edtavDdo_feriado_dataauxdate_Jsonclick = "";
         edtavTfferiado_data_to_Jsonclick = "";
         edtavTfferiado_data_to_Visible = 1;
         edtavTfferiado_data_Jsonclick = "";
         edtavTfferiado_data_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_feriado_fixo_Searchbuttontext = "Pesquisar";
         Ddo_feriado_fixo_Cleanfilter = "Limpar pesquisa";
         Ddo_feriado_fixo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_feriado_fixo_Datalisttype = "FixedValues";
         Ddo_feriado_fixo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_feriado_fixo_Includefilter = Convert.ToBoolean( 0);
         Ddo_feriado_fixo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_feriado_fixo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_feriado_fixo_Titlecontrolidtoreplace = "";
         Ddo_feriado_fixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_feriado_fixo_Cls = "ColumnSettings";
         Ddo_feriado_fixo_Tooltip = "Op��es";
         Ddo_feriado_fixo_Caption = "";
         Ddo_feriado_nome_Searchbuttontext = "Pesquisar";
         Ddo_feriado_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_feriado_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_feriado_nome_Loadingdata = "Carregando dados...";
         Ddo_feriado_nome_Datalistupdateminimumcharacters = 0;
         Ddo_feriado_nome_Datalistproc = "GetWWFeriadosFilterData";
         Ddo_feriado_nome_Datalisttype = "Dynamic";
         Ddo_feriado_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_feriado_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_feriado_nome_Filtertype = "Character";
         Ddo_feriado_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_feriado_nome_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_feriado_nome_Includesortasc = Convert.ToBoolean( 0);
         Ddo_feriado_nome_Titlecontrolidtoreplace = "";
         Ddo_feriado_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_feriado_nome_Cls = "ColumnSettings";
         Ddo_feriado_nome_Tooltip = "Op��es";
         Ddo_feriado_nome_Caption = "";
         Ddo_feriado_data_Searchbuttontext = "Pesquisar";
         Ddo_feriado_data_Rangefilterto = "At�";
         Ddo_feriado_data_Rangefilterfrom = "Desde";
         Ddo_feriado_data_Cleanfilter = "Limpar pesquisa";
         Ddo_feriado_data_Sortdsc = "Ordenar de Z � A";
         Ddo_feriado_data_Sortasc = "Ordenar de A � Z";
         Ddo_feriado_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_feriado_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_feriado_data_Filtertype = "Date";
         Ddo_feriado_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_feriado_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_feriado_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_feriado_data_Titlecontrolidtoreplace = "";
         Ddo_feriado_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_feriado_data_Cls = "ColumnSettings";
         Ddo_feriado_data_Tooltip = "Op��es";
         Ddo_feriado_data_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Feriados";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV39Feriado_DataTitleFilterData',fld:'vFERIADO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV45Feriado_NomeTitleFilterData',fld:'vFERIADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV49Feriado_FixoTitleFilterData',fld:'vFERIADO_FIXOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFeriado_Data_Titleformat',ctrl:'FERIADO_DATA',prop:'Titleformat'},{av:'edtFeriado_Data_Title',ctrl:'FERIADO_DATA',prop:'Title'},{av:'edtFeriado_Nome_Titleformat',ctrl:'FERIADO_NOME',prop:'Titleformat'},{av:'edtFeriado_Nome_Title',ctrl:'FERIADO_NOME',prop:'Title'},{av:'chkFeriado_Fixo_Titleformat',ctrl:'FERIADO_FIXO',prop:'Titleformat'},{av:'chkFeriado_Fixo.Title.Text',ctrl:'FERIADO_FIXO',prop:'Title'},{av:'AV54GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV55GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FERIADO_DATA.ONOPTIONCLICKED","{handler:'E12I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''},{av:'Ddo_feriado_data_Activeeventkey',ctrl:'DDO_FERIADO_DATA',prop:'ActiveEventKey'},{av:'Ddo_feriado_data_Filteredtext_get',ctrl:'DDO_FERIADO_DATA',prop:'FilteredText_get'},{av:'Ddo_feriado_data_Filteredtextto_get',ctrl:'DDO_FERIADO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_feriado_data_Sortedstatus',ctrl:'DDO_FERIADO_DATA',prop:'SortedStatus'},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''}]}");
         setEventMetadata("DDO_FERIADO_NOME.ONOPTIONCLICKED","{handler:'E13I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''},{av:'Ddo_feriado_nome_Activeeventkey',ctrl:'DDO_FERIADO_NOME',prop:'ActiveEventKey'},{av:'Ddo_feriado_nome_Filteredtext_get',ctrl:'DDO_FERIADO_NOME',prop:'FilteredText_get'},{av:'Ddo_feriado_nome_Selectedvalue_get',ctrl:'DDO_FERIADO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("DDO_FERIADO_FIXO.ONOPTIONCLICKED","{handler:'E14I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''},{av:'Ddo_feriado_fixo_Activeeventkey',ctrl:'DDO_FERIADO_FIXO',prop:'ActiveEventKey'},{av:'Ddo_feriado_fixo_Selectedvalue_get',ctrl:'DDO_FERIADO_FIXO',prop:'SelectedValue_get'}],oparms:[{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28I92',iparms:[{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV25Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV26Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtFeriado_Nome_Link',ctrl:'FERIADO_NOME',prop:'Link'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21I92',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22I92',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23I92',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24I92',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25I92',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'Ddo_feriado_data_Filteredtext_set',ctrl:'DDO_FERIADO_DATA',prop:'FilteredText_set'},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'Ddo_feriado_data_Filteredtextto_set',ctrl:'DDO_FERIADO_DATA',prop:'FilteredTextTo_set'},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'Ddo_feriado_nome_Filteredtext_set',ctrl:'DDO_FERIADO_NOME',prop:'FilteredText_set'},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_feriado_nome_Selectedvalue_set',ctrl:'DDO_FERIADO_NOME',prop:'SelectedValue_set'},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'Ddo_feriado_fixo_Selectedvalue_set',ctrl:'DDO_FERIADO_FIXO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAno1_Visible',ctrl:'vANO1',prop:'Visible'},{av:'edtavFeriado_nome1_Visible',ctrl:'vFERIADO_NOME1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA1',prop:'Visible'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'edtavAno2_Visible',ctrl:'vANO2',prop:'Visible'},{av:'edtavFeriado_nome2_Visible',ctrl:'vFERIADO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA2',prop:'Visible'},{av:'edtavAno3_Visible',ctrl:'vANO3',prop:'Visible'},{av:'edtavFeriado_nome3_Visible',ctrl:'vFERIADO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersferiado_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSFERIADO_DATA3',prop:'Visible'}]}");
         setEventMetadata("'DOIMPORTAR'","{handler:'E19I92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Ano1',fld:'vANO1',pic:'ZZZ9',nv:0},{av:'AV16Feriado_Nome1',fld:'vFERIADO_NOME1',pic:'@!',nv:''},{av:'AV28Feriado_Data1',fld:'vFERIADO_DATA1',pic:'',nv:''},{av:'AV29Feriado_Data_To1',fld:'vFERIADO_DATA_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Ano2',fld:'vANO2',pic:'ZZZ9',nv:0},{av:'AV19Feriado_Nome2',fld:'vFERIADO_NOME2',pic:'@!',nv:''},{av:'AV32Feriado_Data2',fld:'vFERIADO_DATA2',pic:'',nv:''},{av:'AV33Feriado_Data_To2',fld:'vFERIADO_DATA_TO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34Ano3',fld:'vANO3',pic:'ZZZ9',nv:0},{av:'AV22Feriado_Nome3',fld:'vFERIADO_NOME3',pic:'@!',nv:''},{av:'AV35Feriado_Data3',fld:'vFERIADO_DATA3',pic:'',nv:''},{av:'AV36Feriado_Data_To3',fld:'vFERIADO_DATA_TO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFFeriado_Data',fld:'vTFFERIADO_DATA',pic:'',nv:''},{av:'AV41TFFeriado_Data_To',fld:'vTFFERIADO_DATA_TO',pic:'',nv:''},{av:'AV46TFFeriado_Nome',fld:'vTFFERIADO_NOME',pic:'@!',nv:''},{av:'AV47TFFeriado_Nome_Sel',fld:'vTFFERIADO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFFeriado_Fixo_Sel',fld:'vTFFERIADO_FIXO_SEL',pic:'9',nv:0},{av:'AV44ddo_Feriado_DataTitleControlIdToReplace',fld:'vDDO_FERIADO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Feriado_NomeTitleControlIdToReplace',fld:'vDDO_FERIADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Feriado_FixoTitleControlIdToReplace',fld:'vDDO_FERIADO_FIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E20I92',iparms:[{av:'A1175Feriado_Data',fld:'FERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_feriado_data_Activeeventkey = "";
         Ddo_feriado_data_Filteredtext_get = "";
         Ddo_feriado_data_Filteredtextto_get = "";
         Ddo_feriado_nome_Activeeventkey = "";
         Ddo_feriado_nome_Filteredtext_get = "";
         Ddo_feriado_nome_Selectedvalue_get = "";
         Ddo_feriado_fixo_Activeeventkey = "";
         Ddo_feriado_fixo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16Feriado_Nome1 = "";
         AV28Feriado_Data1 = DateTime.MinValue;
         AV29Feriado_Data_To1 = DateTime.MinValue;
         AV18DynamicFiltersSelector2 = "";
         AV19Feriado_Nome2 = "";
         AV32Feriado_Data2 = DateTime.MinValue;
         AV33Feriado_Data_To2 = DateTime.MinValue;
         AV21DynamicFiltersSelector3 = "";
         AV22Feriado_Nome3 = "";
         AV35Feriado_Data3 = DateTime.MinValue;
         AV36Feriado_Data_To3 = DateTime.MinValue;
         AV40TFFeriado_Data = DateTime.MinValue;
         AV41TFFeriado_Data_To = DateTime.MinValue;
         AV46TFFeriado_Nome = "";
         AV47TFFeriado_Nome_Sel = "";
         AV44ddo_Feriado_DataTitleControlIdToReplace = "";
         AV48ddo_Feriado_NomeTitleControlIdToReplace = "";
         AV51ddo_Feriado_FixoTitleControlIdToReplace = "";
         AV82Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A1175Feriado_Data = DateTime.MinValue;
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV52DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV39Feriado_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45Feriado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Feriado_FixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_feriado_data_Filteredtext_set = "";
         Ddo_feriado_data_Filteredtextto_set = "";
         Ddo_feriado_data_Sortedstatus = "";
         Ddo_feriado_nome_Filteredtext_set = "";
         Ddo_feriado_nome_Selectedvalue_set = "";
         Ddo_feriado_fixo_Selectedvalue_set = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV42DDO_Feriado_DataAuxDate = DateTime.MinValue;
         AV43DDO_Feriado_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Update = "";
         AV80Update_GXI = "";
         AV26Delete = "";
         AV81Delete_GXI = "";
         A1176Feriado_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV60WWFeriadosDS_3_Feriado_nome1 = "";
         lV66WWFeriadosDS_9_Feriado_nome2 = "";
         lV72WWFeriadosDS_15_Feriado_nome3 = "";
         lV77WWFeriadosDS_20_Tfferiado_nome = "";
         AV58WWFeriadosDS_1_Dynamicfiltersselector1 = "";
         AV60WWFeriadosDS_3_Feriado_nome1 = "";
         AV61WWFeriadosDS_4_Feriado_data1 = DateTime.MinValue;
         AV62WWFeriadosDS_5_Feriado_data_to1 = DateTime.MinValue;
         AV64WWFeriadosDS_7_Dynamicfiltersselector2 = "";
         AV66WWFeriadosDS_9_Feriado_nome2 = "";
         AV67WWFeriadosDS_10_Feriado_data2 = DateTime.MinValue;
         AV68WWFeriadosDS_11_Feriado_data_to2 = DateTime.MinValue;
         AV70WWFeriadosDS_13_Dynamicfiltersselector3 = "";
         AV72WWFeriadosDS_15_Feriado_nome3 = "";
         AV73WWFeriadosDS_16_Feriado_data3 = DateTime.MinValue;
         AV74WWFeriadosDS_17_Feriado_data_to3 = DateTime.MinValue;
         AV75WWFeriadosDS_18_Tfferiado_data = DateTime.MinValue;
         AV76WWFeriadosDS_19_Tfferiado_data_to = DateTime.MinValue;
         AV78WWFeriadosDS_21_Tfferiado_nome_sel = "";
         AV77WWFeriadosDS_20_Tfferiado_nome = "";
         H00I92_A1195Feriado_Fixo = new bool[] {false} ;
         H00I92_n1195Feriado_Fixo = new bool[] {false} ;
         H00I92_A1176Feriado_Nome = new String[] {""} ;
         H00I92_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         H00I93_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV27Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         bttBtnimportar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFeriadostitle_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersferiado_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersferiado_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersferiado_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwferiados__default(),
            new Object[][] {
                new Object[] {
               H00I92_A1195Feriado_Fixo, H00I92_n1195Feriado_Fixo, H00I92_A1176Feriado_Nome, H00I92_A1175Feriado_Data
               }
               , new Object[] {
               H00I93_AGRID_nRecordCount
               }
            }
         );
         AV82Pgmname = "WWFeriados";
         /* GeneXus formulas. */
         AV82Pgmname = "WWFeriados";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_95 ;
      private short nGXsfl_95_idx=1 ;
      private short AV30Ano1 ;
      private short AV31Ano2 ;
      private short AV34Ano3 ;
      private short AV50TFFeriado_Fixo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_95_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV59WWFeriadosDS_2_Ano1 ;
      private short AV65WWFeriadosDS_8_Ano2 ;
      private short AV71WWFeriadosDS_14_Ano3 ;
      private short AV79WWFeriadosDS_22_Tfferiado_fixo_sel ;
      private short edtFeriado_Data_Titleformat ;
      private short edtFeriado_Nome_Titleformat ;
      private short chkFeriado_Fixo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_feriado_nome_Datalistupdateminimumcharacters ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfferiado_data_Visible ;
      private int edtavTfferiado_data_to_Visible ;
      private int edtavTfferiado_nome_Visible ;
      private int edtavTfferiado_nome_sel_Visible ;
      private int edtavTfferiado_fixo_sel_Visible ;
      private int edtavDdo_feriado_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_feriado_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_feriado_fixotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV53PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavAno1_Visible ;
      private int edtavFeriado_nome1_Visible ;
      private int tblTablemergeddynamicfiltersferiado_data1_Visible ;
      private int edtavAno2_Visible ;
      private int edtavFeriado_nome2_Visible ;
      private int tblTablemergeddynamicfiltersferiado_data2_Visible ;
      private int edtavAno3_Visible ;
      private int edtavFeriado_nome3_Visible ;
      private int tblTablemergeddynamicfiltersferiado_data3_Visible ;
      private int AV83GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV54GridCurrentPage ;
      private long AV55GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_feriado_data_Activeeventkey ;
      private String Ddo_feriado_data_Filteredtext_get ;
      private String Ddo_feriado_data_Filteredtextto_get ;
      private String Ddo_feriado_nome_Activeeventkey ;
      private String Ddo_feriado_nome_Filteredtext_get ;
      private String Ddo_feriado_nome_Selectedvalue_get ;
      private String Ddo_feriado_fixo_Activeeventkey ;
      private String Ddo_feriado_fixo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_95_idx="0001" ;
      private String AV16Feriado_Nome1 ;
      private String AV19Feriado_Nome2 ;
      private String AV22Feriado_Nome3 ;
      private String AV46TFFeriado_Nome ;
      private String AV47TFFeriado_Nome_Sel ;
      private String AV82Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_feriado_data_Caption ;
      private String Ddo_feriado_data_Tooltip ;
      private String Ddo_feriado_data_Cls ;
      private String Ddo_feriado_data_Filteredtext_set ;
      private String Ddo_feriado_data_Filteredtextto_set ;
      private String Ddo_feriado_data_Dropdownoptionstype ;
      private String Ddo_feriado_data_Titlecontrolidtoreplace ;
      private String Ddo_feriado_data_Sortedstatus ;
      private String Ddo_feriado_data_Filtertype ;
      private String Ddo_feriado_data_Sortasc ;
      private String Ddo_feriado_data_Sortdsc ;
      private String Ddo_feriado_data_Cleanfilter ;
      private String Ddo_feriado_data_Rangefilterfrom ;
      private String Ddo_feriado_data_Rangefilterto ;
      private String Ddo_feriado_data_Searchbuttontext ;
      private String Ddo_feriado_nome_Caption ;
      private String Ddo_feriado_nome_Tooltip ;
      private String Ddo_feriado_nome_Cls ;
      private String Ddo_feriado_nome_Filteredtext_set ;
      private String Ddo_feriado_nome_Selectedvalue_set ;
      private String Ddo_feriado_nome_Dropdownoptionstype ;
      private String Ddo_feriado_nome_Titlecontrolidtoreplace ;
      private String Ddo_feriado_nome_Filtertype ;
      private String Ddo_feriado_nome_Datalisttype ;
      private String Ddo_feriado_nome_Datalistproc ;
      private String Ddo_feriado_nome_Loadingdata ;
      private String Ddo_feriado_nome_Cleanfilter ;
      private String Ddo_feriado_nome_Noresultsfound ;
      private String Ddo_feriado_nome_Searchbuttontext ;
      private String Ddo_feriado_fixo_Caption ;
      private String Ddo_feriado_fixo_Tooltip ;
      private String Ddo_feriado_fixo_Cls ;
      private String Ddo_feriado_fixo_Selectedvalue_set ;
      private String Ddo_feriado_fixo_Dropdownoptionstype ;
      private String Ddo_feriado_fixo_Titlecontrolidtoreplace ;
      private String Ddo_feriado_fixo_Datalisttype ;
      private String Ddo_feriado_fixo_Datalistfixedvalues ;
      private String Ddo_feriado_fixo_Cleanfilter ;
      private String Ddo_feriado_fixo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfferiado_data_Internalname ;
      private String edtavTfferiado_data_Jsonclick ;
      private String edtavTfferiado_data_to_Internalname ;
      private String edtavTfferiado_data_to_Jsonclick ;
      private String divDdo_feriado_dataauxdates_Internalname ;
      private String edtavDdo_feriado_dataauxdate_Internalname ;
      private String edtavDdo_feriado_dataauxdate_Jsonclick ;
      private String edtavDdo_feriado_dataauxdateto_Internalname ;
      private String edtavDdo_feriado_dataauxdateto_Jsonclick ;
      private String edtavTfferiado_nome_Internalname ;
      private String edtavTfferiado_nome_Jsonclick ;
      private String edtavTfferiado_nome_sel_Internalname ;
      private String edtavTfferiado_nome_sel_Jsonclick ;
      private String edtavTfferiado_fixo_sel_Internalname ;
      private String edtavTfferiado_fixo_sel_Jsonclick ;
      private String edtavDdo_feriado_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_feriado_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_feriado_fixotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtFeriado_Data_Internalname ;
      private String A1176Feriado_Nome ;
      private String edtFeriado_Nome_Internalname ;
      private String chkFeriado_Fixo_Internalname ;
      private String GXCCtl ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String scmdbuf ;
      private String lV60WWFeriadosDS_3_Feriado_nome1 ;
      private String lV66WWFeriadosDS_9_Feriado_nome2 ;
      private String lV72WWFeriadosDS_15_Feriado_nome3 ;
      private String lV77WWFeriadosDS_20_Tfferiado_nome ;
      private String AV60WWFeriadosDS_3_Feriado_nome1 ;
      private String AV66WWFeriadosDS_9_Feriado_nome2 ;
      private String AV72WWFeriadosDS_15_Feriado_nome3 ;
      private String AV78WWFeriadosDS_21_Tfferiado_nome_sel ;
      private String AV77WWFeriadosDS_20_Tfferiado_nome ;
      private String edtavAno1_Internalname ;
      private String edtavFeriado_nome1_Internalname ;
      private String edtavFeriado_data1_Internalname ;
      private String edtavFeriado_data_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAno2_Internalname ;
      private String edtavFeriado_nome2_Internalname ;
      private String edtavFeriado_data2_Internalname ;
      private String edtavFeriado_data_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavAno3_Internalname ;
      private String edtavFeriado_nome3_Internalname ;
      private String edtavFeriado_data3_Internalname ;
      private String edtavFeriado_data_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_feriado_data_Internalname ;
      private String Ddo_feriado_nome_Internalname ;
      private String Ddo_feriado_fixo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFeriado_Data_Title ;
      private String edtFeriado_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtFeriado_Nome_Link ;
      private String tblTablemergeddynamicfiltersferiado_data1_Internalname ;
      private String tblTablemergeddynamicfiltersferiado_data2_Internalname ;
      private String tblTablemergeddynamicfiltersferiado_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String bttBtnimportar_Internalname ;
      private String bttBtnimportar_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblFeriadostitle_Internalname ;
      private String lblFeriadostitle_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavAno1_Jsonclick ;
      private String edtavFeriado_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavAno2_Jsonclick ;
      private String edtavFeriado_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavAno3_Jsonclick ;
      private String edtavFeriado_nome3_Jsonclick ;
      private String edtavFeriado_data3_Jsonclick ;
      private String lblDynamicfiltersferiado_data_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersferiado_data_rangemiddletext3_Jsonclick ;
      private String edtavFeriado_data_to3_Jsonclick ;
      private String edtavFeriado_data2_Jsonclick ;
      private String lblDynamicfiltersferiado_data_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersferiado_data_rangemiddletext2_Jsonclick ;
      private String edtavFeriado_data_to2_Jsonclick ;
      private String edtavFeriado_data1_Jsonclick ;
      private String lblDynamicfiltersferiado_data_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersferiado_data_rangemiddletext1_Jsonclick ;
      private String edtavFeriado_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_95_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFeriado_Data_Jsonclick ;
      private String edtFeriado_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV28Feriado_Data1 ;
      private DateTime AV29Feriado_Data_To1 ;
      private DateTime AV32Feriado_Data2 ;
      private DateTime AV33Feriado_Data_To2 ;
      private DateTime AV35Feriado_Data3 ;
      private DateTime AV36Feriado_Data_To3 ;
      private DateTime AV40TFFeriado_Data ;
      private DateTime AV41TFFeriado_Data_To ;
      private DateTime A1175Feriado_Data ;
      private DateTime AV42DDO_Feriado_DataAuxDate ;
      private DateTime AV43DDO_Feriado_DataAuxDateTo ;
      private DateTime AV61WWFeriadosDS_4_Feriado_data1 ;
      private DateTime AV62WWFeriadosDS_5_Feriado_data_to1 ;
      private DateTime AV67WWFeriadosDS_10_Feriado_data2 ;
      private DateTime AV68WWFeriadosDS_11_Feriado_data_to2 ;
      private DateTime AV73WWFeriadosDS_16_Feriado_data3 ;
      private DateTime AV74WWFeriadosDS_17_Feriado_data_to3 ;
      private DateTime AV75WWFeriadosDS_18_Tfferiado_data ;
      private DateTime AV76WWFeriadosDS_19_Tfferiado_data_to ;
      private bool entryPointCalled ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV14OrderedDsc ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_feriado_data_Includesortasc ;
      private bool Ddo_feriado_data_Includesortdsc ;
      private bool Ddo_feriado_data_Includefilter ;
      private bool Ddo_feriado_data_Filterisrange ;
      private bool Ddo_feriado_data_Includedatalist ;
      private bool Ddo_feriado_nome_Includesortasc ;
      private bool Ddo_feriado_nome_Includesortdsc ;
      private bool Ddo_feriado_nome_Includefilter ;
      private bool Ddo_feriado_nome_Filterisrange ;
      private bool Ddo_feriado_nome_Includedatalist ;
      private bool Ddo_feriado_fixo_Includesortasc ;
      private bool Ddo_feriado_fixo_Includesortdsc ;
      private bool Ddo_feriado_fixo_Includefilter ;
      private bool Ddo_feriado_fixo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1195Feriado_Fixo ;
      private bool n1195Feriado_Fixo ;
      private bool AV63WWFeriadosDS_6_Dynamicfiltersenabled2 ;
      private bool AV69WWFeriadosDS_12_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Update_IsBlob ;
      private bool AV26Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV44ddo_Feriado_DataTitleControlIdToReplace ;
      private String AV48ddo_Feriado_NomeTitleControlIdToReplace ;
      private String AV51ddo_Feriado_FixoTitleControlIdToReplace ;
      private String AV80Update_GXI ;
      private String AV81Delete_GXI ;
      private String AV58WWFeriadosDS_1_Dynamicfiltersselector1 ;
      private String AV64WWFeriadosDS_7_Dynamicfiltersselector2 ;
      private String AV70WWFeriadosDS_13_Dynamicfiltersselector3 ;
      private String AV25Update ;
      private String AV26Delete ;
      private IGxSession AV27Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkFeriado_Fixo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00I92_A1195Feriado_Fixo ;
      private bool[] H00I92_n1195Feriado_Fixo ;
      private String[] H00I92_A1176Feriado_Nome ;
      private DateTime[] H00I92_A1175Feriado_Data ;
      private long[] H00I93_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39Feriado_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45Feriado_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49Feriado_FixoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV52DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwferiados__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00I92( IGxContext context ,
                                             String AV58WWFeriadosDS_1_Dynamicfiltersselector1 ,
                                             short AV59WWFeriadosDS_2_Ano1 ,
                                             String AV60WWFeriadosDS_3_Feriado_nome1 ,
                                             DateTime AV61WWFeriadosDS_4_Feriado_data1 ,
                                             DateTime AV62WWFeriadosDS_5_Feriado_data_to1 ,
                                             bool AV63WWFeriadosDS_6_Dynamicfiltersenabled2 ,
                                             String AV64WWFeriadosDS_7_Dynamicfiltersselector2 ,
                                             short AV65WWFeriadosDS_8_Ano2 ,
                                             String AV66WWFeriadosDS_9_Feriado_nome2 ,
                                             DateTime AV67WWFeriadosDS_10_Feriado_data2 ,
                                             DateTime AV68WWFeriadosDS_11_Feriado_data_to2 ,
                                             bool AV69WWFeriadosDS_12_Dynamicfiltersenabled3 ,
                                             String AV70WWFeriadosDS_13_Dynamicfiltersselector3 ,
                                             short AV71WWFeriadosDS_14_Ano3 ,
                                             String AV72WWFeriadosDS_15_Feriado_nome3 ,
                                             DateTime AV73WWFeriadosDS_16_Feriado_data3 ,
                                             DateTime AV74WWFeriadosDS_17_Feriado_data_to3 ,
                                             DateTime AV75WWFeriadosDS_18_Tfferiado_data ,
                                             DateTime AV76WWFeriadosDS_19_Tfferiado_data_to ,
                                             String AV78WWFeriadosDS_21_Tfferiado_nome_sel ,
                                             String AV77WWFeriadosDS_20_Tfferiado_nome ,
                                             short AV79WWFeriadosDS_22_Tfferiado_fixo_sel ,
                                             DateTime A1175Feriado_Data ,
                                             String A1176Feriado_Nome ,
                                             bool A1195Feriado_Fixo ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Feriado_Fixo], [Feriado_Nome], [Feriado_Data]";
         sFromString = " FROM [Feriados] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "ANO") == 0 ) && ( ! (0==AV59WWFeriadosDS_2_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV59WWFeriadosDS_2_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV59WWFeriadosDS_2_Ano1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWFeriadosDS_3_Feriado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV60WWFeriadosDS_3_Feriado_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV60WWFeriadosDS_3_Feriado_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWFeriadosDS_4_Feriado_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV61WWFeriadosDS_4_Feriado_data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV61WWFeriadosDS_4_Feriado_data1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV62WWFeriadosDS_5_Feriado_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV62WWFeriadosDS_5_Feriado_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV62WWFeriadosDS_5_Feriado_data_to1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "ANO") == 0 ) && ( ! (0==AV65WWFeriadosDS_8_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV65WWFeriadosDS_8_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV65WWFeriadosDS_8_Ano2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWFeriadosDS_9_Feriado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV66WWFeriadosDS_9_Feriado_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV66WWFeriadosDS_9_Feriado_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWFeriadosDS_10_Feriado_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV67WWFeriadosDS_10_Feriado_data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV67WWFeriadosDS_10_Feriado_data2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV68WWFeriadosDS_11_Feriado_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV68WWFeriadosDS_11_Feriado_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV68WWFeriadosDS_11_Feriado_data_to2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "ANO") == 0 ) && ( ! (0==AV71WWFeriadosDS_14_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV71WWFeriadosDS_14_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV71WWFeriadosDS_14_Ano3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWFeriadosDS_15_Feriado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV72WWFeriadosDS_15_Feriado_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV72WWFeriadosDS_15_Feriado_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWFeriadosDS_16_Feriado_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV73WWFeriadosDS_16_Feriado_data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV73WWFeriadosDS_16_Feriado_data3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV74WWFeriadosDS_17_Feriado_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV74WWFeriadosDS_17_Feriado_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV74WWFeriadosDS_17_Feriado_data_to3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWFeriadosDS_18_Tfferiado_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV75WWFeriadosDS_18_Tfferiado_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV75WWFeriadosDS_18_Tfferiado_data)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWFeriadosDS_19_Tfferiado_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV76WWFeriadosDS_19_Tfferiado_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV76WWFeriadosDS_19_Tfferiado_data_to)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV78WWFeriadosDS_21_Tfferiado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWFeriadosDS_20_Tfferiado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like @lV77WWFeriadosDS_20_Tfferiado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like @lV77WWFeriadosDS_20_Tfferiado_nome)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWFeriadosDS_21_Tfferiado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] = @AV78WWFeriadosDS_21_Tfferiado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] = @AV78WWFeriadosDS_21_Tfferiado_nome_sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV79WWFeriadosDS_22_Tfferiado_fixo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 1)";
            }
         }
         if ( AV79WWFeriadosDS_22_Tfferiado_fixo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Data]";
         }
         else if ( AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Feriado_Data]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00I93( IGxContext context ,
                                             String AV58WWFeriadosDS_1_Dynamicfiltersselector1 ,
                                             short AV59WWFeriadosDS_2_Ano1 ,
                                             String AV60WWFeriadosDS_3_Feriado_nome1 ,
                                             DateTime AV61WWFeriadosDS_4_Feriado_data1 ,
                                             DateTime AV62WWFeriadosDS_5_Feriado_data_to1 ,
                                             bool AV63WWFeriadosDS_6_Dynamicfiltersenabled2 ,
                                             String AV64WWFeriadosDS_7_Dynamicfiltersselector2 ,
                                             short AV65WWFeriadosDS_8_Ano2 ,
                                             String AV66WWFeriadosDS_9_Feriado_nome2 ,
                                             DateTime AV67WWFeriadosDS_10_Feriado_data2 ,
                                             DateTime AV68WWFeriadosDS_11_Feriado_data_to2 ,
                                             bool AV69WWFeriadosDS_12_Dynamicfiltersenabled3 ,
                                             String AV70WWFeriadosDS_13_Dynamicfiltersselector3 ,
                                             short AV71WWFeriadosDS_14_Ano3 ,
                                             String AV72WWFeriadosDS_15_Feriado_nome3 ,
                                             DateTime AV73WWFeriadosDS_16_Feriado_data3 ,
                                             DateTime AV74WWFeriadosDS_17_Feriado_data_to3 ,
                                             DateTime AV75WWFeriadosDS_18_Tfferiado_data ,
                                             DateTime AV76WWFeriadosDS_19_Tfferiado_data_to ,
                                             String AV78WWFeriadosDS_21_Tfferiado_nome_sel ,
                                             String AV77WWFeriadosDS_20_Tfferiado_nome ,
                                             short AV79WWFeriadosDS_22_Tfferiado_fixo_sel ,
                                             DateTime A1175Feriado_Data ,
                                             String A1176Feriado_Nome ,
                                             bool A1195Feriado_Fixo ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Feriados] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "ANO") == 0 ) && ( ! (0==AV59WWFeriadosDS_2_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV59WWFeriadosDS_2_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV59WWFeriadosDS_2_Ano1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWFeriadosDS_3_Feriado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV60WWFeriadosDS_3_Feriado_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV60WWFeriadosDS_3_Feriado_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWFeriadosDS_4_Feriado_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV61WWFeriadosDS_4_Feriado_data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV61WWFeriadosDS_4_Feriado_data1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV62WWFeriadosDS_5_Feriado_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV62WWFeriadosDS_5_Feriado_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV62WWFeriadosDS_5_Feriado_data_to1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "ANO") == 0 ) && ( ! (0==AV65WWFeriadosDS_8_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV65WWFeriadosDS_8_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV65WWFeriadosDS_8_Ano2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWFeriadosDS_9_Feriado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV66WWFeriadosDS_9_Feriado_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV66WWFeriadosDS_9_Feriado_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWFeriadosDS_10_Feriado_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV67WWFeriadosDS_10_Feriado_data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV67WWFeriadosDS_10_Feriado_data2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV63WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV68WWFeriadosDS_11_Feriado_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV68WWFeriadosDS_11_Feriado_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV68WWFeriadosDS_11_Feriado_data_to2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "ANO") == 0 ) && ( ! (0==AV71WWFeriadosDS_14_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV71WWFeriadosDS_14_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV71WWFeriadosDS_14_Ano3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWFeriadosDS_15_Feriado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV72WWFeriadosDS_15_Feriado_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV72WWFeriadosDS_15_Feriado_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWFeriadosDS_16_Feriado_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV73WWFeriadosDS_16_Feriado_data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV73WWFeriadosDS_16_Feriado_data3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV69WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV74WWFeriadosDS_17_Feriado_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV74WWFeriadosDS_17_Feriado_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV74WWFeriadosDS_17_Feriado_data_to3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWFeriadosDS_18_Tfferiado_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV75WWFeriadosDS_18_Tfferiado_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV75WWFeriadosDS_18_Tfferiado_data)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWFeriadosDS_19_Tfferiado_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV76WWFeriadosDS_19_Tfferiado_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV76WWFeriadosDS_19_Tfferiado_data_to)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV78WWFeriadosDS_21_Tfferiado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWFeriadosDS_20_Tfferiado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like @lV77WWFeriadosDS_20_Tfferiado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like @lV77WWFeriadosDS_20_Tfferiado_nome)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWFeriadosDS_21_Tfferiado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] = @AV78WWFeriadosDS_21_Tfferiado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] = @AV78WWFeriadosDS_21_Tfferiado_nome_sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV79WWFeriadosDS_22_Tfferiado_fixo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 1)";
            }
         }
         if ( AV79WWFeriadosDS_22_Tfferiado_fixo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00I92(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] );
               case 1 :
                     return conditional_H00I93(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00I92 ;
          prmH00I92 = new Object[] {
          new Object[] {"@AV59WWFeriadosDS_2_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV60WWFeriadosDS_3_Feriado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61WWFeriadosDS_4_Feriado_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62WWFeriadosDS_5_Feriado_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65WWFeriadosDS_8_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV66WWFeriadosDS_9_Feriado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWFeriadosDS_10_Feriado_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68WWFeriadosDS_11_Feriado_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWFeriadosDS_14_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV72WWFeriadosDS_15_Feriado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWFeriadosDS_16_Feriado_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWFeriadosDS_17_Feriado_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75WWFeriadosDS_18_Tfferiado_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV76WWFeriadosDS_19_Tfferiado_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV77WWFeriadosDS_20_Tfferiado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV78WWFeriadosDS_21_Tfferiado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00I93 ;
          prmH00I93 = new Object[] {
          new Object[] {"@AV59WWFeriadosDS_2_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV60WWFeriadosDS_3_Feriado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61WWFeriadosDS_4_Feriado_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62WWFeriadosDS_5_Feriado_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65WWFeriadosDS_8_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV66WWFeriadosDS_9_Feriado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWFeriadosDS_10_Feriado_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68WWFeriadosDS_11_Feriado_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWFeriadosDS_14_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV72WWFeriadosDS_15_Feriado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWFeriadosDS_16_Feriado_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWFeriadosDS_17_Feriado_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75WWFeriadosDS_18_Tfferiado_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV76WWFeriadosDS_19_Tfferiado_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV77WWFeriadosDS_20_Tfferiado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV78WWFeriadosDS_21_Tfferiado_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00I92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I92,11,0,true,false )
             ,new CursorDef("H00I93", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I93,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

}
