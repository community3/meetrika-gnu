/*
               File: ContratoServicosTelas
        Description: Tela associada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:21:25.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicostelas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel5"+"_"+"CONTRATOSERVICOSTELAS_SEQUENCIAL") == 0 )
         {
            AV12ContratoServicosTelas_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoServicosTelas_Sequencial), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSTELAS_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ContratoServicosTelas_Sequencial), "ZZ9")));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX5ASACONTRATOSERVICOSTELAS_SEQUENCIAL2W116( AV12ContratoServicosTelas_Sequencial) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel6"+"_"+"CONTRATOSERVICOSTELAS_SEQUENCIAL") == 0 )
         {
            AV7ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSTELAS_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX6ASACONTRATOSERVICOSTELAS_SEQUENCIAL2W116( AV7ContratoServicosTelas_ContratoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A926ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A926ContratoServicosTelas_ContratoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A39Contratada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A40Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A40Contratada_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A925ContratoServicosTelas_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n925ContratoServicosTelas_ServicoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A925ContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A925ContratoServicosTelas_ServicoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSTELAS_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
               AV12ContratoServicosTelas_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoServicosTelas_Sequencial), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSTELAS_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ContratoServicosTelas_Sequencial), "ZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratoServicosTelas_Status.Name = "CONTRATOSERVICOSTELAS_STATUS";
         cmbContratoServicosTelas_Status.WebTags = "";
         cmbContratoServicosTelas_Status.addItem("", "Todos", 0);
         cmbContratoServicosTelas_Status.addItem("B", "Stand by", 0);
         cmbContratoServicosTelas_Status.addItem("S", "Solicitada", 0);
         cmbContratoServicosTelas_Status.addItem("E", "Em An�lise", 0);
         cmbContratoServicosTelas_Status.addItem("A", "Em execu��o", 0);
         cmbContratoServicosTelas_Status.addItem("R", "Resolvida", 0);
         cmbContratoServicosTelas_Status.addItem("C", "Conferida", 0);
         cmbContratoServicosTelas_Status.addItem("D", "Rejeitada", 0);
         cmbContratoServicosTelas_Status.addItem("H", "Homologada", 0);
         cmbContratoServicosTelas_Status.addItem("O", "Aceite", 0);
         cmbContratoServicosTelas_Status.addItem("P", "A Pagar", 0);
         cmbContratoServicosTelas_Status.addItem("L", "Liquidada", 0);
         cmbContratoServicosTelas_Status.addItem("X", "Cancelada", 0);
         cmbContratoServicosTelas_Status.addItem("N", "N�o Faturada", 0);
         cmbContratoServicosTelas_Status.addItem("J", "Planejamento", 0);
         cmbContratoServicosTelas_Status.addItem("I", "An�lise Planejamento", 0);
         cmbContratoServicosTelas_Status.addItem("T", "Validacao T�cnica", 0);
         cmbContratoServicosTelas_Status.addItem("Q", "Validacao Qualidade", 0);
         cmbContratoServicosTelas_Status.addItem("G", "Em Homologa��o", 0);
         cmbContratoServicosTelas_Status.addItem("M", "Valida��o Mensura��o", 0);
         cmbContratoServicosTelas_Status.addItem("U", "Rascunho", 0);
         if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
         {
            A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
            n932ContratoServicosTelas_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tela associada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoServicosTelas_Tela_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicostelas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicostelas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicosTelas_ContratoCod ,
                           short aP2_ContratoServicosTelas_Sequencial )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicosTelas_ContratoCod = aP1_ContratoServicosTelas_ContratoCod;
         this.AV12ContratoServicosTelas_Sequencial = aP2_ContratoServicosTelas_Sequencial;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoServicosTelas_Status = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
         {
            A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
            n932ContratoServicosTelas_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2W116( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2W116e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_ContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_ContratoCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosTelas_ContratoCod_Visible, edtContratoServicosTelas_ContratoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosTelas.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2W116( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2W116( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2W116e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_51_2W116( true) ;
         }
         return  ;
      }

      protected void wb_table3_51_2W116e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2W116e( true) ;
         }
         else
         {
            wb_table1_2_2W116e( false) ;
         }
      }

      protected void wb_table3_51_2W116( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_51_2W116e( true) ;
         }
         else
         {
            wb_table3_51_2W116e( false) ;
         }
      }

      protected void wb_table2_5_2W116( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2W116( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2W116e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2W116e( true) ;
         }
         else
         {
            wb_table2_5_2W116e( false) ;
         }
      }

      protected void wb_table4_13_2W116( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_servicosigla_Internalname, "Servi�o", "", "", lblTextblockcontratoservicostelas_servicosigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_ServicoSigla_Internalname, StringUtil.RTrim( A937ContratoServicosTelas_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A937ContratoServicosTelas_ServicoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_ServicoSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosTelas_ServicoSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_tela_Internalname, "Nome da Tela", "", "", lblTextblockcontratoservicostelas_tela_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_Tela_Internalname, StringUtil.RTrim( A931ContratoServicosTelas_Tela), StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_Tela_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosTelas_Tela_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_link_Internalname, "Link", "", "", lblTextblockcontratoservicostelas_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_Link_Internalname, A928ContratoServicosTelas_Link, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_Link_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosTelas_Link_Enabled, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "LinkMenu", "left", true, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_parms_Internalname, "Par�metros", "", "", lblTextblockcontratoservicostelas_parms_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_Parms_Internalname, A929ContratoServicosTelas_Parms, A929ContratoServicosTelas_Parms, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_Parms_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosTelas_Parms_Enabled, 0, "text", "", 80, "chr", 1, "row", 500, 0, 0, 0, 1, 0, 0, true, "DescricaoLonga", "left", false, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_status_Internalname, "Quando o Status for", "", "", lblTextblockcontratoservicostelas_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosTelas_Status, cmbContratoServicosTelas_Status_Internalname, StringUtil.RTrim( A932ContratoServicosTelas_Status), 1, cmbContratoServicosTelas_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicosTelas_Status.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_ContratoServicosTelas.htm");
            cmbContratoServicosTelas_Status.CurrentValue = StringUtil.RTrim( A932ContratoServicosTelas_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosTelas_Status_Internalname, "Values", (String)(cmbContratoServicosTelas_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_textmouse_Internalname, "Texto ao passar o mouse", "", "", lblTextblockcontratoservicostelas_textmouse_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_TextMouse_Internalname, StringUtil.RTrim( A934ContratoServicosTelas_TextMouse), StringUtil.RTrim( context.localUtil.Format( A934ContratoServicosTelas_TextMouse, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_TextMouse_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosTelas_TextMouse_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosTelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2W116e( true) ;
         }
         else
         {
            wb_table4_13_2W116e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112W2 */
         E112W2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A937ContratoServicosTelas_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoServicosTelas_ServicoSigla_Internalname));
               n937ContratoServicosTelas_ServicoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A937ContratoServicosTelas_ServicoSigla", A937ContratoServicosTelas_ServicoSigla);
               A931ContratoServicosTelas_Tela = StringUtil.Upper( cgiGet( edtContratoServicosTelas_Tela_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A931ContratoServicosTelas_Tela", A931ContratoServicosTelas_Tela);
               A928ContratoServicosTelas_Link = cgiGet( edtContratoServicosTelas_Link_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A928ContratoServicosTelas_Link", A928ContratoServicosTelas_Link);
               A929ContratoServicosTelas_Parms = cgiGet( edtContratoServicosTelas_Parms_Internalname);
               n929ContratoServicosTelas_Parms = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A929ContratoServicosTelas_Parms", A929ContratoServicosTelas_Parms);
               n929ContratoServicosTelas_Parms = (String.IsNullOrEmpty(StringUtil.RTrim( A929ContratoServicosTelas_Parms)) ? true : false);
               cmbContratoServicosTelas_Status.CurrentValue = cgiGet( cmbContratoServicosTelas_Status_Internalname);
               A932ContratoServicosTelas_Status = cgiGet( cmbContratoServicosTelas_Status_Internalname);
               n932ContratoServicosTelas_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
               n932ContratoServicosTelas_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A932ContratoServicosTelas_Status)) ? true : false);
               A934ContratoServicosTelas_TextMouse = cgiGet( edtContratoServicosTelas_TextMouse_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A934ContratoServicosTelas_TextMouse", A934ContratoServicosTelas_TextMouse);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosTelas_ContratoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosTelas_ContratoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A926ContratoServicosTelas_ContratoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
               }
               else
               {
                  A926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosTelas_ContratoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
               }
               /* Read saved values. */
               Z926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "Z926ContratoServicosTelas_ContratoCod"), ",", "."));
               Z938ContratoServicosTelas_Sequencial = (short)(context.localUtil.CToN( cgiGet( "Z938ContratoServicosTelas_Sequencial"), ",", "."));
               Z931ContratoServicosTelas_Tela = cgiGet( "Z931ContratoServicosTelas_Tela");
               Z928ContratoServicosTelas_Link = cgiGet( "Z928ContratoServicosTelas_Link");
               Z932ContratoServicosTelas_Status = cgiGet( "Z932ContratoServicosTelas_Status");
               n932ContratoServicosTelas_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A932ContratoServicosTelas_Status)) ? true : false);
               Z934ContratoServicosTelas_TextMouse = cgiGet( "Z934ContratoServicosTelas_TextMouse");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSTELAS_CONTRATOCOD"), ",", "."));
               AV12ContratoServicosTelas_Sequencial = (short)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSTELAS_SEQUENCIAL"), ",", "."));
               A938ContratoServicosTelas_Sequencial = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSTELAS_SEQUENCIAL"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               A925ContratoServicosTelas_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSTELAS_SERVICOCOD"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosTelas";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A926ContratoServicosTelas_ContratoCod != Z926ContratoServicosTelas_ContratoCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicostelas:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A926ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
                  A938ContratoServicosTelas_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode116 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode116;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound116 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2W0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112W2 */
                           E112W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122W2 */
                           E122W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122W2 */
            E122W2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2W116( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2W116( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2W0( )
      {
         BeforeValidate2W116( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2W116( ) ;
            }
            else
            {
               CheckExtendedTable2W116( ) ;
               CloseExtendedTableCursors2W116( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2W0( )
      {
      }

      protected void E112W2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtContratoServicosTelas_ContratoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ContratoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ContratoCod_Visible), 5, 0)));
      }

      protected void E122W2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoservicostelas.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2W116( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z931ContratoServicosTelas_Tela = T002W3_A931ContratoServicosTelas_Tela[0];
               Z928ContratoServicosTelas_Link = T002W3_A928ContratoServicosTelas_Link[0];
               Z932ContratoServicosTelas_Status = T002W3_A932ContratoServicosTelas_Status[0];
               Z934ContratoServicosTelas_TextMouse = T002W3_A934ContratoServicosTelas_TextMouse[0];
            }
            else
            {
               Z931ContratoServicosTelas_Tela = A931ContratoServicosTelas_Tela;
               Z928ContratoServicosTelas_Link = A928ContratoServicosTelas_Link;
               Z932ContratoServicosTelas_Status = A932ContratoServicosTelas_Status;
               Z934ContratoServicosTelas_TextMouse = A934ContratoServicosTelas_TextMouse;
            }
         }
         if ( GX_JID == -9 )
         {
            Z938ContratoServicosTelas_Sequencial = A938ContratoServicosTelas_Sequencial;
            Z931ContratoServicosTelas_Tela = A931ContratoServicosTelas_Tela;
            Z928ContratoServicosTelas_Link = A928ContratoServicosTelas_Link;
            Z929ContratoServicosTelas_Parms = A929ContratoServicosTelas_Parms;
            Z932ContratoServicosTelas_Status = A932ContratoServicosTelas_Status;
            Z934ContratoServicosTelas_TextMouse = A934ContratoServicosTelas_TextMouse;
            Z926ContratoServicosTelas_ContratoCod = A926ContratoServicosTelas_ContratoCod;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z925ContratoServicosTelas_ServicoCod = A925ContratoServicosTelas_ServicoCod;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z937ContratoServicosTelas_ServicoSigla = A937ContratoServicosTelas_ServicoSigla;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicosTelas_ContratoCod) )
         {
            A926ContratoServicosTelas_ContratoCod = AV7ContratoServicosTelas_ContratoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
         }
         if ( ! (0==AV7ContratoServicosTelas_ContratoCod) )
         {
            edtContratoServicosTelas_ContratoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ContratoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ContratoCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratoServicosTelas_ContratoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ContratoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ContratoCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV7ContratoServicosTelas_ContratoCod) )
         {
            edtContratoServicosTelas_ContratoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ContratoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ContratoCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV12ContratoServicosTelas_Sequencial) )
         {
            A938ContratoServicosTelas_Sequencial = AV12ContratoServicosTelas_Sequencial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002W4 */
            pr_default.execute(2, new Object[] {A926ContratoServicosTelas_ContratoCod});
            A74Contrato_Codigo = T002W4_A74Contrato_Codigo[0];
            n74Contrato_Codigo = T002W4_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = T002W4_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = T002W4_n925ContratoServicosTelas_ServicoCod[0];
            pr_default.close(2);
            /* Using cursor T002W6 */
            pr_default.execute(4, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            A39Contratada_Codigo = T002W6_A39Contratada_Codigo[0];
            pr_default.close(4);
            /* Using cursor T002W7 */
            pr_default.execute(5, new Object[] {A39Contratada_Codigo});
            A40Contratada_PessoaCod = T002W7_A40Contratada_PessoaCod[0];
            pr_default.close(5);
            /* Using cursor T002W8 */
            pr_default.execute(6, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T002W8_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T002W8_n41Contratada_PessoaNom[0];
            pr_default.close(6);
            /* Using cursor T002W5 */
            pr_default.execute(3, new Object[] {n925ContratoServicosTelas_ServicoCod, A925ContratoServicosTelas_ServicoCod});
            A937ContratoServicosTelas_ServicoSigla = T002W5_A937ContratoServicosTelas_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A937ContratoServicosTelas_ServicoSigla", A937ContratoServicosTelas_ServicoSigla);
            n937ContratoServicosTelas_ServicoSigla = T002W5_n937ContratoServicosTelas_ServicoSigla[0];
            pr_default.close(3);
         }
      }

      protected void Load2W116( )
      {
         /* Using cursor T002W9 */
         pr_default.execute(7, new Object[] {A938ContratoServicosTelas_Sequencial, A926ContratoServicosTelas_ContratoCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound116 = 1;
            A74Contrato_Codigo = T002W9_A74Contrato_Codigo[0];
            n74Contrato_Codigo = T002W9_n74Contrato_Codigo[0];
            A39Contratada_Codigo = T002W9_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = T002W9_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = T002W9_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T002W9_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = T002W9_A937ContratoServicosTelas_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A937ContratoServicosTelas_ServicoSigla", A937ContratoServicosTelas_ServicoSigla);
            n937ContratoServicosTelas_ServicoSigla = T002W9_n937ContratoServicosTelas_ServicoSigla[0];
            A931ContratoServicosTelas_Tela = T002W9_A931ContratoServicosTelas_Tela[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A931ContratoServicosTelas_Tela", A931ContratoServicosTelas_Tela);
            A928ContratoServicosTelas_Link = T002W9_A928ContratoServicosTelas_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A928ContratoServicosTelas_Link", A928ContratoServicosTelas_Link);
            A929ContratoServicosTelas_Parms = T002W9_A929ContratoServicosTelas_Parms[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A929ContratoServicosTelas_Parms", A929ContratoServicosTelas_Parms);
            n929ContratoServicosTelas_Parms = T002W9_n929ContratoServicosTelas_Parms[0];
            A932ContratoServicosTelas_Status = T002W9_A932ContratoServicosTelas_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
            n932ContratoServicosTelas_Status = T002W9_n932ContratoServicosTelas_Status[0];
            A934ContratoServicosTelas_TextMouse = T002W9_A934ContratoServicosTelas_TextMouse[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A934ContratoServicosTelas_TextMouse", A934ContratoServicosTelas_TextMouse);
            A925ContratoServicosTelas_ServicoCod = T002W9_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = T002W9_n925ContratoServicosTelas_ServicoCod[0];
            ZM2W116( -9) ;
         }
         pr_default.close(7);
         OnLoadActions2W116( ) ;
      }

      protected void OnLoadActions2W116( )
      {
      }

      protected void CheckExtendedTable2W116( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002W4 */
         pr_default.execute(2, new Object[] {A926ContratoServicosTelas_ContratoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Telas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A74Contrato_Codigo = T002W4_A74Contrato_Codigo[0];
         n74Contrato_Codigo = T002W4_n74Contrato_Codigo[0];
         A925ContratoServicosTelas_ServicoCod = T002W4_A925ContratoServicosTelas_ServicoCod[0];
         n925ContratoServicosTelas_ServicoCod = T002W4_n925ContratoServicosTelas_ServicoCod[0];
         pr_default.close(2);
         /* Using cursor T002W6 */
         pr_default.execute(4, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T002W6_A39Contratada_Codigo[0];
         pr_default.close(4);
         /* Using cursor T002W7 */
         pr_default.execute(5, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T002W7_A40Contratada_PessoaCod[0];
         pr_default.close(5);
         /* Using cursor T002W8 */
         pr_default.execute(6, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T002W8_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T002W8_n41Contratada_PessoaNom[0];
         pr_default.close(6);
         /* Using cursor T002W5 */
         pr_default.execute(3, new Object[] {n925ContratoServicosTelas_ServicoCod, A925ContratoServicosTelas_ServicoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Telas_Servico'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A937ContratoServicosTelas_ServicoSigla = T002W5_A937ContratoServicosTelas_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A937ContratoServicosTelas_ServicoSigla", A937ContratoServicosTelas_ServicoSigla);
         n937ContratoServicosTelas_ServicoSigla = T002W5_n937ContratoServicosTelas_ServicoSigla[0];
         pr_default.close(3);
         if ( ! ( ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "B") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "S") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "E") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "A") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "R") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "C") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "D") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "H") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "O") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "P") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "L") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "X") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "N") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "J") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "I") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "T") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "Q") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "G") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "M") == 0 ) || ( StringUtil.StrCmp(A932ContratoServicosTelas_Status, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A932ContratoServicosTelas_Status)) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "CONTRATOSERVICOSTELAS_STATUS");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicosTelas_Status_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2W116( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A926ContratoServicosTelas_ContratoCod )
      {
         /* Using cursor T002W10 */
         pr_default.execute(8, new Object[] {A926ContratoServicosTelas_ContratoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Telas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A74Contrato_Codigo = T002W10_A74Contrato_Codigo[0];
         n74Contrato_Codigo = T002W10_n74Contrato_Codigo[0];
         A925ContratoServicosTelas_ServicoCod = T002W10_A925ContratoServicosTelas_ServicoCod[0];
         n925ContratoServicosTelas_ServicoCod = T002W10_n925ContratoServicosTelas_ServicoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_12( int A74Contrato_Codigo )
      {
         /* Using cursor T002W11 */
         pr_default.execute(9, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T002W11_A39Contratada_Codigo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_13( int A39Contratada_Codigo )
      {
         /* Using cursor T002W12 */
         pr_default.execute(10, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T002W12_A40Contratada_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_14( int A40Contratada_PessoaCod )
      {
         /* Using cursor T002W13 */
         pr_default.execute(11, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T002W13_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T002W13_n41Contratada_PessoaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A41Contratada_PessoaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_11( int A925ContratoServicosTelas_ServicoCod )
      {
         /* Using cursor T002W14 */
         pr_default.execute(12, new Object[] {n925ContratoServicosTelas_ServicoCod, A925ContratoServicosTelas_ServicoCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Telas_Servico'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A937ContratoServicosTelas_ServicoSigla = T002W14_A937ContratoServicosTelas_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A937ContratoServicosTelas_ServicoSigla", A937ContratoServicosTelas_ServicoSigla);
         n937ContratoServicosTelas_ServicoSigla = T002W14_n937ContratoServicosTelas_ServicoSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A937ContratoServicosTelas_ServicoSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey2W116( )
      {
         /* Using cursor T002W15 */
         pr_default.execute(13, new Object[] {A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound116 = 1;
         }
         else
         {
            RcdFound116 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002W3 */
         pr_default.execute(1, new Object[] {A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2W116( 9) ;
            RcdFound116 = 1;
            A938ContratoServicosTelas_Sequencial = T002W3_A938ContratoServicosTelas_Sequencial[0];
            A931ContratoServicosTelas_Tela = T002W3_A931ContratoServicosTelas_Tela[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A931ContratoServicosTelas_Tela", A931ContratoServicosTelas_Tela);
            A928ContratoServicosTelas_Link = T002W3_A928ContratoServicosTelas_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A928ContratoServicosTelas_Link", A928ContratoServicosTelas_Link);
            A929ContratoServicosTelas_Parms = T002W3_A929ContratoServicosTelas_Parms[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A929ContratoServicosTelas_Parms", A929ContratoServicosTelas_Parms);
            n929ContratoServicosTelas_Parms = T002W3_n929ContratoServicosTelas_Parms[0];
            A932ContratoServicosTelas_Status = T002W3_A932ContratoServicosTelas_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
            n932ContratoServicosTelas_Status = T002W3_n932ContratoServicosTelas_Status[0];
            A934ContratoServicosTelas_TextMouse = T002W3_A934ContratoServicosTelas_TextMouse[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A934ContratoServicosTelas_TextMouse", A934ContratoServicosTelas_TextMouse);
            A926ContratoServicosTelas_ContratoCod = T002W3_A926ContratoServicosTelas_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
            Z926ContratoServicosTelas_ContratoCod = A926ContratoServicosTelas_ContratoCod;
            Z938ContratoServicosTelas_Sequencial = A938ContratoServicosTelas_Sequencial;
            sMode116 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2W116( ) ;
            if ( AnyError == 1 )
            {
               RcdFound116 = 0;
               InitializeNonKey2W116( ) ;
            }
            Gx_mode = sMode116;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound116 = 0;
            InitializeNonKey2W116( ) ;
            sMode116 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode116;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2W116( ) ;
         if ( RcdFound116 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound116 = 0;
         /* Using cursor T002W16 */
         pr_default.execute(14, new Object[] {A938ContratoServicosTelas_Sequencial, A926ContratoServicosTelas_ContratoCod});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T002W16_A938ContratoServicosTelas_Sequencial[0] < A938ContratoServicosTelas_Sequencial ) || ( T002W16_A938ContratoServicosTelas_Sequencial[0] == A938ContratoServicosTelas_Sequencial ) && ( T002W16_A926ContratoServicosTelas_ContratoCod[0] < A926ContratoServicosTelas_ContratoCod ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T002W16_A938ContratoServicosTelas_Sequencial[0] > A938ContratoServicosTelas_Sequencial ) || ( T002W16_A938ContratoServicosTelas_Sequencial[0] == A938ContratoServicosTelas_Sequencial ) && ( T002W16_A926ContratoServicosTelas_ContratoCod[0] > A926ContratoServicosTelas_ContratoCod ) ) )
            {
               A938ContratoServicosTelas_Sequencial = T002W16_A938ContratoServicosTelas_Sequencial[0];
               A926ContratoServicosTelas_ContratoCod = T002W16_A926ContratoServicosTelas_ContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
               RcdFound116 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound116 = 0;
         /* Using cursor T002W17 */
         pr_default.execute(15, new Object[] {A938ContratoServicosTelas_Sequencial, A926ContratoServicosTelas_ContratoCod});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T002W17_A938ContratoServicosTelas_Sequencial[0] > A938ContratoServicosTelas_Sequencial ) || ( T002W17_A938ContratoServicosTelas_Sequencial[0] == A938ContratoServicosTelas_Sequencial ) && ( T002W17_A926ContratoServicosTelas_ContratoCod[0] > A926ContratoServicosTelas_ContratoCod ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T002W17_A938ContratoServicosTelas_Sequencial[0] < A938ContratoServicosTelas_Sequencial ) || ( T002W17_A938ContratoServicosTelas_Sequencial[0] == A938ContratoServicosTelas_Sequencial ) && ( T002W17_A926ContratoServicosTelas_ContratoCod[0] < A926ContratoServicosTelas_ContratoCod ) ) )
            {
               A938ContratoServicosTelas_Sequencial = T002W17_A938ContratoServicosTelas_Sequencial[0];
               A926ContratoServicosTelas_ContratoCod = T002W17_A926ContratoServicosTelas_ContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
               RcdFound116 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2W116( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoServicosTelas_Tela_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2W116( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound116 == 1 )
            {
               if ( ( A926ContratoServicosTelas_ContratoCod != Z926ContratoServicosTelas_ContratoCod ) || ( A938ContratoServicosTelas_Sequencial != Z938ContratoServicosTelas_Sequencial ) )
               {
                  A926ContratoServicosTelas_ContratoCod = Z926ContratoServicosTelas_ContratoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
                  A938ContratoServicosTelas_Sequencial = Z938ContratoServicosTelas_Sequencial;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoServicosTelas_Tela_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2W116( ) ;
                  GX_FocusControl = edtContratoServicosTelas_Tela_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A926ContratoServicosTelas_ContratoCod != Z926ContratoServicosTelas_ContratoCod ) || ( A938ContratoServicosTelas_Sequencial != Z938ContratoServicosTelas_Sequencial ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoServicosTelas_Tela_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2W116( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoServicosTelas_Tela_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2W116( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A926ContratoServicosTelas_ContratoCod != Z926ContratoServicosTelas_ContratoCod ) || ( A938ContratoServicosTelas_Sequencial != Z938ContratoServicosTelas_Sequencial ) )
         {
            A926ContratoServicosTelas_ContratoCod = Z926ContratoServicosTelas_ContratoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
            A938ContratoServicosTelas_Sequencial = Z938ContratoServicosTelas_Sequencial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoServicosTelas_Tela_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2W116( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002W2 */
            pr_default.execute(0, new Object[] {A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosTelas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z931ContratoServicosTelas_Tela, T002W2_A931ContratoServicosTelas_Tela[0]) != 0 ) || ( StringUtil.StrCmp(Z928ContratoServicosTelas_Link, T002W2_A928ContratoServicosTelas_Link[0]) != 0 ) || ( StringUtil.StrCmp(Z932ContratoServicosTelas_Status, T002W2_A932ContratoServicosTelas_Status[0]) != 0 ) || ( StringUtil.StrCmp(Z934ContratoServicosTelas_TextMouse, T002W2_A934ContratoServicosTelas_TextMouse[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z931ContratoServicosTelas_Tela, T002W2_A931ContratoServicosTelas_Tela[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicostelas:[seudo value changed for attri]"+"ContratoServicosTelas_Tela");
                  GXUtil.WriteLogRaw("Old: ",Z931ContratoServicosTelas_Tela);
                  GXUtil.WriteLogRaw("Current: ",T002W2_A931ContratoServicosTelas_Tela[0]);
               }
               if ( StringUtil.StrCmp(Z928ContratoServicosTelas_Link, T002W2_A928ContratoServicosTelas_Link[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicostelas:[seudo value changed for attri]"+"ContratoServicosTelas_Link");
                  GXUtil.WriteLogRaw("Old: ",Z928ContratoServicosTelas_Link);
                  GXUtil.WriteLogRaw("Current: ",T002W2_A928ContratoServicosTelas_Link[0]);
               }
               if ( StringUtil.StrCmp(Z932ContratoServicosTelas_Status, T002W2_A932ContratoServicosTelas_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicostelas:[seudo value changed for attri]"+"ContratoServicosTelas_Status");
                  GXUtil.WriteLogRaw("Old: ",Z932ContratoServicosTelas_Status);
                  GXUtil.WriteLogRaw("Current: ",T002W2_A932ContratoServicosTelas_Status[0]);
               }
               if ( StringUtil.StrCmp(Z934ContratoServicosTelas_TextMouse, T002W2_A934ContratoServicosTelas_TextMouse[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicostelas:[seudo value changed for attri]"+"ContratoServicosTelas_TextMouse");
                  GXUtil.WriteLogRaw("Old: ",Z934ContratoServicosTelas_TextMouse);
                  GXUtil.WriteLogRaw("Current: ",T002W2_A934ContratoServicosTelas_TextMouse[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosTelas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2W116( )
      {
         BeforeValidate2W116( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2W116( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2W116( 0) ;
            CheckOptimisticConcurrency2W116( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2W116( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2W116( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002W18 */
                     pr_default.execute(16, new Object[] {A938ContratoServicosTelas_Sequencial, A931ContratoServicosTelas_Tela, A928ContratoServicosTelas_Link, n929ContratoServicosTelas_Parms, A929ContratoServicosTelas_Parms, n932ContratoServicosTelas_Status, A932ContratoServicosTelas_Status, A934ContratoServicosTelas_TextMouse, A926ContratoServicosTelas_ContratoCod});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosTelas") ;
                     if ( (pr_default.getStatus(16) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2W0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2W116( ) ;
            }
            EndLevel2W116( ) ;
         }
         CloseExtendedTableCursors2W116( ) ;
      }

      protected void Update2W116( )
      {
         BeforeValidate2W116( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2W116( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2W116( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2W116( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2W116( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002W19 */
                     pr_default.execute(17, new Object[] {A931ContratoServicosTelas_Tela, A928ContratoServicosTelas_Link, n929ContratoServicosTelas_Parms, A929ContratoServicosTelas_Parms, n932ContratoServicosTelas_Status, A932ContratoServicosTelas_Status, A934ContratoServicosTelas_TextMouse, A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosTelas") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosTelas"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2W116( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2W116( ) ;
         }
         CloseExtendedTableCursors2W116( ) ;
      }

      protected void DeferredUpdate2W116( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2W116( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2W116( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2W116( ) ;
            AfterConfirm2W116( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2W116( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002W20 */
                  pr_default.execute(18, new Object[] {A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosTelas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode116 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2W116( ) ;
         Gx_mode = sMode116;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2W116( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002W21 */
            pr_default.execute(19, new Object[] {A926ContratoServicosTelas_ContratoCod});
            A74Contrato_Codigo = T002W21_A74Contrato_Codigo[0];
            n74Contrato_Codigo = T002W21_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = T002W21_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = T002W21_n925ContratoServicosTelas_ServicoCod[0];
            pr_default.close(19);
            /* Using cursor T002W22 */
            pr_default.execute(20, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            A39Contratada_Codigo = T002W22_A39Contratada_Codigo[0];
            pr_default.close(20);
            /* Using cursor T002W23 */
            pr_default.execute(21, new Object[] {A39Contratada_Codigo});
            A40Contratada_PessoaCod = T002W23_A40Contratada_PessoaCod[0];
            pr_default.close(21);
            /* Using cursor T002W24 */
            pr_default.execute(22, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T002W24_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T002W24_n41Contratada_PessoaNom[0];
            pr_default.close(22);
            /* Using cursor T002W25 */
            pr_default.execute(23, new Object[] {n925ContratoServicosTelas_ServicoCod, A925ContratoServicosTelas_ServicoCod});
            A937ContratoServicosTelas_ServicoSigla = T002W25_A937ContratoServicosTelas_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A937ContratoServicosTelas_ServicoSigla", A937ContratoServicosTelas_ServicoSigla);
            n937ContratoServicosTelas_ServicoSigla = T002W25_n937ContratoServicosTelas_ServicoSigla[0];
            pr_default.close(23);
         }
      }

      protected void EndLevel2W116( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2W116( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(23);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(22);
            context.CommitDataStores( "ContratoServicosTelas");
            if ( AnyError == 0 )
            {
               ConfirmValues2W0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(23);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(22);
            context.RollbackDataStores( "ContratoServicosTelas");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2W116( )
      {
         /* Scan By routine */
         /* Using cursor T002W26 */
         pr_default.execute(24);
         RcdFound116 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound116 = 1;
            A926ContratoServicosTelas_ContratoCod = T002W26_A926ContratoServicosTelas_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
            A938ContratoServicosTelas_Sequencial = T002W26_A938ContratoServicosTelas_Sequencial[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2W116( )
      {
         /* Scan next routine */
         pr_default.readNext(24);
         RcdFound116 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound116 = 1;
            A926ContratoServicosTelas_ContratoCod = T002W26_A926ContratoServicosTelas_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
            A938ContratoServicosTelas_Sequencial = T002W26_A938ContratoServicosTelas_Sequencial[0];
         }
      }

      protected void ScanEnd2W116( )
      {
         pr_default.close(24);
      }

      protected void AfterConfirm2W116( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2W116( )
      {
         /* Before Insert Rules */
         GXt_int1 = A938ContratoServicosTelas_Sequencial;
         new prc_getcontratoservicostelas_sequencia(context ).execute(  AV7ContratoServicosTelas_ContratoCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSTELAS_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
         A938ContratoServicosTelas_Sequencial = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
      }

      protected void BeforeUpdate2W116( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2W116( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2W116( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2W116( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2W116( )
      {
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratoServicosTelas_ServicoSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ServicoSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ServicoSigla_Enabled), 5, 0)));
         edtContratoServicosTelas_Tela_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_Tela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_Tela_Enabled), 5, 0)));
         edtContratoServicosTelas_Link_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_Link_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_Link_Enabled), 5, 0)));
         edtContratoServicosTelas_Parms_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_Parms_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_Parms_Enabled), 5, 0)));
         cmbContratoServicosTelas_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosTelas_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosTelas_Status.Enabled), 5, 0)));
         edtContratoServicosTelas_TextMouse_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_TextMouse_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_TextMouse_Enabled), 5, 0)));
         edtContratoServicosTelas_ContratoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosTelas_ContratoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ContratoCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2W0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120212759");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicostelas.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +AV12ContratoServicosTelas_Sequencial)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z926ContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z938ContratoServicosTelas_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z931ContratoServicosTelas_Tela", StringUtil.RTrim( Z931ContratoServicosTelas_Tela));
         GxWebStd.gx_hidden_field( context, "Z928ContratoServicosTelas_Link", Z928ContratoServicosTelas_Link);
         GxWebStd.gx_hidden_field( context, "Z932ContratoServicosTelas_Status", StringUtil.RTrim( Z932ContratoServicosTelas_Status));
         GxWebStd.gx_hidden_field( context, "Z934ContratoServicosTelas_TextMouse", StringUtil.RTrim( Z934ContratoServicosTelas_TextMouse));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSTELAS_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSTELAS_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContratoServicosTelas_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSTELAS_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSTELAS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOSTELAS_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOSTELAS_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ContratoServicosTelas_Sequencial), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosTelas";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicostelas:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicostelas.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +AV12ContratoServicosTelas_Sequencial) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosTelas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tela associada" ;
      }

      protected void InitializeNonKey2W116( )
      {
         A40Contratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         A39Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         A74Contrato_Codigo = 0;
         n74Contrato_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         A925ContratoServicosTelas_ServicoCod = 0;
         n925ContratoServicosTelas_ServicoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A925ContratoServicosTelas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0)));
         A937ContratoServicosTelas_ServicoSigla = "";
         n937ContratoServicosTelas_ServicoSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A937ContratoServicosTelas_ServicoSigla", A937ContratoServicosTelas_ServicoSigla);
         A931ContratoServicosTelas_Tela = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A931ContratoServicosTelas_Tela", A931ContratoServicosTelas_Tela);
         A928ContratoServicosTelas_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A928ContratoServicosTelas_Link", A928ContratoServicosTelas_Link);
         A929ContratoServicosTelas_Parms = "";
         n929ContratoServicosTelas_Parms = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A929ContratoServicosTelas_Parms", A929ContratoServicosTelas_Parms);
         n929ContratoServicosTelas_Parms = (String.IsNullOrEmpty(StringUtil.RTrim( A929ContratoServicosTelas_Parms)) ? true : false);
         A932ContratoServicosTelas_Status = "";
         n932ContratoServicosTelas_Status = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
         n932ContratoServicosTelas_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A932ContratoServicosTelas_Status)) ? true : false);
         A934ContratoServicosTelas_TextMouse = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A934ContratoServicosTelas_TextMouse", A934ContratoServicosTelas_TextMouse);
         Z931ContratoServicosTelas_Tela = "";
         Z928ContratoServicosTelas_Link = "";
         Z932ContratoServicosTelas_Status = "";
         Z934ContratoServicosTelas_TextMouse = "";
      }

      protected void InitAll2W116( )
      {
         A926ContratoServicosTelas_ContratoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
         A938ContratoServicosTelas_Sequencial = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
         InitializeNonKey2W116( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120212781");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicostelas.js", "?20203120212781");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratoservicostelas_servicosigla_Internalname = "TEXTBLOCKCONTRATOSERVICOSTELAS_SERVICOSIGLA";
         edtContratoServicosTelas_ServicoSigla_Internalname = "CONTRATOSERVICOSTELAS_SERVICOSIGLA";
         lblTextblockcontratoservicostelas_tela_Internalname = "TEXTBLOCKCONTRATOSERVICOSTELAS_TELA";
         edtContratoServicosTelas_Tela_Internalname = "CONTRATOSERVICOSTELAS_TELA";
         lblTextblockcontratoservicostelas_link_Internalname = "TEXTBLOCKCONTRATOSERVICOSTELAS_LINK";
         edtContratoServicosTelas_Link_Internalname = "CONTRATOSERVICOSTELAS_LINK";
         lblTextblockcontratoservicostelas_parms_Internalname = "TEXTBLOCKCONTRATOSERVICOSTELAS_PARMS";
         edtContratoServicosTelas_Parms_Internalname = "CONTRATOSERVICOSTELAS_PARMS";
         lblTextblockcontratoservicostelas_status_Internalname = "TEXTBLOCKCONTRATOSERVICOSTELAS_STATUS";
         cmbContratoServicosTelas_Status_Internalname = "CONTRATOSERVICOSTELAS_STATUS";
         lblTextblockcontratoservicostelas_textmouse_Internalname = "TEXTBLOCKCONTRATOSERVICOSTELAS_TEXTMOUSE";
         edtContratoServicosTelas_TextMouse_Internalname = "CONTRATOSERVICOSTELAS_TEXTMOUSE";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoServicosTelas_ContratoCod_Internalname = "CONTRATOSERVICOSTELAS_CONTRATOCOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Tela associada";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tela associada";
         edtContratoServicosTelas_TextMouse_Jsonclick = "";
         edtContratoServicosTelas_TextMouse_Enabled = 1;
         cmbContratoServicosTelas_Status_Jsonclick = "";
         cmbContratoServicosTelas_Status.Enabled = 1;
         edtContratoServicosTelas_Parms_Jsonclick = "";
         edtContratoServicosTelas_Parms_Enabled = 1;
         edtContratoServicosTelas_Link_Jsonclick = "";
         edtContratoServicosTelas_Link_Enabled = 1;
         edtContratoServicosTelas_Tela_Jsonclick = "";
         edtContratoServicosTelas_Tela_Enabled = 1;
         edtContratoServicosTelas_ServicoSigla_Jsonclick = "";
         edtContratoServicosTelas_ServicoSigla_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratoServicosTelas_ContratoCod_Jsonclick = "";
         edtContratoServicosTelas_ContratoCod_Enabled = 1;
         edtContratoServicosTelas_ContratoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX5ASACONTRATOSERVICOSTELAS_SEQUENCIAL2W116( short AV12ContratoServicosTelas_Sequencial )
      {
         if ( ! (0==AV12ContratoServicosTelas_Sequencial) )
         {
            A938ContratoServicosTelas_Sequencial = AV12ContratoServicosTelas_Sequencial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX6ASACONTRATOSERVICOSTELAS_SEQUENCIAL2W116( int AV7ContratoServicosTelas_ContratoCod )
      {
         GXt_int1 = A938ContratoServicosTelas_Sequencial;
         new prc_getcontratoservicostelas_sequencia(context ).execute(  AV7ContratoServicosTelas_ContratoCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSTELAS_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
         A938ContratoServicosTelas_Sequencial = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contratoservicostelas_contratocod( int GX_Parm1 ,
                                                           int GX_Parm2 ,
                                                           int GX_Parm3 ,
                                                           int GX_Parm4 ,
                                                           int GX_Parm5 ,
                                                           String GX_Parm6 ,
                                                           String GX_Parm7 )
      {
         A926ContratoServicosTelas_ContratoCod = GX_Parm1;
         A74Contrato_Codigo = GX_Parm2;
         n74Contrato_Codigo = false;
         A39Contratada_Codigo = GX_Parm3;
         A40Contratada_PessoaCod = GX_Parm4;
         A925ContratoServicosTelas_ServicoCod = GX_Parm5;
         n925ContratoServicosTelas_ServicoCod = false;
         A41Contratada_PessoaNom = GX_Parm6;
         n41Contratada_PessoaNom = false;
         A937ContratoServicosTelas_ServicoSigla = GX_Parm7;
         n937ContratoServicosTelas_ServicoSigla = false;
         /* Using cursor T002W21 */
         pr_default.execute(19, new Object[] {A926ContratoServicosTelas_ContratoCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Telas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSTELAS_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosTelas_ContratoCod_Internalname;
         }
         A74Contrato_Codigo = T002W21_A74Contrato_Codigo[0];
         n74Contrato_Codigo = T002W21_n74Contrato_Codigo[0];
         A925ContratoServicosTelas_ServicoCod = T002W21_A925ContratoServicosTelas_ServicoCod[0];
         n925ContratoServicosTelas_ServicoCod = T002W21_n925ContratoServicosTelas_ServicoCod[0];
         pr_default.close(19);
         /* Using cursor T002W22 */
         pr_default.execute(20, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T002W22_A39Contratada_Codigo[0];
         pr_default.close(20);
         /* Using cursor T002W23 */
         pr_default.execute(21, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T002W23_A40Contratada_PessoaCod[0];
         pr_default.close(21);
         /* Using cursor T002W24 */
         pr_default.execute(22, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T002W24_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = T002W24_n41Contratada_PessoaNom[0];
         pr_default.close(22);
         /* Using cursor T002W25 */
         pr_default.execute(23, new Object[] {n925ContratoServicosTelas_ServicoCod, A925ContratoServicosTelas_ServicoCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Telas_Servico'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A937ContratoServicosTelas_ServicoSigla = T002W25_A937ContratoServicosTelas_ServicoSigla[0];
         n937ContratoServicosTelas_ServicoSigla = T002W25_n937ContratoServicosTelas_ServicoSigla[0];
         pr_default.close(23);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A74Contrato_Codigo = 0;
            n74Contrato_Codigo = false;
            A925ContratoServicosTelas_ServicoCod = 0;
            n925ContratoServicosTelas_ServicoCod = false;
            A39Contratada_Codigo = 0;
            A40Contratada_PessoaCod = 0;
            A41Contratada_PessoaNom = "";
            n41Contratada_PessoaNom = false;
            A937ContratoServicosTelas_ServicoSigla = "";
            n937ContratoServicosTelas_ServicoSigla = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A925ContratoServicosTelas_ServicoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A41Contratada_PessoaNom));
         isValidOutput.Add(StringUtil.RTrim( A937ContratoServicosTelas_ServicoSigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV12ContratoServicosTelas_Sequencial',fld:'vCONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122W2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(23);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(22);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z931ContratoServicosTelas_Tela = "";
         Z928ContratoServicosTelas_Link = "";
         Z932ContratoServicosTelas_Status = "";
         Z934ContratoServicosTelas_TextMouse = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A932ContratoServicosTelas_Status = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratoservicostelas_servicosigla_Jsonclick = "";
         A937ContratoServicosTelas_ServicoSigla = "";
         lblTextblockcontratoservicostelas_tela_Jsonclick = "";
         A931ContratoServicosTelas_Tela = "";
         lblTextblockcontratoservicostelas_link_Jsonclick = "";
         A928ContratoServicosTelas_Link = "";
         lblTextblockcontratoservicostelas_parms_Jsonclick = "";
         A929ContratoServicosTelas_Parms = "";
         lblTextblockcontratoservicostelas_status_Jsonclick = "";
         lblTextblockcontratoservicostelas_textmouse_Jsonclick = "";
         A934ContratoServicosTelas_TextMouse = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode116 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z929ContratoServicosTelas_Parms = "";
         Z41Contratada_PessoaNom = "";
         Z937ContratoServicosTelas_ServicoSigla = "";
         T002W4_A74Contrato_Codigo = new int[1] ;
         T002W4_n74Contrato_Codigo = new bool[] {false} ;
         T002W4_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         T002W4_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         T002W6_A39Contratada_Codigo = new int[1] ;
         T002W7_A40Contratada_PessoaCod = new int[1] ;
         T002W8_A41Contratada_PessoaNom = new String[] {""} ;
         T002W8_n41Contratada_PessoaNom = new bool[] {false} ;
         T002W5_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         T002W5_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         T002W9_A74Contrato_Codigo = new int[1] ;
         T002W9_n74Contrato_Codigo = new bool[] {false} ;
         T002W9_A39Contratada_Codigo = new int[1] ;
         T002W9_A40Contratada_PessoaCod = new int[1] ;
         T002W9_A938ContratoServicosTelas_Sequencial = new short[1] ;
         T002W9_A41Contratada_PessoaNom = new String[] {""} ;
         T002W9_n41Contratada_PessoaNom = new bool[] {false} ;
         T002W9_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         T002W9_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         T002W9_A931ContratoServicosTelas_Tela = new String[] {""} ;
         T002W9_A928ContratoServicosTelas_Link = new String[] {""} ;
         T002W9_A929ContratoServicosTelas_Parms = new String[] {""} ;
         T002W9_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         T002W9_A932ContratoServicosTelas_Status = new String[] {""} ;
         T002W9_n932ContratoServicosTelas_Status = new bool[] {false} ;
         T002W9_A934ContratoServicosTelas_TextMouse = new String[] {""} ;
         T002W9_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T002W9_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         T002W9_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         T002W10_A74Contrato_Codigo = new int[1] ;
         T002W10_n74Contrato_Codigo = new bool[] {false} ;
         T002W10_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         T002W10_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         T002W11_A39Contratada_Codigo = new int[1] ;
         T002W12_A40Contratada_PessoaCod = new int[1] ;
         T002W13_A41Contratada_PessoaNom = new String[] {""} ;
         T002W13_n41Contratada_PessoaNom = new bool[] {false} ;
         T002W14_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         T002W14_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         T002W15_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T002W15_A938ContratoServicosTelas_Sequencial = new short[1] ;
         T002W3_A938ContratoServicosTelas_Sequencial = new short[1] ;
         T002W3_A931ContratoServicosTelas_Tela = new String[] {""} ;
         T002W3_A928ContratoServicosTelas_Link = new String[] {""} ;
         T002W3_A929ContratoServicosTelas_Parms = new String[] {""} ;
         T002W3_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         T002W3_A932ContratoServicosTelas_Status = new String[] {""} ;
         T002W3_n932ContratoServicosTelas_Status = new bool[] {false} ;
         T002W3_A934ContratoServicosTelas_TextMouse = new String[] {""} ;
         T002W3_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T002W16_A938ContratoServicosTelas_Sequencial = new short[1] ;
         T002W16_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T002W17_A938ContratoServicosTelas_Sequencial = new short[1] ;
         T002W17_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T002W2_A938ContratoServicosTelas_Sequencial = new short[1] ;
         T002W2_A931ContratoServicosTelas_Tela = new String[] {""} ;
         T002W2_A928ContratoServicosTelas_Link = new String[] {""} ;
         T002W2_A929ContratoServicosTelas_Parms = new String[] {""} ;
         T002W2_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         T002W2_A932ContratoServicosTelas_Status = new String[] {""} ;
         T002W2_n932ContratoServicosTelas_Status = new bool[] {false} ;
         T002W2_A934ContratoServicosTelas_TextMouse = new String[] {""} ;
         T002W2_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T002W21_A74Contrato_Codigo = new int[1] ;
         T002W21_n74Contrato_Codigo = new bool[] {false} ;
         T002W21_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         T002W21_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         T002W22_A39Contratada_Codigo = new int[1] ;
         T002W23_A40Contratada_PessoaCod = new int[1] ;
         T002W24_A41Contratada_PessoaNom = new String[] {""} ;
         T002W24_n41Contratada_PessoaNom = new bool[] {false} ;
         T002W25_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         T002W25_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         T002W26_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T002W26_A938ContratoServicosTelas_Sequencial = new short[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicostelas__default(),
            new Object[][] {
                new Object[] {
               T002W2_A938ContratoServicosTelas_Sequencial, T002W2_A931ContratoServicosTelas_Tela, T002W2_A928ContratoServicosTelas_Link, T002W2_A929ContratoServicosTelas_Parms, T002W2_n929ContratoServicosTelas_Parms, T002W2_A932ContratoServicosTelas_Status, T002W2_n932ContratoServicosTelas_Status, T002W2_A934ContratoServicosTelas_TextMouse, T002W2_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               T002W3_A938ContratoServicosTelas_Sequencial, T002W3_A931ContratoServicosTelas_Tela, T002W3_A928ContratoServicosTelas_Link, T002W3_A929ContratoServicosTelas_Parms, T002W3_n929ContratoServicosTelas_Parms, T002W3_A932ContratoServicosTelas_Status, T002W3_n932ContratoServicosTelas_Status, T002W3_A934ContratoServicosTelas_TextMouse, T002W3_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               T002W4_A74Contrato_Codigo, T002W4_n74Contrato_Codigo, T002W4_A925ContratoServicosTelas_ServicoCod, T002W4_n925ContratoServicosTelas_ServicoCod
               }
               , new Object[] {
               T002W5_A937ContratoServicosTelas_ServicoSigla, T002W5_n937ContratoServicosTelas_ServicoSigla
               }
               , new Object[] {
               T002W6_A39Contratada_Codigo
               }
               , new Object[] {
               T002W7_A40Contratada_PessoaCod
               }
               , new Object[] {
               T002W8_A41Contratada_PessoaNom, T002W8_n41Contratada_PessoaNom
               }
               , new Object[] {
               T002W9_A74Contrato_Codigo, T002W9_n74Contrato_Codigo, T002W9_A39Contratada_Codigo, T002W9_A40Contratada_PessoaCod, T002W9_A938ContratoServicosTelas_Sequencial, T002W9_A41Contratada_PessoaNom, T002W9_n41Contratada_PessoaNom, T002W9_A937ContratoServicosTelas_ServicoSigla, T002W9_n937ContratoServicosTelas_ServicoSigla, T002W9_A931ContratoServicosTelas_Tela,
               T002W9_A928ContratoServicosTelas_Link, T002W9_A929ContratoServicosTelas_Parms, T002W9_n929ContratoServicosTelas_Parms, T002W9_A932ContratoServicosTelas_Status, T002W9_n932ContratoServicosTelas_Status, T002W9_A934ContratoServicosTelas_TextMouse, T002W9_A926ContratoServicosTelas_ContratoCod, T002W9_A925ContratoServicosTelas_ServicoCod, T002W9_n925ContratoServicosTelas_ServicoCod
               }
               , new Object[] {
               T002W10_A74Contrato_Codigo, T002W10_n74Contrato_Codigo, T002W10_A925ContratoServicosTelas_ServicoCod, T002W10_n925ContratoServicosTelas_ServicoCod
               }
               , new Object[] {
               T002W11_A39Contratada_Codigo
               }
               , new Object[] {
               T002W12_A40Contratada_PessoaCod
               }
               , new Object[] {
               T002W13_A41Contratada_PessoaNom, T002W13_n41Contratada_PessoaNom
               }
               , new Object[] {
               T002W14_A937ContratoServicosTelas_ServicoSigla, T002W14_n937ContratoServicosTelas_ServicoSigla
               }
               , new Object[] {
               T002W15_A926ContratoServicosTelas_ContratoCod, T002W15_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               T002W16_A938ContratoServicosTelas_Sequencial, T002W16_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               T002W17_A938ContratoServicosTelas_Sequencial, T002W17_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002W21_A74Contrato_Codigo, T002W21_n74Contrato_Codigo, T002W21_A925ContratoServicosTelas_ServicoCod, T002W21_n925ContratoServicosTelas_ServicoCod
               }
               , new Object[] {
               T002W22_A39Contratada_Codigo
               }
               , new Object[] {
               T002W23_A40Contratada_PessoaCod
               }
               , new Object[] {
               T002W24_A41Contratada_PessoaNom, T002W24_n41Contratada_PessoaNom
               }
               , new Object[] {
               T002W25_A937ContratoServicosTelas_ServicoSigla, T002W25_n937ContratoServicosTelas_ServicoSigla
               }
               , new Object[] {
               T002W26_A926ContratoServicosTelas_ContratoCod, T002W26_A938ContratoServicosTelas_Sequencial
               }
            }
         );
      }

      private short wcpOAV12ContratoServicosTelas_Sequencial ;
      private short Z938ContratoServicosTelas_Sequencial ;
      private short GxWebError ;
      private short AV12ContratoServicosTelas_Sequencial ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A938ContratoServicosTelas_Sequencial ;
      private short RcdFound116 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short GXt_int1 ;
      private short wbTemp ;
      private int wcpOAV7ContratoServicosTelas_ContratoCod ;
      private int Z926ContratoServicosTelas_ContratoCod ;
      private int AV7ContratoServicosTelas_ContratoCod ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A925ContratoServicosTelas_ServicoCod ;
      private int trnEnded ;
      private int edtContratoServicosTelas_ContratoCod_Visible ;
      private int edtContratoServicosTelas_ContratoCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratoServicosTelas_ServicoSigla_Enabled ;
      private int edtContratoServicosTelas_Tela_Enabled ;
      private int edtContratoServicosTelas_Link_Enabled ;
      private int edtContratoServicosTelas_Parms_Enabled ;
      private int edtContratoServicosTelas_TextMouse_Enabled ;
      private int Z74Contrato_Codigo ;
      private int Z925ContratoServicosTelas_ServicoCod ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z931ContratoServicosTelas_Tela ;
      private String Z932ContratoServicosTelas_Status ;
      private String Z934ContratoServicosTelas_TextMouse ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A932ContratoServicosTelas_Status ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoServicosTelas_Tela_Internalname ;
      private String TempTags ;
      private String edtContratoServicosTelas_ContratoCod_Internalname ;
      private String edtContratoServicosTelas_ContratoCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratoservicostelas_servicosigla_Internalname ;
      private String lblTextblockcontratoservicostelas_servicosigla_Jsonclick ;
      private String edtContratoServicosTelas_ServicoSigla_Internalname ;
      private String A937ContratoServicosTelas_ServicoSigla ;
      private String edtContratoServicosTelas_ServicoSigla_Jsonclick ;
      private String lblTextblockcontratoservicostelas_tela_Internalname ;
      private String lblTextblockcontratoservicostelas_tela_Jsonclick ;
      private String A931ContratoServicosTelas_Tela ;
      private String edtContratoServicosTelas_Tela_Jsonclick ;
      private String lblTextblockcontratoservicostelas_link_Internalname ;
      private String lblTextblockcontratoservicostelas_link_Jsonclick ;
      private String edtContratoServicosTelas_Link_Internalname ;
      private String edtContratoServicosTelas_Link_Jsonclick ;
      private String lblTextblockcontratoservicostelas_parms_Internalname ;
      private String lblTextblockcontratoservicostelas_parms_Jsonclick ;
      private String edtContratoServicosTelas_Parms_Internalname ;
      private String edtContratoServicosTelas_Parms_Jsonclick ;
      private String lblTextblockcontratoservicostelas_status_Internalname ;
      private String lblTextblockcontratoservicostelas_status_Jsonclick ;
      private String cmbContratoServicosTelas_Status_Internalname ;
      private String cmbContratoServicosTelas_Status_Jsonclick ;
      private String lblTextblockcontratoservicostelas_textmouse_Internalname ;
      private String lblTextblockcontratoservicostelas_textmouse_Jsonclick ;
      private String edtContratoServicosTelas_TextMouse_Internalname ;
      private String A934ContratoServicosTelas_TextMouse ;
      private String edtContratoServicosTelas_TextMouse_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode116 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z41Contratada_PessoaNom ;
      private String Z937ContratoServicosTelas_ServicoSigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool n74Contrato_Codigo ;
      private bool n925ContratoServicosTelas_ServicoCod ;
      private bool toggleJsOutput ;
      private bool n932ContratoServicosTelas_Status ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n937ContratoServicosTelas_ServicoSigla ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A929ContratoServicosTelas_Parms ;
      private String Z929ContratoServicosTelas_Parms ;
      private String Z928ContratoServicosTelas_Link ;
      private String A928ContratoServicosTelas_Link ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicosTelas_Status ;
      private IDataStoreProvider pr_default ;
      private int[] T002W4_A74Contrato_Codigo ;
      private bool[] T002W4_n74Contrato_Codigo ;
      private int[] T002W4_A925ContratoServicosTelas_ServicoCod ;
      private bool[] T002W4_n925ContratoServicosTelas_ServicoCod ;
      private int[] T002W6_A39Contratada_Codigo ;
      private int[] T002W7_A40Contratada_PessoaCod ;
      private String[] T002W8_A41Contratada_PessoaNom ;
      private bool[] T002W8_n41Contratada_PessoaNom ;
      private String[] T002W5_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] T002W5_n937ContratoServicosTelas_ServicoSigla ;
      private int[] T002W9_A74Contrato_Codigo ;
      private bool[] T002W9_n74Contrato_Codigo ;
      private int[] T002W9_A39Contratada_Codigo ;
      private int[] T002W9_A40Contratada_PessoaCod ;
      private short[] T002W9_A938ContratoServicosTelas_Sequencial ;
      private String[] T002W9_A41Contratada_PessoaNom ;
      private bool[] T002W9_n41Contratada_PessoaNom ;
      private String[] T002W9_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] T002W9_n937ContratoServicosTelas_ServicoSigla ;
      private String[] T002W9_A931ContratoServicosTelas_Tela ;
      private String[] T002W9_A928ContratoServicosTelas_Link ;
      private String[] T002W9_A929ContratoServicosTelas_Parms ;
      private bool[] T002W9_n929ContratoServicosTelas_Parms ;
      private String[] T002W9_A932ContratoServicosTelas_Status ;
      private bool[] T002W9_n932ContratoServicosTelas_Status ;
      private String[] T002W9_A934ContratoServicosTelas_TextMouse ;
      private int[] T002W9_A926ContratoServicosTelas_ContratoCod ;
      private int[] T002W9_A925ContratoServicosTelas_ServicoCod ;
      private bool[] T002W9_n925ContratoServicosTelas_ServicoCod ;
      private int[] T002W10_A74Contrato_Codigo ;
      private bool[] T002W10_n74Contrato_Codigo ;
      private int[] T002W10_A925ContratoServicosTelas_ServicoCod ;
      private bool[] T002W10_n925ContratoServicosTelas_ServicoCod ;
      private int[] T002W11_A39Contratada_Codigo ;
      private int[] T002W12_A40Contratada_PessoaCod ;
      private String[] T002W13_A41Contratada_PessoaNom ;
      private bool[] T002W13_n41Contratada_PessoaNom ;
      private String[] T002W14_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] T002W14_n937ContratoServicosTelas_ServicoSigla ;
      private int[] T002W15_A926ContratoServicosTelas_ContratoCod ;
      private short[] T002W15_A938ContratoServicosTelas_Sequencial ;
      private short[] T002W3_A938ContratoServicosTelas_Sequencial ;
      private String[] T002W3_A931ContratoServicosTelas_Tela ;
      private String[] T002W3_A928ContratoServicosTelas_Link ;
      private String[] T002W3_A929ContratoServicosTelas_Parms ;
      private bool[] T002W3_n929ContratoServicosTelas_Parms ;
      private String[] T002W3_A932ContratoServicosTelas_Status ;
      private bool[] T002W3_n932ContratoServicosTelas_Status ;
      private String[] T002W3_A934ContratoServicosTelas_TextMouse ;
      private int[] T002W3_A926ContratoServicosTelas_ContratoCod ;
      private short[] T002W16_A938ContratoServicosTelas_Sequencial ;
      private int[] T002W16_A926ContratoServicosTelas_ContratoCod ;
      private short[] T002W17_A938ContratoServicosTelas_Sequencial ;
      private int[] T002W17_A926ContratoServicosTelas_ContratoCod ;
      private short[] T002W2_A938ContratoServicosTelas_Sequencial ;
      private String[] T002W2_A931ContratoServicosTelas_Tela ;
      private String[] T002W2_A928ContratoServicosTelas_Link ;
      private String[] T002W2_A929ContratoServicosTelas_Parms ;
      private bool[] T002W2_n929ContratoServicosTelas_Parms ;
      private String[] T002W2_A932ContratoServicosTelas_Status ;
      private bool[] T002W2_n932ContratoServicosTelas_Status ;
      private String[] T002W2_A934ContratoServicosTelas_TextMouse ;
      private int[] T002W2_A926ContratoServicosTelas_ContratoCod ;
      private int[] T002W21_A74Contrato_Codigo ;
      private bool[] T002W21_n74Contrato_Codigo ;
      private int[] T002W21_A925ContratoServicosTelas_ServicoCod ;
      private bool[] T002W21_n925ContratoServicosTelas_ServicoCod ;
      private int[] T002W22_A39Contratada_Codigo ;
      private int[] T002W23_A40Contratada_PessoaCod ;
      private String[] T002W24_A41Contratada_PessoaNom ;
      private bool[] T002W24_n41Contratada_PessoaNom ;
      private String[] T002W25_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] T002W25_n937ContratoServicosTelas_ServicoSigla ;
      private int[] T002W26_A926ContratoServicosTelas_ContratoCod ;
      private short[] T002W26_A938ContratoServicosTelas_Sequencial ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class contratoservicostelas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002W9 ;
          prmT002W9 = new Object[] {
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W4 ;
          prmT002W4 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W6 ;
          prmT002W6 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W7 ;
          prmT002W7 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W8 ;
          prmT002W8 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W5 ;
          prmT002W5 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W10 ;
          prmT002W10 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W11 ;
          prmT002W11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W12 ;
          prmT002W12 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W13 ;
          prmT002W13 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W14 ;
          prmT002W14 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W15 ;
          prmT002W15 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002W3 ;
          prmT002W3 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002W16 ;
          prmT002W16 = new Object[] {
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W17 ;
          prmT002W17 = new Object[] {
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W2 ;
          prmT002W2 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002W18 ;
          prmT002W18 = new Object[] {
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@ContratoServicosTelas_Parms",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosTelas_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosTelas_TextMouse",SqlDbType.Char,100,0} ,
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W19 ;
          prmT002W19 = new Object[] {
          new Object[] {"@ContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@ContratoServicosTelas_Parms",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosTelas_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosTelas_TextMouse",SqlDbType.Char,100,0} ,
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002W20 ;
          prmT002W20 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosTelas_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002W26 ;
          prmT002W26 = new Object[] {
          } ;
          Object[] prmT002W21 ;
          prmT002W21 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W22 ;
          prmT002W22 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W23 ;
          prmT002W23 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W24 ;
          prmT002W24 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002W25 ;
          prmT002W25 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002W2", "SELECT [ContratoServicosTelas_Sequencial], [ContratoServicosTelas_Tela], [ContratoServicosTelas_Link], [ContratoServicosTelas_Parms], [ContratoServicosTelas_Status], [ContratoServicosTelas_TextMouse], [ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod FROM [ContratoServicosTelas] WITH (UPDLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicosTelas_ContratoCod AND [ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W2,1,0,true,false )
             ,new CursorDef("T002W3", "SELECT [ContratoServicosTelas_Sequencial], [ContratoServicosTelas_Tela], [ContratoServicosTelas_Link], [ContratoServicosTelas_Parms], [ContratoServicosTelas_Status], [ContratoServicosTelas_TextMouse], [ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicosTelas_ContratoCod AND [ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W3,1,0,true,false )
             ,new CursorDef("T002W4", "SELECT [Contrato_Codigo], [Servico_Codigo] AS ContratoServicosTelas_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosTelas_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W4,1,0,true,false )
             ,new CursorDef("T002W5", "SELECT [Servico_Sigla] AS ContratoServicosTelas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosTelas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W5,1,0,true,false )
             ,new CursorDef("T002W6", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W6,1,0,true,false )
             ,new CursorDef("T002W7", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W7,1,0,true,false )
             ,new CursorDef("T002W8", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W8,1,0,true,false )
             ,new CursorDef("T002W9", "SELECT T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, TM1.[ContratoServicosTelas_Sequencial], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, TM1.[ContratoServicosTelas_Tela], TM1.[ContratoServicosTelas_Link], TM1.[ContratoServicosTelas_Parms], TM1.[ContratoServicosTelas_Status], TM1.[ContratoServicosTelas_TextMouse], TM1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod FROM ((((([ContratoServicosTelas] TM1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = TM1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE TM1.[ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial and TM1.[ContratoServicosTelas_ContratoCod] = @ContratoServicosTelas_ContratoCod ORDER BY TM1.[ContratoServicosTelas_ContratoCod], TM1.[ContratoServicosTelas_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002W9,100,0,true,false )
             ,new CursorDef("T002W10", "SELECT [Contrato_Codigo], [Servico_Codigo] AS ContratoServicosTelas_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosTelas_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W10,1,0,true,false )
             ,new CursorDef("T002W11", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W11,1,0,true,false )
             ,new CursorDef("T002W12", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W12,1,0,true,false )
             ,new CursorDef("T002W13", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W13,1,0,true,false )
             ,new CursorDef("T002W14", "SELECT [Servico_Sigla] AS ContratoServicosTelas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosTelas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W14,1,0,true,false )
             ,new CursorDef("T002W15", "SELECT [ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, [ContratoServicosTelas_Sequencial] FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicosTelas_ContratoCod AND [ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002W15,1,0,true,false )
             ,new CursorDef("T002W16", "SELECT TOP 1 [ContratoServicosTelas_Sequencial], [ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE ( [ContratoServicosTelas_Sequencial] > @ContratoServicosTelas_Sequencial or [ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial and [ContratoServicosTelas_ContratoCod] > @ContratoServicosTelas_ContratoCod) ORDER BY [ContratoServicosTelas_ContratoCod], [ContratoServicosTelas_Sequencial]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002W16,1,0,true,true )
             ,new CursorDef("T002W17", "SELECT TOP 1 [ContratoServicosTelas_Sequencial], [ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE ( [ContratoServicosTelas_Sequencial] < @ContratoServicosTelas_Sequencial or [ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial and [ContratoServicosTelas_ContratoCod] < @ContratoServicosTelas_ContratoCod) ORDER BY [ContratoServicosTelas_ContratoCod] DESC, [ContratoServicosTelas_Sequencial] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002W17,1,0,true,true )
             ,new CursorDef("T002W18", "INSERT INTO [ContratoServicosTelas]([ContratoServicosTelas_Sequencial], [ContratoServicosTelas_Tela], [ContratoServicosTelas_Link], [ContratoServicosTelas_Parms], [ContratoServicosTelas_Status], [ContratoServicosTelas_TextMouse], [ContratoServicosTelas_ContratoCod]) VALUES(@ContratoServicosTelas_Sequencial, @ContratoServicosTelas_Tela, @ContratoServicosTelas_Link, @ContratoServicosTelas_Parms, @ContratoServicosTelas_Status, @ContratoServicosTelas_TextMouse, @ContratoServicosTelas_ContratoCod)", GxErrorMask.GX_NOMASK,prmT002W18)
             ,new CursorDef("T002W19", "UPDATE [ContratoServicosTelas] SET [ContratoServicosTelas_Tela]=@ContratoServicosTelas_Tela, [ContratoServicosTelas_Link]=@ContratoServicosTelas_Link, [ContratoServicosTelas_Parms]=@ContratoServicosTelas_Parms, [ContratoServicosTelas_Status]=@ContratoServicosTelas_Status, [ContratoServicosTelas_TextMouse]=@ContratoServicosTelas_TextMouse  WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicosTelas_ContratoCod AND [ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial", GxErrorMask.GX_NOMASK,prmT002W19)
             ,new CursorDef("T002W20", "DELETE FROM [ContratoServicosTelas]  WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicosTelas_ContratoCod AND [ContratoServicosTelas_Sequencial] = @ContratoServicosTelas_Sequencial", GxErrorMask.GX_NOMASK,prmT002W20)
             ,new CursorDef("T002W21", "SELECT [Contrato_Codigo], [Servico_Codigo] AS ContratoServicosTelas_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosTelas_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W21,1,0,true,false )
             ,new CursorDef("T002W22", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W22,1,0,true,false )
             ,new CursorDef("T002W23", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W23,1,0,true,false )
             ,new CursorDef("T002W24", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W24,1,0,true,false )
             ,new CursorDef("T002W25", "SELECT [Servico_Sigla] AS ContratoServicosTelas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosTelas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002W25,1,0,true,false )
             ,new CursorDef("T002W26", "SELECT [ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, [ContratoServicosTelas_Sequencial] FROM [ContratoServicosTelas] WITH (NOLOCK) ORDER BY [ContratoServicosTelas_ContratoCod], [ContratoServicosTelas_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002W26,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 100) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((int[]) buf[17])[0] = rslt.getInt(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                stmt.SetParameter(6, (String)parms[7]);
                stmt.SetParameter(7, (int)parms[8]);
                return;
             case 17 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                stmt.SetParameter(5, (String)parms[6]);
                stmt.SetParameter(6, (int)parms[7]);
                stmt.SetParameter(7, (short)parms[8]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
