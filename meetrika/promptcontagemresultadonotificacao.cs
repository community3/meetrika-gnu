/*
               File: PromptContagemResultadoNotificacao
        Description: Select Notifica��es da Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:8:31.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemresultadonotificacao : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemresultadonotificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemresultadonotificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref long aP0_InOutContagemResultadoNotificacao_Codigo ,
                           ref DateTime aP1_InOutContagemResultadoNotificacao_DataHora )
      {
         this.AV7InOutContagemResultadoNotificacao_Codigo = aP0_InOutContagemResultadoNotificacao_Codigo;
         this.AV8InOutContagemResultadoNotificacao_DataHora = aP1_InOutContagemResultadoNotificacao_DataHora;
         executePrivate();
         aP0_InOutContagemResultadoNotificacao_Codigo=this.AV7InOutContagemResultadoNotificacao_Codigo;
         aP1_InOutContagemResultadoNotificacao_DataHora=this.AV8InOutContagemResultadoNotificacao_DataHora;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbContagemResultadoNotificacao_Midia = new GXCombobox();
         cmbContagemResultadoNotificacao_Aut = new GXCombobox();
         cmbContagemResultadoNotificacao_Sec = new GXCombobox();
         cmbContagemResultadoNotificacao_Logged = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_104 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_104_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_104_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContagemResultadoNotificacao_DataHora1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
               AV18ContagemResultadoNotificacao_DataHora_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DataHora_To1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
               AV19ContagemResultadoNotificacao_UsuNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_UsuNom1", AV19ContagemResultadoNotificacao_UsuNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23ContagemResultadoNotificacao_DataHora2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
               AV24ContagemResultadoNotificacao_DataHora_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DataHora_To2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
               AV25ContagemResultadoNotificacao_UsuNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoNotificacao_UsuNom2", AV25ContagemResultadoNotificacao_UsuNom2);
               AV27DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
               AV29ContagemResultadoNotificacao_DataHora3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
               AV30ContagemResultadoNotificacao_DataHora_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultadoNotificacao_DataHora_To3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
               AV31ContagemResultadoNotificacao_UsuNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultadoNotificacao_UsuNom3", AV31ContagemResultadoNotificacao_UsuNom3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultadoNotificacao_Codigo), 10, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContagemResultadoNotificacao_DataHora = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV8InOutContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAQR2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               Gx_date = DateTimeUtil.Today( context);
               context.Gx_err = 0;
               WSQR2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEQR2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311983193");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemresultadonotificacao.aspx") + "?" + UrlEncode("" +AV7InOutContagemResultadoNotificacao_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV8InOutContagemResultadoNotificacao_DataHora))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM1", StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM2", StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM3", StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_104", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_104), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADONOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADONOTIFICACAO_DATAHORA", context.localUtil.TToC( AV8InOutContagemResultadoNotificacao_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormQR2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemResultadoNotificacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Select Notifica��es da Contagem" ;
      }

      protected void WBQR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_QR2( true) ;
         }
         else
         {
            wb_table1_2_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(125, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(126, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"");
         }
         wbLoad = true;
      }

      protected void STARTQR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Select Notifica��es da Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPQR0( ) ;
      }

      protected void WSQR2( )
      {
         STARTQR2( ) ;
         EVTQR2( ) ;
      }

      protected void EVTQR2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11QR2 */
                           E11QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12QR2 */
                           E12QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13QR2 */
                           E13QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14QR2 */
                           E14QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15QR2 */
                           E15QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16QR2 */
                           E16QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17QR2 */
                           E17QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18QR2 */
                           E18QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19QR2 */
                           E19QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20QR2 */
                           E20QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21QR2 */
                           E21QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22QR2 */
                           E22QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23QR2 */
                           E23QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24QR2 */
                           E24QR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_104_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
                           SubsflControlProps_1042( ) ;
                           GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Enabled_" + sGXsfl_104_idx;
                           Contagemresultadonotificacao_observacao_Enabled = StringUtil.StrToBool( cgiGet( GXCCtl));
                           context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagemresultadonotificacao_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Contagemresultadonotificacao_observacao_Enabled));
                           AV38Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV38Select)) ? AV41Select_GXI : context.convertURL( context.PathToRelativeUrl( AV38Select))));
                           A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Codigo_Internalname), ",", "."));
                           A1416ContagemResultadoNotificacao_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoNotificacao_DataHora_Internalname), 0);
                           n1416ContagemResultadoNotificacao_DataHora = false;
                           A1413ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuCod_Internalname), ",", "."));
                           A1427ContagemResultadoNotificacao_UsuPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuPesCod_Internalname), ",", "."));
                           n1427ContagemResultadoNotificacao_UsuPesCod = false;
                           A1422ContagemResultadoNotificacao_UsuNom = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_UsuNom_Internalname));
                           n1422ContagemResultadoNotificacao_UsuNom = false;
                           A1417ContagemResultadoNotificacao_Assunto = cgiGet( edtContagemResultadoNotificacao_Assunto_Internalname);
                           n1417ContagemResultadoNotificacao_Assunto = false;
                           A1418ContagemResultadoNotificacao_CorpoEmail = cgiGet( edtContagemResultadoNotificacao_CorpoEmail_Internalname);
                           n1418ContagemResultadoNotificacao_CorpoEmail = false;
                           cmbContagemResultadoNotificacao_Midia.Name = cmbContagemResultadoNotificacao_Midia_Internalname;
                           cmbContagemResultadoNotificacao_Midia.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Midia_Internalname);
                           A1420ContagemResultadoNotificacao_Midia = cgiGet( cmbContagemResultadoNotificacao_Midia_Internalname);
                           n1420ContagemResultadoNotificacao_Midia = false;
                           A1956ContagemResultadoNotificacao_Host = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_Host_Internalname));
                           n1956ContagemResultadoNotificacao_Host = false;
                           A1957ContagemResultadoNotificacao_User = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_User_Internalname));
                           n1957ContagemResultadoNotificacao_User = false;
                           A1958ContagemResultadoNotificacao_Port = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Port_Internalname), ",", "."));
                           n1958ContagemResultadoNotificacao_Port = false;
                           cmbContagemResultadoNotificacao_Aut.Name = cmbContagemResultadoNotificacao_Aut_Internalname;
                           cmbContagemResultadoNotificacao_Aut.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname);
                           A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname));
                           n1959ContagemResultadoNotificacao_Aut = false;
                           cmbContagemResultadoNotificacao_Sec.Name = cmbContagemResultadoNotificacao_Sec_Internalname;
                           cmbContagemResultadoNotificacao_Sec.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname);
                           A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname), "."));
                           n1960ContagemResultadoNotificacao_Sec = false;
                           cmbContagemResultadoNotificacao_Logged.Name = cmbContagemResultadoNotificacao_Logged_Internalname;
                           cmbContagemResultadoNotificacao_Logged.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname);
                           A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname), "."));
                           n1961ContagemResultadoNotificacao_Logged = false;
                           GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_" + sGXsfl_104_idx;
                           A1415ContagemResultadoNotificacao_Observacao = cgiGet( GXCCtl);
                           n1415ContagemResultadoNotificacao_Observacao = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25QR2 */
                                 E25QR2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E26QR2 */
                                 E26QR2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27QR2 */
                                 E27QR2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_datahora1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1"), 0) != AV17ContagemResultadoNotificacao_DataHora1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_datahora_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1"), 0) != AV18ContagemResultadoNotificacao_DataHora_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_usunom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM1"), AV19ContagemResultadoNotificacao_UsuNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_datahora2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2"), 0) != AV23ContagemResultadoNotificacao_DataHora2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_datahora_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2"), 0) != AV24ContagemResultadoNotificacao_DataHora_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_usunom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM2"), AV25ContagemResultadoNotificacao_UsuNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_datahora3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3"), 0) != AV29ContagemResultadoNotificacao_DataHora3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_datahora_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3"), 0) != AV30ContagemResultadoNotificacao_DataHora_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadonotificacao_usunom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM3"), AV31ContagemResultadoNotificacao_UsuNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E28QR2 */
                                       E28QR2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEQR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQR2( ) ;
            }
         }
      }

      protected void PAQR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADONOTIFICACAO_DATAHORA", "da Notifica��o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADONOTIFICACAO_USUNOM", "Remetente", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Per�odo", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADONOTIFICACAO_DATAHORA", "da Notifica��o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADONOTIFICACAO_USUNOM", "Remetente", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator2.addItem("3", "Per�odo", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADONOTIFICACAO_DATAHORA", "da Notifica��o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADONOTIFICACAO_USUNOM", "Remetente", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator3.addItem("3", "Per�odo", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_MIDIA_" + sGXsfl_104_idx;
            cmbContagemResultadoNotificacao_Midia.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Midia.WebTags = "";
            cmbContagemResultadoNotificacao_Midia.addItem("EML", "Email", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("SMS", "SMS", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("TEL", "Telefone", 0);
            if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
            {
               A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
               n1420ContagemResultadoNotificacao_Midia = false;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_AUT_" + sGXsfl_104_idx;
            cmbContagemResultadoNotificacao_Aut.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Aut.WebTags = "";
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
            {
               A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
               n1959ContagemResultadoNotificacao_Aut = false;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_SEC_" + sGXsfl_104_idx;
            cmbContagemResultadoNotificacao_Sec.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Sec.WebTags = "";
            cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
            cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
            if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
            {
               A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
               n1960ContagemResultadoNotificacao_Sec = false;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_LOGGED_" + sGXsfl_104_idx;
            cmbContagemResultadoNotificacao_Logged.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Logged.WebTags = "";
            cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
            if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
            {
               A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
               n1961ContagemResultadoNotificacao_Logged = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1042( ) ;
         while ( nGXsfl_104_idx <= nRC_GXsfl_104 )
         {
            sendrow_1042( ) ;
            nGXsfl_104_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_104_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_104_idx+1));
            sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
            SubsflControlProps_1042( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       DateTime AV17ContagemResultadoNotificacao_DataHora1 ,
                                       DateTime AV18ContagemResultadoNotificacao_DataHora_To1 ,
                                       String AV19ContagemResultadoNotificacao_UsuNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       DateTime AV23ContagemResultadoNotificacao_DataHora2 ,
                                       DateTime AV24ContagemResultadoNotificacao_DataHora_To2 ,
                                       String AV25ContagemResultadoNotificacao_UsuNom2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       short AV28DynamicFiltersOperator3 ,
                                       DateTime AV29ContagemResultadoNotificacao_DataHora3 ,
                                       DateTime AV30ContagemResultadoNotificacao_DataHora_To3 ,
                                       String AV31ContagemResultadoNotificacao_UsuNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFQR2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DATAHORA", GetSecureSignedToken( "", context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USUCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_USUCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO", GetSecureSignedToken( "", A1417ContagemResultadoNotificacao_Assunto));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO", A1417ContagemResultadoNotificacao_Assunto);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1418ContagemResultadoNotificacao_CorpoEmail, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL", A1418ContagemResultadoNotificacao_CorpoEmail);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_MIDIA", StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_HOST", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_HOST", StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USER", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_USER", StringUtil.RTrim( A1957ContagemResultadoNotificacao_User));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_PORT", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_PORT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( "", A1959ContagemResultadoNotificacao_Aut));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_AUT", StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_SEC", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADONOTIFICACAO_LOGGED", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFQR2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 104;
         /* Execute user event: E26QR2 */
         E26QR2 ();
         nGXsfl_104_idx = 1;
         sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
         SubsflControlProps_1042( ) ;
         nGXsfl_104_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1042( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17ContagemResultadoNotificacao_DataHora1 ,
                                                 AV18ContagemResultadoNotificacao_DataHora_To1 ,
                                                 AV19ContagemResultadoNotificacao_UsuNom1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23ContagemResultadoNotificacao_DataHora2 ,
                                                 AV24ContagemResultadoNotificacao_DataHora_To2 ,
                                                 AV25ContagemResultadoNotificacao_UsuNom2 ,
                                                 AV26DynamicFiltersEnabled3 ,
                                                 AV27DynamicFiltersSelector3 ,
                                                 AV28DynamicFiltersOperator3 ,
                                                 AV29ContagemResultadoNotificacao_DataHora3 ,
                                                 AV30ContagemResultadoNotificacao_DataHora_To3 ,
                                                 AV31ContagemResultadoNotificacao_UsuNom3 ,
                                                 A1416ContagemResultadoNotificacao_DataHora ,
                                                 A1422ContagemResultadoNotificacao_UsuNom ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV19ContagemResultadoNotificacao_UsuNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_UsuNom1", AV19ContagemResultadoNotificacao_UsuNom1);
            lV19ContagemResultadoNotificacao_UsuNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_UsuNom1", AV19ContagemResultadoNotificacao_UsuNom1);
            lV25ContagemResultadoNotificacao_UsuNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoNotificacao_UsuNom2", AV25ContagemResultadoNotificacao_UsuNom2);
            lV25ContagemResultadoNotificacao_UsuNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoNotificacao_UsuNom2", AV25ContagemResultadoNotificacao_UsuNom2);
            lV31ContagemResultadoNotificacao_UsuNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultadoNotificacao_UsuNom3", AV31ContagemResultadoNotificacao_UsuNom3);
            lV31ContagemResultadoNotificacao_UsuNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultadoNotificacao_UsuNom3", AV31ContagemResultadoNotificacao_UsuNom3);
            /* Using cursor H00QR2 */
            pr_default.execute(0, new Object[] {AV17ContagemResultadoNotificacao_DataHora1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV18ContagemResultadoNotificacao_DataHora_To1, lV19ContagemResultadoNotificacao_UsuNom1, lV19ContagemResultadoNotificacao_UsuNom1, AV23ContagemResultadoNotificacao_DataHora2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV24ContagemResultadoNotificacao_DataHora_To2, lV25ContagemResultadoNotificacao_UsuNom2, lV25ContagemResultadoNotificacao_UsuNom2, AV29ContagemResultadoNotificacao_DataHora3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV30ContagemResultadoNotificacao_DataHora_To3, lV31ContagemResultadoNotificacao_UsuNom3, lV31ContagemResultadoNotificacao_UsuNom3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_104_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1415ContagemResultadoNotificacao_Observacao = H00QR2_A1415ContagemResultadoNotificacao_Observacao[0];
               n1415ContagemResultadoNotificacao_Observacao = H00QR2_n1415ContagemResultadoNotificacao_Observacao[0];
               A1961ContagemResultadoNotificacao_Logged = H00QR2_A1961ContagemResultadoNotificacao_Logged[0];
               n1961ContagemResultadoNotificacao_Logged = H00QR2_n1961ContagemResultadoNotificacao_Logged[0];
               A1960ContagemResultadoNotificacao_Sec = H00QR2_A1960ContagemResultadoNotificacao_Sec[0];
               n1960ContagemResultadoNotificacao_Sec = H00QR2_n1960ContagemResultadoNotificacao_Sec[0];
               A1959ContagemResultadoNotificacao_Aut = H00QR2_A1959ContagemResultadoNotificacao_Aut[0];
               n1959ContagemResultadoNotificacao_Aut = H00QR2_n1959ContagemResultadoNotificacao_Aut[0];
               A1958ContagemResultadoNotificacao_Port = H00QR2_A1958ContagemResultadoNotificacao_Port[0];
               n1958ContagemResultadoNotificacao_Port = H00QR2_n1958ContagemResultadoNotificacao_Port[0];
               A1957ContagemResultadoNotificacao_User = H00QR2_A1957ContagemResultadoNotificacao_User[0];
               n1957ContagemResultadoNotificacao_User = H00QR2_n1957ContagemResultadoNotificacao_User[0];
               A1956ContagemResultadoNotificacao_Host = H00QR2_A1956ContagemResultadoNotificacao_Host[0];
               n1956ContagemResultadoNotificacao_Host = H00QR2_n1956ContagemResultadoNotificacao_Host[0];
               A1420ContagemResultadoNotificacao_Midia = H00QR2_A1420ContagemResultadoNotificacao_Midia[0];
               n1420ContagemResultadoNotificacao_Midia = H00QR2_n1420ContagemResultadoNotificacao_Midia[0];
               A1418ContagemResultadoNotificacao_CorpoEmail = H00QR2_A1418ContagemResultadoNotificacao_CorpoEmail[0];
               n1418ContagemResultadoNotificacao_CorpoEmail = H00QR2_n1418ContagemResultadoNotificacao_CorpoEmail[0];
               A1417ContagemResultadoNotificacao_Assunto = H00QR2_A1417ContagemResultadoNotificacao_Assunto[0];
               n1417ContagemResultadoNotificacao_Assunto = H00QR2_n1417ContagemResultadoNotificacao_Assunto[0];
               A1422ContagemResultadoNotificacao_UsuNom = H00QR2_A1422ContagemResultadoNotificacao_UsuNom[0];
               n1422ContagemResultadoNotificacao_UsuNom = H00QR2_n1422ContagemResultadoNotificacao_UsuNom[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = H00QR2_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = H00QR2_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1413ContagemResultadoNotificacao_UsuCod = H00QR2_A1413ContagemResultadoNotificacao_UsuCod[0];
               A1416ContagemResultadoNotificacao_DataHora = H00QR2_A1416ContagemResultadoNotificacao_DataHora[0];
               n1416ContagemResultadoNotificacao_DataHora = H00QR2_n1416ContagemResultadoNotificacao_DataHora[0];
               A1412ContagemResultadoNotificacao_Codigo = H00QR2_A1412ContagemResultadoNotificacao_Codigo[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = H00QR2_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = H00QR2_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1422ContagemResultadoNotificacao_UsuNom = H00QR2_A1422ContagemResultadoNotificacao_UsuNom[0];
               n1422ContagemResultadoNotificacao_UsuNom = H00QR2_n1422ContagemResultadoNotificacao_UsuNom[0];
               /* Execute user event: E27QR2 */
               E27QR2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 104;
            WBQR0( ) ;
         }
         nGXsfl_104_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17ContagemResultadoNotificacao_DataHora1 ,
                                              AV18ContagemResultadoNotificacao_DataHora_To1 ,
                                              AV19ContagemResultadoNotificacao_UsuNom1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23ContagemResultadoNotificacao_DataHora2 ,
                                              AV24ContagemResultadoNotificacao_DataHora_To2 ,
                                              AV25ContagemResultadoNotificacao_UsuNom2 ,
                                              AV26DynamicFiltersEnabled3 ,
                                              AV27DynamicFiltersSelector3 ,
                                              AV28DynamicFiltersOperator3 ,
                                              AV29ContagemResultadoNotificacao_DataHora3 ,
                                              AV30ContagemResultadoNotificacao_DataHora_To3 ,
                                              AV31ContagemResultadoNotificacao_UsuNom3 ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV19ContagemResultadoNotificacao_UsuNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_UsuNom1", AV19ContagemResultadoNotificacao_UsuNom1);
         lV19ContagemResultadoNotificacao_UsuNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_UsuNom1", AV19ContagemResultadoNotificacao_UsuNom1);
         lV25ContagemResultadoNotificacao_UsuNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoNotificacao_UsuNom2", AV25ContagemResultadoNotificacao_UsuNom2);
         lV25ContagemResultadoNotificacao_UsuNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoNotificacao_UsuNom2", AV25ContagemResultadoNotificacao_UsuNom2);
         lV31ContagemResultadoNotificacao_UsuNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultadoNotificacao_UsuNom3", AV31ContagemResultadoNotificacao_UsuNom3);
         lV31ContagemResultadoNotificacao_UsuNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultadoNotificacao_UsuNom3", AV31ContagemResultadoNotificacao_UsuNom3);
         /* Using cursor H00QR3 */
         pr_default.execute(1, new Object[] {AV17ContagemResultadoNotificacao_DataHora1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV18ContagemResultadoNotificacao_DataHora_To1, lV19ContagemResultadoNotificacao_UsuNom1, lV19ContagemResultadoNotificacao_UsuNom1, AV23ContagemResultadoNotificacao_DataHora2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV24ContagemResultadoNotificacao_DataHora_To2, lV25ContagemResultadoNotificacao_UsuNom2, lV25ContagemResultadoNotificacao_UsuNom2, AV29ContagemResultadoNotificacao_DataHora3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV30ContagemResultadoNotificacao_DataHora_To3, lV31ContagemResultadoNotificacao_UsuNom3, lV31ContagemResultadoNotificacao_UsuNom3});
         GRID_nRecordCount = H00QR3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return (int)(0) ;
      }

      protected void STRUPQR0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25QR2 */
         E25QR2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadonotificacao_datahora1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Notificacao_Data Hora1"}), 1, "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1");
               GX_FocusControl = edtavContagemresultadonotificacao_datahora1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContagemResultadoNotificacao_DataHora1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV17ContagemResultadoNotificacao_DataHora1 = context.localUtil.CToT( cgiGet( edtavContagemresultadonotificacao_datahora1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadonotificacao_datahora_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Notificacao_Data Hora_To1"}), 1, "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1");
               GX_FocusControl = edtavContagemresultadonotificacao_datahora_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContagemResultadoNotificacao_DataHora_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DataHora_To1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV18ContagemResultadoNotificacao_DataHora_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultadonotificacao_datahora_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DataHora_To1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            AV19ContagemResultadoNotificacao_UsuNom1 = StringUtil.Upper( cgiGet( edtavContagemresultadonotificacao_usunom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_UsuNom1", AV19ContagemResultadoNotificacao_UsuNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadonotificacao_datahora2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Notificacao_Data Hora2"}), 1, "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2");
               GX_FocusControl = edtavContagemresultadonotificacao_datahora2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23ContagemResultadoNotificacao_DataHora2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV23ContagemResultadoNotificacao_DataHora2 = context.localUtil.CToT( cgiGet( edtavContagemresultadonotificacao_datahora2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadonotificacao_datahora_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Notificacao_Data Hora_To2"}), 1, "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2");
               GX_FocusControl = edtavContagemresultadonotificacao_datahora_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContagemResultadoNotificacao_DataHora_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DataHora_To2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV24ContagemResultadoNotificacao_DataHora_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultadonotificacao_datahora_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DataHora_To2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            AV25ContagemResultadoNotificacao_UsuNom2 = StringUtil.Upper( cgiGet( edtavContagemresultadonotificacao_usunom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoNotificacao_UsuNom2", AV25ContagemResultadoNotificacao_UsuNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadonotificacao_datahora3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Notificacao_Data Hora3"}), 1, "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3");
               GX_FocusControl = edtavContagemresultadonotificacao_datahora3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29ContagemResultadoNotificacao_DataHora3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV29ContagemResultadoNotificacao_DataHora3 = context.localUtil.CToT( cgiGet( edtavContagemresultadonotificacao_datahora3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadonotificacao_datahora_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Notificacao_Data Hora_To3"}), 1, "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3");
               GX_FocusControl = edtavContagemresultadonotificacao_datahora_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30ContagemResultadoNotificacao_DataHora_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultadoNotificacao_DataHora_To3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV30ContagemResultadoNotificacao_DataHora_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultadonotificacao_datahora_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultadoNotificacao_DataHora_To3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            AV31ContagemResultadoNotificacao_UsuNom3 = StringUtil.Upper( cgiGet( edtavContagemresultadonotificacao_usunom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultadoNotificacao_UsuNom3", AV31ContagemResultadoNotificacao_UsuNom3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_104 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_104"), ",", "."));
            AV36GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV37GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1"), 0) != AV17ContagemResultadoNotificacao_DataHora1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1"), 0) != AV18ContagemResultadoNotificacao_DataHora_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM1"), AV19ContagemResultadoNotificacao_UsuNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2"), 0) != AV23ContagemResultadoNotificacao_DataHora2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2"), 0) != AV24ContagemResultadoNotificacao_DataHora_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM2"), AV25ContagemResultadoNotificacao_UsuNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3"), 0) != AV29ContagemResultadoNotificacao_DataHora3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3"), 0) != AV30ContagemResultadoNotificacao_DataHora_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADONOTIFICACAO_USUNOM3"), AV31ContagemResultadoNotificacao_UsuNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25QR2 */
         E25QR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25QR2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Select Notifica��es da Contagem";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "da Notifica��o", 0);
         cmbavOrderedby.addItem("2", "da Notifica��o", 0);
         cmbavOrderedby.addItem("3", "Codigo", 0);
         cmbavOrderedby.addItem("4", "codigo", 0);
         cmbavOrderedby.addItem("5", "Remetente", 0);
         cmbavOrderedby.addItem("6", "Assunto", 0);
         cmbavOrderedby.addItem("7", "do Email", 0);
         cmbavOrderedby.addItem("8", "da Notifica��o", 0);
         cmbavOrderedby.addItem("9", "Host", 0);
         cmbavOrderedby.addItem("10", "Usu�rio", 0);
         cmbavOrderedby.addItem("11", "Porta", 0);
         cmbavOrderedby.addItem("12", "Autentica��o", 0);
         cmbavOrderedby.addItem("13", "Seguran�a", 0);
         cmbavOrderedby.addItem("14", "Login", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E26QR2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Per�odo", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Passado", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Hoje", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "No futuro", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Per�odo", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV26DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Passado", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Hoje", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "No futuro", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Per�odo", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         edtContagemResultadoNotificacao_Codigo_Titleformat = 2;
         edtContagemResultadoNotificacao_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "da Notifica��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Codigo_Internalname, "Title", edtContagemResultadoNotificacao_Codigo_Title);
         edtContagemResultadoNotificacao_DataHora_Titleformat = 2;
         edtContagemResultadoNotificacao_DataHora_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "da Notifica��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_DataHora_Internalname, "Title", edtContagemResultadoNotificacao_DataHora_Title);
         edtContagemResultadoNotificacao_UsuCod_Titleformat = 2;
         edtContagemResultadoNotificacao_UsuCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Codigo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuCod_Internalname, "Title", edtContagemResultadoNotificacao_UsuCod_Title);
         edtContagemResultadoNotificacao_UsuPesCod_Titleformat = 2;
         edtContagemResultadoNotificacao_UsuPesCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "codigo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuPesCod_Internalname, "Title", edtContagemResultadoNotificacao_UsuPesCod_Title);
         edtContagemResultadoNotificacao_UsuNom_Titleformat = 2;
         edtContagemResultadoNotificacao_UsuNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Remetente", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_UsuNom_Internalname, "Title", edtContagemResultadoNotificacao_UsuNom_Title);
         edtContagemResultadoNotificacao_Assunto_Titleformat = 2;
         edtContagemResultadoNotificacao_Assunto_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Assunto", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Assunto_Internalname, "Title", edtContagemResultadoNotificacao_Assunto_Title);
         edtContagemResultadoNotificacao_CorpoEmail_Titleformat = 2;
         edtContagemResultadoNotificacao_CorpoEmail_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "do Email", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_CorpoEmail_Internalname, "Title", edtContagemResultadoNotificacao_CorpoEmail_Title);
         cmbContagemResultadoNotificacao_Midia_Titleformat = 2;
         cmbContagemResultadoNotificacao_Midia.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV13OrderedBy==8) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "da Notifica��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Midia_Internalname, "Title", cmbContagemResultadoNotificacao_Midia.Title.Text);
         edtContagemResultadoNotificacao_Host_Titleformat = 2;
         edtContagemResultadoNotificacao_Host_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV13OrderedBy==9) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Host", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Host_Internalname, "Title", edtContagemResultadoNotificacao_Host_Title);
         edtContagemResultadoNotificacao_User_Titleformat = 2;
         edtContagemResultadoNotificacao_User_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV13OrderedBy==10) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_User_Internalname, "Title", edtContagemResultadoNotificacao_User_Title);
         edtContagemResultadoNotificacao_Port_Titleformat = 2;
         edtContagemResultadoNotificacao_Port_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV13OrderedBy==11) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Porta", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNotificacao_Port_Internalname, "Title", edtContagemResultadoNotificacao_Port_Title);
         cmbContagemResultadoNotificacao_Aut_Titleformat = 2;
         cmbContagemResultadoNotificacao_Aut.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV13OrderedBy==12) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Autentica��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Aut_Internalname, "Title", cmbContagemResultadoNotificacao_Aut.Title.Text);
         cmbContagemResultadoNotificacao_Sec_Titleformat = 2;
         cmbContagemResultadoNotificacao_Sec.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV13OrderedBy==13) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Seguran�a", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Sec_Internalname, "Title", cmbContagemResultadoNotificacao_Sec.Title.Text);
         cmbContagemResultadoNotificacao_Logged_Titleformat = 2;
         cmbContagemResultadoNotificacao_Logged.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV13OrderedBy==14) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Login", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Logged_Internalname, "Title", cmbContagemResultadoNotificacao_Logged.Title.Text);
         AV36GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GridCurrentPage), 10, 0)));
         AV37GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37GridPageCount), 10, 0)));
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E11QR2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV35PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV35PageToGo) ;
         }
      }

      private void E27QR2( )
      {
         /* Grid_Load Routine */
         AV38Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV38Select);
         AV41Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 104;
         }
         sendrow_1042( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_104_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(104, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E28QR2 */
         E28QR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28QR2( )
      {
         /* Enter Routine */
         AV7InOutContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultadoNotificacao_Codigo), 10, 0)));
         AV8InOutContagemResultadoNotificacao_DataHora = A1416ContagemResultadoNotificacao_DataHora;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV8InOutContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
         context.setWebReturnParms(new Object[] {(long)AV7InOutContagemResultadoNotificacao_Codigo,context.localUtil.Format( AV8InOutContagemResultadoNotificacao_DataHora, "99/99/99 99:99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E12QR2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E17QR2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
      }

      protected void E13QR2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18QR2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19QR2( )
      {
         /* Dynamicfiltersoperator1_Click Routine */
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
         {
            AV17ContagemResultadoNotificacao_DataHora1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            AV18ContagemResultadoNotificacao_DataHora_To1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DataHora_To1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA1OPERATORVALUES' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
      }

      protected void E20QR2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
      }

      protected void E14QR2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21QR2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E22QR2( )
      {
         /* Dynamicfiltersoperator2_Click Routine */
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
         {
            AV23ContagemResultadoNotificacao_DataHora2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            AV24ContagemResultadoNotificacao_DataHora_To2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DataHora_To2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA2OPERATORVALUES' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
      }

      protected void E15QR2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23QR2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E24QR2( )
      {
         /* Dynamicfiltersoperator3_Click Routine */
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
         {
            AV29ContagemResultadoNotificacao_DataHora3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            AV30ContagemResultadoNotificacao_DataHora_To3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultadoNotificacao_DataHora_To3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA3OPERATORVALUES' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContagemResultadoNotificacao_DataHora1, AV18ContagemResultadoNotificacao_DataHora_To1, AV19ContagemResultadoNotificacao_UsuNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContagemResultadoNotificacao_DataHora2, AV24ContagemResultadoNotificacao_DataHora_To2, AV25ContagemResultadoNotificacao_UsuNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContagemResultadoNotificacao_DataHora3, AV30ContagemResultadoNotificacao_DataHora_To3, AV31ContagemResultadoNotificacao_UsuNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
      }

      protected void E16QR2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible), 5, 0)));
         edtavContagemresultadonotificacao_usunom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_usunom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_usunom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA1OPERATORVALUES' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
         {
            edtavContagemresultadonotificacao_usunom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_usunom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_usunom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S172( )
      {
         /* 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA1OPERATORVALUES' Routine */
         cellContagemresultadonotificacao_datahora_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_cell1_Internalname, "Class", cellContagemresultadonotificacao_datahora_cell1_Class);
         cellContagemresultadonotificacao_datahora_to_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_to_cell1_Internalname, "Class", cellContagemresultadonotificacao_datahora_to_cell1_Class);
         lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultadonotificacao_datahora_rangemiddletext_11_Internalname, "Class", lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class);
         if ( AV16DynamicFiltersOperator1 == 0 )
         {
            AV17ContagemResultadoNotificacao_DataHora1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV16DynamicFiltersOperator1 == 1 )
         {
            AV17ContagemResultadoNotificacao_DataHora1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            AV18ContagemResultadoNotificacao_DataHora_To1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DataHora_To1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV16DynamicFiltersOperator1 == 2 )
         {
            AV17ContagemResultadoNotificacao_DataHora1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV16DynamicFiltersOperator1 == 3 )
         {
            cellContagemresultadonotificacao_datahora_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_cell1_Internalname, "Class", cellContagemresultadonotificacao_datahora_cell1_Class);
            cellContagemresultadonotificacao_datahora_to_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_to_cell1_Internalname, "Class", cellContagemresultadonotificacao_datahora_to_cell1_Class);
            lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultadonotificacao_datahora_rangemiddletext_11_Internalname, "Class", lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible), 5, 0)));
         edtavContagemresultadonotificacao_usunom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_usunom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_usunom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA2OPERATORVALUES' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
         {
            edtavContagemresultadonotificacao_usunom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_usunom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_usunom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA2OPERATORVALUES' Routine */
         cellContagemresultadonotificacao_datahora_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_cell2_Internalname, "Class", cellContagemresultadonotificacao_datahora_cell2_Class);
         cellContagemresultadonotificacao_datahora_to_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_to_cell2_Internalname, "Class", cellContagemresultadonotificacao_datahora_to_cell2_Class);
         lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultadonotificacao_datahora_rangemiddletext_12_Internalname, "Class", lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class);
         if ( AV22DynamicFiltersOperator2 == 0 )
         {
            AV23ContagemResultadoNotificacao_DataHora2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV22DynamicFiltersOperator2 == 1 )
         {
            AV23ContagemResultadoNotificacao_DataHora2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            AV24ContagemResultadoNotificacao_DataHora_To2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DataHora_To2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV22DynamicFiltersOperator2 == 2 )
         {
            AV23ContagemResultadoNotificacao_DataHora2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV22DynamicFiltersOperator2 == 3 )
         {
            cellContagemresultadonotificacao_datahora_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_cell2_Internalname, "Class", cellContagemresultadonotificacao_datahora_cell2_Class);
            cellContagemresultadonotificacao_datahora_to_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_to_cell2_Internalname, "Class", cellContagemresultadonotificacao_datahora_to_cell2_Class);
            lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultadonotificacao_datahora_rangemiddletext_12_Internalname, "Class", lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class);
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible), 5, 0)));
         edtavContagemresultadonotificacao_usunom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_usunom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_usunom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA3OPERATORVALUES' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
         {
            edtavContagemresultadonotificacao_usunom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadonotificacao_usunom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadonotificacao_usunom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'UPDATECONTAGEMRESULTADONOTIFICACAO_DATAHORA3OPERATORVALUES' Routine */
         cellContagemresultadonotificacao_datahora_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_cell3_Internalname, "Class", cellContagemresultadonotificacao_datahora_cell3_Class);
         cellContagemresultadonotificacao_datahora_to_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_to_cell3_Internalname, "Class", cellContagemresultadonotificacao_datahora_to_cell3_Class);
         lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultadonotificacao_datahora_rangemiddletext_13_Internalname, "Class", lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class);
         if ( AV28DynamicFiltersOperator3 == 0 )
         {
            AV29ContagemResultadoNotificacao_DataHora3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV28DynamicFiltersOperator3 == 1 )
         {
            AV29ContagemResultadoNotificacao_DataHora3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            AV30ContagemResultadoNotificacao_DataHora_To3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultadoNotificacao_DataHora_To3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV28DynamicFiltersOperator3 == 2 )
         {
            AV29ContagemResultadoNotificacao_DataHora3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV28DynamicFiltersOperator3 == 3 )
         {
            cellContagemresultadonotificacao_datahora_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_cell3_Internalname, "Class", cellContagemresultadonotificacao_datahora_cell3_Class);
            cellContagemresultadonotificacao_datahora_to_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultadonotificacao_datahora_to_cell3_Internalname, "Class", cellContagemresultadonotificacao_datahora_to_cell3_Class);
            lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultadonotificacao_datahora_rangemiddletext_13_Internalname, "Class", lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class);
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23ContagemResultadoNotificacao_DataHora2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
         AV24ContagemResultadoNotificacao_DataHora_To2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DataHora_To2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         AV29ContagemResultadoNotificacao_DataHora3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
         AV30ContagemResultadoNotificacao_DataHora_To3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultadoNotificacao_DataHora_To3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ContagemResultadoNotificacao_DataHora1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
         AV18ContagemResultadoNotificacao_DataHora_To1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DataHora_To1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContagemResultadoNotificacao_DataHora1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoNotificacao_DataHora1", context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " "));
               AV18ContagemResultadoNotificacao_DataHora_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoNotificacao_DataHora_To1", context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19ContagemResultadoNotificacao_UsuNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoNotificacao_UsuNom1", AV19ContagemResultadoNotificacao_UsuNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23ContagemResultadoNotificacao_DataHora2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultadoNotificacao_DataHora2", context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " "));
                  AV24ContagemResultadoNotificacao_DataHora_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoNotificacao_DataHora_To2", context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25ContagemResultadoNotificacao_UsuNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoNotificacao_UsuNom2", AV25ContagemResultadoNotificacao_UsuNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV29ContagemResultadoNotificacao_DataHora3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoNotificacao_DataHora3", context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " "));
                     AV30ContagemResultadoNotificacao_DataHora_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultadoNotificacao_DataHora_To3", context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV31ContagemResultadoNotificacao_UsuNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultadoNotificacao_UsuNom3", AV31ContagemResultadoNotificacao_UsuNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ! ( (DateTime.MinValue==AV17ContagemResultadoNotificacao_DataHora1) && (DateTime.MinValue==AV18ContagemResultadoNotificacao_DataHora_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19ContagemResultadoNotificacao_UsuNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ! ( (DateTime.MinValue==AV23ContagemResultadoNotificacao_DataHora2) && (DateTime.MinValue==AV24ContagemResultadoNotificacao_DataHora_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContagemResultadoNotificacao_UsuNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ! ( (DateTime.MinValue==AV29ContagemResultadoNotificacao_DataHora3) && (DateTime.MinValue==AV30ContagemResultadoNotificacao_DataHora_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31ContagemResultadoNotificacao_UsuNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_QR2( true) ;
         }
         else
         {
            wb_table2_5_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_98_QR2( true) ;
         }
         else
         {
            wb_table3_98_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_98_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QR2e( true) ;
         }
         else
         {
            wb_table1_2_QR2e( false) ;
         }
      }

      protected void wb_table3_98_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_101_QR2( true) ;
         }
         else
         {
            wb_table4_101_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_101_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_98_QR2e( true) ;
         }
         else
         {
            wb_table3_98_QR2e( false) ;
         }
      }

      protected void wb_table4_101_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"104\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DataHora_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DataHora_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DataHora_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_UsuCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_UsuCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_UsuCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_UsuPesCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_UsuPesCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_UsuPesCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_UsuNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_UsuNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_UsuNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Assunto_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Assunto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Assunto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_CorpoEmail_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_CorpoEmail_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_CorpoEmail_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultadoNotificacao_Midia_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultadoNotificacao_Midia.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultadoNotificacao_Midia.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Host_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Host_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Host_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_User_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_User_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_User_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Port_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Port_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Port_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultadoNotificacao_Aut_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultadoNotificacao_Aut.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultadoNotificacao_Aut.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultadoNotificacao_Sec_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultadoNotificacao_Sec.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultadoNotificacao_Sec.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultadoNotificacao_Logged_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultadoNotificacao_Logged.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultadoNotificacao_Logged.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV38Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DataHora_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DataHora_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_UsuCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_UsuCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_UsuPesCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_UsuPesCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_UsuNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_UsuNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1417ContagemResultadoNotificacao_Assunto);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Assunto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Assunto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1418ContagemResultadoNotificacao_CorpoEmail);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_CorpoEmail_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_CorpoEmail_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultadoNotificacao_Midia.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultadoNotificacao_Midia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Host_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Host_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1957ContagemResultadoNotificacao_User));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_User_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_User_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Port_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Port_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultadoNotificacao_Aut.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultadoNotificacao_Aut_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultadoNotificacao_Sec.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultadoNotificacao_Sec_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultadoNotificacao_Logged.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultadoNotificacao_Logged_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 104 )
         {
            wbEnd = 0;
            nRC_GXsfl_104 = (short)(nGXsfl_104_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_101_QR2e( true) ;
         }
         else
         {
            wb_table4_101_QR2e( false) ;
         }
      }

      protected void wb_table2_5_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemResultadoNotificacao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_QR2( true) ;
         }
         else
         {
            wb_table5_14_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_QR2e( true) ;
         }
         else
         {
            wb_table2_5_QR2e( false) ;
         }
      }

      protected void wb_table5_14_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_QR2( true) ;
         }
         else
         {
            wb_table6_19_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_QR2e( true) ;
         }
         else
         {
            wb_table5_14_QR2e( false) ;
         }
      }

      protected void wb_table6_19_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemResultadoNotificacao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_QR2( true) ;
         }
         else
         {
            wb_table7_28_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptContagemResultadoNotificacao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_53_QR2( true) ;
         }
         else
         {
            wb_table8_53_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table8_53_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_PromptContagemResultadoNotificacao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_78_QR2( true) ;
         }
         else
         {
            wb_table9_78_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table9_78_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_QR2e( true) ;
         }
         else
         {
            wb_table6_19_QR2e( false) ;
         }
      }

      protected void wb_table9_78_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR3.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "", true, "HLP_PromptContagemResultadoNotificacao.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_83_QR2( true) ;
         }
         else
         {
            wb_table10_83_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table10_83_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_usunom3_Internalname, StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3), StringUtil.RTrim( context.localUtil.Format( AV31ContagemResultadoNotificacao_UsuNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_usunom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_usunom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_78_QR2e( true) ;
         }
         else
         {
            wb_table9_78_QR2e( false) ;
         }
      }

      protected void wb_table10_83_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Internalname, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_cell3_Internalname+"\"  class='"+cellContagemresultadonotificacao_datahora_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadonotificacao_datahora3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_datahora3_Internalname, context.localUtil.TToC( AV29ContagemResultadoNotificacao_DataHora3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV29ContagemResultadoNotificacao_DataHora3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_datahora3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadonotificacao_datahora3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell3_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadonotificacao_datahora_rangemiddletext_13_Internalname, "to", "", "", lblContagemresultadonotificacao_datahora_rangemiddletext_13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class, 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_to_cell3_Internalname+"\"  class='"+cellContagemresultadonotificacao_datahora_to_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadonotificacao_datahora_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_datahora_to3_Internalname, context.localUtil.TToC( AV30ContagemResultadoNotificacao_DataHora_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV30ContagemResultadoNotificacao_DataHora_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_datahora_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadonotificacao_datahora_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_83_QR2e( true) ;
         }
         else
         {
            wb_table10_83_QR2e( false) ;
         }
      }

      protected void wb_table8_53_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR2.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_PromptContagemResultadoNotificacao.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_58_QR2( true) ;
         }
         else
         {
            wb_table11_58_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table11_58_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_usunom2_Internalname, StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2), StringUtil.RTrim( context.localUtil.Format( AV25ContagemResultadoNotificacao_UsuNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_usunom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_usunom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_53_QR2e( true) ;
         }
         else
         {
            wb_table8_53_QR2e( false) ;
         }
      }

      protected void wb_table11_58_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Internalname, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_cell2_Internalname+"\"  class='"+cellContagemresultadonotificacao_datahora_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadonotificacao_datahora2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_datahora2_Internalname, context.localUtil.TToC( AV23ContagemResultadoNotificacao_DataHora2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV23ContagemResultadoNotificacao_DataHora2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_datahora2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadonotificacao_datahora2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell2_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadonotificacao_datahora_rangemiddletext_12_Internalname, "to", "", "", lblContagemresultadonotificacao_datahora_rangemiddletext_12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class, 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_to_cell2_Internalname+"\"  class='"+cellContagemresultadonotificacao_datahora_to_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadonotificacao_datahora_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_datahora_to2_Internalname, context.localUtil.TToC( AV24ContagemResultadoNotificacao_DataHora_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV24ContagemResultadoNotificacao_DataHora_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_datahora_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadonotificacao_datahora_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_58_QR2e( true) ;
         }
         else
         {
            wb_table11_58_QR2e( false) ;
         }
      }

      protected void wb_table7_28_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR1.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContagemResultadoNotificacao.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_33_QR2( true) ;
         }
         else
         {
            wb_table12_33_QR2( false) ;
         }
         return  ;
      }

      protected void wb_table12_33_QR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_usunom1_Internalname, StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1), StringUtil.RTrim( context.localUtil.Format( AV19ContagemResultadoNotificacao_UsuNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_usunom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadonotificacao_usunom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_QR2e( true) ;
         }
         else
         {
            wb_table7_28_QR2e( false) ;
         }
      }

      protected void wb_table12_33_QR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Internalname, tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_cell1_Internalname+"\"  class='"+cellContagemresultadonotificacao_datahora_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadonotificacao_datahora1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_datahora1_Internalname, context.localUtil.TToC( AV17ContagemResultadoNotificacao_DataHora1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV17ContagemResultadoNotificacao_DataHora1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_datahora1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadonotificacao_datahora1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell1_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadonotificacao_datahora_rangemiddletext_11_Internalname, "to", "", "", lblContagemresultadonotificacao_datahora_rangemiddletext_11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class, 0, "", 1, 1, 0, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadonotificacao_datahora_to_cell1_Internalname+"\"  class='"+cellContagemresultadonotificacao_datahora_to_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadonotificacao_datahora_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadonotificacao_datahora_to1_Internalname, context.localUtil.TToC( AV18ContagemResultadoNotificacao_DataHora_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV18ContagemResultadoNotificacao_DataHora_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadonotificacao_datahora_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadonotificacao_datahora_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_33_QR2e( true) ;
         }
         else
         {
            wb_table12_33_QR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContagemResultadoNotificacao_Codigo = Convert.ToInt64(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultadoNotificacao_Codigo), 10, 0)));
         AV8InOutContagemResultadoNotificacao_DataHora = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV8InOutContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQR2( ) ;
         WSQR2( ) ;
         WEQR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311983621");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemresultadonotificacao.js", "?2020311983622");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1042( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_Codigo_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CODIGO_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_DataHora_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_UsuCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUCOD_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_UsuPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUPESCOD_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_UsuNom_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUNOM_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_Assunto_Internalname = "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_CorpoEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL_"+sGXsfl_104_idx;
         cmbContagemResultadoNotificacao_Midia_Internalname = "CONTAGEMRESULTADONOTIFICACAO_MIDIA_"+sGXsfl_104_idx;
         Contagemresultadonotificacao_observacao_Internalname = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_Host_Internalname = "CONTAGEMRESULTADONOTIFICACAO_HOST_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_User_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USER_"+sGXsfl_104_idx;
         edtContagemResultadoNotificacao_Port_Internalname = "CONTAGEMRESULTADONOTIFICACAO_PORT_"+sGXsfl_104_idx;
         cmbContagemResultadoNotificacao_Aut_Internalname = "CONTAGEMRESULTADONOTIFICACAO_AUT_"+sGXsfl_104_idx;
         cmbContagemResultadoNotificacao_Sec_Internalname = "CONTAGEMRESULTADONOTIFICACAO_SEC_"+sGXsfl_104_idx;
         cmbContagemResultadoNotificacao_Logged_Internalname = "CONTAGEMRESULTADONOTIFICACAO_LOGGED_"+sGXsfl_104_idx;
      }

      protected void SubsflControlProps_fel_1042( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_Codigo_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CODIGO_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_DataHora_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_UsuCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUCOD_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_UsuPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUPESCOD_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_UsuNom_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUNOM_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_Assunto_Internalname = "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_CorpoEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL_"+sGXsfl_104_fel_idx;
         cmbContagemResultadoNotificacao_Midia_Internalname = "CONTAGEMRESULTADONOTIFICACAO_MIDIA_"+sGXsfl_104_fel_idx;
         Contagemresultadonotificacao_observacao_Internalname = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_Host_Internalname = "CONTAGEMRESULTADONOTIFICACAO_HOST_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_User_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USER_"+sGXsfl_104_fel_idx;
         edtContagemResultadoNotificacao_Port_Internalname = "CONTAGEMRESULTADONOTIFICACAO_PORT_"+sGXsfl_104_fel_idx;
         cmbContagemResultadoNotificacao_Aut_Internalname = "CONTAGEMRESULTADONOTIFICACAO_AUT_"+sGXsfl_104_fel_idx;
         cmbContagemResultadoNotificacao_Sec_Internalname = "CONTAGEMRESULTADONOTIFICACAO_SEC_"+sGXsfl_104_fel_idx;
         cmbContagemResultadoNotificacao_Logged_Internalname = "CONTAGEMRESULTADONOTIFICACAO_LOGGED_"+sGXsfl_104_fel_idx;
      }

      protected void sendrow_1042( )
      {
         SubsflControlProps_1042( ) ;
         WBQR0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_104_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_104_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_104_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 105,'',false,'',104)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV38Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV38Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV38Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV38Select)) ? AV41Select_GXI : context.PathToRelativeUrl( AV38Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_104_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV38Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")),context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo10",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DataHora_Internalname,context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DataHora_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_UsuCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_UsuCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_UsuPesCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1427ContagemResultadoNotificacao_UsuPesCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_UsuPesCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_UsuNom_Internalname,StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom),StringUtil.RTrim( context.localUtil.Format( A1422ContagemResultadoNotificacao_UsuNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_UsuNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Assunto_Internalname,(String)A1417ContagemResultadoNotificacao_Assunto,(String)A1417ContagemResultadoNotificacao_Assunto,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Assunto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1000,(short)0,(short)0,(short)104,(short)1,(short)0,(short)-1,(bool)true,(String)"Observacao",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_CorpoEmail_Internalname,(String)A1418ContagemResultadoNotificacao_CorpoEmail,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_CorpoEmail_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8000,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_104_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_MIDIA_" + sGXsfl_104_idx;
               cmbContagemResultadoNotificacao_Midia.Name = GXCCtl;
               cmbContagemResultadoNotificacao_Midia.WebTags = "";
               cmbContagemResultadoNotificacao_Midia.addItem("EML", "Email", 0);
               cmbContagemResultadoNotificacao_Midia.addItem("SMS", "SMS", 0);
               cmbContagemResultadoNotificacao_Midia.addItem("TEL", "Telefone", 0);
               if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
               {
                  A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
                  n1420ContagemResultadoNotificacao_Midia = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultadoNotificacao_Midia,(String)cmbContagemResultadoNotificacao_Midia_Internalname,StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia),(short)1,(String)cmbContagemResultadoNotificacao_Midia_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultadoNotificacao_Midia.CurrentValue = StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Midia_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Midia.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td>") ;
            }
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAOContainer"+"_"+sGXsfl_104_idx,(short)1});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Host_Internalname,StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host),StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Host_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_User_Internalname,StringUtil.RTrim( A1957ContagemResultadoNotificacao_User),StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_User_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Port_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Port_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_104_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_AUT_" + sGXsfl_104_idx;
               cmbContagemResultadoNotificacao_Aut.Name = GXCCtl;
               cmbContagemResultadoNotificacao_Aut.WebTags = "";
               cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
               {
                  A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
                  n1959ContagemResultadoNotificacao_Aut = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultadoNotificacao_Aut,(String)cmbContagemResultadoNotificacao_Aut_Internalname,StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut),(short)1,(String)cmbContagemResultadoNotificacao_Aut_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultadoNotificacao_Aut.CurrentValue = StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Aut_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Aut.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_104_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_SEC_" + sGXsfl_104_idx;
               cmbContagemResultadoNotificacao_Sec.Name = GXCCtl;
               cmbContagemResultadoNotificacao_Sec.WebTags = "";
               cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
               cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
               if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
               {
                  A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
                  n1960ContagemResultadoNotificacao_Sec = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultadoNotificacao_Sec,(String)cmbContagemResultadoNotificacao_Sec_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)),(short)1,(String)cmbContagemResultadoNotificacao_Sec_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultadoNotificacao_Sec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Sec_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Sec.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_104_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_LOGGED_" + sGXsfl_104_idx;
               cmbContagemResultadoNotificacao_Logged.Name = GXCCtl;
               cmbContagemResultadoNotificacao_Logged.WebTags = "";
               cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
               cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
               cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
               if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
               {
                  A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
                  n1961ContagemResultadoNotificacao_Logged = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultadoNotificacao_Logged,(String)cmbContagemResultadoNotificacao_Logged_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)),(short)1,(String)cmbContagemResultadoNotificacao_Logged_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultadoNotificacao_Logged.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNotificacao_Logged_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Logged.ToJavascriptSource()));
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_" + sGXsfl_104_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A1415ContagemResultadoNotificacao_Observacao);
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DATAHORA"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USUCOD"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, A1417ContagemResultadoNotificacao_Assunto));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, StringUtil.RTrim( context.localUtil.Format( A1418ContagemResultadoNotificacao_CorpoEmail, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_HOST"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USER"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_PORT"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, A1959ContagemResultadoNotificacao_Aut));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Enabled_" + sGXsfl_104_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.BoolToStr( Contagemresultadonotificacao_observacao_Enabled));
            GridContainer.AddRow(GridRow);
            nGXsfl_104_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_104_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_104_idx+1));
            sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
            SubsflControlProps_1042( ) ;
         }
         /* End function sendrow_1042 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagemresultadonotificacao_datahora1_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1";
         cellContagemresultadonotificacao_datahora_cell1_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL1";
         lblContagemresultadonotificacao_datahora_rangemiddletext_11_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_11";
         cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell1_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_1_CELL1";
         edtavContagemresultadonotificacao_datahora_to1_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1";
         cellContagemresultadonotificacao_datahora_to_cell1_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL1";
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA1";
         edtavContagemresultadonotificacao_usunom1_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_USUNOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagemresultadonotificacao_datahora2_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2";
         cellContagemresultadonotificacao_datahora_cell2_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL2";
         lblContagemresultadonotificacao_datahora_rangemiddletext_12_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_12";
         cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell2_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_1_CELL2";
         edtavContagemresultadonotificacao_datahora_to2_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2";
         cellContagemresultadonotificacao_datahora_to_cell2_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL2";
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA2";
         edtavContagemresultadonotificacao_usunom2_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_USUNOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagemresultadonotificacao_datahora3_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3";
         cellContagemresultadonotificacao_datahora_cell3_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL3";
         lblContagemresultadonotificacao_datahora_rangemiddletext_13_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_13";
         cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell3_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_1_CELL3";
         edtavContagemresultadonotificacao_datahora_to3_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3";
         cellContagemresultadonotificacao_datahora_to_cell3_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL3";
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA3";
         edtavContagemresultadonotificacao_usunom3_Internalname = "vCONTAGEMRESULTADONOTIFICACAO_USUNOM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagemResultadoNotificacao_Codigo_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CODIGO";
         edtContagemResultadoNotificacao_DataHora_Internalname = "CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         edtContagemResultadoNotificacao_UsuCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUCOD";
         edtContagemResultadoNotificacao_UsuPesCod_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUPESCOD";
         edtContagemResultadoNotificacao_UsuNom_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USUNOM";
         edtContagemResultadoNotificacao_Assunto_Internalname = "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         edtContagemResultadoNotificacao_CorpoEmail_Internalname = "CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL";
         cmbContagemResultadoNotificacao_Midia_Internalname = "CONTAGEMRESULTADONOTIFICACAO_MIDIA";
         Contagemresultadonotificacao_observacao_Internalname = "CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO";
         edtContagemResultadoNotificacao_Host_Internalname = "CONTAGEMRESULTADONOTIFICACAO_HOST";
         edtContagemResultadoNotificacao_User_Internalname = "CONTAGEMRESULTADONOTIFICACAO_USER";
         edtContagemResultadoNotificacao_Port_Internalname = "CONTAGEMRESULTADONOTIFICACAO_PORT";
         cmbContagemResultadoNotificacao_Aut_Internalname = "CONTAGEMRESULTADONOTIFICACAO_AUT";
         cmbContagemResultadoNotificacao_Sec_Internalname = "CONTAGEMRESULTADONOTIFICACAO_SEC";
         cmbContagemResultadoNotificacao_Logged_Internalname = "CONTAGEMRESULTADONOTIFICACAO_LOGGED";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContagemResultadoNotificacao_Logged_Jsonclick = "";
         cmbContagemResultadoNotificacao_Sec_Jsonclick = "";
         cmbContagemResultadoNotificacao_Aut_Jsonclick = "";
         edtContagemResultadoNotificacao_Port_Jsonclick = "";
         edtContagemResultadoNotificacao_User_Jsonclick = "";
         edtContagemResultadoNotificacao_Host_Jsonclick = "";
         Contagemresultadonotificacao_observacao_Enabled = Convert.ToBoolean( 0);
         cmbContagemResultadoNotificacao_Midia_Jsonclick = "";
         edtContagemResultadoNotificacao_CorpoEmail_Jsonclick = "";
         edtContagemResultadoNotificacao_Assunto_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuNom_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuPesCod_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuCod_Jsonclick = "";
         edtContagemResultadoNotificacao_DataHora_Jsonclick = "";
         edtContagemResultadoNotificacao_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagemresultadonotificacao_datahora_to1_Jsonclick = "";
         cellContagemresultadonotificacao_datahora_to_cell1_Class = "";
         edtavContagemresultadonotificacao_datahora1_Jsonclick = "";
         cellContagemresultadonotificacao_datahora_cell1_Class = "";
         edtavContagemresultadonotificacao_usunom1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemresultadonotificacao_datahora_to2_Jsonclick = "";
         cellContagemresultadonotificacao_datahora_to_cell2_Class = "";
         edtavContagemresultadonotificacao_datahora2_Jsonclick = "";
         cellContagemresultadonotificacao_datahora_cell2_Class = "";
         edtavContagemresultadonotificacao_usunom2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemresultadonotificacao_datahora_to3_Jsonclick = "";
         cellContagemresultadonotificacao_datahora_to_cell3_Class = "";
         edtavContagemresultadonotificacao_datahora3_Jsonclick = "";
         cellContagemresultadonotificacao_datahora_cell3_Class = "";
         edtavContagemresultadonotificacao_usunom3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbContagemResultadoNotificacao_Logged_Titleformat = 0;
         cmbContagemResultadoNotificacao_Sec_Titleformat = 0;
         cmbContagemResultadoNotificacao_Aut_Titleformat = 0;
         edtContagemResultadoNotificacao_Port_Titleformat = 0;
         edtContagemResultadoNotificacao_User_Titleformat = 0;
         edtContagemResultadoNotificacao_Host_Titleformat = 0;
         cmbContagemResultadoNotificacao_Midia_Titleformat = 0;
         edtContagemResultadoNotificacao_CorpoEmail_Titleformat = 0;
         edtContagemResultadoNotificacao_Assunto_Titleformat = 0;
         edtContagemResultadoNotificacao_UsuNom_Titleformat = 0;
         edtContagemResultadoNotificacao_UsuPesCod_Titleformat = 0;
         edtContagemResultadoNotificacao_UsuCod_Titleformat = 0;
         edtContagemResultadoNotificacao_DataHora_Titleformat = 0;
         edtContagemResultadoNotificacao_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemresultadonotificacao_usunom3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible = 1;
         lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemresultadonotificacao_usunom2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible = 1;
         lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemresultadonotificacao_usunom1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible = 1;
         cmbContagemResultadoNotificacao_Logged.Title.Text = "Login";
         cmbContagemResultadoNotificacao_Sec.Title.Text = "Seguran�a";
         cmbContagemResultadoNotificacao_Aut.Title.Text = "Autentica��o";
         edtContagemResultadoNotificacao_Port_Title = "Porta";
         edtContagemResultadoNotificacao_User_Title = "Usu�rio";
         edtContagemResultadoNotificacao_Host_Title = "Host";
         cmbContagemResultadoNotificacao_Midia.Title.Text = "da Notifica��o";
         edtContagemResultadoNotificacao_CorpoEmail_Title = "do Email";
         edtContagemResultadoNotificacao_Assunto_Title = "Assunto";
         edtContagemResultadoNotificacao_UsuNom_Title = "Remetente";
         edtContagemResultadoNotificacao_UsuPesCod_Title = "codigo";
         edtContagemResultadoNotificacao_UsuCod_Title = "Codigo";
         edtContagemResultadoNotificacao_DataHora_Title = "da Notifica��o";
         edtContagemResultadoNotificacao_Codigo_Title = "da Notifica��o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Select Notifica��es da Contagem";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContagemResultadoNotificacao_Codigo_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Codigo_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',prop:'Title'},{av:'edtContagemResultadoNotificacao_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DataHora_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'Title'},{av:'edtContagemResultadoNotificacao_UsuCod_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_UsuCod_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',prop:'Title'},{av:'edtContagemResultadoNotificacao_UsuPesCod_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUPESCOD',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_UsuPesCod_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUPESCOD',prop:'Title'},{av:'edtContagemResultadoNotificacao_UsuNom_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_UsuNom_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'Title'},{av:'edtContagemResultadoNotificacao_Assunto_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Assunto_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'Title'},{av:'edtContagemResultadoNotificacao_CorpoEmail_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_CorpoEmail_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL',prop:'Title'},{av:'cmbContagemResultadoNotificacao_Midia'},{av:'edtContagemResultadoNotificacao_Host_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Host_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'Title'},{av:'edtContagemResultadoNotificacao_User_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USER',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_User_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USER',prop:'Title'},{av:'edtContagemResultadoNotificacao_Port_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Port_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'Title'},{av:'cmbContagemResultadoNotificacao_Aut'},{av:'cmbContagemResultadoNotificacao_Sec'},{av:'cmbContagemResultadoNotificacao_Logged'},{av:'AV36GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV37GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E27QR2',iparms:[],oparms:[{av:'AV38Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E28QR2',iparms:[{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A1416ContagemResultadoNotificacao_DataHora',fld:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''}],oparms:[{av:'AV7InOutContagemResultadoNotificacao_Codigo',fld:'vINOUTCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV8InOutContagemResultadoNotificacao_DataHora',fld:'vINOUTCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadonotificacao_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18QR2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadonotificacao_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR1.CLICK","{handler:'E19QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultadonotificacao_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadonotificacao_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21QR2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cellContagemresultadonotificacao_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR2.CLICK","{handler:'E22QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultadonotificacao_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadonotificacao_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E23QR2',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'cellContagemresultadonotificacao_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR3.CLICK","{handler:'E24QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultadonotificacao_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16QR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContagemResultadoNotificacao_DataHora1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoNotificacao_DataHora_To1',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom1_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContagemResultadoNotificacao_DataHora2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV24ContagemResultadoNotificacao_DataHora_To2',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultadoNotificacao_DataHora3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultadoNotificacao_DataHora_To3',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19ContagemResultadoNotificacao_UsuNom1',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25ContagemResultadoNotificacao_UsuNom2',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',pic:'@!',nv:''},{av:'AV31ContagemResultadoNotificacao_UsuNom3',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',pic:'@!',nv:''},{av:'cellContagemresultadonotificacao_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom2_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADONOTIFICACAO_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadonotificacao_usunom3_Visible',ctrl:'vCONTAGEMRESULTADONOTIFICACAO_USUNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'cellContagemresultadonotificacao_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadonotificacao_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContagemResultadoNotificacao_DataHora1 = (DateTime)(DateTime.MinValue);
         AV18ContagemResultadoNotificacao_DataHora_To1 = (DateTime)(DateTime.MinValue);
         AV19ContagemResultadoNotificacao_UsuNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23ContagemResultadoNotificacao_DataHora2 = (DateTime)(DateTime.MinValue);
         AV24ContagemResultadoNotificacao_DataHora_To2 = (DateTime)(DateTime.MinValue);
         AV25ContagemResultadoNotificacao_UsuNom2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV29ContagemResultadoNotificacao_DataHora3 = (DateTime)(DateTime.MinValue);
         AV30ContagemResultadoNotificacao_DataHora_To3 = (DateTime)(DateTime.MinValue);
         AV31ContagemResultadoNotificacao_UsuNom3 = "";
         GXKey = "";
         Gx_date = DateTime.MinValue;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV38Select = "";
         AV41Select_GXI = "";
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         A1422ContagemResultadoNotificacao_UsuNom = "";
         A1417ContagemResultadoNotificacao_Assunto = "";
         A1418ContagemResultadoNotificacao_CorpoEmail = "";
         A1420ContagemResultadoNotificacao_Midia = "";
         A1956ContagemResultadoNotificacao_Host = "";
         A1957ContagemResultadoNotificacao_User = "";
         A1415ContagemResultadoNotificacao_Observacao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19ContagemResultadoNotificacao_UsuNom1 = "";
         lV25ContagemResultadoNotificacao_UsuNom2 = "";
         lV31ContagemResultadoNotificacao_UsuNom3 = "";
         H00QR2_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         H00QR2_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         H00QR2_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         H00QR2_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         H00QR2_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         H00QR2_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         H00QR2_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         H00QR2_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         H00QR2_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         H00QR2_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         H00QR2_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         H00QR2_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         H00QR2_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         H00QR2_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         H00QR2_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         H00QR2_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         H00QR2_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         H00QR2_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         H00QR2_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         H00QR2_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         H00QR2_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         H00QR2_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         H00QR2_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         H00QR2_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         H00QR2_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         H00QR2_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00QR2_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         H00QR2_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         H00QR3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblContagemresultadonotificacao_datahora_rangemiddletext_13_Jsonclick = "";
         lblContagemresultadonotificacao_datahora_rangemiddletext_12_Jsonclick = "";
         lblContagemresultadonotificacao_datahora_rangemiddletext_11_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemresultadonotificacao__default(),
            new Object[][] {
                new Object[] {
               H00QR2_A1415ContagemResultadoNotificacao_Observacao, H00QR2_n1415ContagemResultadoNotificacao_Observacao, H00QR2_A1961ContagemResultadoNotificacao_Logged, H00QR2_n1961ContagemResultadoNotificacao_Logged, H00QR2_A1960ContagemResultadoNotificacao_Sec, H00QR2_n1960ContagemResultadoNotificacao_Sec, H00QR2_A1959ContagemResultadoNotificacao_Aut, H00QR2_n1959ContagemResultadoNotificacao_Aut, H00QR2_A1958ContagemResultadoNotificacao_Port, H00QR2_n1958ContagemResultadoNotificacao_Port,
               H00QR2_A1957ContagemResultadoNotificacao_User, H00QR2_n1957ContagemResultadoNotificacao_User, H00QR2_A1956ContagemResultadoNotificacao_Host, H00QR2_n1956ContagemResultadoNotificacao_Host, H00QR2_A1420ContagemResultadoNotificacao_Midia, H00QR2_n1420ContagemResultadoNotificacao_Midia, H00QR2_A1418ContagemResultadoNotificacao_CorpoEmail, H00QR2_n1418ContagemResultadoNotificacao_CorpoEmail, H00QR2_A1417ContagemResultadoNotificacao_Assunto, H00QR2_n1417ContagemResultadoNotificacao_Assunto,
               H00QR2_A1422ContagemResultadoNotificacao_UsuNom, H00QR2_n1422ContagemResultadoNotificacao_UsuNom, H00QR2_A1427ContagemResultadoNotificacao_UsuPesCod, H00QR2_n1427ContagemResultadoNotificacao_UsuPesCod, H00QR2_A1413ContagemResultadoNotificacao_UsuCod, H00QR2_A1416ContagemResultadoNotificacao_DataHora, H00QR2_n1416ContagemResultadoNotificacao_DataHora, H00QR2_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               H00QR3_AGRID_nRecordCount
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_104 ;
      private short nGXsfl_104_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV28DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1958ContagemResultadoNotificacao_Port ;
      private short A1960ContagemResultadoNotificacao_Sec ;
      private short A1961ContagemResultadoNotificacao_Logged ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_104_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoNotificacao_Codigo_Titleformat ;
      private short edtContagemResultadoNotificacao_DataHora_Titleformat ;
      private short edtContagemResultadoNotificacao_UsuCod_Titleformat ;
      private short edtContagemResultadoNotificacao_UsuPesCod_Titleformat ;
      private short edtContagemResultadoNotificacao_UsuNom_Titleformat ;
      private short edtContagemResultadoNotificacao_Assunto_Titleformat ;
      private short edtContagemResultadoNotificacao_CorpoEmail_Titleformat ;
      private short cmbContagemResultadoNotificacao_Midia_Titleformat ;
      private short edtContagemResultadoNotificacao_Host_Titleformat ;
      private short edtContagemResultadoNotificacao_User_Titleformat ;
      private short edtContagemResultadoNotificacao_Port_Titleformat ;
      private short cmbContagemResultadoNotificacao_Aut_Titleformat ;
      private short cmbContagemResultadoNotificacao_Sec_Titleformat ;
      private short cmbContagemResultadoNotificacao_Logged_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A1413ContagemResultadoNotificacao_UsuCod ;
      private int A1427ContagemResultadoNotificacao_UsuPesCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV35PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Visible ;
      private int edtavContagemresultadonotificacao_usunom1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Visible ;
      private int edtavContagemresultadonotificacao_usunom2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Visible ;
      private int edtavContagemresultadonotificacao_usunom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long AV7InOutContagemResultadoNotificacao_Codigo ;
      private long wcpOAV7InOutContagemResultadoNotificacao_Codigo ;
      private long GRID_nFirstRecordOnPage ;
      private long AV36GridCurrentPage ;
      private long AV37GridPageCount ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_104_idx="0001" ;
      private String AV19ContagemResultadoNotificacao_UsuNom1 ;
      private String AV25ContagemResultadoNotificacao_UsuNom2 ;
      private String AV31ContagemResultadoNotificacao_UsuNom3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Contagemresultadonotificacao_observacao_Internalname ;
      private String edtavSelect_Internalname ;
      private String edtContagemResultadoNotificacao_Codigo_Internalname ;
      private String edtContagemResultadoNotificacao_DataHora_Internalname ;
      private String edtContagemResultadoNotificacao_UsuCod_Internalname ;
      private String edtContagemResultadoNotificacao_UsuPesCod_Internalname ;
      private String A1422ContagemResultadoNotificacao_UsuNom ;
      private String edtContagemResultadoNotificacao_UsuNom_Internalname ;
      private String edtContagemResultadoNotificacao_Assunto_Internalname ;
      private String edtContagemResultadoNotificacao_CorpoEmail_Internalname ;
      private String cmbContagemResultadoNotificacao_Midia_Internalname ;
      private String A1420ContagemResultadoNotificacao_Midia ;
      private String A1956ContagemResultadoNotificacao_Host ;
      private String edtContagemResultadoNotificacao_Host_Internalname ;
      private String A1957ContagemResultadoNotificacao_User ;
      private String edtContagemResultadoNotificacao_User_Internalname ;
      private String edtContagemResultadoNotificacao_Port_Internalname ;
      private String cmbContagemResultadoNotificacao_Aut_Internalname ;
      private String cmbContagemResultadoNotificacao_Sec_Internalname ;
      private String cmbContagemResultadoNotificacao_Logged_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV19ContagemResultadoNotificacao_UsuNom1 ;
      private String lV25ContagemResultadoNotificacao_UsuNom2 ;
      private String lV31ContagemResultadoNotificacao_UsuNom3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemresultadonotificacao_datahora1_Internalname ;
      private String edtavContagemresultadonotificacao_datahora_to1_Internalname ;
      private String edtavContagemresultadonotificacao_usunom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemresultadonotificacao_datahora2_Internalname ;
      private String edtavContagemresultadonotificacao_datahora_to2_Internalname ;
      private String edtavContagemresultadonotificacao_usunom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemresultadonotificacao_datahora3_Internalname ;
      private String edtavContagemresultadonotificacao_datahora_to3_Internalname ;
      private String edtavContagemresultadonotificacao_usunom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemResultadoNotificacao_Codigo_Title ;
      private String edtContagemResultadoNotificacao_DataHora_Title ;
      private String edtContagemResultadoNotificacao_UsuCod_Title ;
      private String edtContagemResultadoNotificacao_UsuPesCod_Title ;
      private String edtContagemResultadoNotificacao_UsuNom_Title ;
      private String edtContagemResultadoNotificacao_Assunto_Title ;
      private String edtContagemResultadoNotificacao_CorpoEmail_Title ;
      private String edtContagemResultadoNotificacao_Host_Title ;
      private String edtContagemResultadoNotificacao_User_Title ;
      private String edtContagemResultadoNotificacao_Port_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora1_Internalname ;
      private String cellContagemresultadonotificacao_datahora_cell1_Class ;
      private String cellContagemresultadonotificacao_datahora_cell1_Internalname ;
      private String cellContagemresultadonotificacao_datahora_to_cell1_Class ;
      private String cellContagemresultadonotificacao_datahora_to_cell1_Internalname ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_11_Class ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_11_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora2_Internalname ;
      private String cellContagemresultadonotificacao_datahora_cell2_Class ;
      private String cellContagemresultadonotificacao_datahora_cell2_Internalname ;
      private String cellContagemresultadonotificacao_datahora_to_cell2_Class ;
      private String cellContagemresultadonotificacao_datahora_to_cell2_Internalname ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_12_Class ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_12_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadonotificacao_datahora3_Internalname ;
      private String cellContagemresultadonotificacao_datahora_cell3_Class ;
      private String cellContagemresultadonotificacao_datahora_cell3_Internalname ;
      private String cellContagemresultadonotificacao_datahora_to_cell3_Class ;
      private String cellContagemresultadonotificacao_datahora_to_cell3_Internalname ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_13_Class ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_13_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultadonotificacao_usunom3_Jsonclick ;
      private String edtavContagemresultadonotificacao_datahora3_Jsonclick ;
      private String cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell3_Internalname ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_13_Jsonclick ;
      private String edtavContagemresultadonotificacao_datahora_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultadonotificacao_usunom2_Jsonclick ;
      private String edtavContagemresultadonotificacao_datahora2_Jsonclick ;
      private String cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell2_Internalname ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_12_Jsonclick ;
      private String edtavContagemresultadonotificacao_datahora_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultadonotificacao_usunom1_Jsonclick ;
      private String edtavContagemresultadonotificacao_datahora1_Jsonclick ;
      private String cellContagemresultadonotificacao_datahora_rangemiddletext_1_cell1_Internalname ;
      private String lblContagemresultadonotificacao_datahora_rangemiddletext_11_Jsonclick ;
      private String edtavContagemresultadonotificacao_datahora_to1_Jsonclick ;
      private String sGXsfl_104_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultadoNotificacao_Codigo_Jsonclick ;
      private String edtContagemResultadoNotificacao_DataHora_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuPesCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuNom_Jsonclick ;
      private String edtContagemResultadoNotificacao_Assunto_Jsonclick ;
      private String edtContagemResultadoNotificacao_CorpoEmail_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Midia_Jsonclick ;
      private String edtContagemResultadoNotificacao_Host_Jsonclick ;
      private String edtContagemResultadoNotificacao_User_Jsonclick ;
      private String edtContagemResultadoNotificacao_Port_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Aut_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Sec_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Logged_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutContagemResultadoNotificacao_DataHora ;
      private DateTime wcpOAV8InOutContagemResultadoNotificacao_DataHora ;
      private DateTime AV17ContagemResultadoNotificacao_DataHora1 ;
      private DateTime AV18ContagemResultadoNotificacao_DataHora_To1 ;
      private DateTime AV23ContagemResultadoNotificacao_DataHora2 ;
      private DateTime AV24ContagemResultadoNotificacao_DataHora_To2 ;
      private DateTime AV29ContagemResultadoNotificacao_DataHora3 ;
      private DateTime AV30ContagemResultadoNotificacao_DataHora_To3 ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool Contagemresultadonotificacao_observacao_Enabled ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool n1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool n1422ContagemResultadoNotificacao_UsuNom ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool n1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool n1420ContagemResultadoNotificacao_Midia ;
      private bool n1956ContagemResultadoNotificacao_Host ;
      private bool n1957ContagemResultadoNotificacao_User ;
      private bool n1958ContagemResultadoNotificacao_Port ;
      private bool A1959ContagemResultadoNotificacao_Aut ;
      private bool n1959ContagemResultadoNotificacao_Aut ;
      private bool n1960ContagemResultadoNotificacao_Sec ;
      private bool n1961ContagemResultadoNotificacao_Logged ;
      private bool n1415ContagemResultadoNotificacao_Observacao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV38Select_IsBlob ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String A1415ContagemResultadoNotificacao_Observacao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV41Select_GXI ;
      private String A1418ContagemResultadoNotificacao_CorpoEmail ;
      private String AV38Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private long aP0_InOutContagemResultadoNotificacao_Codigo ;
      private DateTime aP1_InOutContagemResultadoNotificacao_DataHora ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbContagemResultadoNotificacao_Midia ;
      private GXCombobox cmbContagemResultadoNotificacao_Aut ;
      private GXCombobox cmbContagemResultadoNotificacao_Sec ;
      private GXCombobox cmbContagemResultadoNotificacao_Logged ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00QR2_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] H00QR2_n1415ContagemResultadoNotificacao_Observacao ;
      private short[] H00QR2_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] H00QR2_n1961ContagemResultadoNotificacao_Logged ;
      private short[] H00QR2_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] H00QR2_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] H00QR2_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] H00QR2_n1959ContagemResultadoNotificacao_Aut ;
      private short[] H00QR2_A1958ContagemResultadoNotificacao_Port ;
      private bool[] H00QR2_n1958ContagemResultadoNotificacao_Port ;
      private String[] H00QR2_A1957ContagemResultadoNotificacao_User ;
      private bool[] H00QR2_n1957ContagemResultadoNotificacao_User ;
      private String[] H00QR2_A1956ContagemResultadoNotificacao_Host ;
      private bool[] H00QR2_n1956ContagemResultadoNotificacao_Host ;
      private String[] H00QR2_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] H00QR2_n1420ContagemResultadoNotificacao_Midia ;
      private String[] H00QR2_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] H00QR2_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] H00QR2_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] H00QR2_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] H00QR2_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] H00QR2_n1422ContagemResultadoNotificacao_UsuNom ;
      private int[] H00QR2_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] H00QR2_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] H00QR2_A1413ContagemResultadoNotificacao_UsuCod ;
      private DateTime[] H00QR2_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] H00QR2_n1416ContagemResultadoNotificacao_DataHora ;
      private long[] H00QR2_A1412ContagemResultadoNotificacao_Codigo ;
      private long[] H00QR3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
   }

   public class promptcontagemresultadonotificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00QR2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             DateTime AV17ContagemResultadoNotificacao_DataHora1 ,
                                             DateTime AV18ContagemResultadoNotificacao_DataHora_To1 ,
                                             String AV19ContagemResultadoNotificacao_UsuNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             DateTime AV23ContagemResultadoNotificacao_DataHora2 ,
                                             DateTime AV24ContagemResultadoNotificacao_DataHora_To2 ,
                                             String AV25ContagemResultadoNotificacao_UsuNom2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             DateTime AV29ContagemResultadoNotificacao_DataHora3 ,
                                             DateTime AV30ContagemResultadoNotificacao_DataHora_To3 ,
                                             String AV31ContagemResultadoNotificacao_UsuNom3 ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [23] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemResultadoNotificacao_Observacao], T1.[ContagemResultadoNotificacao_Logged], T1.[ContagemResultadoNotificacao_Sec], T1.[ContagemResultadoNotificacao_Aut], T1.[ContagemResultadoNotificacao_Port], T1.[ContagemResultadoNotificacao_User], T1.[ContagemResultadoNotificacao_Host], T1.[ContagemResultadoNotificacao_Midia], T1.[ContagemResultadoNotificacao_CorpoEmail], T1.[ContagemResultadoNotificacao_Assunto], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T1.[ContagemResultadoNotificacao_DataHora], T1.[ContagemResultadoNotificacao_Codigo]";
         sFromString = " FROM (([ContagemResultadoNotificacao] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV17ContagemResultadoNotificacao_DataHora1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV17ContagemResultadoNotificacao_DataHora1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV17ContagemResultadoNotificacao_DataHora1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 2 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV17ContagemResultadoNotificacao_DataHora1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] >= @AV17ContagemResultadoNotificacao_DataHora1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] >= @AV17ContagemResultadoNotificacao_DataHora1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (DateTime.MinValue==AV18ContagemResultadoNotificacao_DataHora_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV16DynamicFiltersOperator1 == 3 ) && ( ! (DateTime.MinValue==AV18ContagemResultadoNotificacao_DataHora_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] <= @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] <= @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV23ContagemResultadoNotificacao_DataHora2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV23ContagemResultadoNotificacao_DataHora2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV23ContagemResultadoNotificacao_DataHora2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 2 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV23ContagemResultadoNotificacao_DataHora2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] >= @AV23ContagemResultadoNotificacao_DataHora2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] >= @AV23ContagemResultadoNotificacao_DataHora2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoNotificacao_DataHora_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV22DynamicFiltersOperator2 == 3 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoNotificacao_DataHora_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] <= @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] <= @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV29ContagemResultadoNotificacao_DataHora3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV29ContagemResultadoNotificacao_DataHora3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV29ContagemResultadoNotificacao_DataHora3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 2 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV29ContagemResultadoNotificacao_DataHora3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] >= @AV29ContagemResultadoNotificacao_DataHora3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] >= @AV29ContagemResultadoNotificacao_DataHora3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! (DateTime.MinValue==AV30ContagemResultadoNotificacao_DataHora_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV28DynamicFiltersOperator3 == 3 ) && ( ! (DateTime.MinValue==AV30ContagemResultadoNotificacao_DataHora_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] <= @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] <= @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_DataHora]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_DataHora] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_UsuCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_UsuCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Usuario_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Assunto]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Assunto] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_CorpoEmail]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_CorpoEmail] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Midia]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Midia] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Host]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Host] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_User]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_User] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Port]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Port] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Aut]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Aut] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Sec]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Sec] DESC";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Logged]";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Logged] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00QR3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             DateTime AV17ContagemResultadoNotificacao_DataHora1 ,
                                             DateTime AV18ContagemResultadoNotificacao_DataHora_To1 ,
                                             String AV19ContagemResultadoNotificacao_UsuNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             DateTime AV23ContagemResultadoNotificacao_DataHora2 ,
                                             DateTime AV24ContagemResultadoNotificacao_DataHora_To2 ,
                                             String AV25ContagemResultadoNotificacao_UsuNom2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             DateTime AV29ContagemResultadoNotificacao_DataHora3 ,
                                             DateTime AV30ContagemResultadoNotificacao_DataHora_To3 ,
                                             String AV31ContagemResultadoNotificacao_UsuNom3 ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [18] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContagemResultadoNotificacao] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV17ContagemResultadoNotificacao_DataHora1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV17ContagemResultadoNotificacao_DataHora1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV17ContagemResultadoNotificacao_DataHora1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 2 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV17ContagemResultadoNotificacao_DataHora1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] >= @AV17ContagemResultadoNotificacao_DataHora1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] >= @AV17ContagemResultadoNotificacao_DataHora1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (DateTime.MinValue==AV18ContagemResultadoNotificacao_DataHora_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV16DynamicFiltersOperator1 == 3 ) && ( ! (DateTime.MinValue==AV18ContagemResultadoNotificacao_DataHora_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] <= @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] <= @AV18ContagemResultadoNotificacao_DataHora_To1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultadoNotificacao_UsuNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV19ContagemResultadoNotificacao_UsuNom1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV23ContagemResultadoNotificacao_DataHora2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV23ContagemResultadoNotificacao_DataHora2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV23ContagemResultadoNotificacao_DataHora2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 2 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV23ContagemResultadoNotificacao_DataHora2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] >= @AV23ContagemResultadoNotificacao_DataHora2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] >= @AV23ContagemResultadoNotificacao_DataHora2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoNotificacao_DataHora_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV22DynamicFiltersOperator2 == 3 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoNotificacao_DataHora_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] <= @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] <= @AV24ContagemResultadoNotificacao_DataHora_To2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultadoNotificacao_UsuNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV25ContagemResultadoNotificacao_UsuNom2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV29ContagemResultadoNotificacao_DataHora3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV29ContagemResultadoNotificacao_DataHora3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV29ContagemResultadoNotificacao_DataHora3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 2 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV29ContagemResultadoNotificacao_DataHora3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] >= @AV29ContagemResultadoNotificacao_DataHora3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] >= @AV29ContagemResultadoNotificacao_DataHora3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! (DateTime.MinValue==AV30ContagemResultadoNotificacao_DataHora_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] < @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] < @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( AV28DynamicFiltersOperator3 == 3 ) && ( ! (DateTime.MinValue==AV30ContagemResultadoNotificacao_DataHora_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoNotificacao_DataHora] <= @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoNotificacao_DataHora] <= @AV30ContagemResultadoNotificacao_DataHora_To3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultadoNotificacao_UsuNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV31ContagemResultadoNotificacao_UsuNom3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00QR2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (bool)dynConstraints[20] );
               case 1 :
                     return conditional_H00QR3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (bool)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QR2 ;
          prmH00QR2 = new Object[] {
          new Object[] {"@AV17ContagemResultadoNotificacao_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV17ContagemResultadoNotificacao_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_UsuNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_UsuNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoNotificacao_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoNotificacao_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV25ContagemResultadoNotificacao_UsuNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContagemResultadoNotificacao_UsuNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29ContagemResultadoNotificacao_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV29ContagemResultadoNotificacao_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV30ContagemResultadoNotificacao_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV30ContagemResultadoNotificacao_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV31ContagemResultadoNotificacao_UsuNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContagemResultadoNotificacao_UsuNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00QR3 ;
          prmH00QR3 = new Object[] {
          new Object[] {"@AV17ContagemResultadoNotificacao_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV17ContagemResultadoNotificacao_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18ContagemResultadoNotificacao_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_UsuNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContagemResultadoNotificacao_UsuNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV23ContagemResultadoNotificacao_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoNotificacao_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoNotificacao_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV25ContagemResultadoNotificacao_UsuNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContagemResultadoNotificacao_UsuNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29ContagemResultadoNotificacao_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV29ContagemResultadoNotificacao_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV30ContagemResultadoNotificacao_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV30ContagemResultadoNotificacao_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV31ContagemResultadoNotificacao_UsuNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContagemResultadoNotificacao_UsuNom3",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QR2,11,0,true,false )
             ,new CursorDef("H00QR3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QR3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 3) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[25])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((long[]) buf[27])[0] = rslt.getLong(15) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                return;
       }
    }

 }

}
