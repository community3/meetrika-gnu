/*
               File: ContratoCaixaEntrada
        Description: Configura��es da Caixa de Entrada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:27.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratocaixaentrada : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel3"+"_"+"CONTRATOCAIXAENTRADA_CODIGO") == 0 )
         {
            AV13ContratoCaixaEntrada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoCaixaEntrada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX3ASACONTRATOCAIXAENTRADA_CODIGO58230( AV13ContratoCaixaEntrada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel4"+"_"+"CONTRATOCAIXAENTRADA_CODIGO") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX4ASACONTRATOCAIXAENTRADA_CODIGO58230( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A39Contratada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A40Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A40Contratada_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contrato_Codigo), "ZZZZZ9")));
               AV13ContratoCaixaEntrada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoCaixaEntrada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratoCaixaEntrada_GestorTodasEnviadas.Name = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         cmbContratoCaixaEntrada_GestorTodasEnviadas.WebTags = "";
         cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbContratoCaixaEntrada_GestorTodasEnviadas.ItemCount > 0 )
         {
            if ( (false==A2097ContratoCaixaEntrada_GestorTodasEnviadas) )
            {
               A2097ContratoCaixaEntrada_GestorTodasEnviadas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            }
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cmbContratoCaixaEntrada_GestorTodasEnviadas.getValidValue(StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Configura��es da Caixa de Entrada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratocaixaentrada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratocaixaentrada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Contrato_Codigo ,
                           int aP2_ContratoCaixaEntrada_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV13ContratoCaixaEntrada_Codigo = aP2_ContratoCaixaEntrada_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoCaixaEntrada_GestorTodasEnviadas = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoCaixaEntrada_GestorTodasEnviadas.ItemCount > 0 )
         {
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cmbContratoCaixaEntrada_GestorTodasEnviadas.getValidValue(StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_58230( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_58230e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContrato_Codigo_Visible, edtContrato_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoCaixaEntrada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoCaixaEntrada_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A2098ContratoCaixaEntrada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoCaixaEntrada_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoCaixaEntrada_Codigo_Visible, edtContratoCaixaEntrada_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoCaixaEntrada.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_58230( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_58230( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_58230e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_39_58230( true) ;
         }
         return  ;
      }

      protected void wb_table3_39_58230e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_58230e( true) ;
         }
         else
         {
            wb_table1_2_58230e( false) ;
         }
      }

      protected void wb_table3_39_58230( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_58230e( true) ;
         }
         else
         {
            wb_table3_39_58230e( false) ;
         }
      }

      protected void wb_table2_5_58230( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_58230( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_58230e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_58230e( true) ;
         }
         else
         {
            wb_table2_5_58230e( false) ;
         }
      }

      protected void wb_table4_13_58230( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_16_58230( true) ;
         }
         return  ;
      }

      protected void wb_table5_16_58230e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_58230e( true) ;
         }
         else
         {
            wb_table4_13_58230e( false) ;
         }
      }

      protected void wb_table5_16_58230( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblAtt_Internalname, tblAtt_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N�mero do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratocaixaentrada_gestortodasenviadas_Internalname, "Gestor visualiza todas as OS Enviadas", "", "", lblTextblockcontratocaixaentrada_gestortodasenviadas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_31_58230( true) ;
         }
         return  ;
      }

      protected void wb_table6_31_58230e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_58230e( true) ;
         }
         else
         {
            wb_table5_16_58230e( false) ;
         }
      }

      protected void wb_table6_31_58230( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratocaixaentrada_gestortodasenviadas_Internalname, tblTablemergedcontratocaixaentrada_gestortodasenviadas_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoCaixaEntrada_GestorTodasEnviadas, cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname, StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas), 1, cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratoCaixaEntrada_GestorTodasEnviadas.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_ContratoCaixaEntrada.htm");
            cmbContratoCaixaEntrada_GestorTodasEnviadas.CurrentValue = StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname, "Values", (String)(cmbContratoCaixaEntrada_GestorTodasEnviadas.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image BootstrapTooltipRight";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgContratocaixaentrada_gestortodasenviadas_infoicon_Internalname, context.GetImagePath( "2e4f3057-53d8-4ff3-81df-57002c73064d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgContratocaixaentrada_gestortodasenviadas_infoicon_Visible, 1, "", "Permite ao Gestor do Contrato visualizar todas as OS(s) envidas", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_58230e( true) ;
         }
         else
         {
            wb_table6_31_58230e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11582 */
         E11582 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               cmbContratoCaixaEntrada_GestorTodasEnviadas.CurrentValue = cgiGet( cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname);
               A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cgiGet( cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A74Contrato_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               }
               else
               {
                  A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoCaixaEntrada_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoCaixaEntrada_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOCAIXAENTRADA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoCaixaEntrada_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2098ContratoCaixaEntrada_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
               }
               else
               {
                  A2098ContratoCaixaEntrada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoCaixaEntrada_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z2098ContratoCaixaEntrada_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2098ContratoCaixaEntrada_Codigo"), ",", "."));
               Z74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               Z2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cgiGet( "Z2097ContratoCaixaEntrada_GestorTodasEnviadas"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATO_CODIGO"), ",", "."));
               AV13ContratoCaixaEntrada_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOCAIXAENTRADA_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               A42Contratada_PessoaCNPJ = cgiGet( "CONTRATADA_PESSOACNPJ");
               n42Contratada_PessoaCNPJ = false;
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoCaixaEntrada";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A2098ContratoCaixaEntrada_Codigo != Z2098ContratoCaixaEntrada_Codigo ) || ( A74Contrato_Codigo != Z74Contrato_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratocaixaentrada:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A2098ContratoCaixaEntrada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
                  A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode230 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode230;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound230 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_580( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOCAIXAENTRADA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratoCaixaEntrada_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11582 */
                           E11582 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12582 */
                           E12582 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12582 */
            E12582 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll58230( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes58230( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_580( )
      {
         BeforeValidate58230( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls58230( ) ;
            }
            else
            {
               CheckExtendedTable58230( ) ;
               CloseExtendedTableCursors58230( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption580( )
      {
      }

      protected void E11582( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
         edtContratoCaixaEntrada_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoCaixaEntrada_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoCaixaEntrada_Codigo_Visible), 5, 0)));
      }

      protected void E12582( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratocaixaentrada.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM58230( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2097ContratoCaixaEntrada_GestorTodasEnviadas = T00583_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0];
            }
            else
            {
               Z2097ContratoCaixaEntrada_GestorTodasEnviadas = A2097ContratoCaixaEntrada_GestorTodasEnviadas;
            }
         }
         if ( GX_JID == -9 )
         {
            Z2098ContratoCaixaEntrada_Codigo = A2098ContratoCaixaEntrada_Codigo;
            Z2097ContratoCaixaEntrada_GestorTodasEnviadas = A2097ContratoCaixaEntrada_GestorTodasEnviadas;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         if ( ! (0==AV7Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV7Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( ! (0==AV7Contrato_Codigo) )
         {
            edtContrato_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContrato_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7Contrato_Codigo) )
         {
            edtContrato_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV13ContratoCaixaEntrada_Codigo) )
         {
            A2098ContratoCaixaEntrada_Codigo = AV13ContratoCaixaEntrada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
         }
         if ( ! (0==AV13ContratoCaixaEntrada_Codigo) )
         {
            edtContratoCaixaEntrada_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoCaixaEntrada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoCaixaEntrada_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContratoCaixaEntrada_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoCaixaEntrada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoCaixaEntrada_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV13ContratoCaixaEntrada_Codigo) )
         {
            edtContratoCaixaEntrada_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoCaixaEntrada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoCaixaEntrada_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2097ContratoCaixaEntrada_GestorTodasEnviadas) && ( Gx_BScreen == 0 ) )
         {
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00584 */
            pr_default.execute(2, new Object[] {A74Contrato_Codigo});
            A77Contrato_Numero = T00584_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A39Contratada_Codigo = T00584_A39Contratada_Codigo[0];
            pr_default.close(2);
            /* Using cursor T00585 */
            pr_default.execute(3, new Object[] {A39Contratada_Codigo});
            A40Contratada_PessoaCod = T00585_A40Contratada_PessoaCod[0];
            pr_default.close(3);
            /* Using cursor T00586 */
            pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T00586_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T00586_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T00586_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = T00586_n42Contratada_PessoaCNPJ[0];
            pr_default.close(4);
         }
      }

      protected void Load58230( )
      {
         /* Using cursor T00587 */
         pr_default.execute(5, new Object[] {A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound230 = 1;
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = T00587_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            A77Contrato_Numero = T00587_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A41Contratada_PessoaNom = T00587_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T00587_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T00587_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = T00587_n42Contratada_PessoaCNPJ[0];
            A39Contratada_Codigo = T00587_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = T00587_A40Contratada_PessoaCod[0];
            ZM58230( -9) ;
         }
         pr_default.close(5);
         OnLoadActions58230( ) ;
      }

      protected void OnLoadActions58230( )
      {
      }

      protected void CheckExtendedTable58230( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00584 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A77Contrato_Numero = T00584_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A39Contratada_Codigo = T00584_A39Contratada_Codigo[0];
         pr_default.close(2);
         /* Using cursor T00585 */
         pr_default.execute(3, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T00585_A40Contratada_PessoaCod[0];
         pr_default.close(3);
         /* Using cursor T00586 */
         pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T00586_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T00586_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T00586_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = T00586_n42Contratada_PessoaCNPJ[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors58230( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A74Contrato_Codigo )
      {
         /* Using cursor T00588 */
         pr_default.execute(6, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A77Contrato_Numero = T00588_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A39Contratada_Codigo = T00588_A39Contratada_Codigo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A77Contrato_Numero))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_11( int A39Contratada_Codigo )
      {
         /* Using cursor T00589 */
         pr_default.execute(7, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T00589_A40Contratada_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_12( int A40Contratada_PessoaCod )
      {
         /* Using cursor T005810 */
         pr_default.execute(8, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T005810_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T005810_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T005810_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = T005810_n42Contratada_PessoaCNPJ[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A41Contratada_PessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( A42Contratada_PessoaCNPJ)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey58230( )
      {
         /* Using cursor T005811 */
         pr_default.execute(9, new Object[] {A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound230 = 1;
         }
         else
         {
            RcdFound230 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00583 */
         pr_default.execute(1, new Object[] {A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM58230( 9) ;
            RcdFound230 = 1;
            A2098ContratoCaixaEntrada_Codigo = T00583_A2098ContratoCaixaEntrada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = T00583_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            A74Contrato_Codigo = T00583_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            Z2098ContratoCaixaEntrada_Codigo = A2098ContratoCaixaEntrada_Codigo;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            sMode230 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load58230( ) ;
            if ( AnyError == 1 )
            {
               RcdFound230 = 0;
               InitializeNonKey58230( ) ;
            }
            Gx_mode = sMode230;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound230 = 0;
            InitializeNonKey58230( ) ;
            sMode230 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode230;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey58230( ) ;
         if ( RcdFound230 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound230 = 0;
         /* Using cursor T005812 */
         pr_default.execute(10, new Object[] {A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T005812_A2098ContratoCaixaEntrada_Codigo[0] < A2098ContratoCaixaEntrada_Codigo ) || ( T005812_A2098ContratoCaixaEntrada_Codigo[0] == A2098ContratoCaixaEntrada_Codigo ) && ( T005812_A74Contrato_Codigo[0] < A74Contrato_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T005812_A2098ContratoCaixaEntrada_Codigo[0] > A2098ContratoCaixaEntrada_Codigo ) || ( T005812_A2098ContratoCaixaEntrada_Codigo[0] == A2098ContratoCaixaEntrada_Codigo ) && ( T005812_A74Contrato_Codigo[0] > A74Contrato_Codigo ) ) )
            {
               A2098ContratoCaixaEntrada_Codigo = T005812_A2098ContratoCaixaEntrada_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
               A74Contrato_Codigo = T005812_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               RcdFound230 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound230 = 0;
         /* Using cursor T005813 */
         pr_default.execute(11, new Object[] {A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T005813_A2098ContratoCaixaEntrada_Codigo[0] > A2098ContratoCaixaEntrada_Codigo ) || ( T005813_A2098ContratoCaixaEntrada_Codigo[0] == A2098ContratoCaixaEntrada_Codigo ) && ( T005813_A74Contrato_Codigo[0] > A74Contrato_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T005813_A2098ContratoCaixaEntrada_Codigo[0] < A2098ContratoCaixaEntrada_Codigo ) || ( T005813_A2098ContratoCaixaEntrada_Codigo[0] == A2098ContratoCaixaEntrada_Codigo ) && ( T005813_A74Contrato_Codigo[0] < A74Contrato_Codigo ) ) )
            {
               A2098ContratoCaixaEntrada_Codigo = T005813_A2098ContratoCaixaEntrada_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
               A74Contrato_Codigo = T005813_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               RcdFound230 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey58230( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert58230( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound230 == 1 )
            {
               if ( ( A2098ContratoCaixaEntrada_Codigo != Z2098ContratoCaixaEntrada_Codigo ) || ( A74Contrato_Codigo != Z74Contrato_Codigo ) )
               {
                  A2098ContratoCaixaEntrada_Codigo = Z2098ContratoCaixaEntrada_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
                  A74Contrato_Codigo = Z74Contrato_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOCAIXAENTRADA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoCaixaEntrada_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update58230( ) ;
                  GX_FocusControl = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A2098ContratoCaixaEntrada_Codigo != Z2098ContratoCaixaEntrada_Codigo ) || ( A74Contrato_Codigo != Z74Contrato_Codigo ) )
               {
                  /* Insert record */
                  GX_FocusControl = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert58230( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOCAIXAENTRADA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoCaixaEntrada_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert58230( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A2098ContratoCaixaEntrada_Codigo != Z2098ContratoCaixaEntrada_Codigo ) || ( A74Contrato_Codigo != Z74Contrato_Codigo ) )
         {
            A2098ContratoCaixaEntrada_Codigo = Z2098ContratoCaixaEntrada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
            A74Contrato_Codigo = Z74Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOCAIXAENTRADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoCaixaEntrada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency58230( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00582 */
            pr_default.execute(0, new Object[] {A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoCaixaEntrada"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2097ContratoCaixaEntrada_GestorTodasEnviadas != T00582_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0] ) )
            {
               if ( Z2097ContratoCaixaEntrada_GestorTodasEnviadas != T00582_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0] )
               {
                  GXUtil.WriteLog("contratocaixaentrada:[seudo value changed for attri]"+"ContratoCaixaEntrada_GestorTodasEnviadas");
                  GXUtil.WriteLogRaw("Old: ",Z2097ContratoCaixaEntrada_GestorTodasEnviadas);
                  GXUtil.WriteLogRaw("Current: ",T00582_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoCaixaEntrada"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert58230( )
      {
         BeforeValidate58230( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable58230( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM58230( 0) ;
            CheckOptimisticConcurrency58230( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm58230( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert58230( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005814 */
                     pr_default.execute(12, new Object[] {A2098ContratoCaixaEntrada_Codigo, A2097ContratoCaixaEntrada_GestorTodasEnviadas, A74Contrato_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoCaixaEntrada") ;
                     if ( (pr_default.getStatus(12) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption580( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load58230( ) ;
            }
            EndLevel58230( ) ;
         }
         CloseExtendedTableCursors58230( ) ;
      }

      protected void Update58230( )
      {
         BeforeValidate58230( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable58230( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency58230( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm58230( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate58230( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005815 */
                     pr_default.execute(13, new Object[] {A2097ContratoCaixaEntrada_GestorTodasEnviadas, A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoCaixaEntrada") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoCaixaEntrada"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate58230( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel58230( ) ;
         }
         CloseExtendedTableCursors58230( ) ;
      }

      protected void DeferredUpdate58230( )
      {
      }

      protected void delete( )
      {
         BeforeValidate58230( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency58230( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls58230( ) ;
            AfterConfirm58230( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete58230( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005816 */
                  pr_default.execute(14, new Object[] {A2098ContratoCaixaEntrada_Codigo, A74Contrato_Codigo});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoCaixaEntrada") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode230 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel58230( ) ;
         Gx_mode = sMode230;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls58230( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T005817 */
            pr_default.execute(15, new Object[] {A74Contrato_Codigo});
            A77Contrato_Numero = T005817_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A39Contratada_Codigo = T005817_A39Contratada_Codigo[0];
            pr_default.close(15);
            /* Using cursor T005818 */
            pr_default.execute(16, new Object[] {A39Contratada_Codigo});
            A40Contratada_PessoaCod = T005818_A40Contratada_PessoaCod[0];
            pr_default.close(16);
            /* Using cursor T005819 */
            pr_default.execute(17, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T005819_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T005819_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T005819_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = T005819_n42Contratada_PessoaCNPJ[0];
            pr_default.close(17);
         }
      }

      protected void EndLevel58230( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete58230( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            pr_default.close(17);
            context.CommitDataStores( "ContratoCaixaEntrada");
            if ( AnyError == 0 )
            {
               ConfirmValues580( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            pr_default.close(17);
            context.RollbackDataStores( "ContratoCaixaEntrada");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart58230( )
      {
         /* Scan By routine */
         /* Using cursor T005820 */
         pr_default.execute(18);
         RcdFound230 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound230 = 1;
            A2098ContratoCaixaEntrada_Codigo = T005820_A2098ContratoCaixaEntrada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
            A74Contrato_Codigo = T005820_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext58230( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound230 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound230 = 1;
            A2098ContratoCaixaEntrada_Codigo = T005820_A2098ContratoCaixaEntrada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
            A74Contrato_Codigo = T005820_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd58230( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm58230( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert58230( )
      {
         /* Before Insert Rules */
         GXt_int1 = A2098ContratoCaixaEntrada_Codigo;
         new prc_contratocaixaentradaproximoid(context ).execute(  A74Contrato_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         A2098ContratoCaixaEntrada_Codigo = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
      }

      protected void BeforeUpdate58230( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete58230( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete58230( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate58230( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes58230( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         cmbContratoCaixaEntrada_GestorTodasEnviadas.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoCaixaEntrada_GestorTodasEnviadas.Enabled), 5, 0)));
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         edtContratoCaixaEntrada_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoCaixaEntrada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoCaixaEntrada_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues580( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117302890");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratocaixaentrada.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode("" +AV13ContratoCaixaEntrada_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2098ContratoCaixaEntrada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2097ContratoCaixaEntrada_GestorTodasEnviadas", Z2097ContratoCaixaEntrada_GestorTodasEnviadas);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOCAIXAENTRADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13ContratoCaixaEntrada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACNPJ", A42Contratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoCaixaEntrada";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratocaixaentrada:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratocaixaentrada.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode("" +AV13ContratoCaixaEntrada_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoCaixaEntrada" ;
      }

      public override String GetPgmdesc( )
      {
         return "Configura��es da Caixa de Entrada" ;
      }

      protected void InitializeNonKey58230( )
      {
         A77Contrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A39Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         A40Contratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         A2097ContratoCaixaEntrada_GestorTodasEnviadas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
         Z2097ContratoCaixaEntrada_GestorTodasEnviadas = false;
      }

      protected void InitAll58230( )
      {
         A2098ContratoCaixaEntrada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
         A74Contrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         InitializeNonKey58230( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2097ContratoCaixaEntrada_GestorTodasEnviadas = i2097ContratoCaixaEntrada_GestorTodasEnviadas;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2097ContratoCaixaEntrada_GestorTodasEnviadas", A2097ContratoCaixaEntrada_GestorTodasEnviadas);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311730294");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratocaixaentrada.js", "?2020311730294");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratocaixaentrada_gestortodasenviadas_Internalname = "TEXTBLOCKCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         imgContratocaixaentrada_gestortodasenviadas_infoicon_Internalname = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS_INFOICON";
         tblTablemergedcontratocaixaentrada_gestortodasenviadas_Internalname = "TABLEMERGEDCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         tblAtt_Internalname = "ATT";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContratoCaixaEntrada_Codigo_Internalname = "CONTRATOCAIXAENTRADA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Title = "Configura��es da Caixa de Entrada";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Configura��es da Caixa de Entrada";
         imgContratocaixaentrada_gestortodasenviadas_infoicon_Visible = 1;
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick = "";
         cmbContratoCaixaEntrada_GestorTodasEnviadas.Enabled = 1;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratoCaixaEntrada_Codigo_Jsonclick = "";
         edtContratoCaixaEntrada_Codigo_Enabled = 1;
         edtContratoCaixaEntrada_Codigo_Visible = 1;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Enabled = 1;
         edtContrato_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX3ASACONTRATOCAIXAENTRADA_CODIGO58230( int AV13ContratoCaixaEntrada_Codigo )
      {
         if ( ! (0==AV13ContratoCaixaEntrada_Codigo) )
         {
            A2098ContratoCaixaEntrada_Codigo = AV13ContratoCaixaEntrada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX4ASACONTRATOCAIXAENTRADA_CODIGO58230( int A74Contrato_Codigo )
      {
         GXt_int1 = A2098ContratoCaixaEntrada_Codigo;
         new prc_contratocaixaentradaproximoid(context ).execute(  A74Contrato_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         A2098ContratoCaixaEntrada_Codigo = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2098ContratoCaixaEntrada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contrato_codigo( int GX_Parm1 ,
                                         int GX_Parm2 ,
                                         int GX_Parm3 ,
                                         String GX_Parm4 ,
                                         String GX_Parm5 ,
                                         String GX_Parm6 )
      {
         A74Contrato_Codigo = GX_Parm1;
         A39Contratada_Codigo = GX_Parm2;
         A40Contratada_PessoaCod = GX_Parm3;
         A77Contrato_Numero = GX_Parm4;
         A41Contratada_PessoaNom = GX_Parm5;
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = GX_Parm6;
         n42Contratada_PessoaCNPJ = false;
         /* Using cursor T005817 */
         pr_default.execute(15, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
         }
         A77Contrato_Numero = T005817_A77Contrato_Numero[0];
         A39Contratada_Codigo = T005817_A39Contratada_Codigo[0];
         pr_default.close(15);
         /* Using cursor T005818 */
         pr_default.execute(16, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T005818_A40Contratada_PessoaCod[0];
         pr_default.close(16);
         /* Using cursor T005819 */
         pr_default.execute(17, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T005819_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = T005819_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T005819_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = T005819_n42Contratada_PessoaCNPJ[0];
         pr_default.close(17);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A77Contrato_Numero = "";
            A39Contratada_Codigo = 0;
            A40Contratada_PessoaCod = 0;
            A41Contratada_PessoaNom = "";
            n41Contratada_PessoaNom = false;
            A42Contratada_PessoaCNPJ = "";
            n42Contratada_PessoaCNPJ = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A77Contrato_Numero));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A41Contratada_PessoaNom));
         isValidOutput.Add(A42Contratada_PessoaCNPJ);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV13ContratoCaixaEntrada_Codigo',fld:'vCONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12582',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(16);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         A77Contrato_Numero = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratocaixaentrada_gestortodasenviadas_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode230 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z77Contrato_Numero = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         T00584_A77Contrato_Numero = new String[] {""} ;
         T00584_A39Contratada_Codigo = new int[1] ;
         T00585_A40Contratada_PessoaCod = new int[1] ;
         T00586_A41Contratada_PessoaNom = new String[] {""} ;
         T00586_n41Contratada_PessoaNom = new bool[] {false} ;
         T00586_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T00586_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T00587_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T00587_A2097ContratoCaixaEntrada_GestorTodasEnviadas = new bool[] {false} ;
         T00587_A77Contrato_Numero = new String[] {""} ;
         T00587_A41Contratada_PessoaNom = new String[] {""} ;
         T00587_n41Contratada_PessoaNom = new bool[] {false} ;
         T00587_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T00587_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T00587_A74Contrato_Codigo = new int[1] ;
         T00587_A39Contratada_Codigo = new int[1] ;
         T00587_A40Contratada_PessoaCod = new int[1] ;
         T00588_A77Contrato_Numero = new String[] {""} ;
         T00588_A39Contratada_Codigo = new int[1] ;
         T00589_A40Contratada_PessoaCod = new int[1] ;
         T005810_A41Contratada_PessoaNom = new String[] {""} ;
         T005810_n41Contratada_PessoaNom = new bool[] {false} ;
         T005810_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T005810_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T005811_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T005811_A74Contrato_Codigo = new int[1] ;
         T00583_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T00583_A2097ContratoCaixaEntrada_GestorTodasEnviadas = new bool[] {false} ;
         T00583_A74Contrato_Codigo = new int[1] ;
         T005812_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T005812_A74Contrato_Codigo = new int[1] ;
         T005813_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T005813_A74Contrato_Codigo = new int[1] ;
         T00582_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T00582_A2097ContratoCaixaEntrada_GestorTodasEnviadas = new bool[] {false} ;
         T00582_A74Contrato_Codigo = new int[1] ;
         T005817_A77Contrato_Numero = new String[] {""} ;
         T005817_A39Contratada_Codigo = new int[1] ;
         T005818_A40Contratada_PessoaCod = new int[1] ;
         T005819_A41Contratada_PessoaNom = new String[] {""} ;
         T005819_n41Contratada_PessoaNom = new bool[] {false} ;
         T005819_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T005819_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T005820_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T005820_A74Contrato_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratocaixaentrada__default(),
            new Object[][] {
                new Object[] {
               T00582_A2098ContratoCaixaEntrada_Codigo, T00582_A2097ContratoCaixaEntrada_GestorTodasEnviadas, T00582_A74Contrato_Codigo
               }
               , new Object[] {
               T00583_A2098ContratoCaixaEntrada_Codigo, T00583_A2097ContratoCaixaEntrada_GestorTodasEnviadas, T00583_A74Contrato_Codigo
               }
               , new Object[] {
               T00584_A77Contrato_Numero, T00584_A39Contratada_Codigo
               }
               , new Object[] {
               T00585_A40Contratada_PessoaCod
               }
               , new Object[] {
               T00586_A41Contratada_PessoaNom, T00586_n41Contratada_PessoaNom, T00586_A42Contratada_PessoaCNPJ, T00586_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T00587_A2098ContratoCaixaEntrada_Codigo, T00587_A2097ContratoCaixaEntrada_GestorTodasEnviadas, T00587_A77Contrato_Numero, T00587_A41Contratada_PessoaNom, T00587_n41Contratada_PessoaNom, T00587_A42Contratada_PessoaCNPJ, T00587_n42Contratada_PessoaCNPJ, T00587_A74Contrato_Codigo, T00587_A39Contratada_Codigo, T00587_A40Contratada_PessoaCod
               }
               , new Object[] {
               T00588_A77Contrato_Numero, T00588_A39Contratada_Codigo
               }
               , new Object[] {
               T00589_A40Contratada_PessoaCod
               }
               , new Object[] {
               T005810_A41Contratada_PessoaNom, T005810_n41Contratada_PessoaNom, T005810_A42Contratada_PessoaCNPJ, T005810_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T005811_A2098ContratoCaixaEntrada_Codigo, T005811_A74Contrato_Codigo
               }
               , new Object[] {
               T005812_A2098ContratoCaixaEntrada_Codigo, T005812_A74Contrato_Codigo
               }
               , new Object[] {
               T005813_A2098ContratoCaixaEntrada_Codigo, T005813_A74Contrato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005817_A77Contrato_Numero, T005817_A39Contratada_Codigo
               }
               , new Object[] {
               T005818_A40Contratada_PessoaCod
               }
               , new Object[] {
               T005819_A41Contratada_PessoaNom, T005819_n41Contratada_PessoaNom, T005819_A42Contratada_PessoaCNPJ, T005819_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T005820_A2098ContratoCaixaEntrada_Codigo, T005820_A74Contrato_Codigo
               }
            }
         );
         Z2097ContratoCaixaEntrada_GestorTodasEnviadas = false;
         A2097ContratoCaixaEntrada_GestorTodasEnviadas = false;
         i2097ContratoCaixaEntrada_GestorTodasEnviadas = false;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound230 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Contrato_Codigo ;
      private int wcpOAV13ContratoCaixaEntrada_Codigo ;
      private int Z2098ContratoCaixaEntrada_Codigo ;
      private int Z74Contrato_Codigo ;
      private int AV13ContratoCaixaEntrada_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV7Contrato_Codigo ;
      private int trnEnded ;
      private int edtContrato_Codigo_Visible ;
      private int edtContrato_Codigo_Enabled ;
      private int A2098ContratoCaixaEntrada_Codigo ;
      private int edtContratoCaixaEntrada_Codigo_Visible ;
      private int edtContratoCaixaEntrada_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtContrato_Numero_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int imgContratocaixaentrada_gestortodasenviadas_infoicon_Visible ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private int GXt_int1 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname ;
      private String TempTags ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContratoCaixaEntrada_Codigo_Internalname ;
      private String edtContratoCaixaEntrada_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblAtt_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratocaixaentrada_gestortodasenviadas_Internalname ;
      private String lblTextblockcontratocaixaentrada_gestortodasenviadas_Jsonclick ;
      private String tblTablemergedcontratocaixaentrada_gestortodasenviadas_Internalname ;
      private String cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick ;
      private String imgContratocaixaentrada_gestortodasenviadas_infoicon_Internalname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode230 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z77Contrato_Numero ;
      private String Z41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoCaixaEntrada_GestorTodasEnviadas ;
      private IDataStoreProvider pr_default ;
      private String[] T00584_A77Contrato_Numero ;
      private int[] T00584_A39Contratada_Codigo ;
      private int[] T00585_A40Contratada_PessoaCod ;
      private String[] T00586_A41Contratada_PessoaNom ;
      private bool[] T00586_n41Contratada_PessoaNom ;
      private String[] T00586_A42Contratada_PessoaCNPJ ;
      private bool[] T00586_n42Contratada_PessoaCNPJ ;
      private int[] T00587_A2098ContratoCaixaEntrada_Codigo ;
      private bool[] T00587_A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private String[] T00587_A77Contrato_Numero ;
      private String[] T00587_A41Contratada_PessoaNom ;
      private bool[] T00587_n41Contratada_PessoaNom ;
      private String[] T00587_A42Contratada_PessoaCNPJ ;
      private bool[] T00587_n42Contratada_PessoaCNPJ ;
      private int[] T00587_A74Contrato_Codigo ;
      private int[] T00587_A39Contratada_Codigo ;
      private int[] T00587_A40Contratada_PessoaCod ;
      private String[] T00588_A77Contrato_Numero ;
      private int[] T00588_A39Contratada_Codigo ;
      private int[] T00589_A40Contratada_PessoaCod ;
      private String[] T005810_A41Contratada_PessoaNom ;
      private bool[] T005810_n41Contratada_PessoaNom ;
      private String[] T005810_A42Contratada_PessoaCNPJ ;
      private bool[] T005810_n42Contratada_PessoaCNPJ ;
      private int[] T005811_A2098ContratoCaixaEntrada_Codigo ;
      private int[] T005811_A74Contrato_Codigo ;
      private int[] T00583_A2098ContratoCaixaEntrada_Codigo ;
      private bool[] T00583_A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private int[] T00583_A74Contrato_Codigo ;
      private int[] T005812_A2098ContratoCaixaEntrada_Codigo ;
      private int[] T005812_A74Contrato_Codigo ;
      private int[] T005813_A2098ContratoCaixaEntrada_Codigo ;
      private int[] T005813_A74Contrato_Codigo ;
      private int[] T00582_A2098ContratoCaixaEntrada_Codigo ;
      private bool[] T00582_A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private int[] T00582_A74Contrato_Codigo ;
      private String[] T005817_A77Contrato_Numero ;
      private int[] T005817_A39Contratada_Codigo ;
      private int[] T005818_A40Contratada_PessoaCod ;
      private String[] T005819_A41Contratada_PessoaNom ;
      private bool[] T005819_n41Contratada_PessoaNom ;
      private String[] T005819_A42Contratada_PessoaCNPJ ;
      private bool[] T005819_n42Contratada_PessoaCNPJ ;
      private int[] T005820_A2098ContratoCaixaEntrada_Codigo ;
      private int[] T005820_A74Contrato_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class contratocaixaentrada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00587 ;
          prmT00587 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00584 ;
          prmT00584 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00585 ;
          prmT00585 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00586 ;
          prmT00586 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00588 ;
          prmT00588 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00589 ;
          prmT00589 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005810 ;
          prmT005810 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005811 ;
          prmT005811 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00583 ;
          prmT00583 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005812 ;
          prmT005812 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005813 ;
          prmT005813 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00582 ;
          prmT00582 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005814 ;
          prmT005814 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoCaixaEntrada_GestorTodasEnviadas",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005815 ;
          prmT005815 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_GestorTodasEnviadas",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005816 ;
          prmT005816 = new Object[] {
          new Object[] {"@ContratoCaixaEntrada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005820 ;
          prmT005820 = new Object[] {
          } ;
          Object[] prmT005817 ;
          prmT005817 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005818 ;
          prmT005818 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005819 ;
          prmT005819 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00582", "SELECT [ContratoCaixaEntrada_Codigo], [ContratoCaixaEntrada_GestorTodasEnviadas], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (UPDLOCK) WHERE [ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo AND [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00582,1,0,true,false )
             ,new CursorDef("T00583", "SELECT [ContratoCaixaEntrada_Codigo], [ContratoCaixaEntrada_GestorTodasEnviadas], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE [ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo AND [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00583,1,0,true,false )
             ,new CursorDef("T00584", "SELECT [Contrato_Numero], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00584,1,0,true,false )
             ,new CursorDef("T00585", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00585,1,0,true,false )
             ,new CursorDef("T00586", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00586,1,0,true,false )
             ,new CursorDef("T00587", "SELECT TM1.[ContratoCaixaEntrada_Codigo], TM1.[ContratoCaixaEntrada_GestorTodasEnviadas], T2.[Contrato_Numero], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, TM1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod FROM ((([ContratoCaixaEntrada] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE TM1.[ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo and TM1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY TM1.[ContratoCaixaEntrada_Codigo], TM1.[Contrato_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00587,100,0,true,false )
             ,new CursorDef("T00588", "SELECT [Contrato_Numero], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00588,1,0,true,false )
             ,new CursorDef("T00589", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00589,1,0,true,false )
             ,new CursorDef("T005810", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005810,1,0,true,false )
             ,new CursorDef("T005811", "SELECT [ContratoCaixaEntrada_Codigo], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE [ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo AND [Contrato_Codigo] = @Contrato_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005811,1,0,true,false )
             ,new CursorDef("T005812", "SELECT TOP 1 [ContratoCaixaEntrada_Codigo], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE ( [ContratoCaixaEntrada_Codigo] > @ContratoCaixaEntrada_Codigo or [ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo and [Contrato_Codigo] > @Contrato_Codigo) ORDER BY [ContratoCaixaEntrada_Codigo], [Contrato_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005812,1,0,true,true )
             ,new CursorDef("T005813", "SELECT TOP 1 [ContratoCaixaEntrada_Codigo], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE ( [ContratoCaixaEntrada_Codigo] < @ContratoCaixaEntrada_Codigo or [ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo and [Contrato_Codigo] < @Contrato_Codigo) ORDER BY [ContratoCaixaEntrada_Codigo] DESC, [Contrato_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005813,1,0,true,true )
             ,new CursorDef("T005814", "INSERT INTO [ContratoCaixaEntrada]([ContratoCaixaEntrada_Codigo], [ContratoCaixaEntrada_GestorTodasEnviadas], [Contrato_Codigo]) VALUES(@ContratoCaixaEntrada_Codigo, @ContratoCaixaEntrada_GestorTodasEnviadas, @Contrato_Codigo)", GxErrorMask.GX_NOMASK,prmT005814)
             ,new CursorDef("T005815", "UPDATE [ContratoCaixaEntrada] SET [ContratoCaixaEntrada_GestorTodasEnviadas]=@ContratoCaixaEntrada_GestorTodasEnviadas  WHERE [ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo AND [Contrato_Codigo] = @Contrato_Codigo", GxErrorMask.GX_NOMASK,prmT005815)
             ,new CursorDef("T005816", "DELETE FROM [ContratoCaixaEntrada]  WHERE [ContratoCaixaEntrada_Codigo] = @ContratoCaixaEntrada_Codigo AND [Contrato_Codigo] = @Contrato_Codigo", GxErrorMask.GX_NOMASK,prmT005816)
             ,new CursorDef("T005817", "SELECT [Contrato_Numero], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005817,1,0,true,false )
             ,new CursorDef("T005818", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005818,1,0,true,false )
             ,new CursorDef("T005819", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005819,1,0,true,false )
             ,new CursorDef("T005820", "SELECT [ContratoCaixaEntrada_Codigo], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) ORDER BY [ContratoCaixaEntrada_Codigo], [Contrato_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005820,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
