/*
               File: GetPromptGeral_CargoFilterData
        Description: Get Prompt Geral_Cargo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:14.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptgeral_cargofilterdata : GXProcedure
   {
      public getpromptgeral_cargofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptgeral_cargofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptgeral_cargofilterdata objgetpromptgeral_cargofilterdata;
         objgetpromptgeral_cargofilterdata = new getpromptgeral_cargofilterdata();
         objgetpromptgeral_cargofilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptgeral_cargofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptgeral_cargofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptgeral_cargofilterdata.AV22OptionsJson = "" ;
         objgetpromptgeral_cargofilterdata.AV25OptionsDescJson = "" ;
         objgetpromptgeral_cargofilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptgeral_cargofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptgeral_cargofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptgeral_cargofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptgeral_cargofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CARGO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCARGO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CARGO_UONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCARGO_UONOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptGeral_CargoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptGeral_CargoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptGeral_CargoGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "CARGO_ATIVO") == 0 )
            {
               AV34Cargo_Ativo = BooleanUtil.Val( AV32GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME") == 0 )
            {
               AV10TFCargo_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME_SEL") == 0 )
            {
               AV11TFCargo_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_CODIGO") == 0 )
            {
               AV12TFGrupoCargo_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV13TFGrupoCargo_Codigo_To = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM") == 0 )
            {
               AV14TFCargo_UONom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM_SEL") == 0 )
            {
               AV15TFCargo_UONom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_NOME") == 0 )
            {
               AV36Cargo_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_UONOM") == 0 )
            {
               AV37Cargo_UONom1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CARGO_NOME") == 0 )
               {
                  AV40Cargo_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CARGO_UONOM") == 0 )
               {
                  AV41Cargo_UONom2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV42DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV43DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CARGO_NOME") == 0 )
                  {
                     AV44Cargo_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CARGO_UONOM") == 0 )
                  {
                     AV45Cargo_UONom3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCARGO_NOMEOPTIONS' Routine */
         AV10TFCargo_Nome = AV16SearchTxt;
         AV11TFCargo_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36Cargo_Nome1 ,
                                              AV37Cargo_UONom1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40Cargo_Nome2 ,
                                              AV41Cargo_UONom2 ,
                                              AV42DynamicFiltersEnabled3 ,
                                              AV43DynamicFiltersSelector3 ,
                                              AV44Cargo_Nome3 ,
                                              AV45Cargo_UONom3 ,
                                              AV11TFCargo_Nome_Sel ,
                                              AV10TFCargo_Nome ,
                                              AV12TFGrupoCargo_Codigo ,
                                              AV13TFGrupoCargo_Codigo_To ,
                                              AV15TFCargo_UONom_Sel ,
                                              AV14TFCargo_UONom ,
                                              A618Cargo_Nome ,
                                              A1003Cargo_UONom ,
                                              A615GrupoCargo_Codigo ,
                                              A628Cargo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV36Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV36Cargo_Nome1), "%", "");
         lV37Cargo_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV37Cargo_UONom1), 50, "%");
         lV40Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV40Cargo_Nome2), "%", "");
         lV41Cargo_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV41Cargo_UONom2), 50, "%");
         lV44Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV44Cargo_Nome3), "%", "");
         lV45Cargo_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV45Cargo_UONom3), 50, "%");
         lV10TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFCargo_Nome), "%", "");
         lV14TFCargo_UONom = StringUtil.PadR( StringUtil.RTrim( AV14TFCargo_UONom), 50, "%");
         /* Using cursor P00NI2 */
         pr_default.execute(0, new Object[] {lV36Cargo_Nome1, lV37Cargo_UONom1, lV40Cargo_Nome2, lV41Cargo_UONom2, lV44Cargo_Nome3, lV45Cargo_UONom3, lV10TFCargo_Nome, AV11TFCargo_Nome_Sel, AV12TFGrupoCargo_Codigo, AV13TFGrupoCargo_Codigo_To, lV14TFCargo_UONom, AV15TFCargo_UONom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNI2 = false;
            A1002Cargo_UOCod = P00NI2_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NI2_n1002Cargo_UOCod[0];
            A628Cargo_Ativo = P00NI2_A628Cargo_Ativo[0];
            A618Cargo_Nome = P00NI2_A618Cargo_Nome[0];
            A615GrupoCargo_Codigo = P00NI2_A615GrupoCargo_Codigo[0];
            A1003Cargo_UONom = P00NI2_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NI2_n1003Cargo_UONom[0];
            A617Cargo_Codigo = P00NI2_A617Cargo_Codigo[0];
            A1003Cargo_UONom = P00NI2_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NI2_n1003Cargo_UONom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00NI2_A618Cargo_Nome[0], A618Cargo_Nome) == 0 ) )
            {
               BRKNI2 = false;
               A617Cargo_Codigo = P00NI2_A617Cargo_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKNI2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A618Cargo_Nome)) )
            {
               AV20Option = A618Cargo_Nome;
               AV23OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")));
               AV21Options.Add(AV20Option, 0);
               AV24OptionsDesc.Add(AV23OptionDesc, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNI2 )
            {
               BRKNI2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCARGO_UONOMOPTIONS' Routine */
         AV14TFCargo_UONom = AV16SearchTxt;
         AV15TFCargo_UONom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36Cargo_Nome1 ,
                                              AV37Cargo_UONom1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40Cargo_Nome2 ,
                                              AV41Cargo_UONom2 ,
                                              AV42DynamicFiltersEnabled3 ,
                                              AV43DynamicFiltersSelector3 ,
                                              AV44Cargo_Nome3 ,
                                              AV45Cargo_UONom3 ,
                                              AV11TFCargo_Nome_Sel ,
                                              AV10TFCargo_Nome ,
                                              AV12TFGrupoCargo_Codigo ,
                                              AV13TFGrupoCargo_Codigo_To ,
                                              AV15TFCargo_UONom_Sel ,
                                              AV14TFCargo_UONom ,
                                              A618Cargo_Nome ,
                                              A1003Cargo_UONom ,
                                              A615GrupoCargo_Codigo ,
                                              A628Cargo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV36Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV36Cargo_Nome1), "%", "");
         lV37Cargo_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV37Cargo_UONom1), 50, "%");
         lV40Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV40Cargo_Nome2), "%", "");
         lV41Cargo_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV41Cargo_UONom2), 50, "%");
         lV44Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV44Cargo_Nome3), "%", "");
         lV45Cargo_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV45Cargo_UONom3), 50, "%");
         lV10TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFCargo_Nome), "%", "");
         lV14TFCargo_UONom = StringUtil.PadR( StringUtil.RTrim( AV14TFCargo_UONom), 50, "%");
         /* Using cursor P00NI3 */
         pr_default.execute(1, new Object[] {lV36Cargo_Nome1, lV37Cargo_UONom1, lV40Cargo_Nome2, lV41Cargo_UONom2, lV44Cargo_Nome3, lV45Cargo_UONom3, lV10TFCargo_Nome, AV11TFCargo_Nome_Sel, AV12TFGrupoCargo_Codigo, AV13TFGrupoCargo_Codigo_To, lV14TFCargo_UONom, AV15TFCargo_UONom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNI4 = false;
            A1002Cargo_UOCod = P00NI3_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NI3_n1002Cargo_UOCod[0];
            A615GrupoCargo_Codigo = P00NI3_A615GrupoCargo_Codigo[0];
            A1003Cargo_UONom = P00NI3_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NI3_n1003Cargo_UONom[0];
            A618Cargo_Nome = P00NI3_A618Cargo_Nome[0];
            A628Cargo_Ativo = P00NI3_A628Cargo_Ativo[0];
            A617Cargo_Codigo = P00NI3_A617Cargo_Codigo[0];
            A1003Cargo_UONom = P00NI3_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NI3_n1003Cargo_UONom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00NI3_A1002Cargo_UOCod[0] == A1002Cargo_UOCod ) )
            {
               BRKNI4 = false;
               A617Cargo_Codigo = P00NI3_A617Cargo_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKNI4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1003Cargo_UONom)) )
            {
               AV20Option = A1003Cargo_UONom;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNI4 )
            {
               BRKNI4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV34Cargo_Ativo = true;
         AV10TFCargo_Nome = "";
         AV11TFCargo_Nome_Sel = "";
         AV14TFCargo_UONom = "";
         AV15TFCargo_UONom_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV36Cargo_Nome1 = "";
         AV37Cargo_UONom1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV40Cargo_Nome2 = "";
         AV41Cargo_UONom2 = "";
         AV43DynamicFiltersSelector3 = "";
         AV44Cargo_Nome3 = "";
         AV45Cargo_UONom3 = "";
         scmdbuf = "";
         lV36Cargo_Nome1 = "";
         lV37Cargo_UONom1 = "";
         lV40Cargo_Nome2 = "";
         lV41Cargo_UONom2 = "";
         lV44Cargo_Nome3 = "";
         lV45Cargo_UONom3 = "";
         lV10TFCargo_Nome = "";
         lV14TFCargo_UONom = "";
         A618Cargo_Nome = "";
         A1003Cargo_UONom = "";
         P00NI2_A1002Cargo_UOCod = new int[1] ;
         P00NI2_n1002Cargo_UOCod = new bool[] {false} ;
         P00NI2_A628Cargo_Ativo = new bool[] {false} ;
         P00NI2_A618Cargo_Nome = new String[] {""} ;
         P00NI2_A615GrupoCargo_Codigo = new int[1] ;
         P00NI2_A1003Cargo_UONom = new String[] {""} ;
         P00NI2_n1003Cargo_UONom = new bool[] {false} ;
         P00NI2_A617Cargo_Codigo = new int[1] ;
         AV20Option = "";
         AV23OptionDesc = "";
         P00NI3_A1002Cargo_UOCod = new int[1] ;
         P00NI3_n1002Cargo_UOCod = new bool[] {false} ;
         P00NI3_A615GrupoCargo_Codigo = new int[1] ;
         P00NI3_A1003Cargo_UONom = new String[] {""} ;
         P00NI3_n1003Cargo_UONom = new bool[] {false} ;
         P00NI3_A618Cargo_Nome = new String[] {""} ;
         P00NI3_A628Cargo_Ativo = new bool[] {false} ;
         P00NI3_A617Cargo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptgeral_cargofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NI2_A1002Cargo_UOCod, P00NI2_n1002Cargo_UOCod, P00NI2_A628Cargo_Ativo, P00NI2_A618Cargo_Nome, P00NI2_A615GrupoCargo_Codigo, P00NI2_A1003Cargo_UONom, P00NI2_n1003Cargo_UONom, P00NI2_A617Cargo_Codigo
               }
               , new Object[] {
               P00NI3_A1002Cargo_UOCod, P00NI3_n1002Cargo_UOCod, P00NI3_A615GrupoCargo_Codigo, P00NI3_A1003Cargo_UONom, P00NI3_n1003Cargo_UONom, P00NI3_A618Cargo_Nome, P00NI3_A628Cargo_Ativo, P00NI3_A617Cargo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV48GXV1 ;
      private int AV12TFGrupoCargo_Codigo ;
      private int AV13TFGrupoCargo_Codigo_To ;
      private int A615GrupoCargo_Codigo ;
      private int A1002Cargo_UOCod ;
      private int A617Cargo_Codigo ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private String AV14TFCargo_UONom ;
      private String AV15TFCargo_UONom_Sel ;
      private String AV37Cargo_UONom1 ;
      private String AV41Cargo_UONom2 ;
      private String AV45Cargo_UONom3 ;
      private String scmdbuf ;
      private String lV37Cargo_UONom1 ;
      private String lV41Cargo_UONom2 ;
      private String lV45Cargo_UONom3 ;
      private String lV14TFCargo_UONom ;
      private String A1003Cargo_UONom ;
      private bool returnInSub ;
      private bool AV34Cargo_Ativo ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV42DynamicFiltersEnabled3 ;
      private bool A628Cargo_Ativo ;
      private bool BRKNI2 ;
      private bool n1002Cargo_UOCod ;
      private bool n1003Cargo_UONom ;
      private bool BRKNI4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV10TFCargo_Nome ;
      private String AV11TFCargo_Nome_Sel ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV36Cargo_Nome1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV40Cargo_Nome2 ;
      private String AV43DynamicFiltersSelector3 ;
      private String AV44Cargo_Nome3 ;
      private String lV36Cargo_Nome1 ;
      private String lV40Cargo_Nome2 ;
      private String lV44Cargo_Nome3 ;
      private String lV10TFCargo_Nome ;
      private String A618Cargo_Nome ;
      private String AV20Option ;
      private String AV23OptionDesc ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00NI2_A1002Cargo_UOCod ;
      private bool[] P00NI2_n1002Cargo_UOCod ;
      private bool[] P00NI2_A628Cargo_Ativo ;
      private String[] P00NI2_A618Cargo_Nome ;
      private int[] P00NI2_A615GrupoCargo_Codigo ;
      private String[] P00NI2_A1003Cargo_UONom ;
      private bool[] P00NI2_n1003Cargo_UONom ;
      private int[] P00NI2_A617Cargo_Codigo ;
      private int[] P00NI3_A1002Cargo_UOCod ;
      private bool[] P00NI3_n1002Cargo_UOCod ;
      private int[] P00NI3_A615GrupoCargo_Codigo ;
      private String[] P00NI3_A1003Cargo_UONom ;
      private bool[] P00NI3_n1003Cargo_UONom ;
      private String[] P00NI3_A618Cargo_Nome ;
      private bool[] P00NI3_A628Cargo_Ativo ;
      private int[] P00NI3_A617Cargo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getpromptgeral_cargofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NI2( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36Cargo_Nome1 ,
                                             String AV37Cargo_UONom1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             String AV40Cargo_Nome2 ,
                                             String AV41Cargo_UONom2 ,
                                             bool AV42DynamicFiltersEnabled3 ,
                                             String AV43DynamicFiltersSelector3 ,
                                             String AV44Cargo_Nome3 ,
                                             String AV45Cargo_UONom3 ,
                                             String AV11TFCargo_Nome_Sel ,
                                             String AV10TFCargo_Nome ,
                                             int AV12TFGrupoCargo_Codigo ,
                                             int AV13TFGrupoCargo_Codigo_To ,
                                             String AV15TFCargo_UONom_Sel ,
                                             String AV14TFCargo_UONom ,
                                             String A618Cargo_Nome ,
                                             String A1003Cargo_UONom ,
                                             int A615GrupoCargo_Codigo ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Cargo_UOCod] AS Cargo_UOCod, T1.[Cargo_Ativo], T1.[Cargo_Nome], T1.[GrupoCargo_Codigo], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV36Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Cargo_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV37Cargo_UONom1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV40Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Cargo_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV41Cargo_UONom2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV42DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV44Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV42DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Cargo_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV45Cargo_UONom3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV10TFCargo_Nome)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV11TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFGrupoCargo_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] >= @AV12TFGrupoCargo_Codigo)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFGrupoCargo_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] <= @AV13TFGrupoCargo_Codigo_To)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFCargo_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFCargo_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV14TFCargo_UONom)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFCargo_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV15TFCargo_UONom_Sel)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Cargo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00NI3( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36Cargo_Nome1 ,
                                             String AV37Cargo_UONom1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             String AV40Cargo_Nome2 ,
                                             String AV41Cargo_UONom2 ,
                                             bool AV42DynamicFiltersEnabled3 ,
                                             String AV43DynamicFiltersSelector3 ,
                                             String AV44Cargo_Nome3 ,
                                             String AV45Cargo_UONom3 ,
                                             String AV11TFCargo_Nome_Sel ,
                                             String AV10TFCargo_Nome ,
                                             int AV12TFGrupoCargo_Codigo ,
                                             int AV13TFGrupoCargo_Codigo_To ,
                                             String AV15TFCargo_UONom_Sel ,
                                             String AV14TFCargo_UONom ,
                                             String A618Cargo_Nome ,
                                             String A1003Cargo_UONom ,
                                             int A615GrupoCargo_Codigo ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Cargo_UOCod] AS Cargo_UOCod, T1.[GrupoCargo_Codigo], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[Cargo_Nome], T1.[Cargo_Ativo], T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV36Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Cargo_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV37Cargo_UONom1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV40Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Cargo_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV41Cargo_UONom2 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV42DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV44Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV42DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Cargo_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV45Cargo_UONom3 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV10TFCargo_Nome)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV11TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV12TFGrupoCargo_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] >= @AV12TFGrupoCargo_Codigo)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV13TFGrupoCargo_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] <= @AV13TFGrupoCargo_Codigo_To)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFCargo_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFCargo_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV14TFCargo_UONom)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFCargo_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV15TFCargo_UONom_Sel)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Cargo_UOCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NI2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (bool)dynConstraints[20] );
               case 1 :
                     return conditional_P00NI3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (bool)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NI2 ;
          prmP00NI2 = new Object[] {
          new Object[] {"@lV36Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV37Cargo_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV41Cargo_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV45Cargo_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV11TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV12TFGrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFGrupoCargo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFCargo_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFCargo_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00NI3 ;
          prmP00NI3 = new Object[] {
          new Object[] {"@lV36Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV37Cargo_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV41Cargo_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV45Cargo_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV11TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV12TFGrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFGrupoCargo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFCargo_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFCargo_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NI2,100,0,true,false )
             ,new CursorDef("P00NI3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NI3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptgeral_cargofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptgeral_cargofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptgeral_cargofilterdata") )
          {
             return  ;
          }
          getpromptgeral_cargofilterdata worker = new getpromptgeral_cargofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
