/*
               File: PRC_InserirSDTDemandas
        Description: Stub for PRC_InserirSDTDemandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:8:9.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_inserirsdtdemandas : GXProcedure
   {
      public prc_inserirsdtdemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_inserirsdtdemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           bool aP1_SrvParaFtr ,
                           int aP2_Usuario_Codigo ,
                           String aP3_StatusDmn ,
                           int aP4_ContratadaSrv_Codigo ,
                           String aP5_DataSrvFat ,
                           int aP6_ContratadaOrigem_Codigo ,
                           DateTime aP7_PrazoEntrega ,
                           String aP8_Agrupador ,
                           bool aP9_Notificar ,
                           int aP10_ContratoServicosOS_Codigo )
      {
         this.AV2Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV3SrvParaFtr = aP1_SrvParaFtr;
         this.AV4Usuario_Codigo = aP2_Usuario_Codigo;
         this.AV5StatusDmn = aP3_StatusDmn;
         this.AV6ContratadaSrv_Codigo = aP4_ContratadaSrv_Codigo;
         this.AV7DataSrvFat = aP5_DataSrvFat;
         this.AV8ContratadaOrigem_Codigo = aP6_ContratadaOrigem_Codigo;
         this.AV9PrazoEntrega = aP7_PrazoEntrega;
         this.AV10Agrupador = aP8_Agrupador;
         this.AV11Notificar = aP9_Notificar;
         this.AV12ContratoServicosOS_Codigo = aP10_ContratoServicosOS_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 bool aP1_SrvParaFtr ,
                                 int aP2_Usuario_Codigo ,
                                 String aP3_StatusDmn ,
                                 int aP4_ContratadaSrv_Codigo ,
                                 String aP5_DataSrvFat ,
                                 int aP6_ContratadaOrigem_Codigo ,
                                 DateTime aP7_PrazoEntrega ,
                                 String aP8_Agrupador ,
                                 bool aP9_Notificar ,
                                 int aP10_ContratoServicosOS_Codigo )
      {
         prc_inserirsdtdemandas objprc_inserirsdtdemandas;
         objprc_inserirsdtdemandas = new prc_inserirsdtdemandas();
         objprc_inserirsdtdemandas.AV2Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_inserirsdtdemandas.AV3SrvParaFtr = aP1_SrvParaFtr;
         objprc_inserirsdtdemandas.AV4Usuario_Codigo = aP2_Usuario_Codigo;
         objprc_inserirsdtdemandas.AV5StatusDmn = aP3_StatusDmn;
         objprc_inserirsdtdemandas.AV6ContratadaSrv_Codigo = aP4_ContratadaSrv_Codigo;
         objprc_inserirsdtdemandas.AV7DataSrvFat = aP5_DataSrvFat;
         objprc_inserirsdtdemandas.AV8ContratadaOrigem_Codigo = aP6_ContratadaOrigem_Codigo;
         objprc_inserirsdtdemandas.AV9PrazoEntrega = aP7_PrazoEntrega;
         objprc_inserirsdtdemandas.AV10Agrupador = aP8_Agrupador;
         objprc_inserirsdtdemandas.AV11Notificar = aP9_Notificar;
         objprc_inserirsdtdemandas.AV12ContratoServicosOS_Codigo = aP10_ContratoServicosOS_Codigo;
         objprc_inserirsdtdemandas.context.SetSubmitInitialConfig(context);
         objprc_inserirsdtdemandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_inserirsdtdemandas);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_inserirsdtdemandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_Codigo,(bool)AV3SrvParaFtr,(int)AV4Usuario_Codigo,(String)AV5StatusDmn,(int)AV6ContratadaSrv_Codigo,(String)AV7DataSrvFat,(int)AV8ContratadaOrigem_Codigo,(DateTime)AV9PrazoEntrega,(String)AV10Agrupador,(bool)AV11Notificar,(int)AV12ContratoServicosOS_Codigo} ;
         ClassLoader.Execute("aprc_inserirsdtdemandas","GeneXus.Programs.aprc_inserirsdtdemandas", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 11 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2Contratada_Codigo ;
      private int AV4Usuario_Codigo ;
      private int AV6ContratadaSrv_Codigo ;
      private int AV8ContratadaOrigem_Codigo ;
      private int AV12ContratoServicosOS_Codigo ;
      private String AV5StatusDmn ;
      private String AV7DataSrvFat ;
      private String AV10Agrupador ;
      private DateTime AV9PrazoEntrega ;
      private bool AV3SrvParaFtr ;
      private bool AV11Notificar ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
