/*
               File: WWPessoa
        Description:  Pessoa
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:32:41.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwpessoa : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwpessoa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwpessoa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavPessoa_tipopessoa1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavPessoa_tipopessoa2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavPessoa_tipopessoa3 = new GXCombobox();
         cmbPessoa_TipoPessoa = new GXCombobox();
         chkPessoa_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_97 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_97_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_97_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Pessoa_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pessoa_Nome1", AV17Pessoa_Nome1);
               AV44Pessoa_TipoPessoa1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Pessoa_TipoPessoa1", AV44Pessoa_TipoPessoa1);
               AV39Pessoa_Docto1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Pessoa_Docto1", AV39Pessoa_Docto1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22Pessoa_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Nome2", AV22Pessoa_Nome2);
               AV45Pessoa_TipoPessoa2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pessoa_TipoPessoa2", AV45Pessoa_TipoPessoa2);
               AV41Pessoa_Docto2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Pessoa_Docto2", AV41Pessoa_Docto2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27Pessoa_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pessoa_Nome3", AV27Pessoa_Nome3);
               AV46Pessoa_TipoPessoa3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Pessoa_TipoPessoa3", AV46Pessoa_TipoPessoa3);
               AV43Pessoa_Docto3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Pessoa_Docto3", AV43Pessoa_Docto3);
               AV69Usuario_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Usuario_PessoaCod), 6, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV71TFPessoa_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFPessoa_Nome", AV71TFPessoa_Nome);
               AV72TFPessoa_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFPessoa_Nome_Sel", AV72TFPessoa_Nome_Sel);
               AV79TFPessoa_Docto = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFPessoa_Docto", AV79TFPessoa_Docto);
               AV80TFPessoa_Docto_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFPessoa_Docto_Sel", AV80TFPessoa_Docto_Sel);
               AV97TFPessoa_IE = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFPessoa_IE", AV97TFPessoa_IE);
               AV98TFPessoa_IE_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFPessoa_IE_Sel", AV98TFPessoa_IE_Sel);
               AV101TFPessoa_Endereco = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFPessoa_Endereco", AV101TFPessoa_Endereco);
               AV102TFPessoa_Endereco_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102TFPessoa_Endereco_Sel", AV102TFPessoa_Endereco_Sel);
               AV105TFPessoa_MunicipioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFPessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0)));
               AV106TFPessoa_MunicipioCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFPessoa_MunicipioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0)));
               AV109TFPessoa_UF = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFPessoa_UF", AV109TFPessoa_UF);
               AV110TFPessoa_UF_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110TFPessoa_UF_Sel", AV110TFPessoa_UF_Sel);
               AV113TFPessoa_CEP = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFPessoa_CEP", AV113TFPessoa_CEP);
               AV114TFPessoa_CEP_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFPessoa_CEP_Sel", AV114TFPessoa_CEP_Sel);
               AV117TFPessoa_Telefone = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFPessoa_Telefone", AV117TFPessoa_Telefone);
               AV118TFPessoa_Telefone_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFPessoa_Telefone_Sel", AV118TFPessoa_Telefone_Sel);
               AV121TFPessoa_Fax = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFPessoa_Fax", AV121TFPessoa_Fax);
               AV122TFPessoa_Fax_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122TFPessoa_Fax_Sel", AV122TFPessoa_Fax_Sel);
               AV83TFPessoa_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFPessoa_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0));
               AV90ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV90ManageFiltersExecutionStep), 1, 0));
               AV73ddo_Pessoa_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Pessoa_NomeTitleControlIdToReplace", AV73ddo_Pessoa_NomeTitleControlIdToReplace);
               AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace", AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace);
               AV81ddo_Pessoa_DoctoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Pessoa_DoctoTitleControlIdToReplace", AV81ddo_Pessoa_DoctoTitleControlIdToReplace);
               AV99ddo_Pessoa_IETitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_Pessoa_IETitleControlIdToReplace", AV99ddo_Pessoa_IETitleControlIdToReplace);
               AV103ddo_Pessoa_EnderecoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103ddo_Pessoa_EnderecoTitleControlIdToReplace", AV103ddo_Pessoa_EnderecoTitleControlIdToReplace);
               AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace", AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace);
               AV111ddo_Pessoa_UFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ddo_Pessoa_UFTitleControlIdToReplace", AV111ddo_Pessoa_UFTitleControlIdToReplace);
               AV115ddo_Pessoa_CEPTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115ddo_Pessoa_CEPTitleControlIdToReplace", AV115ddo_Pessoa_CEPTitleControlIdToReplace);
               AV119ddo_Pessoa_TelefoneTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119ddo_Pessoa_TelefoneTitleControlIdToReplace", AV119ddo_Pessoa_TelefoneTitleControlIdToReplace);
               AV123ddo_Pessoa_FaxTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123ddo_Pessoa_FaxTitleControlIdToReplace", AV123ddo_Pessoa_FaxTitleControlIdToReplace);
               AV84ddo_Pessoa_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Pessoa_AtivoTitleControlIdToReplace", AV84ddo_Pessoa_AtivoTitleControlIdToReplace);
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A57Usuario_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV76TFPessoa_TipoPessoa_Sels);
               AV167Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A34Pessoa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA302( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START302( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117324263");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwpessoa.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_NOME1", StringUtil.RTrim( AV17Pessoa_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_TIPOPESSOA1", StringUtil.RTrim( AV44Pessoa_TipoPessoa1));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_DOCTO1", AV39Pessoa_Docto1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_NOME2", StringUtil.RTrim( AV22Pessoa_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_TIPOPESSOA2", StringUtil.RTrim( AV45Pessoa_TipoPessoa2));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_DOCTO2", AV41Pessoa_Docto2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_NOME3", StringUtil.RTrim( AV27Pessoa_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_TIPOPESSOA3", StringUtil.RTrim( AV46Pessoa_TipoPessoa3));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOA_DOCTO3", AV43Pessoa_Docto3);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69Usuario_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_NOME", StringUtil.RTrim( AV71TFPessoa_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_NOME_SEL", StringUtil.RTrim( AV72TFPessoa_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_DOCTO", AV79TFPessoa_Docto);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_DOCTO_SEL", AV80TFPessoa_Docto_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_IE", StringUtil.RTrim( AV97TFPessoa_IE));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_IE_SEL", StringUtil.RTrim( AV98TFPessoa_IE_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_ENDERECO", AV101TFPessoa_Endereco);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_ENDERECO_SEL", AV102TFPessoa_Endereco_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_MUNICIPIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_MUNICIPIOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_UF", StringUtil.RTrim( AV109TFPessoa_UF));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_UF_SEL", StringUtil.RTrim( AV110TFPessoa_UF_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_CEP", StringUtil.RTrim( AV113TFPessoa_CEP));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_CEP_SEL", StringUtil.RTrim( AV114TFPessoa_CEP_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_TELEFONE", StringUtil.RTrim( AV117TFPessoa_Telefone));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_TELEFONE_SEL", StringUtil.RTrim( AV118TFPessoa_Telefone_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_FAX", StringUtil.RTrim( AV121TFPessoa_Fax));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_FAX_SEL", StringUtil.RTrim( AV122TFPessoa_Fax_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_97", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_97), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV94ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV94ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV85DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV85DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_NOMETITLEFILTERDATA", AV70Pessoa_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_NOMETITLEFILTERDATA", AV70Pessoa_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_TIPOPESSOATITLEFILTERDATA", AV74Pessoa_TipoPessoaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_TIPOPESSOATITLEFILTERDATA", AV74Pessoa_TipoPessoaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_DOCTOTITLEFILTERDATA", AV78Pessoa_DoctoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_DOCTOTITLEFILTERDATA", AV78Pessoa_DoctoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_IETITLEFILTERDATA", AV96Pessoa_IETitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_IETITLEFILTERDATA", AV96Pessoa_IETitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_ENDERECOTITLEFILTERDATA", AV100Pessoa_EnderecoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_ENDERECOTITLEFILTERDATA", AV100Pessoa_EnderecoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_MUNICIPIOCODTITLEFILTERDATA", AV104Pessoa_MunicipioCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_MUNICIPIOCODTITLEFILTERDATA", AV104Pessoa_MunicipioCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_UFTITLEFILTERDATA", AV108Pessoa_UFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_UFTITLEFILTERDATA", AV108Pessoa_UFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_CEPTITLEFILTERDATA", AV112Pessoa_CEPTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_CEPTITLEFILTERDATA", AV112Pessoa_CEPTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_TELEFONETITLEFILTERDATA", AV116Pessoa_TelefoneTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_TELEFONETITLEFILTERDATA", AV116Pessoa_TelefoneTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_FAXTITLEFILTERDATA", AV120Pessoa_FaxTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_FAXTITLEFILTERDATA", AV120Pessoa_FaxTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_ATIVOTITLEFILTERDATA", AV82Pessoa_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_ATIVOTITLEFILTERDATA", AV82Pessoa_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFPESSOA_TIPOPESSOA_SELS", AV76TFPessoa_TipoPessoa_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFPESSOA_TIPOPESSOA_SELS", AV76TFPessoa_TipoPessoa_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV167Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Caption", StringUtil.RTrim( Ddo_pessoa_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Tooltip", StringUtil.RTrim( Ddo_pessoa_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Cls", StringUtil.RTrim( Ddo_pessoa_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Filtertype", StringUtil.RTrim( Ddo_pessoa_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Datalisttype", StringUtil.RTrim( Ddo_pessoa_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Datalistproc", StringUtil.RTrim( Ddo_pessoa_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Sortasc", StringUtil.RTrim( Ddo_pessoa_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Sortdsc", StringUtil.RTrim( Ddo_pessoa_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Loadingdata", StringUtil.RTrim( Ddo_pessoa_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Caption", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Tooltip", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Cls", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_tipopessoa_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_tipopessoa_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_tipopessoa_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_tipopessoa_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Datalisttype", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Allowmultipleselection", StringUtil.BoolToStr( Ddo_pessoa_tipopessoa_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Datalistfixedvalues", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Sortasc", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Sortdsc", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Caption", StringUtil.RTrim( Ddo_pessoa_docto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Tooltip", StringUtil.RTrim( Ddo_pessoa_docto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Cls", StringUtil.RTrim( Ddo_pessoa_docto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_docto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_docto_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_docto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_docto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_docto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_docto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_docto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_docto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Filtertype", StringUtil.RTrim( Ddo_pessoa_docto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_docto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_docto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Datalisttype", StringUtil.RTrim( Ddo_pessoa_docto_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Datalistproc", StringUtil.RTrim( Ddo_pessoa_docto_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_docto_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Sortasc", StringUtil.RTrim( Ddo_pessoa_docto_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Sortdsc", StringUtil.RTrim( Ddo_pessoa_docto_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Loadingdata", StringUtil.RTrim( Ddo_pessoa_docto_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_docto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_docto_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_docto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Caption", StringUtil.RTrim( Ddo_pessoa_ie_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Tooltip", StringUtil.RTrim( Ddo_pessoa_ie_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Cls", StringUtil.RTrim( Ddo_pessoa_ie_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_ie_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_ie_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_ie_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_ie_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_ie_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_ie_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_ie_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_ie_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filtertype", StringUtil.RTrim( Ddo_pessoa_ie_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_ie_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_ie_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Datalisttype", StringUtil.RTrim( Ddo_pessoa_ie_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Datalistproc", StringUtil.RTrim( Ddo_pessoa_ie_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_ie_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Sortasc", StringUtil.RTrim( Ddo_pessoa_ie_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Sortdsc", StringUtil.RTrim( Ddo_pessoa_ie_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Loadingdata", StringUtil.RTrim( Ddo_pessoa_ie_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_ie_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_ie_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_ie_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Caption", StringUtil.RTrim( Ddo_pessoa_endereco_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Tooltip", StringUtil.RTrim( Ddo_pessoa_endereco_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Cls", StringUtil.RTrim( Ddo_pessoa_endereco_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_endereco_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_endereco_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_endereco_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_endereco_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_endereco_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_endereco_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_endereco_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_endereco_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Filtertype", StringUtil.RTrim( Ddo_pessoa_endereco_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_endereco_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_endereco_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Datalisttype", StringUtil.RTrim( Ddo_pessoa_endereco_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Datalistproc", StringUtil.RTrim( Ddo_pessoa_endereco_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_endereco_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Sortasc", StringUtil.RTrim( Ddo_pessoa_endereco_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Sortdsc", StringUtil.RTrim( Ddo_pessoa_endereco_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Loadingdata", StringUtil.RTrim( Ddo_pessoa_endereco_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_endereco_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_endereco_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_endereco_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Caption", StringUtil.RTrim( Ddo_pessoa_municipiocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Tooltip", StringUtil.RTrim( Ddo_pessoa_municipiocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Cls", StringUtil.RTrim( Ddo_pessoa_municipiocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_municipiocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_pessoa_municipiocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_municipiocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_municipiocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_municipiocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_municipiocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_municipiocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_municipiocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Filtertype", StringUtil.RTrim( Ddo_pessoa_municipiocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_municipiocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_municipiocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Sortasc", StringUtil.RTrim( Ddo_pessoa_municipiocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Sortdsc", StringUtil.RTrim( Ddo_pessoa_municipiocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_municipiocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_pessoa_municipiocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Rangefilterto", StringUtil.RTrim( Ddo_pessoa_municipiocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_municipiocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Caption", StringUtil.RTrim( Ddo_pessoa_uf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Tooltip", StringUtil.RTrim( Ddo_pessoa_uf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Cls", StringUtil.RTrim( Ddo_pessoa_uf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_uf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_uf_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_uf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_uf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_uf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_uf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_uf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_uf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Filtertype", StringUtil.RTrim( Ddo_pessoa_uf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_uf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_uf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Datalisttype", StringUtil.RTrim( Ddo_pessoa_uf_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Datalistproc", StringUtil.RTrim( Ddo_pessoa_uf_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_uf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Sortasc", StringUtil.RTrim( Ddo_pessoa_uf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Sortdsc", StringUtil.RTrim( Ddo_pessoa_uf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Loadingdata", StringUtil.RTrim( Ddo_pessoa_uf_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_uf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_uf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_uf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Caption", StringUtil.RTrim( Ddo_pessoa_cep_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Tooltip", StringUtil.RTrim( Ddo_pessoa_cep_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Cls", StringUtil.RTrim( Ddo_pessoa_cep_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_cep_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_cep_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_cep_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_cep_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_cep_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_cep_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_cep_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_cep_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Filtertype", StringUtil.RTrim( Ddo_pessoa_cep_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_cep_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_cep_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Datalisttype", StringUtil.RTrim( Ddo_pessoa_cep_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Datalistproc", StringUtil.RTrim( Ddo_pessoa_cep_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_cep_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Sortasc", StringUtil.RTrim( Ddo_pessoa_cep_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Sortdsc", StringUtil.RTrim( Ddo_pessoa_cep_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Loadingdata", StringUtil.RTrim( Ddo_pessoa_cep_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_cep_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_cep_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_cep_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Caption", StringUtil.RTrim( Ddo_pessoa_telefone_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Tooltip", StringUtil.RTrim( Ddo_pessoa_telefone_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Cls", StringUtil.RTrim( Ddo_pessoa_telefone_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_telefone_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_telefone_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_telefone_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_telefone_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_telefone_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filtertype", StringUtil.RTrim( Ddo_pessoa_telefone_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_telefone_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Datalisttype", StringUtil.RTrim( Ddo_pessoa_telefone_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Datalistproc", StringUtil.RTrim( Ddo_pessoa_telefone_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_telefone_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Sortasc", StringUtil.RTrim( Ddo_pessoa_telefone_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Sortdsc", StringUtil.RTrim( Ddo_pessoa_telefone_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Loadingdata", StringUtil.RTrim( Ddo_pessoa_telefone_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_telefone_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_telefone_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_telefone_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Caption", StringUtil.RTrim( Ddo_pessoa_fax_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Tooltip", StringUtil.RTrim( Ddo_pessoa_fax_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Cls", StringUtil.RTrim( Ddo_pessoa_fax_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_fax_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_fax_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_fax_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_fax_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_fax_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_fax_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_fax_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_fax_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Filtertype", StringUtil.RTrim( Ddo_pessoa_fax_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_fax_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_fax_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Datalisttype", StringUtil.RTrim( Ddo_pessoa_fax_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Datalistproc", StringUtil.RTrim( Ddo_pessoa_fax_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_fax_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Sortasc", StringUtil.RTrim( Ddo_pessoa_fax_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Sortdsc", StringUtil.RTrim( Ddo_pessoa_fax_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Loadingdata", StringUtil.RTrim( Ddo_pessoa_fax_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_fax_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_fax_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_fax_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Caption", StringUtil.RTrim( Ddo_pessoa_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Tooltip", StringUtil.RTrim( Ddo_pessoa_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Cls", StringUtil.RTrim( Ddo_pessoa_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_pessoa_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_pessoa_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Sortasc", StringUtil.RTrim( Ddo_pessoa_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_pessoa_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TIPOPESSOA_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_tipopessoa_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_docto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_docto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_DOCTO_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_docto_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_ie_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_ie_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_ie_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_endereco_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_endereco_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ENDERECO_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_endereco_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_municipiocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_municipiocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_MUNICIPIOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_pessoa_municipiocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_uf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_uf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_UF_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_uf_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_cep_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_cep_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_CEP_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_cep_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_telefone_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_telefone_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_telefone_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_fax_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_fax_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_FAX_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_fax_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE302( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT302( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwpessoa.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWPessoa" ;
      }

      public override String GetPgmdesc( )
      {
         return " Pessoa" ;
      }

      protected void WB300( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_302( true) ;
         }
         else
         {
            wb_table1_2_302( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_302e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69Usuario_PessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV69Usuario_PessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavUsuario_pessoacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoa.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(118, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(119, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV90ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_nome_Internalname, StringUtil.RTrim( AV71TFPessoa_Nome), StringUtil.RTrim( context.localUtil.Format( AV71TFPessoa_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_nome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfpessoa_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_nome_sel_Internalname, StringUtil.RTrim( AV72TFPessoa_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV72TFPessoa_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_nome_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfpessoa_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_docto_Internalname, AV79TFPessoa_Docto, StringUtil.RTrim( context.localUtil.Format( AV79TFPessoa_Docto, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_docto_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_docto_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_docto_sel_Internalname, AV80TFPessoa_Docto_Sel, StringUtil.RTrim( context.localUtil.Format( AV80TFPessoa_Docto_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_docto_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_docto_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_ie_Internalname, StringUtil.RTrim( AV97TFPessoa_IE), StringUtil.RTrim( context.localUtil.Format( AV97TFPessoa_IE, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_ie_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_ie_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_ie_sel_Internalname, StringUtil.RTrim( AV98TFPessoa_IE_Sel), StringUtil.RTrim( context.localUtil.Format( AV98TFPessoa_IE_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_ie_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_ie_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_endereco_Internalname, AV101TFPessoa_Endereco, StringUtil.RTrim( context.localUtil.Format( AV101TFPessoa_Endereco, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_endereco_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_endereco_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_endereco_sel_Internalname, AV102TFPessoa_Endereco_Sel, StringUtil.RTrim( context.localUtil.Format( AV102TFPessoa_Endereco_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_endereco_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_endereco_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_municipiocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105TFPessoa_MunicipioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_municipiocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_municipiocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_municipiocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV106TFPessoa_MunicipioCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_municipiocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_municipiocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_uf_Internalname, StringUtil.RTrim( AV109TFPessoa_UF), StringUtil.RTrim( context.localUtil.Format( AV109TFPessoa_UF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_uf_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_uf_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_uf_sel_Internalname, StringUtil.RTrim( AV110TFPessoa_UF_Sel), StringUtil.RTrim( context.localUtil.Format( AV110TFPessoa_UF_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_uf_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_uf_sel_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_cep_Internalname, StringUtil.RTrim( AV113TFPessoa_CEP), StringUtil.RTrim( context.localUtil.Format( AV113TFPessoa_CEP, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_cep_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_cep_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_cep_sel_Internalname, StringUtil.RTrim( AV114TFPessoa_CEP_Sel), StringUtil.RTrim( context.localUtil.Format( AV114TFPessoa_CEP_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_cep_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_cep_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_telefone_Internalname, StringUtil.RTrim( AV117TFPessoa_Telefone), StringUtil.RTrim( context.localUtil.Format( AV117TFPessoa_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_telefone_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_telefone_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_telefone_sel_Internalname, StringUtil.RTrim( AV118TFPessoa_Telefone_Sel), StringUtil.RTrim( context.localUtil.Format( AV118TFPessoa_Telefone_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_telefone_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_telefone_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_fax_Internalname, StringUtil.RTrim( AV121TFPessoa_Fax), StringUtil.RTrim( context.localUtil.Format( AV121TFPessoa_Fax, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_fax_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_fax_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_fax_sel_Internalname, StringUtil.RTrim( AV122TFPessoa_Fax_Sel), StringUtil.RTrim( context.localUtil.Format( AV122TFPessoa_Fax_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_fax_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_fax_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV83TFPessoa_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_nometitlecontrolidtoreplace_Internalname, AV73ddo_Pessoa_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,141);\"", 0, edtavDdo_pessoa_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_TIPOPESSOAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Internalname, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", 0, edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_DOCTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_doctotitlecontrolidtoreplace_Internalname, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", 0, edtavDdo_pessoa_doctotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_IEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname, AV99ddo_Pessoa_IETitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", 0, edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_ENDERECOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Internalname, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", 0, edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_MUNICIPIOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Internalname, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", 0, edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_UFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_uftitlecontrolidtoreplace_Internalname, AV111ddo_Pessoa_UFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", 0, edtavDdo_pessoa_uftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_CEPContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_ceptitlecontrolidtoreplace_Internalname, AV115ddo_Pessoa_CEPTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_pessoa_ceptitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_TELEFONEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_FAXContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_faxtitlecontrolidtoreplace_Internalname, AV123ddo_Pessoa_FaxTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", 0, edtavDdo_pessoa_faxtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'" + sGXsfl_97_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_ativotitlecontrolidtoreplace_Internalname, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,161);\"", 0, edtavDdo_pessoa_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoa.htm");
         }
         wbLoad = true;
      }

      protected void START302( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Pessoa", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP300( ) ;
      }

      protected void WS302( )
      {
         START302( ) ;
         EVT302( ) ;
      }

      protected void EVT302( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11302 */
                              E11302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12302 */
                              E12302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13302 */
                              E13302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_TIPOPESSOA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14302 */
                              E14302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_DOCTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15302 */
                              E15302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_IE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16302 */
                              E16302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_ENDERECO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17302 */
                              E17302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_MUNICIPIOCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18302 */
                              E18302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_UF.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19302 */
                              E19302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_CEP.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20302 */
                              E20302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_TELEFONE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21302 */
                              E21302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_FAX.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22302 */
                              E22302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23302 */
                              E23302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24302 */
                              E24302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25302 */
                              E25302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26302 */
                              E26302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27302 */
                              E27302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28302 */
                              E28302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29302 */
                              E29302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E30302 */
                              E30302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E31302 */
                              E31302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E32302 */
                              E32302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E33302 */
                              E33302 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_97_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
                              SubsflControlProps_972( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV164Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV165Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV89Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV89Display)) ? AV166Display_GXI : context.convertURL( context.PathToRelativeUrl( AV89Display))));
                              A34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPessoa_Codigo_Internalname), ",", "."));
                              A35Pessoa_Nome = StringUtil.Upper( cgiGet( edtPessoa_Nome_Internalname));
                              cmbPessoa_TipoPessoa.Name = cmbPessoa_TipoPessoa_Internalname;
                              cmbPessoa_TipoPessoa.CurrentValue = cgiGet( cmbPessoa_TipoPessoa_Internalname);
                              A36Pessoa_TipoPessoa = cgiGet( cmbPessoa_TipoPessoa_Internalname);
                              A37Pessoa_Docto = cgiGet( edtPessoa_Docto_Internalname);
                              A518Pessoa_IE = cgiGet( edtPessoa_IE_Internalname);
                              n518Pessoa_IE = false;
                              A519Pessoa_Endereco = cgiGet( edtPessoa_Endereco_Internalname);
                              n519Pessoa_Endereco = false;
                              A503Pessoa_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( edtPessoa_MunicipioCod_Internalname), ",", "."));
                              n503Pessoa_MunicipioCod = false;
                              A520Pessoa_UF = StringUtil.Upper( cgiGet( edtPessoa_UF_Internalname));
                              n520Pessoa_UF = false;
                              A521Pessoa_CEP = cgiGet( edtPessoa_CEP_Internalname);
                              n521Pessoa_CEP = false;
                              A522Pessoa_Telefone = cgiGet( edtPessoa_Telefone_Internalname);
                              n522Pessoa_Telefone = false;
                              A523Pessoa_Fax = cgiGet( edtPessoa_Fax_Internalname);
                              n523Pessoa_Fax = false;
                              A38Pessoa_Ativo = StringUtil.StrToBool( cgiGet( chkPessoa_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E34302 */
                                    E34302 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E35302 */
                                    E35302 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E36302 */
                                    E36302 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_NOME1"), AV17Pessoa_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_tipopessoa1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_TIPOPESSOA1"), AV44Pessoa_TipoPessoa1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_docto1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_DOCTO1"), AV39Pessoa_Docto1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_NOME2"), AV22Pessoa_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_tipopessoa2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_TIPOPESSOA2"), AV45Pessoa_TipoPessoa2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_docto2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_DOCTO2"), AV41Pessoa_Docto2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_NOME3"), AV27Pessoa_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_tipopessoa3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_TIPOPESSOA3"), AV46Pessoa_TipoPessoa3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoa_docto3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_DOCTO3"), AV43Pessoa_Docto3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoacod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vUSUARIO_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV69Usuario_PessoaCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_NOME"), AV71TFPessoa_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_NOME_SEL"), AV72TFPessoa_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_docto Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_DOCTO"), AV79TFPessoa_Docto) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_docto_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_DOCTO_SEL"), AV80TFPessoa_Docto_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_ie Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE"), AV97TFPessoa_IE) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_ie_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE_SEL"), AV98TFPessoa_IE_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_endereco Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_ENDERECO"), AV101TFPessoa_Endereco) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_endereco_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_ENDERECO_SEL"), AV102TFPessoa_Endereco_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_municipiocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPESSOA_MUNICIPIOCOD"), ",", ".") != Convert.ToDecimal( AV105TFPessoa_MunicipioCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_municipiocod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPESSOA_MUNICIPIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV106TFPessoa_MunicipioCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_uf Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_UF"), AV109TFPessoa_UF) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_uf_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_UF_SEL"), AV110TFPessoa_UF_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_cep Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_CEP"), AV113TFPessoa_CEP) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_cep_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_CEP_SEL"), AV114TFPessoa_CEP_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_telefone Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE"), AV117TFPessoa_Telefone) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_telefone_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE_SEL"), AV118TFPessoa_Telefone_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_fax Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_FAX"), AV121TFPessoa_Fax) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_fax_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_FAX_SEL"), AV122TFPessoa_Fax_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPESSOA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV83TFPessoa_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE302( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA302( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PESSOA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("PESSOA_TIPOPESSOA", "Tipo", 0);
            cmbavDynamicfiltersselector1.addItem("PESSOA_DOCTO", "Documento", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavPessoa_tipopessoa1.Name = "vPESSOA_TIPOPESSOA1";
            cmbavPessoa_tipopessoa1.WebTags = "";
            cmbavPessoa_tipopessoa1.addItem("", "Todos", 0);
            cmbavPessoa_tipopessoa1.addItem("F", "F�sica", 0);
            cmbavPessoa_tipopessoa1.addItem("J", "Jur�dica", 0);
            if ( cmbavPessoa_tipopessoa1.ItemCount > 0 )
            {
               AV44Pessoa_TipoPessoa1 = cmbavPessoa_tipopessoa1.getValidValue(AV44Pessoa_TipoPessoa1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Pessoa_TipoPessoa1", AV44Pessoa_TipoPessoa1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PESSOA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("PESSOA_TIPOPESSOA", "Tipo", 0);
            cmbavDynamicfiltersselector2.addItem("PESSOA_DOCTO", "Documento", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavPessoa_tipopessoa2.Name = "vPESSOA_TIPOPESSOA2";
            cmbavPessoa_tipopessoa2.WebTags = "";
            cmbavPessoa_tipopessoa2.addItem("", "Todos", 0);
            cmbavPessoa_tipopessoa2.addItem("F", "F�sica", 0);
            cmbavPessoa_tipopessoa2.addItem("J", "Jur�dica", 0);
            if ( cmbavPessoa_tipopessoa2.ItemCount > 0 )
            {
               AV45Pessoa_TipoPessoa2 = cmbavPessoa_tipopessoa2.getValidValue(AV45Pessoa_TipoPessoa2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pessoa_TipoPessoa2", AV45Pessoa_TipoPessoa2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PESSOA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("PESSOA_TIPOPESSOA", "Tipo", 0);
            cmbavDynamicfiltersselector3.addItem("PESSOA_DOCTO", "Documento", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            cmbavPessoa_tipopessoa3.Name = "vPESSOA_TIPOPESSOA3";
            cmbavPessoa_tipopessoa3.WebTags = "";
            cmbavPessoa_tipopessoa3.addItem("", "Todos", 0);
            cmbavPessoa_tipopessoa3.addItem("F", "F�sica", 0);
            cmbavPessoa_tipopessoa3.addItem("J", "Jur�dica", 0);
            if ( cmbavPessoa_tipopessoa3.ItemCount > 0 )
            {
               AV46Pessoa_TipoPessoa3 = cmbavPessoa_tipopessoa3.getValidValue(AV46Pessoa_TipoPessoa3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Pessoa_TipoPessoa3", AV46Pessoa_TipoPessoa3);
            }
            GXCCtl = "PESSOA_TIPOPESSOA_" + sGXsfl_97_idx;
            cmbPessoa_TipoPessoa.Name = GXCCtl;
            cmbPessoa_TipoPessoa.WebTags = "";
            cmbPessoa_TipoPessoa.addItem("", "(Nenhum)", 0);
            cmbPessoa_TipoPessoa.addItem("F", "F�sica", 0);
            cmbPessoa_TipoPessoa.addItem("J", "Jur�dica", 0);
            if ( cmbPessoa_TipoPessoa.ItemCount > 0 )
            {
               A36Pessoa_TipoPessoa = cmbPessoa_TipoPessoa.getValidValue(A36Pessoa_TipoPessoa);
            }
            GXCCtl = "PESSOA_ATIVO_" + sGXsfl_97_idx;
            chkPessoa_Ativo.Name = GXCCtl;
            chkPessoa_Ativo.WebTags = "";
            chkPessoa_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPessoa_Ativo_Internalname, "TitleCaption", chkPessoa_Ativo.Caption);
            chkPessoa_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_972( ) ;
         while ( nGXsfl_97_idx <= nRC_GXsfl_97 )
         {
            sendrow_972( ) ;
            nGXsfl_97_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_97_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_97_idx+1));
            sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
            SubsflControlProps_972( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Pessoa_Nome1 ,
                                       String AV44Pessoa_TipoPessoa1 ,
                                       String AV39Pessoa_Docto1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22Pessoa_Nome2 ,
                                       String AV45Pessoa_TipoPessoa2 ,
                                       String AV41Pessoa_Docto2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27Pessoa_Nome3 ,
                                       String AV46Pessoa_TipoPessoa3 ,
                                       String AV43Pessoa_Docto3 ,
                                       int AV69Usuario_PessoaCod ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV71TFPessoa_Nome ,
                                       String AV72TFPessoa_Nome_Sel ,
                                       String AV79TFPessoa_Docto ,
                                       String AV80TFPessoa_Docto_Sel ,
                                       String AV97TFPessoa_IE ,
                                       String AV98TFPessoa_IE_Sel ,
                                       String AV101TFPessoa_Endereco ,
                                       String AV102TFPessoa_Endereco_Sel ,
                                       int AV105TFPessoa_MunicipioCod ,
                                       int AV106TFPessoa_MunicipioCod_To ,
                                       String AV109TFPessoa_UF ,
                                       String AV110TFPessoa_UF_Sel ,
                                       String AV113TFPessoa_CEP ,
                                       String AV114TFPessoa_CEP_Sel ,
                                       String AV117TFPessoa_Telefone ,
                                       String AV118TFPessoa_Telefone_Sel ,
                                       String AV121TFPessoa_Fax ,
                                       String AV122TFPessoa_Fax_Sel ,
                                       short AV83TFPessoa_Ativo_Sel ,
                                       short AV90ManageFiltersExecutionStep ,
                                       String AV73ddo_Pessoa_NomeTitleControlIdToReplace ,
                                       String AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace ,
                                       String AV81ddo_Pessoa_DoctoTitleControlIdToReplace ,
                                       String AV99ddo_Pessoa_IETitleControlIdToReplace ,
                                       String AV103ddo_Pessoa_EnderecoTitleControlIdToReplace ,
                                       String AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace ,
                                       String AV111ddo_Pessoa_UFTitleControlIdToReplace ,
                                       String AV115ddo_Pessoa_CEPTitleControlIdToReplace ,
                                       String AV119ddo_Pessoa_TelefoneTitleControlIdToReplace ,
                                       String AV123ddo_Pessoa_FaxTitleControlIdToReplace ,
                                       String AV84ddo_Pessoa_AtivoTitleControlIdToReplace ,
                                       int A1Usuario_Codigo ,
                                       int A57Usuario_PessoaCod ,
                                       IGxCollection AV76TFPessoa_TipoPessoa_Sels ,
                                       String AV167Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A34Pessoa_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF302( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PESSOA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A34Pessoa_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "PESSOA_NOME", StringUtil.RTrim( A35Pessoa_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_TIPOPESSOA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A36Pessoa_TipoPessoa, ""))));
         GxWebStd.gx_hidden_field( context, "PESSOA_TIPOPESSOA", StringUtil.RTrim( A36Pessoa_TipoPessoa));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_DOCTO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A37Pessoa_Docto, ""))));
         GxWebStd.gx_hidden_field( context, "PESSOA_DOCTO", A37Pessoa_Docto);
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_IE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, ""))));
         GxWebStd.gx_hidden_field( context, "PESSOA_IE", StringUtil.RTrim( A518Pessoa_IE));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_ENDERECO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, ""))));
         GxWebStd.gx_hidden_field( context, "PESSOA_ENDERECO", A519Pessoa_Endereco);
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_MUNICIPIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A503Pessoa_MunicipioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PESSOA_MUNICIPIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A503Pessoa_MunicipioCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_CEP", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, ""))));
         GxWebStd.gx_hidden_field( context, "PESSOA_CEP", StringUtil.RTrim( A521Pessoa_CEP));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_TELEFONE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, ""))));
         GxWebStd.gx_hidden_field( context, "PESSOA_TELEFONE", StringUtil.RTrim( A522Pessoa_Telefone));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_FAX", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, ""))));
         GxWebStd.gx_hidden_field( context, "PESSOA_FAX", StringUtil.RTrim( A523Pessoa_Fax));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_ATIVO", GetSecureSignedToken( "", A38Pessoa_Ativo));
         GxWebStd.gx_hidden_field( context, "PESSOA_ATIVO", StringUtil.BoolToStr( A38Pessoa_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavPessoa_tipopessoa1.ItemCount > 0 )
         {
            AV44Pessoa_TipoPessoa1 = cmbavPessoa_tipopessoa1.getValidValue(AV44Pessoa_TipoPessoa1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Pessoa_TipoPessoa1", AV44Pessoa_TipoPessoa1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavPessoa_tipopessoa2.ItemCount > 0 )
         {
            AV45Pessoa_TipoPessoa2 = cmbavPessoa_tipopessoa2.getValidValue(AV45Pessoa_TipoPessoa2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pessoa_TipoPessoa2", AV45Pessoa_TipoPessoa2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavPessoa_tipopessoa3.ItemCount > 0 )
         {
            AV46Pessoa_TipoPessoa3 = cmbavPessoa_tipopessoa3.getValidValue(AV46Pessoa_TipoPessoa3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Pessoa_TipoPessoa3", AV46Pessoa_TipoPessoa3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF302( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV167Pgmname = "WWPessoa";
         context.Gx_err = 0;
      }

      protected void RF302( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 97;
         /* Execute user event: E35302 */
         E35302 ();
         nGXsfl_97_idx = 1;
         sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
         SubsflControlProps_972( ) ;
         nGXsfl_97_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_972( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A36Pessoa_TipoPessoa ,
                                                 AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                                 AV127WWPessoaDS_1_Dynamicfiltersselector1 ,
                                                 AV128WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                                 AV129WWPessoaDS_3_Pessoa_nome1 ,
                                                 AV130WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                                 AV131WWPessoaDS_5_Pessoa_docto1 ,
                                                 AV132WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                                 AV133WWPessoaDS_7_Dynamicfiltersselector2 ,
                                                 AV134WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                                 AV135WWPessoaDS_9_Pessoa_nome2 ,
                                                 AV136WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                                 AV137WWPessoaDS_11_Pessoa_docto2 ,
                                                 AV138WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                                 AV139WWPessoaDS_13_Dynamicfiltersselector3 ,
                                                 AV140WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                                 AV141WWPessoaDS_15_Pessoa_nome3 ,
                                                 AV142WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                                 AV143WWPessoaDS_17_Pessoa_docto3 ,
                                                 AV145WWPessoaDS_19_Tfpessoa_nome_sel ,
                                                 AV144WWPessoaDS_18_Tfpessoa_nome ,
                                                 AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                                 AV148WWPessoaDS_22_Tfpessoa_docto_sel ,
                                                 AV147WWPessoaDS_21_Tfpessoa_docto ,
                                                 AV150WWPessoaDS_24_Tfpessoa_ie_sel ,
                                                 AV149WWPessoaDS_23_Tfpessoa_ie ,
                                                 AV152WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                                 AV151WWPessoaDS_25_Tfpessoa_endereco ,
                                                 AV153WWPessoaDS_27_Tfpessoa_municipiocod ,
                                                 AV154WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                                 AV156WWPessoaDS_30_Tfpessoa_uf_sel ,
                                                 AV155WWPessoaDS_29_Tfpessoa_uf ,
                                                 AV158WWPessoaDS_32_Tfpessoa_cep_sel ,
                                                 AV157WWPessoaDS_31_Tfpessoa_cep ,
                                                 AV160WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                                 AV159WWPessoaDS_33_Tfpessoa_telefone ,
                                                 AV162WWPessoaDS_36_Tfpessoa_fax_sel ,
                                                 AV161WWPessoaDS_35_Tfpessoa_fax ,
                                                 AV163WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                                 AV69Usuario_PessoaCod ,
                                                 A35Pessoa_Nome ,
                                                 A37Pessoa_Docto ,
                                                 A518Pessoa_IE ,
                                                 A519Pessoa_Endereco ,
                                                 A503Pessoa_MunicipioCod ,
                                                 A520Pessoa_UF ,
                                                 A521Pessoa_CEP ,
                                                 A522Pessoa_Telefone ,
                                                 A523Pessoa_Fax ,
                                                 A38Pessoa_Ativo ,
                                                 A34Pessoa_Codigo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV129WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1), 100, "%");
            lV129WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1), 100, "%");
            lV131WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1), "%", "");
            lV131WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1), "%", "");
            lV135WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2), 100, "%");
            lV135WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2), 100, "%");
            lV137WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2), "%", "");
            lV137WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2), "%", "");
            lV141WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3), 100, "%");
            lV141WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3), 100, "%");
            lV143WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3), "%", "");
            lV143WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3), "%", "");
            lV144WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV144WWPessoaDS_18_Tfpessoa_nome), 100, "%");
            lV147WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV147WWPessoaDS_21_Tfpessoa_docto), "%", "");
            lV149WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV149WWPessoaDS_23_Tfpessoa_ie), 15, "%");
            lV151WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV151WWPessoaDS_25_Tfpessoa_endereco), "%", "");
            lV155WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV155WWPessoaDS_29_Tfpessoa_uf), 2, "%");
            lV157WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV157WWPessoaDS_31_Tfpessoa_cep), 10, "%");
            lV159WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV159WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
            lV161WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV161WWPessoaDS_35_Tfpessoa_fax), 15, "%");
            /* Using cursor H00302 */
            pr_default.execute(0, new Object[] {lV129WWPessoaDS_3_Pessoa_nome1, lV129WWPessoaDS_3_Pessoa_nome1, AV130WWPessoaDS_4_Pessoa_tipopessoa1, lV131WWPessoaDS_5_Pessoa_docto1, lV131WWPessoaDS_5_Pessoa_docto1, lV135WWPessoaDS_9_Pessoa_nome2, lV135WWPessoaDS_9_Pessoa_nome2, AV136WWPessoaDS_10_Pessoa_tipopessoa2, lV137WWPessoaDS_11_Pessoa_docto2, lV137WWPessoaDS_11_Pessoa_docto2, lV141WWPessoaDS_15_Pessoa_nome3, lV141WWPessoaDS_15_Pessoa_nome3, AV142WWPessoaDS_16_Pessoa_tipopessoa3, lV143WWPessoaDS_17_Pessoa_docto3, lV143WWPessoaDS_17_Pessoa_docto3, lV144WWPessoaDS_18_Tfpessoa_nome, AV145WWPessoaDS_19_Tfpessoa_nome_sel, lV147WWPessoaDS_21_Tfpessoa_docto, AV148WWPessoaDS_22_Tfpessoa_docto_sel, lV149WWPessoaDS_23_Tfpessoa_ie, AV150WWPessoaDS_24_Tfpessoa_ie_sel, lV151WWPessoaDS_25_Tfpessoa_endereco, AV152WWPessoaDS_26_Tfpessoa_endereco_sel, AV153WWPessoaDS_27_Tfpessoa_municipiocod, AV154WWPessoaDS_28_Tfpessoa_municipiocod_to, lV155WWPessoaDS_29_Tfpessoa_uf, AV156WWPessoaDS_30_Tfpessoa_uf_sel, lV157WWPessoaDS_31_Tfpessoa_cep, AV158WWPessoaDS_32_Tfpessoa_cep_sel, lV159WWPessoaDS_33_Tfpessoa_telefone, AV160WWPessoaDS_34_Tfpessoa_telefone_sel, lV161WWPessoaDS_35_Tfpessoa_fax, AV162WWPessoaDS_36_Tfpessoa_fax_sel, AV69Usuario_PessoaCod, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_97_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A38Pessoa_Ativo = H00302_A38Pessoa_Ativo[0];
               A523Pessoa_Fax = H00302_A523Pessoa_Fax[0];
               n523Pessoa_Fax = H00302_n523Pessoa_Fax[0];
               A522Pessoa_Telefone = H00302_A522Pessoa_Telefone[0];
               n522Pessoa_Telefone = H00302_n522Pessoa_Telefone[0];
               A521Pessoa_CEP = H00302_A521Pessoa_CEP[0];
               n521Pessoa_CEP = H00302_n521Pessoa_CEP[0];
               A520Pessoa_UF = H00302_A520Pessoa_UF[0];
               n520Pessoa_UF = H00302_n520Pessoa_UF[0];
               A503Pessoa_MunicipioCod = H00302_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = H00302_n503Pessoa_MunicipioCod[0];
               A519Pessoa_Endereco = H00302_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = H00302_n519Pessoa_Endereco[0];
               A518Pessoa_IE = H00302_A518Pessoa_IE[0];
               n518Pessoa_IE = H00302_n518Pessoa_IE[0];
               A37Pessoa_Docto = H00302_A37Pessoa_Docto[0];
               A36Pessoa_TipoPessoa = H00302_A36Pessoa_TipoPessoa[0];
               A35Pessoa_Nome = H00302_A35Pessoa_Nome[0];
               A34Pessoa_Codigo = H00302_A34Pessoa_Codigo[0];
               A520Pessoa_UF = H00302_A520Pessoa_UF[0];
               n520Pessoa_UF = H00302_n520Pessoa_UF[0];
               /* Execute user event: E36302 */
               E36302 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 97;
            WB300( ) ;
         }
         nGXsfl_97_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV128WWPessoaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV129WWPessoaDS_3_Pessoa_nome1 = AV17Pessoa_Nome1;
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = AV44Pessoa_TipoPessoa1;
         AV131WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV132WWPessoaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV134WWPessoaDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV135WWPessoaDS_9_Pessoa_nome2 = AV22Pessoa_Nome2;
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = AV45Pessoa_TipoPessoa2;
         AV137WWPessoaDS_11_Pessoa_docto2 = AV41Pessoa_Docto2;
         AV138WWPessoaDS_12_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV140WWPessoaDS_14_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV141WWPessoaDS_15_Pessoa_nome3 = AV27Pessoa_Nome3;
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = AV46Pessoa_TipoPessoa3;
         AV143WWPessoaDS_17_Pessoa_docto3 = AV43Pessoa_Docto3;
         AV144WWPessoaDS_18_Tfpessoa_nome = AV71TFPessoa_Nome;
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = AV72TFPessoa_Nome_Sel;
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV76TFPessoa_TipoPessoa_Sels;
         AV147WWPessoaDS_21_Tfpessoa_docto = AV79TFPessoa_Docto;
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = AV80TFPessoa_Docto_Sel;
         AV149WWPessoaDS_23_Tfpessoa_ie = AV97TFPessoa_IE;
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = AV98TFPessoa_IE_Sel;
         AV151WWPessoaDS_25_Tfpessoa_endereco = AV101TFPessoa_Endereco;
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = AV102TFPessoa_Endereco_Sel;
         AV153WWPessoaDS_27_Tfpessoa_municipiocod = AV105TFPessoa_MunicipioCod;
         AV154WWPessoaDS_28_Tfpessoa_municipiocod_to = AV106TFPessoa_MunicipioCod_To;
         AV155WWPessoaDS_29_Tfpessoa_uf = AV109TFPessoa_UF;
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = AV110TFPessoa_UF_Sel;
         AV157WWPessoaDS_31_Tfpessoa_cep = AV113TFPessoa_CEP;
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = AV114TFPessoa_CEP_Sel;
         AV159WWPessoaDS_33_Tfpessoa_telefone = AV117TFPessoa_Telefone;
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = AV118TFPessoa_Telefone_Sel;
         AV161WWPessoaDS_35_Tfpessoa_fax = AV121TFPessoa_Fax;
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = AV122TFPessoa_Fax_Sel;
         AV163WWPessoaDS_37_Tfpessoa_ativo_sel = AV83TFPessoa_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV127WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV128WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV129WWPessoaDS_3_Pessoa_nome1 ,
                                              AV130WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV131WWPessoaDS_5_Pessoa_docto1 ,
                                              AV132WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV133WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV134WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV135WWPessoaDS_9_Pessoa_nome2 ,
                                              AV136WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV137WWPessoaDS_11_Pessoa_docto2 ,
                                              AV138WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV139WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV140WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV141WWPessoaDS_15_Pessoa_nome3 ,
                                              AV142WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV143WWPessoaDS_17_Pessoa_docto3 ,
                                              AV145WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV144WWPessoaDS_18_Tfpessoa_nome ,
                                              AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV148WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV147WWPessoaDS_21_Tfpessoa_docto ,
                                              AV150WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV149WWPessoaDS_23_Tfpessoa_ie ,
                                              AV152WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV151WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV153WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV154WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV156WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV155WWPessoaDS_29_Tfpessoa_uf ,
                                              AV158WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV157WWPessoaDS_31_Tfpessoa_cep ,
                                              AV160WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV159WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV162WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV161WWPessoaDS_35_Tfpessoa_fax ,
                                              AV163WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV69Usuario_PessoaCod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV129WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV129WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV131WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV131WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV135WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV135WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV137WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV137WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV141WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV141WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV143WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV143WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV144WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV144WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV147WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV147WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV149WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV149WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV151WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV151WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV155WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV155WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV157WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV157WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV159WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV159WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV161WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV161WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor H00303 */
         pr_default.execute(1, new Object[] {lV129WWPessoaDS_3_Pessoa_nome1, lV129WWPessoaDS_3_Pessoa_nome1, AV130WWPessoaDS_4_Pessoa_tipopessoa1, lV131WWPessoaDS_5_Pessoa_docto1, lV131WWPessoaDS_5_Pessoa_docto1, lV135WWPessoaDS_9_Pessoa_nome2, lV135WWPessoaDS_9_Pessoa_nome2, AV136WWPessoaDS_10_Pessoa_tipopessoa2, lV137WWPessoaDS_11_Pessoa_docto2, lV137WWPessoaDS_11_Pessoa_docto2, lV141WWPessoaDS_15_Pessoa_nome3, lV141WWPessoaDS_15_Pessoa_nome3, AV142WWPessoaDS_16_Pessoa_tipopessoa3, lV143WWPessoaDS_17_Pessoa_docto3, lV143WWPessoaDS_17_Pessoa_docto3, lV144WWPessoaDS_18_Tfpessoa_nome, AV145WWPessoaDS_19_Tfpessoa_nome_sel, lV147WWPessoaDS_21_Tfpessoa_docto, AV148WWPessoaDS_22_Tfpessoa_docto_sel, lV149WWPessoaDS_23_Tfpessoa_ie, AV150WWPessoaDS_24_Tfpessoa_ie_sel, lV151WWPessoaDS_25_Tfpessoa_endereco, AV152WWPessoaDS_26_Tfpessoa_endereco_sel, AV153WWPessoaDS_27_Tfpessoa_municipiocod, AV154WWPessoaDS_28_Tfpessoa_municipiocod_to, lV155WWPessoaDS_29_Tfpessoa_uf, AV156WWPessoaDS_30_Tfpessoa_uf_sel, lV157WWPessoaDS_31_Tfpessoa_cep, AV158WWPessoaDS_32_Tfpessoa_cep_sel, lV159WWPessoaDS_33_Tfpessoa_telefone, AV160WWPessoaDS_34_Tfpessoa_telefone_sel, lV161WWPessoaDS_35_Tfpessoa_fax, AV162WWPessoaDS_36_Tfpessoa_fax_sel, AV69Usuario_PessoaCod});
         GRID_nRecordCount = H00303_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV128WWPessoaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV129WWPessoaDS_3_Pessoa_nome1 = AV17Pessoa_Nome1;
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = AV44Pessoa_TipoPessoa1;
         AV131WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV132WWPessoaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV134WWPessoaDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV135WWPessoaDS_9_Pessoa_nome2 = AV22Pessoa_Nome2;
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = AV45Pessoa_TipoPessoa2;
         AV137WWPessoaDS_11_Pessoa_docto2 = AV41Pessoa_Docto2;
         AV138WWPessoaDS_12_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV140WWPessoaDS_14_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV141WWPessoaDS_15_Pessoa_nome3 = AV27Pessoa_Nome3;
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = AV46Pessoa_TipoPessoa3;
         AV143WWPessoaDS_17_Pessoa_docto3 = AV43Pessoa_Docto3;
         AV144WWPessoaDS_18_Tfpessoa_nome = AV71TFPessoa_Nome;
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = AV72TFPessoa_Nome_Sel;
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV76TFPessoa_TipoPessoa_Sels;
         AV147WWPessoaDS_21_Tfpessoa_docto = AV79TFPessoa_Docto;
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = AV80TFPessoa_Docto_Sel;
         AV149WWPessoaDS_23_Tfpessoa_ie = AV97TFPessoa_IE;
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = AV98TFPessoa_IE_Sel;
         AV151WWPessoaDS_25_Tfpessoa_endereco = AV101TFPessoa_Endereco;
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = AV102TFPessoa_Endereco_Sel;
         AV153WWPessoaDS_27_Tfpessoa_municipiocod = AV105TFPessoa_MunicipioCod;
         AV154WWPessoaDS_28_Tfpessoa_municipiocod_to = AV106TFPessoa_MunicipioCod_To;
         AV155WWPessoaDS_29_Tfpessoa_uf = AV109TFPessoa_UF;
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = AV110TFPessoa_UF_Sel;
         AV157WWPessoaDS_31_Tfpessoa_cep = AV113TFPessoa_CEP;
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = AV114TFPessoa_CEP_Sel;
         AV159WWPessoaDS_33_Tfpessoa_telefone = AV117TFPessoa_Telefone;
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = AV118TFPessoa_Telefone_Sel;
         AV161WWPessoaDS_35_Tfpessoa_fax = AV121TFPessoa_Fax;
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = AV122TFPessoa_Fax_Sel;
         AV163WWPessoaDS_37_Tfpessoa_ativo_sel = AV83TFPessoa_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV128WWPessoaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV129WWPessoaDS_3_Pessoa_nome1 = AV17Pessoa_Nome1;
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = AV44Pessoa_TipoPessoa1;
         AV131WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV132WWPessoaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV134WWPessoaDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV135WWPessoaDS_9_Pessoa_nome2 = AV22Pessoa_Nome2;
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = AV45Pessoa_TipoPessoa2;
         AV137WWPessoaDS_11_Pessoa_docto2 = AV41Pessoa_Docto2;
         AV138WWPessoaDS_12_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV140WWPessoaDS_14_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV141WWPessoaDS_15_Pessoa_nome3 = AV27Pessoa_Nome3;
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = AV46Pessoa_TipoPessoa3;
         AV143WWPessoaDS_17_Pessoa_docto3 = AV43Pessoa_Docto3;
         AV144WWPessoaDS_18_Tfpessoa_nome = AV71TFPessoa_Nome;
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = AV72TFPessoa_Nome_Sel;
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV76TFPessoa_TipoPessoa_Sels;
         AV147WWPessoaDS_21_Tfpessoa_docto = AV79TFPessoa_Docto;
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = AV80TFPessoa_Docto_Sel;
         AV149WWPessoaDS_23_Tfpessoa_ie = AV97TFPessoa_IE;
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = AV98TFPessoa_IE_Sel;
         AV151WWPessoaDS_25_Tfpessoa_endereco = AV101TFPessoa_Endereco;
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = AV102TFPessoa_Endereco_Sel;
         AV153WWPessoaDS_27_Tfpessoa_municipiocod = AV105TFPessoa_MunicipioCod;
         AV154WWPessoaDS_28_Tfpessoa_municipiocod_to = AV106TFPessoa_MunicipioCod_To;
         AV155WWPessoaDS_29_Tfpessoa_uf = AV109TFPessoa_UF;
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = AV110TFPessoa_UF_Sel;
         AV157WWPessoaDS_31_Tfpessoa_cep = AV113TFPessoa_CEP;
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = AV114TFPessoa_CEP_Sel;
         AV159WWPessoaDS_33_Tfpessoa_telefone = AV117TFPessoa_Telefone;
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = AV118TFPessoa_Telefone_Sel;
         AV161WWPessoaDS_35_Tfpessoa_fax = AV121TFPessoa_Fax;
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = AV122TFPessoa_Fax_Sel;
         AV163WWPessoaDS_37_Tfpessoa_ativo_sel = AV83TFPessoa_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV128WWPessoaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV129WWPessoaDS_3_Pessoa_nome1 = AV17Pessoa_Nome1;
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = AV44Pessoa_TipoPessoa1;
         AV131WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV132WWPessoaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV134WWPessoaDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV135WWPessoaDS_9_Pessoa_nome2 = AV22Pessoa_Nome2;
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = AV45Pessoa_TipoPessoa2;
         AV137WWPessoaDS_11_Pessoa_docto2 = AV41Pessoa_Docto2;
         AV138WWPessoaDS_12_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV140WWPessoaDS_14_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV141WWPessoaDS_15_Pessoa_nome3 = AV27Pessoa_Nome3;
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = AV46Pessoa_TipoPessoa3;
         AV143WWPessoaDS_17_Pessoa_docto3 = AV43Pessoa_Docto3;
         AV144WWPessoaDS_18_Tfpessoa_nome = AV71TFPessoa_Nome;
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = AV72TFPessoa_Nome_Sel;
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV76TFPessoa_TipoPessoa_Sels;
         AV147WWPessoaDS_21_Tfpessoa_docto = AV79TFPessoa_Docto;
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = AV80TFPessoa_Docto_Sel;
         AV149WWPessoaDS_23_Tfpessoa_ie = AV97TFPessoa_IE;
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = AV98TFPessoa_IE_Sel;
         AV151WWPessoaDS_25_Tfpessoa_endereco = AV101TFPessoa_Endereco;
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = AV102TFPessoa_Endereco_Sel;
         AV153WWPessoaDS_27_Tfpessoa_municipiocod = AV105TFPessoa_MunicipioCod;
         AV154WWPessoaDS_28_Tfpessoa_municipiocod_to = AV106TFPessoa_MunicipioCod_To;
         AV155WWPessoaDS_29_Tfpessoa_uf = AV109TFPessoa_UF;
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = AV110TFPessoa_UF_Sel;
         AV157WWPessoaDS_31_Tfpessoa_cep = AV113TFPessoa_CEP;
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = AV114TFPessoa_CEP_Sel;
         AV159WWPessoaDS_33_Tfpessoa_telefone = AV117TFPessoa_Telefone;
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = AV118TFPessoa_Telefone_Sel;
         AV161WWPessoaDS_35_Tfpessoa_fax = AV121TFPessoa_Fax;
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = AV122TFPessoa_Fax_Sel;
         AV163WWPessoaDS_37_Tfpessoa_ativo_sel = AV83TFPessoa_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV128WWPessoaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV129WWPessoaDS_3_Pessoa_nome1 = AV17Pessoa_Nome1;
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = AV44Pessoa_TipoPessoa1;
         AV131WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV132WWPessoaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV134WWPessoaDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV135WWPessoaDS_9_Pessoa_nome2 = AV22Pessoa_Nome2;
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = AV45Pessoa_TipoPessoa2;
         AV137WWPessoaDS_11_Pessoa_docto2 = AV41Pessoa_Docto2;
         AV138WWPessoaDS_12_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV140WWPessoaDS_14_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV141WWPessoaDS_15_Pessoa_nome3 = AV27Pessoa_Nome3;
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = AV46Pessoa_TipoPessoa3;
         AV143WWPessoaDS_17_Pessoa_docto3 = AV43Pessoa_Docto3;
         AV144WWPessoaDS_18_Tfpessoa_nome = AV71TFPessoa_Nome;
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = AV72TFPessoa_Nome_Sel;
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV76TFPessoa_TipoPessoa_Sels;
         AV147WWPessoaDS_21_Tfpessoa_docto = AV79TFPessoa_Docto;
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = AV80TFPessoa_Docto_Sel;
         AV149WWPessoaDS_23_Tfpessoa_ie = AV97TFPessoa_IE;
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = AV98TFPessoa_IE_Sel;
         AV151WWPessoaDS_25_Tfpessoa_endereco = AV101TFPessoa_Endereco;
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = AV102TFPessoa_Endereco_Sel;
         AV153WWPessoaDS_27_Tfpessoa_municipiocod = AV105TFPessoa_MunicipioCod;
         AV154WWPessoaDS_28_Tfpessoa_municipiocod_to = AV106TFPessoa_MunicipioCod_To;
         AV155WWPessoaDS_29_Tfpessoa_uf = AV109TFPessoa_UF;
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = AV110TFPessoa_UF_Sel;
         AV157WWPessoaDS_31_Tfpessoa_cep = AV113TFPessoa_CEP;
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = AV114TFPessoa_CEP_Sel;
         AV159WWPessoaDS_33_Tfpessoa_telefone = AV117TFPessoa_Telefone;
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = AV118TFPessoa_Telefone_Sel;
         AV161WWPessoaDS_35_Tfpessoa_fax = AV121TFPessoa_Fax;
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = AV122TFPessoa_Fax_Sel;
         AV163WWPessoaDS_37_Tfpessoa_ativo_sel = AV83TFPessoa_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV128WWPessoaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV129WWPessoaDS_3_Pessoa_nome1 = AV17Pessoa_Nome1;
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = AV44Pessoa_TipoPessoa1;
         AV131WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV132WWPessoaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV134WWPessoaDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV135WWPessoaDS_9_Pessoa_nome2 = AV22Pessoa_Nome2;
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = AV45Pessoa_TipoPessoa2;
         AV137WWPessoaDS_11_Pessoa_docto2 = AV41Pessoa_Docto2;
         AV138WWPessoaDS_12_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV140WWPessoaDS_14_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV141WWPessoaDS_15_Pessoa_nome3 = AV27Pessoa_Nome3;
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = AV46Pessoa_TipoPessoa3;
         AV143WWPessoaDS_17_Pessoa_docto3 = AV43Pessoa_Docto3;
         AV144WWPessoaDS_18_Tfpessoa_nome = AV71TFPessoa_Nome;
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = AV72TFPessoa_Nome_Sel;
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV76TFPessoa_TipoPessoa_Sels;
         AV147WWPessoaDS_21_Tfpessoa_docto = AV79TFPessoa_Docto;
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = AV80TFPessoa_Docto_Sel;
         AV149WWPessoaDS_23_Tfpessoa_ie = AV97TFPessoa_IE;
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = AV98TFPessoa_IE_Sel;
         AV151WWPessoaDS_25_Tfpessoa_endereco = AV101TFPessoa_Endereco;
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = AV102TFPessoa_Endereco_Sel;
         AV153WWPessoaDS_27_Tfpessoa_municipiocod = AV105TFPessoa_MunicipioCod;
         AV154WWPessoaDS_28_Tfpessoa_municipiocod_to = AV106TFPessoa_MunicipioCod_To;
         AV155WWPessoaDS_29_Tfpessoa_uf = AV109TFPessoa_UF;
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = AV110TFPessoa_UF_Sel;
         AV157WWPessoaDS_31_Tfpessoa_cep = AV113TFPessoa_CEP;
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = AV114TFPessoa_CEP_Sel;
         AV159WWPessoaDS_33_Tfpessoa_telefone = AV117TFPessoa_Telefone;
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = AV118TFPessoa_Telefone_Sel;
         AV161WWPessoaDS_35_Tfpessoa_fax = AV121TFPessoa_Fax;
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = AV122TFPessoa_Fax_Sel;
         AV163WWPessoaDS_37_Tfpessoa_ativo_sel = AV83TFPessoa_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP300( )
      {
         /* Before Start, stand alone formulas. */
         AV167Pgmname = "WWPessoa";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E34302 */
         E34302 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV94ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV85DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_NOMETITLEFILTERDATA"), AV70Pessoa_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_TIPOPESSOATITLEFILTERDATA"), AV74Pessoa_TipoPessoaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_DOCTOTITLEFILTERDATA"), AV78Pessoa_DoctoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_IETITLEFILTERDATA"), AV96Pessoa_IETitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_ENDERECOTITLEFILTERDATA"), AV100Pessoa_EnderecoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_MUNICIPIOCODTITLEFILTERDATA"), AV104Pessoa_MunicipioCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_UFTITLEFILTERDATA"), AV108Pessoa_UFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_CEPTITLEFILTERDATA"), AV112Pessoa_CEPTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_TELEFONETITLEFILTERDATA"), AV116Pessoa_TelefoneTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_FAXTITLEFILTERDATA"), AV120Pessoa_FaxTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_ATIVOTITLEFILTERDATA"), AV82Pessoa_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Pessoa_Nome1 = StringUtil.Upper( cgiGet( edtavPessoa_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pessoa_Nome1", AV17Pessoa_Nome1);
            cmbavPessoa_tipopessoa1.Name = cmbavPessoa_tipopessoa1_Internalname;
            cmbavPessoa_tipopessoa1.CurrentValue = cgiGet( cmbavPessoa_tipopessoa1_Internalname);
            AV44Pessoa_TipoPessoa1 = cgiGet( cmbavPessoa_tipopessoa1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Pessoa_TipoPessoa1", AV44Pessoa_TipoPessoa1);
            AV39Pessoa_Docto1 = cgiGet( edtavPessoa_docto1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Pessoa_Docto1", AV39Pessoa_Docto1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22Pessoa_Nome2 = StringUtil.Upper( cgiGet( edtavPessoa_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Nome2", AV22Pessoa_Nome2);
            cmbavPessoa_tipopessoa2.Name = cmbavPessoa_tipopessoa2_Internalname;
            cmbavPessoa_tipopessoa2.CurrentValue = cgiGet( cmbavPessoa_tipopessoa2_Internalname);
            AV45Pessoa_TipoPessoa2 = cgiGet( cmbavPessoa_tipopessoa2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pessoa_TipoPessoa2", AV45Pessoa_TipoPessoa2);
            AV41Pessoa_Docto2 = cgiGet( edtavPessoa_docto2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Pessoa_Docto2", AV41Pessoa_Docto2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27Pessoa_Nome3 = StringUtil.Upper( cgiGet( edtavPessoa_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pessoa_Nome3", AV27Pessoa_Nome3);
            cmbavPessoa_tipopessoa3.Name = cmbavPessoa_tipopessoa3_Internalname;
            cmbavPessoa_tipopessoa3.CurrentValue = cgiGet( cmbavPessoa_tipopessoa3_Internalname);
            AV46Pessoa_TipoPessoa3 = cgiGet( cmbavPessoa_tipopessoa3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Pessoa_TipoPessoa3", AV46Pessoa_TipoPessoa3);
            AV43Pessoa_Docto3 = cgiGet( edtavPessoa_docto3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Pessoa_Docto3", AV43Pessoa_Docto3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUsuario_pessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUsuario_pessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSUARIO_PESSOACOD");
               GX_FocusControl = edtavUsuario_pessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69Usuario_PessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Usuario_PessoaCod), 6, 0)));
            }
            else
            {
               AV69Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavUsuario_pessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Usuario_PessoaCod), 6, 0)));
            }
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV90ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV90ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV90ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV90ManageFiltersExecutionStep), 1, 0));
            }
            AV71TFPessoa_Nome = StringUtil.Upper( cgiGet( edtavTfpessoa_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFPessoa_Nome", AV71TFPessoa_Nome);
            AV72TFPessoa_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfpessoa_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFPessoa_Nome_Sel", AV72TFPessoa_Nome_Sel);
            AV79TFPessoa_Docto = cgiGet( edtavTfpessoa_docto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFPessoa_Docto", AV79TFPessoa_Docto);
            AV80TFPessoa_Docto_Sel = cgiGet( edtavTfpessoa_docto_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFPessoa_Docto_Sel", AV80TFPessoa_Docto_Sel);
            AV97TFPessoa_IE = cgiGet( edtavTfpessoa_ie_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFPessoa_IE", AV97TFPessoa_IE);
            AV98TFPessoa_IE_Sel = cgiGet( edtavTfpessoa_ie_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFPessoa_IE_Sel", AV98TFPessoa_IE_Sel);
            AV101TFPessoa_Endereco = cgiGet( edtavTfpessoa_endereco_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFPessoa_Endereco", AV101TFPessoa_Endereco);
            AV102TFPessoa_Endereco_Sel = cgiGet( edtavTfpessoa_endereco_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102TFPessoa_Endereco_Sel", AV102TFPessoa_Endereco_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfpessoa_municipiocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfpessoa_municipiocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPESSOA_MUNICIPIOCOD");
               GX_FocusControl = edtavTfpessoa_municipiocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV105TFPessoa_MunicipioCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFPessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0)));
            }
            else
            {
               AV105TFPessoa_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( edtavTfpessoa_municipiocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFPessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfpessoa_municipiocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfpessoa_municipiocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPESSOA_MUNICIPIOCOD_TO");
               GX_FocusControl = edtavTfpessoa_municipiocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV106TFPessoa_MunicipioCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFPessoa_MunicipioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0)));
            }
            else
            {
               AV106TFPessoa_MunicipioCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfpessoa_municipiocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFPessoa_MunicipioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0)));
            }
            AV109TFPessoa_UF = StringUtil.Upper( cgiGet( edtavTfpessoa_uf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFPessoa_UF", AV109TFPessoa_UF);
            AV110TFPessoa_UF_Sel = StringUtil.Upper( cgiGet( edtavTfpessoa_uf_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110TFPessoa_UF_Sel", AV110TFPessoa_UF_Sel);
            AV113TFPessoa_CEP = cgiGet( edtavTfpessoa_cep_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFPessoa_CEP", AV113TFPessoa_CEP);
            AV114TFPessoa_CEP_Sel = cgiGet( edtavTfpessoa_cep_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFPessoa_CEP_Sel", AV114TFPessoa_CEP_Sel);
            AV117TFPessoa_Telefone = cgiGet( edtavTfpessoa_telefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFPessoa_Telefone", AV117TFPessoa_Telefone);
            AV118TFPessoa_Telefone_Sel = cgiGet( edtavTfpessoa_telefone_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFPessoa_Telefone_Sel", AV118TFPessoa_Telefone_Sel);
            AV121TFPessoa_Fax = cgiGet( edtavTfpessoa_fax_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFPessoa_Fax", AV121TFPessoa_Fax);
            AV122TFPessoa_Fax_Sel = cgiGet( edtavTfpessoa_fax_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122TFPessoa_Fax_Sel", AV122TFPessoa_Fax_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfpessoa_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfpessoa_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPESSOA_ATIVO_SEL");
               GX_FocusControl = edtavTfpessoa_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83TFPessoa_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFPessoa_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0));
            }
            else
            {
               AV83TFPessoa_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfpessoa_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFPessoa_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0));
            }
            AV73ddo_Pessoa_NomeTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Pessoa_NomeTitleControlIdToReplace", AV73ddo_Pessoa_NomeTitleControlIdToReplace);
            AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace", AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace);
            AV81ddo_Pessoa_DoctoTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_doctotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Pessoa_DoctoTitleControlIdToReplace", AV81ddo_Pessoa_DoctoTitleControlIdToReplace);
            AV99ddo_Pessoa_IETitleControlIdToReplace = cgiGet( edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_Pessoa_IETitleControlIdToReplace", AV99ddo_Pessoa_IETitleControlIdToReplace);
            AV103ddo_Pessoa_EnderecoTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103ddo_Pessoa_EnderecoTitleControlIdToReplace", AV103ddo_Pessoa_EnderecoTitleControlIdToReplace);
            AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace", AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace);
            AV111ddo_Pessoa_UFTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_uftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ddo_Pessoa_UFTitleControlIdToReplace", AV111ddo_Pessoa_UFTitleControlIdToReplace);
            AV115ddo_Pessoa_CEPTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_ceptitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115ddo_Pessoa_CEPTitleControlIdToReplace", AV115ddo_Pessoa_CEPTitleControlIdToReplace);
            AV119ddo_Pessoa_TelefoneTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119ddo_Pessoa_TelefoneTitleControlIdToReplace", AV119ddo_Pessoa_TelefoneTitleControlIdToReplace);
            AV123ddo_Pessoa_FaxTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_faxtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123ddo_Pessoa_FaxTitleControlIdToReplace", AV123ddo_Pessoa_FaxTitleControlIdToReplace);
            AV84ddo_Pessoa_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Pessoa_AtivoTitleControlIdToReplace", AV84ddo_Pessoa_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_97 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_97"), ",", "."));
            AV87GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV88GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_pessoa_nome_Caption = cgiGet( "DDO_PESSOA_NOME_Caption");
            Ddo_pessoa_nome_Tooltip = cgiGet( "DDO_PESSOA_NOME_Tooltip");
            Ddo_pessoa_nome_Cls = cgiGet( "DDO_PESSOA_NOME_Cls");
            Ddo_pessoa_nome_Filteredtext_set = cgiGet( "DDO_PESSOA_NOME_Filteredtext_set");
            Ddo_pessoa_nome_Selectedvalue_set = cgiGet( "DDO_PESSOA_NOME_Selectedvalue_set");
            Ddo_pessoa_nome_Dropdownoptionstype = cgiGet( "DDO_PESSOA_NOME_Dropdownoptionstype");
            Ddo_pessoa_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_NOME_Titlecontrolidtoreplace");
            Ddo_pessoa_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_NOME_Includesortasc"));
            Ddo_pessoa_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_NOME_Includesortdsc"));
            Ddo_pessoa_nome_Sortedstatus = cgiGet( "DDO_PESSOA_NOME_Sortedstatus");
            Ddo_pessoa_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_NOME_Includefilter"));
            Ddo_pessoa_nome_Filtertype = cgiGet( "DDO_PESSOA_NOME_Filtertype");
            Ddo_pessoa_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_NOME_Filterisrange"));
            Ddo_pessoa_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_NOME_Includedatalist"));
            Ddo_pessoa_nome_Datalisttype = cgiGet( "DDO_PESSOA_NOME_Datalisttype");
            Ddo_pessoa_nome_Datalistproc = cgiGet( "DDO_PESSOA_NOME_Datalistproc");
            Ddo_pessoa_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_nome_Sortasc = cgiGet( "DDO_PESSOA_NOME_Sortasc");
            Ddo_pessoa_nome_Sortdsc = cgiGet( "DDO_PESSOA_NOME_Sortdsc");
            Ddo_pessoa_nome_Loadingdata = cgiGet( "DDO_PESSOA_NOME_Loadingdata");
            Ddo_pessoa_nome_Cleanfilter = cgiGet( "DDO_PESSOA_NOME_Cleanfilter");
            Ddo_pessoa_nome_Noresultsfound = cgiGet( "DDO_PESSOA_NOME_Noresultsfound");
            Ddo_pessoa_nome_Searchbuttontext = cgiGet( "DDO_PESSOA_NOME_Searchbuttontext");
            Ddo_pessoa_tipopessoa_Caption = cgiGet( "DDO_PESSOA_TIPOPESSOA_Caption");
            Ddo_pessoa_tipopessoa_Tooltip = cgiGet( "DDO_PESSOA_TIPOPESSOA_Tooltip");
            Ddo_pessoa_tipopessoa_Cls = cgiGet( "DDO_PESSOA_TIPOPESSOA_Cls");
            Ddo_pessoa_tipopessoa_Selectedvalue_set = cgiGet( "DDO_PESSOA_TIPOPESSOA_Selectedvalue_set");
            Ddo_pessoa_tipopessoa_Dropdownoptionstype = cgiGet( "DDO_PESSOA_TIPOPESSOA_Dropdownoptionstype");
            Ddo_pessoa_tipopessoa_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_TIPOPESSOA_Titlecontrolidtoreplace");
            Ddo_pessoa_tipopessoa_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TIPOPESSOA_Includesortasc"));
            Ddo_pessoa_tipopessoa_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TIPOPESSOA_Includesortdsc"));
            Ddo_pessoa_tipopessoa_Sortedstatus = cgiGet( "DDO_PESSOA_TIPOPESSOA_Sortedstatus");
            Ddo_pessoa_tipopessoa_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TIPOPESSOA_Includefilter"));
            Ddo_pessoa_tipopessoa_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TIPOPESSOA_Includedatalist"));
            Ddo_pessoa_tipopessoa_Datalisttype = cgiGet( "DDO_PESSOA_TIPOPESSOA_Datalisttype");
            Ddo_pessoa_tipopessoa_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TIPOPESSOA_Allowmultipleselection"));
            Ddo_pessoa_tipopessoa_Datalistfixedvalues = cgiGet( "DDO_PESSOA_TIPOPESSOA_Datalistfixedvalues");
            Ddo_pessoa_tipopessoa_Sortasc = cgiGet( "DDO_PESSOA_TIPOPESSOA_Sortasc");
            Ddo_pessoa_tipopessoa_Sortdsc = cgiGet( "DDO_PESSOA_TIPOPESSOA_Sortdsc");
            Ddo_pessoa_tipopessoa_Cleanfilter = cgiGet( "DDO_PESSOA_TIPOPESSOA_Cleanfilter");
            Ddo_pessoa_tipopessoa_Searchbuttontext = cgiGet( "DDO_PESSOA_TIPOPESSOA_Searchbuttontext");
            Ddo_pessoa_docto_Caption = cgiGet( "DDO_PESSOA_DOCTO_Caption");
            Ddo_pessoa_docto_Tooltip = cgiGet( "DDO_PESSOA_DOCTO_Tooltip");
            Ddo_pessoa_docto_Cls = cgiGet( "DDO_PESSOA_DOCTO_Cls");
            Ddo_pessoa_docto_Filteredtext_set = cgiGet( "DDO_PESSOA_DOCTO_Filteredtext_set");
            Ddo_pessoa_docto_Selectedvalue_set = cgiGet( "DDO_PESSOA_DOCTO_Selectedvalue_set");
            Ddo_pessoa_docto_Dropdownoptionstype = cgiGet( "DDO_PESSOA_DOCTO_Dropdownoptionstype");
            Ddo_pessoa_docto_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_DOCTO_Titlecontrolidtoreplace");
            Ddo_pessoa_docto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_DOCTO_Includesortasc"));
            Ddo_pessoa_docto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_DOCTO_Includesortdsc"));
            Ddo_pessoa_docto_Sortedstatus = cgiGet( "DDO_PESSOA_DOCTO_Sortedstatus");
            Ddo_pessoa_docto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_DOCTO_Includefilter"));
            Ddo_pessoa_docto_Filtertype = cgiGet( "DDO_PESSOA_DOCTO_Filtertype");
            Ddo_pessoa_docto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_DOCTO_Filterisrange"));
            Ddo_pessoa_docto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_DOCTO_Includedatalist"));
            Ddo_pessoa_docto_Datalisttype = cgiGet( "DDO_PESSOA_DOCTO_Datalisttype");
            Ddo_pessoa_docto_Datalistproc = cgiGet( "DDO_PESSOA_DOCTO_Datalistproc");
            Ddo_pessoa_docto_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_DOCTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_docto_Sortasc = cgiGet( "DDO_PESSOA_DOCTO_Sortasc");
            Ddo_pessoa_docto_Sortdsc = cgiGet( "DDO_PESSOA_DOCTO_Sortdsc");
            Ddo_pessoa_docto_Loadingdata = cgiGet( "DDO_PESSOA_DOCTO_Loadingdata");
            Ddo_pessoa_docto_Cleanfilter = cgiGet( "DDO_PESSOA_DOCTO_Cleanfilter");
            Ddo_pessoa_docto_Noresultsfound = cgiGet( "DDO_PESSOA_DOCTO_Noresultsfound");
            Ddo_pessoa_docto_Searchbuttontext = cgiGet( "DDO_PESSOA_DOCTO_Searchbuttontext");
            Ddo_pessoa_ie_Caption = cgiGet( "DDO_PESSOA_IE_Caption");
            Ddo_pessoa_ie_Tooltip = cgiGet( "DDO_PESSOA_IE_Tooltip");
            Ddo_pessoa_ie_Cls = cgiGet( "DDO_PESSOA_IE_Cls");
            Ddo_pessoa_ie_Filteredtext_set = cgiGet( "DDO_PESSOA_IE_Filteredtext_set");
            Ddo_pessoa_ie_Selectedvalue_set = cgiGet( "DDO_PESSOA_IE_Selectedvalue_set");
            Ddo_pessoa_ie_Dropdownoptionstype = cgiGet( "DDO_PESSOA_IE_Dropdownoptionstype");
            Ddo_pessoa_ie_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_IE_Titlecontrolidtoreplace");
            Ddo_pessoa_ie_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includesortasc"));
            Ddo_pessoa_ie_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includesortdsc"));
            Ddo_pessoa_ie_Sortedstatus = cgiGet( "DDO_PESSOA_IE_Sortedstatus");
            Ddo_pessoa_ie_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includefilter"));
            Ddo_pessoa_ie_Filtertype = cgiGet( "DDO_PESSOA_IE_Filtertype");
            Ddo_pessoa_ie_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Filterisrange"));
            Ddo_pessoa_ie_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includedatalist"));
            Ddo_pessoa_ie_Datalisttype = cgiGet( "DDO_PESSOA_IE_Datalisttype");
            Ddo_pessoa_ie_Datalistproc = cgiGet( "DDO_PESSOA_IE_Datalistproc");
            Ddo_pessoa_ie_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_IE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_ie_Sortasc = cgiGet( "DDO_PESSOA_IE_Sortasc");
            Ddo_pessoa_ie_Sortdsc = cgiGet( "DDO_PESSOA_IE_Sortdsc");
            Ddo_pessoa_ie_Loadingdata = cgiGet( "DDO_PESSOA_IE_Loadingdata");
            Ddo_pessoa_ie_Cleanfilter = cgiGet( "DDO_PESSOA_IE_Cleanfilter");
            Ddo_pessoa_ie_Noresultsfound = cgiGet( "DDO_PESSOA_IE_Noresultsfound");
            Ddo_pessoa_ie_Searchbuttontext = cgiGet( "DDO_PESSOA_IE_Searchbuttontext");
            Ddo_pessoa_endereco_Caption = cgiGet( "DDO_PESSOA_ENDERECO_Caption");
            Ddo_pessoa_endereco_Tooltip = cgiGet( "DDO_PESSOA_ENDERECO_Tooltip");
            Ddo_pessoa_endereco_Cls = cgiGet( "DDO_PESSOA_ENDERECO_Cls");
            Ddo_pessoa_endereco_Filteredtext_set = cgiGet( "DDO_PESSOA_ENDERECO_Filteredtext_set");
            Ddo_pessoa_endereco_Selectedvalue_set = cgiGet( "DDO_PESSOA_ENDERECO_Selectedvalue_set");
            Ddo_pessoa_endereco_Dropdownoptionstype = cgiGet( "DDO_PESSOA_ENDERECO_Dropdownoptionstype");
            Ddo_pessoa_endereco_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_ENDERECO_Titlecontrolidtoreplace");
            Ddo_pessoa_endereco_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ENDERECO_Includesortasc"));
            Ddo_pessoa_endereco_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ENDERECO_Includesortdsc"));
            Ddo_pessoa_endereco_Sortedstatus = cgiGet( "DDO_PESSOA_ENDERECO_Sortedstatus");
            Ddo_pessoa_endereco_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ENDERECO_Includefilter"));
            Ddo_pessoa_endereco_Filtertype = cgiGet( "DDO_PESSOA_ENDERECO_Filtertype");
            Ddo_pessoa_endereco_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ENDERECO_Filterisrange"));
            Ddo_pessoa_endereco_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ENDERECO_Includedatalist"));
            Ddo_pessoa_endereco_Datalisttype = cgiGet( "DDO_PESSOA_ENDERECO_Datalisttype");
            Ddo_pessoa_endereco_Datalistproc = cgiGet( "DDO_PESSOA_ENDERECO_Datalistproc");
            Ddo_pessoa_endereco_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_ENDERECO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_endereco_Sortasc = cgiGet( "DDO_PESSOA_ENDERECO_Sortasc");
            Ddo_pessoa_endereco_Sortdsc = cgiGet( "DDO_PESSOA_ENDERECO_Sortdsc");
            Ddo_pessoa_endereco_Loadingdata = cgiGet( "DDO_PESSOA_ENDERECO_Loadingdata");
            Ddo_pessoa_endereco_Cleanfilter = cgiGet( "DDO_PESSOA_ENDERECO_Cleanfilter");
            Ddo_pessoa_endereco_Noresultsfound = cgiGet( "DDO_PESSOA_ENDERECO_Noresultsfound");
            Ddo_pessoa_endereco_Searchbuttontext = cgiGet( "DDO_PESSOA_ENDERECO_Searchbuttontext");
            Ddo_pessoa_municipiocod_Caption = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Caption");
            Ddo_pessoa_municipiocod_Tooltip = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Tooltip");
            Ddo_pessoa_municipiocod_Cls = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Cls");
            Ddo_pessoa_municipiocod_Filteredtext_set = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Filteredtext_set");
            Ddo_pessoa_municipiocod_Filteredtextto_set = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Filteredtextto_set");
            Ddo_pessoa_municipiocod_Dropdownoptionstype = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Dropdownoptionstype");
            Ddo_pessoa_municipiocod_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Titlecontrolidtoreplace");
            Ddo_pessoa_municipiocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Includesortasc"));
            Ddo_pessoa_municipiocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Includesortdsc"));
            Ddo_pessoa_municipiocod_Sortedstatus = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Sortedstatus");
            Ddo_pessoa_municipiocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Includefilter"));
            Ddo_pessoa_municipiocod_Filtertype = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Filtertype");
            Ddo_pessoa_municipiocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Filterisrange"));
            Ddo_pessoa_municipiocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Includedatalist"));
            Ddo_pessoa_municipiocod_Sortasc = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Sortasc");
            Ddo_pessoa_municipiocod_Sortdsc = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Sortdsc");
            Ddo_pessoa_municipiocod_Cleanfilter = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Cleanfilter");
            Ddo_pessoa_municipiocod_Rangefilterfrom = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Rangefilterfrom");
            Ddo_pessoa_municipiocod_Rangefilterto = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Rangefilterto");
            Ddo_pessoa_municipiocod_Searchbuttontext = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Searchbuttontext");
            Ddo_pessoa_uf_Caption = cgiGet( "DDO_PESSOA_UF_Caption");
            Ddo_pessoa_uf_Tooltip = cgiGet( "DDO_PESSOA_UF_Tooltip");
            Ddo_pessoa_uf_Cls = cgiGet( "DDO_PESSOA_UF_Cls");
            Ddo_pessoa_uf_Filteredtext_set = cgiGet( "DDO_PESSOA_UF_Filteredtext_set");
            Ddo_pessoa_uf_Selectedvalue_set = cgiGet( "DDO_PESSOA_UF_Selectedvalue_set");
            Ddo_pessoa_uf_Dropdownoptionstype = cgiGet( "DDO_PESSOA_UF_Dropdownoptionstype");
            Ddo_pessoa_uf_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_UF_Titlecontrolidtoreplace");
            Ddo_pessoa_uf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_UF_Includesortasc"));
            Ddo_pessoa_uf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_UF_Includesortdsc"));
            Ddo_pessoa_uf_Sortedstatus = cgiGet( "DDO_PESSOA_UF_Sortedstatus");
            Ddo_pessoa_uf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_UF_Includefilter"));
            Ddo_pessoa_uf_Filtertype = cgiGet( "DDO_PESSOA_UF_Filtertype");
            Ddo_pessoa_uf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_UF_Filterisrange"));
            Ddo_pessoa_uf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_UF_Includedatalist"));
            Ddo_pessoa_uf_Datalisttype = cgiGet( "DDO_PESSOA_UF_Datalisttype");
            Ddo_pessoa_uf_Datalistproc = cgiGet( "DDO_PESSOA_UF_Datalistproc");
            Ddo_pessoa_uf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_UF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_uf_Sortasc = cgiGet( "DDO_PESSOA_UF_Sortasc");
            Ddo_pessoa_uf_Sortdsc = cgiGet( "DDO_PESSOA_UF_Sortdsc");
            Ddo_pessoa_uf_Loadingdata = cgiGet( "DDO_PESSOA_UF_Loadingdata");
            Ddo_pessoa_uf_Cleanfilter = cgiGet( "DDO_PESSOA_UF_Cleanfilter");
            Ddo_pessoa_uf_Noresultsfound = cgiGet( "DDO_PESSOA_UF_Noresultsfound");
            Ddo_pessoa_uf_Searchbuttontext = cgiGet( "DDO_PESSOA_UF_Searchbuttontext");
            Ddo_pessoa_cep_Caption = cgiGet( "DDO_PESSOA_CEP_Caption");
            Ddo_pessoa_cep_Tooltip = cgiGet( "DDO_PESSOA_CEP_Tooltip");
            Ddo_pessoa_cep_Cls = cgiGet( "DDO_PESSOA_CEP_Cls");
            Ddo_pessoa_cep_Filteredtext_set = cgiGet( "DDO_PESSOA_CEP_Filteredtext_set");
            Ddo_pessoa_cep_Selectedvalue_set = cgiGet( "DDO_PESSOA_CEP_Selectedvalue_set");
            Ddo_pessoa_cep_Dropdownoptionstype = cgiGet( "DDO_PESSOA_CEP_Dropdownoptionstype");
            Ddo_pessoa_cep_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_CEP_Titlecontrolidtoreplace");
            Ddo_pessoa_cep_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_CEP_Includesortasc"));
            Ddo_pessoa_cep_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_CEP_Includesortdsc"));
            Ddo_pessoa_cep_Sortedstatus = cgiGet( "DDO_PESSOA_CEP_Sortedstatus");
            Ddo_pessoa_cep_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_CEP_Includefilter"));
            Ddo_pessoa_cep_Filtertype = cgiGet( "DDO_PESSOA_CEP_Filtertype");
            Ddo_pessoa_cep_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_CEP_Filterisrange"));
            Ddo_pessoa_cep_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_CEP_Includedatalist"));
            Ddo_pessoa_cep_Datalisttype = cgiGet( "DDO_PESSOA_CEP_Datalisttype");
            Ddo_pessoa_cep_Datalistproc = cgiGet( "DDO_PESSOA_CEP_Datalistproc");
            Ddo_pessoa_cep_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_CEP_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_cep_Sortasc = cgiGet( "DDO_PESSOA_CEP_Sortasc");
            Ddo_pessoa_cep_Sortdsc = cgiGet( "DDO_PESSOA_CEP_Sortdsc");
            Ddo_pessoa_cep_Loadingdata = cgiGet( "DDO_PESSOA_CEP_Loadingdata");
            Ddo_pessoa_cep_Cleanfilter = cgiGet( "DDO_PESSOA_CEP_Cleanfilter");
            Ddo_pessoa_cep_Noresultsfound = cgiGet( "DDO_PESSOA_CEP_Noresultsfound");
            Ddo_pessoa_cep_Searchbuttontext = cgiGet( "DDO_PESSOA_CEP_Searchbuttontext");
            Ddo_pessoa_telefone_Caption = cgiGet( "DDO_PESSOA_TELEFONE_Caption");
            Ddo_pessoa_telefone_Tooltip = cgiGet( "DDO_PESSOA_TELEFONE_Tooltip");
            Ddo_pessoa_telefone_Cls = cgiGet( "DDO_PESSOA_TELEFONE_Cls");
            Ddo_pessoa_telefone_Filteredtext_set = cgiGet( "DDO_PESSOA_TELEFONE_Filteredtext_set");
            Ddo_pessoa_telefone_Selectedvalue_set = cgiGet( "DDO_PESSOA_TELEFONE_Selectedvalue_set");
            Ddo_pessoa_telefone_Dropdownoptionstype = cgiGet( "DDO_PESSOA_TELEFONE_Dropdownoptionstype");
            Ddo_pessoa_telefone_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_TELEFONE_Titlecontrolidtoreplace");
            Ddo_pessoa_telefone_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includesortasc"));
            Ddo_pessoa_telefone_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includesortdsc"));
            Ddo_pessoa_telefone_Sortedstatus = cgiGet( "DDO_PESSOA_TELEFONE_Sortedstatus");
            Ddo_pessoa_telefone_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includefilter"));
            Ddo_pessoa_telefone_Filtertype = cgiGet( "DDO_PESSOA_TELEFONE_Filtertype");
            Ddo_pessoa_telefone_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Filterisrange"));
            Ddo_pessoa_telefone_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includedatalist"));
            Ddo_pessoa_telefone_Datalisttype = cgiGet( "DDO_PESSOA_TELEFONE_Datalisttype");
            Ddo_pessoa_telefone_Datalistproc = cgiGet( "DDO_PESSOA_TELEFONE_Datalistproc");
            Ddo_pessoa_telefone_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_TELEFONE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_telefone_Sortasc = cgiGet( "DDO_PESSOA_TELEFONE_Sortasc");
            Ddo_pessoa_telefone_Sortdsc = cgiGet( "DDO_PESSOA_TELEFONE_Sortdsc");
            Ddo_pessoa_telefone_Loadingdata = cgiGet( "DDO_PESSOA_TELEFONE_Loadingdata");
            Ddo_pessoa_telefone_Cleanfilter = cgiGet( "DDO_PESSOA_TELEFONE_Cleanfilter");
            Ddo_pessoa_telefone_Noresultsfound = cgiGet( "DDO_PESSOA_TELEFONE_Noresultsfound");
            Ddo_pessoa_telefone_Searchbuttontext = cgiGet( "DDO_PESSOA_TELEFONE_Searchbuttontext");
            Ddo_pessoa_fax_Caption = cgiGet( "DDO_PESSOA_FAX_Caption");
            Ddo_pessoa_fax_Tooltip = cgiGet( "DDO_PESSOA_FAX_Tooltip");
            Ddo_pessoa_fax_Cls = cgiGet( "DDO_PESSOA_FAX_Cls");
            Ddo_pessoa_fax_Filteredtext_set = cgiGet( "DDO_PESSOA_FAX_Filteredtext_set");
            Ddo_pessoa_fax_Selectedvalue_set = cgiGet( "DDO_PESSOA_FAX_Selectedvalue_set");
            Ddo_pessoa_fax_Dropdownoptionstype = cgiGet( "DDO_PESSOA_FAX_Dropdownoptionstype");
            Ddo_pessoa_fax_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_FAX_Titlecontrolidtoreplace");
            Ddo_pessoa_fax_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_FAX_Includesortasc"));
            Ddo_pessoa_fax_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_FAX_Includesortdsc"));
            Ddo_pessoa_fax_Sortedstatus = cgiGet( "DDO_PESSOA_FAX_Sortedstatus");
            Ddo_pessoa_fax_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_FAX_Includefilter"));
            Ddo_pessoa_fax_Filtertype = cgiGet( "DDO_PESSOA_FAX_Filtertype");
            Ddo_pessoa_fax_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_FAX_Filterisrange"));
            Ddo_pessoa_fax_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_FAX_Includedatalist"));
            Ddo_pessoa_fax_Datalisttype = cgiGet( "DDO_PESSOA_FAX_Datalisttype");
            Ddo_pessoa_fax_Datalistproc = cgiGet( "DDO_PESSOA_FAX_Datalistproc");
            Ddo_pessoa_fax_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_FAX_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_fax_Sortasc = cgiGet( "DDO_PESSOA_FAX_Sortasc");
            Ddo_pessoa_fax_Sortdsc = cgiGet( "DDO_PESSOA_FAX_Sortdsc");
            Ddo_pessoa_fax_Loadingdata = cgiGet( "DDO_PESSOA_FAX_Loadingdata");
            Ddo_pessoa_fax_Cleanfilter = cgiGet( "DDO_PESSOA_FAX_Cleanfilter");
            Ddo_pessoa_fax_Noresultsfound = cgiGet( "DDO_PESSOA_FAX_Noresultsfound");
            Ddo_pessoa_fax_Searchbuttontext = cgiGet( "DDO_PESSOA_FAX_Searchbuttontext");
            Ddo_pessoa_ativo_Caption = cgiGet( "DDO_PESSOA_ATIVO_Caption");
            Ddo_pessoa_ativo_Tooltip = cgiGet( "DDO_PESSOA_ATIVO_Tooltip");
            Ddo_pessoa_ativo_Cls = cgiGet( "DDO_PESSOA_ATIVO_Cls");
            Ddo_pessoa_ativo_Selectedvalue_set = cgiGet( "DDO_PESSOA_ATIVO_Selectedvalue_set");
            Ddo_pessoa_ativo_Dropdownoptionstype = cgiGet( "DDO_PESSOA_ATIVO_Dropdownoptionstype");
            Ddo_pessoa_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_ATIVO_Titlecontrolidtoreplace");
            Ddo_pessoa_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ATIVO_Includesortasc"));
            Ddo_pessoa_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ATIVO_Includesortdsc"));
            Ddo_pessoa_ativo_Sortedstatus = cgiGet( "DDO_PESSOA_ATIVO_Sortedstatus");
            Ddo_pessoa_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ATIVO_Includefilter"));
            Ddo_pessoa_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_ATIVO_Includedatalist"));
            Ddo_pessoa_ativo_Datalisttype = cgiGet( "DDO_PESSOA_ATIVO_Datalisttype");
            Ddo_pessoa_ativo_Datalistfixedvalues = cgiGet( "DDO_PESSOA_ATIVO_Datalistfixedvalues");
            Ddo_pessoa_ativo_Sortasc = cgiGet( "DDO_PESSOA_ATIVO_Sortasc");
            Ddo_pessoa_ativo_Sortdsc = cgiGet( "DDO_PESSOA_ATIVO_Sortdsc");
            Ddo_pessoa_ativo_Cleanfilter = cgiGet( "DDO_PESSOA_ATIVO_Cleanfilter");
            Ddo_pessoa_ativo_Searchbuttontext = cgiGet( "DDO_PESSOA_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_pessoa_nome_Activeeventkey = cgiGet( "DDO_PESSOA_NOME_Activeeventkey");
            Ddo_pessoa_nome_Filteredtext_get = cgiGet( "DDO_PESSOA_NOME_Filteredtext_get");
            Ddo_pessoa_nome_Selectedvalue_get = cgiGet( "DDO_PESSOA_NOME_Selectedvalue_get");
            Ddo_pessoa_tipopessoa_Activeeventkey = cgiGet( "DDO_PESSOA_TIPOPESSOA_Activeeventkey");
            Ddo_pessoa_tipopessoa_Selectedvalue_get = cgiGet( "DDO_PESSOA_TIPOPESSOA_Selectedvalue_get");
            Ddo_pessoa_docto_Activeeventkey = cgiGet( "DDO_PESSOA_DOCTO_Activeeventkey");
            Ddo_pessoa_docto_Filteredtext_get = cgiGet( "DDO_PESSOA_DOCTO_Filteredtext_get");
            Ddo_pessoa_docto_Selectedvalue_get = cgiGet( "DDO_PESSOA_DOCTO_Selectedvalue_get");
            Ddo_pessoa_ie_Activeeventkey = cgiGet( "DDO_PESSOA_IE_Activeeventkey");
            Ddo_pessoa_ie_Filteredtext_get = cgiGet( "DDO_PESSOA_IE_Filteredtext_get");
            Ddo_pessoa_ie_Selectedvalue_get = cgiGet( "DDO_PESSOA_IE_Selectedvalue_get");
            Ddo_pessoa_endereco_Activeeventkey = cgiGet( "DDO_PESSOA_ENDERECO_Activeeventkey");
            Ddo_pessoa_endereco_Filteredtext_get = cgiGet( "DDO_PESSOA_ENDERECO_Filteredtext_get");
            Ddo_pessoa_endereco_Selectedvalue_get = cgiGet( "DDO_PESSOA_ENDERECO_Selectedvalue_get");
            Ddo_pessoa_municipiocod_Activeeventkey = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Activeeventkey");
            Ddo_pessoa_municipiocod_Filteredtext_get = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Filteredtext_get");
            Ddo_pessoa_municipiocod_Filteredtextto_get = cgiGet( "DDO_PESSOA_MUNICIPIOCOD_Filteredtextto_get");
            Ddo_pessoa_uf_Activeeventkey = cgiGet( "DDO_PESSOA_UF_Activeeventkey");
            Ddo_pessoa_uf_Filteredtext_get = cgiGet( "DDO_PESSOA_UF_Filteredtext_get");
            Ddo_pessoa_uf_Selectedvalue_get = cgiGet( "DDO_PESSOA_UF_Selectedvalue_get");
            Ddo_pessoa_cep_Activeeventkey = cgiGet( "DDO_PESSOA_CEP_Activeeventkey");
            Ddo_pessoa_cep_Filteredtext_get = cgiGet( "DDO_PESSOA_CEP_Filteredtext_get");
            Ddo_pessoa_cep_Selectedvalue_get = cgiGet( "DDO_PESSOA_CEP_Selectedvalue_get");
            Ddo_pessoa_telefone_Activeeventkey = cgiGet( "DDO_PESSOA_TELEFONE_Activeeventkey");
            Ddo_pessoa_telefone_Filteredtext_get = cgiGet( "DDO_PESSOA_TELEFONE_Filteredtext_get");
            Ddo_pessoa_telefone_Selectedvalue_get = cgiGet( "DDO_PESSOA_TELEFONE_Selectedvalue_get");
            Ddo_pessoa_fax_Activeeventkey = cgiGet( "DDO_PESSOA_FAX_Activeeventkey");
            Ddo_pessoa_fax_Filteredtext_get = cgiGet( "DDO_PESSOA_FAX_Filteredtext_get");
            Ddo_pessoa_fax_Selectedvalue_get = cgiGet( "DDO_PESSOA_FAX_Selectedvalue_get");
            Ddo_pessoa_ativo_Activeeventkey = cgiGet( "DDO_PESSOA_ATIVO_Activeeventkey");
            Ddo_pessoa_ativo_Selectedvalue_get = cgiGet( "DDO_PESSOA_ATIVO_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_NOME1"), AV17Pessoa_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_TIPOPESSOA1"), AV44Pessoa_TipoPessoa1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_DOCTO1"), AV39Pessoa_Docto1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_NOME2"), AV22Pessoa_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_TIPOPESSOA2"), AV45Pessoa_TipoPessoa2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_DOCTO2"), AV41Pessoa_Docto2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_NOME3"), AV27Pessoa_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_TIPOPESSOA3"), AV46Pessoa_TipoPessoa3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPESSOA_DOCTO3"), AV43Pessoa_Docto3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vUSUARIO_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV69Usuario_PessoaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_NOME"), AV71TFPessoa_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_NOME_SEL"), AV72TFPessoa_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_DOCTO"), AV79TFPessoa_Docto) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_DOCTO_SEL"), AV80TFPessoa_Docto_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE"), AV97TFPessoa_IE) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE_SEL"), AV98TFPessoa_IE_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_ENDERECO"), AV101TFPessoa_Endereco) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_ENDERECO_SEL"), AV102TFPessoa_Endereco_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPESSOA_MUNICIPIOCOD"), ",", ".") != Convert.ToDecimal( AV105TFPessoa_MunicipioCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPESSOA_MUNICIPIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV106TFPessoa_MunicipioCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_UF"), AV109TFPessoa_UF) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_UF_SEL"), AV110TFPessoa_UF_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_CEP"), AV113TFPessoa_CEP) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_CEP_SEL"), AV114TFPessoa_CEP_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE"), AV117TFPessoa_Telefone) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE_SEL"), AV118TFPessoa_Telefone_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_FAX"), AV121TFPessoa_Fax) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_FAX_SEL"), AV122TFPessoa_Fax_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPESSOA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV83TFPessoa_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E34302 */
         E34302 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E34302( )
      {
         /* Start Routine */
         edtavUsuario_pessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoacod_Visible), 5, 0)));
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV44Pessoa_TipoPessoa1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Pessoa_TipoPessoa1", AV44Pessoa_TipoPessoa1);
         AV15DynamicFiltersSelector1 = "PESSOA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV45Pessoa_TipoPessoa2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pessoa_TipoPessoa2", AV45Pessoa_TipoPessoa2);
         AV20DynamicFiltersSelector2 = "PESSOA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV46Pessoa_TipoPessoa3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Pessoa_TipoPessoa3", AV46Pessoa_TipoPessoa3);
         AV25DynamicFiltersSelector3 = "PESSOA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfpessoa_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_nome_Visible), 5, 0)));
         edtavTfpessoa_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_nome_sel_Visible), 5, 0)));
         edtavTfpessoa_docto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_docto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_docto_Visible), 5, 0)));
         edtavTfpessoa_docto_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_docto_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_docto_sel_Visible), 5, 0)));
         edtavTfpessoa_ie_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_ie_Visible), 5, 0)));
         edtavTfpessoa_ie_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_ie_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_ie_sel_Visible), 5, 0)));
         edtavTfpessoa_endereco_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_endereco_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_endereco_Visible), 5, 0)));
         edtavTfpessoa_endereco_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_endereco_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_endereco_sel_Visible), 5, 0)));
         edtavTfpessoa_municipiocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_municipiocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_municipiocod_Visible), 5, 0)));
         edtavTfpessoa_municipiocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_municipiocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_municipiocod_to_Visible), 5, 0)));
         edtavTfpessoa_uf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_uf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_uf_Visible), 5, 0)));
         edtavTfpessoa_uf_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_uf_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_uf_sel_Visible), 5, 0)));
         edtavTfpessoa_cep_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_cep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_cep_Visible), 5, 0)));
         edtavTfpessoa_cep_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_cep_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_cep_sel_Visible), 5, 0)));
         edtavTfpessoa_telefone_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_telefone_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_telefone_Visible), 5, 0)));
         edtavTfpessoa_telefone_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_telefone_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_telefone_sel_Visible), 5, 0)));
         edtavTfpessoa_fax_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_fax_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_fax_Visible), 5, 0)));
         edtavTfpessoa_fax_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_fax_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_fax_sel_Visible), 5, 0)));
         edtavTfpessoa_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_ativo_sel_Visible), 5, 0)));
         Ddo_pessoa_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "TitleControlIdToReplace", Ddo_pessoa_nome_Titlecontrolidtoreplace);
         AV73ddo_Pessoa_NomeTitleControlIdToReplace = Ddo_pessoa_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Pessoa_NomeTitleControlIdToReplace", AV73ddo_Pessoa_NomeTitleControlIdToReplace);
         edtavDdo_pessoa_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_tipopessoa_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_TipoPessoa";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_tipopessoa_Internalname, "TitleControlIdToReplace", Ddo_pessoa_tipopessoa_Titlecontrolidtoreplace);
         AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace = Ddo_pessoa_tipopessoa_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace", AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace);
         edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_docto_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_Docto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "TitleControlIdToReplace", Ddo_pessoa_docto_Titlecontrolidtoreplace);
         AV81ddo_Pessoa_DoctoTitleControlIdToReplace = Ddo_pessoa_docto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Pessoa_DoctoTitleControlIdToReplace", AV81ddo_Pessoa_DoctoTitleControlIdToReplace);
         edtavDdo_pessoa_doctotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_doctotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_doctotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_ie_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_IE";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "TitleControlIdToReplace", Ddo_pessoa_ie_Titlecontrolidtoreplace);
         AV99ddo_Pessoa_IETitleControlIdToReplace = Ddo_pessoa_ie_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99ddo_Pessoa_IETitleControlIdToReplace", AV99ddo_Pessoa_IETitleControlIdToReplace);
         edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_endereco_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_Endereco";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "TitleControlIdToReplace", Ddo_pessoa_endereco_Titlecontrolidtoreplace);
         AV103ddo_Pessoa_EnderecoTitleControlIdToReplace = Ddo_pessoa_endereco_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103ddo_Pessoa_EnderecoTitleControlIdToReplace", AV103ddo_Pessoa_EnderecoTitleControlIdToReplace);
         edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_municipiocod_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_MunicipioCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "TitleControlIdToReplace", Ddo_pessoa_municipiocod_Titlecontrolidtoreplace);
         AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace = Ddo_pessoa_municipiocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace", AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace);
         edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_uf_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_UF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "TitleControlIdToReplace", Ddo_pessoa_uf_Titlecontrolidtoreplace);
         AV111ddo_Pessoa_UFTitleControlIdToReplace = Ddo_pessoa_uf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ddo_Pessoa_UFTitleControlIdToReplace", AV111ddo_Pessoa_UFTitleControlIdToReplace);
         edtavDdo_pessoa_uftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_uftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_uftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_cep_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_CEP";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "TitleControlIdToReplace", Ddo_pessoa_cep_Titlecontrolidtoreplace);
         AV115ddo_Pessoa_CEPTitleControlIdToReplace = Ddo_pessoa_cep_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115ddo_Pessoa_CEPTitleControlIdToReplace", AV115ddo_Pessoa_CEPTitleControlIdToReplace);
         edtavDdo_pessoa_ceptitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_ceptitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_ceptitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_telefone_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_Telefone";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "TitleControlIdToReplace", Ddo_pessoa_telefone_Titlecontrolidtoreplace);
         AV119ddo_Pessoa_TelefoneTitleControlIdToReplace = Ddo_pessoa_telefone_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119ddo_Pessoa_TelefoneTitleControlIdToReplace", AV119ddo_Pessoa_TelefoneTitleControlIdToReplace);
         edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_fax_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_Fax";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "TitleControlIdToReplace", Ddo_pessoa_fax_Titlecontrolidtoreplace);
         AV123ddo_Pessoa_FaxTitleControlIdToReplace = Ddo_pessoa_fax_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123ddo_Pessoa_FaxTitleControlIdToReplace", AV123ddo_Pessoa_FaxTitleControlIdToReplace);
         edtavDdo_pessoa_faxtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_faxtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_faxtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ativo_Internalname, "TitleControlIdToReplace", Ddo_pessoa_ativo_Titlecontrolidtoreplace);
         AV84ddo_Pessoa_AtivoTitleControlIdToReplace = Ddo_pessoa_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Pessoa_AtivoTitleControlIdToReplace", AV84ddo_Pessoa_AtivoTitleControlIdToReplace);
         edtavDdo_pessoa_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Pessoas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Tipo da Pessoa", 0);
         cmbavOrderedby.addItem("3", "Documento", 0);
         cmbavOrderedby.addItem("4", "Insc. Estadual", 0);
         cmbavOrderedby.addItem("5", "Endere�o", 0);
         cmbavOrderedby.addItem("6", "Municipio", 0);
         cmbavOrderedby.addItem("7", "UF", 0);
         cmbavOrderedby.addItem("8", "CEP", 0);
         cmbavOrderedby.addItem("9", "Telefone", 0);
         cmbavOrderedby.addItem("10", "Fax", 0);
         cmbavOrderedby.addItem("11", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV85DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV85DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            AV44Pessoa_TipoPessoa1 = "F";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Pessoa_TipoPessoa1", AV44Pessoa_TipoPessoa1);
         }
      }

      protected void E35302( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV70Pessoa_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Pessoa_TipoPessoaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Pessoa_DoctoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV96Pessoa_IETitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV100Pessoa_EnderecoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV104Pessoa_MunicipioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV108Pessoa_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV112Pessoa_CEPTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV116Pessoa_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV120Pessoa_FaxTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Pessoa_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV90ManageFiltersExecutionStep == 1 )
         {
            AV90ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV90ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV90ManageFiltersExecutionStep == 2 )
         {
            AV90ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV90ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_DOCTO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_DOCTO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_DOCTO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPessoa_Nome_Titleformat = 2;
         edtPessoa_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV73ddo_Pessoa_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Nome_Internalname, "Title", edtPessoa_Nome_Title);
         cmbPessoa_TipoPessoa_Titleformat = 2;
         cmbPessoa_TipoPessoa.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo da Pessoa", AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPessoa_TipoPessoa_Internalname, "Title", cmbPessoa_TipoPessoa.Title.Text);
         edtPessoa_Docto_Titleformat = 2;
         edtPessoa_Docto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Documento", AV81ddo_Pessoa_DoctoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Docto_Internalname, "Title", edtPessoa_Docto_Title);
         edtPessoa_IE_Titleformat = 2;
         edtPessoa_IE_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Insc. Estadual", AV99ddo_Pessoa_IETitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_IE_Internalname, "Title", edtPessoa_IE_Title);
         edtPessoa_Endereco_Titleformat = 2;
         edtPessoa_Endereco_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Endere�o", AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Endereco_Internalname, "Title", edtPessoa_Endereco_Title);
         edtPessoa_MunicipioCod_Titleformat = 2;
         edtPessoa_MunicipioCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Municipio", AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_MunicipioCod_Internalname, "Title", edtPessoa_MunicipioCod_Title);
         edtPessoa_UF_Titleformat = 2;
         edtPessoa_UF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "UF", AV111ddo_Pessoa_UFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_UF_Internalname, "Title", edtPessoa_UF_Title);
         edtPessoa_CEP_Titleformat = 2;
         edtPessoa_CEP_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CEP", AV115ddo_Pessoa_CEPTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_CEP_Internalname, "Title", edtPessoa_CEP_Title);
         edtPessoa_Telefone_Titleformat = 2;
         edtPessoa_Telefone_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Telefone", AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Telefone_Internalname, "Title", edtPessoa_Telefone_Title);
         edtPessoa_Fax_Titleformat = 2;
         edtPessoa_Fax_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fax", AV123ddo_Pessoa_FaxTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Fax_Internalname, "Title", edtPessoa_Fax_Title);
         chkPessoa_Ativo_Titleformat = 2;
         chkPessoa_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV84ddo_Pessoa_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPessoa_Ativo_Internalname, "Title", chkPessoa_Ativo.Title.Text);
         AV87GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87GridCurrentPage), 10, 0)));
         AV88GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88GridPageCount), 10, 0)));
         edtavUpdate_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Enabled), 5, 0)));
         edtavDelete_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Enabled), 5, 0)));
         edtavDisplay_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Enabled), 5, 0)));
         if ( AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV69Usuario_PessoaCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Usuario_PessoaCod), 6, 0)));
         }
         else
         {
            /* Using cursor H00304 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Userid});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1Usuario_Codigo = H00304_A1Usuario_Codigo[0];
               A57Usuario_PessoaCod = H00304_A57Usuario_PessoaCod[0];
               AV69Usuario_PessoaCod = A57Usuario_PessoaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Usuario_PessoaCod), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV128WWPessoaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV129WWPessoaDS_3_Pessoa_nome1 = AV17Pessoa_Nome1;
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = AV44Pessoa_TipoPessoa1;
         AV131WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV132WWPessoaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV134WWPessoaDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV135WWPessoaDS_9_Pessoa_nome2 = AV22Pessoa_Nome2;
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = AV45Pessoa_TipoPessoa2;
         AV137WWPessoaDS_11_Pessoa_docto2 = AV41Pessoa_Docto2;
         AV138WWPessoaDS_12_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV140WWPessoaDS_14_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV141WWPessoaDS_15_Pessoa_nome3 = AV27Pessoa_Nome3;
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = AV46Pessoa_TipoPessoa3;
         AV143WWPessoaDS_17_Pessoa_docto3 = AV43Pessoa_Docto3;
         AV144WWPessoaDS_18_Tfpessoa_nome = AV71TFPessoa_Nome;
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = AV72TFPessoa_Nome_Sel;
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV76TFPessoa_TipoPessoa_Sels;
         AV147WWPessoaDS_21_Tfpessoa_docto = AV79TFPessoa_Docto;
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = AV80TFPessoa_Docto_Sel;
         AV149WWPessoaDS_23_Tfpessoa_ie = AV97TFPessoa_IE;
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = AV98TFPessoa_IE_Sel;
         AV151WWPessoaDS_25_Tfpessoa_endereco = AV101TFPessoa_Endereco;
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = AV102TFPessoa_Endereco_Sel;
         AV153WWPessoaDS_27_Tfpessoa_municipiocod = AV105TFPessoa_MunicipioCod;
         AV154WWPessoaDS_28_Tfpessoa_municipiocod_to = AV106TFPessoa_MunicipioCod_To;
         AV155WWPessoaDS_29_Tfpessoa_uf = AV109TFPessoa_UF;
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = AV110TFPessoa_UF_Sel;
         AV157WWPessoaDS_31_Tfpessoa_cep = AV113TFPessoa_CEP;
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = AV114TFPessoa_CEP_Sel;
         AV159WWPessoaDS_33_Tfpessoa_telefone = AV117TFPessoa_Telefone;
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = AV118TFPessoa_Telefone_Sel;
         AV161WWPessoaDS_35_Tfpessoa_fax = AV121TFPessoa_Fax;
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = AV122TFPessoa_Fax_Sel;
         AV163WWPessoaDS_37_Tfpessoa_ativo_sel = AV83TFPessoa_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70Pessoa_NomeTitleFilterData", AV70Pessoa_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74Pessoa_TipoPessoaTitleFilterData", AV74Pessoa_TipoPessoaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78Pessoa_DoctoTitleFilterData", AV78Pessoa_DoctoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96Pessoa_IETitleFilterData", AV96Pessoa_IETitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV100Pessoa_EnderecoTitleFilterData", AV100Pessoa_EnderecoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV104Pessoa_MunicipioCodTitleFilterData", AV104Pessoa_MunicipioCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV108Pessoa_UFTitleFilterData", AV108Pessoa_UFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV112Pessoa_CEPTitleFilterData", AV112Pessoa_CEPTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV116Pessoa_TelefoneTitleFilterData", AV116Pessoa_TelefoneTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV120Pessoa_FaxTitleFilterData", AV120Pessoa_FaxTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82Pessoa_AtivoTitleFilterData", AV82Pessoa_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV94ManageFiltersData", AV94ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12302( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV86PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV86PageToGo) ;
         }
      }

      protected void E13302( )
      {
         /* Ddo_pessoa_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "SortedStatus", Ddo_pessoa_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "SortedStatus", Ddo_pessoa_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFPessoa_Nome = Ddo_pessoa_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFPessoa_Nome", AV71TFPessoa_Nome);
            AV72TFPessoa_Nome_Sel = Ddo_pessoa_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFPessoa_Nome_Sel", AV72TFPessoa_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14302( )
      {
         /* Ddo_pessoa_tipopessoa_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_tipopessoa_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_tipopessoa_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_tipopessoa_Internalname, "SortedStatus", Ddo_pessoa_tipopessoa_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_tipopessoa_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_tipopessoa_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_tipopessoa_Internalname, "SortedStatus", Ddo_pessoa_tipopessoa_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_tipopessoa_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFPessoa_TipoPessoa_SelsJson = Ddo_pessoa_tipopessoa_Selectedvalue_get;
            AV76TFPessoa_TipoPessoa_Sels.FromJSonString(AV75TFPessoa_TipoPessoa_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76TFPessoa_TipoPessoa_Sels", AV76TFPessoa_TipoPessoa_Sels);
      }

      protected void E15302( )
      {
         /* Ddo_pessoa_docto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_docto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_docto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "SortedStatus", Ddo_pessoa_docto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_docto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_docto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "SortedStatus", Ddo_pessoa_docto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_docto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFPessoa_Docto = Ddo_pessoa_docto_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFPessoa_Docto", AV79TFPessoa_Docto);
            AV80TFPessoa_Docto_Sel = Ddo_pessoa_docto_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFPessoa_Docto_Sel", AV80TFPessoa_Docto_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16302( )
      {
         /* Ddo_pessoa_ie_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_ie_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_ie_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_ie_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_ie_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_ie_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV97TFPessoa_IE = Ddo_pessoa_ie_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFPessoa_IE", AV97TFPessoa_IE);
            AV98TFPessoa_IE_Sel = Ddo_pessoa_ie_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFPessoa_IE_Sel", AV98TFPessoa_IE_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17302( )
      {
         /* Ddo_pessoa_endereco_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_endereco_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_endereco_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "SortedStatus", Ddo_pessoa_endereco_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_endereco_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_endereco_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "SortedStatus", Ddo_pessoa_endereco_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_endereco_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV101TFPessoa_Endereco = Ddo_pessoa_endereco_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFPessoa_Endereco", AV101TFPessoa_Endereco);
            AV102TFPessoa_Endereco_Sel = Ddo_pessoa_endereco_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102TFPessoa_Endereco_Sel", AV102TFPessoa_Endereco_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18302( )
      {
         /* Ddo_pessoa_municipiocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_municipiocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_municipiocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "SortedStatus", Ddo_pessoa_municipiocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_municipiocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_municipiocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "SortedStatus", Ddo_pessoa_municipiocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_municipiocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV105TFPessoa_MunicipioCod = (int)(NumberUtil.Val( Ddo_pessoa_municipiocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFPessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0)));
            AV106TFPessoa_MunicipioCod_To = (int)(NumberUtil.Val( Ddo_pessoa_municipiocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFPessoa_MunicipioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19302( )
      {
         /* Ddo_pessoa_uf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_uf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_uf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "SortedStatus", Ddo_pessoa_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_uf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_uf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "SortedStatus", Ddo_pessoa_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_uf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV109TFPessoa_UF = Ddo_pessoa_uf_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFPessoa_UF", AV109TFPessoa_UF);
            AV110TFPessoa_UF_Sel = Ddo_pessoa_uf_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110TFPessoa_UF_Sel", AV110TFPessoa_UF_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20302( )
      {
         /* Ddo_pessoa_cep_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_cep_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_cep_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "SortedStatus", Ddo_pessoa_cep_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_cep_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_cep_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "SortedStatus", Ddo_pessoa_cep_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_cep_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV113TFPessoa_CEP = Ddo_pessoa_cep_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFPessoa_CEP", AV113TFPessoa_CEP);
            AV114TFPessoa_CEP_Sel = Ddo_pessoa_cep_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFPessoa_CEP_Sel", AV114TFPessoa_CEP_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E21302( )
      {
         /* Ddo_pessoa_telefone_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_telefone_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_telefone_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_telefone_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_telefone_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_telefone_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV117TFPessoa_Telefone = Ddo_pessoa_telefone_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFPessoa_Telefone", AV117TFPessoa_Telefone);
            AV118TFPessoa_Telefone_Sel = Ddo_pessoa_telefone_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFPessoa_Telefone_Sel", AV118TFPessoa_Telefone_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E22302( )
      {
         /* Ddo_pessoa_fax_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_fax_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_fax_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "SortedStatus", Ddo_pessoa_fax_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_fax_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_fax_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "SortedStatus", Ddo_pessoa_fax_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_fax_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV121TFPessoa_Fax = Ddo_pessoa_fax_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFPessoa_Fax", AV121TFPessoa_Fax);
            AV122TFPessoa_Fax_Sel = Ddo_pessoa_fax_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122TFPessoa_Fax_Sel", AV122TFPessoa_Fax_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E23302( )
      {
         /* Ddo_pessoa_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ativo_Internalname, "SortedStatus", Ddo_pessoa_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ativo_Internalname, "SortedStatus", Ddo_pessoa_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFPessoa_Ativo_Sel = (short)(NumberUtil.Val( Ddo_pessoa_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFPessoa_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E36302( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("pessoa.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A34Pessoa_Codigo);
            AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV164Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV31Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV164Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("pessoa.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A34Pessoa_Codigo);
            AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV165Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV32Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV165Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A34Pessoa_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV89Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV89Display);
            AV166Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV89Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV89Display);
            AV166Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         if ( ! AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
            edtavDelete_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 97;
         }
         sendrow_972( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_97_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(97, GridRow);
         }
      }

      protected void E24302( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E29302( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
      }

      protected void E25302( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavPessoa_tipopessoa1.CurrentValue = StringUtil.RTrim( AV44Pessoa_TipoPessoa1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Values", cmbavPessoa_tipopessoa1.ToJavascriptSource());
         cmbavPessoa_tipopessoa2.CurrentValue = StringUtil.RTrim( AV45Pessoa_TipoPessoa2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Values", cmbavPessoa_tipopessoa2.ToJavascriptSource());
         cmbavPessoa_tipopessoa3.CurrentValue = StringUtil.RTrim( AV46Pessoa_TipoPessoa3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Values", cmbavPessoa_tipopessoa3.ToJavascriptSource());
      }

      protected void E30302( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E31302( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
      }

      protected void E26302( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavPessoa_tipopessoa1.CurrentValue = StringUtil.RTrim( AV44Pessoa_TipoPessoa1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Values", cmbavPessoa_tipopessoa1.ToJavascriptSource());
         cmbavPessoa_tipopessoa2.CurrentValue = StringUtil.RTrim( AV45Pessoa_TipoPessoa2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Values", cmbavPessoa_tipopessoa2.ToJavascriptSource());
         cmbavPessoa_tipopessoa3.CurrentValue = StringUtil.RTrim( AV46Pessoa_TipoPessoa3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Values", cmbavPessoa_tipopessoa3.ToJavascriptSource());
      }

      protected void E32302( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E27302( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pessoa_Nome1, AV44Pessoa_TipoPessoa1, AV39Pessoa_Docto1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Pessoa_Nome2, AV45Pessoa_TipoPessoa2, AV41Pessoa_Docto2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Pessoa_Nome3, AV46Pessoa_TipoPessoa3, AV43Pessoa_Docto3, AV69Usuario_PessoaCod, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV71TFPessoa_Nome, AV72TFPessoa_Nome_Sel, AV79TFPessoa_Docto, AV80TFPessoa_Docto_Sel, AV97TFPessoa_IE, AV98TFPessoa_IE_Sel, AV101TFPessoa_Endereco, AV102TFPessoa_Endereco_Sel, AV105TFPessoa_MunicipioCod, AV106TFPessoa_MunicipioCod_To, AV109TFPessoa_UF, AV110TFPessoa_UF_Sel, AV113TFPessoa_CEP, AV114TFPessoa_CEP_Sel, AV117TFPessoa_Telefone, AV118TFPessoa_Telefone_Sel, AV121TFPessoa_Fax, AV122TFPessoa_Fax_Sel, AV83TFPessoa_Ativo_Sel, AV90ManageFiltersExecutionStep, AV73ddo_Pessoa_NomeTitleControlIdToReplace, AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace, AV81ddo_Pessoa_DoctoTitleControlIdToReplace, AV99ddo_Pessoa_IETitleControlIdToReplace, AV103ddo_Pessoa_EnderecoTitleControlIdToReplace, AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace, AV111ddo_Pessoa_UFTitleControlIdToReplace, AV115ddo_Pessoa_CEPTitleControlIdToReplace, AV119ddo_Pessoa_TelefoneTitleControlIdToReplace, AV123ddo_Pessoa_FaxTitleControlIdToReplace, AV84ddo_Pessoa_AtivoTitleControlIdToReplace, A1Usuario_Codigo, A57Usuario_PessoaCod, AV76TFPessoa_TipoPessoa_Sels, AV167Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, AV6WWPContext, A34Pessoa_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavPessoa_tipopessoa1.CurrentValue = StringUtil.RTrim( AV44Pessoa_TipoPessoa1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Values", cmbavPessoa_tipopessoa1.ToJavascriptSource());
         cmbavPessoa_tipopessoa2.CurrentValue = StringUtil.RTrim( AV45Pessoa_TipoPessoa2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Values", cmbavPessoa_tipopessoa2.ToJavascriptSource());
         cmbavPessoa_tipopessoa3.CurrentValue = StringUtil.RTrim( AV46Pessoa_TipoPessoa3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Values", cmbavPessoa_tipopessoa3.ToJavascriptSource());
      }

      protected void E33302( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E11302( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S232 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWPessoaFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV90ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV90ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWPessoaFilters")), new Object[] {});
            AV90ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV90ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV91ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWPessoaFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV91ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV167Pgmname+"GridState",  AV91ManageFiltersXml) ;
               AV10GridState.FromXml(AV91ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S222 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76TFPessoa_TipoPessoa_Sels", AV76TFPessoa_TipoPessoa_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavPessoa_tipopessoa1.CurrentValue = StringUtil.RTrim( AV44Pessoa_TipoPessoa1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Values", cmbavPessoa_tipopessoa1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavPessoa_tipopessoa2.CurrentValue = StringUtil.RTrim( AV45Pessoa_TipoPessoa2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Values", cmbavPessoa_tipopessoa2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavPessoa_tipopessoa3.CurrentValue = StringUtil.RTrim( AV46Pessoa_TipoPessoa3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Values", cmbavPessoa_tipopessoa3.ToJavascriptSource());
      }

      protected void E28302( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76TFPessoa_TipoPessoa_Sels", AV76TFPessoa_TipoPessoa_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavPessoa_tipopessoa1.CurrentValue = StringUtil.RTrim( AV44Pessoa_TipoPessoa1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Values", cmbavPessoa_tipopessoa1.ToJavascriptSource());
         cmbavPessoa_tipopessoa2.CurrentValue = StringUtil.RTrim( AV45Pessoa_TipoPessoa2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Values", cmbavPessoa_tipopessoa2.ToJavascriptSource());
         cmbavPessoa_tipopessoa3.CurrentValue = StringUtil.RTrim( AV46Pessoa_TipoPessoa3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Values", cmbavPessoa_tipopessoa3.ToJavascriptSource());
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_pessoa_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "SortedStatus", Ddo_pessoa_nome_Sortedstatus);
         Ddo_pessoa_tipopessoa_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_tipopessoa_Internalname, "SortedStatus", Ddo_pessoa_tipopessoa_Sortedstatus);
         Ddo_pessoa_docto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "SortedStatus", Ddo_pessoa_docto_Sortedstatus);
         Ddo_pessoa_ie_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
         Ddo_pessoa_endereco_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "SortedStatus", Ddo_pessoa_endereco_Sortedstatus);
         Ddo_pessoa_municipiocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "SortedStatus", Ddo_pessoa_municipiocod_Sortedstatus);
         Ddo_pessoa_uf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "SortedStatus", Ddo_pessoa_uf_Sortedstatus);
         Ddo_pessoa_cep_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "SortedStatus", Ddo_pessoa_cep_Sortedstatus);
         Ddo_pessoa_telefone_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
         Ddo_pessoa_fax_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "SortedStatus", Ddo_pessoa_fax_Sortedstatus);
         Ddo_pessoa_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ativo_Internalname, "SortedStatus", Ddo_pessoa_ativo_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_pessoa_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "SortedStatus", Ddo_pessoa_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_pessoa_tipopessoa_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_tipopessoa_Internalname, "SortedStatus", Ddo_pessoa_tipopessoa_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_pessoa_docto_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "SortedStatus", Ddo_pessoa_docto_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_pessoa_ie_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_pessoa_endereco_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "SortedStatus", Ddo_pessoa_endereco_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_pessoa_municipiocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "SortedStatus", Ddo_pessoa_municipiocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_pessoa_uf_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "SortedStatus", Ddo_pessoa_uf_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_pessoa_cep_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "SortedStatus", Ddo_pessoa_cep_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_pessoa_telefone_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_pessoa_fax_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "SortedStatus", Ddo_pessoa_fax_Sortedstatus);
         }
         else if ( AV13OrderedBy == 11 )
         {
            Ddo_pessoa_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ativo_Internalname, "SortedStatus", Ddo_pessoa_ativo_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavPessoa_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_nome1_Visible), 5, 0)));
         cmbavPessoa_tipopessoa1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPessoa_tipopessoa1.Visible), 5, 0)));
         edtavPessoa_docto1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_docto1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_docto1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_NOME") == 0 )
         {
            edtavPessoa_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_TIPOPESSOA") == 0 )
         {
            cmbavPessoa_tipopessoa1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPessoa_tipopessoa1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_DOCTO") == 0 )
         {
            edtavPessoa_docto1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_docto1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_docto1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavPessoa_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_nome2_Visible), 5, 0)));
         cmbavPessoa_tipopessoa2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPessoa_tipopessoa2.Visible), 5, 0)));
         edtavPessoa_docto2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_docto2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_docto2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_NOME") == 0 )
         {
            edtavPessoa_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_TIPOPESSOA") == 0 )
         {
            cmbavPessoa_tipopessoa2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPessoa_tipopessoa2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_DOCTO") == 0 )
         {
            edtavPessoa_docto2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_docto2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_docto2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavPessoa_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_nome3_Visible), 5, 0)));
         cmbavPessoa_tipopessoa3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPessoa_tipopessoa3.Visible), 5, 0)));
         edtavPessoa_docto3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_docto3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_docto3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_NOME") == 0 )
         {
            edtavPessoa_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_TIPOPESSOA") == 0 )
         {
            cmbavPessoa_tipopessoa3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPessoa_tipopessoa3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_DOCTO") == 0 )
         {
            edtavPessoa_docto3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_docto3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_docto3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "PESSOA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22Pessoa_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Nome2", AV22Pessoa_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "PESSOA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27Pessoa_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pessoa_Nome3", AV27Pessoa_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV94ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV95ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV95ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV95ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV95ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV95ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV95ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV94ManageFiltersData.Add(AV95ManageFiltersDataItem, 0);
         AV95ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV95ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV95ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV95ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV95ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV94ManageFiltersData.Add(AV95ManageFiltersDataItem, 0);
         AV95ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV95ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV94ManageFiltersData.Add(AV95ManageFiltersDataItem, 0);
         AV92ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWPessoaFilters"), "");
         AV168GXV1 = 1;
         while ( AV168GXV1 <= AV92ManageFiltersItems.Count )
         {
            AV93ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV92ManageFiltersItems.Item(AV168GXV1));
            AV95ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV95ManageFiltersDataItem.gxTpr_Title = AV93ManageFiltersItem.gxTpr_Title;
            AV95ManageFiltersDataItem.gxTpr_Eventkey = AV93ManageFiltersItem.gxTpr_Title;
            AV95ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV95ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV94ManageFiltersData.Add(AV95ManageFiltersDataItem, 0);
            if ( AV94ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV168GXV1 = (int)(AV168GXV1+1);
         }
         if ( AV94ManageFiltersData.Count > 3 )
         {
            AV95ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV95ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV94ManageFiltersData.Add(AV95ManageFiltersDataItem, 0);
            AV95ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV95ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV95ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV95ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV95ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV95ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV94ManageFiltersData.Add(AV95ManageFiltersDataItem, 0);
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV71TFPessoa_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFPessoa_Nome", AV71TFPessoa_Nome);
         Ddo_pessoa_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "FilteredText_set", Ddo_pessoa_nome_Filteredtext_set);
         AV72TFPessoa_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFPessoa_Nome_Sel", AV72TFPessoa_Nome_Sel);
         Ddo_pessoa_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "SelectedValue_set", Ddo_pessoa_nome_Selectedvalue_set);
         AV76TFPessoa_TipoPessoa_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_pessoa_tipopessoa_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_tipopessoa_Internalname, "SelectedValue_set", Ddo_pessoa_tipopessoa_Selectedvalue_set);
         AV79TFPessoa_Docto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFPessoa_Docto", AV79TFPessoa_Docto);
         Ddo_pessoa_docto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "FilteredText_set", Ddo_pessoa_docto_Filteredtext_set);
         AV80TFPessoa_Docto_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFPessoa_Docto_Sel", AV80TFPessoa_Docto_Sel);
         Ddo_pessoa_docto_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "SelectedValue_set", Ddo_pessoa_docto_Selectedvalue_set);
         AV97TFPessoa_IE = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFPessoa_IE", AV97TFPessoa_IE);
         Ddo_pessoa_ie_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "FilteredText_set", Ddo_pessoa_ie_Filteredtext_set);
         AV98TFPessoa_IE_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFPessoa_IE_Sel", AV98TFPessoa_IE_Sel);
         Ddo_pessoa_ie_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SelectedValue_set", Ddo_pessoa_ie_Selectedvalue_set);
         AV101TFPessoa_Endereco = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFPessoa_Endereco", AV101TFPessoa_Endereco);
         Ddo_pessoa_endereco_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "FilteredText_set", Ddo_pessoa_endereco_Filteredtext_set);
         AV102TFPessoa_Endereco_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102TFPessoa_Endereco_Sel", AV102TFPessoa_Endereco_Sel);
         Ddo_pessoa_endereco_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "SelectedValue_set", Ddo_pessoa_endereco_Selectedvalue_set);
         AV105TFPessoa_MunicipioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFPessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0)));
         Ddo_pessoa_municipiocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "FilteredText_set", Ddo_pessoa_municipiocod_Filteredtext_set);
         AV106TFPessoa_MunicipioCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFPessoa_MunicipioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0)));
         Ddo_pessoa_municipiocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "FilteredTextTo_set", Ddo_pessoa_municipiocod_Filteredtextto_set);
         AV109TFPessoa_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFPessoa_UF", AV109TFPessoa_UF);
         Ddo_pessoa_uf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "FilteredText_set", Ddo_pessoa_uf_Filteredtext_set);
         AV110TFPessoa_UF_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110TFPessoa_UF_Sel", AV110TFPessoa_UF_Sel);
         Ddo_pessoa_uf_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "SelectedValue_set", Ddo_pessoa_uf_Selectedvalue_set);
         AV113TFPessoa_CEP = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFPessoa_CEP", AV113TFPessoa_CEP);
         Ddo_pessoa_cep_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "FilteredText_set", Ddo_pessoa_cep_Filteredtext_set);
         AV114TFPessoa_CEP_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFPessoa_CEP_Sel", AV114TFPessoa_CEP_Sel);
         Ddo_pessoa_cep_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "SelectedValue_set", Ddo_pessoa_cep_Selectedvalue_set);
         AV117TFPessoa_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFPessoa_Telefone", AV117TFPessoa_Telefone);
         Ddo_pessoa_telefone_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "FilteredText_set", Ddo_pessoa_telefone_Filteredtext_set);
         AV118TFPessoa_Telefone_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFPessoa_Telefone_Sel", AV118TFPessoa_Telefone_Sel);
         Ddo_pessoa_telefone_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SelectedValue_set", Ddo_pessoa_telefone_Selectedvalue_set);
         AV121TFPessoa_Fax = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFPessoa_Fax", AV121TFPessoa_Fax);
         Ddo_pessoa_fax_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "FilteredText_set", Ddo_pessoa_fax_Filteredtext_set);
         AV122TFPessoa_Fax_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122TFPessoa_Fax_Sel", AV122TFPessoa_Fax_Sel);
         Ddo_pessoa_fax_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "SelectedValue_set", Ddo_pessoa_fax_Selectedvalue_set);
         AV83TFPessoa_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFPessoa_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0));
         Ddo_pessoa_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ativo_Internalname, "SelectedValue_set", Ddo_pessoa_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "PESSOA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Pessoa_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pessoa_Nome1", AV17Pessoa_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get(AV167Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV167Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV35Session.Get(AV167Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV169GXV2 = 1;
         while ( AV169GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV169GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_NOME") == 0 )
            {
               AV71TFPessoa_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFPessoa_Nome", AV71TFPessoa_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFPessoa_Nome)) )
               {
                  Ddo_pessoa_nome_Filteredtext_set = AV71TFPessoa_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "FilteredText_set", Ddo_pessoa_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_NOME_SEL") == 0 )
            {
               AV72TFPessoa_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFPessoa_Nome_Sel", AV72TFPessoa_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFPessoa_Nome_Sel)) )
               {
                  Ddo_pessoa_nome_Selectedvalue_set = AV72TFPessoa_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_nome_Internalname, "SelectedValue_set", Ddo_pessoa_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_TIPOPESSOA_SEL") == 0 )
            {
               AV75TFPessoa_TipoPessoa_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV76TFPessoa_TipoPessoa_Sels.FromJSonString(AV75TFPessoa_TipoPessoa_SelsJson);
               if ( ! ( AV76TFPessoa_TipoPessoa_Sels.Count == 0 ) )
               {
                  Ddo_pessoa_tipopessoa_Selectedvalue_set = AV75TFPessoa_TipoPessoa_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_tipopessoa_Internalname, "SelectedValue_set", Ddo_pessoa_tipopessoa_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_DOCTO") == 0 )
            {
               AV79TFPessoa_Docto = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFPessoa_Docto", AV79TFPessoa_Docto);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFPessoa_Docto)) )
               {
                  Ddo_pessoa_docto_Filteredtext_set = AV79TFPessoa_Docto;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "FilteredText_set", Ddo_pessoa_docto_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_DOCTO_SEL") == 0 )
            {
               AV80TFPessoa_Docto_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFPessoa_Docto_Sel", AV80TFPessoa_Docto_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFPessoa_Docto_Sel)) )
               {
                  Ddo_pessoa_docto_Selectedvalue_set = AV80TFPessoa_Docto_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_docto_Internalname, "SelectedValue_set", Ddo_pessoa_docto_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE") == 0 )
            {
               AV97TFPessoa_IE = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFPessoa_IE", AV97TFPessoa_IE);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97TFPessoa_IE)) )
               {
                  Ddo_pessoa_ie_Filteredtext_set = AV97TFPessoa_IE;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "FilteredText_set", Ddo_pessoa_ie_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE_SEL") == 0 )
            {
               AV98TFPessoa_IE_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98TFPessoa_IE_Sel", AV98TFPessoa_IE_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98TFPessoa_IE_Sel)) )
               {
                  Ddo_pessoa_ie_Selectedvalue_set = AV98TFPessoa_IE_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SelectedValue_set", Ddo_pessoa_ie_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_ENDERECO") == 0 )
            {
               AV101TFPessoa_Endereco = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFPessoa_Endereco", AV101TFPessoa_Endereco);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101TFPessoa_Endereco)) )
               {
                  Ddo_pessoa_endereco_Filteredtext_set = AV101TFPessoa_Endereco;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "FilteredText_set", Ddo_pessoa_endereco_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_ENDERECO_SEL") == 0 )
            {
               AV102TFPessoa_Endereco_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102TFPessoa_Endereco_Sel", AV102TFPessoa_Endereco_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102TFPessoa_Endereco_Sel)) )
               {
                  Ddo_pessoa_endereco_Selectedvalue_set = AV102TFPessoa_Endereco_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_endereco_Internalname, "SelectedValue_set", Ddo_pessoa_endereco_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_MUNICIPIOCOD") == 0 )
            {
               AV105TFPessoa_MunicipioCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFPessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0)));
               AV106TFPessoa_MunicipioCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFPessoa_MunicipioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0)));
               if ( ! (0==AV105TFPessoa_MunicipioCod) )
               {
                  Ddo_pessoa_municipiocod_Filteredtext_set = StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "FilteredText_set", Ddo_pessoa_municipiocod_Filteredtext_set);
               }
               if ( ! (0==AV106TFPessoa_MunicipioCod_To) )
               {
                  Ddo_pessoa_municipiocod_Filteredtextto_set = StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_municipiocod_Internalname, "FilteredTextTo_set", Ddo_pessoa_municipiocod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_UF") == 0 )
            {
               AV109TFPessoa_UF = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFPessoa_UF", AV109TFPessoa_UF);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109TFPessoa_UF)) )
               {
                  Ddo_pessoa_uf_Filteredtext_set = AV109TFPessoa_UF;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "FilteredText_set", Ddo_pessoa_uf_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_UF_SEL") == 0 )
            {
               AV110TFPessoa_UF_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110TFPessoa_UF_Sel", AV110TFPessoa_UF_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110TFPessoa_UF_Sel)) )
               {
                  Ddo_pessoa_uf_Selectedvalue_set = AV110TFPessoa_UF_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_uf_Internalname, "SelectedValue_set", Ddo_pessoa_uf_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_CEP") == 0 )
            {
               AV113TFPessoa_CEP = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFPessoa_CEP", AV113TFPessoa_CEP);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113TFPessoa_CEP)) )
               {
                  Ddo_pessoa_cep_Filteredtext_set = AV113TFPessoa_CEP;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "FilteredText_set", Ddo_pessoa_cep_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_CEP_SEL") == 0 )
            {
               AV114TFPessoa_CEP_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFPessoa_CEP_Sel", AV114TFPessoa_CEP_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114TFPessoa_CEP_Sel)) )
               {
                  Ddo_pessoa_cep_Selectedvalue_set = AV114TFPessoa_CEP_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_cep_Internalname, "SelectedValue_set", Ddo_pessoa_cep_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE") == 0 )
            {
               AV117TFPessoa_Telefone = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFPessoa_Telefone", AV117TFPessoa_Telefone);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117TFPessoa_Telefone)) )
               {
                  Ddo_pessoa_telefone_Filteredtext_set = AV117TFPessoa_Telefone;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "FilteredText_set", Ddo_pessoa_telefone_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE_SEL") == 0 )
            {
               AV118TFPessoa_Telefone_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFPessoa_Telefone_Sel", AV118TFPessoa_Telefone_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118TFPessoa_Telefone_Sel)) )
               {
                  Ddo_pessoa_telefone_Selectedvalue_set = AV118TFPessoa_Telefone_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SelectedValue_set", Ddo_pessoa_telefone_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_FAX") == 0 )
            {
               AV121TFPessoa_Fax = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFPessoa_Fax", AV121TFPessoa_Fax);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121TFPessoa_Fax)) )
               {
                  Ddo_pessoa_fax_Filteredtext_set = AV121TFPessoa_Fax;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "FilteredText_set", Ddo_pessoa_fax_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_FAX_SEL") == 0 )
            {
               AV122TFPessoa_Fax_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122TFPessoa_Fax_Sel", AV122TFPessoa_Fax_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122TFPessoa_Fax_Sel)) )
               {
                  Ddo_pessoa_fax_Selectedvalue_set = AV122TFPessoa_Fax_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_fax_Internalname, "SelectedValue_set", Ddo_pessoa_fax_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_ATIVO_SEL") == 0 )
            {
               AV83TFPessoa_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFPessoa_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0));
               if ( ! (0==AV83TFPessoa_Ativo_Sel) )
               {
                  Ddo_pessoa_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ativo_Internalname, "SelectedValue_set", Ddo_pessoa_ativo_Selectedvalue_set);
               }
            }
            AV169GXV2 = (int)(AV169GXV2+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Pessoa_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pessoa_Nome1", AV17Pessoa_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_TIPOPESSOA") == 0 )
            {
               AV44Pessoa_TipoPessoa1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Pessoa_TipoPessoa1", AV44Pessoa_TipoPessoa1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_DOCTO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV39Pessoa_Docto1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Pessoa_Docto1", AV39Pessoa_Docto1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Pessoa_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Nome2", AV22Pessoa_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_TIPOPESSOA") == 0 )
               {
                  AV45Pessoa_TipoPessoa2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pessoa_TipoPessoa2", AV45Pessoa_TipoPessoa2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_DOCTO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV41Pessoa_Docto2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Pessoa_Docto2", AV41Pessoa_Docto2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27Pessoa_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pessoa_Nome3", AV27Pessoa_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_TIPOPESSOA") == 0 )
                  {
                     AV46Pessoa_TipoPessoa3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Pessoa_TipoPessoa3", AV46Pessoa_TipoPessoa3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_DOCTO") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV43Pessoa_Docto3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Pessoa_Docto3", AV43Pessoa_Docto3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV35Session.Get(AV167Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFPessoa_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFPessoa_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFPessoa_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV72TFPessoa_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV76TFPessoa_TipoPessoa_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_TIPOPESSOA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV76TFPessoa_TipoPessoa_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFPessoa_Docto)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_DOCTO";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFPessoa_Docto;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFPessoa_Docto_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_DOCTO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV80TFPessoa_Docto_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97TFPessoa_IE)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_IE";
            AV11GridStateFilterValue.gxTpr_Value = AV97TFPessoa_IE;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98TFPessoa_IE_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_IE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV98TFPessoa_IE_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101TFPessoa_Endereco)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_ENDERECO";
            AV11GridStateFilterValue.gxTpr_Value = AV101TFPessoa_Endereco;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102TFPessoa_Endereco_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_ENDERECO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV102TFPessoa_Endereco_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV105TFPessoa_MunicipioCod) && (0==AV106TFPessoa_MunicipioCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_MUNICIPIOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV105TFPessoa_MunicipioCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV106TFPessoa_MunicipioCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109TFPessoa_UF)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_UF";
            AV11GridStateFilterValue.gxTpr_Value = AV109TFPessoa_UF;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110TFPessoa_UF_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_UF_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV110TFPessoa_UF_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113TFPessoa_CEP)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_CEP";
            AV11GridStateFilterValue.gxTpr_Value = AV113TFPessoa_CEP;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114TFPessoa_CEP_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_CEP_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV114TFPessoa_CEP_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117TFPessoa_Telefone)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_TELEFONE";
            AV11GridStateFilterValue.gxTpr_Value = AV117TFPessoa_Telefone;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118TFPessoa_Telefone_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_TELEFONE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV118TFPessoa_Telefone_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121TFPessoa_Fax)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_FAX";
            AV11GridStateFilterValue.gxTpr_Value = AV121TFPessoa_Fax;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122TFPessoa_Fax_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_FAX_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV122TFPessoa_Fax_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV83TFPessoa_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV83TFPessoa_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV167Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Pessoa_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Pessoa_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_TIPOPESSOA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Pessoa_TipoPessoa1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV44Pessoa_TipoPessoa1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOA_DOCTO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Pessoa_Docto1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39Pessoa_Docto1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Pessoa_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Pessoa_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_TIPOPESSOA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Pessoa_TipoPessoa2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV45Pessoa_TipoPessoa2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PESSOA_DOCTO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Pessoa_Docto2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41Pessoa_Docto2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Pessoa_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27Pessoa_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_TIPOPESSOA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Pessoa_TipoPessoa3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV46Pessoa_TipoPessoa3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "PESSOA_DOCTO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Pessoa_Docto3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV43Pessoa_Docto3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV167Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Pessoa";
         AV35Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_302( true) ;
         }
         else
         {
            wb_table2_8_302( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_91_302( true) ;
         }
         else
         {
            wb_table3_91_302( false) ;
         }
         return  ;
      }

      protected void wb_table3_91_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_302e( true) ;
         }
         else
         {
            wb_table1_2_302e( false) ;
         }
      }

      protected void wb_table3_91_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_94_302( true) ;
         }
         else
         {
            wb_table4_94_302( false) ;
         }
         return  ;
      }

      protected void wb_table4_94_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_91_302e( true) ;
         }
         else
         {
            wb_table3_91_302e( false) ;
         }
      }

      protected void wb_table4_94_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"97\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbPessoa_TipoPessoa_Titleformat == 0 )
               {
                  context.SendWebValue( cmbPessoa_TipoPessoa.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbPessoa_TipoPessoa.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Docto_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Docto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Docto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_IE_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_IE_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_IE_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Endereco_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Endereco_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Endereco_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_MunicipioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_MunicipioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_MunicipioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_UF_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_UF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_UF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_CEP_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_CEP_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_CEP_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Telefone_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Telefone_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Telefone_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Fax_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Fax_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Fax_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkPessoa_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkPessoa_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkPessoa_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV89Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A34Pessoa_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A35Pessoa_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A36Pessoa_TipoPessoa));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbPessoa_TipoPessoa.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbPessoa_TipoPessoa_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A37Pessoa_Docto);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Docto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Docto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A518Pessoa_IE));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_IE_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_IE_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A519Pessoa_Endereco);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Endereco_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Endereco_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A503Pessoa_MunicipioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_MunicipioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_MunicipioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A520Pessoa_UF));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_UF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_UF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A521Pessoa_CEP));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_CEP_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_CEP_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A522Pessoa_Telefone));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Telefone_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Telefone_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A523Pessoa_Fax));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Fax_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Fax_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A38Pessoa_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkPessoa_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkPessoa_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 97 )
         {
            wbEnd = 0;
            nRC_GXsfl_97 = (short)(nGXsfl_97_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_94_302e( true) ;
         }
         else
         {
            wb_table4_94_302e( false) ;
         }
      }

      protected void wb_table2_8_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_302( true) ;
         }
         else
         {
            wb_table5_11_302( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_16_302( true) ;
         }
         else
         {
            wb_table6_16_302( false) ;
         }
         return  ;
      }

      protected void wb_table6_16_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_24_302( true) ;
         }
         else
         {
            wb_table7_24_302( false) ;
         }
         return  ;
      }

      protected void wb_table7_24_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_302e( true) ;
         }
         else
         {
            wb_table2_8_302e( false) ;
         }
      }

      protected void wb_table7_24_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_31_302( true) ;
         }
         else
         {
            wb_table8_31_302( false) ;
         }
         return  ;
      }

      protected void wb_table8_31_302e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_24_302e( true) ;
         }
         else
         {
            wb_table7_24_302e( false) ;
         }
      }

      protected void wb_table8_31_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_WWPessoa.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_40_302( true) ;
         }
         else
         {
            wb_table9_40_302( false) ;
         }
         return  ;
      }

      protected void wb_table9_40_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoa.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_WWPessoa.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_59_302( true) ;
         }
         else
         {
            wb_table10_59_302( false) ;
         }
         return  ;
      }

      protected void wb_table10_59_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoa.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWPessoa.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_78_302( true) ;
         }
         else
         {
            wb_table11_78_302( false) ;
         }
         return  ;
      }

      protected void wb_table11_78_302e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_31_302e( true) ;
         }
         else
         {
            wb_table8_31_302e( false) ;
         }
      }

      protected void wb_table11_78_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "", true, "HLP_WWPessoa.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_nome3_Internalname, StringUtil.RTrim( AV27Pessoa_Nome3), StringUtil.RTrim( context.localUtil.Format( AV27Pessoa_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPessoa_nome3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavPessoa_tipopessoa3, cmbavPessoa_tipopessoa3_Internalname, StringUtil.RTrim( AV46Pessoa_TipoPessoa3), 1, cmbavPessoa_tipopessoa3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavPessoa_tipopessoa3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "", true, "HLP_WWPessoa.htm");
            cmbavPessoa_tipopessoa3.CurrentValue = StringUtil.RTrim( AV46Pessoa_TipoPessoa3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa3_Internalname, "Values", (String)(cmbavPessoa_tipopessoa3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_docto3_Internalname, AV43Pessoa_Docto3, StringUtil.RTrim( context.localUtil.Format( AV43Pessoa_Docto3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_docto3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPessoa_docto3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_78_302e( true) ;
         }
         else
         {
            wb_table11_78_302e( false) ;
         }
      }

      protected void wb_table10_59_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_WWPessoa.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_nome2_Internalname, StringUtil.RTrim( AV22Pessoa_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22Pessoa_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPessoa_nome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavPessoa_tipopessoa2, cmbavPessoa_tipopessoa2_Internalname, StringUtil.RTrim( AV45Pessoa_TipoPessoa2), 1, cmbavPessoa_tipopessoa2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavPessoa_tipopessoa2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_WWPessoa.htm");
            cmbavPessoa_tipopessoa2.CurrentValue = StringUtil.RTrim( AV45Pessoa_TipoPessoa2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa2_Internalname, "Values", (String)(cmbavPessoa_tipopessoa2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_docto2_Internalname, AV41Pessoa_Docto2, StringUtil.RTrim( context.localUtil.Format( AV41Pessoa_Docto2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_docto2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPessoa_docto2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_59_302e( true) ;
         }
         else
         {
            wb_table10_59_302e( false) ;
         }
      }

      protected void wb_table9_40_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_WWPessoa.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_nome1_Internalname, StringUtil.RTrim( AV17Pessoa_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Pessoa_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPessoa_nome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavPessoa_tipopessoa1, cmbavPessoa_tipopessoa1_Internalname, StringUtil.RTrim( AV44Pessoa_TipoPessoa1), 1, cmbavPessoa_tipopessoa1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavPessoa_tipopessoa1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWPessoa.htm");
            cmbavPessoa_tipopessoa1.CurrentValue = StringUtil.RTrim( AV44Pessoa_TipoPessoa1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPessoa_tipopessoa1_Internalname, "Values", (String)(cmbavPessoa_tipopessoa1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_docto1_Internalname, AV39Pessoa_Docto1, StringUtil.RTrim( context.localUtil.Format( AV39Pessoa_Docto1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_docto1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPessoa_docto1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_40_302e( true) ;
         }
         else
         {
            wb_table9_40_302e( false) ;
         }
      }

      protected void wb_table6_16_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableorders_Internalname, tblTableorders_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_97_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_WWPessoa.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_97_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_16_302e( true) ;
         }
         else
         {
            wb_table6_16_302e( false) ;
         }
      }

      protected void wb_table5_11_302( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPessoatitle_Internalname, "Pessoas", "", "", lblPessoatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWPessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_302e( true) ;
         }
         else
         {
            wb_table5_11_302e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA302( ) ;
         WS302( ) ;
         WE302( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117331215");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwpessoa.js", "?20203117331215");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_972( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_97_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_97_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_97_idx;
         edtPessoa_Codigo_Internalname = "PESSOA_CODIGO_"+sGXsfl_97_idx;
         edtPessoa_Nome_Internalname = "PESSOA_NOME_"+sGXsfl_97_idx;
         cmbPessoa_TipoPessoa_Internalname = "PESSOA_TIPOPESSOA_"+sGXsfl_97_idx;
         edtPessoa_Docto_Internalname = "PESSOA_DOCTO_"+sGXsfl_97_idx;
         edtPessoa_IE_Internalname = "PESSOA_IE_"+sGXsfl_97_idx;
         edtPessoa_Endereco_Internalname = "PESSOA_ENDERECO_"+sGXsfl_97_idx;
         edtPessoa_MunicipioCod_Internalname = "PESSOA_MUNICIPIOCOD_"+sGXsfl_97_idx;
         edtPessoa_UF_Internalname = "PESSOA_UF_"+sGXsfl_97_idx;
         edtPessoa_CEP_Internalname = "PESSOA_CEP_"+sGXsfl_97_idx;
         edtPessoa_Telefone_Internalname = "PESSOA_TELEFONE_"+sGXsfl_97_idx;
         edtPessoa_Fax_Internalname = "PESSOA_FAX_"+sGXsfl_97_idx;
         chkPessoa_Ativo_Internalname = "PESSOA_ATIVO_"+sGXsfl_97_idx;
      }

      protected void SubsflControlProps_fel_972( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_97_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_97_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_97_fel_idx;
         edtPessoa_Codigo_Internalname = "PESSOA_CODIGO_"+sGXsfl_97_fel_idx;
         edtPessoa_Nome_Internalname = "PESSOA_NOME_"+sGXsfl_97_fel_idx;
         cmbPessoa_TipoPessoa_Internalname = "PESSOA_TIPOPESSOA_"+sGXsfl_97_fel_idx;
         edtPessoa_Docto_Internalname = "PESSOA_DOCTO_"+sGXsfl_97_fel_idx;
         edtPessoa_IE_Internalname = "PESSOA_IE_"+sGXsfl_97_fel_idx;
         edtPessoa_Endereco_Internalname = "PESSOA_ENDERECO_"+sGXsfl_97_fel_idx;
         edtPessoa_MunicipioCod_Internalname = "PESSOA_MUNICIPIOCOD_"+sGXsfl_97_fel_idx;
         edtPessoa_UF_Internalname = "PESSOA_UF_"+sGXsfl_97_fel_idx;
         edtPessoa_CEP_Internalname = "PESSOA_CEP_"+sGXsfl_97_fel_idx;
         edtPessoa_Telefone_Internalname = "PESSOA_TELEFONE_"+sGXsfl_97_fel_idx;
         edtPessoa_Fax_Internalname = "PESSOA_FAX_"+sGXsfl_97_fel_idx;
         chkPessoa_Ativo_Internalname = "PESSOA_ATIVO_"+sGXsfl_97_fel_idx;
      }

      protected void sendrow_972( )
      {
         SubsflControlProps_972( ) ;
         WB300( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_97_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_97_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_97_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV164Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV164Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV165Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV165Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV89Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV89Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV166Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV89Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV89Display)) ? AV166Display_GXI : context.PathToRelativeUrl( AV89Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV89Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A34Pessoa_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Nome_Internalname,StringUtil.RTrim( A35Pessoa_Nome),StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_97_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PESSOA_TIPOPESSOA_" + sGXsfl_97_idx;
               cmbPessoa_TipoPessoa.Name = GXCCtl;
               cmbPessoa_TipoPessoa.WebTags = "";
               cmbPessoa_TipoPessoa.addItem("", "(Nenhum)", 0);
               cmbPessoa_TipoPessoa.addItem("F", "F�sica", 0);
               cmbPessoa_TipoPessoa.addItem("J", "Jur�dica", 0);
               if ( cmbPessoa_TipoPessoa.ItemCount > 0 )
               {
                  A36Pessoa_TipoPessoa = cmbPessoa_TipoPessoa.getValidValue(A36Pessoa_TipoPessoa);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbPessoa_TipoPessoa,(String)cmbPessoa_TipoPessoa_Internalname,StringUtil.RTrim( A36Pessoa_TipoPessoa),(short)1,(String)cmbPessoa_TipoPessoa_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbPessoa_TipoPessoa.CurrentValue = StringUtil.RTrim( A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPessoa_TipoPessoa_Internalname, "Values", (String)(cmbPessoa_TipoPessoa.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Docto_Internalname,(String)A37Pessoa_Docto,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Docto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_IE_Internalname,StringUtil.RTrim( A518Pessoa_IE),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_IE_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"IE",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Endereco_Internalname,(String)A519Pessoa_Endereco,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Endereco_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_MunicipioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A503Pessoa_MunicipioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A503Pessoa_MunicipioCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_MunicipioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_UF_Internalname,StringUtil.RTrim( A520Pessoa_UF),StringUtil.RTrim( context.localUtil.Format( A520Pessoa_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_CEP_Internalname,StringUtil.RTrim( A521Pessoa_CEP),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_CEP_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Telefone_Internalname,StringUtil.RTrim( A522Pessoa_Telefone),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Telefone_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Fax_Internalname,StringUtil.RTrim( A523Pessoa_Fax),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Fax_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkPessoa_Ativo_Internalname,StringUtil.BoolToStr( A38Pessoa_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_CODIGO"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_NOME"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_TIPOPESSOA"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A36Pessoa_TipoPessoa, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_DOCTO"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A37Pessoa_Docto, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_IE"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_ENDERECO"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_MUNICIPIOCOD"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, context.localUtil.Format( (decimal)(A503Pessoa_MunicipioCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_CEP"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_TELEFONE"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_FAX"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOA_ATIVO"+"_"+sGXsfl_97_idx, GetSecureSignedToken( sGXsfl_97_idx, A38Pessoa_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_97_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_97_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_97_idx+1));
            sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
            SubsflControlProps_972( ) ;
         }
         /* End function sendrow_972 */
      }

      protected void init_default_properties( )
      {
         lblPessoatitle_Internalname = "PESSOATITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableorders_Internalname = "TABLEORDERS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavPessoa_nome1_Internalname = "vPESSOA_NOME1";
         cmbavPessoa_tipopessoa1_Internalname = "vPESSOA_TIPOPESSOA1";
         edtavPessoa_docto1_Internalname = "vPESSOA_DOCTO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavPessoa_nome2_Internalname = "vPESSOA_NOME2";
         cmbavPessoa_tipopessoa2_Internalname = "vPESSOA_TIPOPESSOA2";
         edtavPessoa_docto2_Internalname = "vPESSOA_DOCTO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavPessoa_nome3_Internalname = "vPESSOA_NOME3";
         cmbavPessoa_tipopessoa3_Internalname = "vPESSOA_TIPOPESSOA3";
         edtavPessoa_docto3_Internalname = "vPESSOA_DOCTO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtPessoa_Codigo_Internalname = "PESSOA_CODIGO";
         edtPessoa_Nome_Internalname = "PESSOA_NOME";
         cmbPessoa_TipoPessoa_Internalname = "PESSOA_TIPOPESSOA";
         edtPessoa_Docto_Internalname = "PESSOA_DOCTO";
         edtPessoa_IE_Internalname = "PESSOA_IE";
         edtPessoa_Endereco_Internalname = "PESSOA_ENDERECO";
         edtPessoa_MunicipioCod_Internalname = "PESSOA_MUNICIPIOCOD";
         edtPessoa_UF_Internalname = "PESSOA_UF";
         edtPessoa_CEP_Internalname = "PESSOA_CEP";
         edtPessoa_Telefone_Internalname = "PESSOA_TELEFONE";
         edtPessoa_Fax_Internalname = "PESSOA_FAX";
         chkPessoa_Ativo_Internalname = "PESSOA_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavUsuario_pessoacod_Internalname = "vUSUARIO_PESSOACOD";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfpessoa_nome_Internalname = "vTFPESSOA_NOME";
         edtavTfpessoa_nome_sel_Internalname = "vTFPESSOA_NOME_SEL";
         edtavTfpessoa_docto_Internalname = "vTFPESSOA_DOCTO";
         edtavTfpessoa_docto_sel_Internalname = "vTFPESSOA_DOCTO_SEL";
         edtavTfpessoa_ie_Internalname = "vTFPESSOA_IE";
         edtavTfpessoa_ie_sel_Internalname = "vTFPESSOA_IE_SEL";
         edtavTfpessoa_endereco_Internalname = "vTFPESSOA_ENDERECO";
         edtavTfpessoa_endereco_sel_Internalname = "vTFPESSOA_ENDERECO_SEL";
         edtavTfpessoa_municipiocod_Internalname = "vTFPESSOA_MUNICIPIOCOD";
         edtavTfpessoa_municipiocod_to_Internalname = "vTFPESSOA_MUNICIPIOCOD_TO";
         edtavTfpessoa_uf_Internalname = "vTFPESSOA_UF";
         edtavTfpessoa_uf_sel_Internalname = "vTFPESSOA_UF_SEL";
         edtavTfpessoa_cep_Internalname = "vTFPESSOA_CEP";
         edtavTfpessoa_cep_sel_Internalname = "vTFPESSOA_CEP_SEL";
         edtavTfpessoa_telefone_Internalname = "vTFPESSOA_TELEFONE";
         edtavTfpessoa_telefone_sel_Internalname = "vTFPESSOA_TELEFONE_SEL";
         edtavTfpessoa_fax_Internalname = "vTFPESSOA_FAX";
         edtavTfpessoa_fax_sel_Internalname = "vTFPESSOA_FAX_SEL";
         edtavTfpessoa_ativo_sel_Internalname = "vTFPESSOA_ATIVO_SEL";
         Ddo_pessoa_nome_Internalname = "DDO_PESSOA_NOME";
         edtavDdo_pessoa_nometitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_pessoa_tipopessoa_Internalname = "DDO_PESSOA_TIPOPESSOA";
         edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE";
         Ddo_pessoa_docto_Internalname = "DDO_PESSOA_DOCTO";
         edtavDdo_pessoa_doctotitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE";
         Ddo_pessoa_ie_Internalname = "DDO_PESSOA_IE";
         edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_IETITLECONTROLIDTOREPLACE";
         Ddo_pessoa_endereco_Internalname = "DDO_PESSOA_ENDERECO";
         edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE";
         Ddo_pessoa_municipiocod_Internalname = "DDO_PESSOA_MUNICIPIOCOD";
         edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE";
         Ddo_pessoa_uf_Internalname = "DDO_PESSOA_UF";
         edtavDdo_pessoa_uftitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE";
         Ddo_pessoa_cep_Internalname = "DDO_PESSOA_CEP";
         edtavDdo_pessoa_ceptitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE";
         Ddo_pessoa_telefone_Internalname = "DDO_PESSOA_TELEFONE";
         edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE";
         Ddo_pessoa_fax_Internalname = "DDO_PESSOA_FAX";
         edtavDdo_pessoa_faxtitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE";
         Ddo_pessoa_ativo_Internalname = "DDO_PESSOA_ATIVO";
         edtavDdo_pessoa_ativotitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtPessoa_Fax_Jsonclick = "";
         edtPessoa_Telefone_Jsonclick = "";
         edtPessoa_CEP_Jsonclick = "";
         edtPessoa_UF_Jsonclick = "";
         edtPessoa_MunicipioCod_Jsonclick = "";
         edtPessoa_Endereco_Jsonclick = "";
         edtPessoa_IE_Jsonclick = "";
         edtPessoa_Docto_Jsonclick = "";
         cmbPessoa_TipoPessoa_Jsonclick = "";
         edtPessoa_Nome_Jsonclick = "";
         edtPessoa_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         edtavPessoa_docto1_Jsonclick = "";
         cmbavPessoa_tipopessoa1_Jsonclick = "";
         edtavPessoa_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavPessoa_docto2_Jsonclick = "";
         cmbavPessoa_tipopessoa2_Jsonclick = "";
         edtavPessoa_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavPessoa_docto3_Jsonclick = "";
         cmbavPessoa_tipopessoa3_Jsonclick = "";
         edtavPessoa_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkPessoa_Ativo_Titleformat = 0;
         edtPessoa_Fax_Titleformat = 0;
         edtPessoa_Telefone_Titleformat = 0;
         edtPessoa_CEP_Titleformat = 0;
         edtPessoa_UF_Titleformat = 0;
         edtPessoa_MunicipioCod_Titleformat = 0;
         edtPessoa_Endereco_Titleformat = 0;
         edtPessoa_IE_Titleformat = 0;
         edtPessoa_Docto_Titleformat = 0;
         cmbPessoa_TipoPessoa_Titleformat = 0;
         edtPessoa_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavPessoa_docto3_Visible = 1;
         cmbavPessoa_tipopessoa3.Visible = 1;
         edtavPessoa_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavPessoa_docto2_Visible = 1;
         cmbavPessoa_tipopessoa2.Visible = 1;
         edtavPessoa_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavPessoa_docto1_Visible = 1;
         cmbavPessoa_tipopessoa1.Visible = 1;
         edtavPessoa_nome1_Visible = 1;
         edtavDisplay_Enabled = 1;
         edtavDelete_Enabled = 1;
         edtavUpdate_Enabled = 1;
         chkPessoa_Ativo.Title.Text = "Ativo?";
         edtPessoa_Fax_Title = "Fax";
         edtPessoa_Telefone_Title = "Telefone";
         edtPessoa_CEP_Title = "CEP";
         edtPessoa_UF_Title = "UF";
         edtPessoa_MunicipioCod_Title = "Municipio";
         edtPessoa_Endereco_Title = "Endere�o";
         edtPessoa_IE_Title = "Insc. Estadual";
         edtPessoa_Docto_Title = "Documento";
         cmbPessoa_TipoPessoa.Title.Text = "Tipo da Pessoa";
         edtPessoa_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkPessoa_Ativo.Caption = "";
         edtavDdo_pessoa_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_faxtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_ceptitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_uftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_doctotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfpessoa_ativo_sel_Jsonclick = "";
         edtavTfpessoa_ativo_sel_Visible = 1;
         edtavTfpessoa_fax_sel_Jsonclick = "";
         edtavTfpessoa_fax_sel_Visible = 1;
         edtavTfpessoa_fax_Jsonclick = "";
         edtavTfpessoa_fax_Visible = 1;
         edtavTfpessoa_telefone_sel_Jsonclick = "";
         edtavTfpessoa_telefone_sel_Visible = 1;
         edtavTfpessoa_telefone_Jsonclick = "";
         edtavTfpessoa_telefone_Visible = 1;
         edtavTfpessoa_cep_sel_Jsonclick = "";
         edtavTfpessoa_cep_sel_Visible = 1;
         edtavTfpessoa_cep_Jsonclick = "";
         edtavTfpessoa_cep_Visible = 1;
         edtavTfpessoa_uf_sel_Jsonclick = "";
         edtavTfpessoa_uf_sel_Visible = 1;
         edtavTfpessoa_uf_Jsonclick = "";
         edtavTfpessoa_uf_Visible = 1;
         edtavTfpessoa_municipiocod_to_Jsonclick = "";
         edtavTfpessoa_municipiocod_to_Visible = 1;
         edtavTfpessoa_municipiocod_Jsonclick = "";
         edtavTfpessoa_municipiocod_Visible = 1;
         edtavTfpessoa_endereco_sel_Jsonclick = "";
         edtavTfpessoa_endereco_sel_Visible = 1;
         edtavTfpessoa_endereco_Jsonclick = "";
         edtavTfpessoa_endereco_Visible = 1;
         edtavTfpessoa_ie_sel_Jsonclick = "";
         edtavTfpessoa_ie_sel_Visible = 1;
         edtavTfpessoa_ie_Jsonclick = "";
         edtavTfpessoa_ie_Visible = 1;
         edtavTfpessoa_docto_sel_Jsonclick = "";
         edtavTfpessoa_docto_sel_Visible = 1;
         edtavTfpessoa_docto_Jsonclick = "";
         edtavTfpessoa_docto_Visible = 1;
         edtavTfpessoa_nome_sel_Jsonclick = "";
         edtavTfpessoa_nome_sel_Visible = 1;
         edtavTfpessoa_nome_Jsonclick = "";
         edtavTfpessoa_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtavUsuario_pessoacod_Jsonclick = "";
         edtavUsuario_pessoacod_Visible = 1;
         Ddo_pessoa_ativo_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_pessoa_ativo_Datalisttype = "FixedValues";
         Ddo_pessoa_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_pessoa_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_ativo_Titlecontrolidtoreplace = "";
         Ddo_pessoa_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_ativo_Cls = "ColumnSettings";
         Ddo_pessoa_ativo_Tooltip = "Op��es";
         Ddo_pessoa_ativo_Caption = "";
         Ddo_pessoa_fax_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_fax_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_fax_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_fax_Loadingdata = "Carregando dados...";
         Ddo_pessoa_fax_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_fax_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_fax_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_fax_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_fax_Datalisttype = "Dynamic";
         Ddo_pessoa_fax_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_fax_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_fax_Filtertype = "Character";
         Ddo_pessoa_fax_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_fax_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_fax_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_fax_Titlecontrolidtoreplace = "";
         Ddo_pessoa_fax_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_fax_Cls = "ColumnSettings";
         Ddo_pessoa_fax_Tooltip = "Op��es";
         Ddo_pessoa_fax_Caption = "";
         Ddo_pessoa_telefone_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_telefone_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_telefone_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_telefone_Loadingdata = "Carregando dados...";
         Ddo_pessoa_telefone_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_telefone_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_telefone_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_telefone_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_telefone_Datalisttype = "Dynamic";
         Ddo_pessoa_telefone_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_telefone_Filtertype = "Character";
         Ddo_pessoa_telefone_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Titlecontrolidtoreplace = "";
         Ddo_pessoa_telefone_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_telefone_Cls = "ColumnSettings";
         Ddo_pessoa_telefone_Tooltip = "Op��es";
         Ddo_pessoa_telefone_Caption = "";
         Ddo_pessoa_cep_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_cep_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_cep_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_cep_Loadingdata = "Carregando dados...";
         Ddo_pessoa_cep_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_cep_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_cep_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_cep_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_cep_Datalisttype = "Dynamic";
         Ddo_pessoa_cep_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_cep_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_cep_Filtertype = "Character";
         Ddo_pessoa_cep_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_cep_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_cep_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_cep_Titlecontrolidtoreplace = "";
         Ddo_pessoa_cep_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_cep_Cls = "ColumnSettings";
         Ddo_pessoa_cep_Tooltip = "Op��es";
         Ddo_pessoa_cep_Caption = "";
         Ddo_pessoa_uf_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_uf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_uf_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_uf_Loadingdata = "Carregando dados...";
         Ddo_pessoa_uf_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_uf_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_uf_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_uf_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_uf_Datalisttype = "Dynamic";
         Ddo_pessoa_uf_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_uf_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_uf_Filtertype = "Character";
         Ddo_pessoa_uf_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_uf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_uf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_uf_Titlecontrolidtoreplace = "";
         Ddo_pessoa_uf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_uf_Cls = "ColumnSettings";
         Ddo_pessoa_uf_Tooltip = "Op��es";
         Ddo_pessoa_uf_Caption = "";
         Ddo_pessoa_municipiocod_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_municipiocod_Rangefilterto = "At�";
         Ddo_pessoa_municipiocod_Rangefilterfrom = "Desde";
         Ddo_pessoa_municipiocod_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_municipiocod_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_municipiocod_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_municipiocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_pessoa_municipiocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_pessoa_municipiocod_Filtertype = "Numeric";
         Ddo_pessoa_municipiocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_municipiocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_municipiocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_municipiocod_Titlecontrolidtoreplace = "";
         Ddo_pessoa_municipiocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_municipiocod_Cls = "ColumnSettings";
         Ddo_pessoa_municipiocod_Tooltip = "Op��es";
         Ddo_pessoa_municipiocod_Caption = "";
         Ddo_pessoa_endereco_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_endereco_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_endereco_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_endereco_Loadingdata = "Carregando dados...";
         Ddo_pessoa_endereco_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_endereco_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_endereco_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_endereco_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_endereco_Datalisttype = "Dynamic";
         Ddo_pessoa_endereco_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_endereco_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_endereco_Filtertype = "Character";
         Ddo_pessoa_endereco_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_endereco_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_endereco_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_endereco_Titlecontrolidtoreplace = "";
         Ddo_pessoa_endereco_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_endereco_Cls = "ColumnSettings";
         Ddo_pessoa_endereco_Tooltip = "Op��es";
         Ddo_pessoa_endereco_Caption = "";
         Ddo_pessoa_ie_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_ie_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_ie_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_ie_Loadingdata = "Carregando dados...";
         Ddo_pessoa_ie_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_ie_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_ie_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_ie_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_ie_Datalisttype = "Dynamic";
         Ddo_pessoa_ie_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_ie_Filtertype = "Character";
         Ddo_pessoa_ie_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Titlecontrolidtoreplace = "";
         Ddo_pessoa_ie_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_ie_Cls = "ColumnSettings";
         Ddo_pessoa_ie_Tooltip = "Op��es";
         Ddo_pessoa_ie_Caption = "";
         Ddo_pessoa_docto_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_docto_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_docto_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_docto_Loadingdata = "Carregando dados...";
         Ddo_pessoa_docto_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_docto_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_docto_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_docto_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_docto_Datalisttype = "Dynamic";
         Ddo_pessoa_docto_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_docto_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_docto_Filtertype = "Character";
         Ddo_pessoa_docto_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_docto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_docto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_docto_Titlecontrolidtoreplace = "";
         Ddo_pessoa_docto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_docto_Cls = "ColumnSettings";
         Ddo_pessoa_docto_Tooltip = "Op��es";
         Ddo_pessoa_docto_Caption = "";
         Ddo_pessoa_tipopessoa_Searchbuttontext = "Filtrar Selecionados";
         Ddo_pessoa_tipopessoa_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_tipopessoa_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_tipopessoa_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_tipopessoa_Datalistfixedvalues = "F:F�sica,J:Jur�dica";
         Ddo_pessoa_tipopessoa_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_pessoa_tipopessoa_Datalisttype = "FixedValues";
         Ddo_pessoa_tipopessoa_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_tipopessoa_Includefilter = Convert.ToBoolean( 0);
         Ddo_pessoa_tipopessoa_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_tipopessoa_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_tipopessoa_Titlecontrolidtoreplace = "";
         Ddo_pessoa_tipopessoa_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_tipopessoa_Cls = "ColumnSettings";
         Ddo_pessoa_tipopessoa_Tooltip = "Op��es";
         Ddo_pessoa_tipopessoa_Caption = "";
         Ddo_pessoa_nome_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_nome_Loadingdata = "Carregando dados...";
         Ddo_pessoa_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_nome_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_nome_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_nome_Datalistproc = "GetWWPessoaFilterData";
         Ddo_pessoa_nome_Datalisttype = "Dynamic";
         Ddo_pessoa_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_nome_Filtertype = "Character";
         Ddo_pessoa_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_nome_Titlecontrolidtoreplace = "";
         Ddo_pessoa_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_nome_Cls = "ColumnSettings";
         Ddo_pessoa_nome_Tooltip = "Op��es";
         Ddo_pessoa_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Pessoa";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV70Pessoa_NomeTitleFilterData',fld:'vPESSOA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV74Pessoa_TipoPessoaTitleFilterData',fld:'vPESSOA_TIPOPESSOATITLEFILTERDATA',pic:'',nv:null},{av:'AV78Pessoa_DoctoTitleFilterData',fld:'vPESSOA_DOCTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV96Pessoa_IETitleFilterData',fld:'vPESSOA_IETITLEFILTERDATA',pic:'',nv:null},{av:'AV100Pessoa_EnderecoTitleFilterData',fld:'vPESSOA_ENDERECOTITLEFILTERDATA',pic:'',nv:null},{av:'AV104Pessoa_MunicipioCodTitleFilterData',fld:'vPESSOA_MUNICIPIOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV108Pessoa_UFTitleFilterData',fld:'vPESSOA_UFTITLEFILTERDATA',pic:'',nv:null},{av:'AV112Pessoa_CEPTitleFilterData',fld:'vPESSOA_CEPTITLEFILTERDATA',pic:'',nv:null},{av:'AV116Pessoa_TelefoneTitleFilterData',fld:'vPESSOA_TELEFONETITLEFILTERDATA',pic:'',nv:null},{av:'AV120Pessoa_FaxTitleFilterData',fld:'vPESSOA_FAXTITLEFILTERDATA',pic:'',nv:null},{av:'AV82Pessoa_AtivoTitleFilterData',fld:'vPESSOA_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtPessoa_Nome_Titleformat',ctrl:'PESSOA_NOME',prop:'Titleformat'},{av:'edtPessoa_Nome_Title',ctrl:'PESSOA_NOME',prop:'Title'},{av:'cmbPessoa_TipoPessoa'},{av:'edtPessoa_Docto_Titleformat',ctrl:'PESSOA_DOCTO',prop:'Titleformat'},{av:'edtPessoa_Docto_Title',ctrl:'PESSOA_DOCTO',prop:'Title'},{av:'edtPessoa_IE_Titleformat',ctrl:'PESSOA_IE',prop:'Titleformat'},{av:'edtPessoa_IE_Title',ctrl:'PESSOA_IE',prop:'Title'},{av:'edtPessoa_Endereco_Titleformat',ctrl:'PESSOA_ENDERECO',prop:'Titleformat'},{av:'edtPessoa_Endereco_Title',ctrl:'PESSOA_ENDERECO',prop:'Title'},{av:'edtPessoa_MunicipioCod_Titleformat',ctrl:'PESSOA_MUNICIPIOCOD',prop:'Titleformat'},{av:'edtPessoa_MunicipioCod_Title',ctrl:'PESSOA_MUNICIPIOCOD',prop:'Title'},{av:'edtPessoa_UF_Titleformat',ctrl:'PESSOA_UF',prop:'Titleformat'},{av:'edtPessoa_UF_Title',ctrl:'PESSOA_UF',prop:'Title'},{av:'edtPessoa_CEP_Titleformat',ctrl:'PESSOA_CEP',prop:'Titleformat'},{av:'edtPessoa_CEP_Title',ctrl:'PESSOA_CEP',prop:'Title'},{av:'edtPessoa_Telefone_Titleformat',ctrl:'PESSOA_TELEFONE',prop:'Titleformat'},{av:'edtPessoa_Telefone_Title',ctrl:'PESSOA_TELEFONE',prop:'Title'},{av:'edtPessoa_Fax_Titleformat',ctrl:'PESSOA_FAX',prop:'Titleformat'},{av:'edtPessoa_Fax_Title',ctrl:'PESSOA_FAX',prop:'Title'},{av:'chkPessoa_Ativo_Titleformat',ctrl:'PESSOA_ATIVO',prop:'Titleformat'},{av:'chkPessoa_Ativo.Title.Text',ctrl:'PESSOA_ATIVO',prop:'Title'},{av:'AV87GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV88GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV94ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PESSOA_NOME.ONOPTIONCLICKED","{handler:'E13302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_nome_Activeeventkey',ctrl:'DDO_PESSOA_NOME',prop:'ActiveEventKey'},{av:'Ddo_pessoa_nome_Filteredtext_get',ctrl:'DDO_PESSOA_NOME',prop:'FilteredText_get'},{av:'Ddo_pessoa_nome_Selectedvalue_get',ctrl:'DDO_PESSOA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_TIPOPESSOA.ONOPTIONCLICKED","{handler:'E14302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_tipopessoa_Activeeventkey',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'ActiveEventKey'},{av:'Ddo_pessoa_tipopessoa_Selectedvalue_get',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_DOCTO.ONOPTIONCLICKED","{handler:'E15302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_docto_Activeeventkey',ctrl:'DDO_PESSOA_DOCTO',prop:'ActiveEventKey'},{av:'Ddo_pessoa_docto_Filteredtext_get',ctrl:'DDO_PESSOA_DOCTO',prop:'FilteredText_get'},{av:'Ddo_pessoa_docto_Selectedvalue_get',ctrl:'DDO_PESSOA_DOCTO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_IE.ONOPTIONCLICKED","{handler:'E16302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_ie_Activeeventkey',ctrl:'DDO_PESSOA_IE',prop:'ActiveEventKey'},{av:'Ddo_pessoa_ie_Filteredtext_get',ctrl:'DDO_PESSOA_IE',prop:'FilteredText_get'},{av:'Ddo_pessoa_ie_Selectedvalue_get',ctrl:'DDO_PESSOA_IE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_ENDERECO.ONOPTIONCLICKED","{handler:'E17302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_endereco_Activeeventkey',ctrl:'DDO_PESSOA_ENDERECO',prop:'ActiveEventKey'},{av:'Ddo_pessoa_endereco_Filteredtext_get',ctrl:'DDO_PESSOA_ENDERECO',prop:'FilteredText_get'},{av:'Ddo_pessoa_endereco_Selectedvalue_get',ctrl:'DDO_PESSOA_ENDERECO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_MUNICIPIOCOD.ONOPTIONCLICKED","{handler:'E18302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_municipiocod_Activeeventkey',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'ActiveEventKey'},{av:'Ddo_pessoa_municipiocod_Filteredtext_get',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'FilteredText_get'},{av:'Ddo_pessoa_municipiocod_Filteredtextto_get',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_UF.ONOPTIONCLICKED","{handler:'E19302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_uf_Activeeventkey',ctrl:'DDO_PESSOA_UF',prop:'ActiveEventKey'},{av:'Ddo_pessoa_uf_Filteredtext_get',ctrl:'DDO_PESSOA_UF',prop:'FilteredText_get'},{av:'Ddo_pessoa_uf_Selectedvalue_get',ctrl:'DDO_PESSOA_UF',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_CEP.ONOPTIONCLICKED","{handler:'E20302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_cep_Activeeventkey',ctrl:'DDO_PESSOA_CEP',prop:'ActiveEventKey'},{av:'Ddo_pessoa_cep_Filteredtext_get',ctrl:'DDO_PESSOA_CEP',prop:'FilteredText_get'},{av:'Ddo_pessoa_cep_Selectedvalue_get',ctrl:'DDO_PESSOA_CEP',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_TELEFONE.ONOPTIONCLICKED","{handler:'E21302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_telefone_Activeeventkey',ctrl:'DDO_PESSOA_TELEFONE',prop:'ActiveEventKey'},{av:'Ddo_pessoa_telefone_Filteredtext_get',ctrl:'DDO_PESSOA_TELEFONE',prop:'FilteredText_get'},{av:'Ddo_pessoa_telefone_Selectedvalue_get',ctrl:'DDO_PESSOA_TELEFONE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_FAX.ONOPTIONCLICKED","{handler:'E22302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_fax_Activeeventkey',ctrl:'DDO_PESSOA_FAX',prop:'ActiveEventKey'},{av:'Ddo_pessoa_fax_Filteredtext_get',ctrl:'DDO_PESSOA_FAX',prop:'FilteredText_get'},{av:'Ddo_pessoa_fax_Selectedvalue_get',ctrl:'DDO_PESSOA_FAX',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_ATIVO.ONOPTIONCLICKED","{handler:'E23302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pessoa_ativo_Activeeventkey',ctrl:'DDO_PESSOA_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_pessoa_ativo_Selectedvalue_get',ctrl:'DDO_PESSOA_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E36302',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV89Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E24302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E29302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E25302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'edtavPessoa_nome2_Visible',ctrl:'vPESSOA_NOME2',prop:'Visible'},{av:'cmbavPessoa_tipopessoa2'},{av:'edtavPessoa_docto2_Visible',ctrl:'vPESSOA_DOCTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPessoa_nome3_Visible',ctrl:'vPESSOA_NOME3',prop:'Visible'},{av:'cmbavPessoa_tipopessoa3'},{av:'edtavPessoa_docto3_Visible',ctrl:'vPESSOA_DOCTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavPessoa_nome1_Visible',ctrl:'vPESSOA_NOME1',prop:'Visible'},{av:'cmbavPessoa_tipopessoa1'},{av:'edtavPessoa_docto1_Visible',ctrl:'vPESSOA_DOCTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E30302',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavPessoa_nome1_Visible',ctrl:'vPESSOA_NOME1',prop:'Visible'},{av:'cmbavPessoa_tipopessoa1'},{av:'edtavPessoa_docto1_Visible',ctrl:'vPESSOA_DOCTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E31302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E26302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'edtavPessoa_nome2_Visible',ctrl:'vPESSOA_NOME2',prop:'Visible'},{av:'cmbavPessoa_tipopessoa2'},{av:'edtavPessoa_docto2_Visible',ctrl:'vPESSOA_DOCTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPessoa_nome3_Visible',ctrl:'vPESSOA_NOME3',prop:'Visible'},{av:'cmbavPessoa_tipopessoa3'},{av:'edtavPessoa_docto3_Visible',ctrl:'vPESSOA_DOCTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavPessoa_nome1_Visible',ctrl:'vPESSOA_NOME1',prop:'Visible'},{av:'cmbavPessoa_tipopessoa1'},{av:'edtavPessoa_docto1_Visible',ctrl:'vPESSOA_DOCTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E32302',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavPessoa_nome2_Visible',ctrl:'vPESSOA_NOME2',prop:'Visible'},{av:'cmbavPessoa_tipopessoa2'},{av:'edtavPessoa_docto2_Visible',ctrl:'vPESSOA_DOCTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E27302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'edtavPessoa_nome2_Visible',ctrl:'vPESSOA_NOME2',prop:'Visible'},{av:'cmbavPessoa_tipopessoa2'},{av:'edtavPessoa_docto2_Visible',ctrl:'vPESSOA_DOCTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPessoa_nome3_Visible',ctrl:'vPESSOA_NOME3',prop:'Visible'},{av:'cmbavPessoa_tipopessoa3'},{av:'edtavPessoa_docto3_Visible',ctrl:'vPESSOA_DOCTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavPessoa_nome1_Visible',ctrl:'vPESSOA_NOME1',prop:'Visible'},{av:'cmbavPessoa_tipopessoa1'},{av:'edtavPessoa_docto1_Visible',ctrl:'vPESSOA_DOCTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E33302',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavPessoa_nome3_Visible',ctrl:'vPESSOA_NOME3',prop:'Visible'},{av:'cmbavPessoa_tipopessoa3'},{av:'edtavPessoa_docto3_Visible',ctrl:'vPESSOA_DOCTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'Ddo_pessoa_nome_Filteredtext_set',ctrl:'DDO_PESSOA_NOME',prop:'FilteredText_set'},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pessoa_nome_Selectedvalue_set',ctrl:'DDO_PESSOA_NOME',prop:'SelectedValue_set'},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'Ddo_pessoa_tipopessoa_Selectedvalue_set',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SelectedValue_set'},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'Ddo_pessoa_docto_Filteredtext_set',ctrl:'DDO_PESSOA_DOCTO',prop:'FilteredText_set'},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'Ddo_pessoa_docto_Selectedvalue_set',ctrl:'DDO_PESSOA_DOCTO',prop:'SelectedValue_set'},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'Ddo_pessoa_ie_Filteredtext_set',ctrl:'DDO_PESSOA_IE',prop:'FilteredText_set'},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_ie_Selectedvalue_set',ctrl:'DDO_PESSOA_IE',prop:'SelectedValue_set'},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'Ddo_pessoa_endereco_Filteredtext_set',ctrl:'DDO_PESSOA_ENDERECO',prop:'FilteredText_set'},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'Ddo_pessoa_endereco_Selectedvalue_set',ctrl:'DDO_PESSOA_ENDERECO',prop:'SelectedValue_set'},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoa_municipiocod_Filteredtext_set',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'FilteredText_set'},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoa_municipiocod_Filteredtextto_set',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'FilteredTextTo_set'},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'Ddo_pessoa_uf_Filteredtext_set',ctrl:'DDO_PESSOA_UF',prop:'FilteredText_set'},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'Ddo_pessoa_uf_Selectedvalue_set',ctrl:'DDO_PESSOA_UF',prop:'SelectedValue_set'},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'Ddo_pessoa_cep_Filteredtext_set',ctrl:'DDO_PESSOA_CEP',prop:'FilteredText_set'},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'Ddo_pessoa_cep_Selectedvalue_set',ctrl:'DDO_PESSOA_CEP',prop:'SelectedValue_set'},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'Ddo_pessoa_telefone_Filteredtext_set',ctrl:'DDO_PESSOA_TELEFONE',prop:'FilteredText_set'},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_telefone_Selectedvalue_set',ctrl:'DDO_PESSOA_TELEFONE',prop:'SelectedValue_set'},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'Ddo_pessoa_fax_Filteredtext_set',ctrl:'DDO_PESSOA_FAX',prop:'FilteredText_set'},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'Ddo_pessoa_fax_Selectedvalue_set',ctrl:'DDO_PESSOA_FAX',prop:'SelectedValue_set'},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_pessoa_ativo_Selectedvalue_set',ctrl:'DDO_PESSOA_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'Ddo_pessoa_ativo_Sortedstatus',ctrl:'DDO_PESSOA_ATIVO',prop:'SortedStatus'},{av:'Ddo_pessoa_fax_Sortedstatus',ctrl:'DDO_PESSOA_FAX',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_pessoa_cep_Sortedstatus',ctrl:'DDO_PESSOA_CEP',prop:'SortedStatus'},{av:'Ddo_pessoa_uf_Sortedstatus',ctrl:'DDO_PESSOA_UF',prop:'SortedStatus'},{av:'Ddo_pessoa_municipiocod_Sortedstatus',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'SortedStatus'},{av:'Ddo_pessoa_endereco_Sortedstatus',ctrl:'DDO_PESSOA_ENDERECO',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_docto_Sortedstatus',ctrl:'DDO_PESSOA_DOCTO',prop:'SortedStatus'},{av:'Ddo_pessoa_tipopessoa_Sortedstatus',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SortedStatus'},{av:'Ddo_pessoa_nome_Sortedstatus',ctrl:'DDO_PESSOA_NOME',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'edtavPessoa_nome1_Visible',ctrl:'vPESSOA_NOME1',prop:'Visible'},{av:'cmbavPessoa_tipopessoa1'},{av:'edtavPessoa_docto1_Visible',ctrl:'vPESSOA_DOCTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavPessoa_nome2_Visible',ctrl:'vPESSOA_NOME2',prop:'Visible'},{av:'cmbavPessoa_tipopessoa2'},{av:'edtavPessoa_docto2_Visible',ctrl:'vPESSOA_DOCTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPessoa_nome3_Visible',ctrl:'vPESSOA_NOME3',prop:'Visible'},{av:'cmbavPessoa_tipopessoa3'},{av:'edtavPessoa_docto3_Visible',ctrl:'vPESSOA_DOCTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E28302',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'AV69Usuario_PessoaCod',fld:'vUSUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'AV90ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV73ddo_Pessoa_NomeTitleControlIdToReplace',fld:'vDDO_PESSOA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace',fld:'vDDO_PESSOA_TIPOPESSOATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Pessoa_DoctoTitleControlIdToReplace',fld:'vDDO_PESSOA_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV99ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV103ddo_Pessoa_EnderecoTitleControlIdToReplace',fld:'vDDO_PESSOA_ENDERECOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace',fld:'vDDO_PESSOA_MUNICIPIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Pessoa_UFTitleControlIdToReplace',fld:'vDDO_PESSOA_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV115ddo_Pessoa_CEPTitleControlIdToReplace',fld:'vDDO_PESSOA_CEPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123ddo_Pessoa_FaxTitleControlIdToReplace',fld:'vDDO_PESSOA_FAXTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Pessoa_AtivoTitleControlIdToReplace',fld:'vDDO_PESSOA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'AV167Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV71TFPessoa_Nome',fld:'vTFPESSOA_NOME',pic:'@!',nv:''},{av:'Ddo_pessoa_nome_Filteredtext_set',ctrl:'DDO_PESSOA_NOME',prop:'FilteredText_set'},{av:'AV72TFPessoa_Nome_Sel',fld:'vTFPESSOA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pessoa_nome_Selectedvalue_set',ctrl:'DDO_PESSOA_NOME',prop:'SelectedValue_set'},{av:'AV76TFPessoa_TipoPessoa_Sels',fld:'vTFPESSOA_TIPOPESSOA_SELS',pic:'',nv:null},{av:'Ddo_pessoa_tipopessoa_Selectedvalue_set',ctrl:'DDO_PESSOA_TIPOPESSOA',prop:'SelectedValue_set'},{av:'AV79TFPessoa_Docto',fld:'vTFPESSOA_DOCTO',pic:'',nv:''},{av:'Ddo_pessoa_docto_Filteredtext_set',ctrl:'DDO_PESSOA_DOCTO',prop:'FilteredText_set'},{av:'AV80TFPessoa_Docto_Sel',fld:'vTFPESSOA_DOCTO_SEL',pic:'',nv:''},{av:'Ddo_pessoa_docto_Selectedvalue_set',ctrl:'DDO_PESSOA_DOCTO',prop:'SelectedValue_set'},{av:'AV97TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'Ddo_pessoa_ie_Filteredtext_set',ctrl:'DDO_PESSOA_IE',prop:'FilteredText_set'},{av:'AV98TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_ie_Selectedvalue_set',ctrl:'DDO_PESSOA_IE',prop:'SelectedValue_set'},{av:'AV101TFPessoa_Endereco',fld:'vTFPESSOA_ENDERECO',pic:'',nv:''},{av:'Ddo_pessoa_endereco_Filteredtext_set',ctrl:'DDO_PESSOA_ENDERECO',prop:'FilteredText_set'},{av:'AV102TFPessoa_Endereco_Sel',fld:'vTFPESSOA_ENDERECO_SEL',pic:'',nv:''},{av:'Ddo_pessoa_endereco_Selectedvalue_set',ctrl:'DDO_PESSOA_ENDERECO',prop:'SelectedValue_set'},{av:'AV105TFPessoa_MunicipioCod',fld:'vTFPESSOA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoa_municipiocod_Filteredtext_set',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'FilteredText_set'},{av:'AV106TFPessoa_MunicipioCod_To',fld:'vTFPESSOA_MUNICIPIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoa_municipiocod_Filteredtextto_set',ctrl:'DDO_PESSOA_MUNICIPIOCOD',prop:'FilteredTextTo_set'},{av:'AV109TFPessoa_UF',fld:'vTFPESSOA_UF',pic:'@!',nv:''},{av:'Ddo_pessoa_uf_Filteredtext_set',ctrl:'DDO_PESSOA_UF',prop:'FilteredText_set'},{av:'AV110TFPessoa_UF_Sel',fld:'vTFPESSOA_UF_SEL',pic:'@!',nv:''},{av:'Ddo_pessoa_uf_Selectedvalue_set',ctrl:'DDO_PESSOA_UF',prop:'SelectedValue_set'},{av:'AV113TFPessoa_CEP',fld:'vTFPESSOA_CEP',pic:'',nv:''},{av:'Ddo_pessoa_cep_Filteredtext_set',ctrl:'DDO_PESSOA_CEP',prop:'FilteredText_set'},{av:'AV114TFPessoa_CEP_Sel',fld:'vTFPESSOA_CEP_SEL',pic:'',nv:''},{av:'Ddo_pessoa_cep_Selectedvalue_set',ctrl:'DDO_PESSOA_CEP',prop:'SelectedValue_set'},{av:'AV117TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'Ddo_pessoa_telefone_Filteredtext_set',ctrl:'DDO_PESSOA_TELEFONE',prop:'FilteredText_set'},{av:'AV118TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_telefone_Selectedvalue_set',ctrl:'DDO_PESSOA_TELEFONE',prop:'SelectedValue_set'},{av:'AV121TFPessoa_Fax',fld:'vTFPESSOA_FAX',pic:'',nv:''},{av:'Ddo_pessoa_fax_Filteredtext_set',ctrl:'DDO_PESSOA_FAX',prop:'FilteredText_set'},{av:'AV122TFPessoa_Fax_Sel',fld:'vTFPESSOA_FAX_SEL',pic:'',nv:''},{av:'Ddo_pessoa_fax_Selectedvalue_set',ctrl:'DDO_PESSOA_FAX',prop:'SelectedValue_set'},{av:'AV83TFPessoa_Ativo_Sel',fld:'vTFPESSOA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_pessoa_ativo_Selectedvalue_set',ctrl:'DDO_PESSOA_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pessoa_Nome1',fld:'vPESSOA_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavPessoa_nome1_Visible',ctrl:'vPESSOA_NOME1',prop:'Visible'},{av:'cmbavPessoa_tipopessoa1'},{av:'edtavPessoa_docto1_Visible',ctrl:'vPESSOA_DOCTO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Pessoa_Nome2',fld:'vPESSOA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Pessoa_Nome3',fld:'vPESSOA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV44Pessoa_TipoPessoa1',fld:'vPESSOA_TIPOPESSOA1',pic:'',nv:''},{av:'AV39Pessoa_Docto1',fld:'vPESSOA_DOCTO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV45Pessoa_TipoPessoa2',fld:'vPESSOA_TIPOPESSOA2',pic:'',nv:''},{av:'AV41Pessoa_Docto2',fld:'vPESSOA_DOCTO2',pic:'',nv:''},{av:'AV46Pessoa_TipoPessoa3',fld:'vPESSOA_TIPOPESSOA3',pic:'',nv:''},{av:'AV43Pessoa_Docto3',fld:'vPESSOA_DOCTO3',pic:'',nv:''},{av:'edtavPessoa_nome2_Visible',ctrl:'vPESSOA_NOME2',prop:'Visible'},{av:'cmbavPessoa_tipopessoa2'},{av:'edtavPessoa_docto2_Visible',ctrl:'vPESSOA_DOCTO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPessoa_nome3_Visible',ctrl:'vPESSOA_NOME3',prop:'Visible'},{av:'cmbavPessoa_tipopessoa3'},{av:'edtavPessoa_docto3_Visible',ctrl:'vPESSOA_DOCTO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_pessoa_nome_Activeeventkey = "";
         Ddo_pessoa_nome_Filteredtext_get = "";
         Ddo_pessoa_nome_Selectedvalue_get = "";
         Ddo_pessoa_tipopessoa_Activeeventkey = "";
         Ddo_pessoa_tipopessoa_Selectedvalue_get = "";
         Ddo_pessoa_docto_Activeeventkey = "";
         Ddo_pessoa_docto_Filteredtext_get = "";
         Ddo_pessoa_docto_Selectedvalue_get = "";
         Ddo_pessoa_ie_Activeeventkey = "";
         Ddo_pessoa_ie_Filteredtext_get = "";
         Ddo_pessoa_ie_Selectedvalue_get = "";
         Ddo_pessoa_endereco_Activeeventkey = "";
         Ddo_pessoa_endereco_Filteredtext_get = "";
         Ddo_pessoa_endereco_Selectedvalue_get = "";
         Ddo_pessoa_municipiocod_Activeeventkey = "";
         Ddo_pessoa_municipiocod_Filteredtext_get = "";
         Ddo_pessoa_municipiocod_Filteredtextto_get = "";
         Ddo_pessoa_uf_Activeeventkey = "";
         Ddo_pessoa_uf_Filteredtext_get = "";
         Ddo_pessoa_uf_Selectedvalue_get = "";
         Ddo_pessoa_cep_Activeeventkey = "";
         Ddo_pessoa_cep_Filteredtext_get = "";
         Ddo_pessoa_cep_Selectedvalue_get = "";
         Ddo_pessoa_telefone_Activeeventkey = "";
         Ddo_pessoa_telefone_Filteredtext_get = "";
         Ddo_pessoa_telefone_Selectedvalue_get = "";
         Ddo_pessoa_fax_Activeeventkey = "";
         Ddo_pessoa_fax_Filteredtext_get = "";
         Ddo_pessoa_fax_Selectedvalue_get = "";
         Ddo_pessoa_ativo_Activeeventkey = "";
         Ddo_pessoa_ativo_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Pessoa_Nome1 = "";
         AV44Pessoa_TipoPessoa1 = "";
         AV39Pessoa_Docto1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22Pessoa_Nome2 = "";
         AV45Pessoa_TipoPessoa2 = "";
         AV41Pessoa_Docto2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27Pessoa_Nome3 = "";
         AV46Pessoa_TipoPessoa3 = "";
         AV43Pessoa_Docto3 = "";
         AV71TFPessoa_Nome = "";
         AV72TFPessoa_Nome_Sel = "";
         AV79TFPessoa_Docto = "";
         AV80TFPessoa_Docto_Sel = "";
         AV97TFPessoa_IE = "";
         AV98TFPessoa_IE_Sel = "";
         AV101TFPessoa_Endereco = "";
         AV102TFPessoa_Endereco_Sel = "";
         AV109TFPessoa_UF = "";
         AV110TFPessoa_UF_Sel = "";
         AV113TFPessoa_CEP = "";
         AV114TFPessoa_CEP_Sel = "";
         AV117TFPessoa_Telefone = "";
         AV118TFPessoa_Telefone_Sel = "";
         AV121TFPessoa_Fax = "";
         AV122TFPessoa_Fax_Sel = "";
         AV73ddo_Pessoa_NomeTitleControlIdToReplace = "";
         AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace = "";
         AV81ddo_Pessoa_DoctoTitleControlIdToReplace = "";
         AV99ddo_Pessoa_IETitleControlIdToReplace = "";
         AV103ddo_Pessoa_EnderecoTitleControlIdToReplace = "";
         AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace = "";
         AV111ddo_Pessoa_UFTitleControlIdToReplace = "";
         AV115ddo_Pessoa_CEPTitleControlIdToReplace = "";
         AV119ddo_Pessoa_TelefoneTitleControlIdToReplace = "";
         AV123ddo_Pessoa_FaxTitleControlIdToReplace = "";
         AV84ddo_Pessoa_AtivoTitleControlIdToReplace = "";
         AV76TFPessoa_TipoPessoa_Sels = new GxSimpleCollection();
         AV167Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV94ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV70Pessoa_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Pessoa_TipoPessoaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Pessoa_DoctoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV96Pessoa_IETitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV100Pessoa_EnderecoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV104Pessoa_MunicipioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV108Pessoa_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV112Pessoa_CEPTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV116Pessoa_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV120Pessoa_FaxTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Pessoa_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_pessoa_nome_Filteredtext_set = "";
         Ddo_pessoa_nome_Selectedvalue_set = "";
         Ddo_pessoa_nome_Sortedstatus = "";
         Ddo_pessoa_tipopessoa_Selectedvalue_set = "";
         Ddo_pessoa_tipopessoa_Sortedstatus = "";
         Ddo_pessoa_docto_Filteredtext_set = "";
         Ddo_pessoa_docto_Selectedvalue_set = "";
         Ddo_pessoa_docto_Sortedstatus = "";
         Ddo_pessoa_ie_Filteredtext_set = "";
         Ddo_pessoa_ie_Selectedvalue_set = "";
         Ddo_pessoa_ie_Sortedstatus = "";
         Ddo_pessoa_endereco_Filteredtext_set = "";
         Ddo_pessoa_endereco_Selectedvalue_set = "";
         Ddo_pessoa_endereco_Sortedstatus = "";
         Ddo_pessoa_municipiocod_Filteredtext_set = "";
         Ddo_pessoa_municipiocod_Filteredtextto_set = "";
         Ddo_pessoa_municipiocod_Sortedstatus = "";
         Ddo_pessoa_uf_Filteredtext_set = "";
         Ddo_pessoa_uf_Selectedvalue_set = "";
         Ddo_pessoa_uf_Sortedstatus = "";
         Ddo_pessoa_cep_Filteredtext_set = "";
         Ddo_pessoa_cep_Selectedvalue_set = "";
         Ddo_pessoa_cep_Sortedstatus = "";
         Ddo_pessoa_telefone_Filteredtext_set = "";
         Ddo_pessoa_telefone_Selectedvalue_set = "";
         Ddo_pessoa_telefone_Sortedstatus = "";
         Ddo_pessoa_fax_Filteredtext_set = "";
         Ddo_pessoa_fax_Selectedvalue_set = "";
         Ddo_pessoa_fax_Sortedstatus = "";
         Ddo_pessoa_ativo_Selectedvalue_set = "";
         Ddo_pessoa_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV164Update_GXI = "";
         AV32Delete = "";
         AV165Delete_GXI = "";
         AV89Display = "";
         AV166Display_GXI = "";
         A35Pessoa_Nome = "";
         A36Pessoa_TipoPessoa = "";
         A37Pessoa_Docto = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A520Pessoa_UF = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV129WWPessoaDS_3_Pessoa_nome1 = "";
         lV131WWPessoaDS_5_Pessoa_docto1 = "";
         lV135WWPessoaDS_9_Pessoa_nome2 = "";
         lV137WWPessoaDS_11_Pessoa_docto2 = "";
         lV141WWPessoaDS_15_Pessoa_nome3 = "";
         lV143WWPessoaDS_17_Pessoa_docto3 = "";
         lV144WWPessoaDS_18_Tfpessoa_nome = "";
         lV147WWPessoaDS_21_Tfpessoa_docto = "";
         lV149WWPessoaDS_23_Tfpessoa_ie = "";
         lV151WWPessoaDS_25_Tfpessoa_endereco = "";
         lV155WWPessoaDS_29_Tfpessoa_uf = "";
         lV157WWPessoaDS_31_Tfpessoa_cep = "";
         lV159WWPessoaDS_33_Tfpessoa_telefone = "";
         lV161WWPessoaDS_35_Tfpessoa_fax = "";
         AV127WWPessoaDS_1_Dynamicfiltersselector1 = "";
         AV129WWPessoaDS_3_Pessoa_nome1 = "";
         AV130WWPessoaDS_4_Pessoa_tipopessoa1 = "";
         AV131WWPessoaDS_5_Pessoa_docto1 = "";
         AV133WWPessoaDS_7_Dynamicfiltersselector2 = "";
         AV135WWPessoaDS_9_Pessoa_nome2 = "";
         AV136WWPessoaDS_10_Pessoa_tipopessoa2 = "";
         AV137WWPessoaDS_11_Pessoa_docto2 = "";
         AV139WWPessoaDS_13_Dynamicfiltersselector3 = "";
         AV141WWPessoaDS_15_Pessoa_nome3 = "";
         AV142WWPessoaDS_16_Pessoa_tipopessoa3 = "";
         AV143WWPessoaDS_17_Pessoa_docto3 = "";
         AV145WWPessoaDS_19_Tfpessoa_nome_sel = "";
         AV144WWPessoaDS_18_Tfpessoa_nome = "";
         AV148WWPessoaDS_22_Tfpessoa_docto_sel = "";
         AV147WWPessoaDS_21_Tfpessoa_docto = "";
         AV150WWPessoaDS_24_Tfpessoa_ie_sel = "";
         AV149WWPessoaDS_23_Tfpessoa_ie = "";
         AV152WWPessoaDS_26_Tfpessoa_endereco_sel = "";
         AV151WWPessoaDS_25_Tfpessoa_endereco = "";
         AV156WWPessoaDS_30_Tfpessoa_uf_sel = "";
         AV155WWPessoaDS_29_Tfpessoa_uf = "";
         AV158WWPessoaDS_32_Tfpessoa_cep_sel = "";
         AV157WWPessoaDS_31_Tfpessoa_cep = "";
         AV160WWPessoaDS_34_Tfpessoa_telefone_sel = "";
         AV159WWPessoaDS_33_Tfpessoa_telefone = "";
         AV162WWPessoaDS_36_Tfpessoa_fax_sel = "";
         AV161WWPessoaDS_35_Tfpessoa_fax = "";
         H00302_A38Pessoa_Ativo = new bool[] {false} ;
         H00302_A523Pessoa_Fax = new String[] {""} ;
         H00302_n523Pessoa_Fax = new bool[] {false} ;
         H00302_A522Pessoa_Telefone = new String[] {""} ;
         H00302_n522Pessoa_Telefone = new bool[] {false} ;
         H00302_A521Pessoa_CEP = new String[] {""} ;
         H00302_n521Pessoa_CEP = new bool[] {false} ;
         H00302_A520Pessoa_UF = new String[] {""} ;
         H00302_n520Pessoa_UF = new bool[] {false} ;
         H00302_A503Pessoa_MunicipioCod = new int[1] ;
         H00302_n503Pessoa_MunicipioCod = new bool[] {false} ;
         H00302_A519Pessoa_Endereco = new String[] {""} ;
         H00302_n519Pessoa_Endereco = new bool[] {false} ;
         H00302_A518Pessoa_IE = new String[] {""} ;
         H00302_n518Pessoa_IE = new bool[] {false} ;
         H00302_A37Pessoa_Docto = new String[] {""} ;
         H00302_A36Pessoa_TipoPessoa = new String[] {""} ;
         H00302_A35Pessoa_Nome = new String[] {""} ;
         H00302_A34Pessoa_Codigo = new int[1] ;
         H00303_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H00304_A1Usuario_Codigo = new int[1] ;
         H00304_A57Usuario_PessoaCod = new int[1] ;
         AV75TFPessoa_TipoPessoa_SelsJson = "";
         GridRow = new GXWebRow();
         AV91ManageFiltersXml = "";
         GXt_char2 = "";
         AV95ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV92ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV93ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV35Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblPessoatitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwpessoa__default(),
            new Object[][] {
                new Object[] {
               H00302_A38Pessoa_Ativo, H00302_A523Pessoa_Fax, H00302_n523Pessoa_Fax, H00302_A522Pessoa_Telefone, H00302_n522Pessoa_Telefone, H00302_A521Pessoa_CEP, H00302_n521Pessoa_CEP, H00302_A520Pessoa_UF, H00302_n520Pessoa_UF, H00302_A503Pessoa_MunicipioCod,
               H00302_n503Pessoa_MunicipioCod, H00302_A519Pessoa_Endereco, H00302_n519Pessoa_Endereco, H00302_A518Pessoa_IE, H00302_n518Pessoa_IE, H00302_A37Pessoa_Docto, H00302_A36Pessoa_TipoPessoa, H00302_A35Pessoa_Nome, H00302_A34Pessoa_Codigo
               }
               , new Object[] {
               H00303_AGRID_nRecordCount
               }
               , new Object[] {
               H00304_A1Usuario_Codigo, H00304_A57Usuario_PessoaCod
               }
            }
         );
         AV167Pgmname = "WWPessoa";
         /* GeneXus formulas. */
         AV167Pgmname = "WWPessoa";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_97 ;
      private short nGXsfl_97_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV83TFPessoa_Ativo_Sel ;
      private short AV90ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_97_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV128WWPessoaDS_2_Dynamicfiltersoperator1 ;
      private short AV134WWPessoaDS_8_Dynamicfiltersoperator2 ;
      private short AV140WWPessoaDS_14_Dynamicfiltersoperator3 ;
      private short AV163WWPessoaDS_37_Tfpessoa_ativo_sel ;
      private short edtPessoa_Nome_Titleformat ;
      private short cmbPessoa_TipoPessoa_Titleformat ;
      private short edtPessoa_Docto_Titleformat ;
      private short edtPessoa_IE_Titleformat ;
      private short edtPessoa_Endereco_Titleformat ;
      private short edtPessoa_MunicipioCod_Titleformat ;
      private short edtPessoa_UF_Titleformat ;
      private short edtPessoa_CEP_Titleformat ;
      private short edtPessoa_Telefone_Titleformat ;
      private short edtPessoa_Fax_Titleformat ;
      private short chkPessoa_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV69Usuario_PessoaCod ;
      private int AV105TFPessoa_MunicipioCod ;
      private int AV106TFPessoa_MunicipioCod_To ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A34Pessoa_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_pessoa_nome_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_docto_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_ie_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_endereco_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_uf_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_cep_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_telefone_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_fax_Datalistupdateminimumcharacters ;
      private int edtavUsuario_pessoacod_Visible ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfpessoa_nome_Visible ;
      private int edtavTfpessoa_nome_sel_Visible ;
      private int edtavTfpessoa_docto_Visible ;
      private int edtavTfpessoa_docto_sel_Visible ;
      private int edtavTfpessoa_ie_Visible ;
      private int edtavTfpessoa_ie_sel_Visible ;
      private int edtavTfpessoa_endereco_Visible ;
      private int edtavTfpessoa_endereco_sel_Visible ;
      private int edtavTfpessoa_municipiocod_Visible ;
      private int edtavTfpessoa_municipiocod_to_Visible ;
      private int edtavTfpessoa_uf_Visible ;
      private int edtavTfpessoa_uf_sel_Visible ;
      private int edtavTfpessoa_cep_Visible ;
      private int edtavTfpessoa_cep_sel_Visible ;
      private int edtavTfpessoa_telefone_Visible ;
      private int edtavTfpessoa_telefone_sel_Visible ;
      private int edtavTfpessoa_fax_Visible ;
      private int edtavTfpessoa_fax_sel_Visible ;
      private int edtavTfpessoa_ativo_sel_Visible ;
      private int edtavDdo_pessoa_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_doctotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_uftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_ceptitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_faxtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_ativotitlecontrolidtoreplace_Visible ;
      private int A503Pessoa_MunicipioCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ;
      private int AV153WWPessoaDS_27_Tfpessoa_municipiocod ;
      private int AV154WWPessoaDS_28_Tfpessoa_municipiocod_to ;
      private int edtavOrdereddsc_Visible ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int AV86PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavPessoa_nome1_Visible ;
      private int edtavPessoa_docto1_Visible ;
      private int edtavPessoa_nome2_Visible ;
      private int edtavPessoa_docto2_Visible ;
      private int edtavPessoa_nome3_Visible ;
      private int edtavPessoa_docto3_Visible ;
      private int AV168GXV1 ;
      private int AV169GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV87GridCurrentPage ;
      private long AV88GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_pessoa_nome_Activeeventkey ;
      private String Ddo_pessoa_nome_Filteredtext_get ;
      private String Ddo_pessoa_nome_Selectedvalue_get ;
      private String Ddo_pessoa_tipopessoa_Activeeventkey ;
      private String Ddo_pessoa_tipopessoa_Selectedvalue_get ;
      private String Ddo_pessoa_docto_Activeeventkey ;
      private String Ddo_pessoa_docto_Filteredtext_get ;
      private String Ddo_pessoa_docto_Selectedvalue_get ;
      private String Ddo_pessoa_ie_Activeeventkey ;
      private String Ddo_pessoa_ie_Filteredtext_get ;
      private String Ddo_pessoa_ie_Selectedvalue_get ;
      private String Ddo_pessoa_endereco_Activeeventkey ;
      private String Ddo_pessoa_endereco_Filteredtext_get ;
      private String Ddo_pessoa_endereco_Selectedvalue_get ;
      private String Ddo_pessoa_municipiocod_Activeeventkey ;
      private String Ddo_pessoa_municipiocod_Filteredtext_get ;
      private String Ddo_pessoa_municipiocod_Filteredtextto_get ;
      private String Ddo_pessoa_uf_Activeeventkey ;
      private String Ddo_pessoa_uf_Filteredtext_get ;
      private String Ddo_pessoa_uf_Selectedvalue_get ;
      private String Ddo_pessoa_cep_Activeeventkey ;
      private String Ddo_pessoa_cep_Filteredtext_get ;
      private String Ddo_pessoa_cep_Selectedvalue_get ;
      private String Ddo_pessoa_telefone_Activeeventkey ;
      private String Ddo_pessoa_telefone_Filteredtext_get ;
      private String Ddo_pessoa_telefone_Selectedvalue_get ;
      private String Ddo_pessoa_fax_Activeeventkey ;
      private String Ddo_pessoa_fax_Filteredtext_get ;
      private String Ddo_pessoa_fax_Selectedvalue_get ;
      private String Ddo_pessoa_ativo_Activeeventkey ;
      private String Ddo_pessoa_ativo_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_97_idx="0001" ;
      private String AV17Pessoa_Nome1 ;
      private String AV44Pessoa_TipoPessoa1 ;
      private String AV22Pessoa_Nome2 ;
      private String AV45Pessoa_TipoPessoa2 ;
      private String AV27Pessoa_Nome3 ;
      private String AV46Pessoa_TipoPessoa3 ;
      private String AV71TFPessoa_Nome ;
      private String AV72TFPessoa_Nome_Sel ;
      private String AV97TFPessoa_IE ;
      private String AV98TFPessoa_IE_Sel ;
      private String AV109TFPessoa_UF ;
      private String AV110TFPessoa_UF_Sel ;
      private String AV113TFPessoa_CEP ;
      private String AV114TFPessoa_CEP_Sel ;
      private String AV117TFPessoa_Telefone ;
      private String AV118TFPessoa_Telefone_Sel ;
      private String AV121TFPessoa_Fax ;
      private String AV122TFPessoa_Fax_Sel ;
      private String AV167Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_pessoa_nome_Caption ;
      private String Ddo_pessoa_nome_Tooltip ;
      private String Ddo_pessoa_nome_Cls ;
      private String Ddo_pessoa_nome_Filteredtext_set ;
      private String Ddo_pessoa_nome_Selectedvalue_set ;
      private String Ddo_pessoa_nome_Dropdownoptionstype ;
      private String Ddo_pessoa_nome_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_nome_Sortedstatus ;
      private String Ddo_pessoa_nome_Filtertype ;
      private String Ddo_pessoa_nome_Datalisttype ;
      private String Ddo_pessoa_nome_Datalistproc ;
      private String Ddo_pessoa_nome_Sortasc ;
      private String Ddo_pessoa_nome_Sortdsc ;
      private String Ddo_pessoa_nome_Loadingdata ;
      private String Ddo_pessoa_nome_Cleanfilter ;
      private String Ddo_pessoa_nome_Noresultsfound ;
      private String Ddo_pessoa_nome_Searchbuttontext ;
      private String Ddo_pessoa_tipopessoa_Caption ;
      private String Ddo_pessoa_tipopessoa_Tooltip ;
      private String Ddo_pessoa_tipopessoa_Cls ;
      private String Ddo_pessoa_tipopessoa_Selectedvalue_set ;
      private String Ddo_pessoa_tipopessoa_Dropdownoptionstype ;
      private String Ddo_pessoa_tipopessoa_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_tipopessoa_Sortedstatus ;
      private String Ddo_pessoa_tipopessoa_Datalisttype ;
      private String Ddo_pessoa_tipopessoa_Datalistfixedvalues ;
      private String Ddo_pessoa_tipopessoa_Sortasc ;
      private String Ddo_pessoa_tipopessoa_Sortdsc ;
      private String Ddo_pessoa_tipopessoa_Cleanfilter ;
      private String Ddo_pessoa_tipopessoa_Searchbuttontext ;
      private String Ddo_pessoa_docto_Caption ;
      private String Ddo_pessoa_docto_Tooltip ;
      private String Ddo_pessoa_docto_Cls ;
      private String Ddo_pessoa_docto_Filteredtext_set ;
      private String Ddo_pessoa_docto_Selectedvalue_set ;
      private String Ddo_pessoa_docto_Dropdownoptionstype ;
      private String Ddo_pessoa_docto_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_docto_Sortedstatus ;
      private String Ddo_pessoa_docto_Filtertype ;
      private String Ddo_pessoa_docto_Datalisttype ;
      private String Ddo_pessoa_docto_Datalistproc ;
      private String Ddo_pessoa_docto_Sortasc ;
      private String Ddo_pessoa_docto_Sortdsc ;
      private String Ddo_pessoa_docto_Loadingdata ;
      private String Ddo_pessoa_docto_Cleanfilter ;
      private String Ddo_pessoa_docto_Noresultsfound ;
      private String Ddo_pessoa_docto_Searchbuttontext ;
      private String Ddo_pessoa_ie_Caption ;
      private String Ddo_pessoa_ie_Tooltip ;
      private String Ddo_pessoa_ie_Cls ;
      private String Ddo_pessoa_ie_Filteredtext_set ;
      private String Ddo_pessoa_ie_Selectedvalue_set ;
      private String Ddo_pessoa_ie_Dropdownoptionstype ;
      private String Ddo_pessoa_ie_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_ie_Sortedstatus ;
      private String Ddo_pessoa_ie_Filtertype ;
      private String Ddo_pessoa_ie_Datalisttype ;
      private String Ddo_pessoa_ie_Datalistproc ;
      private String Ddo_pessoa_ie_Sortasc ;
      private String Ddo_pessoa_ie_Sortdsc ;
      private String Ddo_pessoa_ie_Loadingdata ;
      private String Ddo_pessoa_ie_Cleanfilter ;
      private String Ddo_pessoa_ie_Noresultsfound ;
      private String Ddo_pessoa_ie_Searchbuttontext ;
      private String Ddo_pessoa_endereco_Caption ;
      private String Ddo_pessoa_endereco_Tooltip ;
      private String Ddo_pessoa_endereco_Cls ;
      private String Ddo_pessoa_endereco_Filteredtext_set ;
      private String Ddo_pessoa_endereco_Selectedvalue_set ;
      private String Ddo_pessoa_endereco_Dropdownoptionstype ;
      private String Ddo_pessoa_endereco_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_endereco_Sortedstatus ;
      private String Ddo_pessoa_endereco_Filtertype ;
      private String Ddo_pessoa_endereco_Datalisttype ;
      private String Ddo_pessoa_endereco_Datalistproc ;
      private String Ddo_pessoa_endereco_Sortasc ;
      private String Ddo_pessoa_endereco_Sortdsc ;
      private String Ddo_pessoa_endereco_Loadingdata ;
      private String Ddo_pessoa_endereco_Cleanfilter ;
      private String Ddo_pessoa_endereco_Noresultsfound ;
      private String Ddo_pessoa_endereco_Searchbuttontext ;
      private String Ddo_pessoa_municipiocod_Caption ;
      private String Ddo_pessoa_municipiocod_Tooltip ;
      private String Ddo_pessoa_municipiocod_Cls ;
      private String Ddo_pessoa_municipiocod_Filteredtext_set ;
      private String Ddo_pessoa_municipiocod_Filteredtextto_set ;
      private String Ddo_pessoa_municipiocod_Dropdownoptionstype ;
      private String Ddo_pessoa_municipiocod_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_municipiocod_Sortedstatus ;
      private String Ddo_pessoa_municipiocod_Filtertype ;
      private String Ddo_pessoa_municipiocod_Sortasc ;
      private String Ddo_pessoa_municipiocod_Sortdsc ;
      private String Ddo_pessoa_municipiocod_Cleanfilter ;
      private String Ddo_pessoa_municipiocod_Rangefilterfrom ;
      private String Ddo_pessoa_municipiocod_Rangefilterto ;
      private String Ddo_pessoa_municipiocod_Searchbuttontext ;
      private String Ddo_pessoa_uf_Caption ;
      private String Ddo_pessoa_uf_Tooltip ;
      private String Ddo_pessoa_uf_Cls ;
      private String Ddo_pessoa_uf_Filteredtext_set ;
      private String Ddo_pessoa_uf_Selectedvalue_set ;
      private String Ddo_pessoa_uf_Dropdownoptionstype ;
      private String Ddo_pessoa_uf_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_uf_Sortedstatus ;
      private String Ddo_pessoa_uf_Filtertype ;
      private String Ddo_pessoa_uf_Datalisttype ;
      private String Ddo_pessoa_uf_Datalistproc ;
      private String Ddo_pessoa_uf_Sortasc ;
      private String Ddo_pessoa_uf_Sortdsc ;
      private String Ddo_pessoa_uf_Loadingdata ;
      private String Ddo_pessoa_uf_Cleanfilter ;
      private String Ddo_pessoa_uf_Noresultsfound ;
      private String Ddo_pessoa_uf_Searchbuttontext ;
      private String Ddo_pessoa_cep_Caption ;
      private String Ddo_pessoa_cep_Tooltip ;
      private String Ddo_pessoa_cep_Cls ;
      private String Ddo_pessoa_cep_Filteredtext_set ;
      private String Ddo_pessoa_cep_Selectedvalue_set ;
      private String Ddo_pessoa_cep_Dropdownoptionstype ;
      private String Ddo_pessoa_cep_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_cep_Sortedstatus ;
      private String Ddo_pessoa_cep_Filtertype ;
      private String Ddo_pessoa_cep_Datalisttype ;
      private String Ddo_pessoa_cep_Datalistproc ;
      private String Ddo_pessoa_cep_Sortasc ;
      private String Ddo_pessoa_cep_Sortdsc ;
      private String Ddo_pessoa_cep_Loadingdata ;
      private String Ddo_pessoa_cep_Cleanfilter ;
      private String Ddo_pessoa_cep_Noresultsfound ;
      private String Ddo_pessoa_cep_Searchbuttontext ;
      private String Ddo_pessoa_telefone_Caption ;
      private String Ddo_pessoa_telefone_Tooltip ;
      private String Ddo_pessoa_telefone_Cls ;
      private String Ddo_pessoa_telefone_Filteredtext_set ;
      private String Ddo_pessoa_telefone_Selectedvalue_set ;
      private String Ddo_pessoa_telefone_Dropdownoptionstype ;
      private String Ddo_pessoa_telefone_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_telefone_Sortedstatus ;
      private String Ddo_pessoa_telefone_Filtertype ;
      private String Ddo_pessoa_telefone_Datalisttype ;
      private String Ddo_pessoa_telefone_Datalistproc ;
      private String Ddo_pessoa_telefone_Sortasc ;
      private String Ddo_pessoa_telefone_Sortdsc ;
      private String Ddo_pessoa_telefone_Loadingdata ;
      private String Ddo_pessoa_telefone_Cleanfilter ;
      private String Ddo_pessoa_telefone_Noresultsfound ;
      private String Ddo_pessoa_telefone_Searchbuttontext ;
      private String Ddo_pessoa_fax_Caption ;
      private String Ddo_pessoa_fax_Tooltip ;
      private String Ddo_pessoa_fax_Cls ;
      private String Ddo_pessoa_fax_Filteredtext_set ;
      private String Ddo_pessoa_fax_Selectedvalue_set ;
      private String Ddo_pessoa_fax_Dropdownoptionstype ;
      private String Ddo_pessoa_fax_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_fax_Sortedstatus ;
      private String Ddo_pessoa_fax_Filtertype ;
      private String Ddo_pessoa_fax_Datalisttype ;
      private String Ddo_pessoa_fax_Datalistproc ;
      private String Ddo_pessoa_fax_Sortasc ;
      private String Ddo_pessoa_fax_Sortdsc ;
      private String Ddo_pessoa_fax_Loadingdata ;
      private String Ddo_pessoa_fax_Cleanfilter ;
      private String Ddo_pessoa_fax_Noresultsfound ;
      private String Ddo_pessoa_fax_Searchbuttontext ;
      private String Ddo_pessoa_ativo_Caption ;
      private String Ddo_pessoa_ativo_Tooltip ;
      private String Ddo_pessoa_ativo_Cls ;
      private String Ddo_pessoa_ativo_Selectedvalue_set ;
      private String Ddo_pessoa_ativo_Dropdownoptionstype ;
      private String Ddo_pessoa_ativo_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_ativo_Sortedstatus ;
      private String Ddo_pessoa_ativo_Datalisttype ;
      private String Ddo_pessoa_ativo_Datalistfixedvalues ;
      private String Ddo_pessoa_ativo_Sortasc ;
      private String Ddo_pessoa_ativo_Sortdsc ;
      private String Ddo_pessoa_ativo_Cleanfilter ;
      private String Ddo_pessoa_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavUsuario_pessoacod_Internalname ;
      private String edtavUsuario_pessoacod_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfpessoa_nome_Internalname ;
      private String edtavTfpessoa_nome_Jsonclick ;
      private String edtavTfpessoa_nome_sel_Internalname ;
      private String edtavTfpessoa_nome_sel_Jsonclick ;
      private String edtavTfpessoa_docto_Internalname ;
      private String edtavTfpessoa_docto_Jsonclick ;
      private String edtavTfpessoa_docto_sel_Internalname ;
      private String edtavTfpessoa_docto_sel_Jsonclick ;
      private String edtavTfpessoa_ie_Internalname ;
      private String edtavTfpessoa_ie_Jsonclick ;
      private String edtavTfpessoa_ie_sel_Internalname ;
      private String edtavTfpessoa_ie_sel_Jsonclick ;
      private String edtavTfpessoa_endereco_Internalname ;
      private String edtavTfpessoa_endereco_Jsonclick ;
      private String edtavTfpessoa_endereco_sel_Internalname ;
      private String edtavTfpessoa_endereco_sel_Jsonclick ;
      private String edtavTfpessoa_municipiocod_Internalname ;
      private String edtavTfpessoa_municipiocod_Jsonclick ;
      private String edtavTfpessoa_municipiocod_to_Internalname ;
      private String edtavTfpessoa_municipiocod_to_Jsonclick ;
      private String edtavTfpessoa_uf_Internalname ;
      private String edtavTfpessoa_uf_Jsonclick ;
      private String edtavTfpessoa_uf_sel_Internalname ;
      private String edtavTfpessoa_uf_sel_Jsonclick ;
      private String edtavTfpessoa_cep_Internalname ;
      private String edtavTfpessoa_cep_Jsonclick ;
      private String edtavTfpessoa_cep_sel_Internalname ;
      private String edtavTfpessoa_cep_sel_Jsonclick ;
      private String edtavTfpessoa_telefone_Internalname ;
      private String edtavTfpessoa_telefone_Jsonclick ;
      private String edtavTfpessoa_telefone_sel_Internalname ;
      private String edtavTfpessoa_telefone_sel_Jsonclick ;
      private String edtavTfpessoa_fax_Internalname ;
      private String edtavTfpessoa_fax_Jsonclick ;
      private String edtavTfpessoa_fax_sel_Internalname ;
      private String edtavTfpessoa_fax_sel_Jsonclick ;
      private String edtavTfpessoa_ativo_sel_Internalname ;
      private String edtavTfpessoa_ativo_sel_Jsonclick ;
      private String edtavDdo_pessoa_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_tipopessoatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_doctotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_enderecotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_municipiocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_uftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_ceptitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_faxtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtPessoa_Codigo_Internalname ;
      private String A35Pessoa_Nome ;
      private String edtPessoa_Nome_Internalname ;
      private String cmbPessoa_TipoPessoa_Internalname ;
      private String A36Pessoa_TipoPessoa ;
      private String edtPessoa_Docto_Internalname ;
      private String A518Pessoa_IE ;
      private String edtPessoa_IE_Internalname ;
      private String edtPessoa_Endereco_Internalname ;
      private String edtPessoa_MunicipioCod_Internalname ;
      private String A520Pessoa_UF ;
      private String edtPessoa_UF_Internalname ;
      private String A521Pessoa_CEP ;
      private String edtPessoa_CEP_Internalname ;
      private String A522Pessoa_Telefone ;
      private String edtPessoa_Telefone_Internalname ;
      private String A523Pessoa_Fax ;
      private String edtPessoa_Fax_Internalname ;
      private String chkPessoa_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV129WWPessoaDS_3_Pessoa_nome1 ;
      private String lV135WWPessoaDS_9_Pessoa_nome2 ;
      private String lV141WWPessoaDS_15_Pessoa_nome3 ;
      private String lV144WWPessoaDS_18_Tfpessoa_nome ;
      private String lV149WWPessoaDS_23_Tfpessoa_ie ;
      private String lV155WWPessoaDS_29_Tfpessoa_uf ;
      private String lV157WWPessoaDS_31_Tfpessoa_cep ;
      private String lV159WWPessoaDS_33_Tfpessoa_telefone ;
      private String lV161WWPessoaDS_35_Tfpessoa_fax ;
      private String AV129WWPessoaDS_3_Pessoa_nome1 ;
      private String AV130WWPessoaDS_4_Pessoa_tipopessoa1 ;
      private String AV135WWPessoaDS_9_Pessoa_nome2 ;
      private String AV136WWPessoaDS_10_Pessoa_tipopessoa2 ;
      private String AV141WWPessoaDS_15_Pessoa_nome3 ;
      private String AV142WWPessoaDS_16_Pessoa_tipopessoa3 ;
      private String AV145WWPessoaDS_19_Tfpessoa_nome_sel ;
      private String AV144WWPessoaDS_18_Tfpessoa_nome ;
      private String AV150WWPessoaDS_24_Tfpessoa_ie_sel ;
      private String AV149WWPessoaDS_23_Tfpessoa_ie ;
      private String AV156WWPessoaDS_30_Tfpessoa_uf_sel ;
      private String AV155WWPessoaDS_29_Tfpessoa_uf ;
      private String AV158WWPessoaDS_32_Tfpessoa_cep_sel ;
      private String AV157WWPessoaDS_31_Tfpessoa_cep ;
      private String AV160WWPessoaDS_34_Tfpessoa_telefone_sel ;
      private String AV159WWPessoaDS_33_Tfpessoa_telefone ;
      private String AV162WWPessoaDS_36_Tfpessoa_fax_sel ;
      private String AV161WWPessoaDS_35_Tfpessoa_fax ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavPessoa_nome1_Internalname ;
      private String cmbavPessoa_tipopessoa1_Internalname ;
      private String edtavPessoa_docto1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavPessoa_nome2_Internalname ;
      private String cmbavPessoa_tipopessoa2_Internalname ;
      private String edtavPessoa_docto2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavPessoa_nome3_Internalname ;
      private String cmbavPessoa_tipopessoa3_Internalname ;
      private String edtavPessoa_docto3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_pessoa_nome_Internalname ;
      private String Ddo_pessoa_tipopessoa_Internalname ;
      private String Ddo_pessoa_docto_Internalname ;
      private String Ddo_pessoa_ie_Internalname ;
      private String Ddo_pessoa_endereco_Internalname ;
      private String Ddo_pessoa_municipiocod_Internalname ;
      private String Ddo_pessoa_uf_Internalname ;
      private String Ddo_pessoa_cep_Internalname ;
      private String Ddo_pessoa_telefone_Internalname ;
      private String Ddo_pessoa_fax_Internalname ;
      private String Ddo_pessoa_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtPessoa_Nome_Title ;
      private String edtPessoa_Docto_Title ;
      private String edtPessoa_IE_Title ;
      private String edtPessoa_Endereco_Title ;
      private String edtPessoa_MunicipioCod_Title ;
      private String edtPessoa_UF_Title ;
      private String edtPessoa_CEP_Title ;
      private String edtPessoa_Telefone_Title ;
      private String edtPessoa_Fax_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavPessoa_nome3_Jsonclick ;
      private String cmbavPessoa_tipopessoa3_Jsonclick ;
      private String edtavPessoa_docto3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavPessoa_nome2_Jsonclick ;
      private String cmbavPessoa_tipopessoa2_Jsonclick ;
      private String edtavPessoa_docto2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavPessoa_nome1_Jsonclick ;
      private String cmbavPessoa_tipopessoa1_Jsonclick ;
      private String edtavPessoa_docto1_Jsonclick ;
      private String tblTableorders_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblPessoatitle_Internalname ;
      private String lblPessoatitle_Jsonclick ;
      private String sGXsfl_97_fel_idx="0001" ;
      private String ROClassString ;
      private String edtPessoa_Codigo_Jsonclick ;
      private String edtPessoa_Nome_Jsonclick ;
      private String cmbPessoa_TipoPessoa_Jsonclick ;
      private String edtPessoa_Docto_Jsonclick ;
      private String edtPessoa_IE_Jsonclick ;
      private String edtPessoa_Endereco_Jsonclick ;
      private String edtPessoa_MunicipioCod_Jsonclick ;
      private String edtPessoa_UF_Jsonclick ;
      private String edtPessoa_CEP_Jsonclick ;
      private String edtPessoa_Telefone_Jsonclick ;
      private String edtPessoa_Fax_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_pessoa_nome_Includesortasc ;
      private bool Ddo_pessoa_nome_Includesortdsc ;
      private bool Ddo_pessoa_nome_Includefilter ;
      private bool Ddo_pessoa_nome_Filterisrange ;
      private bool Ddo_pessoa_nome_Includedatalist ;
      private bool Ddo_pessoa_tipopessoa_Includesortasc ;
      private bool Ddo_pessoa_tipopessoa_Includesortdsc ;
      private bool Ddo_pessoa_tipopessoa_Includefilter ;
      private bool Ddo_pessoa_tipopessoa_Includedatalist ;
      private bool Ddo_pessoa_tipopessoa_Allowmultipleselection ;
      private bool Ddo_pessoa_docto_Includesortasc ;
      private bool Ddo_pessoa_docto_Includesortdsc ;
      private bool Ddo_pessoa_docto_Includefilter ;
      private bool Ddo_pessoa_docto_Filterisrange ;
      private bool Ddo_pessoa_docto_Includedatalist ;
      private bool Ddo_pessoa_ie_Includesortasc ;
      private bool Ddo_pessoa_ie_Includesortdsc ;
      private bool Ddo_pessoa_ie_Includefilter ;
      private bool Ddo_pessoa_ie_Filterisrange ;
      private bool Ddo_pessoa_ie_Includedatalist ;
      private bool Ddo_pessoa_endereco_Includesortasc ;
      private bool Ddo_pessoa_endereco_Includesortdsc ;
      private bool Ddo_pessoa_endereco_Includefilter ;
      private bool Ddo_pessoa_endereco_Filterisrange ;
      private bool Ddo_pessoa_endereco_Includedatalist ;
      private bool Ddo_pessoa_municipiocod_Includesortasc ;
      private bool Ddo_pessoa_municipiocod_Includesortdsc ;
      private bool Ddo_pessoa_municipiocod_Includefilter ;
      private bool Ddo_pessoa_municipiocod_Filterisrange ;
      private bool Ddo_pessoa_municipiocod_Includedatalist ;
      private bool Ddo_pessoa_uf_Includesortasc ;
      private bool Ddo_pessoa_uf_Includesortdsc ;
      private bool Ddo_pessoa_uf_Includefilter ;
      private bool Ddo_pessoa_uf_Filterisrange ;
      private bool Ddo_pessoa_uf_Includedatalist ;
      private bool Ddo_pessoa_cep_Includesortasc ;
      private bool Ddo_pessoa_cep_Includesortdsc ;
      private bool Ddo_pessoa_cep_Includefilter ;
      private bool Ddo_pessoa_cep_Filterisrange ;
      private bool Ddo_pessoa_cep_Includedatalist ;
      private bool Ddo_pessoa_telefone_Includesortasc ;
      private bool Ddo_pessoa_telefone_Includesortdsc ;
      private bool Ddo_pessoa_telefone_Includefilter ;
      private bool Ddo_pessoa_telefone_Filterisrange ;
      private bool Ddo_pessoa_telefone_Includedatalist ;
      private bool Ddo_pessoa_fax_Includesortasc ;
      private bool Ddo_pessoa_fax_Includesortdsc ;
      private bool Ddo_pessoa_fax_Includefilter ;
      private bool Ddo_pessoa_fax_Filterisrange ;
      private bool Ddo_pessoa_fax_Includedatalist ;
      private bool Ddo_pessoa_ativo_Includesortasc ;
      private bool Ddo_pessoa_ativo_Includesortdsc ;
      private bool Ddo_pessoa_ativo_Includefilter ;
      private bool Ddo_pessoa_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n520Pessoa_UF ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool A38Pessoa_Ativo ;
      private bool AV132WWPessoaDS_6_Dynamicfiltersenabled2 ;
      private bool AV138WWPessoaDS_12_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV89Display_IsBlob ;
      private String AV75TFPessoa_TipoPessoa_SelsJson ;
      private String AV91ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV39Pessoa_Docto1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV41Pessoa_Docto2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV43Pessoa_Docto3 ;
      private String AV79TFPessoa_Docto ;
      private String AV80TFPessoa_Docto_Sel ;
      private String AV101TFPessoa_Endereco ;
      private String AV102TFPessoa_Endereco_Sel ;
      private String AV73ddo_Pessoa_NomeTitleControlIdToReplace ;
      private String AV77ddo_Pessoa_TipoPessoaTitleControlIdToReplace ;
      private String AV81ddo_Pessoa_DoctoTitleControlIdToReplace ;
      private String AV99ddo_Pessoa_IETitleControlIdToReplace ;
      private String AV103ddo_Pessoa_EnderecoTitleControlIdToReplace ;
      private String AV107ddo_Pessoa_MunicipioCodTitleControlIdToReplace ;
      private String AV111ddo_Pessoa_UFTitleControlIdToReplace ;
      private String AV115ddo_Pessoa_CEPTitleControlIdToReplace ;
      private String AV119ddo_Pessoa_TelefoneTitleControlIdToReplace ;
      private String AV123ddo_Pessoa_FaxTitleControlIdToReplace ;
      private String AV84ddo_Pessoa_AtivoTitleControlIdToReplace ;
      private String AV164Update_GXI ;
      private String AV165Delete_GXI ;
      private String AV166Display_GXI ;
      private String A37Pessoa_Docto ;
      private String A519Pessoa_Endereco ;
      private String lV131WWPessoaDS_5_Pessoa_docto1 ;
      private String lV137WWPessoaDS_11_Pessoa_docto2 ;
      private String lV143WWPessoaDS_17_Pessoa_docto3 ;
      private String lV147WWPessoaDS_21_Tfpessoa_docto ;
      private String lV151WWPessoaDS_25_Tfpessoa_endereco ;
      private String AV127WWPessoaDS_1_Dynamicfiltersselector1 ;
      private String AV131WWPessoaDS_5_Pessoa_docto1 ;
      private String AV133WWPessoaDS_7_Dynamicfiltersselector2 ;
      private String AV137WWPessoaDS_11_Pessoa_docto2 ;
      private String AV139WWPessoaDS_13_Dynamicfiltersselector3 ;
      private String AV143WWPessoaDS_17_Pessoa_docto3 ;
      private String AV148WWPessoaDS_22_Tfpessoa_docto_sel ;
      private String AV147WWPessoaDS_21_Tfpessoa_docto ;
      private String AV152WWPessoaDS_26_Tfpessoa_endereco_sel ;
      private String AV151WWPessoaDS_25_Tfpessoa_endereco ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV89Display ;
      private IGxSession AV35Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavPessoa_tipopessoa1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavPessoa_tipopessoa2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavPessoa_tipopessoa3 ;
      private GXCombobox cmbPessoa_TipoPessoa ;
      private GXCheckbox chkPessoa_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00302_A38Pessoa_Ativo ;
      private String[] H00302_A523Pessoa_Fax ;
      private bool[] H00302_n523Pessoa_Fax ;
      private String[] H00302_A522Pessoa_Telefone ;
      private bool[] H00302_n522Pessoa_Telefone ;
      private String[] H00302_A521Pessoa_CEP ;
      private bool[] H00302_n521Pessoa_CEP ;
      private String[] H00302_A520Pessoa_UF ;
      private bool[] H00302_n520Pessoa_UF ;
      private int[] H00302_A503Pessoa_MunicipioCod ;
      private bool[] H00302_n503Pessoa_MunicipioCod ;
      private String[] H00302_A519Pessoa_Endereco ;
      private bool[] H00302_n519Pessoa_Endereco ;
      private String[] H00302_A518Pessoa_IE ;
      private bool[] H00302_n518Pessoa_IE ;
      private String[] H00302_A37Pessoa_Docto ;
      private String[] H00302_A36Pessoa_TipoPessoa ;
      private String[] H00302_A35Pessoa_Nome ;
      private int[] H00302_A34Pessoa_Codigo ;
      private long[] H00303_AGRID_nRecordCount ;
      private int[] H00304_A1Usuario_Codigo ;
      private int[] H00304_A57Usuario_PessoaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV76TFPessoa_TipoPessoa_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV94ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70Pessoa_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74Pessoa_TipoPessoaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78Pessoa_DoctoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV96Pessoa_IETitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV100Pessoa_EnderecoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV104Pessoa_MunicipioCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV108Pessoa_UFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV112Pessoa_CEPTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV116Pessoa_TelefoneTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV120Pessoa_FaxTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82Pessoa_AtivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV92ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV95ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV85DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV93ManageFiltersItem ;
   }

   public class wwpessoa__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00302( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV127WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV128WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV129WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV130WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV131WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV132WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV133WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV134WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV135WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV136WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV137WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV138WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV139WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV140WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV141WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV142WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV143WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV145WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV144WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV148WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV147WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV150WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV149WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV152WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV151WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV153WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV154WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV156WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV155WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV158WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV157WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV160WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV159WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV162WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV161WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV163WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV69Usuario_PessoaCod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [39] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Pessoa_Ativo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome], T1.[Pessoa_Codigo]";
         sFromString = " FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV129WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV129WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV130WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV131WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV131WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV135WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV135WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV136WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV136WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV137WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV137WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV141WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV141WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV142WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV142WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV143WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV143WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV145WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV144WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV145WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV145WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV148WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV147WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV148WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV150WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV149WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV150WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV152WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV151WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV152WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV153WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV153WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (0==AV154WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV154WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV155WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV156WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV158WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV157WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV158WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV160WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV159WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV160WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV162WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV161WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV162WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV162WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV163WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV163WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV69Usuario_PessoaCod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV69Usuario_PessoaCod)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_TipoPessoa]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_TipoPessoa] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_IE]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_IE] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Endereco]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Endereco] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_MunicipioCod]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_MunicipioCod] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Estado_UF]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Estado_UF] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_CEP]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_CEP] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Telefone]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Telefone] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Fax]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Fax] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Ativo]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Pessoa_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00303( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV127WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV128WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV129WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV130WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV131WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV132WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV133WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV134WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV135WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV136WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV137WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV138WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV139WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV140WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV141WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV142WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV143WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV145WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV144WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV148WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV147WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV150WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV149WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV152WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV151WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV153WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV154WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV156WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV155WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV158WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV157WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV160WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV159WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV162WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV161WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV163WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV69Usuario_PessoaCod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [34] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV129WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV129WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV130WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV131WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV127WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV128WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV131WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV135WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV135WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV136WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV136WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV137WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV132WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV133WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV134WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV137WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV141WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV141WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV142WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV142WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV143WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV138WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV139WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV140WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV143WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV145WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV144WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV145WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV145WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV146WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV148WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV147WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV148WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV150WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV149WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV150WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV152WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV151WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV152WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV153WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV153WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (0==AV154WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV154WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV155WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV156WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV158WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV157WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV158WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV160WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV159WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV160WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV162WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV161WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV162WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV162WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( AV163WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV163WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV69Usuario_PessoaCod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV69Usuario_PessoaCod)";
         }
         else
         {
            GXv_int5[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00302(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] , (short)dynConstraints[51] , (bool)dynConstraints[52] );
               case 1 :
                     return conditional_H00303(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] , (short)dynConstraints[51] , (bool)dynConstraints[52] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00304 ;
          prmH00304 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00302 ;
          prmH00302 = new Object[] {
          new Object[] {"@lV129WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV129WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV130WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV131WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV131WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV135WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV135WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV136WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV137WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV137WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV141WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV141WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV142WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV143WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV143WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV144WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV145WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV147WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV148WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV149WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV150WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV151WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV152WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV153WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV156WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV157WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV158WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV159WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV160WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV161WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV162WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV69Usuario_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00303 ;
          prmH00303 = new Object[] {
          new Object[] {"@lV129WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV129WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV130WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV131WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV131WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV135WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV135WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV136WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV137WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV137WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV141WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV141WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV142WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV143WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV143WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV144WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV145WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV147WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV148WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV149WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV150WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV151WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV152WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV153WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV156WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV157WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV158WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV159WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV160WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV161WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV162WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV69Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00302", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00302,11,0,true,false )
             ,new CursorDef("H00303", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00303,1,0,true,false )
             ,new CursorDef("H00304", "SELECT TOP 1 [Usuario_Codigo], [Usuario_PessoaCod] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV6WWPContext__Userid ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00304,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 2) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 100) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[74]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
