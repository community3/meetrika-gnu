/*
               File: WP_SS
        Description: Solicita��o de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:23:45.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_ss : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_ss( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_ss( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavServicogrupo_codigo = new GXCombobox();
         cmbavServico_codigo = new GXCombobox();
         dynavSistema_codigo = new GXCombobox();
         chkavIndicadorautomatico = new GXCheckbox();
         dynavRequisito_tiporeqcod = new GXCombobox();
         cmbavRequisito_prioridade = new GXCombobox();
         cmbavRequisito_status = new GXCombobox();
         cmbavSdt_requisitos__requisito_prioridade = new GXCombobox();
         cmbavSdt_requisitos__requisito_status = new GXCombobox();
         chkavTemservicopadrao = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV22WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_CODIGOMJ2( AV22WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vREQUISITO_TIPOREQCOD") == 0 )
            {
               AV114LinhaNegocio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114LinhaNegocio_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvREQUISITO_TIPOREQCODMJ2( AV114LinhaNegocio_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridsdt_requisitos") == 0 )
            {
               nRC_GXsfl_163 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_163_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_163_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridsdt_requisitos_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridsdt_requisitos") == 0 )
            {
               subGridsdt_requisitos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A58Usuario_PessoaNom = GetNextPar( );
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A1075Usuario_CargoUOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1075Usuario_CargoUOCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1075Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0)));
               A1076Usuario_CargoUONom = GetNextPar( );
               n1076Usuario_CargoUONom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1076Usuario_CargoUONom", A1076Usuario_CargoUONom);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV16SolicitacaoServico);
               A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               A605Servico_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
               A158ServicoGrupo_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               A2047Servico_LinNegCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n2047Servico_LinNegCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
               A632Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
               A1635Servico_IsPublico = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
               AV35TemServicoPadrao = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TemServicoPadrao", AV35TemServicoPadrao);
               A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV22WWPContext);
               A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               A129Sistema_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               A633Servico_UO = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n633Servico_UO = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
               AV17Usuario_CargoUOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Usuario_CargoUOCod), 6, 0)));
               A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1530Servico_TipoHierarquia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
               AV15ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
               AV64Agrupador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV64Agrupador);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV106SDT_Requisitos);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridrascunho") == 0 )
            {
               nRC_GXsfl_178 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_178_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_178_idx = GetNextPar( );
               edtContagemResultado_DataDmn_Linktarget = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataDmn_Internalname, "Linktarget", edtContagemResultado_DataDmn_Linktarget);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridrascunho_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridrascunho") == 0 )
            {
               subGridrascunho_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV22WWPContext);
               edtContagemResultado_DataDmn_Linktarget = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataDmn_Internalname, "Linktarget", edtContagemResultado_DataDmn_Linktarget);
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A58Usuario_PessoaNom = GetNextPar( );
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A1075Usuario_CargoUOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1075Usuario_CargoUOCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1075Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0)));
               A1076Usuario_CargoUONom = GetNextPar( );
               n1076Usuario_CargoUONom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1076Usuario_CargoUONom", A1076Usuario_CargoUONom);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV16SolicitacaoServico);
               A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               A605Servico_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
               A158ServicoGrupo_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               A2047Servico_LinNegCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n2047Servico_LinNegCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
               A632Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
               A1635Servico_IsPublico = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
               AV35TemServicoPadrao = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TemServicoPadrao", AV35TemServicoPadrao);
               A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               A129Sistema_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               A633Servico_UO = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n633Servico_UO = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
               AV17Usuario_CargoUOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Usuario_CargoUOCod), 6, 0)));
               A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1530Servico_TipoHierarquia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
               AV15ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridrascunho_refresh( subGridrascunho_Rows, AV22WWPContext, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, A456ContagemResultado_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMJ2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMJ2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221234959");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_ss.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Dadosdass", AV82DadosDaSS);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Dadosdass", AV82DadosDaSS);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_requisitos", AV106SDT_Requisitos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_requisitos", AV106SDT_Requisitos);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_163", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_163), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_178", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_178), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOUOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1075Usuario_CargoUOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOUONOM", StringUtil.RTrim( A1076Usuario_CargoUONom));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOSERVICO", AV16SolicitacaoServico);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOSERVICO", AV16SolicitacaoServico);
         }
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_DESCRICAO", A158ServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "SERVICO_LINNEGCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2047Servico_LinNegCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ATIVO", A632Servico_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ISPUBLICO", A1635Servico_IsPublico);
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_SIGLA", StringUtil.RTrim( A129Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, "SERVICO_UO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CARGOUOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Usuario_CargoUOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_TIPOHIERARQUIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1530Servico_TipoHierarquia), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_REQUISITOS", AV106SDT_Requisitos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_REQUISITOS", AV106SDT_Requisitos);
         }
         GxWebStd.gx_hidden_field( context, "vCALLER", StringUtil.RTrim( AV87Caller));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOREQUISITO", AV113ContagemResultadoRequisito);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOREQUISITO", AV113ContagemResultadoRequisito);
         }
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSTATUSANTERIOR", StringUtil.RTrim( AV73StatusAnterior));
         GxWebStd.gx_boolean_hidden_field( context, "vCHECKREQUIREDFIELDSRESULT", AV5CheckRequiredFieldsResult);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDADOSDASS", AV82DadosDaSS);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDADOSDASS", AV82DadosDaSS);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO", AV96Requisito);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO", AV96Requisito);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOREQUISITO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOREQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOREQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vCONTRATANTESEMEMAILSDA", AV27ContratanteSemEmailSda);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV93Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV93Codigos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOS", AV20Usuarios);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOS", AV20Usuarios);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATTACHMENTS", AV23Attachments);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATTACHMENTS", AV23Attachments);
         }
         GxWebStd.gx_hidden_field( context, "vRESULTADO", StringUtil.RTrim( AV25Resultado));
         GxWebStd.gx_hidden_field( context, "vORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66Ordem), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vREQUISITO_PONTUACAO", StringUtil.LTrim( StringUtil.NToC( AV104Requisito_Pontuacao, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vI", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78i), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_AGRUPADOR", A1926Requisito_Agrupador);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_REQUISITOITEM", AV107SDT_RequisitoItem);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_REQUISITOITEM", AV107SDT_RequisitoItem);
         }
         GxWebStd.gx_hidden_field( context, "REQUISITO_IDENTIFICADOR", A2001Requisito_Identificador);
         GxWebStd.gx_hidden_field( context, "REQUISITO_TITULO", A1927Requisito_Titulo);
         GxWebStd.gx_hidden_field( context, "REQUISITO_DESCRICAO", A1923Requisito_Descricao);
         GxWebStd.gx_hidden_field( context, "REQUISITO_PRIORIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2002Requisito_Prioridade), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_PONTUACAO", StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_STATUS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1934Requisito_Status), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO", A588ContagemResultadoEvidencia_Arquivo);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_LINK", A1449ContagemResultadoEvidencia_Link);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ", StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_DATA", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_DESCRICAO", A587ContagemResultadoEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_RDMNCREATED", context.localUtil.TToC( A1393ContagemResultadoEvidencia_RdmnCreated, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vACTIVETABID", StringUtil.RTrim( AV74ActiveTabId));
         GxWebStd.gx_hidden_field( context, "vGXV14", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV141GXV14), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV22WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV22WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Width", StringUtil.RTrim( Dvpanel_requisitos_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Cls", StringUtil.RTrim( Dvpanel_requisitos_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Title", StringUtil.RTrim( Dvpanel_requisitos_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Collapsible", StringUtil.BoolToStr( Dvpanel_requisitos_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Collapsed", StringUtil.BoolToStr( Dvpanel_requisitos_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Autowidth", StringUtil.BoolToStr( Dvpanel_requisitos_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Autoheight", StringUtil.BoolToStr( Dvpanel_requisitos_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_requisitos_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Iconposition", StringUtil.RTrim( Dvpanel_requisitos_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REQUISITOS_Autoscroll", StringUtil.BoolToStr( Dvpanel_requisitos_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Width", StringUtil.RTrim( Gxuitabspanel_tabs_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Cls", StringUtil.RTrim( Gxuitabspanel_tabs_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabs_Designtimetabs));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Yesbuttonposition", StringUtil.RTrim( Confirmpanel_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_Areatrabalho_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22WWPContext.gxTpr_Areatrabalho_codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vDADOSDASS_Contagemresultado_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV82DadosDaSS.gxTpr_Contagemresultado_codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO_Text", StringUtil.RTrim( cmbavServico_codigo.Description));
         GxWebStd.gx_hidden_field( context, "vSOLICITACAOSERVICO_Contagemresultado_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_Userid", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22WWPContext.gxTpr_Userid), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Activetabid", StringUtil.RTrim( Gxuitabspanel_tabs_Activetabid));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wcwc_contagemresultadoevidencias == null ) )
         {
            WebComp_Wcwc_contagemresultadoevidencias.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMJ2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMJ2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_ss.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_SS" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicita��o de Servi�o" ;
      }

      protected void WBMJ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_MJ2( true) ;
         }
         else
         {
            wb_table1_2_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 209,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLinhanegocio_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV114LinhaNegocio_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV114LinhaNegocio_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,209);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLinhanegocio_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavLinhanegocio_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 210,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisitoindex_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63RequisitoIndex), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63RequisitoIndex), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,210);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisitoindex_Jsonclick, 0, "Attribute", "", "", "", edtavRequisitoindex_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SS.htm");
         }
         wbLoad = true;
      }

      protected void STARTMJ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Solicita��o de Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMJ0( ) ;
      }

      protected void WSMJ2( )
      {
         STARTMJ2( ) ;
         EVTMJ2( ) ;
      }

      protected void EVTMJ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MJ2 */
                              E11MJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12MJ2 */
                                    E12MJ2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13MJ2 */
                              E13MJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOSALVAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14MJ2 */
                              E14MJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOBTNINCLUIRREQNEG'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15MJ2 */
                              E15MJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICOGRUPO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16MJ2 */
                              E16MJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17MJ2 */
                              E17MJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VINDICADORAUTOMATICO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18MJ2 */
                              E18MJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDSDT_REQUISITOSPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDSDT_REQUISITOSPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridsdt_requisitos_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridsdt_requisitos_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridsdt_requisitos_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridsdt_requisitos_lastpage( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDRASCUNHOPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDRASCUNHOPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridrascunho_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridrascunho_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridrascunho_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridrascunho_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 23), "GRIDSDT_REQUISITOS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 26), "GRIDSDT_REQUISITOS.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 23), "VBTNEXCLUIRREQNEG.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 23), "VBTNALTERARREQNEG.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 23), "VBTNALTERARREQNEG.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 23), "VBTNEXCLUIRREQNEG.CLICK") == 0 ) )
                           {
                              nGXsfl_163_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1632( ) ;
                              AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
                              if ( ( AV106SDT_Requisitos.Count >= AV124GXV7 ) && ( AV124GXV7 > 0 ) )
                              {
                                 AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
                                 AV62btnAlterarReqNeg = cgiGet( edtavBtnalterarreqneg_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnalterarreqneg_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV62btnAlterarReqNeg)) ? AV133Btnalterarreqneg_GXI : context.convertURL( context.PathToRelativeUrl( AV62btnAlterarReqNeg))));
                                 AV44btnExcluirReqNeg = cgiGet( edtavBtnexcluirreqneg_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnexcluirreqneg_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV44btnExcluirReqNeg)) ? AV134Btnexcluirreqneg_GXI : context.convertURL( context.PathToRelativeUrl( AV44btnExcluirReqNeg))));
                                 AV64Agrupador = cgiGet( edtavAgrupador_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV64Agrupador);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E19MJ2 */
                                    E19MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDSDT_REQUISITOS.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20MJ2 */
                                    E20MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDSDT_REQUISITOS.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E21MJ2 */
                                    E21MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22MJ2 */
                                    E22MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBTNEXCLUIRREQNEG.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23MJ2 */
                                    E23MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBTNALTERARREQNEG.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24MJ2 */
                                    E24MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "GRIDRASCUNHO.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "GRIDRASCUNHO.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "VBTNCONTINUAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "VBTNCONTINUAR.CLICK") == 0 ) )
                           {
                              nGXsfl_178_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_178_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_178_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1783( ) ;
                              AV75btnContinuar = cgiGet( edtavBtncontinuar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtncontinuar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV75btnContinuar)) ? AV131Btncontinuar_GXI : context.convertURL( context.PathToRelativeUrl( AV75btnContinuar))));
                              AV76btnCancelar = cgiGet( edtavBtncancelar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtncancelar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV76btnCancelar)) ? AV132Btncancelar_GXI : context.convertURL( context.PathToRelativeUrl( AV76btnCancelar))));
                              A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                              A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataDmn_Internalname), 0));
                              A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                              n493ContagemResultado_DemandaFM = false;
                              A494ContagemResultado_Descricao = cgiGet( edtContagemResultado_Descricao_Internalname);
                              n494ContagemResultado_Descricao = false;
                              A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataEntrega_Internalname), 0));
                              n472ContagemResultado_DataEntrega = false;
                              A1350ContagemResultado_DataCadastro = context.localUtil.CToT( cgiGet( edtContagemResultado_DataCadastro_Internalname), 0);
                              n1350ContagemResultado_DataCadastro = false;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdrequisitos_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdrequisitos_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDREQUISITOS");
                                 GX_FocusControl = edtavQtdrequisitos_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV77QtdRequisitos = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdrequisitos_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV77QtdRequisitos), 4, 0)));
                              }
                              else
                              {
                                 AV77QtdRequisitos = (short)(context.localUtil.CToN( cgiGet( edtavQtdrequisitos_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdrequisitos_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV77QtdRequisitos), 4, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdanexos_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdanexos_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDANEXOS");
                                 GX_FocusControl = edtavQtdanexos_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV86QtdAnexos = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdanexos_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV86QtdAnexos), 4, 0)));
                              }
                              else
                              {
                                 AV86QtdAnexos = (short)(context.localUtil.CToN( cgiGet( edtavQtdanexos_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdanexos_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV86QtdAnexos), 4, 0)));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDRASCUNHO.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25MJ3 */
                                    E25MJ3 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDRASCUNHO.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26MJ2 */
                                    E26MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBTNCONTINUAR.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27MJ2 */
                                    E27MJ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 99 )
                        {
                           OldWcwc_contagemresultadoevidencias = cgiGet( "W0099");
                           if ( ( StringUtil.Len( OldWcwc_contagemresultadoevidencias) == 0 ) || ( StringUtil.StrCmp(OldWcwc_contagemresultadoevidencias, WebComp_Wcwc_contagemresultadoevidencias_Component) != 0 ) )
                           {
                              WebComp_Wcwc_contagemresultadoevidencias = getWebComponent(GetType(), "GeneXus.Programs", OldWcwc_contagemresultadoevidencias, new Object[] {context} );
                              WebComp_Wcwc_contagemresultadoevidencias.ComponentInit();
                              WebComp_Wcwc_contagemresultadoevidencias.Name = "OldWcwc_contagemresultadoevidencias";
                              WebComp_Wcwc_contagemresultadoevidencias_Component = OldWcwc_contagemresultadoevidencias;
                           }
                           if ( StringUtil.Len( WebComp_Wcwc_contagemresultadoevidencias_Component) != 0 )
                           {
                              WebComp_Wcwc_contagemresultadoevidencias.componentprocess("W0099", "", sEvt);
                           }
                           WebComp_Wcwc_contagemresultadoevidencias_Component = OldWcwc_contagemresultadoevidencias;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMJ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMJ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavServicogrupo_codigo.Name = "vSERVICOGRUPO_CODIGO";
            cmbavServicogrupo_codigo.WebTags = "";
            if ( cmbavServicogrupo_codigo.ItemCount > 0 )
            {
               AV15ServicoGrupo_Codigo = (int)(NumberUtil.Val( cmbavServicogrupo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
            }
            cmbavServico_codigo.Name = "vSERVICO_CODIGO";
            cmbavServico_codigo.WebTags = "";
            if ( cmbavServico_codigo.ItemCount > 0 )
            {
               AV14Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
            }
            dynavSistema_codigo.Name = "vSISTEMA_CODIGO";
            dynavSistema_codigo.WebTags = "";
            chkavIndicadorautomatico.Name = "vINDICADORAUTOMATICO";
            chkavIndicadorautomatico.WebTags = "";
            chkavIndicadorautomatico.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIndicadorautomatico_Internalname, "TitleCaption", chkavIndicadorautomatico.Caption);
            chkavIndicadorautomatico.CheckedValue = "false";
            dynavRequisito_tiporeqcod.Name = "vREQUISITO_TIPOREQCOD";
            dynavRequisito_tiporeqcod.WebTags = "";
            cmbavRequisito_prioridade.Name = "vREQUISITO_PRIORIDADE";
            cmbavRequisito_prioridade.WebTags = "";
            cmbavRequisito_prioridade.addItem("1", "Alta", 0);
            cmbavRequisito_prioridade.addItem("2", "Alta M�dia", 0);
            cmbavRequisito_prioridade.addItem("3", "Alta Baixa", 0);
            cmbavRequisito_prioridade.addItem("4", "M�dia Alta", 0);
            cmbavRequisito_prioridade.addItem("5", "M�dia M�dia", 0);
            cmbavRequisito_prioridade.addItem("6", "M�dia Baixa", 0);
            cmbavRequisito_prioridade.addItem("7", "Baixa Alta", 0);
            cmbavRequisito_prioridade.addItem("8", "Baixa M�dia", 0);
            cmbavRequisito_prioridade.addItem("9", "Baixa Baixa", 0);
            if ( cmbavRequisito_prioridade.ItemCount > 0 )
            {
               AV102Requisito_Prioridade = (short)(NumberUtil.Val( cmbavRequisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)));
            }
            cmbavRequisito_status.Name = "vREQUISITO_STATUS";
            cmbavRequisito_status.WebTags = "";
            cmbavRequisito_status.addItem("0", "Rascunho", 0);
            cmbavRequisito_status.addItem("1", "Solicitado", 0);
            cmbavRequisito_status.addItem("2", "Aprovado", 0);
            cmbavRequisito_status.addItem("3", "N�o Aprovado", 0);
            cmbavRequisito_status.addItem("4", "Pausado", 0);
            cmbavRequisito_status.addItem("5", "Cancelado", 0);
            if ( cmbavRequisito_status.ItemCount > 0 )
            {
               AV103Requisito_Status = (short)(NumberUtil.Val( cmbavRequisito_status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0)));
            }
            GXCCtl = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_" + sGXsfl_163_idx;
            cmbavSdt_requisitos__requisito_prioridade.Name = GXCCtl;
            cmbavSdt_requisitos__requisito_prioridade.WebTags = "";
            cmbavSdt_requisitos__requisito_prioridade.addItem("1", "Alta", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("2", "Alta M�dia", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("3", "Alta Baixa", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("4", "M�dia Alta", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("5", "M�dia M�dia", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("6", "M�dia Baixa", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("7", "Baixa Alta", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("8", "Baixa M�dia", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("9", "Baixa Baixa", 0);
            if ( cmbavSdt_requisitos__requisito_prioridade.ItemCount > 0 )
            {
               if ( ( AV124GXV7 > 0 ) && ( AV106SDT_Requisitos.Count >= AV124GXV7 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade) )
               {
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade), 2, 0))), "."));
               }
            }
            GXCCtl = "SDT_REQUISITOS__REQUISITO_STATUS_" + sGXsfl_163_idx;
            cmbavSdt_requisitos__requisito_status.Name = GXCCtl;
            cmbavSdt_requisitos__requisito_status.WebTags = "";
            cmbavSdt_requisitos__requisito_status.addItem("0", "Rascunho", 0);
            cmbavSdt_requisitos__requisito_status.addItem("1", "Solicitado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("2", "Aprovado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("3", "N�o Aprovado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("4", "Pausado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("5", "Cancelado", 0);
            if ( cmbavSdt_requisitos__requisito_status.ItemCount > 0 )
            {
               if ( ( AV124GXV7 > 0 ) && ( AV106SDT_Requisitos.Count >= AV124GXV7 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status) )
               {
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status), 4, 0))), "."));
               }
            }
            chkavTemservicopadrao.Name = "vTEMSERVICOPADRAO";
            chkavTemservicopadrao.WebTags = "";
            chkavTemservicopadrao.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTemservicopadrao_Internalname, "TitleCaption", chkavTemservicopadrao.Caption);
            chkavTemservicopadrao.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavUsuario_pessoanom_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvREQUISITO_TIPOREQCOD_htmlMJ2( AV114LinhaNegocio_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvSISTEMA_CODIGOMJ2( wwpbaseobjects.SdtWWPContext AV22WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_CODIGO_dataMJ2( AV22WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_CODIGO_htmlMJ2( wwpbaseobjects.SdtWWPContext AV22WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_CODIGO_dataMJ2( AV22WWPContext) ;
         gxdynajaxindex = 1;
         dynavSistema_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV36Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_CODIGO_dataMJ2( wwpbaseobjects.SdtWWPContext AV22WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00MJ2 */
         pr_default.execute(0, new Object[] {AV22WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00MJ2_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00MJ2_A129Sistema_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvREQUISITO_TIPOREQCODMJ2( int AV114LinhaNegocio_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvREQUISITO_TIPOREQCOD_dataMJ2( AV114LinhaNegocio_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvREQUISITO_TIPOREQCOD_htmlMJ2( int AV114LinhaNegocio_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvREQUISITO_TIPOREQCOD_dataMJ2( AV114LinhaNegocio_Codigo) ;
         gxdynajaxindex = 1;
         dynavRequisito_tiporeqcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavRequisito_tiporeqcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavRequisito_tiporeqcod.ItemCount > 0 )
         {
            AV115Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0)));
         }
      }

      protected void GXDLVvREQUISITO_TIPOREQCOD_dataMJ2( int AV114LinhaNegocio_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00MJ3 */
         pr_default.execute(1, new Object[] {AV114LinhaNegocio_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00MJ3_A2041TipoRequisito_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00MJ3_A2042TipoRequisito_Identificador[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGridsdt_requisitos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1632( ) ;
         while ( nGXsfl_163_idx <= nRC_GXsfl_163 )
         {
            sendrow_1632( ) ;
            nGXsfl_163_idx = (short)(((subGridsdt_requisitos_Islastpage==1)&&(nGXsfl_163_idx+1>subGridsdt_requisitos_Recordsperpage( )) ? 1 : nGXsfl_163_idx+1));
            sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
            SubsflControlProps_1632( ) ;
         }
         context.GX_webresponse.AddString(Gridsdt_requisitosContainer.ToJavascriptSource());
         /* End function gxnrGridsdt_requisitos_newrow */
      }

      protected void gxnrGridrascunho_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1783( ) ;
         while ( nGXsfl_178_idx <= nRC_GXsfl_178 )
         {
            sendrow_1783( ) ;
            nGXsfl_178_idx = (short)(((subGridrascunho_Islastpage==1)&&(nGXsfl_178_idx+1>subGridrascunho_Recordsperpage( )) ? 1 : nGXsfl_178_idx+1));
            sGXsfl_178_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_178_idx), 4, 0)), 4, "0");
            SubsflControlProps_1783( ) ;
         }
         context.GX_webresponse.AddString(GridrascunhoContainer.ToJavascriptSource());
         /* End function gxnrGridrascunho_newrow */
      }

      protected void gxgrGridsdt_requisitos_refresh( int subGridsdt_requisitos_Rows ,
                                                     int A1Usuario_Codigo ,
                                                     String A58Usuario_PessoaNom ,
                                                     int A1075Usuario_CargoUOCod ,
                                                     String A1076Usuario_CargoUONom ,
                                                     SdtSolicitacaoServico AV16SolicitacaoServico ,
                                                     int A155Servico_Codigo ,
                                                     int A157ServicoGrupo_Codigo ,
                                                     String A605Servico_Sigla ,
                                                     String A158ServicoGrupo_Descricao ,
                                                     int A2047Servico_LinNegCod ,
                                                     bool A632Servico_Ativo ,
                                                     bool A1635Servico_IsPublico ,
                                                     bool AV35TemServicoPadrao ,
                                                     int A63ContratanteUsuario_ContratanteCod ,
                                                     wwpbaseobjects.SdtWWPContext AV22WWPContext ,
                                                     int A60ContratanteUsuario_UsuarioCod ,
                                                     int A127Sistema_Codigo ,
                                                     String A129Sistema_Sigla ,
                                                     int A633Servico_UO ,
                                                     int AV17Usuario_CargoUOCod ,
                                                     short A1530Servico_TipoHierarquia ,
                                                     int AV15ServicoGrupo_Codigo ,
                                                     String AV64Agrupador ,
                                                     IGxCollection AV106SDT_Requisitos )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Rows), 6, 0, ".", "")));
         GRIDSDT_REQUISITOS_nCurrentRecord = 0;
         RFMJ2( ) ;
         /* End function gxgrGridsdt_requisitos_refresh */
      }

      protected void gxgrGridrascunho_refresh( int subGridrascunho_Rows ,
                                               wwpbaseobjects.SdtWWPContext AV22WWPContext ,
                                               int A1Usuario_Codigo ,
                                               String A58Usuario_PessoaNom ,
                                               int A1075Usuario_CargoUOCod ,
                                               String A1076Usuario_CargoUONom ,
                                               SdtSolicitacaoServico AV16SolicitacaoServico ,
                                               int A155Servico_Codigo ,
                                               int A157ServicoGrupo_Codigo ,
                                               String A605Servico_Sigla ,
                                               String A158ServicoGrupo_Descricao ,
                                               int A2047Servico_LinNegCod ,
                                               bool A632Servico_Ativo ,
                                               bool A1635Servico_IsPublico ,
                                               bool AV35TemServicoPadrao ,
                                               int A63ContratanteUsuario_ContratanteCod ,
                                               int A60ContratanteUsuario_UsuarioCod ,
                                               int A127Sistema_Codigo ,
                                               String A129Sistema_Sigla ,
                                               int A633Servico_UO ,
                                               int AV17Usuario_CargoUOCod ,
                                               short A1530Servico_TipoHierarquia ,
                                               int AV15ServicoGrupo_Codigo ,
                                               int A456ContagemResultado_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Rows), 6, 0, ".", "")));
         /* Execute user event: E22MJ2 */
         E22MJ2 ();
         GRIDRASCUNHO_nCurrentRecord = 0;
         RFMJ3( ) ;
         /* End function gxgrGridrascunho_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavServicogrupo_codigo.ItemCount > 0 )
         {
            AV15ServicoGrupo_Codigo = (int)(NumberUtil.Val( cmbavServicogrupo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
         }
         if ( cmbavServico_codigo.ItemCount > 0 )
         {
            AV14Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV36Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0)));
         }
         if ( dynavRequisito_tiporeqcod.ItemCount > 0 )
         {
            AV115Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0)));
         }
         if ( cmbavRequisito_prioridade.ItemCount > 0 )
         {
            AV102Requisito_Prioridade = (short)(NumberUtil.Val( cmbavRequisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)));
         }
         if ( cmbavRequisito_status.ItemCount > 0 )
         {
            AV103Requisito_Status = (short)(NumberUtil.Val( cmbavRequisito_status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         /* Execute user event: E22MJ2 */
         E22MJ2 ();
         RFMJ2( ) ;
         RFMJ3( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         cmbavRequisito_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRequisito_status.Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_codigo_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_identificador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_titulo_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_descricao_Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_prioridade.Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_status.Enabled), 5, 0)));
         edtavQtdrequisitos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdrequisitos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdrequisitos_Enabled), 5, 0)));
         edtavQtdanexos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdanexos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdanexos_Enabled), 5, 0)));
      }

      protected void RFMJ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Gridsdt_requisitosContainer.ClearRows();
         }
         wbStart = 163;
         /* Execute user event: E21MJ2 */
         E21MJ2 ();
         nGXsfl_163_idx = 1;
         sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
         SubsflControlProps_1632( ) ;
         nGXsfl_163_Refreshing = 1;
         Gridsdt_requisitosContainer.AddObjectProperty("GridName", "Gridsdt_requisitos");
         Gridsdt_requisitosContainer.AddObjectProperty("CmpContext", "");
         Gridsdt_requisitosContainer.AddObjectProperty("InMasterPage", "false");
         Gridsdt_requisitosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         Gridsdt_requisitosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Gridsdt_requisitosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Gridsdt_requisitosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Backcolorstyle), 1, 0, ".", "")));
         Gridsdt_requisitosContainer.PageSize = subGridsdt_requisitos_Recordsperpage( );
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcwc_contagemresultadoevidencias_Component) != 0 )
               {
                  WebComp_Wcwc_contagemresultadoevidencias.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1632( ) ;
            /* Execute user event: E20MJ2 */
            E20MJ2 ();
            if ( ( GRIDSDT_REQUISITOS_nCurrentRecord > 0 ) && ( GRIDSDT_REQUISITOS_nGridOutOfScope == 0 ) && ( nGXsfl_163_idx == 1 ) )
            {
               GRIDSDT_REQUISITOS_nCurrentRecord = 0;
               GRIDSDT_REQUISITOS_nGridOutOfScope = 1;
               subgridsdt_requisitos_firstpage( ) ;
               /* Execute user event: E20MJ2 */
               E20MJ2 ();
            }
            wbEnd = 163;
            WBMJ0( ) ;
         }
         nGXsfl_163_Refreshing = 0;
      }

      protected void RFMJ3( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridrascunhoContainer.ClearRows();
         }
         wbStart = 178;
         /* Execute user event: E26MJ2 */
         E26MJ2 ();
         nGXsfl_178_idx = 1;
         sGXsfl_178_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_178_idx), 4, 0)), 4, "0");
         SubsflControlProps_1783( ) ;
         nGXsfl_178_Refreshing = 1;
         GridrascunhoContainer.AddObjectProperty("GridName", "Gridrascunho");
         GridrascunhoContainer.AddObjectProperty("CmpContext", "");
         GridrascunhoContainer.AddObjectProperty("InMasterPage", "false");
         GridrascunhoContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridrascunhoContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridrascunhoContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridrascunhoContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Backcolorstyle), 1, 0, ".", "")));
         GridrascunhoContainer.PageSize = subGridrascunho_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1783( ) ;
            GXPagingFrom3 = (int)(((subGridrascunho_Rows==0) ? 1 : GRIDRASCUNHO_nFirstRecordOnPage+1));
            GXPagingTo3 = (int)(((subGridrascunho_Rows==0) ? 10000 : GRIDRASCUNHO_nFirstRecordOnPage+subGridrascunho_Recordsperpage( )+1));
            /* Using cursor H00MJ6 */
            pr_default.execute(2, new Object[] {AV22WWPContext.gxTpr_Userid, GXPagingFrom3, GXPagingTo3});
            nGXsfl_178_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGridrascunho_Rows == 0 ) || ( GRIDRASCUNHO_nCurrentRecord < subGridrascunho_Recordsperpage( ) ) ) ) )
            {
               A508ContagemResultado_Owner = H00MJ6_A508ContagemResultado_Owner[0];
               A484ContagemResultado_StatusDmn = H00MJ6_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00MJ6_n484ContagemResultado_StatusDmn[0];
               A1350ContagemResultado_DataCadastro = H00MJ6_A1350ContagemResultado_DataCadastro[0];
               n1350ContagemResultado_DataCadastro = H00MJ6_n1350ContagemResultado_DataCadastro[0];
               A472ContagemResultado_DataEntrega = H00MJ6_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = H00MJ6_n472ContagemResultado_DataEntrega[0];
               A494ContagemResultado_Descricao = H00MJ6_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = H00MJ6_n494ContagemResultado_Descricao[0];
               A493ContagemResultado_DemandaFM = H00MJ6_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00MJ6_n493ContagemResultado_DemandaFM[0];
               A471ContagemResultado_DataDmn = H00MJ6_A471ContagemResultado_DataDmn[0];
               A456ContagemResultado_Codigo = H00MJ6_A456ContagemResultado_Codigo[0];
               A40000GXC1 = H00MJ6_A40000GXC1[0];
               n40000GXC1 = H00MJ6_n40000GXC1[0];
               A40001GXC2 = H00MJ6_A40001GXC2[0];
               n40001GXC2 = H00MJ6_n40001GXC2[0];
               A40000GXC1 = H00MJ6_A40000GXC1[0];
               n40000GXC1 = H00MJ6_n40000GXC1[0];
               A40001GXC2 = H00MJ6_A40001GXC2[0];
               n40001GXC2 = H00MJ6_n40001GXC2[0];
               /* Execute user event: E25MJ3 */
               E25MJ3 ();
               pr_default.readNext(2);
            }
            GRIDRASCUNHO_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 178;
            WBMJ0( ) ;
         }
         nGXsfl_178_Refreshing = 0;
      }

      protected int subGridsdt_requisitos_Pagecount( )
      {
         GRIDSDT_REQUISITOS_nRecordCount = subGridsdt_requisitos_Recordcount( );
         if ( ((int)((GRIDSDT_REQUISITOS_nRecordCount) % (subGridsdt_requisitos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDSDT_REQUISITOS_nRecordCount/ (decimal)(subGridsdt_requisitos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDSDT_REQUISITOS_nRecordCount/ (decimal)(subGridsdt_requisitos_Recordsperpage( ))))+1) ;
      }

      protected int subGridsdt_requisitos_Recordcount( )
      {
         return AV106SDT_Requisitos.Count ;
      }

      protected int subGridsdt_requisitos_Recordsperpage( )
      {
         if ( subGridsdt_requisitos_Rows > 0 )
         {
            return subGridsdt_requisitos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridsdt_requisitos_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDSDT_REQUISITOS_nFirstRecordOnPage/ (decimal)(subGridsdt_requisitos_Recordsperpage( ))))+1) ;
      }

      protected short subgridsdt_requisitos_firstpage( )
      {
         GRIDSDT_REQUISITOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         }
         return 0 ;
      }

      protected short subgridsdt_requisitos_nextpage( )
      {
         GRIDSDT_REQUISITOS_nRecordCount = subGridsdt_requisitos_Recordcount( );
         if ( ( GRIDSDT_REQUISITOS_nRecordCount >= subGridsdt_requisitos_Recordsperpage( ) ) && ( GRIDSDT_REQUISITOS_nEOF == 0 ) )
         {
            GRIDSDT_REQUISITOS_nFirstRecordOnPage = (long)(GRIDSDT_REQUISITOS_nFirstRecordOnPage+subGridsdt_requisitos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         }
         return (short)(((GRIDSDT_REQUISITOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridsdt_requisitos_previouspage( )
      {
         if ( GRIDSDT_REQUISITOS_nFirstRecordOnPage >= subGridsdt_requisitos_Recordsperpage( ) )
         {
            GRIDSDT_REQUISITOS_nFirstRecordOnPage = (long)(GRIDSDT_REQUISITOS_nFirstRecordOnPage-subGridsdt_requisitos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         }
         return 0 ;
      }

      protected short subgridsdt_requisitos_lastpage( )
      {
         GRIDSDT_REQUISITOS_nRecordCount = subGridsdt_requisitos_Recordcount( );
         if ( GRIDSDT_REQUISITOS_nRecordCount > subGridsdt_requisitos_Recordsperpage( ) )
         {
            if ( ((int)((GRIDSDT_REQUISITOS_nRecordCount) % (subGridsdt_requisitos_Recordsperpage( )))) == 0 )
            {
               GRIDSDT_REQUISITOS_nFirstRecordOnPage = (long)(GRIDSDT_REQUISITOS_nRecordCount-subGridsdt_requisitos_Recordsperpage( ));
            }
            else
            {
               GRIDSDT_REQUISITOS_nFirstRecordOnPage = (long)(GRIDSDT_REQUISITOS_nRecordCount-((int)((GRIDSDT_REQUISITOS_nRecordCount) % (subGridsdt_requisitos_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDSDT_REQUISITOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         }
         return 0 ;
      }

      protected int subgridsdt_requisitos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDSDT_REQUISITOS_nFirstRecordOnPage = (long)(subGridsdt_requisitos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDSDT_REQUISITOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         }
         return (int)(0) ;
      }

      protected int subGridrascunho_Pagecount( )
      {
         GRIDRASCUNHO_nRecordCount = subGridrascunho_Recordcount( );
         if ( ((int)((GRIDRASCUNHO_nRecordCount) % (subGridrascunho_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDRASCUNHO_nRecordCount/ (decimal)(subGridrascunho_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDRASCUNHO_nRecordCount/ (decimal)(subGridrascunho_Recordsperpage( ))))+1) ;
      }

      protected int subGridrascunho_Recordcount( )
      {
         /* Using cursor H00MJ9 */
         pr_default.execute(3, new Object[] {AV22WWPContext.gxTpr_Userid});
         GRIDRASCUNHO_nRecordCount = H00MJ9_AGRIDRASCUNHO_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRIDRASCUNHO_nRecordCount) ;
      }

      protected int subGridrascunho_Recordsperpage( )
      {
         if ( subGridrascunho_Rows > 0 )
         {
            return subGridrascunho_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridrascunho_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDRASCUNHO_nFirstRecordOnPage/ (decimal)(subGridrascunho_Recordsperpage( ))))+1) ;
      }

      protected short subgridrascunho_firstpage( )
      {
         GRIDRASCUNHO_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrascunho_refresh( subGridrascunho_Rows, AV22WWPContext, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgridrascunho_nextpage( )
      {
         GRIDRASCUNHO_nRecordCount = subGridrascunho_Recordcount( );
         if ( ( GRIDRASCUNHO_nRecordCount >= subGridrascunho_Recordsperpage( ) ) && ( GRIDRASCUNHO_nEOF == 0 ) )
         {
            GRIDRASCUNHO_nFirstRecordOnPage = (long)(GRIDRASCUNHO_nFirstRecordOnPage+subGridrascunho_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrascunho_refresh( subGridrascunho_Rows, AV22WWPContext, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, A456ContagemResultado_Codigo) ;
         }
         return (short)(((GRIDRASCUNHO_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridrascunho_previouspage( )
      {
         if ( GRIDRASCUNHO_nFirstRecordOnPage >= subGridrascunho_Recordsperpage( ) )
         {
            GRIDRASCUNHO_nFirstRecordOnPage = (long)(GRIDRASCUNHO_nFirstRecordOnPage-subGridrascunho_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrascunho_refresh( subGridrascunho_Rows, AV22WWPContext, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgridrascunho_lastpage( )
      {
         GRIDRASCUNHO_nRecordCount = subGridrascunho_Recordcount( );
         if ( GRIDRASCUNHO_nRecordCount > subGridrascunho_Recordsperpage( ) )
         {
            if ( ((int)((GRIDRASCUNHO_nRecordCount) % (subGridrascunho_Recordsperpage( )))) == 0 )
            {
               GRIDRASCUNHO_nFirstRecordOnPage = (long)(GRIDRASCUNHO_nRecordCount-subGridrascunho_Recordsperpage( ));
            }
            else
            {
               GRIDRASCUNHO_nFirstRecordOnPage = (long)(GRIDRASCUNHO_nRecordCount-((int)((GRIDRASCUNHO_nRecordCount) % (subGridrascunho_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDRASCUNHO_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrascunho_refresh( subGridrascunho_Rows, AV22WWPContext, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected int subgridrascunho_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDRASCUNHO_nFirstRecordOnPage = (long)(subGridrascunho_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDRASCUNHO_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRASCUNHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrascunho_refresh( subGridrascunho_Rows, AV22WWPContext, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, A456ContagemResultado_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPMJ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         cmbavRequisito_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRequisito_status.Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_codigo_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_identificador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_titulo_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_descricao_Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_prioridade.Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_status.Enabled), 5, 0)));
         edtavQtdrequisitos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdrequisitos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdrequisitos_Enabled), 5, 0)));
         edtavQtdanexos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdanexos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdanexos_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E19MJ2 */
         E19MJ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvSISTEMA_CODIGO_htmlMJ2( AV22WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Dadosdass"), AV82DadosDaSS);
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_requisitos"), AV106SDT_Requisitos);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_REQUISITOS"), AV106SDT_Requisitos);
            ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV22WWPContext);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_REQUISITOITEM"), AV107SDT_RequisitoItem);
            /* Read variables values. */
            AV19Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom", AV19Usuario_PessoaNom);
            AV18Usuario_CargoUONom = StringUtil.Upper( cgiGet( edtavUsuario_cargouonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Usuario_CargoUONom", AV18Usuario_CargoUONom);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavDadosdass_contagemresultado_ss_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavDadosdass_contagemresultado_ss_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "DADOSDASS_CONTAGEMRESULTADO_SS");
               GX_FocusControl = edtavDadosdass_contagemresultado_ss_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82DadosDaSS.gxTpr_Contagemresultado_ss = 0;
            }
            else
            {
               AV82DadosDaSS.gxTpr_Contagemresultado_ss = (int)(context.localUtil.CToN( cgiGet( edtavDadosdass_contagemresultado_ss_Internalname), ",", "."));
            }
            cmbavServicogrupo_codigo.Name = cmbavServicogrupo_codigo_Internalname;
            cmbavServicogrupo_codigo.CurrentValue = cgiGet( cmbavServicogrupo_codigo_Internalname);
            AV15ServicoGrupo_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavServicogrupo_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
            cmbavServico_codigo.Name = cmbavServico_codigo_Internalname;
            cmbavServico_codigo.CurrentValue = cgiGet( cmbavServico_codigo_Internalname);
            AV14Servico_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
            dynavSistema_codigo.Name = dynavSistema_codigo_Internalname;
            dynavSistema_codigo.CurrentValue = cgiGet( dynavSistema_codigo_Internalname);
            AV36Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0)));
            AV82DadosDaSS.gxTpr_Contagemresultado_descricao = cgiGet( edtavDadosdass_contagemresultado_descricao_Internalname);
            AV82DadosDaSS.gxTpr_Contagemresultado_observacao = cgiGet( edtavDadosdass_contagemresultado_observacao_Internalname);
            AV82DadosDaSS.gxTpr_Contagemresultado_referencia = cgiGet( edtavDadosdass_contagemresultado_referencia_Internalname);
            AV82DadosDaSS.gxTpr_Contagemresultado_restricoes = cgiGet( edtavDadosdass_contagemresultado_restricoes_Internalname);
            AV82DadosDaSS.gxTpr_Contagemresultado_prioridadeprevista = cgiGet( edtavDadosdass_contagemresultado_prioridadeprevista_Internalname);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDataprevista_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data Prevista"}), 1, "vDATAPREVISTA");
               GX_FocusControl = edtavDataprevista_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83DataPrevista = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DataPrevista", context.localUtil.TToC( AV83DataPrevista, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV83DataPrevista = context.localUtil.CToT( cgiGet( edtavDataprevista_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DataPrevista", context.localUtil.TToC( AV83DataPrevista, 8, 5, 0, 3, "/", ":", " "));
            }
            AV97Requisito_Agrupador = cgiGet( edtavRequisito_agrupador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Requisito_Agrupador", AV97Requisito_Agrupador);
            AV100Requisito_Identificador = cgiGet( edtavRequisito_identificador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Requisito_Identificador", AV100Requisito_Identificador);
            AV91IndicadorAutomatico = StringUtil.StrToBool( cgiGet( chkavIndicadorautomatico_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91IndicadorAutomatico", AV91IndicadorAutomatico);
            dynavRequisito_tiporeqcod.Name = dynavRequisito_tiporeqcod_Internalname;
            dynavRequisito_tiporeqcod.CurrentValue = cgiGet( dynavRequisito_tiporeqcod_Internalname);
            AV115Requisito_TipoReqCod = (int)(NumberUtil.Val( cgiGet( dynavRequisito_tiporeqcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0)));
            AV101Requisito_Titulo = cgiGet( edtavRequisito_titulo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101Requisito_Titulo", AV101Requisito_Titulo);
            AV99Requisito_Descricao = cgiGet( edtavRequisito_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99Requisito_Descricao", AV99Requisito_Descricao);
            cmbavRequisito_prioridade.Name = cmbavRequisito_prioridade_Internalname;
            cmbavRequisito_prioridade.CurrentValue = cgiGet( cmbavRequisito_prioridade_Internalname);
            AV102Requisito_Prioridade = (short)(NumberUtil.Val( cgiGet( cmbavRequisito_prioridade_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)));
            cmbavRequisito_status.Name = cmbavRequisito_status_Internalname;
            cmbavRequisito_status.CurrentValue = cgiGet( cmbavRequisito_status_Internalname);
            AV103Requisito_Status = (short)(NumberUtil.Val( cgiGet( cmbavRequisito_status_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_responsavel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_RESPONSAVEL");
               GX_FocusControl = edtavServico_responsavel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29Servico_Responsavel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Servico_Responsavel), 6, 0)));
            }
            else
            {
               AV29Servico_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtavServico_responsavel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Servico_Responsavel), 6, 0)));
            }
            AV35TemServicoPadrao = StringUtil.StrToBool( cgiGet( chkavTemservicopadrao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TemServicoPadrao", AV35TemServicoPadrao);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLinhanegocio_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLinhanegocio_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLINHANEGOCIO_CODIGO");
               GX_FocusControl = edtavLinhanegocio_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV114LinhaNegocio_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114LinhaNegocio_Codigo), 6, 0)));
            }
            else
            {
               AV114LinhaNegocio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavLinhanegocio_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114LinhaNegocio_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRequisitoindex_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRequisitoindex_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREQUISITOINDEX");
               GX_FocusControl = edtavRequisitoindex_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63RequisitoIndex = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63RequisitoIndex", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63RequisitoIndex), 4, 0)));
            }
            else
            {
               AV63RequisitoIndex = (short)(context.localUtil.CToN( cgiGet( edtavRequisitoindex_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63RequisitoIndex", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63RequisitoIndex), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_163 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_163"), ",", "."));
            nRC_GXsfl_178 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_178"), ",", "."));
            AV74ActiveTabId = cgiGet( "vACTIVETABID");
            AV141GXV14 = (int)(context.localUtil.CToN( cgiGet( "vGXV14"), ",", "."));
            GRIDSDT_REQUISITOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDSDT_REQUISITOS_nFirstRecordOnPage"), ",", "."));
            GRIDRASCUNHO_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDRASCUNHO_nFirstRecordOnPage"), ",", "."));
            GRIDSDT_REQUISITOS_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDSDT_REQUISITOS_nEOF"), ",", "."));
            GRIDRASCUNHO_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDRASCUNHO_nEOF"), ",", "."));
            subGridsdt_requisitos_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDSDT_REQUISITOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Rows), 6, 0, ".", "")));
            subGridrascunho_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDRASCUNHO_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Rows), 6, 0, ".", "")));
            Dvpanel_requisitos_Width = cgiGet( "DVPANEL_REQUISITOS_Width");
            Dvpanel_requisitos_Cls = cgiGet( "DVPANEL_REQUISITOS_Cls");
            Dvpanel_requisitos_Title = cgiGet( "DVPANEL_REQUISITOS_Title");
            Dvpanel_requisitos_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_REQUISITOS_Collapsible"));
            Dvpanel_requisitos_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_REQUISITOS_Collapsed"));
            Dvpanel_requisitos_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_REQUISITOS_Autowidth"));
            Dvpanel_requisitos_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_REQUISITOS_Autoheight"));
            Dvpanel_requisitos_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_REQUISITOS_Showcollapseicon"));
            Dvpanel_requisitos_Iconposition = cgiGet( "DVPANEL_REQUISITOS_Iconposition");
            Dvpanel_requisitos_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_REQUISITOS_Autoscroll"));
            Gxuitabspanel_tabs_Width = cgiGet( "GXUITABSPANEL_TABS_Width");
            Gxuitabspanel_tabs_Cls = cgiGet( "GXUITABSPANEL_TABS_Cls");
            Gxuitabspanel_tabs_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autowidth"));
            Gxuitabspanel_tabs_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autoheight"));
            Gxuitabspanel_tabs_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autoscroll"));
            Gxuitabspanel_tabs_Designtimetabs = cgiGet( "GXUITABSPANEL_TABS_Designtimetabs");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( "CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( "CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Yesbuttonposition = cgiGet( "CONFIRMPANEL_Yesbuttonposition");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            nRC_GXsfl_163 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_163"), ",", "."));
            nGXsfl_163_fel_idx = 0;
            while ( nGXsfl_163_fel_idx < nRC_GXsfl_163 )
            {
               nGXsfl_163_fel_idx = (short)(((subGridsdt_requisitos_Islastpage==1)&&(nGXsfl_163_fel_idx+1>subGridsdt_requisitos_Recordsperpage( )) ? 1 : nGXsfl_163_fel_idx+1));
               sGXsfl_163_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_1632( ) ;
               AV124GXV7 = (short)(nGXsfl_163_fel_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
               if ( ( AV106SDT_Requisitos.Count >= AV124GXV7 ) && ( AV124GXV7 > 0 ) )
               {
                  AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
                  AV62btnAlterarReqNeg = cgiGet( edtavBtnalterarreqneg_Internalname);
                  AV44btnExcluirReqNeg = cgiGet( edtavBtnexcluirreqneg_Internalname);
                  AV64Agrupador = cgiGet( edtavAgrupador_Internalname);
               }
            }
            if ( nGXsfl_163_fel_idx == 0 )
            {
               nGXsfl_163_idx = 1;
               sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
               SubsflControlProps_1632( ) ;
            }
            nGXsfl_163_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E19MJ2 */
         E19MJ2 ();
         if (returnInSub) return;
      }

      protected void E19MJ2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV22WWPContext) ;
         edtavLinhanegocio_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_codigo_Visible), 5, 0)));
         edtavRequisitoindex_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisitoindex_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisitoindex_Visible), 5, 0)));
         subGridrascunho_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDRASCUNHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Rows), 6, 0, ".", "")));
         subGridsdt_requisitos_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Rows), 6, 0, ".", "")));
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcwc_contagemresultadoevidencias_Component), StringUtil.Lower( "WC_ContagemResultadoEvidencias")) != 0 )
         {
            WebComp_Wcwc_contagemresultadoevidencias = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wcwc_contagemresultadoevidencias.ComponentInit();
            WebComp_Wcwc_contagemresultadoevidencias.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wcwc_contagemresultadoevidencias_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( StringUtil.Len( WebComp_Wcwc_contagemresultadoevidencias_Component) != 0 )
         {
            WebComp_Wcwc_contagemresultadoevidencias.setjustcreated();
            WebComp_Wcwc_contagemresultadoevidencias.componentprepare(new Object[] {(String)"W0099",(String)""});
            WebComp_Wcwc_contagemresultadoevidencias.componentbind(new Object[] {});
         }
         AV102Requisito_Prioridade = 5;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)));
         AV103Requisito_Status = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         edtavServico_responsavel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_responsavel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_responsavel_Visible), 5, 0)));
         chkavTemservicopadrao.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTemservicopadrao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavTemservicopadrao.Visible), 5, 0)));
         edtavDadosdass_contagemresultado_descricao_Width = 98;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_descricao_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_descricao_Width), 9, 0)));
         edtavDadosdass_contagemresultado_prioridadeprevista_Width = 98;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_prioridadeprevista_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_prioridadeprevista_Width), 9, 0)));
         edtContagemResultado_DataDmn_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataDmn_Internalname, "Linktarget", edtContagemResultado_DataDmn_Linktarget);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90Request.QueryString)) )
         {
            AV21Websession.Remove("Caller");
            AV21Websession.Remove("Codigo");
         }
      }

      private void E20MJ2( )
      {
         /* Gridsdt_requisitos_Load Routine */
         AV124GXV7 = 1;
         while ( AV124GXV7 <= AV106SDT_Requisitos.Count )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
            AV62btnAlterarReqNeg = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnalterarreqneg_Internalname, AV62btnAlterarReqNeg);
            AV133Btnalterarreqneg_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavBtnalterarreqneg_Tooltiptext = "";
            AV44btnExcluirReqNeg = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnexcluirreqneg_Internalname, AV44btnExcluirReqNeg);
            AV134Btnexcluirreqneg_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavBtnexcluirreqneg_Tooltiptext = "";
            if ( ( ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_sequencia > 1 ) && ( StringUtil.StrCmp(AV64Agrupador, ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_agrupador) == 0 ) )
            {
               edtavAgrupador_Forecolor = GXUtil.RGB( 255, 255, 255);
            }
            else
            {
               edtavAgrupador_Forecolor = GXUtil.RGB( 0, 0, 0);
               AV64Agrupador = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_agrupador;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV64Agrupador);
            }
            if ( ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_codigo < 0 )
            {
               edtavBtnalterarreqneg_Visible = 0;
               edtavBtnexcluirreqneg_Visible = 0;
               edtavSdt_requisitos__requisito_identificador_Fontstrikethru = 1;
               edtavSdt_requisitos__requisito_titulo_Fontstrikethru = 1;
               cmbavSdt_requisitos__requisito_prioridade.FontStrikethru = 1;
               cmbavSdt_requisitos__requisito_status.FontStrikethru = 1;
            }
            else
            {
               edtavBtnalterarreqneg_Visible = 1;
               edtavBtnexcluirreqneg_Visible = 1;
               edtavSdt_requisitos__requisito_identificador_Fontstrikethru = 0;
               edtavSdt_requisitos__requisito_titulo_Fontstrikethru = 0;
               cmbavSdt_requisitos__requisito_prioridade.FontStrikethru = 0;
               cmbavSdt_requisitos__requisito_status.FontStrikethru = 0;
            }
            this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "SetTabTitle", "", new Object[] {(String)"TabRequisitos","Requisitos ("+StringUtil.Trim( StringUtil.Str( (decimal)(subGridsdt_requisitos_Recordcount( )), 10, 0))+")"});
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 163;
            }
            if ( ( subGridsdt_requisitos_Islastpage == 1 ) || ( subGridsdt_requisitos_Rows == 0 ) || ( ( GRIDSDT_REQUISITOS_nCurrentRecord >= GRIDSDT_REQUISITOS_nFirstRecordOnPage ) && ( GRIDSDT_REQUISITOS_nCurrentRecord < GRIDSDT_REQUISITOS_nFirstRecordOnPage + subGridsdt_requisitos_Recordsperpage( ) ) ) )
            {
               sendrow_1632( ) ;
               GRIDSDT_REQUISITOS_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nEOF), 1, 0, ".", "")));
               if ( GRIDSDT_REQUISITOS_nCurrentRecord + 1 >= subGridsdt_requisitos_Recordcount( ) )
               {
                  GRIDSDT_REQUISITOS_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDSDT_REQUISITOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSDT_REQUISITOS_nEOF), 1, 0, ".", "")));
               }
            }
            GRIDSDT_REQUISITOS_nCurrentRecord = (long)(GRIDSDT_REQUISITOS_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_163_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(163, Gridsdt_requisitosRow);
            }
            AV124GXV7 = (short)(AV124GXV7+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12MJ2 */
         E12MJ2 ();
         if (returnInSub) return;
      }

      protected void E12MJ2( )
      {
         AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
         if ( AV106SDT_Requisitos.Count >= AV124GXV7 )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
         }
         /* Enter Routine */
         if ( StringUtil.StrCmp(AV87Caller, "OS") == 0 )
         {
            /* Execute user subroutine: 'SETREQUISITOS' */
            S112 ();
            if (returnInSub) return;
            if ( (0==AV106SDT_Requisitos.Count) )
            {
               GX_msglist.addItem("Sem requisitos para gravar!");
            }
            else
            {
               if ( AV113ContagemResultadoRequisito.Success() )
               {
                  new prc_inslogresponsavel(context ).execute( ref  AV67Codigo,  0,  "U",  "D",  AV22WWPContext.gxTpr_Userid,  0,  AV73StatusAnterior,  AV73StatusAnterior,  StringUtil.Trim( StringUtil.Str( (decimal)(AV106SDT_Requisitos.Count), 9, 0))+" Requisito/s de Neg�cio adicionados",  AV83DataPrevista,  false) ;
                  context.CommitDataStores( "WP_SS");
                  AV21Websession.Remove("Caller");
                  AV21Websession.Remove("Codigo");
                  lblTbjava_Caption = "<script language=\"javascript\" type=\"javascript\">parent.location.replace(parent.location.href); </script>";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
               }
               else
               {
                  /* Execute user subroutine: 'MESSAGES' */
                  S122 ();
                  if (returnInSub) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'CHECKOUT' */
            S132 ();
            if (returnInSub) return;
            if ( AV5CheckRequiredFieldsResult )
            {
               if ( (0==AV82DadosDaSS.gxTpr_Contagemresultado_codigo) )
               {
                  AV16SolicitacaoServico = new SdtSolicitacaoServico(context);
                  /* Execute user subroutine: 'CARREGA CODIGO SS' */
                  S142 ();
                  if (returnInSub) return;
               }
               if ( (0==AV82DadosDaSS.gxTpr_Contagemresultado_ss) )
               {
                  GX_msglist.addItem("Falha ao gerar o c�digo da solicita��o");
               }
               else
               {
                  /* Execute user subroutine: 'SETDADOSSOLICITACAO' */
                  S152 ();
                  if (returnInSub) return;
                  AV16SolicitacaoServico.gxTpr_Contagemresultado_statusdmn = "S";
                  AV16SolicitacaoServico.gxTpr_Contagemresultado_responsavel = AV29Servico_Responsavel;
                  AV16SolicitacaoServico.Save();
                  if ( AV16SolicitacaoServico.Success() )
                  {
                     AV67Codigo = AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Codigo), 6, 0)));
                     if ( (0==AV106SDT_Requisitos.Count) )
                     {
                        AV107SDT_RequisitoItem = new SdtSDT_Requisitos_SDT_RequisitosItem(context);
                        AV107SDT_RequisitoItem.gxTpr_Requisito_codigo = 0;
                        AV107SDT_RequisitoItem.gxTpr_Requisito_titulo = AV16SolicitacaoServico.gxTpr_Contagemresultado_descricao;
                        AV107SDT_RequisitoItem.gxTpr_Requisito_descricao = AV16SolicitacaoServico.gxTpr_Contagemresultado_observacao;
                        AV107SDT_RequisitoItem.gxTpr_Requisito_prioridade = 5;
                        AV107SDT_RequisitoItem.gxTpr_Requisito_status = 1;
                        AV107SDT_RequisitoItem.gxTpr_Requisito_ordem = 1;
                        AV107SDT_RequisitoItem.gxTpr_Requisito_sequencia = 1;
                        AV106SDT_Requisitos.Add(AV107SDT_RequisitoItem, 0);
                        gx_BV163 = true;
                     }
                     /* Execute user subroutine: 'SETREQUISITOS' */
                     S112 ();
                     if (returnInSub) return;
                     if ( AV113ContagemResultadoRequisito.Success() && AV96Requisito.Success() )
                     {
                        GXt_int1 = AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo;
                        new prc_inslogresponsavel(context ).execute( ref  GXt_int1,  AV16SolicitacaoServico.gxTpr_Contagemresultado_responsavel,  "S",  "D",  AV22WWPContext.gxTpr_Userid,  0,  AV73StatusAnterior,  AV16SolicitacaoServico.gxTpr_Contagemresultado_statusdmn,  "",  AV16SolicitacaoServico.gxTpr_Contagemresultado_dataprevista,  false) ;
                        AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo = GXt_int1;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73StatusAnterior", AV73StatusAnterior);
                        GXt_int2 = AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo;
                        new prc_newevidenciademanda(context ).execute( ref  GXt_int2) ;
                        AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo = GXt_int2;
                        context.CommitDataStores( "WP_SS");
                        /* Execute user subroutine: 'ENVIONOTIFICACAO' */
                        S162 ();
                        if (returnInSub) return;
                     }
                     else
                     {
                        /* Execute user subroutine: 'MESSAGES' */
                        S122 ();
                        if (returnInSub) return;
                     }
                  }
                  else
                  {
                     /* Execute user subroutine: 'MESSAGES' */
                     S122 ();
                     if (returnInSub) return;
                  }
               }
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16SolicitacaoServico", AV16SolicitacaoServico);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107SDT_RequisitoItem", AV107SDT_RequisitoItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV106SDT_Requisitos", AV106SDT_Requisitos);
         nGXsfl_163_bak_idx = nGXsfl_163_idx;
         gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         nGXsfl_163_idx = nGXsfl_163_bak_idx;
         sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
         SubsflControlProps_1632( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV113ContagemResultadoRequisito", AV113ContagemResultadoRequisito);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96Requisito", AV96Requisito);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82DadosDaSS", AV82DadosDaSS);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV93Codigos", AV93Codigos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20Usuarios", AV20Usuarios);
      }

      protected void E13MJ2( )
      {
         /* 'DoFechar' Routine */
         AV21Websession.Remove("Caller");
         AV21Websession.Remove("Codigo");
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14MJ2( )
      {
         AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
         if ( AV106SDT_Requisitos.Count >= AV124GXV7 )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
         }
         /* 'DoSalvar' Routine */
         /* Execute user subroutine: 'CHECKOUT' */
         S132 ();
         if (returnInSub) return;
         if ( AV5CheckRequiredFieldsResult )
         {
            if ( (0==AV82DadosDaSS.gxTpr_Contagemresultado_codigo) )
            {
               AV16SolicitacaoServico = new SdtSolicitacaoServico(context);
               /* Execute user subroutine: 'CARREGA CODIGO SS' */
               S142 ();
               if (returnInSub) return;
            }
            if ( (0==AV82DadosDaSS.gxTpr_Contagemresultado_ss) )
            {
               GX_msglist.addItem("Falha ao gerar o c�digo da solicita��o");
            }
            else
            {
               /* Execute user subroutine: 'SETDADOSSOLICITACAO' */
               S152 ();
               if (returnInSub) return;
               AV16SolicitacaoServico.gxTpr_Contagemresultado_statusdmn = "U";
               AV16SolicitacaoServico.gxTpr_Contagemresultado_responsavel = AV22WWPContext.gxTpr_Userid;
               AV16SolicitacaoServico.Save();
               if ( AV16SolicitacaoServico.Success() )
               {
                  AV67Codigo = AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Codigo), 6, 0)));
                  if ( AV106SDT_Requisitos.Count > 0 )
                  {
                     /* Execute user subroutine: 'SETREQUISITOS' */
                     S112 ();
                     if (returnInSub) return;
                  }
                  if ( (0==AV106SDT_Requisitos.Count) || AV96Requisito.Success() )
                  {
                     GXt_int2 = AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo;
                     new prc_inslogresponsavel(context ).execute( ref  GXt_int2,  0,  "UN",  "D",  AV22WWPContext.gxTpr_Userid,  0,  AV73StatusAnterior,  AV16SolicitacaoServico.gxTpr_Contagemresultado_statusdmn,  "",  AV16SolicitacaoServico.gxTpr_Contagemresultado_dataprevista,  false) ;
                     AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo = GXt_int2;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73StatusAnterior", AV73StatusAnterior);
                     GXt_int1 = AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo;
                     new prc_newevidenciademanda(context ).execute( ref  GXt_int1) ;
                     AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo = GXt_int1;
                     context.CommitDataStores( "WP_SS");
                     Confirmpanel_Confirmationtext = "Rascunho da SS "+StringUtil.Trim( StringUtil.Str( (decimal)(AV16SolicitacaoServico.gxTpr_Contagemresultado_ss), 8, 0))+" salvo.";
                     context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel_Internalname, "ConfirmationText", Confirmpanel_Confirmationtext);
                     this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
                  }
                  else
                  {
                     AV136GXV9 = 1;
                     AV135GXV8 = AV96Requisito.GetMessages();
                     while ( AV136GXV9 <= AV135GXV8.Count )
                     {
                        AV10Message = ((SdtMessages_Message)AV135GXV8.Item(AV136GXV9));
                        GX_msglist.addItem(StringUtil.Trim( AV10Message.gxTpr_Description));
                        AV136GXV9 = (int)(AV136GXV9+1);
                     }
                  }
               }
               else
               {
                  AV138GXV11 = 1;
                  AV137GXV10 = AV16SolicitacaoServico.GetMessages();
                  while ( AV138GXV11 <= AV137GXV10.Count )
                  {
                     AV10Message = ((SdtMessages_Message)AV137GXV10.Item(AV138GXV11));
                     GX_msglist.addItem(StringUtil.Trim( AV10Message.gxTpr_Description));
                     AV138GXV11 = (int)(AV138GXV11+1);
                  }
               }
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16SolicitacaoServico", AV16SolicitacaoServico);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82DadosDaSS", AV82DadosDaSS);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107SDT_RequisitoItem", AV107SDT_RequisitoItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV113ContagemResultadoRequisito", AV113ContagemResultadoRequisito);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96Requisito", AV96Requisito);
      }

      protected void E15MJ2( )
      {
         AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
         if ( AV106SDT_Requisitos.Count >= AV124GXV7 )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
         }
         /* 'DobtnIncluirReqNeg' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100Requisito_Identificador)) )
         {
            GX_msglist.addItem("Identificador do Requisito n�o foi informado. Por favor verifique!");
         }
         else
         {
            AV52IsLiberado = true;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101Requisito_Titulo)) )
            {
               GX_msglist.addItem("Nome do Requisito de Neg�cio n�o Informado! Por favor verifique.");
               AV52IsLiberado = false;
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99Requisito_Descricao)) )
            {
               GX_msglist.addItem("Descri��o do Requisito de Neg�cio n�o Informado! Por favor verifique.");
               AV52IsLiberado = false;
            }
            if ( AV52IsLiberado )
            {
               AV49IsCadastrado = false;
               AV63RequisitoIndex = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63RequisitoIndex", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63RequisitoIndex), 4, 0)));
               AV139GXV12 = 1;
               while ( AV139GXV12 <= AV106SDT_Requisitos.Count )
               {
                  AV107SDT_RequisitoItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV139GXV12));
                  AV63RequisitoIndex = (short)(AV63RequisitoIndex+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63RequisitoIndex", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63RequisitoIndex), 4, 0)));
                  if ( AV107SDT_RequisitoItem.gxTpr_Requisito_editando )
                  {
                     AV49IsCadastrado = true;
                     if (true) break;
                  }
                  AV139GXV12 = (int)(AV139GXV12+1);
               }
               if ( ! AV49IsCadastrado )
               {
                  if ( AV106SDT_Requisitos.Count > 1 )
                  {
                     if ( ( StringUtil.StrCmp(AV64Agrupador, AV97Requisito_Agrupador) != 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( AV97Requisito_Agrupador)) )
                     {
                        AV140GXV13 = 1;
                        while ( AV140GXV13 <= AV106SDT_Requisitos.Count )
                        {
                           AV107SDT_RequisitoItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV140GXV13));
                           if ( StringUtil.StrCmp(AV107SDT_RequisitoItem.gxTpr_Requisito_agrupador, AV97Requisito_Agrupador) == 0 )
                           {
                              AV66Ordem = AV107SDT_RequisitoItem.gxTpr_Requisito_ordem;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Ordem), 4, 0)));
                              AV49IsCadastrado = true;
                              if (true) break;
                           }
                           AV140GXV13 = (int)(AV140GXV13+1);
                        }
                        if ( ! AV49IsCadastrado )
                        {
                           AV66Ordem = (short)(AV66Ordem+1);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Ordem), 4, 0)));
                        }
                     }
                  }
                  AV107SDT_RequisitoItem = new SdtSDT_Requisitos_SDT_RequisitosItem(context);
                  AV107SDT_RequisitoItem.gxTpr_Requisito_codigo = 0;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_identificador = AV100Requisito_Identificador;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_titulo = AV101Requisito_Titulo;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_descricao = AV99Requisito_Descricao;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_prioridade = AV102Requisito_Prioridade;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_agrupador = AV97Requisito_Agrupador;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_pontuacao = AV104Requisito_Pontuacao;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_status = AV103Requisito_Status;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_ordem = AV66Ordem;
                  AV107SDT_RequisitoItem.gxTpr_Requisito_sequencia = (short)(AV106SDT_Requisitos.Count+1);
                  AV106SDT_Requisitos.Add(AV107SDT_RequisitoItem, 0);
                  gx_BV163 = true;
                  Dvpanel_requisitos_Title = "(�ltimo: "+StringUtil.Trim( AV97Requisito_Agrupador)+", "+StringUtil.Trim( AV100Requisito_Identificador)+")";
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_requisitos_Internalname, "Title", Dvpanel_requisitos_Title);
               }
               else
               {
                  GX_msglist.addItem(StringUtil.Format( "Requisito \"%1\" atualizado!", AV100Requisito_Identificador, "", "", "", "", "", "", "", ""));
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_identificador = AV100Requisito_Identificador;
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_titulo = AV101Requisito_Titulo;
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_descricao = AV99Requisito_Descricao;
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_prioridade = AV102Requisito_Prioridade;
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_agrupador = AV97Requisito_Agrupador;
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_pontuacao = AV104Requisito_Pontuacao;
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_status = AV103Requisito_Status;
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV63RequisitoIndex)).gxTpr_Requisito_editando = false;
               }
               AV106SDT_Requisitos.Sort("Requisito_Agrupador, Requisito_Ordem, Requisito_Sequencia");
               gx_BV163 = true;
               bttBtnbtnincluirreqneg_Caption = "Incluir Requisito";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbtnincluirreqneg_Internalname, "Caption", bttBtnbtnincluirreqneg_Caption);
               if ( AV91IndicadorAutomatico )
               {
                  AV100Requisito_Identificador = StringUtil.Trim( StringUtil.Str( NumberUtil.Val( AV100Requisito_Identificador, ".")+1, 18, 0));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Requisito_Identificador", AV100Requisito_Identificador);
               }
               else
               {
                  AV100Requisito_Identificador = "";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Requisito_Identificador", AV100Requisito_Identificador);
               }
               AV101Requisito_Titulo = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101Requisito_Titulo", AV101Requisito_Titulo);
               AV99Requisito_Descricao = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99Requisito_Descricao", AV99Requisito_Descricao);
               AV102Requisito_Prioridade = 5;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)));
               AV104Requisito_Pontuacao = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV104Requisito_Pontuacao, 14, 5)));
               AV103Requisito_Status = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0)));
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97Requisito_Agrupador)) )
               {
                  GX_FocusControl = edtavRequisito_agrupador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  context.DoAjaxSetFocus(GX_FocusControl);
               }
               else
               {
                  GX_FocusControl = edtavRequisito_identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  context.DoAjaxSetFocus(GX_FocusControl);
               }
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107SDT_RequisitoItem", AV107SDT_RequisitoItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV106SDT_Requisitos", AV106SDT_Requisitos);
         nGXsfl_163_bak_idx = nGXsfl_163_idx;
         gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         nGXsfl_163_idx = nGXsfl_163_bak_idx;
         sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
         SubsflControlProps_1632( ) ;
         cmbavRequisito_prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_prioridade_Internalname, "Values", cmbavRequisito_prioridade.ToJavascriptSource());
         cmbavRequisito_status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_status_Internalname, "Values", cmbavRequisito_status.ToJavascriptSource());
      }

      protected void E26MJ2( )
      {
         /* Gridrascunho_Refresh Routine */
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "HideTabById", "", new Object[] {(String)"TabRascunho"});
      }

      protected void E21MJ2( )
      {
         /* Gridsdt_requisitos_Refresh Routine */
         AV64Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV64Agrupador);
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "SetTabTitle", "", new Object[] {(String)"TabRequisitos",(String)"Requisitos"});
      }

      protected void E22MJ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV22WWPContext) ;
         AV87Caller = AV21Websession.Get("Caller");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Caller", AV87Caller);
         if ( StringUtil.StrCmp(AV87Caller, "OS") == 0 )
         {
            this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "HideTabById", "", new Object[] {(String)"TabSolicitacao"});
            bttBtnenter_Caption = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
            bttBtnenter_Tooltiptext = "Criar Requisitos de Neg�cio";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            bttBtnsalvar_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnsalvar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnsalvar_Visible), 5, 0)));
            lblTextblocktitle_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocktitle_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocktitle_Visible), 5, 0)));
            Form.Caption = " ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV67Codigo = (int)(NumberUtil.Val( AV21Websession.Get("Codigo"), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Codigo), 6, 0)));
            AV89ContagemResultado.Load(AV67Codigo);
            AV73StatusAnterior = AV89ContagemResultado.gxTpr_Contagemresultado_statusdmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73StatusAnterior", AV73StatusAnterior);
            AV83DataPrevista = AV89ContagemResultado.gxTpr_Contagemresultado_dataprevista;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DataPrevista", context.localUtil.TToC( AV83DataPrevista, 8, 5, 0, 3, "/", ":", " "));
         }
         /* Using cursor H00MJ10 */
         pr_default.execute(4, new Object[] {AV22WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A57Usuario_PessoaCod = H00MJ10_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = H00MJ10_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = H00MJ10_n1073Usuario_CargoCod[0];
            A1Usuario_Codigo = H00MJ10_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00MJ10_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00MJ10_n58Usuario_PessoaNom[0];
            A1075Usuario_CargoUOCod = H00MJ10_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = H00MJ10_n1075Usuario_CargoUOCod[0];
            A1076Usuario_CargoUONom = H00MJ10_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = H00MJ10_n1076Usuario_CargoUONom[0];
            A58Usuario_PessoaNom = H00MJ10_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00MJ10_n58Usuario_PessoaNom[0];
            A1075Usuario_CargoUOCod = H00MJ10_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = H00MJ10_n1075Usuario_CargoUOCod[0];
            A1076Usuario_CargoUONom = H00MJ10_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = H00MJ10_n1076Usuario_CargoUONom[0];
            AV19Usuario_PessoaNom = A58Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom", AV19Usuario_PessoaNom);
            AV17Usuario_CargoUOCod = A1075Usuario_CargoUOCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Usuario_CargoUOCod), 6, 0)));
            AV18Usuario_CargoUONom = A1076Usuario_CargoUONom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Usuario_CargoUONom", AV18Usuario_CargoUONom);
            if ( StringUtil.StrCmp(AV16SolicitacaoServico.GetMode(), "INS") == 0 )
            {
               AV16SolicitacaoServico.gxTpr_Contagemresultado_owner = AV22WWPContext.gxTpr_Userid;
               AV16SolicitacaoServico.gxTpr_Contagemresultado_uoowner = A1075Usuario_CargoUOCod;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
         AV35TemServicoPadrao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TemServicoPadrao", AV35TemServicoPadrao);
         AV83DataPrevista = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DataPrevista", context.localUtil.TToC( AV83DataPrevista, 8, 5, 0, 3, "/", ":", " "));
         AV82DadosDaSS = new SdtSolicitacaoServico(context);
         GXt_int2 = AV14Servico_Codigo;
         GXt_int1 = AV22WWPContext.gxTpr_Contratante_codigo;
         new prc_solicitacaopadrao(context ).execute( ref  GXt_int1, out  GXt_int2) ;
         AV22WWPContext.gxTpr_Contratante_codigo = GXt_int1;
         AV14Servico_Codigo = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
         if ( AV14Servico_Codigo > 0 )
         {
            AV82DadosDaSS.gxTpr_Contagemresultado_servicoss = AV14Servico_Codigo;
            AV35TemServicoPadrao = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TemServicoPadrao", AV35TemServicoPadrao);
            /* Using cursor H00MJ11 */
            pr_default.execute(5, new Object[] {AV14Servico_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A155Servico_Codigo = H00MJ11_A155Servico_Codigo[0];
               A157ServicoGrupo_Codigo = H00MJ11_A157ServicoGrupo_Codigo[0];
               A605Servico_Sigla = H00MJ11_A605Servico_Sigla[0];
               A158ServicoGrupo_Descricao = H00MJ11_A158ServicoGrupo_Descricao[0];
               A2047Servico_LinNegCod = H00MJ11_A2047Servico_LinNegCod[0];
               n2047Servico_LinNegCod = H00MJ11_n2047Servico_LinNegCod[0];
               A158ServicoGrupo_Descricao = H00MJ11_A158ServicoGrupo_Descricao[0];
               AV15ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
               cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), A605Servico_Sigla, 0);
               cmbavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)), A158ServicoGrupo_Descricao, 0);
               AV15ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
               AV114LinhaNegocio_Codigo = A2047Servico_LinNegCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114LinhaNegocio_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
         }
         else
         {
            /* Execute user subroutine: 'CARREGAGRUPOSDESERVICOS' */
            S172 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S182 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'CARREGASISTEMAS' */
         S192 ();
         if (returnInSub) return;
         edtavDadosdass_contagemresultado_ss_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_ss_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_ss_Enabled), 5, 0)));
         edtavUsuario_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
         edtavUsuario_cargouonom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargouonom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargouonom_Enabled), 5, 0)));
         AV21Websession.Remove("ArquivosEvd");
         AV106SDT_Requisitos.Clear();
         gx_BV163 = true;
         AV64Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV64Agrupador);
         AV66Ordem = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Ordem), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22WWPContext", AV22WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16SolicitacaoServico", AV16SolicitacaoServico);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82DadosDaSS", AV82DadosDaSS);
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV106SDT_Requisitos", AV106SDT_Requisitos);
         dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", dynavSistema_codigo.ToJavascriptSource());
      }

      protected void E16MJ2( )
      {
         /* Servicogrupo_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S202 ();
         if (returnInSub) return;
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
      }

      protected void E17MJ2( )
      {
         /* Servico_codigo_Click Routine */
         AV114LinhaNegocio_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114LinhaNegocio_Codigo), 6, 0)));
         /* Using cursor H00MJ12 */
         pr_default.execute(6, new Object[] {AV14Servico_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A2047Servico_LinNegCod = H00MJ12_A2047Servico_LinNegCod[0];
            n2047Servico_LinNegCod = H00MJ12_n2047Servico_LinNegCod[0];
            A155Servico_Codigo = H00MJ12_A155Servico_Codigo[0];
            GXt_int2 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1551Servico_Responsavel = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
            AV114LinhaNegocio_Codigo = A2047Servico_LinNegCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114LinhaNegocio_Codigo), 6, 0)));
            AV145GXLvl438 = 0;
            /* Using cursor H00MJ13 */
            pr_default.execute(7, new Object[] {A1551Servico_Responsavel});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A57Usuario_PessoaCod = H00MJ13_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = H00MJ13_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = H00MJ13_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00MJ13_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = H00MJ13_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00MJ13_n58Usuario_PessoaNom[0];
               AV145GXLvl438 = 1;
               AV34Responsavel = "Respons�vel: " + A58Usuario_PessoaNom;
               AV29Servico_Responsavel = A1551Servico_Responsavel;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Servico_Responsavel), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(7);
            if ( AV145GXLvl438 == 0 )
            {
               AV34Responsavel = "Sem respons�vel cadastrado!";
               AV29Servico_Responsavel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Servico_Responsavel), 6, 0)));
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
         cmbavServico_codigo.TooltipText = AV34Responsavel;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Tooltiptext", cmbavServico_codigo.TooltipText);
         /* Execute user subroutine: 'BTNENTER' */
         S212 ();
         if (returnInSub) return;
      }

      protected void E11MJ2( )
      {
         /* Confirmpanel_Close Routine */
         context.DoAjaxRefresh();
      }

      protected void E23MJ2( )
      {
         AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
         if ( AV106SDT_Requisitos.Count >= AV124GXV7 )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
         }
         /* Btnexcluirreqneg_Click Routine */
         if ( ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_codigo > 0 )
         {
            ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_codigo = (int)(((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_codigo*-1);
         }
         else
         {
            AV46Index = 1;
            AV61ReqIdentificador = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_identificador;
            AV146GXV15 = 1;
            while ( AV146GXV15 <= AV106SDT_Requisitos.Count )
            {
               AV107SDT_RequisitoItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV146GXV15));
               if ( StringUtil.StrCmp(AV107SDT_RequisitoItem.gxTpr_Requisito_identificador, AV61ReqIdentificador) == 0 )
               {
                  AV106SDT_Requisitos.RemoveItem(AV46Index);
                  gx_BV163 = true;
                  if (true) break;
               }
               AV46Index = (short)(AV46Index+1);
               AV146GXV15 = (int)(AV146GXV15+1);
            }
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78i), 4, 0)));
            while ( AV78i <= AV106SDT_Requisitos.Count )
            {
               ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV78i)).gxTpr_Requisito_sequencia = (short)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV78i)).gxTpr_Requisito_sequencia-1);
               AV78i = (short)(AV78i+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78i), 4, 0)));
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107SDT_RequisitoItem", AV107SDT_RequisitoItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV106SDT_Requisitos", AV106SDT_Requisitos);
         nGXsfl_163_bak_idx = nGXsfl_163_idx;
         gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         nGXsfl_163_idx = nGXsfl_163_bak_idx;
         sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
         SubsflControlProps_1632( ) ;
      }

      protected void E24MJ2( )
      {
         AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
         if ( AV106SDT_Requisitos.Count >= AV124GXV7 )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
         }
         /* Btnalterarreqneg_Click Routine */
         AV100Requisito_Identificador = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_identificador;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Requisito_Identificador", AV100Requisito_Identificador);
         AV101Requisito_Titulo = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_titulo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101Requisito_Titulo", AV101Requisito_Titulo);
         AV99Requisito_Descricao = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99Requisito_Descricao", AV99Requisito_Descricao);
         AV97Requisito_Agrupador = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_agrupador;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Requisito_Agrupador", AV97Requisito_Agrupador);
         AV102Requisito_Prioridade = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_prioridade;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)));
         AV102Requisito_Prioridade = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_prioridade;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)));
         AV103Requisito_Status = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_status;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0)));
         AV104Requisito_Pontuacao = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_pontuacao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV104Requisito_Pontuacao, 14, 5)));
         ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV106SDT_Requisitos.CurrentItem)).gxTpr_Requisito_editando = true;
         bttBtnbtnincluirreqneg_Caption = "Atualizar Requisito";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbtnincluirreqneg_Internalname, "Caption", bttBtnbtnincluirreqneg_Caption);
         cmbavRequisito_prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_prioridade_Internalname, "Values", cmbavRequisito_prioridade.ToJavascriptSource());
         cmbavRequisito_status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_status_Internalname, "Values", cmbavRequisito_status.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV106SDT_Requisitos", AV106SDT_Requisitos);
         nGXsfl_163_bak_idx = nGXsfl_163_idx;
         gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         nGXsfl_163_idx = nGXsfl_163_bak_idx;
         sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
         SubsflControlProps_1632( ) ;
      }

      protected void E27MJ2( )
      {
         AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
         if ( AV106SDT_Requisitos.Count >= AV124GXV7 )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
         }
         /* Btncontinuar_Click Routine */
         AV64Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV64Agrupador);
         AV66Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Ordem), 4, 0)));
         AV106SDT_Requisitos.Clear();
         gx_BV163 = true;
         AV82DadosDaSS.Load(A456ContagemResultado_Codigo);
         AV14Servico_Codigo = AV82DadosDaSS.gxTpr_Contagemresultado_servicoss;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
         AV36Sistema_Codigo = AV82DadosDaSS.gxTpr_Contagemresultado_sistemacod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0)));
         AV83DataPrevista = AV82DadosDaSS.gxTpr_Contagemresultado_dataprevista;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DataPrevista", context.localUtil.TToC( AV83DataPrevista, 8, 5, 0, 3, "/", ":", " "));
         /* Using cursor H00MJ14 */
         pr_default.execute(8, new Object[] {AV14Servico_Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A157ServicoGrupo_Codigo = H00MJ14_A157ServicoGrupo_Codigo[0];
            A155Servico_Codigo = H00MJ14_A155Servico_Codigo[0];
            GXt_int2 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1551Servico_Responsavel = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
            AV15ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)));
            AV29Servico_Responsavel = A1551Servico_Responsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Servico_Responsavel), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S202 ();
         if (returnInSub) return;
         /* Using cursor H00MJ15 */
         pr_default.execute(9, new Object[] {AV82DadosDaSS.gxTpr_Contagemresultado_codigo});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A2003ContagemResultadoRequisito_OSCod = H00MJ15_A2003ContagemResultadoRequisito_OSCod[0];
            A2004ContagemResultadoRequisito_ReqCod = H00MJ15_A2004ContagemResultadoRequisito_ReqCod[0];
            A2001Requisito_Identificador = H00MJ15_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = H00MJ15_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = H00MJ15_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = H00MJ15_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = H00MJ15_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = H00MJ15_n1923Requisito_Descricao[0];
            A2002Requisito_Prioridade = H00MJ15_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = H00MJ15_n2002Requisito_Prioridade[0];
            A1932Requisito_Pontuacao = H00MJ15_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = H00MJ15_n1932Requisito_Pontuacao[0];
            A1934Requisito_Status = H00MJ15_A1934Requisito_Status[0];
            n1934Requisito_Status = H00MJ15_n1934Requisito_Status[0];
            A1926Requisito_Agrupador = H00MJ15_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = H00MJ15_n1926Requisito_Agrupador[0];
            A2001Requisito_Identificador = H00MJ15_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = H00MJ15_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = H00MJ15_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = H00MJ15_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = H00MJ15_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = H00MJ15_n1923Requisito_Descricao[0];
            A2002Requisito_Prioridade = H00MJ15_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = H00MJ15_n2002Requisito_Prioridade[0];
            A1932Requisito_Pontuacao = H00MJ15_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = H00MJ15_n1932Requisito_Pontuacao[0];
            A1934Requisito_Status = H00MJ15_A1934Requisito_Status[0];
            n1934Requisito_Status = H00MJ15_n1934Requisito_Status[0];
            A1926Requisito_Agrupador = H00MJ15_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = H00MJ15_n1926Requisito_Agrupador[0];
            if ( StringUtil.StrCmp(AV64Agrupador, A1926Requisito_Agrupador) != 0 )
            {
               AV66Ordem = (short)(AV66Ordem+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Ordem), 4, 0)));
            }
            AV106SDT_Requisitos = new GxObjectCollection( context, "SDT_Requisitos.SDT_RequisitosItem", "GxEv3Up14_Meetrika", "SdtSDT_Requisitos_SDT_RequisitosItem", "GeneXus.Programs");
            gx_BV163 = true;
            AV107SDT_RequisitoItem.gxTpr_Requisito_codigo = A2004ContagemResultadoRequisito_ReqCod;
            AV107SDT_RequisitoItem.gxTpr_Requisito_identificador = A2001Requisito_Identificador;
            AV107SDT_RequisitoItem.gxTpr_Requisito_titulo = A1927Requisito_Titulo;
            AV107SDT_RequisitoItem.gxTpr_Requisito_descricao = A1923Requisito_Descricao;
            AV107SDT_RequisitoItem.gxTpr_Requisito_prioridade = A2002Requisito_Prioridade;
            AV107SDT_RequisitoItem.gxTpr_Requisito_agrupador = A1926Requisito_Agrupador;
            AV107SDT_RequisitoItem.gxTpr_Requisito_pontuacao = A1932Requisito_Pontuacao;
            AV107SDT_RequisitoItem.gxTpr_Requisito_status = A1934Requisito_Status;
            AV107SDT_RequisitoItem.gxTpr_Requisito_ordem = AV66Ordem;
            AV107SDT_RequisitoItem.gxTpr_Requisito_sequencia = (short)(AV106SDT_Requisitos.Count+1);
            AV97Requisito_Agrupador = A1926Requisito_Agrupador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Requisito_Agrupador", AV97Requisito_Agrupador);
            AV64Agrupador = A1926Requisito_Agrupador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV64Agrupador);
            AV106SDT_Requisitos.Add(AV107SDT_RequisitoItem, 0);
            gx_BV163 = true;
            pr_default.readNext(9);
         }
         pr_default.close(9);
         AV84Sdt_ContagemResultadoEvidencias.Clear();
         /* Using cursor H00MJ16 */
         pr_default.execute(10, new Object[] {AV82DadosDaSS.gxTpr_Contagemresultado_codigo});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A456ContagemResultado_Codigo = H00MJ16_A456ContagemResultado_Codigo[0];
            A589ContagemResultadoEvidencia_NomeArq = H00MJ16_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = H00MJ16_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A590ContagemResultadoEvidencia_TipoArq = H00MJ16_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = H00MJ16_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A586ContagemResultadoEvidencia_Codigo = H00MJ16_A586ContagemResultadoEvidencia_Codigo[0];
            A1449ContagemResultadoEvidencia_Link = H00MJ16_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = H00MJ16_n1449ContagemResultadoEvidencia_Link[0];
            A591ContagemResultadoEvidencia_Data = H00MJ16_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = H00MJ16_n591ContagemResultadoEvidencia_Data[0];
            A587ContagemResultadoEvidencia_Descricao = H00MJ16_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = H00MJ16_n587ContagemResultadoEvidencia_Descricao[0];
            A1393ContagemResultadoEvidencia_RdmnCreated = H00MJ16_A1393ContagemResultadoEvidencia_RdmnCreated[0];
            n1393ContagemResultadoEvidencia_RdmnCreated = H00MJ16_n1393ContagemResultadoEvidencia_RdmnCreated[0];
            A645TipoDocumento_Codigo = H00MJ16_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = H00MJ16_n645TipoDocumento_Codigo[0];
            A1493ContagemResultadoEvidencia_Owner = H00MJ16_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = H00MJ16_n1493ContagemResultadoEvidencia_Owner[0];
            A588ContagemResultadoEvidencia_Arquivo = H00MJ16_A588ContagemResultadoEvidencia_Arquivo[0];
            n588ContagemResultadoEvidencia_Arquivo = H00MJ16_n588ContagemResultadoEvidencia_Arquivo[0];
            AV85Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
            AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_codigo = A586ContagemResultadoEvidencia_Codigo;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) )
            {
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = A1449ContagemResultadoEvidencia_Link;
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link = A1449ContagemResultadoEvidencia_Link;
            }
            else
            {
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo = A588ContagemResultadoEvidencia_Arquivo;
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = A589ContagemResultadoEvidencia_NomeArq;
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq = A590ContagemResultadoEvidencia_TipoArq;
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data = A591ContagemResultadoEvidencia_Data;
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao = A587ContagemResultadoEvidencia_Descricao;
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_rdmncreated = A1393ContagemResultadoEvidencia_RdmnCreated;
               AV85Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo = A645TipoDocumento_Codigo;
               AV85Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner = A1493ContagemResultadoEvidencia_Owner;
            }
            AV84Sdt_ContagemResultadoEvidencias.Add(AV85Sdt_ArquivoEvidencia, 0);
            pr_default.readNext(10);
         }
         pr_default.close(10);
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "SelectTab", "", new Object[] {(String)"TabRequisitos"});
         AV21Websession.Set("ArquivosEvd", AV84Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
         context.DoAjaxRefreshCmp("W0099"+"");
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV106SDT_Requisitos", AV106SDT_Requisitos);
         nGXsfl_163_bak_idx = nGXsfl_163_idx;
         gxgrGridsdt_requisitos_refresh( subGridsdt_requisitos_Rows, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, AV16SolicitacaoServico, A155Servico_Codigo, A157ServicoGrupo_Codigo, A605Servico_Sigla, A158ServicoGrupo_Descricao, A2047Servico_LinNegCod, A632Servico_Ativo, A1635Servico_IsPublico, AV35TemServicoPadrao, A63ContratanteUsuario_ContratanteCod, AV22WWPContext, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo, A129Sistema_Sigla, A633Servico_UO, AV17Usuario_CargoUOCod, A1530Servico_TipoHierarquia, AV15ServicoGrupo_Codigo, AV64Agrupador, AV106SDT_Requisitos) ;
         nGXsfl_163_idx = nGXsfl_163_bak_idx;
         sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
         SubsflControlProps_1632( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82DadosDaSS", AV82DadosDaSS);
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
         dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", dynavSistema_codigo.ToJavascriptSource());
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107SDT_RequisitoItem", AV107SDT_RequisitoItem);
      }

      protected void E18MJ2( )
      {
         AV124GXV7 = (short)(nGXsfl_163_idx+GRIDSDT_REQUISITOS_nFirstRecordOnPage);
         if ( AV106SDT_Requisitos.Count >= AV124GXV7 )
         {
            AV106SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7));
         }
         /* Indicadorautomatico_Click Routine */
         edtavRequisito_identificador_Enabled = (!AV91IndicadorAutomatico ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_identificador_Enabled), 5, 0)));
         if ( AV91IndicadorAutomatico )
         {
            /* Execute user subroutine: 'NUMERARIDENTIFICADOR' */
            S222 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107SDT_RequisitoItem", AV107SDT_RequisitoItem);
      }

      protected void S182( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         if ( AV35TemServicoPadrao )
         {
            edtavDadosdass_contagemresultado_restricoes_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_restricoes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_restricoes_Visible), 5, 0)));
            edtavDadosdass_contagemresultado_prioridadeprevista_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_prioridadeprevista_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_prioridadeprevista_Visible), 5, 0)));
            edtavDataprevista_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDataprevista_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDataprevista_Visible), 5, 0)));
            edtavDadosdass_contagemresultado_referencia_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_referencia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_referencia_Visible), 5, 0)));
            edtavDadosdass_contagemresultado_ss_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_ss_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_ss_Visible), 5, 0)));
            cmbavServicogrupo_codigo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServicogrupo_codigo.Visible), 5, 0)));
            cmbavServico_codigo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServico_codigo.Visible), 5, 0)));
            lblTbjava_Caption = "<script type='text/javascript'> DADOSDASS_CONTAGEMRESULTADO_DESCRICAO.rows=1; </script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         }
         else
         {
            edtavDadosdass_contagemresultado_restricoes_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_restricoes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_restricoes_Visible), 5, 0)));
            edtavDadosdass_contagemresultado_prioridadeprevista_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_prioridadeprevista_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_prioridadeprevista_Visible), 5, 0)));
            edtavDataprevista_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDataprevista_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDataprevista_Visible), 5, 0)));
            edtavDadosdass_contagemresultado_referencia_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_referencia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_referencia_Visible), 5, 0)));
            edtavDadosdass_contagemresultado_prioridadeprevista_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_prioridadeprevista_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_prioridadeprevista_Visible), 5, 0)));
            edtavDadosdass_contagemresultado_ss_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDadosdass_contagemresultado_ss_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDadosdass_contagemresultado_ss_Visible), 5, 0)));
            cmbavServicogrupo_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServicogrupo_codigo.Visible), 5, 0)));
            cmbavServico_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServico_codigo.Visible), 5, 0)));
            lblTbjava_Caption = "<script type='text/javascript'> DADOSDASS_CONTAGEMRESULTADO_OBSERVACAO.rows=12; </script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         }
      }

      protected void S172( )
      {
         /* 'CARREGAGRUPOSDESERVICOS' Routine */
         cmbavServicogrupo_codigo.removeAllItems();
         cmbavServicogrupo_codigo.addItem("0", "Todos", 0);
         /* Using cursor H00MJ17 */
         pr_default.execute(11);
         while ( (pr_default.getStatus(11) != 101) )
         {
            A1635Servico_IsPublico = H00MJ17_A1635Servico_IsPublico[0];
            A632Servico_Ativo = H00MJ17_A632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00MJ17_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = H00MJ17_A158ServicoGrupo_Descricao[0];
            A158ServicoGrupo_Descricao = H00MJ17_A158ServicoGrupo_Descricao[0];
            cmbavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)), A158ServicoGrupo_Descricao, 0);
            pr_default.readNext(11);
         }
         pr_default.close(11);
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S202( )
      {
         /* 'CARREGASERVICOS' Routine */
         cmbavServico_codigo.removeAllItems();
         cmbavServico_codigo.addItem("0", "(Nenhum)", 0);
         pr_default.dynParam(12, new Object[]{ new Object[]{
                                              AV17Usuario_CargoUOCod ,
                                              A633Servico_UO ,
                                              A632Servico_Ativo ,
                                              A1635Servico_IsPublico ,
                                              A1530Servico_TipoHierarquia ,
                                              A157ServicoGrupo_Codigo ,
                                              AV15ServicoGrupo_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00MJ18 */
         pr_default.execute(12, new Object[] {AV15ServicoGrupo_Codigo, AV17Usuario_CargoUOCod});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A1530Servico_TipoHierarquia = H00MJ18_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = H00MJ18_n1530Servico_TipoHierarquia[0];
            A157ServicoGrupo_Codigo = H00MJ18_A157ServicoGrupo_Codigo[0];
            A1635Servico_IsPublico = H00MJ18_A1635Servico_IsPublico[0];
            A632Servico_Ativo = H00MJ18_A632Servico_Ativo[0];
            A633Servico_UO = H00MJ18_A633Servico_UO[0];
            n633Servico_UO = H00MJ18_n633Servico_UO[0];
            A155Servico_Codigo = H00MJ18_A155Servico_Codigo[0];
            A605Servico_Sigla = H00MJ18_A605Servico_Sigla[0];
            cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), A605Servico_Sigla, 0);
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      protected void S192( )
      {
         /* 'CARREGASISTEMAS' Routine */
         /* Using cursor H00MJ19 */
         pr_default.execute(13, new Object[] {AV22WWPContext.gxTpr_Contratante_codigo, AV22WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A60ContratanteUsuario_UsuarioCod = H00MJ19_A60ContratanteUsuario_UsuarioCod[0];
            A63ContratanteUsuario_ContratanteCod = H00MJ19_A63ContratanteUsuario_ContratanteCod[0];
            A129Sistema_Sigla = H00MJ19_A129Sistema_Sigla[0];
            A127Sistema_Codigo = H00MJ19_A127Sistema_Codigo[0];
            A129Sistema_Sigla = H00MJ19_A129Sistema_Sigla[0];
            dynavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)), A129Sistema_Sigla, 0);
            pr_default.readNext(13);
         }
         pr_default.close(13);
      }

      protected void S232( )
      {
         /* 'GETEMAILS' Routine */
         AV20Usuarios.Clear();
         AV20Usuarios.Add(AV29Servico_Responsavel, 0);
         AV20Usuarios.Add(AV22WWPContext.gxTpr_Userid, 0);
      }

      protected void S142( )
      {
         /* 'CARREGA CODIGO SS' Routine */
         AV28AreaTrabalho.Load(AV22WWPContext.gxTpr_Areatrabalho_codigo);
         AV28AreaTrabalho.gxTpr_Areatrabalho_ss_codigo = (int)(AV28AreaTrabalho.gxTpr_Areatrabalho_ss_codigo+1);
         AV82DadosDaSS.gxTpr_Contagemresultado_demandafm = StringUtil.Trim( StringUtil.Str( (decimal)(AV28AreaTrabalho.gxTpr_Areatrabalho_ss_codigo), 6, 0));
         AV82DadosDaSS.gxTpr_Contagemresultado_ss = AV28AreaTrabalho.gxTpr_Areatrabalho_ss_codigo;
         AV28AreaTrabalho.Save();
         if ( ! AV28AreaTrabalho.Success() )
         {
            AV82DadosDaSS.gxTpr_Contagemresultado_ss = 0;
         }
      }

      protected void S162( )
      {
         /* 'ENVIONOTIFICACAO' Routine */
         /* Execute user subroutine: 'GETEMAILS' */
         S232 ();
         if (returnInSub) return;
         if ( AV27ContratanteSemEmailSda )
         {
            AV25Resultado = " Cadastro da Contratante sem servidor configurado para envio de e-mails!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Resultado", AV25Resultado);
         }
         else
         {
            AV24EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "H� uma nova solicita��o de Servi�o para a prestadora do servi�o de " + cmbavServico_codigo.Description + " conforme segue abaixo os detalhes." + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "No da SS�������������������" + AV16SolicitacaoServico.gxTpr_Contagemresultado_demandafm + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Servi�o��������������������" + cmbavServico_codigo.Description + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Descri��o������������������" + StringUtil.Trim( AV16SolicitacaoServico.gxTpr_Contagemresultado_descricao) + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Data de Abertura�����������" + context.localUtil.DToC( AV16SolicitacaoServico.gxTpr_Contagemresultado_datadmn, 2, "/") + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Expectativa de Atendimento�" + context.localUtil.TToC( AV16SolicitacaoServico.gxTpr_Contagemresultado_dataprevista, 8, 5, 0, 3, "/", ":", " ") + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Requisitante���������������" + AV22WWPContext.gxTpr_Username + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Telefone�������������������" + AV22WWPContext.gxTpr_Contratante_telefone + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Unidade Organizacional�����" + StringUtil.Trim( AV22WWPContext.gxTpr_Areatrabalho_descricao) + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + "Para atendimento da demanda acesse o MEETRIKA - Sistema de Gest�o Contratual e Automa��o de Processos " + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV22WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV22WWPContext.gxTpr_Username) + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + " (autorizado por " + StringUtil.Trim( AV19Usuario_PessoaNom) + ")" + StringUtil.NewLine( );
            AV24EmailText = AV24EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
            AV9Demandante = AV22WWPContext.gxTpr_Areatrabalho_descricao;
            AV12NomeSistema = AV22WWPContext.gxTpr_Parametrossistema_nomesistema;
            AV26Subject = "[" + StringUtil.Trim( AV12NomeSistema) + " - " + StringUtil.Trim( AV9Demandante) + "] SS " + StringUtil.Trim( AV16SolicitacaoServico.gxTpr_Contagemresultado_demandafm) + " solicitada" + " (No reply)";
            AV93Codigos.Add(AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo, 0);
            AV21Websession.Set("DemandaCodigo", AV93Codigos.ToXml(false, true, "Collection", ""));
            new prc_enviaremail(context ).execute(  AV22WWPContext.gxTpr_Areatrabalho_codigo,  AV20Usuarios,  AV26Subject,  AV24EmailText,  AV23Attachments, ref  AV25Resultado) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Resultado", AV25Resultado);
            AV21Websession.Remove("DemandaCodigo");
         }
         Confirmpanel_Confirmationtext = "Servi�o "+StringUtil.Trim( StringUtil.Str( (decimal)(AV16SolicitacaoServico.gxTpr_Contagemresultado_ss), 8, 0))+" solicitado."+StringUtil.NewLine( )+AV25Resultado;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel_Internalname, "ConfirmationText", Confirmpanel_Confirmationtext);
         this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
      }

      protected void S212( )
      {
         /* 'BTNENTER' Routine */
         if ( ( AV14Servico_Codigo > 0 ) && (0==AV29Servico_Responsavel) )
         {
            bttBtnenter_Tooltiptext = "O Servi�o n�o tem respons�vel cadastrado!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            GX_msglist.addItem("Servi�o sem respons�vel cadastrado. Esta solicita��o n�o poder� ser enviada.");
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
         }
         else if ( ! AV22WWPContext.gxTpr_Insert )
         {
            bttBtnenter_Tooltiptext = "A��o n�o permitida ao seu usu�rio!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            bttBtnenter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
         }
         else
         {
            bttBtnenter_Tooltiptext = "Solicitar servi�o";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            bttBtnenter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Enabled), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'CHECKOUT' Routine */
         AV5CheckRequiredFieldsResult = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5CheckRequiredFieldsResult", AV5CheckRequiredFieldsResult);
         if ( ! AV35TemServicoPadrao && (0==AV15ServicoGrupo_Codigo) )
         {
            GX_msglist.addItem("Grupo deve ser informado");
            GX_FocusControl = cmbavServicogrupo_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ! AV35TemServicoPadrao && (0==AV14Servico_Codigo) )
         {
            GX_msglist.addItem("Servi�o deve ser informado");
            GX_FocusControl = cmbavServico_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV36Sistema_Codigo) )
         {
            GX_msglist.addItem("Sistema deve ser informado");
            GX_FocusControl = dynavSistema_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ! AV35TemServicoPadrao && String.IsNullOrEmpty(StringUtil.RTrim( AV82DadosDaSS.gxTpr_Contagemresultado_referencia)) )
         {
            GX_msglist.addItem("Refer�ncia Externa deve ser informada");
            GX_FocusControl = edtavDadosdass_contagemresultado_referencia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ! AV35TemServicoPadrao && String.IsNullOrEmpty(StringUtil.RTrim( AV82DadosDaSS.gxTpr_Contagemresultado_descricao)) )
         {
            GX_msglist.addItem("Assunto deve ser informado");
            GX_FocusControl = edtavDadosdass_contagemresultado_descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV82DadosDaSS.gxTpr_Contagemresultado_observacao)) )
         {
            GX_msglist.addItem("Descri��o deve ser informada");
            GX_FocusControl = edtavDadosdass_contagemresultado_observacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ! AV35TemServicoPadrao && ( AV83DataPrevista < DateTimeUtil.ServerNow( context, "DEFAULT") ) )
         {
            GX_msglist.addItem("Limita��o de Data deve ser informada");
            GX_FocusControl = edtavDataprevista_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ! AV35TemServicoPadrao && String.IsNullOrEmpty(StringUtil.RTrim( AV82DadosDaSS.gxTpr_Contagemresultado_restricoes)) )
         {
            GX_msglist.addItem("Limita��o Geral deve ser informada");
            GX_FocusControl = edtavDadosdass_contagemresultado_restricoes_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV29Servico_Responsavel) )
         {
            GX_msglist.addItem("Sem respons�vel do servi�o cadastrado para o envio da solicita��o!");
         }
         else
         {
            AV5CheckRequiredFieldsResult = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5CheckRequiredFieldsResult", AV5CheckRequiredFieldsResult);
         }
      }

      protected void S152( )
      {
         /* 'SETDADOSSOLICITACAO' Routine */
         if ( AV82DadosDaSS.gxTpr_Contagemresultado_codigo > 0 )
         {
            AV16SolicitacaoServico.Load(AV82DadosDaSS.gxTpr_Contagemresultado_codigo);
         }
         AV73StatusAnterior = AV16SolicitacaoServico.gxTpr_Contagemresultado_statusdmn;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73StatusAnterior", AV73StatusAnterior);
         GXt_int3 = AV30Dias;
         new prc_diasuteisentre(context ).execute(  AV16SolicitacaoServico.gxTpr_Contagemresultado_datadmn,  DateTimeUtil.ResetTime( AV83DataPrevista),  "U", out  GXt_int3) ;
         AV30Dias = GXt_int3;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_servicoss = AV14Servico_Codigo;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_dataprevista = AV83DataPrevista;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_dataentrega = AV83DataPrevista;
         GXt_dtime4 = DateTimeUtil.ResetDate(AV83DataPrevista);
         AV16SolicitacaoServico.gxTpr_Contagemresultado_horaentrega = GXt_dtime4;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_prazoinicialdias = AV30Dias;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_sistemacod = AV36Sistema_Codigo;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_evento = 1;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_datacadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV16SolicitacaoServico.gxTpr_Contagemresultado_osmanual = true;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_descricao = AV82DadosDaSS.gxTpr_Contagemresultado_descricao;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_observacao = StringUtil.Trim( AV82DadosDaSS.gxTpr_Contagemresultado_observacao);
         AV16SolicitacaoServico.gxTpr_Contagemresultado_referencia = AV82DadosDaSS.gxTpr_Contagemresultado_referencia;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_restricoes = AV82DadosDaSS.gxTpr_Contagemresultado_restricoes;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_prioridadeprevista = AV82DadosDaSS.gxTpr_Contagemresultado_prioridadeprevista;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_ss = AV82DadosDaSS.gxTpr_Contagemresultado_ss;
         AV16SolicitacaoServico.gxTpr_Contagemresultado_demandafm = AV82DadosDaSS.gxTpr_Contagemresultado_demandafm;
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Modulo_codigo_SetNull();
         AV16SolicitacaoServico.gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_SetNull();
      }

      protected void S112( )
      {
         /* 'SETREQUISITOS' Routine */
         AV153GXV16 = 1;
         while ( AV153GXV16 <= AV106SDT_Requisitos.Count )
         {
            AV107SDT_RequisitoItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV153GXV16));
            if ( (0==AV107SDT_RequisitoItem.gxTpr_Requisito_codigo) )
            {
               AV96Requisito = new SdtRequisito(context);
               AV96Requisito.gxTv_SdtRequisito_Requisito_reqcod_SetNull();
               AV96Requisito.gxTv_SdtRequisito_Proposta_codigo_SetNull();
               AV96Requisito.gxTv_SdtRequisito_Requisito_tiporeqcod_SetNull();
            }
            else
            {
               if ( AV107SDT_RequisitoItem.gxTpr_Requisito_codigo < 0 )
               {
                  AV98Requisito_Codigo = (int)(AV107SDT_RequisitoItem.gxTpr_Requisito_codigo*-1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV98Requisito_Codigo), 6, 0)));
                  /* Using cursor H00MJ20 */
                  pr_default.execute(14, new Object[] {AV98Requisito_Codigo, AV16SolicitacaoServico.gxTpr_Contagemresultado_codigo});
                  while ( (pr_default.getStatus(14) != 101) )
                  {
                     A2004ContagemResultadoRequisito_ReqCod = H00MJ20_A2004ContagemResultadoRequisito_ReqCod[0];
                     A2003ContagemResultadoRequisito_OSCod = H00MJ20_A2003ContagemResultadoRequisito_OSCod[0];
                     A2005ContagemResultadoRequisito_Codigo = H00MJ20_A2005ContagemResultadoRequisito_Codigo[0];
                     AV112ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(14);
                  }
                  pr_default.close(14);
                  AV113ContagemResultadoRequisito.Load(AV112ContagemResultadoRequisito_Codigo);
                  AV113ContagemResultadoRequisito.Delete();
                  AV96Requisito.Load(AV98Requisito_Codigo);
                  AV96Requisito.Delete();
               }
               else
               {
                  AV96Requisito.Load(AV107SDT_RequisitoItem.gxTpr_Requisito_codigo);
               }
            }
            if ( AV107SDT_RequisitoItem.gxTpr_Requisito_codigo >= 0 )
            {
               AV96Requisito.gxTpr_Requisito_identificador = AV107SDT_RequisitoItem.gxTpr_Requisito_identificador;
               AV96Requisito.gxTpr_Requisito_titulo = AV107SDT_RequisitoItem.gxTpr_Requisito_titulo;
               AV96Requisito.gxTpr_Requisito_descricao = AV107SDT_RequisitoItem.gxTpr_Requisito_descricao;
               AV96Requisito.gxTpr_Requisito_agrupador = AV107SDT_RequisitoItem.gxTpr_Requisito_agrupador;
               AV96Requisito.gxTpr_Requisito_prioridade = AV107SDT_RequisitoItem.gxTpr_Requisito_prioridade;
               AV96Requisito.gxTpr_Requisito_status = AV107SDT_RequisitoItem.gxTpr_Requisito_status;
               AV96Requisito.gxTpr_Requisito_pontuacao = AV107SDT_RequisitoItem.gxTpr_Requisito_pontuacao;
               AV96Requisito.Save();
               if ( AV96Requisito.Success() )
               {
                  AV113ContagemResultadoRequisito = new SdtContagemResultadoRequisito(context);
                  AV113ContagemResultadoRequisito.gxTpr_Contagemresultadorequisito_oscod = AV67Codigo;
                  AV113ContagemResultadoRequisito.gxTpr_Contagemresultadorequisito_reqcod = AV96Requisito.gxTpr_Requisito_codigo;
                  AV113ContagemResultadoRequisito.gxTpr_Contagemresultadorequisito_owner = true;
                  AV113ContagemResultadoRequisito.Save();
               }
            }
            AV153GXV16 = (int)(AV153GXV16+1);
         }
      }

      protected void S122( )
      {
         /* 'MESSAGES' Routine */
         AV156GXV18 = 1;
         AV155GXV17 = AV96Requisito.GetMessages();
         while ( AV156GXV18 <= AV155GXV17.Count )
         {
            AV10Message = ((SdtMessages_Message)AV155GXV17.Item(AV156GXV18));
            GX_msglist.addItem(StringUtil.Trim( AV10Message.gxTpr_Description));
            AV156GXV18 = (int)(AV156GXV18+1);
         }
      }

      protected void S222( )
      {
         /* 'NUMERARIDENTIFICADOR' Routine */
         if ( AV106SDT_Requisitos.Count == 0 )
         {
            AV100Requisito_Identificador = "1";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Requisito_Identificador", AV100Requisito_Identificador);
         }
         else
         {
            AV46Index = 0;
            AV157GXV19 = 1;
            while ( AV157GXV19 <= AV106SDT_Requisitos.Count )
            {
               AV107SDT_RequisitoItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV157GXV19));
               AV46Index = (short)(NumberUtil.Val( AV107SDT_RequisitoItem.gxTpr_Requisito_identificador, "."));
               if ( AV78i < AV46Index )
               {
                  AV78i = AV46Index;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78i), 4, 0)));
               }
               AV157GXV19 = (int)(AV157GXV19+1);
            }
            AV100Requisito_Identificador = StringUtil.Trim( StringUtil.Str( (decimal)(AV78i+1), 18, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Requisito_Identificador", AV100Requisito_Identificador);
            AV106SDT_Requisitos.Sort("Requisito_Agrupador, Requisito_Ordem, Requisito_Sequencia");
            gx_BV163 = true;
         }
      }

      private void E25MJ3( )
      {
         /* Gridrascunho_Load Routine */
         AV75btnContinuar = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtncontinuar_Internalname, AV75btnContinuar);
         AV131Btncontinuar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavBtncontinuar_Tooltiptext = "Continuar cadastro";
         AV76btnCancelar = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtncancelar_Internalname, AV76btnCancelar);
         AV132Btncancelar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavBtncancelar_Tooltiptext = "Cancelar solicita��o";
         edtContagemResultado_DataDmn_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "ShowTab", "", new Object[] {(String)"TabRascunho"});
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "SetTabTitle", "", new Object[] {(String)"TabRascunho","Rascunho ("+StringUtil.Trim( StringUtil.Str( (decimal)(subGridrascunho_Recordcount( )), 10, 0))+")"});
         AV77QtdRequisitos = (short)(A40000GXC1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdrequisitos_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV77QtdRequisitos), 4, 0)));
         AV86QtdAnexos = (short)(A40001GXC2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdanexos_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV86QtdAnexos), 4, 0)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 178;
         }
         sendrow_1783( ) ;
         GRIDRASCUNHO_nCurrentRecord = (long)(GRIDRASCUNHO_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_178_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(178, GridrascunhoRow);
         }
      }

      protected void wb_table1_2_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_MJ2( true) ;
         }
         else
         {
            wb_table2_5_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_11_MJ2( true) ;
         }
         else
         {
            wb_table3_11_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_191_MJ2( true) ;
         }
         else
         {
            wb_table4_191_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_191_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_201_MJ2( true) ;
         }
         else
         {
            wb_table5_201_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_201_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MJ2e( true) ;
         }
         else
         {
            wb_table1_2_MJ2e( false) ;
         }
      }

      protected void wb_table5_201_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblUsrtable_Internalname, tblUsrtable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 206,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Servico_Responsavel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29Servico_Responsavel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,206);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_responsavel_Jsonclick, 0, "Attribute", "", "", "", edtavServico_responsavel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SS.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 207,'',false,'" + sGXsfl_163_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavTemservicopadrao_Internalname, StringUtil.BoolToStr( AV35TemServicoPadrao), "", "", chkavTemservicopadrao.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(207, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,207);\"");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_201_MJ2e( true) ;
         }
         else
         {
            wb_table5_201_MJ2e( false) ;
         }
      }

      protected void wb_table4_191_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTableactions_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 194,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(163), 3, 0)+","+"null"+");", bttBtnenter_Caption, bttBtnenter_Jsonclick, 5, bttBtnenter_Tooltiptext, "", StyleString, ClassString, 1, bttBtnenter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 196,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(163), 3, 0)+","+"null"+");", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 198,'',false,'',0)\"";
            ClassString = "btn btn-primary";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnsalvar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(163), 3, 0)+","+"null"+");", "Salvar rascunho", bttBtnsalvar_Jsonclick, 5, "Salvar rascunho", "", StyleString, ClassString, bttBtnsalvar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOSALVAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_191_MJ2e( true) ;
         }
         else
         {
            wb_table4_191_MJ2e( false) ;
         }
      }

      protected void wb_table3_11_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 0, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Nova Solicita��o de Servi�o", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", lblTextblocktitle_Visible, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleTabSolicitacao"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Solicita��o") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TabSolicitacao"+"\" style=\"display:none;\">") ;
            wb_table6_20_MJ2( true) ;
         }
         else
         {
            wb_table6_20_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_20_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleTabRequisitos"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TabRequisitos"+"\" style=\"display:none;\">") ;
            wb_table7_102_MJ2( true) ;
         }
         else
         {
            wb_table7_102_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_102_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleTabRascunho"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TabRascunho"+"\" style=\"display:none;\">") ;
            wb_table8_175_MJ2( true) ;
         }
         else
         {
            wb_table8_175_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table8_175_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_MJ2e( true) ;
         }
         else
         {
            wb_table3_11_MJ2e( false) ;
         }
      }

      protected void wb_table8_175_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridrascunhoContainer.SetWrapped(nGXWrapped);
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridrascunhoContainer"+"DivS\" data-gxgridid=\"178\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridrascunho_Internalname, subGridrascunho_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridrascunho_Backcolorstyle == 0 )
               {
                  subGridrascunho_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridrascunho_Class) > 0 )
                  {
                     subGridrascunho_Linesclass = subGridrascunho_Class+"Title";
                  }
               }
               else
               {
                  subGridrascunho_Titlebackstyle = 1;
                  if ( subGridrascunho_Backcolorstyle == 1 )
                  {
                     subGridrascunho_Titlebackcolor = subGridrascunho_Allbackcolor;
                     if ( StringUtil.Len( subGridrascunho_Class) > 0 )
                     {
                        subGridrascunho_Linesclass = subGridrascunho_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridrascunho_Class) > 0 )
                     {
                        subGridrascunho_Linesclass = subGridrascunho_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(63), 4, 0))+"px"+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data da SS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "N� da SS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Titulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Limite") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Cadastro") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Requisitos") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrascunho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Anexos") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridrascunhoContainer.AddObjectProperty("GridName", "Gridrascunho");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridrascunhoContainer = new GXWebGrid( context);
               }
               else
               {
                  GridrascunhoContainer.Clear();
               }
               GridrascunhoContainer.SetWrapped(nGXWrapped);
               GridrascunhoContainer.AddObjectProperty("GridName", "Gridrascunho");
               GridrascunhoContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridrascunhoContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Backcolorstyle), 1, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("CmpContext", "");
               GridrascunhoContainer.AddObjectProperty("InMasterPage", "false");
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", context.convertURL( AV75btnContinuar));
               GridrascunhoColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtncontinuar_Tooltiptext));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", context.convertURL( AV76btnCancelar));
               GridrascunhoColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtncancelar_Tooltiptext));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               GridrascunhoColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultado_DataDmn_Link));
               GridrascunhoColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtContagemResultado_DataDmn_Linktarget));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", A494ContagemResultado_Descricao);
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 10, 8, 0, 3, "/", ":", " "));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77QtdRequisitos), 4, 0, ".", "")));
               GridrascunhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtdrequisitos_Enabled), 5, 0, ".", "")));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrascunhoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86QtdAnexos), 4, 0, ".", "")));
               GridrascunhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtdanexos_Enabled), 5, 0, ".", "")));
               GridrascunhoContainer.AddColumnProperties(GridrascunhoColumn);
               GridrascunhoContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Allowselection), 1, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Selectioncolor), 9, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Allowhovering), 1, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Hoveringcolor), 9, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Allowcollapsing), 1, 0, ".", "")));
               GridrascunhoContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrascunho_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 178 )
         {
            wbEnd = 0;
            nRC_GXsfl_178 = (short)(nGXsfl_178_idx-1);
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridrascunhoContainer.AddObjectProperty("GRIDRASCUNHO_nEOF", GRIDRASCUNHO_nEOF);
               GridrascunhoContainer.AddObjectProperty("GRIDRASCUNHO_nFirstRecordOnPage", GRIDRASCUNHO_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridrascunhoContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridrascunho", GridrascunhoContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrascunhoContainerData", GridrascunhoContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrascunhoContainerData"+"V", GridrascunhoContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridrascunhoContainerData"+"V"+"\" value='"+GridrascunhoContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_175_MJ2e( true) ;
         }
         else
         {
            wb_table8_175_MJ2e( false) ;
         }
      }

      protected void wb_table7_102_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_105_MJ2( true) ;
         }
         else
         {
            wb_table9_105_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table9_105_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_149_MJ2( true) ;
         }
         else
         {
            wb_table10_149_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table10_149_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_155_MJ2( true) ;
         }
         else
         {
            wb_table11_155_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table11_155_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_102_MJ2e( true) ;
         }
         else
         {
            wb_table7_102_MJ2e( false) ;
         }
      }

      protected void wb_table11_155_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 5, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_REQUISITOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_REQUISITOSContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table12_160_MJ2( true) ;
         }
         else
         {
            wb_table12_160_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table12_160_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_155_MJ2e( true) ;
         }
         else
         {
            wb_table11_155_MJ2e( false) ;
         }
      }

      protected void wb_table12_160_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblRequisitos_Internalname, tblRequisitos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            Gridsdt_requisitosContainer.SetWrapped(nGXWrapped);
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Gridsdt_requisitosContainer"+"DivS\" data-gxgridid=\"163\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridsdt_requisitos_Internalname, subGridsdt_requisitos_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridsdt_requisitos_Backcolorstyle == 0 )
               {
                  subGridsdt_requisitos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridsdt_requisitos_Class) > 0 )
                  {
                     subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"Title";
                  }
               }
               else
               {
                  subGridsdt_requisitos_Titlebackstyle = 1;
                  if ( subGridsdt_requisitos_Backcolorstyle == 1 )
                  {
                     subGridsdt_requisitos_Titlebackcolor = subGridsdt_requisitos_Allbackcolor;
                     if ( StringUtil.Len( subGridsdt_requisitos_Class) > 0 )
                     {
                        subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridsdt_requisitos_Class) > 0 )
                     {
                        subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+((edtavBtnalterarreqneg_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+((edtavBtnexcluirreqneg_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(100), 4, 0))+"px"+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Agrupador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(100), 4, 0))+"px"+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Identificador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(150), 4, 0))+"px"+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prioridade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsdt_requisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Situa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Gridsdt_requisitosContainer.AddObjectProperty("GridName", "Gridsdt_requisitos");
            }
            else
            {
               Gridsdt_requisitosContainer.AddObjectProperty("GridName", "Gridsdt_requisitos");
               Gridsdt_requisitosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               Gridsdt_requisitosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Backcolorstyle), 1, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("CmpContext", "");
               Gridsdt_requisitosContainer.AddObjectProperty("InMasterPage", "false");
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Value", context.convertURL( AV62btnAlterarReqNeg));
               Gridsdt_requisitosColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnalterarreqneg_Tooltiptext));
               Gridsdt_requisitosColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnalterarreqneg_Visible), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Value", context.convertURL( AV44btnExcluirReqNeg));
               Gridsdt_requisitosColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnexcluirreqneg_Tooltiptext));
               Gridsdt_requisitosColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnexcluirreqneg_Visible), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_codigo_Enabled), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Value", AV64Agrupador);
               Gridsdt_requisitosColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAgrupador_Forecolor), 9, 0, ".", "")));
               Gridsdt_requisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAgrupador_Enabled), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_identificador_Fontstrikethru), 1, 0, ".", "")));
               Gridsdt_requisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_identificador_Enabled), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_titulo_Fontstrikethru), 1, 0, ".", "")));
               Gridsdt_requisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_titulo_Enabled), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_descricao_Enabled), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavSdt_requisitos__requisito_prioridade.FontStrikethru), 1, 0, ".", "")));
               Gridsdt_requisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavSdt_requisitos__requisito_prioridade.Enabled), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               Gridsdt_requisitosColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavSdt_requisitos__requisito_status.FontStrikethru), 1, 0, ".", "")));
               Gridsdt_requisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavSdt_requisitos__requisito_status.Enabled), 5, 0, ".", "")));
               Gridsdt_requisitosContainer.AddColumnProperties(Gridsdt_requisitosColumn);
               Gridsdt_requisitosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Allowselection), 1, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Selectioncolor), 9, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Allowhovering), 1, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Hoveringcolor), 9, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Allowcollapsing), 1, 0, ".", "")));
               Gridsdt_requisitosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsdt_requisitos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 163 )
         {
            wbEnd = 0;
            nRC_GXsfl_163 = (short)(nGXsfl_163_idx-1);
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               Gridsdt_requisitosContainer.AddObjectProperty("GRIDSDT_REQUISITOS_nEOF", GRIDSDT_REQUISITOS_nEOF);
               Gridsdt_requisitosContainer.AddObjectProperty("GRIDSDT_REQUISITOS_nFirstRecordOnPage", GRIDSDT_REQUISITOS_nFirstRecordOnPage);
               AV124GXV7 = nGXsfl_163_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Gridsdt_requisitosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridsdt_requisitos", Gridsdt_requisitosContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Gridsdt_requisitosContainerData", Gridsdt_requisitosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Gridsdt_requisitosContainerData"+"V", Gridsdt_requisitosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridsdt_requisitosContainerData"+"V"+"\" value='"+Gridsdt_requisitosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_160_MJ2e( true) ;
         }
         else
         {
            wb_table12_160_MJ2e( false) ;
         }
      }

      protected void wb_table10_149_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable6_Internalname, tblUnnamedtable6_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'',0)\"";
            ClassString = "btn btn-info";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnincluirreqneg_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(163), 3, 0)+","+"null"+");", bttBtnbtnincluirreqneg_Caption, bttBtnbtnincluirreqneg_Jsonclick, 5, "Clique aqui para Incluir  Novo Requisito de Neg�cio", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBTNINCLUIRREQNEG\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_149_MJ2e( true) ;
         }
         else
         {
            wb_table10_149_MJ2e( false) ;
         }
      }

      protected void wb_table9_105_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_agrupador_Internalname, "Agrupador", "", "", lblTextblockrequisito_agrupador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisito_agrupador_Internalname, AV97Requisito_Agrupador, StringUtil.RTrim( context.localUtil.Format( AV97Requisito_Agrupador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisito_agrupador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_identificador_Internalname, "Identificador", "", "", lblTextblockrequisito_identificador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_115_MJ2( true) ;
         }
         else
         {
            wb_table13_115_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table13_115_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_tiporeqcod_Internalname, "Tipo", "", "", lblTextblockrequisito_tiporeqcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_163_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavRequisito_tiporeqcod, dynavRequisito_tiporeqcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0)), 1, dynavRequisito_tiporeqcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", "", true, "HLP_WP_SS.htm");
            dynavRequisito_tiporeqcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavRequisito_tiporeqcod_Internalname, "Values", (String)(dynavRequisito_tiporeqcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_titulo_Internalname, "Nome", "", "", lblTextblockrequisito_titulo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_163_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_titulo_Internalname, AV101Requisito_Titulo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, 1, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_descricao_Internalname, "Descri��o", "", "", lblTextblockrequisito_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_163_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_descricao_Internalname, AV99Requisito_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 2, 1, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_prioridade_Internalname, "Prioridade", "", "", lblTextblockrequisito_prioridade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_163_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRequisito_prioridade, cmbavRequisito_prioridade_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0)), 1, cmbavRequisito_prioridade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", "", true, "HLP_WP_SS.htm");
            cmbavRequisito_prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisito_Prioridade), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_prioridade_Internalname, "Values", (String)(cmbavRequisito_prioridade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_status_Internalname, "Situa��o", "", "", lblTextblockrequisito_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_163_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRequisito_status, cmbavRequisito_status_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0)), 1, cmbavRequisito_status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavRequisito_status.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", "", true, "HLP_WP_SS.htm");
            cmbavRequisito_status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV103Requisito_Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_status_Internalname, "Values", (String)(cmbavRequisito_status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_105_MJ2e( true) ;
         }
         else
         {
            wb_table9_105_MJ2e( false) ;
         }
      }

      protected void wb_table13_115_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedrequisito_identificador_Internalname, tblTablemergedrequisito_identificador_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisito_identificador_Internalname, AV100Requisito_Identificador, StringUtil.RTrim( context.localUtil.Format( AV100Requisito_Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisito_identificador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRequisito_identificador_Enabled, 1, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicadorautomatico_Internalname, "���(", "", "", lblTextblockindicadorautomatico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_163_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIndicadorautomatico_Internalname, StringUtil.BoolToStr( AV91IndicadorAutomatico), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(122, this, 'true', 'false');gx.ajax.executeCliEvent('e18mj2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblIndicadorautomatico_righttext_Internalname, "Autom�tico )", "", "", lblIndicadorautomatico_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_115_MJ2e( true) ;
         }
         else
         {
            wb_table13_115_MJ2e( false) ;
         }
      }

      protected void wb_table6_20_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable8_Internalname, tblUnnamedtable8_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoanom_Internalname, "Requisitante", "", "", lblTextblockusuario_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom_Internalname, StringUtil.RTrim( AV19Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV19Usuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavUsuario_pessoanom_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_cargouonom_Internalname, "Setor", "", "", lblTextblockusuario_cargouonom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargouonom_Internalname, StringUtil.RTrim( AV18Usuario_CargoUONom), StringUtil.RTrim( context.localUtil.Format( AV18Usuario_CargoUONom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargouonom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavUsuario_cargouonom_Enabled, 1, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdadosdass_contagemresultado_ss_Internalname, "N� Atendimento", "", "", lblTextblockdadosdass_contagemresultado_ss_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDadosdass_contagemresultado_ss_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV82DadosDaSS.gxTpr_Contagemresultado_ss), 8, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV82DadosDaSS.gxTpr_Contagemresultado_ss), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDadosdass_contagemresultado_ss_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavDadosdass_contagemresultado_ss_Visible, edtavDadosdass_contagemresultado_ss_Enabled, 1, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_codigo_Internalname, "Grupo", "", "", lblTextblockservicogrupo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_163_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServicogrupo_codigo, cmbavServicogrupo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0)), 1, cmbavServicogrupo_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICOGRUPO_CODIGO.CLICK."+"'", "int", "", cmbavServicogrupo_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_WP_SS.htm");
            cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ServicoGrupo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", (String)(cmbavServicogrupo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Servi�o", "", "", lblTextblockservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_163_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_codigo, cmbavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)), 1, cmbavServico_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICO_CODIGO.CLICK."+"'", "int", cmbavServico_codigo.TooltipText, cmbavServico_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WP_SS.htm");
            cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", (String)(cmbavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_codigo_Internalname, "Sistema", "", "", lblTextblocksistema_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_163_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_codigo, dynavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0)), 1, dynavSistema_codigo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e28mj1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WP_SS.htm");
            dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", (String)(dynavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdadosdass_contagemresultado_descricao_Internalname, "Assunto", "", "", lblTextblockdadosdass_contagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDadosdass_contagemresultado_descricao_Internalname, AV82DadosDaSS.gxTpr_Contagemresultado_descricao, StringUtil.RTrim( context.localUtil.Format( AV82DadosDaSS.gxTpr_Contagemresultado_descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDadosdass_contagemresultado_descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", edtavDadosdass_contagemresultado_descricao_Width, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"6\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdadosdass_contagemresultado_observacao_Internalname, "Descri��o", "", "", lblTextblockdadosdass_contagemresultado_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"6\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_163_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDadosdass_contagemresultado_observacao_Internalname, AV82DadosDaSS.gxTpr_Contagemresultado_observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", 0, 1, 1, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "20000", 1, "", "", -1, true, "", "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdadosdass_contagemresultado_referencia_Internalname, "Refer�ncia Externa", "", "", lblTextblockdadosdass_contagemresultado_referencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_163_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDadosdass_contagemresultado_referencia_Internalname, AV82DadosDaSS.gxTpr_Contagemresultado_referencia, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", 0, edtavDadosdass_contagemresultado_referencia_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdadosdass_contagemresultado_restricoes_Internalname, "Limita��o Geral", "", "", lblTextblockdadosdass_contagemresultado_restricoes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_163_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDadosdass_contagemresultado_restricoes_Internalname, AV82DadosDaSS.gxTpr_Contagemresultado_restricoes, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavDadosdass_contagemresultado_restricoes_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "250", 1, "", "", -1, true, "", "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdadosdass_contagemresultado_prioridadeprevista_Internalname, "Prioridade", "", "", lblTextblockdadosdass_contagemresultado_prioridadeprevista_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_163_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDadosdass_contagemresultado_prioridadeprevista_Internalname, AV82DadosDaSS.gxTpr_Contagemresultado_prioridadeprevista, StringUtil.RTrim( context.localUtil.Format( AV82DadosDaSS.gxTpr_Contagemresultado_prioridadeprevista, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDadosdass_contagemresultado_prioridadeprevista_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavDadosdass_contagemresultado_prioridadeprevista_Visible, 1, 0, "text", "", edtavDadosdass_contagemresultado_prioridadeprevista_Width, "%", 1, "row", 250, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdataprevista_Internalname, "Limita��o de Data", "", "", lblTextblockdataprevista_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_163_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDataprevista_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDataprevista_Internalname, context.localUtil.TToC( AV83DataPrevista, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV83DataPrevista, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDataprevista_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", edtavDataprevista_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SS.htm");
            GxWebStd.gx_bitmap( context, edtavDataprevista_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavDataprevista_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_SS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_groupanexos_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedgroupcontainergroupanexos_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup9_Internalname, "Anexos", 1, 0, "px", 0, "px", "Group", "", "HLP_WP_SS.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGroupanexos_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table14_96_MJ2( true) ;
         }
         else
         {
            wb_table14_96_MJ2( false) ;
         }
         return  ;
      }

      protected void wb_table14_96_MJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</fieldset>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_20_MJ2e( true) ;
         }
         else
         {
            wb_table6_20_MJ2e( false) ;
         }
      }

      protected void wb_table14_96_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableline7_Internalname, tblTableline7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0099"+"", StringUtil.RTrim( WebComp_Wcwc_contagemresultadoevidencias_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0099"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcwc_contagemresultadoevidencias_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcwc_contagemresultadoevidencias), StringUtil.Lower( WebComp_Wcwc_contagemresultadoevidencias_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0099"+"");
                  }
                  WebComp_Wcwc_contagemresultadoevidencias.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcwc_contagemresultadoevidencias), StringUtil.Lower( WebComp_Wcwc_contagemresultadoevidencias_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_96_MJ2e( true) ;
         }
         else
         {
            wb_table14_96_MJ2e( false) ;
         }
      }

      protected void wb_table2_5_MJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletitle_Internalname, tblTabletitle_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_MJ2e( true) ;
         }
         else
         {
            wb_table2_5_MJ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMJ2( ) ;
         WSMJ2( ) ;
         WEMJ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Wcwc_contagemresultadoevidencias == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcwc_contagemresultadoevidencias_Component) != 0 )
            {
               WebComp_Wcwc_contagemresultadoevidencias.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122123572");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_ss.js", "?20203122123572");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1632( )
      {
         edtavBtnalterarreqneg_Internalname = "vBTNALTERARREQNEG_"+sGXsfl_163_idx;
         edtavBtnexcluirreqneg_Internalname = "vBTNEXCLUIRREQNEG_"+sGXsfl_163_idx;
         edtavSdt_requisitos__requisito_codigo_Internalname = "SDT_REQUISITOS__REQUISITO_CODIGO_"+sGXsfl_163_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_163_idx;
         edtavSdt_requisitos__requisito_identificador_Internalname = "SDT_REQUISITOS__REQUISITO_IDENTIFICADOR_"+sGXsfl_163_idx;
         edtavSdt_requisitos__requisito_titulo_Internalname = "SDT_REQUISITOS__REQUISITO_TITULO_"+sGXsfl_163_idx;
         edtavSdt_requisitos__requisito_descricao_Internalname = "SDT_REQUISITOS__REQUISITO_DESCRICAO_"+sGXsfl_163_idx;
         cmbavSdt_requisitos__requisito_prioridade_Internalname = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_"+sGXsfl_163_idx;
         cmbavSdt_requisitos__requisito_status_Internalname = "SDT_REQUISITOS__REQUISITO_STATUS_"+sGXsfl_163_idx;
      }

      protected void SubsflControlProps_fel_1632( )
      {
         edtavBtnalterarreqneg_Internalname = "vBTNALTERARREQNEG_"+sGXsfl_163_fel_idx;
         edtavBtnexcluirreqneg_Internalname = "vBTNEXCLUIRREQNEG_"+sGXsfl_163_fel_idx;
         edtavSdt_requisitos__requisito_codigo_Internalname = "SDT_REQUISITOS__REQUISITO_CODIGO_"+sGXsfl_163_fel_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_163_fel_idx;
         edtavSdt_requisitos__requisito_identificador_Internalname = "SDT_REQUISITOS__REQUISITO_IDENTIFICADOR_"+sGXsfl_163_fel_idx;
         edtavSdt_requisitos__requisito_titulo_Internalname = "SDT_REQUISITOS__REQUISITO_TITULO_"+sGXsfl_163_fel_idx;
         edtavSdt_requisitos__requisito_descricao_Internalname = "SDT_REQUISITOS__REQUISITO_DESCRICAO_"+sGXsfl_163_fel_idx;
         cmbavSdt_requisitos__requisito_prioridade_Internalname = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_"+sGXsfl_163_fel_idx;
         cmbavSdt_requisitos__requisito_status_Internalname = "SDT_REQUISITOS__REQUISITO_STATUS_"+sGXsfl_163_fel_idx;
      }

      protected void sendrow_1632( )
      {
         SubsflControlProps_1632( ) ;
         WBMJ0( ) ;
         if ( ( subGridsdt_requisitos_Rows * 1 == 0 ) || ( nGXsfl_163_idx <= subGridsdt_requisitos_Recordsperpage( ) * 1 ) )
         {
            Gridsdt_requisitosRow = GXWebRow.GetNew(context,Gridsdt_requisitosContainer);
            if ( subGridsdt_requisitos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridsdt_requisitos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridsdt_requisitos_Class, "") != 0 )
               {
                  subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"Odd";
               }
            }
            else if ( subGridsdt_requisitos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridsdt_requisitos_Backstyle = 0;
               subGridsdt_requisitos_Backcolor = subGridsdt_requisitos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridsdt_requisitos_Class, "") != 0 )
               {
                  subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"Uniform";
               }
            }
            else if ( subGridsdt_requisitos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridsdt_requisitos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridsdt_requisitos_Class, "") != 0 )
               {
                  subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"Odd";
               }
               subGridsdt_requisitos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridsdt_requisitos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridsdt_requisitos_Backstyle = 1;
               if ( ((int)((nGXsfl_163_idx) % (2))) == 0 )
               {
                  subGridsdt_requisitos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridsdt_requisitos_Class, "") != 0 )
                  {
                     subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"Even";
                  }
               }
               else
               {
                  subGridsdt_requisitos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridsdt_requisitos_Class, "") != 0 )
                  {
                     subGridsdt_requisitos_Linesclass = subGridsdt_requisitos_Class+"Odd";
                  }
               }
            }
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridsdt_requisitos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_163_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtnalterarreqneg_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnalterarreqneg_Enabled!=0)&&(edtavBtnalterarreqneg_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 164,'',false,'',163)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV62btnAlterarReqNeg_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV62btnAlterarReqNeg))&&String.IsNullOrEmpty(StringUtil.RTrim( AV133Btnalterarreqneg_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV62btnAlterarReqNeg)));
            Gridsdt_requisitosRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnalterarreqneg_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV62btnAlterarReqNeg)) ? AV133Btnalterarreqneg_GXI : context.PathToRelativeUrl( AV62btnAlterarReqNeg)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavBtnalterarreqneg_Visible,(short)1,(String)"",(String)edtavBtnalterarreqneg_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnalterarreqneg_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNALTERARREQNEG.CLICK."+sGXsfl_163_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV62btnAlterarReqNeg_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtnexcluirreqneg_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnexcluirreqneg_Enabled!=0)&&(edtavBtnexcluirreqneg_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 165,'',false,'',163)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV44btnExcluirReqNeg_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV44btnExcluirReqNeg))&&String.IsNullOrEmpty(StringUtil.RTrim( AV134Btnexcluirreqneg_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV44btnExcluirReqNeg)));
            Gridsdt_requisitosRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnexcluirreqneg_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV44btnExcluirReqNeg)) ? AV134Btnexcluirreqneg_GXI : context.PathToRelativeUrl( AV44btnExcluirReqNeg)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavBtnexcluirreqneg_Visible,(short)1,(String)"",(String)edtavBtnexcluirreqneg_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnexcluirreqneg_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNEXCLUIRREQNEG.CLICK."+sGXsfl_163_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV44btnExcluirReqNeg_IsBlob,(bool)false});
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            Gridsdt_requisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSdt_requisitos__requisito_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_codigo), 6, 0, ",", "")),((edtavSdt_requisitos__requisito_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSdt_requisitos__requisito_codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavSdt_requisitos__requisito_codigo_Enabled,(short)0,(String)"text",(String)"",(short)100,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)163,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 167,'',false,'"+sGXsfl_163_idx+"',163)\"" : " ");
            ROClassString = "BootstrapAttribute";
            Gridsdt_requisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAgrupador_Internalname,(String)AV64Agrupador,(String)"",TempTags+((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,167);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAgrupador_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavAgrupador_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavAgrupador_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)163,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            Gridsdt_requisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSdt_requisitos__requisito_identificador_Internalname,((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_identificador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSdt_requisitos__requisito_identificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)((edtavSdt_requisitos__requisito_identificador_Fontstrikethru==1) ? "text-decoration:line-through;" : ""),(String)ROClassString,(String)"",(short)-1,(int)edtavSdt_requisitos__requisito_identificador_Enabled,(short)0,(String)"text",(String)"",(short)100,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)163,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            Gridsdt_requisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSdt_requisitos__requisito_titulo_Internalname,((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_titulo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSdt_requisitos__requisito_titulo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)((edtavSdt_requisitos__requisito_titulo_Fontstrikethru==1) ? "text-decoration:line-through;" : ""),(String)ROClassString,(String)"",(short)-1,(int)edtavSdt_requisitos__requisito_titulo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)250,(short)0,(short)0,(short)163,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            Gridsdt_requisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSdt_requisitos__requisito_descricao_Internalname,((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_descricao,((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSdt_requisitos__requisito_descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavSdt_requisitos__requisito_descricao_Enabled,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)2,(short)163,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_163_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_" + sGXsfl_163_idx;
               cmbavSdt_requisitos__requisito_prioridade.Name = GXCCtl;
               cmbavSdt_requisitos__requisito_prioridade.WebTags = "";
               cmbavSdt_requisitos__requisito_prioridade.addItem("1", "Alta", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("2", "Alta M�dia", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("3", "Alta Baixa", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("4", "M�dia Alta", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("5", "M�dia M�dia", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("6", "M�dia Baixa", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("7", "Baixa Alta", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("8", "Baixa M�dia", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("9", "Baixa Baixa", 0);
               if ( cmbavSdt_requisitos__requisito_prioridade.ItemCount > 0 )
               {
                  if ( ( AV124GXV7 > 0 ) && ( AV106SDT_Requisitos.Count >= AV124GXV7 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade) )
                  {
                     ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade), 2, 0))), "."));
                  }
               }
            }
            /* ComboBox */
            Gridsdt_requisitosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavSdt_requisitos__requisito_prioridade,(String)cmbavSdt_requisitos__requisito_prioridade_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade), 2, 0)),(short)1,(String)cmbavSdt_requisitos__requisito_prioridade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavSdt_requisitos__requisito_prioridade.Enabled,(short)0,(short)0,(short)150,(String)"px",(short)0,(String)"px",((cmbavSdt_requisitos__requisito_prioridade.FontStrikethru==1) ? "text-decoration:line-through;" : ""),(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavSdt_requisitos__requisito_prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_prioridade), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_prioridade_Internalname, "Values", (String)(cmbavSdt_requisitos__requisito_prioridade.ToJavascriptSource()));
            /* Subfile cell */
            if ( Gridsdt_requisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_163_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SDT_REQUISITOS__REQUISITO_STATUS_" + sGXsfl_163_idx;
               cmbavSdt_requisitos__requisito_status.Name = GXCCtl;
               cmbavSdt_requisitos__requisito_status.WebTags = "";
               cmbavSdt_requisitos__requisito_status.addItem("0", "Rascunho", 0);
               cmbavSdt_requisitos__requisito_status.addItem("1", "Solicitado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("2", "Aprovado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("3", "N�o Aprovado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("4", "Pausado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("5", "Cancelado", 0);
               if ( cmbavSdt_requisitos__requisito_status.ItemCount > 0 )
               {
                  if ( ( AV124GXV7 > 0 ) && ( AV106SDT_Requisitos.Count >= AV124GXV7 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status) )
                  {
                     ((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status), 4, 0))), "."));
                  }
               }
            }
            /* ComboBox */
            Gridsdt_requisitosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavSdt_requisitos__requisito_status,(String)cmbavSdt_requisitos__requisito_status_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status), 4, 0)),(short)1,(String)cmbavSdt_requisitos__requisito_status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavSdt_requisitos__requisito_status.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",((cmbavSdt_requisitos__requisito_status.FontStrikethru==1) ? "text-decoration:line-through;" : ""),(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavSdt_requisitos__requisito_status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV106SDT_Requisitos.Item(AV124GXV7)).gxTpr_Requisito_status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_status_Internalname, "Values", (String)(cmbavSdt_requisitos__requisito_status.ToJavascriptSource()));
            Gridsdt_requisitosContainer.AddRow(Gridsdt_requisitosRow);
            nGXsfl_163_idx = (short)(((subGridsdt_requisitos_Islastpage==1)&&(nGXsfl_163_idx+1>subGridsdt_requisitos_Recordsperpage( )) ? 1 : nGXsfl_163_idx+1));
            sGXsfl_163_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_163_idx), 4, 0)), 4, "0");
            SubsflControlProps_1632( ) ;
         }
         /* End function sendrow_1632 */
      }

      protected void SubsflControlProps_1783( )
      {
         edtavBtncontinuar_Internalname = "vBTNCONTINUAR_"+sGXsfl_178_idx;
         edtavBtncancelar_Internalname = "vBTNCANCELAR_"+sGXsfl_178_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_178_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_178_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_178_idx;
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_178_idx;
         edtContagemResultado_DataEntrega_Internalname = "CONTAGEMRESULTADO_DATAENTREGA_"+sGXsfl_178_idx;
         edtContagemResultado_DataCadastro_Internalname = "CONTAGEMRESULTADO_DATACADASTRO_"+sGXsfl_178_idx;
         edtavQtdrequisitos_Internalname = "vQTDREQUISITOS_"+sGXsfl_178_idx;
         edtavQtdanexos_Internalname = "vQTDANEXOS_"+sGXsfl_178_idx;
      }

      protected void SubsflControlProps_fel_1783( )
      {
         edtavBtncontinuar_Internalname = "vBTNCONTINUAR_"+sGXsfl_178_fel_idx;
         edtavBtncancelar_Internalname = "vBTNCANCELAR_"+sGXsfl_178_fel_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_178_fel_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_178_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_178_fel_idx;
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_178_fel_idx;
         edtContagemResultado_DataEntrega_Internalname = "CONTAGEMRESULTADO_DATAENTREGA_"+sGXsfl_178_fel_idx;
         edtContagemResultado_DataCadastro_Internalname = "CONTAGEMRESULTADO_DATACADASTRO_"+sGXsfl_178_fel_idx;
         edtavQtdrequisitos_Internalname = "vQTDREQUISITOS_"+sGXsfl_178_fel_idx;
         edtavQtdanexos_Internalname = "vQTDANEXOS_"+sGXsfl_178_fel_idx;
      }

      protected void sendrow_1783( )
      {
         SubsflControlProps_1783( ) ;
         WBMJ0( ) ;
         if ( ( subGridrascunho_Rows * 1 == 0 ) || ( nGXsfl_178_idx <= subGridrascunho_Recordsperpage( ) * 1 ) )
         {
            GridrascunhoRow = GXWebRow.GetNew(context,GridrascunhoContainer);
            if ( subGridrascunho_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridrascunho_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridrascunho_Class, "") != 0 )
               {
                  subGridrascunho_Linesclass = subGridrascunho_Class+"Odd";
               }
            }
            else if ( subGridrascunho_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridrascunho_Backstyle = 0;
               subGridrascunho_Backcolor = subGridrascunho_Allbackcolor;
               if ( StringUtil.StrCmp(subGridrascunho_Class, "") != 0 )
               {
                  subGridrascunho_Linesclass = subGridrascunho_Class+"Uniform";
               }
            }
            else if ( subGridrascunho_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridrascunho_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridrascunho_Class, "") != 0 )
               {
                  subGridrascunho_Linesclass = subGridrascunho_Class+"Odd";
               }
               subGridrascunho_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridrascunho_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridrascunho_Backstyle = 1;
               if ( ((int)((nGXsfl_178_idx) % (2))) == 0 )
               {
                  subGridrascunho_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridrascunho_Class, "") != 0 )
                  {
                     subGridrascunho_Linesclass = subGridrascunho_Class+"Even";
                  }
               }
               else
               {
                  subGridrascunho_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridrascunho_Class, "") != 0 )
                  {
                     subGridrascunho_Linesclass = subGridrascunho_Class+"Odd";
                  }
               }
            }
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridrascunho_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_178_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtncontinuar_Enabled!=0)&&(edtavBtncontinuar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 179,'',false,'',178)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV75btnContinuar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV75btnContinuar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV131Btncontinuar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV75btnContinuar)));
            GridrascunhoRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtncontinuar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV75btnContinuar)) ? AV131Btncontinuar_GXI : context.PathToRelativeUrl( AV75btnContinuar)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtncontinuar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtncontinuar_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNCONTINUAR.CLICK."+sGXsfl_178_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV75btnContinuar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtncancelar_Enabled!=0)&&(edtavBtncancelar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 180,'',false,'',178)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV76btnCancelar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV76btnCancelar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV132Btncancelar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV76btnCancelar)));
            GridrascunhoRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtncancelar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV76btnCancelar)) ? AV132Btncancelar_GXI : context.PathToRelativeUrl( AV76btnCancelar)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtncancelar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavBtncancelar_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e29mj3_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV76btnCancelar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataDmn_Internalname,context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"),context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContagemResultado_DataDmn_Link,(String)edtContagemResultado_DataDmn_Linktarget,(String)"",(String)"",(String)edtContagemResultado_DataDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)63,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Descricao_Internalname,(String)A494ContagemResultado_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataEntrega_Internalname,context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"),context.localUtil.Format( A472ContagemResultado_DataEntrega, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataEntrega_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataCadastro_Internalname,context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1350ContagemResultado_DataCadastro, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataCadastro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavQtdrequisitos_Enabled!=0)&&(edtavQtdrequisitos_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 187,'',false,'"+sGXsfl_178_idx+"',178)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdrequisitos_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77QtdRequisitos), 4, 0, ",", "")),((edtavQtdrequisitos_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77QtdRequisitos), "ZZZ9")) : context.localUtil.Format( (decimal)(AV77QtdRequisitos), "ZZZ9")),TempTags+((edtavQtdrequisitos_Enabled!=0)&&(edtavQtdrequisitos_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavQtdrequisitos_Enabled!=0)&&(edtavQtdrequisitos_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,187);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtdrequisitos_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQtdrequisitos_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridrascunhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavQtdanexos_Enabled!=0)&&(edtavQtdanexos_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 188,'',false,'"+sGXsfl_178_idx+"',178)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridrascunhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdanexos_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86QtdAnexos), 4, 0, ",", "")),((edtavQtdanexos_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV86QtdAnexos), "ZZZ9")) : context.localUtil.Format( (decimal)(AV86QtdAnexos), "ZZZ9")),TempTags+((edtavQtdanexos_Enabled!=0)&&(edtavQtdanexos_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavQtdanexos_Enabled!=0)&&(edtavQtdanexos_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,188);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtdanexos_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQtdanexos_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)178,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridrascunhoContainer.AddRow(GridrascunhoRow);
            nGXsfl_178_idx = (short)(((subGridrascunho_Islastpage==1)&&(nGXsfl_178_idx+1>subGridrascunho_Recordsperpage( )) ? 1 : nGXsfl_178_idx+1));
            sGXsfl_178_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_178_idx), 4, 0)), 4, "0");
            SubsflControlProps_1783( ) ;
         }
         /* End function sendrow_1783 */
      }

      protected void init_default_properties( )
      {
         tblTabletitle_Internalname = "TABLETITLE";
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblTextblockusuario_pessoanom_Internalname = "TEXTBLOCKUSUARIO_PESSOANOM";
         edtavUsuario_pessoanom_Internalname = "vUSUARIO_PESSOANOM";
         lblTextblockusuario_cargouonom_Internalname = "TEXTBLOCKUSUARIO_CARGOUONOM";
         edtavUsuario_cargouonom_Internalname = "vUSUARIO_CARGOUONOM";
         lblTextblockdadosdass_contagemresultado_ss_Internalname = "TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_SS";
         edtavDadosdass_contagemresultado_ss_Internalname = "DADOSDASS_CONTAGEMRESULTADO_SS";
         lblTextblockservicogrupo_codigo_Internalname = "TEXTBLOCKSERVICOGRUPO_CODIGO";
         cmbavServicogrupo_codigo_Internalname = "vSERVICOGRUPO_CODIGO";
         lblTextblockservico_codigo_Internalname = "TEXTBLOCKSERVICO_CODIGO";
         cmbavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblocksistema_codigo_Internalname = "TEXTBLOCKSISTEMA_CODIGO";
         dynavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         lblTextblockdadosdass_contagemresultado_descricao_Internalname = "TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_DESCRICAO";
         edtavDadosdass_contagemresultado_descricao_Internalname = "DADOSDASS_CONTAGEMRESULTADO_DESCRICAO";
         lblTextblockdadosdass_contagemresultado_observacao_Internalname = "TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_OBSERVACAO";
         edtavDadosdass_contagemresultado_observacao_Internalname = "DADOSDASS_CONTAGEMRESULTADO_OBSERVACAO";
         lblTextblockdadosdass_contagemresultado_referencia_Internalname = "TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_REFERENCIA";
         edtavDadosdass_contagemresultado_referencia_Internalname = "DADOSDASS_CONTAGEMRESULTADO_REFERENCIA";
         lblTextblockdadosdass_contagemresultado_restricoes_Internalname = "TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_RESTRICOES";
         edtavDadosdass_contagemresultado_restricoes_Internalname = "DADOSDASS_CONTAGEMRESULTADO_RESTRICOES";
         lblTextblockdadosdass_contagemresultado_prioridadeprevista_Internalname = "TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA";
         edtavDadosdass_contagemresultado_prioridadeprevista_Internalname = "DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA";
         lblTextblockdataprevista_Internalname = "TEXTBLOCKDATAPREVISTA";
         edtavDataprevista_Internalname = "vDATAPREVISTA";
         tblTableline7_Internalname = "TABLELINE7";
         div_Internalname = "";
         div_Internalname = "";
         divGroupanexos_Internalname = "GROUPANEXOS";
         grpUnnamedgroup9_Internalname = "UNNAMEDGROUP9";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedgroupcontainergroupanexos_Internalname = "UNNAMEDGROUPCONTAINERGROUPANEXOS";
         div_Internalname = "";
         divLayout_groupanexos_Internalname = "LAYOUT_GROUPANEXOS";
         tblUnnamedtable8_Internalname = "UNNAMEDTABLE8";
         lblTextblockrequisito_agrupador_Internalname = "TEXTBLOCKREQUISITO_AGRUPADOR";
         edtavRequisito_agrupador_Internalname = "vREQUISITO_AGRUPADOR";
         lblTextblockrequisito_identificador_Internalname = "TEXTBLOCKREQUISITO_IDENTIFICADOR";
         edtavRequisito_identificador_Internalname = "vREQUISITO_IDENTIFICADOR";
         lblTextblockindicadorautomatico_Internalname = "TEXTBLOCKINDICADORAUTOMATICO";
         chkavIndicadorautomatico_Internalname = "vINDICADORAUTOMATICO";
         lblIndicadorautomatico_righttext_Internalname = "INDICADORAUTOMATICO_RIGHTTEXT";
         tblTablemergedrequisito_identificador_Internalname = "TABLEMERGEDREQUISITO_IDENTIFICADOR";
         lblTextblockrequisito_tiporeqcod_Internalname = "TEXTBLOCKREQUISITO_TIPOREQCOD";
         dynavRequisito_tiporeqcod_Internalname = "vREQUISITO_TIPOREQCOD";
         lblTextblockrequisito_titulo_Internalname = "TEXTBLOCKREQUISITO_TITULO";
         edtavRequisito_titulo_Internalname = "vREQUISITO_TITULO";
         lblTextblockrequisito_descricao_Internalname = "TEXTBLOCKREQUISITO_DESCRICAO";
         edtavRequisito_descricao_Internalname = "vREQUISITO_DESCRICAO";
         lblTextblockrequisito_prioridade_Internalname = "TEXTBLOCKREQUISITO_PRIORIDADE";
         cmbavRequisito_prioridade_Internalname = "vREQUISITO_PRIORIDADE";
         lblTextblockrequisito_status_Internalname = "TEXTBLOCKREQUISITO_STATUS";
         cmbavRequisito_status_Internalname = "vREQUISITO_STATUS";
         tblUnnamedtable5_Internalname = "UNNAMEDTABLE5";
         bttBtnbtnincluirreqneg_Internalname = "BTNBTNINCLUIRREQNEG";
         tblUnnamedtable6_Internalname = "UNNAMEDTABLE6";
         edtavBtnalterarreqneg_Internalname = "vBTNALTERARREQNEG";
         edtavBtnexcluirreqneg_Internalname = "vBTNEXCLUIRREQNEG";
         edtavSdt_requisitos__requisito_codigo_Internalname = "SDT_REQUISITOS__REQUISITO_CODIGO";
         edtavAgrupador_Internalname = "vAGRUPADOR";
         edtavSdt_requisitos__requisito_identificador_Internalname = "SDT_REQUISITOS__REQUISITO_IDENTIFICADOR";
         edtavSdt_requisitos__requisito_titulo_Internalname = "SDT_REQUISITOS__REQUISITO_TITULO";
         edtavSdt_requisitos__requisito_descricao_Internalname = "SDT_REQUISITOS__REQUISITO_DESCRICAO";
         cmbavSdt_requisitos__requisito_prioridade_Internalname = "SDT_REQUISITOS__REQUISITO_PRIORIDADE";
         cmbavSdt_requisitos__requisito_status_Internalname = "SDT_REQUISITOS__REQUISITO_STATUS";
         tblRequisitos_Internalname = "REQUISITOS";
         Dvpanel_requisitos_Internalname = "DVPANEL_REQUISITOS";
         tblUnnamedtable7_Internalname = "UNNAMEDTABLE7";
         tblUnnamedtable4_Internalname = "UNNAMEDTABLE4";
         edtavBtncontinuar_Internalname = "vBTNCONTINUAR";
         edtavBtncancelar_Internalname = "vBTNCANCELAR";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO";
         edtContagemResultado_DataEntrega_Internalname = "CONTAGEMRESULTADO_DATAENTREGA";
         edtContagemResultado_DataCadastro_Internalname = "CONTAGEMRESULTADO_DATACADASTRO";
         edtavQtdrequisitos_Internalname = "vQTDREQUISITOS";
         edtavQtdanexos_Internalname = "vQTDANEXOS";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         Gxuitabspanel_tabs_Internalname = "GXUITABSPANEL_TABS";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         bttBtnsalvar_Internalname = "BTNSALVAR";
         tblTableactions_Internalname = "TABLEACTIONS";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         edtavServico_responsavel_Internalname = "vSERVICO_RESPONSAVEL";
         chkavTemservicopadrao_Internalname = "vTEMSERVICOPADRAO";
         lblTbjava_Internalname = "TBJAVA";
         tblUsrtable_Internalname = "USRTABLE";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         edtavLinhanegocio_codigo_Internalname = "vLINHANEGOCIO_CODIGO";
         edtavRequisitoindex_Internalname = "vREQUISITOINDEX";
         Form.Internalname = "FORM";
         subGridsdt_requisitos_Internalname = "GRIDSDT_REQUISITOS";
         subGridrascunho_Internalname = "GRIDRASCUNHO";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavQtdanexos_Jsonclick = "";
         edtavQtdanexos_Visible = -1;
         edtavQtdrequisitos_Jsonclick = "";
         edtavQtdrequisitos_Visible = -1;
         edtContagemResultado_DataCadastro_Jsonclick = "";
         edtContagemResultado_DataEntrega_Jsonclick = "";
         edtContagemResultado_Descricao_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_DataDmn_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtavBtncancelar_Jsonclick = "";
         edtavBtncancelar_Visible = -1;
         edtavBtncancelar_Enabled = 1;
         edtavBtncontinuar_Jsonclick = "";
         edtavBtncontinuar_Visible = -1;
         edtavBtncontinuar_Enabled = 1;
         cmbavSdt_requisitos__requisito_status_Jsonclick = "";
         cmbavSdt_requisitos__requisito_prioridade_Jsonclick = "";
         edtavSdt_requisitos__requisito_descricao_Jsonclick = "";
         edtavSdt_requisitos__requisito_titulo_Jsonclick = "";
         edtavSdt_requisitos__requisito_identificador_Jsonclick = "";
         edtavAgrupador_Jsonclick = "";
         edtavAgrupador_Visible = -1;
         edtavSdt_requisitos__requisito_codigo_Jsonclick = "";
         edtavBtnexcluirreqneg_Jsonclick = "";
         edtavBtnexcluirreqneg_Enabled = 1;
         edtavBtnalterarreqneg_Jsonclick = "";
         edtavBtnalterarreqneg_Enabled = 1;
         edtavDataprevista_Jsonclick = "";
         edtavDadosdass_contagemresultado_prioridadeprevista_Jsonclick = "";
         edtavDadosdass_contagemresultado_prioridadeprevista_Width = 100;
         edtavDadosdass_contagemresultado_prioridadeprevista_Visible = 1;
         edtavDadosdass_contagemresultado_restricoes_Visible = 1;
         edtavDadosdass_contagemresultado_referencia_Visible = 1;
         edtavDadosdass_contagemresultado_descricao_Jsonclick = "";
         edtavDadosdass_contagemresultado_descricao_Width = 100;
         dynavSistema_codigo_Jsonclick = "";
         cmbavServico_codigo_Jsonclick = "";
         cmbavServicogrupo_codigo_Jsonclick = "";
         edtavDadosdass_contagemresultado_ss_Jsonclick = "";
         edtavDadosdass_contagemresultado_ss_Enabled = 1;
         edtavDadosdass_contagemresultado_ss_Visible = 1;
         edtavUsuario_cargouonom_Jsonclick = "";
         edtavUsuario_pessoanom_Jsonclick = "";
         edtavRequisito_identificador_Jsonclick = "";
         cmbavRequisito_status_Jsonclick = "";
         cmbavRequisito_status.Enabled = 1;
         cmbavRequisito_prioridade_Jsonclick = "";
         dynavRequisito_tiporeqcod_Jsonclick = "";
         edtavRequisito_agrupador_Jsonclick = "";
         subGridsdt_requisitos_Allowcollapsing = 0;
         subGridsdt_requisitos_Allowselection = 0;
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
         cmbavSdt_requisitos__requisito_status.FontStrikethru = 0;
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         cmbavSdt_requisitos__requisito_prioridade.FontStrikethru = 0;
         edtavSdt_requisitos__requisito_descricao_Enabled = 0;
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         edtavSdt_requisitos__requisito_titulo_Fontstrikethru = 0;
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         edtavSdt_requisitos__requisito_identificador_Fontstrikethru = 0;
         edtavAgrupador_Enabled = 1;
         edtavAgrupador_Forecolor = (int)(0xFFFFFF);
         edtavSdt_requisitos__requisito_codigo_Enabled = 0;
         edtavBtnexcluirreqneg_Tooltiptext = "";
         edtavBtnalterarreqneg_Tooltiptext = "";
         edtavBtnexcluirreqneg_Visible = -1;
         edtavBtnalterarreqneg_Visible = -1;
         subGridsdt_requisitos_Class = "WorkWithBorder WorkWith";
         subGridrascunho_Allowcollapsing = 0;
         subGridrascunho_Allowselection = 0;
         edtavQtdanexos_Enabled = 1;
         edtavQtdrequisitos_Enabled = 1;
         edtContagemResultado_DataDmn_Link = "";
         edtavBtncancelar_Tooltiptext = "Cancelar solicita��o";
         edtavBtncontinuar_Tooltiptext = "Continuar cadastro";
         subGridrascunho_Class = "WorkWithBorder WorkWith";
         lblTextblocktitle_Visible = 1;
         bttBtnsalvar_Visible = 1;
         bttBtnenter_Enabled = 1;
         tblTableactions_Visible = 1;
         lblTbjava_Visible = 1;
         edtavServico_responsavel_Jsonclick = "";
         cmbavServico_codigo.Visible = 1;
         cmbavServicogrupo_codigo.Visible = 1;
         edtavDataprevista_Visible = 1;
         edtavRequisito_identificador_Enabled = 1;
         cmbavServico_codigo.TooltipText = "";
         edtavUsuario_cargouonom_Enabled = 1;
         edtavUsuario_pessoanom_Enabled = 1;
         edtavDadosdass_contagemresultado_ss_Enabled = 1;
         bttBtnenter_Tooltiptext = "Solicitar servi�o";
         bttBtnenter_Caption = "Solicitar";
         bttBtnbtnincluirreqneg_Caption = "Incluir Requisito";
         lblTbjava_Caption = "tbJava";
         edtavDadosdass_contagemresultado_prioridadeprevista_Width = 100;
         edtavDadosdass_contagemresultado_descricao_Width = 100;
         chkavTemservicopadrao.Visible = 1;
         edtavServico_responsavel_Visible = 1;
         subGridrascunho_Backcolorstyle = 3;
         subGridsdt_requisitos_Backcolorstyle = 3;
         cmbavSdt_requisitos__requisito_status.Enabled = -1;
         cmbavSdt_requisitos__requisito_prioridade.Enabled = -1;
         edtavSdt_requisitos__requisito_descricao_Enabled = -1;
         edtavSdt_requisitos__requisito_titulo_Enabled = -1;
         edtavSdt_requisitos__requisito_identificador_Enabled = -1;
         edtavSdt_requisitos__requisito_codigo_Enabled = -1;
         chkavTemservicopadrao.Caption = "";
         chkavIndicadorautomatico.Caption = "";
         edtavRequisitoindex_Jsonclick = "";
         edtavRequisitoindex_Visible = 1;
         edtavLinhanegocio_codigo_Jsonclick = "";
         edtavLinhanegocio_codigo_Visible = 1;
         cmbavServico_codigo.Description = "";
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Yesbuttonposition = "left";
         Confirmpanel_Yesbuttoncaption = "Fechar";
         Confirmpanel_Confirmationtext = "";
         Confirmpanel_Title = "Confirma��o";
         Gxuitabspanel_tabs_Designtimetabs = "[{\"id\":\"TabSolicitacao\"},{\"id\":\"TabRequisitos\"},{\"id\":\"TabRascunho\"}]";
         Gxuitabspanel_tabs_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabs_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tabs_Width = "100%";
         Dvpanel_requisitos_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_requisitos_Iconposition = "left";
         Dvpanel_requisitos_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_requisitos_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_requisitos_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_requisitos_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_requisitos_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_requisitos_Title = "";
         Dvpanel_requisitos_Cls = "GXUI-DVelop-Panel";
         Dvpanel_requisitos_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Solicita��o de Servi�o";
         subGridrascunho_Rows = 0;
         edtContagemResultado_DataDmn_Linktarget = "";
         subGridsdt_requisitos_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Linhanegocio_codigo( int GX_Parm1 ,
                                              GXCombobox dynGX_Parm2 )
      {
         AV114LinhaNegocio_Codigo = GX_Parm1;
         dynavRequisito_tiporeqcod = dynGX_Parm2;
         AV115Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.CurrentValue, "."));
         GXVvREQUISITO_TIPOREQCOD_htmlMJ2( AV114LinhaNegocio_Codigo) ;
         dynload_actions( ) ;
         if ( dynavRequisito_tiporeqcod.ItemCount > 0 )
         {
            AV115Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0))), "."));
         }
         dynavRequisito_tiporeqcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV115Requisito_TipoReqCod), 6, 0));
         isValidOutput.Add(dynavRequisito_tiporeqcod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'edtContagemResultado_DataDmn_Linktarget',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDRASCUNHO.LOAD","{handler:'E25MJ3',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV75btnContinuar',fld:'vBTNCONTINUAR',pic:'',nv:''},{av:'edtavBtncontinuar_Tooltiptext',ctrl:'vBTNCONTINUAR',prop:'Tooltiptext'},{av:'AV76btnCancelar',fld:'vBTNCANCELAR',pic:'',nv:''},{av:'edtavBtncancelar_Tooltiptext',ctrl:'vBTNCANCELAR',prop:'Tooltiptext'},{av:'edtContagemResultado_DataDmn_Link',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Link'},{av:'AV77QtdRequisitos',fld:'vQTDREQUISITOS',pic:'ZZZ9',nv:0},{av:'AV86QtdAnexos',fld:'vQTDANEXOS',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDSDT_REQUISITOS.LOAD","{handler:'E20MJ2',iparms:[{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],oparms:[{av:'AV62btnAlterarReqNeg',fld:'vBTNALTERARREQNEG',pic:'',nv:''},{av:'edtavBtnalterarreqneg_Tooltiptext',ctrl:'vBTNALTERARREQNEG',prop:'Tooltiptext'},{av:'AV44btnExcluirReqNeg',fld:'vBTNEXCLUIRREQNEG',pic:'',nv:''},{av:'edtavBtnexcluirreqneg_Tooltiptext',ctrl:'vBTNEXCLUIRREQNEG',prop:'Tooltiptext'},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'edtavAgrupador_Forecolor',ctrl:'vAGRUPADOR',prop:'Forecolor'},{av:'edtavBtnalterarreqneg_Visible',ctrl:'vBTNALTERARREQNEG',prop:'Visible'},{av:'edtavBtnexcluirreqneg_Visible',ctrl:'vBTNEXCLUIRREQNEG',prop:'Visible'},{ctrl:'SDT_REQUISITOS__REQUISITO_IDENTIFICADOR',prop:'Fontstrikethru'},{ctrl:'SDT_REQUISITOS__REQUISITO_TITULO',prop:'Fontstrikethru'},{ctrl:'SDT_REQUISITOS__REQUISITO_PRIORIDADE',prop:'Fontstrikethru'},{ctrl:'SDT_REQUISITOS__REQUISITO_STATUS',prop:'Fontstrikethru'}]}");
         setEventMetadata("ENTER","{handler:'E12MJ2',iparms:[{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV113ContagemResultadoRequisito',fld:'vCONTAGEMRESULTADOREQUISITO',pic:'',nv:null},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2005ContagemResultadoRequisito_Codigo',fld:'CONTAGEMRESULTADOREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV27ContratanteSemEmailSda',fld:'vCONTRATANTESEMEMAILSDA',pic:'',nv:false},{av:'cmbavServico_codigo'},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV93Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV20Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV23Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV25Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],oparms:[{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV98Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV113ContagemResultadoRequisito',fld:'vCONTAGEMRESULTADOREQUISITO',pic:'',nv:null},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV93Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV25Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'Confirmpanel_Confirmationtext',ctrl:'CONFIRMPANEL',prop:'ConfirmationText'},{av:'AV20Usuarios',fld:'vUSUARIOS',pic:'',nv:null}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E13MJ2',iparms:[],oparms:[]}");
         setEventMetadata("'DOSALVAR'","{handler:'E14MJ2',iparms:[{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2005ContagemResultadoRequisito_Codigo',fld:'CONTAGEMRESULTADOREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'Confirmpanel_Confirmationtext',ctrl:'CONFIRMPANEL',prop:'ConfirmationText'},{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV98Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV113ContagemResultadoRequisito',fld:'vCONTAGEMRESULTADOREQUISITO',pic:'',nv:null},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null}]}");
         setEventMetadata("'DOBTNINCLUIRREQNEG'","{handler:'E15MJ2',iparms:[{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV101Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV97Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{av:'AV102Requisito_Prioridade',fld:'vREQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'AV104Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV103Requisito_Status',fld:'vREQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'AV91IndicadorAutomatico',fld:'vINDICADORAUTOMATICO',pic:'',nv:false},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV63RequisitoIndex',fld:'vREQUISITOINDEX',pic:'ZZZ9',nv:0},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{av:'Dvpanel_requisitos_Title',ctrl:'DVPANEL_REQUISITOS',prop:'Title'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{ctrl:'BTNBTNINCLUIRREQNEG',prop:'Caption'},{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV101Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV102Requisito_Prioridade',fld:'vREQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'AV104Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV103Requisito_Status',fld:'vREQUISITO_STATUS',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDRASCUNHO.REFRESH","{handler:'E26MJ2',iparms:[],oparms:[]}");
         setEventMetadata("GRIDSDT_REQUISITOS.REFRESH","{handler:'E21MJ2',iparms:[],oparms:[{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}]}");
         setEventMetadata("VSERVICOGRUPO_CODIGO.CLICK","{handler:'E16MJ2',iparms:[{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSERVICO_CODIGO.CLICK","{handler:'E17MJ2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'cmbavServico_codigo'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNENTER',prop:'Enabled'}]}");
         setEventMetadata("VSISTEMA_CODIGO.CLICK","{handler:'E28MJ1',iparms:[{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNENTER',prop:'Enabled'}]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11MJ2',iparms:[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("VBTNEXCLUIRREQNEG.CLICK","{handler:'E23MJ2',iparms:[{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],oparms:[{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VBTNALTERARREQNEG.CLICK","{handler:'E24MJ2',iparms:[{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],oparms:[{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV101Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV97Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV102Requisito_Prioridade',fld:'vREQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'AV103Requisito_Status',fld:'vREQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'AV104Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{ctrl:'BTNBTNINCLUIRREQNEG',prop:'Caption'}]}");
         setEventMetadata("VBTNCONTINUAR.CLICK","{handler:'E27MJ2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A2002Requisito_Prioridade',fld:'REQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A591ContagemResultadoEvidencia_Data',fld:'CONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A1393ContagemResultadoEvidencia_RdmnCreated',fld:'CONTAGEMRESULTADOEVIDENCIA_RDMNCREATED',pic:'99/99/99 99:99',nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],oparms:[{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV97Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''}]}");
         setEventMetadata("VBTNCANCELAR.CLICK","{handler:'E29MJ3',iparms:[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultado_DataDmn_Linktarget',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("VINDICADORAUTOMATICO.CLICK","{handler:'E18MJ2',iparms:[{av:'AV91IndicadorAutomatico',fld:'vINDICADORAUTOMATICO',pic:'',nv:false},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0}],oparms:[{av:'edtavRequisito_identificador_Enabled',ctrl:'vREQUISITO_IDENTIFICADOR',prop:'Enabled'},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''}]}");
         setEventMetadata("GRIDSDT_REQUISITOS_FIRSTPAGE","{handler:'subgridsdt_requisitos_firstpage',iparms:[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDSDT_REQUISITOS_PREVPAGE","{handler:'subgridsdt_requisitos_previouspage',iparms:[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDSDT_REQUISITOS_NEXTPAGE","{handler:'subgridsdt_requisitos_nextpage',iparms:[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDSDT_REQUISITOS_LASTPAGE","{handler:'subgridsdt_requisitos_lastpage',iparms:[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDRASCUNHO_FIRSTPAGE","{handler:'subgridrascunho_firstpage',iparms:[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'edtContagemResultado_DataDmn_Linktarget',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDRASCUNHO_PREVPAGE","{handler:'subgridrascunho_previouspage',iparms:[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'edtContagemResultado_DataDmn_Linktarget',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDRASCUNHO_NEXTPAGE","{handler:'subgridrascunho_nextpage',iparms:[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'edtContagemResultado_DataDmn_Linktarget',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDRASCUNHO_LASTPAGE","{handler:'subgridrascunho_lastpage',iparms:[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'edtContagemResultado_DataDmn_Linktarget',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'lblTextblocktitle_Visible',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'edtavUsuario_pessoanom_Enabled',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'edtavUsuario_cargouonom_Enabled',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'edtavDataprevista_Visible',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{av:'cmbavServicogrupo_codigo'},{av:'cmbavServico_codigo'},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV82DadosDaSS = new SdtSolicitacaoServico(context);
         AV16SolicitacaoServico = new SdtSolicitacaoServico(context);
         Gxuitabspanel_tabs_Activetabid = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A58Usuario_PessoaNom = "";
         A1076Usuario_CargoUONom = "";
         A605Servico_Sigla = "";
         A158ServicoGrupo_Descricao = "";
         A129Sistema_Sigla = "";
         AV64Agrupador = "";
         AV106SDT_Requisitos = new GxObjectCollection( context, "SDT_Requisitos.SDT_RequisitosItem", "GxEv3Up14_Meetrika", "SdtSDT_Requisitos_SDT_RequisitosItem", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV87Caller = "";
         AV113ContagemResultadoRequisito = new SdtContagemResultadoRequisito(context);
         AV73StatusAnterior = "";
         AV96Requisito = new SdtRequisito(context);
         AV93Codigos = new GxSimpleCollection();
         AV20Usuarios = new GxSimpleCollection();
         AV23Attachments = new GxSimpleCollection();
         AV25Resultado = "";
         A1926Requisito_Agrupador = "";
         AV107SDT_RequisitoItem = new SdtSDT_Requisitos_SDT_RequisitosItem(context);
         A2001Requisito_Identificador = "";
         A1927Requisito_Titulo = "";
         A1923Requisito_Descricao = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         A1449ContagemResultadoEvidencia_Link = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         A587ContagemResultadoEvidencia_Descricao = "";
         A1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         AV74ActiveTabId = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV62btnAlterarReqNeg = "";
         AV133Btnalterarreqneg_GXI = "";
         AV44btnExcluirReqNeg = "";
         AV134Btnexcluirreqneg_GXI = "";
         AV75btnContinuar = "";
         AV131Btncontinuar_GXI = "";
         AV76btnCancelar = "";
         AV132Btncancelar_GXI = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A493ContagemResultado_DemandaFM = "";
         A494ContagemResultado_Descricao = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         OldWcwc_contagemresultadoevidencias = "";
         WebComp_Wcwc_contagemresultadoevidencias_Component = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00MJ2_A127Sistema_Codigo = new int[1] ;
         H00MJ2_A129Sistema_Sigla = new String[] {""} ;
         H00MJ2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00MJ2_A130Sistema_Ativo = new bool[] {false} ;
         H00MJ3_A2041TipoRequisito_Codigo = new int[1] ;
         H00MJ3_A2042TipoRequisito_Identificador = new String[] {""} ;
         H00MJ3_A2039TipoRequisito_LinNegCod = new int[1] ;
         H00MJ3_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         Gridsdt_requisitosContainer = new GXWebGrid( context);
         GridrascunhoContainer = new GXWebGrid( context);
         H00MJ6_A508ContagemResultado_Owner = new int[1] ;
         H00MJ6_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00MJ6_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00MJ6_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         H00MJ6_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         H00MJ6_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MJ6_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00MJ6_A494ContagemResultado_Descricao = new String[] {""} ;
         H00MJ6_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00MJ6_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00MJ6_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00MJ6_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00MJ6_A456ContagemResultado_Codigo = new int[1] ;
         H00MJ6_A40000GXC1 = new int[1] ;
         H00MJ6_n40000GXC1 = new bool[] {false} ;
         H00MJ6_A40001GXC2 = new int[1] ;
         H00MJ6_n40001GXC2 = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         H00MJ9_AGRIDRASCUNHO_nRecordCount = new long[1] ;
         AV19Usuario_PessoaNom = "";
         AV18Usuario_CargoUONom = "";
         AV83DataPrevista = (DateTime)(DateTime.MinValue);
         AV97Requisito_Agrupador = "";
         AV100Requisito_Identificador = "";
         AV101Requisito_Titulo = "";
         AV99Requisito_Descricao = "";
         AV90Request = new GxHttpRequest( context);
         AV21Websession = context.GetSession();
         Gridsdt_requisitosRow = new GXWebRow();
         AV135GXV8 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV10Message = new SdtMessages_Message(context);
         AV137GXV10 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV89ContagemResultado = new SdtContagemResultado(context);
         H00MJ10_A57Usuario_PessoaCod = new int[1] ;
         H00MJ10_A1073Usuario_CargoCod = new int[1] ;
         H00MJ10_n1073Usuario_CargoCod = new bool[] {false} ;
         H00MJ10_A1Usuario_Codigo = new int[1] ;
         H00MJ10_A58Usuario_PessoaNom = new String[] {""} ;
         H00MJ10_n58Usuario_PessoaNom = new bool[] {false} ;
         H00MJ10_A1075Usuario_CargoUOCod = new int[1] ;
         H00MJ10_n1075Usuario_CargoUOCod = new bool[] {false} ;
         H00MJ10_A1076Usuario_CargoUONom = new String[] {""} ;
         H00MJ10_n1076Usuario_CargoUONom = new bool[] {false} ;
         H00MJ11_A155Servico_Codigo = new int[1] ;
         H00MJ11_A157ServicoGrupo_Codigo = new int[1] ;
         H00MJ11_A605Servico_Sigla = new String[] {""} ;
         H00MJ11_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00MJ11_A2047Servico_LinNegCod = new int[1] ;
         H00MJ11_n2047Servico_LinNegCod = new bool[] {false} ;
         H00MJ12_A2047Servico_LinNegCod = new int[1] ;
         H00MJ12_n2047Servico_LinNegCod = new bool[] {false} ;
         H00MJ12_A155Servico_Codigo = new int[1] ;
         H00MJ13_A57Usuario_PessoaCod = new int[1] ;
         H00MJ13_A1Usuario_Codigo = new int[1] ;
         H00MJ13_A58Usuario_PessoaNom = new String[] {""} ;
         H00MJ13_n58Usuario_PessoaNom = new bool[] {false} ;
         AV34Responsavel = "";
         AV61ReqIdentificador = "";
         H00MJ14_A157ServicoGrupo_Codigo = new int[1] ;
         H00MJ14_A155Servico_Codigo = new int[1] ;
         H00MJ15_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         H00MJ15_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         H00MJ15_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         H00MJ15_A2001Requisito_Identificador = new String[] {""} ;
         H00MJ15_n2001Requisito_Identificador = new bool[] {false} ;
         H00MJ15_A1927Requisito_Titulo = new String[] {""} ;
         H00MJ15_n1927Requisito_Titulo = new bool[] {false} ;
         H00MJ15_A1923Requisito_Descricao = new String[] {""} ;
         H00MJ15_n1923Requisito_Descricao = new bool[] {false} ;
         H00MJ15_A2002Requisito_Prioridade = new short[1] ;
         H00MJ15_n2002Requisito_Prioridade = new bool[] {false} ;
         H00MJ15_A1932Requisito_Pontuacao = new decimal[1] ;
         H00MJ15_n1932Requisito_Pontuacao = new bool[] {false} ;
         H00MJ15_A1934Requisito_Status = new short[1] ;
         H00MJ15_n1934Requisito_Status = new bool[] {false} ;
         H00MJ15_A1926Requisito_Agrupador = new String[] {""} ;
         H00MJ15_n1926Requisito_Agrupador = new bool[] {false} ;
         AV84Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         H00MJ16_A456ContagemResultado_Codigo = new int[1] ;
         H00MJ16_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00MJ16_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00MJ16_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00MJ16_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00MJ16_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00MJ16_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         H00MJ16_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         H00MJ16_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00MJ16_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         H00MJ16_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         H00MJ16_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         H00MJ16_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         H00MJ16_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         H00MJ16_A645TipoDocumento_Codigo = new int[1] ;
         H00MJ16_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00MJ16_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         H00MJ16_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         H00MJ16_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         H00MJ16_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         A588ContagemResultadoEvidencia_Arquivo_Filename = "";
         A588ContagemResultadoEvidencia_Arquivo_Filetype = "";
         AV85Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         H00MJ17_A1635Servico_IsPublico = new bool[] {false} ;
         H00MJ17_A632Servico_Ativo = new bool[] {false} ;
         H00MJ17_A157ServicoGrupo_Codigo = new int[1] ;
         H00MJ17_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00MJ18_A1530Servico_TipoHierarquia = new short[1] ;
         H00MJ18_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H00MJ18_A157ServicoGrupo_Codigo = new int[1] ;
         H00MJ18_A1635Servico_IsPublico = new bool[] {false} ;
         H00MJ18_A632Servico_Ativo = new bool[] {false} ;
         H00MJ18_A633Servico_UO = new int[1] ;
         H00MJ18_n633Servico_UO = new bool[] {false} ;
         H00MJ18_A155Servico_Codigo = new int[1] ;
         H00MJ18_A605Servico_Sigla = new String[] {""} ;
         H00MJ19_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MJ19_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00MJ19_A129Sistema_Sigla = new String[] {""} ;
         H00MJ19_A127Sistema_Codigo = new int[1] ;
         AV28AreaTrabalho = new SdtAreaTrabalho(context);
         AV24EmailText = "";
         AV9Demandante = "";
         AV12NomeSistema = "";
         AV26Subject = "";
         GXt_dtime4 = (DateTime)(DateTime.MinValue);
         H00MJ20_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         H00MJ20_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         H00MJ20_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         AV155GXV17 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         GridrascunhoRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbjava_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         bttBtnsalvar_Jsonclick = "";
         lblTextblocktitle_Jsonclick = "";
         subGridrascunho_Linesclass = "";
         GridrascunhoColumn = new GXWebColumn();
         subGridsdt_requisitos_Linesclass = "";
         Gridsdt_requisitosColumn = new GXWebColumn();
         bttBtnbtnincluirreqneg_Jsonclick = "";
         lblTextblockrequisito_agrupador_Jsonclick = "";
         lblTextblockrequisito_identificador_Jsonclick = "";
         lblTextblockrequisito_tiporeqcod_Jsonclick = "";
         lblTextblockrequisito_titulo_Jsonclick = "";
         lblTextblockrequisito_descricao_Jsonclick = "";
         lblTextblockrequisito_prioridade_Jsonclick = "";
         lblTextblockrequisito_status_Jsonclick = "";
         lblTextblockindicadorautomatico_Jsonclick = "";
         lblIndicadorautomatico_righttext_Jsonclick = "";
         lblTextblockusuario_pessoanom_Jsonclick = "";
         lblTextblockusuario_cargouonom_Jsonclick = "";
         lblTextblockdadosdass_contagemresultado_ss_Jsonclick = "";
         lblTextblockservicogrupo_codigo_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblocksistema_codigo_Jsonclick = "";
         lblTextblockdadosdass_contagemresultado_descricao_Jsonclick = "";
         lblTextblockdadosdass_contagemresultado_observacao_Jsonclick = "";
         lblTextblockdadosdass_contagemresultado_referencia_Jsonclick = "";
         lblTextblockdadosdass_contagemresultado_restricoes_Jsonclick = "";
         lblTextblockdadosdass_contagemresultado_prioridadeprevista_Jsonclick = "";
         lblTextblockdataprevista_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_ss__default(),
            new Object[][] {
                new Object[] {
               H00MJ2_A127Sistema_Codigo, H00MJ2_A129Sistema_Sigla, H00MJ2_A135Sistema_AreaTrabalhoCod, H00MJ2_A130Sistema_Ativo
               }
               , new Object[] {
               H00MJ3_A2041TipoRequisito_Codigo, H00MJ3_A2042TipoRequisito_Identificador, H00MJ3_A2039TipoRequisito_LinNegCod, H00MJ3_n2039TipoRequisito_LinNegCod
               }
               , new Object[] {
               H00MJ6_A508ContagemResultado_Owner, H00MJ6_A484ContagemResultado_StatusDmn, H00MJ6_n484ContagemResultado_StatusDmn, H00MJ6_A1350ContagemResultado_DataCadastro, H00MJ6_n1350ContagemResultado_DataCadastro, H00MJ6_A472ContagemResultado_DataEntrega, H00MJ6_n472ContagemResultado_DataEntrega, H00MJ6_A494ContagemResultado_Descricao, H00MJ6_n494ContagemResultado_Descricao, H00MJ6_A493ContagemResultado_DemandaFM,
               H00MJ6_n493ContagemResultado_DemandaFM, H00MJ6_A471ContagemResultado_DataDmn, H00MJ6_A456ContagemResultado_Codigo, H00MJ6_A40000GXC1, H00MJ6_n40000GXC1, H00MJ6_A40001GXC2, H00MJ6_n40001GXC2
               }
               , new Object[] {
               H00MJ9_AGRIDRASCUNHO_nRecordCount
               }
               , new Object[] {
               H00MJ10_A57Usuario_PessoaCod, H00MJ10_A1073Usuario_CargoCod, H00MJ10_n1073Usuario_CargoCod, H00MJ10_A1Usuario_Codigo, H00MJ10_A58Usuario_PessoaNom, H00MJ10_n58Usuario_PessoaNom, H00MJ10_A1075Usuario_CargoUOCod, H00MJ10_n1075Usuario_CargoUOCod, H00MJ10_A1076Usuario_CargoUONom, H00MJ10_n1076Usuario_CargoUONom
               }
               , new Object[] {
               H00MJ11_A155Servico_Codigo, H00MJ11_A157ServicoGrupo_Codigo, H00MJ11_A605Servico_Sigla, H00MJ11_A158ServicoGrupo_Descricao, H00MJ11_A2047Servico_LinNegCod, H00MJ11_n2047Servico_LinNegCod
               }
               , new Object[] {
               H00MJ12_A2047Servico_LinNegCod, H00MJ12_n2047Servico_LinNegCod, H00MJ12_A155Servico_Codigo
               }
               , new Object[] {
               H00MJ13_A57Usuario_PessoaCod, H00MJ13_A1Usuario_Codigo, H00MJ13_A58Usuario_PessoaNom, H00MJ13_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00MJ14_A157ServicoGrupo_Codigo, H00MJ14_A155Servico_Codigo
               }
               , new Object[] {
               H00MJ15_A2005ContagemResultadoRequisito_Codigo, H00MJ15_A2003ContagemResultadoRequisito_OSCod, H00MJ15_A2004ContagemResultadoRequisito_ReqCod, H00MJ15_A2001Requisito_Identificador, H00MJ15_n2001Requisito_Identificador, H00MJ15_A1927Requisito_Titulo, H00MJ15_n1927Requisito_Titulo, H00MJ15_A1923Requisito_Descricao, H00MJ15_n1923Requisito_Descricao, H00MJ15_A2002Requisito_Prioridade,
               H00MJ15_n2002Requisito_Prioridade, H00MJ15_A1932Requisito_Pontuacao, H00MJ15_n1932Requisito_Pontuacao, H00MJ15_A1934Requisito_Status, H00MJ15_n1934Requisito_Status, H00MJ15_A1926Requisito_Agrupador, H00MJ15_n1926Requisito_Agrupador
               }
               , new Object[] {
               H00MJ16_A456ContagemResultado_Codigo, H00MJ16_A589ContagemResultadoEvidencia_NomeArq, H00MJ16_n589ContagemResultadoEvidencia_NomeArq, H00MJ16_A590ContagemResultadoEvidencia_TipoArq, H00MJ16_n590ContagemResultadoEvidencia_TipoArq, H00MJ16_A586ContagemResultadoEvidencia_Codigo, H00MJ16_A1449ContagemResultadoEvidencia_Link, H00MJ16_n1449ContagemResultadoEvidencia_Link, H00MJ16_A591ContagemResultadoEvidencia_Data, H00MJ16_n591ContagemResultadoEvidencia_Data,
               H00MJ16_A587ContagemResultadoEvidencia_Descricao, H00MJ16_n587ContagemResultadoEvidencia_Descricao, H00MJ16_A1393ContagemResultadoEvidencia_RdmnCreated, H00MJ16_n1393ContagemResultadoEvidencia_RdmnCreated, H00MJ16_A645TipoDocumento_Codigo, H00MJ16_n645TipoDocumento_Codigo, H00MJ16_A1493ContagemResultadoEvidencia_Owner, H00MJ16_n1493ContagemResultadoEvidencia_Owner, H00MJ16_A588ContagemResultadoEvidencia_Arquivo, H00MJ16_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               H00MJ17_A1635Servico_IsPublico, H00MJ17_A632Servico_Ativo, H00MJ17_A157ServicoGrupo_Codigo, H00MJ17_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H00MJ18_A1530Servico_TipoHierarquia, H00MJ18_n1530Servico_TipoHierarquia, H00MJ18_A157ServicoGrupo_Codigo, H00MJ18_A1635Servico_IsPublico, H00MJ18_A632Servico_Ativo, H00MJ18_A633Servico_UO, H00MJ18_n633Servico_UO, H00MJ18_A155Servico_Codigo, H00MJ18_A605Servico_Sigla
               }
               , new Object[] {
               H00MJ19_A60ContratanteUsuario_UsuarioCod, H00MJ19_A63ContratanteUsuario_ContratanteCod, H00MJ19_A129Sistema_Sigla, H00MJ19_A127Sistema_Codigo
               }
               , new Object[] {
               H00MJ20_A2004ContagemResultadoRequisito_ReqCod, H00MJ20_A2003ContagemResultadoRequisito_OSCod, H00MJ20_A2005ContagemResultadoRequisito_Codigo
               }
            }
         );
         WebComp_Wcwc_contagemresultadoevidencias = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         cmbavRequisito_status.Enabled = 0;
         edtavSdt_requisitos__requisito_codigo_Enabled = 0;
         edtavAgrupador_Enabled = 0;
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         edtavSdt_requisitos__requisito_descricao_Enabled = 0;
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
         edtavQtdrequisitos_Enabled = 0;
         edtavQtdanexos_Enabled = 0;
      }

      private short nRcdExists_10 ;
      private short nIsMod_10 ;
      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_163 ;
      private short nGXsfl_163_idx=1 ;
      private short A1530Servico_TipoHierarquia ;
      private short nRC_GXsfl_178 ;
      private short nGXsfl_178_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRIDSDT_REQUISITOS_nEOF ;
      private short GRIDRASCUNHO_nEOF ;
      private short AV66Ordem ;
      private short AV78i ;
      private short A2002Requisito_Prioridade ;
      private short A1934Requisito_Status ;
      private short wbEnd ;
      private short wbStart ;
      private short AV63RequisitoIndex ;
      private short AV124GXV7 ;
      private short AV77QtdRequisitos ;
      private short AV86QtdAnexos ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV102Requisito_Prioridade ;
      private short AV103Requisito_Status ;
      private short nGXsfl_163_Refreshing=0 ;
      private short subGridsdt_requisitos_Backcolorstyle ;
      private short nGXsfl_178_Refreshing=0 ;
      private short subGridrascunho_Backcolorstyle ;
      private short nGXsfl_163_fel_idx=1 ;
      private short edtavSdt_requisitos__requisito_identificador_Fontstrikethru ;
      private short edtavSdt_requisitos__requisito_titulo_Fontstrikethru ;
      private short nGXsfl_163_bak_idx=1 ;
      private short AV145GXLvl438 ;
      private short AV46Index ;
      private short AV30Dias ;
      private short GXt_int3 ;
      private short subGridrascunho_Titlebackstyle ;
      private short subGridrascunho_Allowselection ;
      private short subGridrascunho_Allowhovering ;
      private short subGridrascunho_Allowcollapsing ;
      private short subGridrascunho_Collapsed ;
      private short subGridsdt_requisitos_Titlebackstyle ;
      private short subGridsdt_requisitos_Allowselection ;
      private short subGridsdt_requisitos_Allowhovering ;
      private short subGridsdt_requisitos_Allowcollapsing ;
      private short subGridsdt_requisitos_Collapsed ;
      private short nGXWrapped ;
      private short subGridsdt_requisitos_Backstyle ;
      private short subGridrascunho_Backstyle ;
      private short wbTemp ;
      private int AV114LinhaNegocio_Codigo ;
      private int subGridsdt_requisitos_Rows ;
      private int A1Usuario_Codigo ;
      private int A1075Usuario_CargoUOCod ;
      private int A155Servico_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A2047Servico_LinNegCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A127Sistema_Codigo ;
      private int A633Servico_UO ;
      private int AV17Usuario_CargoUOCod ;
      private int AV15ServicoGrupo_Codigo ;
      private int subGridrascunho_Rows ;
      private int A456ContagemResultado_Codigo ;
      private int AV67Codigo ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A645TipoDocumento_Codigo ;
      private int A1493ContagemResultadoEvidencia_Owner ;
      private int AV141GXV14 ;
      private int A1551Servico_Responsavel ;
      private int edtavLinhanegocio_codigo_Visible ;
      private int edtavRequisitoindex_Visible ;
      private int AV14Servico_Codigo ;
      private int gxdynajaxindex ;
      private int AV36Sistema_Codigo ;
      private int AV115Requisito_TipoReqCod ;
      private int subGridsdt_requisitos_Islastpage ;
      private int subGridrascunho_Islastpage ;
      private int edtavSdt_requisitos__requisito_codigo_Enabled ;
      private int edtavAgrupador_Enabled ;
      private int edtavSdt_requisitos__requisito_identificador_Enabled ;
      private int edtavSdt_requisitos__requisito_titulo_Enabled ;
      private int edtavSdt_requisitos__requisito_descricao_Enabled ;
      private int edtavQtdrequisitos_Enabled ;
      private int edtavQtdanexos_Enabled ;
      private int GRIDSDT_REQUISITOS_nGridOutOfScope ;
      private int GXPagingFrom3 ;
      private int GXPagingTo3 ;
      private int A508ContagemResultado_Owner ;
      private int A40000GXC1 ;
      private int A40001GXC2 ;
      private int AV29Servico_Responsavel ;
      private int lblTbjava_Visible ;
      private int edtavServico_responsavel_Visible ;
      private int edtavDadosdass_contagemresultado_descricao_Width ;
      private int edtavDadosdass_contagemresultado_prioridadeprevista_Width ;
      private int edtavAgrupador_Forecolor ;
      private int edtavBtnalterarreqneg_Visible ;
      private int edtavBtnexcluirreqneg_Visible ;
      private int AV136GXV9 ;
      private int AV138GXV11 ;
      private int AV139GXV12 ;
      private int AV140GXV13 ;
      private int bttBtnsalvar_Visible ;
      private int lblTextblocktitle_Visible ;
      private int A57Usuario_PessoaCod ;
      private int A1073Usuario_CargoCod ;
      private int GXt_int1 ;
      private int edtavDadosdass_contagemresultado_ss_Enabled ;
      private int edtavUsuario_pessoanom_Enabled ;
      private int edtavUsuario_cargouonom_Enabled ;
      private int AV146GXV15 ;
      private int GXt_int2 ;
      private int edtavRequisito_identificador_Enabled ;
      private int edtavDadosdass_contagemresultado_restricoes_Visible ;
      private int edtavDadosdass_contagemresultado_prioridadeprevista_Visible ;
      private int edtavDataprevista_Visible ;
      private int edtavDadosdass_contagemresultado_referencia_Visible ;
      private int edtavDadosdass_contagemresultado_ss_Visible ;
      private int bttBtnenter_Enabled ;
      private int AV153GXV16 ;
      private int AV98Requisito_Codigo ;
      private int AV112ContagemResultadoRequisito_Codigo ;
      private int AV156GXV18 ;
      private int AV157GXV19 ;
      private int tblTableactions_Visible ;
      private int subGridrascunho_Titlebackcolor ;
      private int subGridrascunho_Allbackcolor ;
      private int subGridrascunho_Selectioncolor ;
      private int subGridrascunho_Hoveringcolor ;
      private int subGridsdt_requisitos_Titlebackcolor ;
      private int subGridsdt_requisitos_Allbackcolor ;
      private int subGridsdt_requisitos_Selectioncolor ;
      private int subGridsdt_requisitos_Hoveringcolor ;
      private int idxLst ;
      private int subGridsdt_requisitos_Backcolor ;
      private int edtavBtnalterarreqneg_Enabled ;
      private int edtavBtnexcluirreqneg_Enabled ;
      private int edtavAgrupador_Visible ;
      private int subGridrascunho_Backcolor ;
      private int edtavBtncontinuar_Enabled ;
      private int edtavBtncontinuar_Visible ;
      private int edtavBtncancelar_Enabled ;
      private int edtavBtncancelar_Visible ;
      private int edtavQtdrequisitos_Visible ;
      private int edtavQtdanexos_Visible ;
      private long GRIDSDT_REQUISITOS_nFirstRecordOnPage ;
      private long GRIDRASCUNHO_nFirstRecordOnPage ;
      private long GRIDSDT_REQUISITOS_nCurrentRecord ;
      private long GRIDRASCUNHO_nCurrentRecord ;
      private long GRIDSDT_REQUISITOS_nRecordCount ;
      private long GRIDRASCUNHO_nRecordCount ;
      private decimal AV104Requisito_Pontuacao ;
      private decimal A1932Requisito_Pontuacao ;
      private String Gxuitabspanel_tabs_Activetabid ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_163_idx="0001" ;
      private String A58Usuario_PessoaNom ;
      private String A1076Usuario_CargoUONom ;
      private String A605Servico_Sigla ;
      private String A129Sistema_Sigla ;
      private String edtavAgrupador_Internalname ;
      private String GXKey ;
      private String sGXsfl_178_idx="0001" ;
      private String edtContagemResultado_DataDmn_Linktarget ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV87Caller ;
      private String AV73StatusAnterior ;
      private String AV25Resultado ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String AV74ActiveTabId ;
      private String Dvpanel_requisitos_Width ;
      private String Dvpanel_requisitos_Cls ;
      private String Dvpanel_requisitos_Title ;
      private String Dvpanel_requisitos_Iconposition ;
      private String Gxuitabspanel_tabs_Width ;
      private String Gxuitabspanel_tabs_Cls ;
      private String Gxuitabspanel_tabs_Designtimetabs ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Yesbuttonposition ;
      private String Confirmpanel_Confirmtype ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavLinhanegocio_codigo_Internalname ;
      private String edtavLinhanegocio_codigo_Jsonclick ;
      private String edtavRequisitoindex_Internalname ;
      private String edtavRequisitoindex_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBtnalterarreqneg_Internalname ;
      private String edtavBtnexcluirreqneg_Internalname ;
      private String edtavBtncontinuar_Internalname ;
      private String edtavBtncancelar_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_Descricao_Internalname ;
      private String edtContagemResultado_DataEntrega_Internalname ;
      private String edtContagemResultado_DataCadastro_Internalname ;
      private String edtavQtdrequisitos_Internalname ;
      private String edtavQtdanexos_Internalname ;
      private String OldWcwc_contagemresultadoevidencias ;
      private String WebComp_Wcwc_contagemresultadoevidencias_Component ;
      private String chkavIndicadorautomatico_Internalname ;
      private String GXCCtl ;
      private String chkavTemservicopadrao_Internalname ;
      private String edtavUsuario_pessoanom_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String cmbavRequisito_status_Internalname ;
      private String edtavSdt_requisitos__requisito_codigo_Internalname ;
      private String edtavSdt_requisitos__requisito_identificador_Internalname ;
      private String edtavSdt_requisitos__requisito_titulo_Internalname ;
      private String edtavSdt_requisitos__requisito_descricao_Internalname ;
      private String cmbavSdt_requisitos__requisito_prioridade_Internalname ;
      private String cmbavSdt_requisitos__requisito_status_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV19Usuario_PessoaNom ;
      private String AV18Usuario_CargoUONom ;
      private String edtavUsuario_cargouonom_Internalname ;
      private String edtavDadosdass_contagemresultado_ss_Internalname ;
      private String cmbavServicogrupo_codigo_Internalname ;
      private String cmbavServico_codigo_Internalname ;
      private String dynavSistema_codigo_Internalname ;
      private String edtavDadosdass_contagemresultado_descricao_Internalname ;
      private String edtavDadosdass_contagemresultado_observacao_Internalname ;
      private String edtavDadosdass_contagemresultado_referencia_Internalname ;
      private String edtavDadosdass_contagemresultado_restricoes_Internalname ;
      private String edtavDadosdass_contagemresultado_prioridadeprevista_Internalname ;
      private String edtavDataprevista_Internalname ;
      private String edtavRequisito_agrupador_Internalname ;
      private String edtavRequisito_identificador_Internalname ;
      private String dynavRequisito_tiporeqcod_Internalname ;
      private String edtavRequisito_titulo_Internalname ;
      private String edtavRequisito_descricao_Internalname ;
      private String cmbavRequisito_prioridade_Internalname ;
      private String edtavServico_responsavel_Internalname ;
      private String sGXsfl_163_fel_idx="0001" ;
      private String lblTbjava_Internalname ;
      private String edtavBtnalterarreqneg_Tooltiptext ;
      private String edtavBtnexcluirreqneg_Tooltiptext ;
      private String lblTbjava_Caption ;
      private String Confirmpanel_Internalname ;
      private String Dvpanel_requisitos_Internalname ;
      private String bttBtnbtnincluirreqneg_Caption ;
      private String bttBtnbtnincluirreqneg_Internalname ;
      private String bttBtnenter_Caption ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Tooltiptext ;
      private String bttBtnsalvar_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String AV34Responsavel ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filename ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filetype ;
      private String AV24EmailText ;
      private String AV26Subject ;
      private String edtavBtncontinuar_Tooltiptext ;
      private String edtavBtncancelar_Tooltiptext ;
      private String edtContagemResultado_DataDmn_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUsrtable_Internalname ;
      private String edtavServico_responsavel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String bttBtnsalvar_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String subGridrascunho_Internalname ;
      private String subGridrascunho_Class ;
      private String subGridrascunho_Linesclass ;
      private String tblUnnamedtable4_Internalname ;
      private String tblUnnamedtable7_Internalname ;
      private String tblRequisitos_Internalname ;
      private String subGridsdt_requisitos_Internalname ;
      private String subGridsdt_requisitos_Class ;
      private String subGridsdt_requisitos_Linesclass ;
      private String tblUnnamedtable6_Internalname ;
      private String bttBtnbtnincluirreqneg_Jsonclick ;
      private String tblUnnamedtable5_Internalname ;
      private String lblTextblockrequisito_agrupador_Internalname ;
      private String lblTextblockrequisito_agrupador_Jsonclick ;
      private String edtavRequisito_agrupador_Jsonclick ;
      private String lblTextblockrequisito_identificador_Internalname ;
      private String lblTextblockrequisito_identificador_Jsonclick ;
      private String lblTextblockrequisito_tiporeqcod_Internalname ;
      private String lblTextblockrequisito_tiporeqcod_Jsonclick ;
      private String dynavRequisito_tiporeqcod_Jsonclick ;
      private String lblTextblockrequisito_titulo_Internalname ;
      private String lblTextblockrequisito_titulo_Jsonclick ;
      private String lblTextblockrequisito_descricao_Internalname ;
      private String lblTextblockrequisito_descricao_Jsonclick ;
      private String lblTextblockrequisito_prioridade_Internalname ;
      private String lblTextblockrequisito_prioridade_Jsonclick ;
      private String cmbavRequisito_prioridade_Jsonclick ;
      private String lblTextblockrequisito_status_Internalname ;
      private String lblTextblockrequisito_status_Jsonclick ;
      private String cmbavRequisito_status_Jsonclick ;
      private String tblTablemergedrequisito_identificador_Internalname ;
      private String edtavRequisito_identificador_Jsonclick ;
      private String lblTextblockindicadorautomatico_Internalname ;
      private String lblTextblockindicadorautomatico_Jsonclick ;
      private String lblIndicadorautomatico_righttext_Internalname ;
      private String lblIndicadorautomatico_righttext_Jsonclick ;
      private String tblUnnamedtable8_Internalname ;
      private String lblTextblockusuario_pessoanom_Internalname ;
      private String lblTextblockusuario_pessoanom_Jsonclick ;
      private String edtavUsuario_pessoanom_Jsonclick ;
      private String lblTextblockusuario_cargouonom_Internalname ;
      private String lblTextblockusuario_cargouonom_Jsonclick ;
      private String edtavUsuario_cargouonom_Jsonclick ;
      private String lblTextblockdadosdass_contagemresultado_ss_Internalname ;
      private String lblTextblockdadosdass_contagemresultado_ss_Jsonclick ;
      private String edtavDadosdass_contagemresultado_ss_Jsonclick ;
      private String lblTextblockservicogrupo_codigo_Internalname ;
      private String lblTextblockservicogrupo_codigo_Jsonclick ;
      private String cmbavServicogrupo_codigo_Jsonclick ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String cmbavServico_codigo_Jsonclick ;
      private String lblTextblocksistema_codigo_Internalname ;
      private String lblTextblocksistema_codigo_Jsonclick ;
      private String dynavSistema_codigo_Jsonclick ;
      private String lblTextblockdadosdass_contagemresultado_descricao_Internalname ;
      private String lblTextblockdadosdass_contagemresultado_descricao_Jsonclick ;
      private String edtavDadosdass_contagemresultado_descricao_Jsonclick ;
      private String lblTextblockdadosdass_contagemresultado_observacao_Internalname ;
      private String lblTextblockdadosdass_contagemresultado_observacao_Jsonclick ;
      private String lblTextblockdadosdass_contagemresultado_referencia_Internalname ;
      private String lblTextblockdadosdass_contagemresultado_referencia_Jsonclick ;
      private String lblTextblockdadosdass_contagemresultado_restricoes_Internalname ;
      private String lblTextblockdadosdass_contagemresultado_restricoes_Jsonclick ;
      private String lblTextblockdadosdass_contagemresultado_prioridadeprevista_Internalname ;
      private String lblTextblockdadosdass_contagemresultado_prioridadeprevista_Jsonclick ;
      private String edtavDadosdass_contagemresultado_prioridadeprevista_Jsonclick ;
      private String lblTextblockdataprevista_Internalname ;
      private String lblTextblockdataprevista_Jsonclick ;
      private String edtavDataprevista_Jsonclick ;
      private String divLayout_groupanexos_Internalname ;
      private String divUnnamedgroupcontainergroupanexos_Internalname ;
      private String grpUnnamedgroup9_Internalname ;
      private String divGroupanexos_Internalname ;
      private String tblTableline7_Internalname ;
      private String tblTabletitle_Internalname ;
      private String edtavBtnalterarreqneg_Jsonclick ;
      private String edtavBtnexcluirreqneg_Jsonclick ;
      private String ROClassString ;
      private String edtavSdt_requisitos__requisito_codigo_Jsonclick ;
      private String edtavAgrupador_Jsonclick ;
      private String edtavSdt_requisitos__requisito_identificador_Jsonclick ;
      private String edtavSdt_requisitos__requisito_titulo_Jsonclick ;
      private String edtavSdt_requisitos__requisito_descricao_Jsonclick ;
      private String cmbavSdt_requisitos__requisito_prioridade_Jsonclick ;
      private String cmbavSdt_requisitos__requisito_status_Jsonclick ;
      private String sGXsfl_178_fel_idx="0001" ;
      private String edtavBtncontinuar_Jsonclick ;
      private String edtavBtncancelar_Jsonclick ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtContagemResultado_Descricao_Jsonclick ;
      private String edtContagemResultado_DataEntrega_Jsonclick ;
      private String edtContagemResultado_DataCadastro_Jsonclick ;
      private String edtavQtdrequisitos_Jsonclick ;
      private String edtavQtdanexos_Jsonclick ;
      private String div_Internalname ;
      private String Gxuitabspanel_tabs_Internalname ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private DateTime A1393ContagemResultadoEvidencia_RdmnCreated ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime AV83DataPrevista ;
      private DateTime GXt_dtime4 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool entryPointCalled ;
      private bool n58Usuario_PessoaNom ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n1076Usuario_CargoUONom ;
      private bool n2047Servico_LinNegCod ;
      private bool A632Servico_Ativo ;
      private bool A1635Servico_IsPublico ;
      private bool AV35TemServicoPadrao ;
      private bool n633Servico_UO ;
      private bool n1530Servico_TipoHierarquia ;
      private bool toggleJsOutput ;
      private bool AV5CheckRequiredFieldsResult ;
      private bool AV27ContratanteSemEmailSda ;
      private bool Dvpanel_requisitos_Collapsible ;
      private bool Dvpanel_requisitos_Collapsed ;
      private bool Dvpanel_requisitos_Autowidth ;
      private bool Dvpanel_requisitos_Autoheight ;
      private bool Dvpanel_requisitos_Showcollapseicon ;
      private bool Dvpanel_requisitos_Autoscroll ;
      private bool Gxuitabspanel_tabs_Autowidth ;
      private bool Gxuitabspanel_tabs_Autoheight ;
      private bool Gxuitabspanel_tabs_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n494ContagemResultado_Descricao ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n40000GXC1 ;
      private bool n40001GXC2 ;
      private bool AV91IndicadorAutomatico ;
      private bool returnInSub ;
      private bool gx_BV163 ;
      private bool AV52IsLiberado ;
      private bool AV49IsCadastrado ;
      private bool gx_refresh_fired ;
      private bool n1073Usuario_CargoCod ;
      private bool n2001Requisito_Identificador ;
      private bool n1927Requisito_Titulo ;
      private bool n1923Requisito_Descricao ;
      private bool n2002Requisito_Prioridade ;
      private bool n1932Requisito_Pontuacao ;
      private bool n1934Requisito_Status ;
      private bool n1926Requisito_Agrupador ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n1449ContagemResultadoEvidencia_Link ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n587ContagemResultadoEvidencia_Descricao ;
      private bool n1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool n645TipoDocumento_Codigo ;
      private bool n1493ContagemResultadoEvidencia_Owner ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool AV62btnAlterarReqNeg_IsBlob ;
      private bool AV44btnExcluirReqNeg_IsBlob ;
      private bool AV75btnContinuar_IsBlob ;
      private bool AV76btnCancelar_IsBlob ;
      private String A1923Requisito_Descricao ;
      private String A1449ContagemResultadoEvidencia_Link ;
      private String A587ContagemResultadoEvidencia_Descricao ;
      private String AV99Requisito_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String AV64Agrupador ;
      private String A1926Requisito_Agrupador ;
      private String A2001Requisito_Identificador ;
      private String A1927Requisito_Titulo ;
      private String AV133Btnalterarreqneg_GXI ;
      private String AV134Btnexcluirreqneg_GXI ;
      private String AV131Btncontinuar_GXI ;
      private String AV132Btncancelar_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV97Requisito_Agrupador ;
      private String AV100Requisito_Identificador ;
      private String AV101Requisito_Titulo ;
      private String AV61ReqIdentificador ;
      private String AV9Demandante ;
      private String AV12NomeSistema ;
      private String AV62btnAlterarReqNeg ;
      private String AV44btnExcluirReqNeg ;
      private String AV75btnContinuar ;
      private String AV76btnCancelar ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private GXWebComponent WebComp_Wcwc_contagemresultadoevidencias ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Gridsdt_requisitosContainer ;
      private GXWebGrid GridrascunhoContainer ;
      private GXWebRow Gridsdt_requisitosRow ;
      private GXWebRow GridrascunhoRow ;
      private GXWebColumn GridrascunhoColumn ;
      private GXWebColumn Gridsdt_requisitosColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavServicogrupo_codigo ;
      private GXCombobox cmbavServico_codigo ;
      private GXCombobox dynavSistema_codigo ;
      private GXCheckbox chkavIndicadorautomatico ;
      private GXCombobox dynavRequisito_tiporeqcod ;
      private GXCombobox cmbavRequisito_prioridade ;
      private GXCombobox cmbavRequisito_status ;
      private GXCombobox cmbavSdt_requisitos__requisito_prioridade ;
      private GXCombobox cmbavSdt_requisitos__requisito_status ;
      private GXCheckbox chkavTemservicopadrao ;
      private IDataStoreProvider pr_default ;
      private int[] H00MJ2_A127Sistema_Codigo ;
      private String[] H00MJ2_A129Sistema_Sigla ;
      private int[] H00MJ2_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00MJ2_A130Sistema_Ativo ;
      private int[] H00MJ3_A2041TipoRequisito_Codigo ;
      private String[] H00MJ3_A2042TipoRequisito_Identificador ;
      private int[] H00MJ3_A2039TipoRequisito_LinNegCod ;
      private bool[] H00MJ3_n2039TipoRequisito_LinNegCod ;
      private int[] H00MJ6_A508ContagemResultado_Owner ;
      private String[] H00MJ6_A484ContagemResultado_StatusDmn ;
      private bool[] H00MJ6_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00MJ6_A1350ContagemResultado_DataCadastro ;
      private bool[] H00MJ6_n1350ContagemResultado_DataCadastro ;
      private DateTime[] H00MJ6_A472ContagemResultado_DataEntrega ;
      private bool[] H00MJ6_n472ContagemResultado_DataEntrega ;
      private String[] H00MJ6_A494ContagemResultado_Descricao ;
      private bool[] H00MJ6_n494ContagemResultado_Descricao ;
      private String[] H00MJ6_A493ContagemResultado_DemandaFM ;
      private bool[] H00MJ6_n493ContagemResultado_DemandaFM ;
      private DateTime[] H00MJ6_A471ContagemResultado_DataDmn ;
      private int[] H00MJ6_A456ContagemResultado_Codigo ;
      private int[] H00MJ6_A40000GXC1 ;
      private bool[] H00MJ6_n40000GXC1 ;
      private int[] H00MJ6_A40001GXC2 ;
      private bool[] H00MJ6_n40001GXC2 ;
      private long[] H00MJ9_AGRIDRASCUNHO_nRecordCount ;
      private int[] H00MJ10_A57Usuario_PessoaCod ;
      private int[] H00MJ10_A1073Usuario_CargoCod ;
      private bool[] H00MJ10_n1073Usuario_CargoCod ;
      private int[] H00MJ10_A1Usuario_Codigo ;
      private String[] H00MJ10_A58Usuario_PessoaNom ;
      private bool[] H00MJ10_n58Usuario_PessoaNom ;
      private int[] H00MJ10_A1075Usuario_CargoUOCod ;
      private bool[] H00MJ10_n1075Usuario_CargoUOCod ;
      private String[] H00MJ10_A1076Usuario_CargoUONom ;
      private bool[] H00MJ10_n1076Usuario_CargoUONom ;
      private int[] H00MJ11_A155Servico_Codigo ;
      private int[] H00MJ11_A157ServicoGrupo_Codigo ;
      private String[] H00MJ11_A605Servico_Sigla ;
      private String[] H00MJ11_A158ServicoGrupo_Descricao ;
      private int[] H00MJ11_A2047Servico_LinNegCod ;
      private bool[] H00MJ11_n2047Servico_LinNegCod ;
      private int[] H00MJ12_A2047Servico_LinNegCod ;
      private bool[] H00MJ12_n2047Servico_LinNegCod ;
      private int[] H00MJ12_A155Servico_Codigo ;
      private int[] H00MJ13_A57Usuario_PessoaCod ;
      private int[] H00MJ13_A1Usuario_Codigo ;
      private String[] H00MJ13_A58Usuario_PessoaNom ;
      private bool[] H00MJ13_n58Usuario_PessoaNom ;
      private int[] H00MJ14_A157ServicoGrupo_Codigo ;
      private int[] H00MJ14_A155Servico_Codigo ;
      private int[] H00MJ15_A2005ContagemResultadoRequisito_Codigo ;
      private int[] H00MJ15_A2003ContagemResultadoRequisito_OSCod ;
      private int[] H00MJ15_A2004ContagemResultadoRequisito_ReqCod ;
      private String[] H00MJ15_A2001Requisito_Identificador ;
      private bool[] H00MJ15_n2001Requisito_Identificador ;
      private String[] H00MJ15_A1927Requisito_Titulo ;
      private bool[] H00MJ15_n1927Requisito_Titulo ;
      private String[] H00MJ15_A1923Requisito_Descricao ;
      private bool[] H00MJ15_n1923Requisito_Descricao ;
      private short[] H00MJ15_A2002Requisito_Prioridade ;
      private bool[] H00MJ15_n2002Requisito_Prioridade ;
      private decimal[] H00MJ15_A1932Requisito_Pontuacao ;
      private bool[] H00MJ15_n1932Requisito_Pontuacao ;
      private short[] H00MJ15_A1934Requisito_Status ;
      private bool[] H00MJ15_n1934Requisito_Status ;
      private String[] H00MJ15_A1926Requisito_Agrupador ;
      private bool[] H00MJ15_n1926Requisito_Agrupador ;
      private int[] H00MJ16_A456ContagemResultado_Codigo ;
      private String[] H00MJ16_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00MJ16_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] H00MJ16_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00MJ16_n590ContagemResultadoEvidencia_TipoArq ;
      private int[] H00MJ16_A586ContagemResultadoEvidencia_Codigo ;
      private String[] H00MJ16_A1449ContagemResultadoEvidencia_Link ;
      private bool[] H00MJ16_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] H00MJ16_A591ContagemResultadoEvidencia_Data ;
      private bool[] H00MJ16_n591ContagemResultadoEvidencia_Data ;
      private String[] H00MJ16_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] H00MJ16_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] H00MJ16_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] H00MJ16_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private int[] H00MJ16_A645TipoDocumento_Codigo ;
      private bool[] H00MJ16_n645TipoDocumento_Codigo ;
      private int[] H00MJ16_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] H00MJ16_n1493ContagemResultadoEvidencia_Owner ;
      private String[] H00MJ16_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] H00MJ16_n588ContagemResultadoEvidencia_Arquivo ;
      private bool[] H00MJ17_A1635Servico_IsPublico ;
      private bool[] H00MJ17_A632Servico_Ativo ;
      private int[] H00MJ17_A157ServicoGrupo_Codigo ;
      private String[] H00MJ17_A158ServicoGrupo_Descricao ;
      private short[] H00MJ18_A1530Servico_TipoHierarquia ;
      private bool[] H00MJ18_n1530Servico_TipoHierarquia ;
      private int[] H00MJ18_A157ServicoGrupo_Codigo ;
      private bool[] H00MJ18_A1635Servico_IsPublico ;
      private bool[] H00MJ18_A632Servico_Ativo ;
      private int[] H00MJ18_A633Servico_UO ;
      private bool[] H00MJ18_n633Servico_UO ;
      private int[] H00MJ18_A155Servico_Codigo ;
      private String[] H00MJ18_A605Servico_Sigla ;
      private int[] H00MJ19_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MJ19_A63ContratanteUsuario_ContratanteCod ;
      private String[] H00MJ19_A129Sistema_Sigla ;
      private int[] H00MJ19_A127Sistema_Codigo ;
      private int[] H00MJ20_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] H00MJ20_A2003ContagemResultadoRequisito_OSCod ;
      private int[] H00MJ20_A2005ContagemResultadoRequisito_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV90Request ;
      private IGxSession AV21Websession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV93Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV135GXV8 ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV137GXV10 ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV155GXV17 ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV84Sdt_ContagemResultadoEvidencias ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Requisitos_SDT_RequisitosItem ))]
      private IGxCollection AV106SDT_Requisitos ;
      private GXWebForm Form ;
      private SdtAreaTrabalho AV28AreaTrabalho ;
      private SdtMessages_Message AV10Message ;
      private SdtContagemResultado AV89ContagemResultado ;
      private SdtContagemResultadoRequisito AV113ContagemResultadoRequisito ;
      private wwpbaseobjects.SdtWWPContext AV22WWPContext ;
      private SdtSolicitacaoServico AV82DadosDaSS ;
      private SdtSolicitacaoServico AV16SolicitacaoServico ;
      private SdtRequisito AV96Requisito ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV85Sdt_ArquivoEvidencia ;
      private SdtSDT_Requisitos_SDT_RequisitosItem AV107SDT_RequisitoItem ;
   }

   public class wp_ss__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MJ18( IGxContext context ,
                                              int AV17Usuario_CargoUOCod ,
                                              int A633Servico_UO ,
                                              bool A632Servico_Ativo ,
                                              bool A1635Servico_IsPublico ,
                                              short A1530Servico_TipoHierarquia ,
                                              int A157ServicoGrupo_Codigo ,
                                              int AV15ServicoGrupo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [2] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [Servico_TipoHierarquia], [ServicoGrupo_Codigo], [Servico_IsPublico], [Servico_Ativo], [Servico_UO], [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Servico_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ([Servico_IsPublico] = 1)";
         scmdbuf = scmdbuf + " and ([Servico_TipoHierarquia] = 1)";
         scmdbuf = scmdbuf + " and ([ServicoGrupo_Codigo] = @AV15ServicoGrupo_Codigo)";
         if ( ! (0==AV17Usuario_CargoUOCod) )
         {
            sWhereString = sWhereString + " and ([Servico_UO] = @AV17Usuario_CargoUOCod)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Servico_Sigla]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 12 :
                     return conditional_H00MJ18(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (short)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MJ2 ;
          prmH00MJ2 = new Object[] {
          new Object[] {"@AV22WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ3 ;
          prmH00MJ3 = new Object[] {
          new Object[] {"@AV114LinhaNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ6 ;
          prmH00MJ6 = new Object[] {
          new Object[] {"@AV22WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom3",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo3",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00MJ9 ;
          prmH00MJ9 = new Object[] {
          new Object[] {"@AV22WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MJ10 ;
          prmH00MJ10 = new Object[] {
          new Object[] {"@AV22WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MJ11 ;
          prmH00MJ11 = new Object[] {
          new Object[] {"@AV14Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ12 ;
          prmH00MJ12 = new Object[] {
          new Object[] {"@AV14Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ13 ;
          prmH00MJ13 = new Object[] {
          new Object[] {"@Servico_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ14 ;
          prmH00MJ14 = new Object[] {
          new Object[] {"@AV14Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ15 ;
          prmH00MJ15 = new Object[] {
          new Object[] {"@AV82Dado_2Contagemresultado_c",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ16 ;
          prmH00MJ16 = new Object[] {
          new Object[] {"@AV82Dado_2Contagemresultado_c",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ17 ;
          prmH00MJ17 = new Object[] {
          } ;
          Object[] prmH00MJ19 ;
          prmH00MJ19 = new Object[] {
          new Object[] {"@AV22WWPC_3Contratante_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MJ20 ;
          prmH00MJ20 = new Object[] {
          new Object[] {"@AV98Requisito_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16Soli_4Contagemresultado_c",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MJ18 ;
          prmH00MJ18 = new Object[] {
          new Object[] {"@AV15ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17Usuario_CargoUOCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MJ2", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV22WWPC_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ2,0,0,true,false )
             ,new CursorDef("H00MJ3", "SELECT [TipoRequisito_Codigo], [TipoRequisito_Identificador], [TipoRequisito_LinNegCod] FROM [TipoRequisito] WITH (NOLOCK) WHERE (@AV114LinhaNegocio_Codigo = convert(int, 0)) or [TipoRequisito_LinNegCod] = @AV114LinhaNegocio_Codigo ORDER BY [TipoRequisito_Identificador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ3,0,0,true,false )
             ,new CursorDef("H00MJ6", "SELECT * FROM (SELECT  T1.[ContagemResultado_Owner], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataCadastro], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Codigo], COALESCE( T2.[GXC1], 0) AS GXC1, COALESCE( T3.[GXC2], 0) AS GXC2, ROW_NUMBER() OVER ( ORDER BY T1.[ContagemResultado_StatusDmn] ) AS GX_ROW_NUMBER FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS GXC1, T5.[ContagemResultado_Codigo] FROM [ContagemResultadoRequisito] T4 WITH (NOLOCK),  [ContagemResultado] T5 WITH (NOLOCK) WHERE T4.[ContagemResultadoRequisito_OSCod] = T5.[ContagemResultado_Codigo] and T4.[ContagemResultadoRequisito_Owner] = 1 GROUP BY T5.[ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT COUNT(*) AS GXC2, [ContagemResultado_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_StatusDmn] = 'U') AND (T1.[ContagemResultado_Owner] = @AV22WWPContext__Userid)) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom3 AND @GXPagingTo3 OR @GXPagingTo3 < @GXPagingFrom3 AND GX_ROW_NUMBER >= @GXPagingFrom3",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ6,11,0,true,false )
             ,new CursorDef("H00MJ9", "SELECT COUNT(*) FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS GXC1, T5.[ContagemResultado_Codigo] FROM [ContagemResultadoRequisito] T4 WITH (NOLOCK),  [ContagemResultado] T5 WITH (NOLOCK) WHERE T4.[ContagemResultadoRequisito_OSCod] = T5.[ContagemResultado_Codigo] and T4.[ContagemResultadoRequisito_Owner] = 1 GROUP BY T5.[ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT COUNT(*) AS GXC2, [ContagemResultado_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_StatusDmn] = 'U') AND (T1.[ContagemResultado_Owner] = @AV22WWPContext__Userid) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ9,1,0,true,false )
             ,new CursorDef("H00MJ10", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T3.[Cargo_UOCod] AS Usuario_CargoUOCod, T4.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom FROM ((([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T4 WITH (NOLOCK) ON T4.[UnidadeOrganizacional_Codigo] = T3.[Cargo_UOCod]) WHERE T1.[Usuario_Codigo] = @AV22WWPContext__Userid ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ10,1,0,true,true )
             ,new CursorDef("H00MJ11", "SELECT TOP 1 T1.[Servico_Codigo], T1.[ServicoGrupo_Codigo], T1.[Servico_Sigla], T2.[ServicoGrupo_Descricao], T1.[Servico_LinNegCod] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo]) WHERE T1.[Servico_Codigo] = @AV14Servico_Codigo ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ11,1,0,true,true )
             ,new CursorDef("H00MJ12", "SELECT TOP 1 [Servico_LinNegCod], [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV14Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ12,1,0,true,true )
             ,new CursorDef("H00MJ13", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @Servico_Responsavel ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ13,1,0,true,true )
             ,new CursorDef("H00MJ14", "SELECT TOP 1 [ServicoGrupo_Codigo], [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV14Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ14,1,0,true,true )
             ,new CursorDef("H00MJ15", "SELECT T1.[ContagemResultadoRequisito_Codigo], T1.[ContagemResultadoRequisito_OSCod], T1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod, T2.[Requisito_Identificador], T2.[Requisito_Titulo], T2.[Requisito_Descricao], T2.[Requisito_Prioridade], T2.[Requisito_Pontuacao], T2.[Requisito_Status], T2.[Requisito_Agrupador] FROM ([ContagemResultadoRequisito] T1 WITH (NOLOCK) INNER JOIN [Requisito] T2 WITH (NOLOCK) ON T2.[Requisito_Codigo] = T1.[ContagemResultadoRequisito_ReqCod]) WHERE T1.[ContagemResultadoRequisito_OSCod] = @AV82Dado_2Contagemresultado_c ORDER BY T1.[ContagemResultadoRequisito_OSCod], T2.[Requisito_Agrupador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ15,100,0,true,false )
             ,new CursorDef("H00MJ16", "SELECT [ContagemResultado_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [TipoDocumento_Codigo], [ContagemResultadoEvidencia_Owner], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV82Dado_2Contagemresultado_c ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ16,100,0,true,false )
             ,new CursorDef("H00MJ17", "SELECT DISTINCT NULL AS [Servico_IsPublico], NULL AS [Servico_Ativo], [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM ( SELECT TOP(100) PERCENT T1.[Servico_IsPublico], T1.[Servico_Ativo], T1.[ServicoGrupo_Codigo], T2.[ServicoGrupo_Descricao] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo]) WHERE (T1.[Servico_Ativo] = 1) AND (T1.[Servico_IsPublico] = 1) ORDER BY T2.[ServicoGrupo_Descricao]) DistinctT ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ17,100,0,true,false )
             ,new CursorDef("H00MJ18", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ18,100,0,true,false )
             ,new CursorDef("H00MJ19", "SELECT T1.[ContratanteUsuario_UsuarioCod], T1.[ContratanteUsuario_ContratanteCod], T2.[Sistema_Sigla], T1.[Sistema_Codigo] FROM ([ContratanteUsuarioSistema] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV22WWPC_3Contratante_codigo and T1.[ContratanteUsuario_UsuarioCod] = @AV22WWPContext__Userid ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ19,100,0,true,false )
             ,new CursorDef("H00MJ20", "SELECT TOP 1 [ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod, [ContagemResultadoRequisito_OSCod], [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE ([ContagemResultadoRequisito_ReqCod] = @AV98Requisito_Codigo) AND ([ContagemResultadoRequisito_OSCod] = @AV16Soli_4Contagemresultado_c) ORDER BY [ContagemResultadoRequisito_ReqCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MJ20,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(11, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 11 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 15) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 25) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
