/*
               File: ContagemResultadoNotasWC
        Description: Contagem Resultado Notas WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:51.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadonotaswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadonotaswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadonotaswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultadoNota_DemandaCod )
      {
         this.AV16ContagemResultadoNota_DemandaCod = aP0_ContagemResultadoNota_DemandaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV16ContagemResultadoNota_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV16ContagemResultadoNota_DemandaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV16ContagemResultadoNota_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0)));
                  AV23Pgmname = GetNextPar( );
                  A1328ContagemResultadoNota_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1328ContagemResultadoNota_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1328ContagemResultadoNota_UsuarioCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A1857ContagemResultadoNotas_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n1857ContagemResultadoNotas_Ativo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1857ContagemResultadoNotas_Ativo", A1857ContagemResultadoNotas_Ativo);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16ContagemResultadoNota_DemandaCod, AV23Pgmname, A1328ContagemResultadoNota_UsuarioCod, AV6WWPContext, A1857ContagemResultadoNotas_Ativo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAFF2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV23Pgmname = "ContagemResultadoNotasWC";
               context.Gx_err = 0;
               WSFF2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Notas WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311765169");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadonotaswc.aspx") + "?" + UrlEncode("" +AV16ContagemResultadoNota_DemandaCod)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV16ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV16ContagemResultadoNota_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADONOTA_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV23Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTA_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1328ContagemResultadoNota_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTAS_ATIVO", A1857ContagemResultadoNotas_Ativo);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Nobuttoncaption", StringUtil.RTrim( Confirmpanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormFF2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadonotaswc.js", "?2020311765178");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoNotasWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Notas WC" ;
      }

      protected void WBFF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadonotaswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_FF2( true) ;
         }
         else
         {
            wb_table1_2_FF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNota_DemandaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1327ContagemResultadoNota_DemandaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1327ContagemResultadoNota_DemandaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNota_DemandaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoNota_DemandaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoNotasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoNotasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContagemResultadoNotasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
         }
         wbLoad = true;
      }

      protected void STARTFF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Notas WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPFF0( ) ;
            }
         }
      }

      protected void WSFF2( )
      {
         STARTFF2( ) ;
         EVTFF2( ) ;
      }

      protected void EVTFF2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11FF2 */
                                    E11FF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VINSERT.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12FF2 */
                                    E12FF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFF0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFF0( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              AV17Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV22Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV17Delete))));
                              A1331ContagemResultadoNota_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNota_Codigo_Internalname), ",", "."));
                              A1332ContagemResultadoNota_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoNota_DataHora_Internalname), 0);
                              A1330ContagemResultadoNota_UsuarioPesNom = StringUtil.Upper( cgiGet( edtContagemResultadoNota_UsuarioPesNom_Internalname));
                              n1330ContagemResultadoNota_UsuarioPesNom = false;
                              A1334ContagemResultadoNota_Nota = cgiGet( edtContagemResultadoNota_Nota_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13FF2 */
                                          E13FF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14FF2 */
                                          E14FF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15FF2 */
                                          E15FF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPFF0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormFF2( ) ;
            }
         }
      }

      protected void PAFF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV16ContagemResultadoNota_DemandaCod ,
                                       String AV23Pgmname ,
                                       int A1328ContagemResultadoNota_UsuarioCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       bool A1857ContagemResultadoNotas_Ativo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFFF2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTA_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1332ContagemResultadoNota_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTA_DATAHORA", context.localUtil.TToC( A1332ContagemResultadoNota_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTA_NOTA", GetSecureSignedToken( sPrefix, A1334ContagemResultadoNota_Nota));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTA_NOTA", A1334ContagemResultadoNota_Nota);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV23Pgmname = "ContagemResultadoNotasWC";
         context.Gx_err = 0;
      }

      protected void RFFF2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E14FF2 */
         E14FF2 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1327ContagemResultadoNota_DemandaCod ,
                                                 AV16ContagemResultadoNota_DemandaCod },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00FF2 */
            pr_default.execute(0, new Object[] {AV16ContagemResultadoNota_DemandaCod, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_5_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1329ContagemResultadoNota_UsuarioPesCod = H00FF2_A1329ContagemResultadoNota_UsuarioPesCod[0];
               n1329ContagemResultadoNota_UsuarioPesCod = H00FF2_n1329ContagemResultadoNota_UsuarioPesCod[0];
               A1328ContagemResultadoNota_UsuarioCod = H00FF2_A1328ContagemResultadoNota_UsuarioCod[0];
               A1857ContagemResultadoNotas_Ativo = H00FF2_A1857ContagemResultadoNotas_Ativo[0];
               n1857ContagemResultadoNotas_Ativo = H00FF2_n1857ContagemResultadoNotas_Ativo[0];
               A1327ContagemResultadoNota_DemandaCod = H00FF2_A1327ContagemResultadoNota_DemandaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1327ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1327ContagemResultadoNota_DemandaCod), 6, 0)));
               A1334ContagemResultadoNota_Nota = H00FF2_A1334ContagemResultadoNota_Nota[0];
               A1330ContagemResultadoNota_UsuarioPesNom = H00FF2_A1330ContagemResultadoNota_UsuarioPesNom[0];
               n1330ContagemResultadoNota_UsuarioPesNom = H00FF2_n1330ContagemResultadoNota_UsuarioPesNom[0];
               A1332ContagemResultadoNota_DataHora = H00FF2_A1332ContagemResultadoNota_DataHora[0];
               A1331ContagemResultadoNota_Codigo = H00FF2_A1331ContagemResultadoNota_Codigo[0];
               A1329ContagemResultadoNota_UsuarioPesCod = H00FF2_A1329ContagemResultadoNota_UsuarioPesCod[0];
               n1329ContagemResultadoNota_UsuarioPesCod = H00FF2_n1329ContagemResultadoNota_UsuarioPesCod[0];
               A1330ContagemResultadoNota_UsuarioPesNom = H00FF2_A1330ContagemResultadoNota_UsuarioPesNom[0];
               n1330ContagemResultadoNota_UsuarioPesNom = H00FF2_n1330ContagemResultadoNota_UsuarioPesNom[0];
               /* Execute user event: E15FF2 */
               E15FF2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 5;
            WBFF0( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1327ContagemResultadoNota_DemandaCod ,
                                              AV16ContagemResultadoNota_DemandaCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00FF3 */
         pr_default.execute(1, new Object[] {AV16ContagemResultadoNota_DemandaCod});
         GRID_nRecordCount = H00FF3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16ContagemResultadoNota_DemandaCod, AV23Pgmname, A1328ContagemResultadoNota_UsuarioCod, AV6WWPContext, A1857ContagemResultadoNotas_Ativo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16ContagemResultadoNota_DemandaCod, AV23Pgmname, A1328ContagemResultadoNota_UsuarioCod, AV6WWPContext, A1857ContagemResultadoNotas_Ativo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16ContagemResultadoNota_DemandaCod, AV23Pgmname, A1328ContagemResultadoNota_UsuarioCod, AV6WWPContext, A1857ContagemResultadoNotas_Ativo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16ContagemResultadoNota_DemandaCod, AV23Pgmname, A1328ContagemResultadoNota_UsuarioCod, AV6WWPContext, A1857ContagemResultadoNotas_Ativo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16ContagemResultadoNota_DemandaCod, AV23Pgmname, A1328ContagemResultadoNota_UsuarioCod, AV6WWPContext, A1857ContagemResultadoNotas_Ativo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPFF0( )
      {
         /* Before Start, stand alone formulas. */
         AV23Pgmname = "ContagemResultadoNotasWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13FF2 */
         E13FF2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV18Insert = cgiGet( imgavInsert_Internalname);
            A1327ContagemResultadoNota_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNota_DemandaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1327ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1327ContagemResultadoNota_DemandaCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOAV16ContagemResultadoNota_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV16ContagemResultadoNota_DemandaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Nobuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Nobuttoncaption");
            Confirmpanel_Result = cgiGet( sPrefix+"CONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13FF2 */
         E13FF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13FF2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtContagemResultadoNota_DemandaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNota_DemandaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNota_DemandaCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E14FF2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultadoNota_DataHora_Titleformat = 2;
         edtContagemResultadoNota_DataHora_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Data", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNota_DataHora_Internalname, "Title", edtContagemResultadoNota_DataHora_Title);
         edtContagemResultadoNota_Nota_Titleformat = 2;
         edtContagemResultadoNota_Nota_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Nota", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNota_Nota_Internalname, "Title", edtContagemResultadoNota_Nota_Title);
         edtContagemResultadoNota_UsuarioPesNom_Titleformat = 2;
         edtContagemResultadoNota_UsuarioPesNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNota_UsuarioPesNom_Internalname, "Title", edtContagemResultadoNota_UsuarioPesNom_Title);
         imgavInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgavInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavInsert_Visible), 5, 0)));
         AV18Insert = context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgavInsert_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Insert)) ? AV21Insert_GXI : context.convertURL( context.PathToRelativeUrl( AV18Insert))));
         AV21Insert_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgavInsert_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Insert)) ? AV21Insert_GXI : context.convertURL( context.PathToRelativeUrl( AV18Insert))));
         imgavInsert_Tooltiptext = "Inserir uma nota";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgavInsert_Internalname, "Tooltiptext", imgavInsert_Tooltiptext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      private void E15FF2( )
      {
         /* Grid_Load Routine */
         AV17Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV17Delete);
         AV22Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Desativar nota";
         edtavDelete_Visible = ((A1328ContagemResultadoNota_UsuarioCod==AV6WWPContext.gxTpr_Userid) ? 1 : 0);
         if ( A1857ContagemResultadoNotas_Ativo )
         {
            edtContagemResultadoNota_DataHora_Forecolor = GXUtil.RGB(0,0,0);
            edtContagemResultadoNota_UsuarioPesNom_Forecolor = GXUtil.RGB(0,0,0);
            edtContagemResultadoNota_Nota_Forecolor = GXUtil.RGB(0,0,0);
            edtContagemResultadoNota_UsuarioPesNom_Fontstrikethru = 0;
            edtContagemResultadoNota_Nota_Fontstrikethru = 0;
         }
         else
         {
            edtavDelete_Visible = 0;
            edtContagemResultadoNota_DataHora_Forecolor = GXUtil.RGB(128,0,0);
            edtContagemResultadoNota_UsuarioPesNom_Forecolor = GXUtil.RGB(128,0,0);
            edtContagemResultadoNota_Nota_Forecolor = GXUtil.RGB(128,0,0);
            edtContagemResultadoNota_UsuarioPesNom_Fontstrikethru = 1;
            edtContagemResultadoNota_Nota_Fontstrikethru = 1;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 5;
         }
         sendrow_52( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void E12FF2( )
      {
         /* Insert_Click Routine */
         context.PopUp(formatLink("wp_novanota.aspx") + "?" + UrlEncode("" +AV16ContagemResultadoNota_DemandaCod), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E11FF2( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            new prc_desativarnota(context ).execute( ref  A1331ContagemResultadoNota_Codigo) ;
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16ContagemResultadoNota_DemandaCod, AV23Pgmname, A1328ContagemResultadoNota_UsuarioCod, AV6WWPContext, A1857ContagemResultadoNotas_Ativo, sPrefix) ;
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV23Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV23Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV23Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV23Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         new wwpbaseobjects.savegridstate(context ).execute(  AV23Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV23Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoNotas";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultadoNota_DemandaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_FF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado Nota_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNota_DataHora_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNota_DataHora_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNota_DataHora_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNota_UsuarioPesNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNota_UsuarioPesNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNota_UsuarioPesNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(570), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNota_Nota_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNota_Nota_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNota_Nota_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Delete));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1331ContagemResultadoNota_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1332ContagemResultadoNota_DataHora, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNota_DataHora_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_DataHora_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_DataHora_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1330ContagemResultadoNota_UsuarioPesNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNota_UsuarioPesNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_UsuarioPesNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_UsuarioPesNom_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_UsuarioPesNom_Fontstrikethru), 1, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1334ContagemResultadoNota_Nota);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNota_Nota_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_Nota_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_Nota_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNota_Nota_Fontstrikethru), 1, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active Bitmap Variable */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV18Insert_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV18Insert))&&String.IsNullOrEmpty(StringUtil.RTrim( AV21Insert_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV18Insert)));
            GxWebStd.gx_bitmap( context, imgavInsert_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV18Insert)) ? AV21Insert_GXI : context.PathToRelativeUrl( AV18Insert)), "", "", "", context.GetTheme( ), imgavInsert_Visible, 1, "", imgavInsert_Tooltiptext, 0, -1, 0, "", 0, "", 0, 0, 5, imgavInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVINSERT.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV18Insert_IsBlob, false, "HLP_ContagemResultadoNotasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FF2e( true) ;
         }
         else
         {
            wb_table1_2_FF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV16ContagemResultadoNota_DemandaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFF2( ) ;
         WSFF2( ) ;
         WEFF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV16ContagemResultadoNota_DemandaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAFF2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadonotaswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAFF2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV16ContagemResultadoNota_DemandaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0)));
         }
         wcpOAV16ContagemResultadoNota_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV16ContagemResultadoNota_DemandaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV16ContagemResultadoNota_DemandaCod != wcpOAV16ContagemResultadoNota_DemandaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV16ContagemResultadoNota_DemandaCod = AV16ContagemResultadoNota_DemandaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV16ContagemResultadoNota_DemandaCod = cgiGet( sPrefix+"AV16ContagemResultadoNota_DemandaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV16ContagemResultadoNota_DemandaCod) > 0 )
         {
            AV16ContagemResultadoNota_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV16ContagemResultadoNota_DemandaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ContagemResultadoNota_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0)));
         }
         else
         {
            AV16ContagemResultadoNota_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV16ContagemResultadoNota_DemandaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAFF2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSFF2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSFF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV16ContagemResultadoNota_DemandaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ContagemResultadoNota_DemandaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV16ContagemResultadoNota_DemandaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV16ContagemResultadoNota_DemandaCod_CTRL", StringUtil.RTrim( sCtrlAV16ContagemResultadoNota_DemandaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEFF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311765243");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("contagemresultadonotaswc.js", "?2020311765243");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_idx;
         edtContagemResultadoNota_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_CODIGO_"+sGXsfl_5_idx;
         edtContagemResultadoNota_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_DATAHORA_"+sGXsfl_5_idx;
         edtContagemResultadoNota_UsuarioPesNom_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_USUARIOPESNOM_"+sGXsfl_5_idx;
         edtContagemResultadoNota_Nota_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_NOTA_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_fel_idx;
         edtContagemResultadoNota_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_CODIGO_"+sGXsfl_5_fel_idx;
         edtContagemResultadoNota_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_DATAHORA_"+sGXsfl_5_fel_idx;
         edtContagemResultadoNota_UsuarioPesNom_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_USUARIOPESNOM_"+sGXsfl_5_fel_idx;
         edtContagemResultadoNota_Nota_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_NOTA_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBFF0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_5_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 6,'"+sPrefix+"',false,'',5)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV17Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV22Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV22Delete_GXI : context.PathToRelativeUrl( AV17Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavDelete_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e16ff2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV17Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNota_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1331ContagemResultadoNota_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1331ContagemResultadoNota_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNota_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNota_DataHora_Internalname,context.localUtil.TToC( A1332ContagemResultadoNota_DataHora, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1332ContagemResultadoNota_DataHora, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNota_DataHora_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContagemResultadoNota_DataHora_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNota_UsuarioPesNom_Internalname,StringUtil.RTrim( A1330ContagemResultadoNota_UsuarioPesNom),StringUtil.RTrim( context.localUtil.Format( A1330ContagemResultadoNota_UsuarioPesNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNota_UsuarioPesNom_Jsonclick,(short)0,(String)"BootstrapAttribute",((edtContagemResultadoNota_UsuarioPesNom_Fontstrikethru==1) ? "text-decoration:line-through;" : "")+"color:"+context.BuildHTMLColor( edtContagemResultadoNota_UsuarioPesNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNota_Nota_Internalname,(String)A1334ContagemResultadoNota_Nota,(String)A1334ContagemResultadoNota_Nota,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNota_Nota_Jsonclick,(short)0,(String)"BootstrapAttribute",((edtContagemResultadoNota_Nota_Fontstrikethru==1) ? "text-decoration:line-through;" : "")+"color:"+context.BuildHTMLColor( edtContagemResultadoNota_Nota_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)570,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)5,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga2M",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTA_DATAHORA"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( A1332ContagemResultadoNota_DataHora, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTA_NOTA"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, A1334ContagemResultadoNota_Nota));
            GridContainer.AddRow(GridRow);
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContagemResultadoNota_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_CODIGO";
         edtContagemResultadoNota_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_DATAHORA";
         edtContagemResultadoNota_UsuarioPesNom_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_USUARIOPESNOM";
         edtContagemResultadoNota_Nota_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_NOTA";
         imgavInsert_Internalname = sPrefix+"vINSERT";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtContagemResultadoNota_DemandaCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTA_DEMANDACOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultadoNota_Nota_Jsonclick = "";
         edtContagemResultadoNota_UsuarioPesNom_Jsonclick = "";
         edtContagemResultadoNota_DataHora_Jsonclick = "";
         edtContagemResultadoNota_Codigo_Jsonclick = "";
         edtavDelete_Jsonclick = "";
         edtavDelete_Enabled = 1;
         imgavInsert_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemResultadoNota_Nota_Fontstrikethru = 0;
         edtContagemResultadoNota_Nota_Forecolor = (int)(0x000000);
         edtContagemResultadoNota_UsuarioPesNom_Fontstrikethru = 0;
         edtContagemResultadoNota_UsuarioPesNom_Forecolor = (int)(0x000000);
         edtContagemResultadoNota_DataHora_Forecolor = (int)(0x000000);
         edtavDelete_Tooltiptext = "";
         edtContagemResultadoNota_Nota_Titleformat = 0;
         edtContagemResultadoNota_UsuarioPesNom_Titleformat = 0;
         edtContagemResultadoNota_DataHora_Titleformat = 0;
         edtavDelete_Visible = -1;
         subGrid_Class = "WorkWith";
         imgavInsert_Tooltiptext = "";
         imgavInsert_Visible = 1;
         edtContagemResultadoNota_UsuarioPesNom_Title = "Usu�rio";
         edtContagemResultadoNota_Nota_Title = "Notas";
         edtContagemResultadoNota_DataHora_Title = "Data";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContagemResultadoNota_DemandaCod_Jsonclick = "";
         edtContagemResultadoNota_DemandaCod_Visible = 1;
         Confirmpanel_Nobuttoncaption = "N�o";
         Confirmpanel_Yesbuttoncaption = "Sim";
         Confirmpanel_Confirmationtext = "Confirma desativar esta Nota?";
         Confirmpanel_Title = "Aten��o";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16ContagemResultadoNota_DemandaCod',fld:'vCONTAGEMRESULTADONOTA_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNota_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoNota_DataHora_Title',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Title'},{av:'edtContagemResultadoNota_Nota_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Titleformat'},{av:'edtContagemResultadoNota_Nota_Title',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Title'},{av:'edtContagemResultadoNota_UsuarioPesNom_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Titleformat'},{av:'edtContagemResultadoNota_UsuarioPesNom_Title',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Title'},{av:'imgavInsert_Visible',ctrl:'vINSERT',prop:'Visible'},{av:'AV18Insert',fld:'vINSERT',pic:'',nv:''},{av:'imgavInsert_Tooltiptext',ctrl:'vINSERT',prop:'Tooltiptext'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E15FF2',iparms:[{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false}],oparms:[{av:'AV17Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtContagemResultadoNota_DataHora_Forecolor',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Forecolor'},{av:'edtContagemResultadoNota_UsuarioPesNom_Forecolor',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Forecolor'},{av:'edtContagemResultadoNota_Nota_Forecolor',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Forecolor'},{av:'edtContagemResultadoNota_UsuarioPesNom_Fontstrikethru',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Fontstrikethru'},{av:'edtContagemResultadoNota_Nota_Fontstrikethru',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Fontstrikethru'}]}");
         setEventMetadata("VDELETE.CLICK","{handler:'E16FF2',iparms:[],oparms:[]}");
         setEventMetadata("VINSERT.CLICK","{handler:'E12FF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16ContagemResultadoNota_DemandaCod',fld:'vCONTAGEMRESULTADONOTA_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11FF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16ContagemResultadoNota_DemandaCod',fld:'vCONTAGEMRESULTADONOTA_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'},{av:'A1331ContagemResultadoNota_Codigo',fld:'CONTAGEMRESULTADONOTA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A1331ContagemResultadoNota_Codigo',fld:'CONTAGEMRESULTADONOTA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16ContagemResultadoNota_DemandaCod',fld:'vCONTAGEMRESULTADONOTA_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNota_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoNota_DataHora_Title',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Title'},{av:'edtContagemResultadoNota_Nota_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Titleformat'},{av:'edtContagemResultadoNota_Nota_Title',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Title'},{av:'edtContagemResultadoNota_UsuarioPesNom_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Titleformat'},{av:'edtContagemResultadoNota_UsuarioPesNom_Title',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Title'},{av:'imgavInsert_Visible',ctrl:'vINSERT',prop:'Visible'},{av:'AV18Insert',fld:'vINSERT',pic:'',nv:''},{av:'imgavInsert_Tooltiptext',ctrl:'vINSERT',prop:'Tooltiptext'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16ContagemResultadoNota_DemandaCod',fld:'vCONTAGEMRESULTADONOTA_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNota_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoNota_DataHora_Title',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Title'},{av:'edtContagemResultadoNota_Nota_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Titleformat'},{av:'edtContagemResultadoNota_Nota_Title',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Title'},{av:'edtContagemResultadoNota_UsuarioPesNom_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Titleformat'},{av:'edtContagemResultadoNota_UsuarioPesNom_Title',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Title'},{av:'imgavInsert_Visible',ctrl:'vINSERT',prop:'Visible'},{av:'AV18Insert',fld:'vINSERT',pic:'',nv:''},{av:'imgavInsert_Tooltiptext',ctrl:'vINSERT',prop:'Tooltiptext'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16ContagemResultadoNota_DemandaCod',fld:'vCONTAGEMRESULTADONOTA_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNota_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoNota_DataHora_Title',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Title'},{av:'edtContagemResultadoNota_Nota_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Titleformat'},{av:'edtContagemResultadoNota_Nota_Title',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Title'},{av:'edtContagemResultadoNota_UsuarioPesNom_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Titleformat'},{av:'edtContagemResultadoNota_UsuarioPesNom_Title',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Title'},{av:'imgavInsert_Visible',ctrl:'vINSERT',prop:'Visible'},{av:'AV18Insert',fld:'vINSERT',pic:'',nv:''},{av:'imgavInsert_Tooltiptext',ctrl:'vINSERT',prop:'Tooltiptext'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16ContagemResultadoNota_DemandaCod',fld:'vCONTAGEMRESULTADONOTA_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1328ContagemResultadoNota_UsuarioCod',fld:'CONTAGEMRESULTADONOTA_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1857ContagemResultadoNotas_Ativo',fld:'CONTAGEMRESULTADONOTAS_ATIVO',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNota_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoNota_DataHora_Title',ctrl:'CONTAGEMRESULTADONOTA_DATAHORA',prop:'Title'},{av:'edtContagemResultadoNota_Nota_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Titleformat'},{av:'edtContagemResultadoNota_Nota_Title',ctrl:'CONTAGEMRESULTADONOTA_NOTA',prop:'Title'},{av:'edtContagemResultadoNota_UsuarioPesNom_Titleformat',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Titleformat'},{av:'edtContagemResultadoNota_UsuarioPesNom_Title',ctrl:'CONTAGEMRESULTADONOTA_USUARIOPESNOM',prop:'Title'},{av:'imgavInsert_Visible',ctrl:'vINSERT',prop:'Visible'},{av:'AV18Insert',fld:'vINSERT',pic:'',nv:''},{av:'imgavInsert_Tooltiptext',ctrl:'vINSERT',prop:'Tooltiptext'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Confirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV23Pgmname = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17Delete = "";
         AV22Delete_GXI = "";
         A1332ContagemResultadoNota_DataHora = (DateTime)(DateTime.MinValue);
         A1330ContagemResultadoNota_UsuarioPesNom = "";
         A1334ContagemResultadoNota_Nota = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00FF2_A1329ContagemResultadoNota_UsuarioPesCod = new int[1] ;
         H00FF2_n1329ContagemResultadoNota_UsuarioPesCod = new bool[] {false} ;
         H00FF2_A1328ContagemResultadoNota_UsuarioCod = new int[1] ;
         H00FF2_A1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         H00FF2_n1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         H00FF2_A1327ContagemResultadoNota_DemandaCod = new int[1] ;
         H00FF2_A1334ContagemResultadoNota_Nota = new String[] {""} ;
         H00FF2_A1330ContagemResultadoNota_UsuarioPesNom = new String[] {""} ;
         H00FF2_n1330ContagemResultadoNota_UsuarioPesNom = new bool[] {false} ;
         H00FF2_A1332ContagemResultadoNota_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00FF2_A1331ContagemResultadoNota_Codigo = new int[1] ;
         H00FF3_AGRID_nRecordCount = new long[1] ;
         AV18Insert = "";
         AV21Insert_GXI = "";
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         ClassString = "";
         StyleString = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV16ContagemResultadoNota_DemandaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadonotaswc__default(),
            new Object[][] {
                new Object[] {
               H00FF2_A1329ContagemResultadoNota_UsuarioPesCod, H00FF2_n1329ContagemResultadoNota_UsuarioPesCod, H00FF2_A1328ContagemResultadoNota_UsuarioCod, H00FF2_A1857ContagemResultadoNotas_Ativo, H00FF2_n1857ContagemResultadoNotas_Ativo, H00FF2_A1327ContagemResultadoNota_DemandaCod, H00FF2_A1334ContagemResultadoNota_Nota, H00FF2_A1330ContagemResultadoNota_UsuarioPesNom, H00FF2_n1330ContagemResultadoNota_UsuarioPesNom, H00FF2_A1332ContagemResultadoNota_DataHora,
               H00FF2_A1331ContagemResultadoNota_Codigo
               }
               , new Object[] {
               H00FF3_AGRID_nRecordCount
               }
            }
         );
         AV23Pgmname = "ContagemResultadoNotasWC";
         /* GeneXus formulas. */
         AV23Pgmname = "ContagemResultadoNotasWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoNota_DataHora_Titleformat ;
      private short edtContagemResultadoNota_Nota_Titleformat ;
      private short edtContagemResultadoNota_UsuarioPesNom_Titleformat ;
      private short edtContagemResultadoNota_UsuarioPesNom_Fontstrikethru ;
      private short edtContagemResultadoNota_Nota_Fontstrikethru ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV16ContagemResultadoNota_DemandaCod ;
      private int wcpOAV16ContagemResultadoNota_DemandaCod ;
      private int subGrid_Rows ;
      private int A1328ContagemResultadoNota_UsuarioCod ;
      private int A1327ContagemResultadoNota_DemandaCod ;
      private int edtContagemResultadoNota_DemandaCod_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int A1331ContagemResultadoNota_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A1329ContagemResultadoNota_UsuarioPesCod ;
      private int imgavInsert_Visible ;
      private int edtavDelete_Visible ;
      private int edtContagemResultadoNota_DataHora_Forecolor ;
      private int edtContagemResultadoNota_UsuarioPesNom_Forecolor ;
      private int edtContagemResultadoNota_Nota_Forecolor ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDelete_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Confirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String AV23Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Nobuttoncaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoNota_DemandaCod_Internalname ;
      private String edtContagemResultadoNota_DemandaCod_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDelete_Internalname ;
      private String edtContagemResultadoNota_Codigo_Internalname ;
      private String edtContagemResultadoNota_DataHora_Internalname ;
      private String A1330ContagemResultadoNota_UsuarioPesNom ;
      private String edtContagemResultadoNota_UsuarioPesNom_Internalname ;
      private String edtContagemResultadoNota_Nota_Internalname ;
      private String scmdbuf ;
      private String imgavInsert_Internalname ;
      private String edtContagemResultadoNota_DataHora_Title ;
      private String edtContagemResultadoNota_Nota_Title ;
      private String edtContagemResultadoNota_UsuarioPesNom_Title ;
      private String imgavInsert_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String ClassString ;
      private String StyleString ;
      private String imgavInsert_Jsonclick ;
      private String sCtrlAV16ContagemResultadoNota_DemandaCod ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String edtavDelete_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultadoNota_Codigo_Jsonclick ;
      private String edtContagemResultadoNota_DataHora_Jsonclick ;
      private String edtContagemResultadoNota_UsuarioPesNom_Jsonclick ;
      private String edtContagemResultadoNota_Nota_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private String Confirmpanel_Internalname ;
      private DateTime A1332ContagemResultadoNota_DataHora ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool A1857ContagemResultadoNotas_Ativo ;
      private bool n1857ContagemResultadoNotas_Ativo ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1330ContagemResultadoNota_UsuarioPesNom ;
      private bool n1329ContagemResultadoNota_UsuarioPesCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV18Insert_IsBlob ;
      private bool AV17Delete_IsBlob ;
      private String A1334ContagemResultadoNota_Nota ;
      private String AV22Delete_GXI ;
      private String AV21Insert_GXI ;
      private String AV17Delete ;
      private String AV18Insert ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00FF2_A1329ContagemResultadoNota_UsuarioPesCod ;
      private bool[] H00FF2_n1329ContagemResultadoNota_UsuarioPesCod ;
      private int[] H00FF2_A1328ContagemResultadoNota_UsuarioCod ;
      private bool[] H00FF2_A1857ContagemResultadoNotas_Ativo ;
      private bool[] H00FF2_n1857ContagemResultadoNotas_Ativo ;
      private int[] H00FF2_A1327ContagemResultadoNota_DemandaCod ;
      private String[] H00FF2_A1334ContagemResultadoNota_Nota ;
      private String[] H00FF2_A1330ContagemResultadoNota_UsuarioPesNom ;
      private bool[] H00FF2_n1330ContagemResultadoNota_UsuarioPesNom ;
      private DateTime[] H00FF2_A1332ContagemResultadoNota_DataHora ;
      private int[] H00FF2_A1331ContagemResultadoNota_Codigo ;
      private long[] H00FF3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemresultadonotaswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00FF2( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1327ContagemResultadoNota_DemandaCod ,
                                             int AV16ContagemResultadoNota_DemandaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Usuario_PessoaCod] AS ContagemResultadoNota_UsuarioPesCod, T1.[ContagemResultadoNota_UsuarioCod] AS ContagemResultadoNota_UsuarioCod, T1.[ContagemResultadoNotas_Ativo], T1.[ContagemResultadoNota_DemandaCod], T1.[ContagemResultadoNota_Nota], T3.[Pessoa_Nome] AS ContagemResultadoNota_UsuarioPesNom, T1.[ContagemResultadoNota_DataHora], T1.[ContagemResultadoNota_Codigo]";
         sFromString = " FROM (([ContagemResultadoNotas] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNota_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContagemResultadoNota_DemandaCod] = @AV16ContagemResultadoNota_DemandaCod)";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNota_DemandaCod], T1.[ContagemResultadoNota_DataHora]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNota_DemandaCod] DESC, T1.[ContagemResultadoNota_DataHora] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNota_DemandaCod], T1.[ContagemResultadoNota_Nota]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNota_DemandaCod] DESC, T1.[ContagemResultadoNota_Nota] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNota_DemandaCod], T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNota_DemandaCod] DESC, T3.[Pessoa_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNota_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00FF3( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1327ContagemResultadoNota_DemandaCod ,
                                             int AV16ContagemResultadoNota_DemandaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [1] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContagemResultadoNotas] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNota_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoNota_DemandaCod] = @AV16ContagemResultadoNota_DemandaCod)";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00FF2(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 1 :
                     return conditional_H00FF3(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FF2 ;
          prmH00FF2 = new Object[] {
          new Object[] {"@AV16ContagemResultadoNota_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00FF3 ;
          prmH00FF3 = new Object[] {
          new Object[] {"@AV16ContagemResultadoNota_DemandaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FF2,11,0,true,false )
             ,new CursorDef("H00FF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FF3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
