/*
               File: GetSistemaEvidenciasWCFilterData
        Description: Get Sistema Evidencias WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:40.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getsistemaevidenciaswcfilterdata : GXProcedure
   {
      public getsistemaevidenciaswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getsistemaevidenciaswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getsistemaevidenciaswcfilterdata objgetsistemaevidenciaswcfilterdata;
         objgetsistemaevidenciaswcfilterdata = new getsistemaevidenciaswcfilterdata();
         objgetsistemaevidenciaswcfilterdata.AV14DDOName = aP0_DDOName;
         objgetsistemaevidenciaswcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetsistemaevidenciaswcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetsistemaevidenciaswcfilterdata.AV18OptionsJson = "" ;
         objgetsistemaevidenciaswcfilterdata.AV21OptionsDescJson = "" ;
         objgetsistemaevidenciaswcfilterdata.AV23OptionIndexesJson = "" ;
         objgetsistemaevidenciaswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetsistemaevidenciaswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetsistemaevidenciaswcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getsistemaevidenciaswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFEVIDENCIA_NOMEARQOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("SistemaEvidenciasWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "SistemaEvidenciasWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("SistemaEvidenciasWCGridState"), "");
         }
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV33GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
            {
               AV10TFFuncaoAPFEvidencia_NomeArq = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV11TFFuncaoAPFEvidencia_NomeArq_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAOAPF_SISTEMACOD") == 0 )
            {
               AV30FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV33GXV1 = (int)(AV33GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPFEVIDENCIA_NOMEARQOPTIONS' Routine */
         AV10TFFuncaoAPFEvidencia_NomeArq = AV12SearchTxt;
         AV11TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                              AV10TFFuncaoAPFEvidencia_NomeArq ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A360FuncaoAPF_SistemaCod ,
                                              AV30FuncaoAPF_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV10TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_NomeArq), 50, "%");
         /* Using cursor P00FY2 */
         pr_default.execute(0, new Object[] {AV30FuncaoAPF_SistemaCod, lV10TFFuncaoAPFEvidencia_NomeArq, AV11TFFuncaoAPFEvidencia_NomeArq_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A165FuncaoAPF_Codigo = P00FY2_A165FuncaoAPF_Codigo[0];
            A409FuncaoAPFEvidencia_NomeArq = P00FY2_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P00FY2_n409FuncaoAPFEvidencia_NomeArq[0];
            A360FuncaoAPF_SistemaCod = P00FY2_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = P00FY2_n360FuncaoAPF_SistemaCod[0];
            A406FuncaoAPFEvidencia_Codigo = P00FY2_A406FuncaoAPFEvidencia_Codigo[0];
            A360FuncaoAPF_SistemaCod = P00FY2_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = P00FY2_n360FuncaoAPF_SistemaCod[0];
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq)) )
            {
               AV16Option = A409FuncaoAPFEvidencia_NomeArq;
               AV15InsertIndex = 1;
               while ( ( AV15InsertIndex <= AV17Options.Count ) && ( StringUtil.StrCmp(((String)AV17Options.Item(AV15InsertIndex)), AV16Option) < 0 ) )
               {
                  AV15InsertIndex = (int)(AV15InsertIndex+1);
               }
               if ( ( AV15InsertIndex <= AV17Options.Count ) && ( StringUtil.StrCmp(((String)AV17Options.Item(AV15InsertIndex)), AV16Option) == 0 ) )
               {
                  AV24count = (long)(NumberUtil.Val( ((String)AV22OptionIndexes.Item(AV15InsertIndex)), "."));
                  AV24count = (long)(AV24count+1);
                  AV22OptionIndexes.RemoveItem(AV15InsertIndex);
                  AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), AV15InsertIndex);
               }
               else
               {
                  AV17Options.Add(AV16Option, AV15InsertIndex);
                  AV22OptionIndexes.Add("1", AV15InsertIndex);
               }
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoAPFEvidencia_NomeArq = "";
         AV11TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         scmdbuf = "";
         lV10TFFuncaoAPFEvidencia_NomeArq = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         P00FY2_A165FuncaoAPF_Codigo = new int[1] ;
         P00FY2_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P00FY2_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P00FY2_A360FuncaoAPF_SistemaCod = new int[1] ;
         P00FY2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P00FY2_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getsistemaevidenciaswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FY2_A165FuncaoAPF_Codigo, P00FY2_A409FuncaoAPFEvidencia_NomeArq, P00FY2_n409FuncaoAPFEvidencia_NomeArq, P00FY2_A360FuncaoAPF_SistemaCod, P00FY2_n360FuncaoAPF_SistemaCod, P00FY2_A406FuncaoAPFEvidencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV33GXV1 ;
      private int AV30FuncaoAPF_SistemaCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int AV15InsertIndex ;
      private long AV24count ;
      private String AV10TFFuncaoAPFEvidencia_NomeArq ;
      private String AV11TFFuncaoAPFEvidencia_NomeArq_Sel ;
      private String scmdbuf ;
      private String lV10TFFuncaoAPFEvidencia_NomeArq ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private bool returnInSub ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool n360FuncaoAPF_SistemaCod ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FY2_A165FuncaoAPF_Codigo ;
      private String[] P00FY2_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P00FY2_n409FuncaoAPFEvidencia_NomeArq ;
      private int[] P00FY2_A360FuncaoAPF_SistemaCod ;
      private bool[] P00FY2_n360FuncaoAPF_SistemaCod ;
      private int[] P00FY2_A406FuncaoAPFEvidencia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
   }

   public class getsistemaevidenciaswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FY2( IGxContext context ,
                                             String AV11TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV10TFFuncaoAPFEvidencia_NomeArq ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             int A360FuncaoAPF_SistemaCod ,
                                             int AV30FuncaoAPF_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFEvidencia_NomeArq], T2.[FuncaoAPF_SistemaCod], T1.[FuncaoAPFEvidencia_Codigo] FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[FuncaoAPF_SistemaCod] = @AV30FuncaoAPF_SistemaCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] like @lV10TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] = @AV11TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPFEvidencia_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FY2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FY2 ;
          prmP00FY2 = new Object[] {
          new Object[] {"@AV30FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FY2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getsistemaevidenciaswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getsistemaevidenciaswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getsistemaevidenciaswcfilterdata") )
          {
             return  ;
          }
          getsistemaevidenciaswcfilterdata worker = new getsistemaevidenciaswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
