/*
               File: PRC_ExisteConferencia
        Description: PRC_Existe Conferencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:11.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existeconferencia : GXProcedure
   {
      public prc_existeconferencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existeconferencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           out bool aP1_Contratante_ExisteConferencia )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8Contratante_ExisteConferencia = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Contratante_ExisteConferencia=this.AV8Contratante_ExisteConferencia;
      }

      public bool executeUdp( ref int aP0_AreaTrabalho_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8Contratante_ExisteConferencia = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Contratante_ExisteConferencia=this.AV8Contratante_ExisteConferencia;
         return AV8Contratante_ExisteConferencia ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 out bool aP1_Contratante_ExisteConferencia )
      {
         prc_existeconferencia objprc_existeconferencia;
         objprc_existeconferencia = new prc_existeconferencia();
         objprc_existeconferencia.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_existeconferencia.AV8Contratante_ExisteConferencia = false ;
         objprc_existeconferencia.context.SetSubmitInitialConfig(context);
         objprc_existeconferencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existeconferencia);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Contratante_ExisteConferencia=this.AV8Contratante_ExisteConferencia;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existeconferencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004B2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P004B2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P004B2_n29Contratante_Codigo[0];
            A594Contratante_ExisteConferencia = P004B2_A594Contratante_ExisteConferencia[0];
            A594Contratante_ExisteConferencia = P004B2_A594Contratante_ExisteConferencia[0];
            AV8Contratante_ExisteConferencia = A594Contratante_ExisteConferencia;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004B2_A29Contratante_Codigo = new int[1] ;
         P004B2_n29Contratante_Codigo = new bool[] {false} ;
         P004B2_A5AreaTrabalho_Codigo = new int[1] ;
         P004B2_A594Contratante_ExisteConferencia = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existeconferencia__default(),
            new Object[][] {
                new Object[] {
               P004B2_A29Contratante_Codigo, P004B2_n29Contratante_Codigo, P004B2_A5AreaTrabalho_Codigo, P004B2_A594Contratante_ExisteConferencia
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV8Contratante_ExisteConferencia ;
      private bool n29Contratante_Codigo ;
      private bool A594Contratante_ExisteConferencia ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004B2_A29Contratante_Codigo ;
      private bool[] P004B2_n29Contratante_Codigo ;
      private int[] P004B2_A5AreaTrabalho_Codigo ;
      private bool[] P004B2_A594Contratante_ExisteConferencia ;
      private bool aP1_Contratante_ExisteConferencia ;
   }

   public class prc_existeconferencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004B2 ;
          prmP004B2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004B2", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_ExisteConferencia] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004B2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
