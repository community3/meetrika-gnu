/*
               File: WWParametrosSistema
        Description:  Parametros Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:37:52.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwparametrossistema : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwparametrossistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwparametrossistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbParametrosSistema_PadronizarStrings = new GXCombobox();
         cmbParametrosSistema_LicensiadoCadastrado = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_33 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_33_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_33_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV61TFParametrosSistema_NomeSistema = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFParametrosSistema_NomeSistema", AV61TFParametrosSistema_NomeSistema);
               AV62TFParametrosSistema_NomeSistema_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFParametrosSistema_NomeSistema_Sel", AV62TFParametrosSistema_NomeSistema_Sel);
               AV65TFParametrosSistema_PadronizarStrings_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFParametrosSistema_PadronizarStrings_Sel", StringUtil.Str( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0));
               AV68TFParametrosSistema_FatorAjuste = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV68TFParametrosSistema_FatorAjuste, 6, 2)));
               AV69TFParametrosSistema_FatorAjuste_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFParametrosSistema_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV69TFParametrosSistema_FatorAjuste_To, 6, 2)));
               AV72TFParametrosSistema_LicensiadoCadastrado_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFParametrosSistema_LicensiadoCadastrado_Sel", StringUtil.Str( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0));
               AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace", AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace);
               AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace", AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace);
               AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace", AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace);
               AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace", AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace);
               AV87Pgmname = GetNextPar( );
               A330ParametrosSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61TFParametrosSistema_NomeSistema, AV62TFParametrosSistema_NomeSistema_Sel, AV65TFParametrosSistema_PadronizarStrings_Sel, AV68TFParametrosSistema_FatorAjuste, AV69TFParametrosSistema_FatorAjuste_To, AV72TFParametrosSistema_LicensiadoCadastrado_Sel, AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, AV87Pgmname, A330ParametrosSistema_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA8I2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START8I2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117375248");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwparametrossistema.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSSISTEMA_NOMESISTEMA", StringUtil.RTrim( AV61TFParametrosSistema_NomeSistema));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL", StringUtil.RTrim( AV62TFParametrosSistema_NomeSistema_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSSISTEMA_FATORAJUSTE", StringUtil.LTrim( StringUtil.NToC( AV68TFParametrosSistema_FatorAjuste, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSSISTEMA_FATORAJUSTE_TO", StringUtil.LTrim( StringUtil.NToC( AV69TFParametrosSistema_FatorAjuste_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_33", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_33), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV74DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV74DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSSISTEMA_NOMESISTEMATITLEFILTERDATA", AV60ParametrosSistema_NomeSistemaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSSISTEMA_NOMESISTEMATITLEFILTERDATA", AV60ParametrosSistema_NomeSistemaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSSISTEMA_PADRONIZARSTRINGSTITLEFILTERDATA", AV64ParametrosSistema_PadronizarStringsTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSSISTEMA_PADRONIZARSTRINGSTITLEFILTERDATA", AV64ParametrosSistema_PadronizarStringsTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSSISTEMA_FATORAJUSTETITLEFILTERDATA", AV67ParametrosSistema_FatorAjusteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSSISTEMA_FATORAJUSTETITLEFILTERDATA", AV67ParametrosSistema_FatorAjusteTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLEFILTERDATA", AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLEFILTERDATA", AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV87Pgmname));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Caption", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Tooltip", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Cls", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filteredtext_set", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Selectedvalue_set", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includesortasc", StringUtil.BoolToStr( Ddo_parametrossistema_nomesistema_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrossistema_nomesistema_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Sortedstatus", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includefilter", StringUtil.BoolToStr( Ddo_parametrossistema_nomesistema_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filtertype", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filterisrange", StringUtil.BoolToStr( Ddo_parametrossistema_nomesistema_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includedatalist", StringUtil.BoolToStr( Ddo_parametrossistema_nomesistema_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Datalisttype", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Datalistproc", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_parametrossistema_nomesistema_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Sortasc", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Sortdsc", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Loadingdata", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Cleanfilter", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Noresultsfound", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Searchbuttontext", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Caption", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Tooltip", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Cls", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Selectedvalue_set", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includesortasc", StringUtil.BoolToStr( Ddo_parametrossistema_padronizarstrings_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrossistema_padronizarstrings_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Sortedstatus", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includefilter", StringUtil.BoolToStr( Ddo_parametrossistema_padronizarstrings_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includedatalist", StringUtil.BoolToStr( Ddo_parametrossistema_padronizarstrings_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Datalisttype", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Datalistfixedvalues", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Sortasc", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Sortdsc", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Cleanfilter", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Searchbuttontext", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Caption", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Tooltip", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Cls", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtext_set", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtextto_set", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includesortasc", StringUtil.BoolToStr( Ddo_parametrossistema_fatorajuste_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrossistema_fatorajuste_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Sortedstatus", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includefilter", StringUtil.BoolToStr( Ddo_parametrossistema_fatorajuste_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filtertype", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filterisrange", StringUtil.BoolToStr( Ddo_parametrossistema_fatorajuste_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includedatalist", StringUtil.BoolToStr( Ddo_parametrossistema_fatorajuste_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Sortasc", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Sortdsc", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Cleanfilter", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Rangefilterfrom", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Rangefilterto", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Searchbuttontext", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Caption", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Tooltip", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Cls", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Selectedvalue_set", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includesortasc", StringUtil.BoolToStr( Ddo_parametrossistema_licensiadocadastrado_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrossistema_licensiadocadastrado_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Sortedstatus", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includefilter", StringUtil.BoolToStr( Ddo_parametrossistema_licensiadocadastrado_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includedatalist", StringUtil.BoolToStr( Ddo_parametrossistema_licensiadocadastrado_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Datalisttype", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Datalistfixedvalues", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Sortasc", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Sortdsc", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Cleanfilter", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Searchbuttontext", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Activeeventkey", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filteredtext_get", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Selectedvalue_get", StringUtil.RTrim( Ddo_parametrossistema_nomesistema_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Activeeventkey", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Selectedvalue_get", StringUtil.RTrim( Ddo_parametrossistema_padronizarstrings_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Activeeventkey", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtext_get", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtextto_get", StringUtil.RTrim( Ddo_parametrossistema_fatorajuste_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Activeeventkey", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Selectedvalue_get", StringUtil.RTrim( Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE8I2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT8I2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwparametrossistema.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWParametrosSistema" ;
      }

      public override String GetPgmdesc( )
      {
         return " Parametros Sistema" ;
      }

      protected void WB8I0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_8I2( true) ;
         }
         else
         {
            wb_table1_2_8I2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8I2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrossistema_nomesistema_Internalname, StringUtil.RTrim( AV61TFParametrosSistema_NomeSistema), StringUtil.RTrim( context.localUtil.Format( AV61TFParametrosSistema_NomeSistema, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrossistema_nomesistema_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrossistema_nomesistema_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrossistema_nomesistema_sel_Internalname, StringUtil.RTrim( AV62TFParametrosSistema_NomeSistema_Sel), StringUtil.RTrim( context.localUtil.Format( AV62TFParametrosSistema_NomeSistema_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrossistema_nomesistema_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrossistema_nomesistema_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrossistema_padronizarstrings_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrossistema_padronizarstrings_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrossistema_padronizarstrings_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWParametrosSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrossistema_fatorajuste_Internalname, StringUtil.LTrim( StringUtil.NToC( AV68TFParametrosSistema_FatorAjuste, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV68TFParametrosSistema_FatorAjuste, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrossistema_fatorajuste_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrossistema_fatorajuste_Visible, 1, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWParametrosSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrossistema_fatorajuste_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV69TFParametrosSistema_FatorAjuste_To, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV69TFParametrosSistema_FatorAjuste_To, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrossistema_fatorajuste_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrossistema_fatorajuste_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWParametrosSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrossistema_licensiadocadastrado_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrossistema_licensiadocadastrado_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrossistema_licensiadocadastrado_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWParametrosSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSSISTEMA_NOMESISTEMAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_33_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Internalname, AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_33_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Internalname, AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSSISTEMA_FATORAJUSTEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_33_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Internalname, AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_33_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Internalname, AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosSistema.htm");
         }
         wbLoad = true;
      }

      protected void START8I2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Parametros Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8I0( ) ;
      }

      protected void WS8I2( )
      {
         START8I2( ) ;
         EVT8I2( ) ;
      }

      protected void EVT8I2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E118I2 */
                              E118I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSSISTEMA_NOMESISTEMA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E128I2 */
                              E128I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E138I2 */
                              E138I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSSISTEMA_FATORAJUSTE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E148I2 */
                              E148I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E158I2 */
                              E158I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E168I2 */
                              E168I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_33_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
                              SubsflControlProps_332( ) ;
                              AV29Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV86Update_GXI : context.convertURL( context.PathToRelativeUrl( AV29Update))));
                              A330ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtParametrosSistema_Codigo_Internalname), ",", "."));
                              A331ParametrosSistema_NomeSistema = StringUtil.Upper( cgiGet( edtParametrosSistema_NomeSistema_Internalname));
                              cmbParametrosSistema_PadronizarStrings.Name = cmbParametrosSistema_PadronizarStrings_Internalname;
                              cmbParametrosSistema_PadronizarStrings.CurrentValue = cgiGet( cmbParametrosSistema_PadronizarStrings_Internalname);
                              A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cgiGet( cmbParametrosSistema_PadronizarStrings_Internalname));
                              A708ParametrosSistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtParametrosSistema_FatorAjuste_Internalname), ",", ".");
                              n708ParametrosSistema_FatorAjuste = false;
                              cmbParametrosSistema_LicensiadoCadastrado.Name = cmbParametrosSistema_LicensiadoCadastrado_Internalname;
                              cmbParametrosSistema_LicensiadoCadastrado.CurrentValue = cgiGet( cmbParametrosSistema_LicensiadoCadastrado_Internalname);
                              A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cgiGet( cmbParametrosSistema_LicensiadoCadastrado_Internalname));
                              n399ParametrosSistema_LicensiadoCadastrado = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E178I2 */
                                    E178I2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E188I2 */
                                    E188I2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E198I2 */
                                    E198I2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrossistema_nomesistema Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSSISTEMA_NOMESISTEMA"), AV61TFParametrosSistema_NomeSistema) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrossistema_nomesistema_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL"), AV62TFParametrosSistema_NomeSistema_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrossistema_padronizarstrings_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL"), ",", ".") != Convert.ToDecimal( AV65TFParametrosSistema_PadronizarStrings_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrossistema_fatorajuste Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_FATORAJUSTE"), ",", ".") != AV68TFParametrosSistema_FatorAjuste )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrossistema_fatorajuste_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_FATORAJUSTE_TO"), ",", ".") != AV69TFParametrosSistema_FatorAjuste_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrossistema_licensiadocadastrado_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL"), ",", ".") != Convert.ToDecimal( AV72TFParametrosSistema_LicensiadoCadastrado_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8I2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA8I2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            GXCCtl = "PARAMETROSSISTEMA_PADRONIZARSTRINGS_" + sGXsfl_33_idx;
            cmbParametrosSistema_PadronizarStrings.Name = GXCCtl;
            cmbParametrosSistema_PadronizarStrings.WebTags = "";
            cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbParametrosSistema_PadronizarStrings.ItemCount > 0 )
            {
               A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cmbParametrosSistema_PadronizarStrings.getValidValue(StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings)));
            }
            GXCCtl = "PARAMETROSSISTEMA_LICENSIADOCADASTRADO_" + sGXsfl_33_idx;
            cmbParametrosSistema_LicensiadoCadastrado.Name = GXCCtl;
            cmbParametrosSistema_LicensiadoCadastrado.WebTags = "";
            cmbParametrosSistema_LicensiadoCadastrado.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbParametrosSistema_LicensiadoCadastrado.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbParametrosSistema_LicensiadoCadastrado.ItemCount > 0 )
            {
               A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cmbParametrosSistema_LicensiadoCadastrado.getValidValue(StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado)));
               n399ParametrosSistema_LicensiadoCadastrado = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_332( ) ;
         while ( nGXsfl_33_idx <= nRC_GXsfl_33 )
         {
            sendrow_332( ) ;
            nGXsfl_33_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_33_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_33_idx+1));
            sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
            SubsflControlProps_332( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV61TFParametrosSistema_NomeSistema ,
                                       String AV62TFParametrosSistema_NomeSistema_Sel ,
                                       short AV65TFParametrosSistema_PadronizarStrings_Sel ,
                                       decimal AV68TFParametrosSistema_FatorAjuste ,
                                       decimal AV69TFParametrosSistema_FatorAjuste_To ,
                                       short AV72TFParametrosSistema_LicensiadoCadastrado_Sel ,
                                       String AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace ,
                                       String AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace ,
                                       String AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace ,
                                       String AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace ,
                                       String AV87Pgmname ,
                                       int A330ParametrosSistema_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF8I2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A330ParametrosSistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_NOMESISTEMA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!"))));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_NOMESISTEMA", StringUtil.RTrim( A331ParametrosSistema_NomeSistema));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_PADRONIZARSTRINGS", GetSecureSignedToken( "", A417ParametrosSistema_PadronizarStrings));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_PADRONIZARSTRINGS", StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_FATORAJUSTE", GetSecureSignedToken( "", context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_FATORAJUSTE", StringUtil.LTrim( StringUtil.NToC( A708ParametrosSistema_FatorAjuste, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_LICENSIADOCADASTRADO", GetSecureSignedToken( "", A399ParametrosSistema_LicensiadoCadastrado));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_LICENSIADOCADASTRADO", StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8I2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV87Pgmname = "WWParametrosSistema";
         context.Gx_err = 0;
      }

      protected void RF8I2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 33;
         /* Execute user event: E188I2 */
         E188I2 ();
         nGXsfl_33_idx = 1;
         sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
         SubsflControlProps_332( ) ;
         nGXsfl_33_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_332( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel ,
                                                 AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema ,
                                                 AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel ,
                                                 AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste ,
                                                 AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to ,
                                                 AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel ,
                                                 A331ParametrosSistema_NomeSistema ,
                                                 A417ParametrosSistema_PadronizarStrings ,
                                                 A708ParametrosSistema_FatorAjuste ,
                                                 A399ParametrosSistema_LicensiadoCadastrado ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = StringUtil.PadR( StringUtil.RTrim( AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema), 50, "%");
            /* Using cursor H008I2 */
            pr_default.execute(0, new Object[] {lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema, AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel, AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste, AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_33_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A399ParametrosSistema_LicensiadoCadastrado = H008I2_A399ParametrosSistema_LicensiadoCadastrado[0];
               n399ParametrosSistema_LicensiadoCadastrado = H008I2_n399ParametrosSistema_LicensiadoCadastrado[0];
               A708ParametrosSistema_FatorAjuste = H008I2_A708ParametrosSistema_FatorAjuste[0];
               n708ParametrosSistema_FatorAjuste = H008I2_n708ParametrosSistema_FatorAjuste[0];
               A417ParametrosSistema_PadronizarStrings = H008I2_A417ParametrosSistema_PadronizarStrings[0];
               A331ParametrosSistema_NomeSistema = H008I2_A331ParametrosSistema_NomeSistema[0];
               A330ParametrosSistema_Codigo = H008I2_A330ParametrosSistema_Codigo[0];
               /* Execute user event: E198I2 */
               E198I2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 33;
            WB8I0( ) ;
         }
         nGXsfl_33_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = AV61TFParametrosSistema_NomeSistema;
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = AV62TFParametrosSistema_NomeSistema_Sel;
         AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel = AV65TFParametrosSistema_PadronizarStrings_Sel;
         AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste = AV68TFParametrosSistema_FatorAjuste;
         AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to = AV69TFParametrosSistema_FatorAjuste_To;
         AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel = AV72TFParametrosSistema_LicensiadoCadastrado_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel ,
                                              AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema ,
                                              AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel ,
                                              AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste ,
                                              AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to ,
                                              AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel ,
                                              A331ParametrosSistema_NomeSistema ,
                                              A417ParametrosSistema_PadronizarStrings ,
                                              A708ParametrosSistema_FatorAjuste ,
                                              A399ParametrosSistema_LicensiadoCadastrado ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = StringUtil.PadR( StringUtil.RTrim( AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema), 50, "%");
         /* Using cursor H008I3 */
         pr_default.execute(1, new Object[] {lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema, AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel, AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste, AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to});
         GRID_nRecordCount = H008I3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = AV61TFParametrosSistema_NomeSistema;
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = AV62TFParametrosSistema_NomeSistema_Sel;
         AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel = AV65TFParametrosSistema_PadronizarStrings_Sel;
         AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste = AV68TFParametrosSistema_FatorAjuste;
         AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to = AV69TFParametrosSistema_FatorAjuste_To;
         AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel = AV72TFParametrosSistema_LicensiadoCadastrado_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61TFParametrosSistema_NomeSistema, AV62TFParametrosSistema_NomeSistema_Sel, AV65TFParametrosSistema_PadronizarStrings_Sel, AV68TFParametrosSistema_FatorAjuste, AV69TFParametrosSistema_FatorAjuste_To, AV72TFParametrosSistema_LicensiadoCadastrado_Sel, AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, AV87Pgmname, A330ParametrosSistema_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = AV61TFParametrosSistema_NomeSistema;
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = AV62TFParametrosSistema_NomeSistema_Sel;
         AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel = AV65TFParametrosSistema_PadronizarStrings_Sel;
         AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste = AV68TFParametrosSistema_FatorAjuste;
         AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to = AV69TFParametrosSistema_FatorAjuste_To;
         AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel = AV72TFParametrosSistema_LicensiadoCadastrado_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61TFParametrosSistema_NomeSistema, AV62TFParametrosSistema_NomeSistema_Sel, AV65TFParametrosSistema_PadronizarStrings_Sel, AV68TFParametrosSistema_FatorAjuste, AV69TFParametrosSistema_FatorAjuste_To, AV72TFParametrosSistema_LicensiadoCadastrado_Sel, AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, AV87Pgmname, A330ParametrosSistema_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = AV61TFParametrosSistema_NomeSistema;
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = AV62TFParametrosSistema_NomeSistema_Sel;
         AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel = AV65TFParametrosSistema_PadronizarStrings_Sel;
         AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste = AV68TFParametrosSistema_FatorAjuste;
         AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to = AV69TFParametrosSistema_FatorAjuste_To;
         AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel = AV72TFParametrosSistema_LicensiadoCadastrado_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61TFParametrosSistema_NomeSistema, AV62TFParametrosSistema_NomeSistema_Sel, AV65TFParametrosSistema_PadronizarStrings_Sel, AV68TFParametrosSistema_FatorAjuste, AV69TFParametrosSistema_FatorAjuste_To, AV72TFParametrosSistema_LicensiadoCadastrado_Sel, AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, AV87Pgmname, A330ParametrosSistema_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = AV61TFParametrosSistema_NomeSistema;
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = AV62TFParametrosSistema_NomeSistema_Sel;
         AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel = AV65TFParametrosSistema_PadronizarStrings_Sel;
         AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste = AV68TFParametrosSistema_FatorAjuste;
         AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to = AV69TFParametrosSistema_FatorAjuste_To;
         AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel = AV72TFParametrosSistema_LicensiadoCadastrado_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61TFParametrosSistema_NomeSistema, AV62TFParametrosSistema_NomeSistema_Sel, AV65TFParametrosSistema_PadronizarStrings_Sel, AV68TFParametrosSistema_FatorAjuste, AV69TFParametrosSistema_FatorAjuste_To, AV72TFParametrosSistema_LicensiadoCadastrado_Sel, AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, AV87Pgmname, A330ParametrosSistema_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = AV61TFParametrosSistema_NomeSistema;
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = AV62TFParametrosSistema_NomeSistema_Sel;
         AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel = AV65TFParametrosSistema_PadronizarStrings_Sel;
         AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste = AV68TFParametrosSistema_FatorAjuste;
         AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to = AV69TFParametrosSistema_FatorAjuste_To;
         AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel = AV72TFParametrosSistema_LicensiadoCadastrado_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61TFParametrosSistema_NomeSistema, AV62TFParametrosSistema_NomeSistema_Sel, AV65TFParametrosSistema_PadronizarStrings_Sel, AV68TFParametrosSistema_FatorAjuste, AV69TFParametrosSistema_FatorAjuste_To, AV72TFParametrosSistema_LicensiadoCadastrado_Sel, AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, AV87Pgmname, A330ParametrosSistema_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP8I0( )
      {
         /* Before Start, stand alone formulas. */
         AV87Pgmname = "WWParametrosSistema";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E178I2 */
         E178I2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV74DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSSISTEMA_NOMESISTEMATITLEFILTERDATA"), AV60ParametrosSistema_NomeSistemaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSSISTEMA_PADRONIZARSTRINGSTITLEFILTERDATA"), AV64ParametrosSistema_PadronizarStringsTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSSISTEMA_FATORAJUSTETITLEFILTERDATA"), AV67ParametrosSistema_FatorAjusteTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLEFILTERDATA"), AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            AV61TFParametrosSistema_NomeSistema = StringUtil.Upper( cgiGet( edtavTfparametrossistema_nomesistema_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFParametrosSistema_NomeSistema", AV61TFParametrosSistema_NomeSistema);
            AV62TFParametrosSistema_NomeSistema_Sel = StringUtil.Upper( cgiGet( edtavTfparametrossistema_nomesistema_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFParametrosSistema_NomeSistema_Sel", AV62TFParametrosSistema_NomeSistema_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_padronizarstrings_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_padronizarstrings_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL");
               GX_FocusControl = edtavTfparametrossistema_padronizarstrings_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFParametrosSistema_PadronizarStrings_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFParametrosSistema_PadronizarStrings_Sel", StringUtil.Str( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0));
            }
            else
            {
               AV65TFParametrosSistema_PadronizarStrings_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfparametrossistema_padronizarstrings_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFParametrosSistema_PadronizarStrings_Sel", StringUtil.Str( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_fatorajuste_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_fatorajuste_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPARAMETROSSISTEMA_FATORAJUSTE");
               GX_FocusControl = edtavTfparametrossistema_fatorajuste_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFParametrosSistema_FatorAjuste = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV68TFParametrosSistema_FatorAjuste, 6, 2)));
            }
            else
            {
               AV68TFParametrosSistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtavTfparametrossistema_fatorajuste_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV68TFParametrosSistema_FatorAjuste, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_fatorajuste_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_fatorajuste_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPARAMETROSSISTEMA_FATORAJUSTE_TO");
               GX_FocusControl = edtavTfparametrossistema_fatorajuste_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFParametrosSistema_FatorAjuste_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFParametrosSistema_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV69TFParametrosSistema_FatorAjuste_To, 6, 2)));
            }
            else
            {
               AV69TFParametrosSistema_FatorAjuste_To = context.localUtil.CToN( cgiGet( edtavTfparametrossistema_fatorajuste_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFParametrosSistema_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV69TFParametrosSistema_FatorAjuste_To, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_licensiadocadastrado_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfparametrossistema_licensiadocadastrado_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL");
               GX_FocusControl = edtavTfparametrossistema_licensiadocadastrado_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72TFParametrosSistema_LicensiadoCadastrado_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFParametrosSistema_LicensiadoCadastrado_Sel", StringUtil.Str( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0));
            }
            else
            {
               AV72TFParametrosSistema_LicensiadoCadastrado_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfparametrossistema_licensiadocadastrado_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFParametrosSistema_LicensiadoCadastrado_Sel", StringUtil.Str( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0));
            }
            AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace = cgiGet( edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace", AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace);
            AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace = cgiGet( edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace", AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace);
            AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace = cgiGet( edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace", AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace);
            AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace = cgiGet( edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace", AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_33 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_33"), ",", "."));
            AV76GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV77GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_parametrossistema_nomesistema_Caption = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Caption");
            Ddo_parametrossistema_nomesistema_Tooltip = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Tooltip");
            Ddo_parametrossistema_nomesistema_Cls = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Cls");
            Ddo_parametrossistema_nomesistema_Filteredtext_set = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filteredtext_set");
            Ddo_parametrossistema_nomesistema_Selectedvalue_set = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Selectedvalue_set");
            Ddo_parametrossistema_nomesistema_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Dropdownoptionstype");
            Ddo_parametrossistema_nomesistema_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Titlecontrolidtoreplace");
            Ddo_parametrossistema_nomesistema_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includesortasc"));
            Ddo_parametrossistema_nomesistema_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includesortdsc"));
            Ddo_parametrossistema_nomesistema_Sortedstatus = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Sortedstatus");
            Ddo_parametrossistema_nomesistema_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includefilter"));
            Ddo_parametrossistema_nomesistema_Filtertype = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filtertype");
            Ddo_parametrossistema_nomesistema_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filterisrange"));
            Ddo_parametrossistema_nomesistema_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Includedatalist"));
            Ddo_parametrossistema_nomesistema_Datalisttype = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Datalisttype");
            Ddo_parametrossistema_nomesistema_Datalistproc = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Datalistproc");
            Ddo_parametrossistema_nomesistema_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_parametrossistema_nomesistema_Sortasc = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Sortasc");
            Ddo_parametrossistema_nomesistema_Sortdsc = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Sortdsc");
            Ddo_parametrossistema_nomesistema_Loadingdata = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Loadingdata");
            Ddo_parametrossistema_nomesistema_Cleanfilter = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Cleanfilter");
            Ddo_parametrossistema_nomesistema_Noresultsfound = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Noresultsfound");
            Ddo_parametrossistema_nomesistema_Searchbuttontext = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Searchbuttontext");
            Ddo_parametrossistema_padronizarstrings_Caption = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Caption");
            Ddo_parametrossistema_padronizarstrings_Tooltip = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Tooltip");
            Ddo_parametrossistema_padronizarstrings_Cls = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Cls");
            Ddo_parametrossistema_padronizarstrings_Selectedvalue_set = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Selectedvalue_set");
            Ddo_parametrossistema_padronizarstrings_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Dropdownoptionstype");
            Ddo_parametrossistema_padronizarstrings_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Titlecontrolidtoreplace");
            Ddo_parametrossistema_padronizarstrings_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includesortasc"));
            Ddo_parametrossistema_padronizarstrings_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includesortdsc"));
            Ddo_parametrossistema_padronizarstrings_Sortedstatus = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Sortedstatus");
            Ddo_parametrossistema_padronizarstrings_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includefilter"));
            Ddo_parametrossistema_padronizarstrings_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Includedatalist"));
            Ddo_parametrossistema_padronizarstrings_Datalisttype = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Datalisttype");
            Ddo_parametrossistema_padronizarstrings_Datalistfixedvalues = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Datalistfixedvalues");
            Ddo_parametrossistema_padronizarstrings_Sortasc = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Sortasc");
            Ddo_parametrossistema_padronizarstrings_Sortdsc = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Sortdsc");
            Ddo_parametrossistema_padronizarstrings_Cleanfilter = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Cleanfilter");
            Ddo_parametrossistema_padronizarstrings_Searchbuttontext = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Searchbuttontext");
            Ddo_parametrossistema_fatorajuste_Caption = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Caption");
            Ddo_parametrossistema_fatorajuste_Tooltip = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Tooltip");
            Ddo_parametrossistema_fatorajuste_Cls = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Cls");
            Ddo_parametrossistema_fatorajuste_Filteredtext_set = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtext_set");
            Ddo_parametrossistema_fatorajuste_Filteredtextto_set = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtextto_set");
            Ddo_parametrossistema_fatorajuste_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Dropdownoptionstype");
            Ddo_parametrossistema_fatorajuste_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Titlecontrolidtoreplace");
            Ddo_parametrossistema_fatorajuste_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includesortasc"));
            Ddo_parametrossistema_fatorajuste_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includesortdsc"));
            Ddo_parametrossistema_fatorajuste_Sortedstatus = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Sortedstatus");
            Ddo_parametrossistema_fatorajuste_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includefilter"));
            Ddo_parametrossistema_fatorajuste_Filtertype = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filtertype");
            Ddo_parametrossistema_fatorajuste_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filterisrange"));
            Ddo_parametrossistema_fatorajuste_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Includedatalist"));
            Ddo_parametrossistema_fatorajuste_Sortasc = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Sortasc");
            Ddo_parametrossistema_fatorajuste_Sortdsc = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Sortdsc");
            Ddo_parametrossistema_fatorajuste_Cleanfilter = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Cleanfilter");
            Ddo_parametrossistema_fatorajuste_Rangefilterfrom = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Rangefilterfrom");
            Ddo_parametrossistema_fatorajuste_Rangefilterto = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Rangefilterto");
            Ddo_parametrossistema_fatorajuste_Searchbuttontext = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Searchbuttontext");
            Ddo_parametrossistema_licensiadocadastrado_Caption = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Caption");
            Ddo_parametrossistema_licensiadocadastrado_Tooltip = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Tooltip");
            Ddo_parametrossistema_licensiadocadastrado_Cls = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Cls");
            Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_set = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Selectedvalue_set");
            Ddo_parametrossistema_licensiadocadastrado_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Dropdownoptionstype");
            Ddo_parametrossistema_licensiadocadastrado_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Titlecontrolidtoreplace");
            Ddo_parametrossistema_licensiadocadastrado_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includesortasc"));
            Ddo_parametrossistema_licensiadocadastrado_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includesortdsc"));
            Ddo_parametrossistema_licensiadocadastrado_Sortedstatus = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Sortedstatus");
            Ddo_parametrossistema_licensiadocadastrado_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includefilter"));
            Ddo_parametrossistema_licensiadocadastrado_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Includedatalist"));
            Ddo_parametrossistema_licensiadocadastrado_Datalisttype = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Datalisttype");
            Ddo_parametrossistema_licensiadocadastrado_Datalistfixedvalues = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Datalistfixedvalues");
            Ddo_parametrossistema_licensiadocadastrado_Sortasc = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Sortasc");
            Ddo_parametrossistema_licensiadocadastrado_Sortdsc = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Sortdsc");
            Ddo_parametrossistema_licensiadocadastrado_Cleanfilter = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Cleanfilter");
            Ddo_parametrossistema_licensiadocadastrado_Searchbuttontext = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_parametrossistema_nomesistema_Activeeventkey = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Activeeventkey");
            Ddo_parametrossistema_nomesistema_Filteredtext_get = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Filteredtext_get");
            Ddo_parametrossistema_nomesistema_Selectedvalue_get = cgiGet( "DDO_PARAMETROSSISTEMA_NOMESISTEMA_Selectedvalue_get");
            Ddo_parametrossistema_padronizarstrings_Activeeventkey = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Activeeventkey");
            Ddo_parametrossistema_padronizarstrings_Selectedvalue_get = cgiGet( "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS_Selectedvalue_get");
            Ddo_parametrossistema_fatorajuste_Activeeventkey = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Activeeventkey");
            Ddo_parametrossistema_fatorajuste_Filteredtext_get = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtext_get");
            Ddo_parametrossistema_fatorajuste_Filteredtextto_get = cgiGet( "DDO_PARAMETROSSISTEMA_FATORAJUSTE_Filteredtextto_get");
            Ddo_parametrossistema_licensiadocadastrado_Activeeventkey = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Activeeventkey");
            Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_get = cgiGet( "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSSISTEMA_NOMESISTEMA"), AV61TFParametrosSistema_NomeSistema) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL"), AV62TFParametrosSistema_NomeSistema_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL"), ",", ".") != Convert.ToDecimal( AV65TFParametrosSistema_PadronizarStrings_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_FATORAJUSTE"), ",", ".") != AV68TFParametrosSistema_FatorAjuste )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_FATORAJUSTE_TO"), ",", ".") != AV69TFParametrosSistema_FatorAjuste_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL"), ",", ".") != Convert.ToDecimal( AV72TFParametrosSistema_LicensiadoCadastrado_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E178I2 */
         E178I2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E178I2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfparametrossistema_nomesistema_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrossistema_nomesistema_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrossistema_nomesistema_Visible), 5, 0)));
         edtavTfparametrossistema_nomesistema_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrossistema_nomesistema_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrossistema_nomesistema_sel_Visible), 5, 0)));
         edtavTfparametrossistema_padronizarstrings_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrossistema_padronizarstrings_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrossistema_padronizarstrings_sel_Visible), 5, 0)));
         edtavTfparametrossistema_fatorajuste_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrossistema_fatorajuste_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrossistema_fatorajuste_Visible), 5, 0)));
         edtavTfparametrossistema_fatorajuste_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrossistema_fatorajuste_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrossistema_fatorajuste_to_Visible), 5, 0)));
         edtavTfparametrossistema_licensiadocadastrado_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrossistema_licensiadocadastrado_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrossistema_licensiadocadastrado_sel_Visible), 5, 0)));
         Ddo_parametrossistema_nomesistema_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosSistema_NomeSistema";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_nomesistema_Internalname, "TitleControlIdToReplace", Ddo_parametrossistema_nomesistema_Titlecontrolidtoreplace);
         AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace = Ddo_parametrossistema_nomesistema_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace", AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace);
         edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_parametrossistema_padronizarstrings_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosSistema_PadronizarStrings";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_padronizarstrings_Internalname, "TitleControlIdToReplace", Ddo_parametrossistema_padronizarstrings_Titlecontrolidtoreplace);
         AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace = Ddo_parametrossistema_padronizarstrings_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace", AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace);
         edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_parametrossistema_fatorajuste_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosSistema_FatorAjuste";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_fatorajuste_Internalname, "TitleControlIdToReplace", Ddo_parametrossistema_fatorajuste_Titlecontrolidtoreplace);
         AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace = Ddo_parametrossistema_fatorajuste_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace", AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace);
         edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_parametrossistema_licensiadocadastrado_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosSistema_LicensiadoCadastrado";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_licensiadocadastrado_Internalname, "TitleControlIdToReplace", Ddo_parametrossistema_licensiadocadastrado_Titlecontrolidtoreplace);
         AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace = Ddo_parametrossistema_licensiadocadastrado_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace", AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace);
         edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Parametros Sistema";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Sistema", 0);
         cmbavOrderedby.addItem("2", "Padronizar Strings", 0);
         cmbavOrderedby.addItem("3", "FA", 0);
         cmbavOrderedby.addItem("4", "Licenciado cadastrado?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV74DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV74DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E188I2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV60ParametrosSistema_NomeSistemaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ParametrosSistema_PadronizarStringsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67ParametrosSistema_FatorAjusteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtParametrosSistema_NomeSistema_Titleformat = 2;
         edtParametrosSistema_NomeSistema_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_NomeSistema_Internalname, "Title", edtParametrosSistema_NomeSistema_Title);
         cmbParametrosSistema_PadronizarStrings_Titleformat = 2;
         cmbParametrosSistema_PadronizarStrings.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Padronizar Strings", AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_PadronizarStrings_Internalname, "Title", cmbParametrosSistema_PadronizarStrings.Title.Text);
         edtParametrosSistema_FatorAjuste_Titleformat = 2;
         edtParametrosSistema_FatorAjuste_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "FA", AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_FatorAjuste_Internalname, "Title", edtParametrosSistema_FatorAjuste_Title);
         cmbParametrosSistema_LicensiadoCadastrado_Titleformat = 2;
         cmbParametrosSistema_LicensiadoCadastrado.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Licenciado cadastrado?", AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_LicensiadoCadastrado_Internalname, "Title", cmbParametrosSistema_LicensiadoCadastrado.Title.Text);
         AV76GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76GridCurrentPage), 10, 0)));
         AV77GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77GridPageCount), 10, 0)));
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = AV61TFParametrosSistema_NomeSistema;
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = AV62TFParametrosSistema_NomeSistema_Sel;
         AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel = AV65TFParametrosSistema_PadronizarStrings_Sel;
         AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste = AV68TFParametrosSistema_FatorAjuste;
         AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to = AV69TFParametrosSistema_FatorAjuste_To;
         AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel = AV72TFParametrosSistema_LicensiadoCadastrado_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60ParametrosSistema_NomeSistemaTitleFilterData", AV60ParametrosSistema_NomeSistemaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64ParametrosSistema_PadronizarStringsTitleFilterData", AV64ParametrosSistema_PadronizarStringsTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV67ParametrosSistema_FatorAjusteTitleFilterData", AV67ParametrosSistema_FatorAjusteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData", AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData);
      }

      protected void E118I2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV75PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV75PageToGo) ;
         }
      }

      protected void E128I2( )
      {
         /* Ddo_parametrossistema_nomesistema_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrossistema_nomesistema_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_nomesistema_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_nomesistema_Internalname, "SortedStatus", Ddo_parametrossistema_nomesistema_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_nomesistema_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_nomesistema_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_nomesistema_Internalname, "SortedStatus", Ddo_parametrossistema_nomesistema_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_nomesistema_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFParametrosSistema_NomeSistema = Ddo_parametrossistema_nomesistema_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFParametrosSistema_NomeSistema", AV61TFParametrosSistema_NomeSistema);
            AV62TFParametrosSistema_NomeSistema_Sel = Ddo_parametrossistema_nomesistema_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFParametrosSistema_NomeSistema_Sel", AV62TFParametrosSistema_NomeSistema_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E138I2( )
      {
         /* Ddo_parametrossistema_padronizarstrings_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrossistema_padronizarstrings_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_padronizarstrings_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_padronizarstrings_Internalname, "SortedStatus", Ddo_parametrossistema_padronizarstrings_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_padronizarstrings_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_padronizarstrings_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_padronizarstrings_Internalname, "SortedStatus", Ddo_parametrossistema_padronizarstrings_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_padronizarstrings_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFParametrosSistema_PadronizarStrings_Sel = (short)(NumberUtil.Val( Ddo_parametrossistema_padronizarstrings_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFParametrosSistema_PadronizarStrings_Sel", StringUtil.Str( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E148I2( )
      {
         /* Ddo_parametrossistema_fatorajuste_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrossistema_fatorajuste_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_fatorajuste_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_fatorajuste_Internalname, "SortedStatus", Ddo_parametrossistema_fatorajuste_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_fatorajuste_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_fatorajuste_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_fatorajuste_Internalname, "SortedStatus", Ddo_parametrossistema_fatorajuste_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_fatorajuste_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV68TFParametrosSistema_FatorAjuste = NumberUtil.Val( Ddo_parametrossistema_fatorajuste_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV68TFParametrosSistema_FatorAjuste, 6, 2)));
            AV69TFParametrosSistema_FatorAjuste_To = NumberUtil.Val( Ddo_parametrossistema_fatorajuste_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFParametrosSistema_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV69TFParametrosSistema_FatorAjuste_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E158I2( )
      {
         /* Ddo_parametrossistema_licensiadocadastrado_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrossistema_licensiadocadastrado_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_licensiadocadastrado_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_licensiadocadastrado_Internalname, "SortedStatus", Ddo_parametrossistema_licensiadocadastrado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_licensiadocadastrado_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrossistema_licensiadocadastrado_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_licensiadocadastrado_Internalname, "SortedStatus", Ddo_parametrossistema_licensiadocadastrado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrossistema_licensiadocadastrado_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV72TFParametrosSistema_LicensiadoCadastrado_Sel = (short)(NumberUtil.Val( Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFParametrosSistema_LicensiadoCadastrado_Sel", StringUtil.Str( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E198I2( )
      {
         /* Grid_Load Routine */
         AV29Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV29Update);
         AV86Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A330ParametrosSistema_Codigo);
         edtParametrosSistema_NomeSistema_Link = formatLink("viewparametrossistema.aspx") + "?" + UrlEncode("" +A330ParametrosSistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 33;
         }
         sendrow_332( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_33_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(33, GridRow);
         }
      }

      protected void E168I2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_parametrossistema_nomesistema_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_nomesistema_Internalname, "SortedStatus", Ddo_parametrossistema_nomesistema_Sortedstatus);
         Ddo_parametrossistema_padronizarstrings_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_padronizarstrings_Internalname, "SortedStatus", Ddo_parametrossistema_padronizarstrings_Sortedstatus);
         Ddo_parametrossistema_fatorajuste_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_fatorajuste_Internalname, "SortedStatus", Ddo_parametrossistema_fatorajuste_Sortedstatus);
         Ddo_parametrossistema_licensiadocadastrado_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_licensiadocadastrado_Internalname, "SortedStatus", Ddo_parametrossistema_licensiadocadastrado_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_parametrossistema_nomesistema_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_nomesistema_Internalname, "SortedStatus", Ddo_parametrossistema_nomesistema_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_parametrossistema_padronizarstrings_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_padronizarstrings_Internalname, "SortedStatus", Ddo_parametrossistema_padronizarstrings_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_parametrossistema_fatorajuste_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_fatorajuste_Internalname, "SortedStatus", Ddo_parametrossistema_fatorajuste_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_parametrossistema_licensiadocadastrado_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_licensiadocadastrado_Internalname, "SortedStatus", Ddo_parametrossistema_licensiadocadastrado_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV51Session.Get(AV87Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV87Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV51Session.Get(AV87Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV88GXV1 = 1;
         while ( AV88GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV88GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSSISTEMA_NOMESISTEMA") == 0 )
            {
               AV61TFParametrosSistema_NomeSistema = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFParametrosSistema_NomeSistema", AV61TFParametrosSistema_NomeSistema);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFParametrosSistema_NomeSistema)) )
               {
                  Ddo_parametrossistema_nomesistema_Filteredtext_set = AV61TFParametrosSistema_NomeSistema;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_nomesistema_Internalname, "FilteredText_set", Ddo_parametrossistema_nomesistema_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSSISTEMA_NOMESISTEMA_SEL") == 0 )
            {
               AV62TFParametrosSistema_NomeSistema_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFParametrosSistema_NomeSistema_Sel", AV62TFParametrosSistema_NomeSistema_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFParametrosSistema_NomeSistema_Sel)) )
               {
                  Ddo_parametrossistema_nomesistema_Selectedvalue_set = AV62TFParametrosSistema_NomeSistema_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_nomesistema_Internalname, "SelectedValue_set", Ddo_parametrossistema_nomesistema_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL") == 0 )
            {
               AV65TFParametrosSistema_PadronizarStrings_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFParametrosSistema_PadronizarStrings_Sel", StringUtil.Str( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0));
               if ( ! (0==AV65TFParametrosSistema_PadronizarStrings_Sel) )
               {
                  Ddo_parametrossistema_padronizarstrings_Selectedvalue_set = StringUtil.Str( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_padronizarstrings_Internalname, "SelectedValue_set", Ddo_parametrossistema_padronizarstrings_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSSISTEMA_FATORAJUSTE") == 0 )
            {
               AV68TFParametrosSistema_FatorAjuste = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV68TFParametrosSistema_FatorAjuste, 6, 2)));
               AV69TFParametrosSistema_FatorAjuste_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFParametrosSistema_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV69TFParametrosSistema_FatorAjuste_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV68TFParametrosSistema_FatorAjuste) )
               {
                  Ddo_parametrossistema_fatorajuste_Filteredtext_set = StringUtil.Str( AV68TFParametrosSistema_FatorAjuste, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_fatorajuste_Internalname, "FilteredText_set", Ddo_parametrossistema_fatorajuste_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV69TFParametrosSistema_FatorAjuste_To) )
               {
                  Ddo_parametrossistema_fatorajuste_Filteredtextto_set = StringUtil.Str( AV69TFParametrosSistema_FatorAjuste_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_fatorajuste_Internalname, "FilteredTextTo_set", Ddo_parametrossistema_fatorajuste_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL") == 0 )
            {
               AV72TFParametrosSistema_LicensiadoCadastrado_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFParametrosSistema_LicensiadoCadastrado_Sel", StringUtil.Str( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0));
               if ( ! (0==AV72TFParametrosSistema_LicensiadoCadastrado_Sel) )
               {
                  Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_set = StringUtil.Str( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrossistema_licensiadocadastrado_Internalname, "SelectedValue_set", Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_set);
               }
            }
            AV88GXV1 = (int)(AV88GXV1+1);
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV51Session.Get(AV87Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFParametrosSistema_NomeSistema)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSSISTEMA_NOMESISTEMA";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFParametrosSistema_NomeSistema;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFParametrosSistema_NomeSistema_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSSISTEMA_NOMESISTEMA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV62TFParametrosSistema_NomeSistema_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV65TFParametrosSistema_PadronizarStrings_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV65TFParametrosSistema_PadronizarStrings_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV68TFParametrosSistema_FatorAjuste) && (Convert.ToDecimal(0)==AV69TFParametrosSistema_FatorAjuste_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSSISTEMA_FATORAJUSTE";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV68TFParametrosSistema_FatorAjuste, 6, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV69TFParametrosSistema_FatorAjuste_To, 6, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV72TFParametrosSistema_LicensiadoCadastrado_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV72TFParametrosSistema_LicensiadoCadastrado_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV87Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV87Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ParametrosSistema";
         AV51Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_8I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_8I2( true) ;
         }
         else
         {
            wb_table2_8_8I2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_27_8I2( true) ;
         }
         else
         {
            wb_table3_27_8I2( false) ;
         }
         return  ;
      }

      protected void wb_table3_27_8I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8I2e( true) ;
         }
         else
         {
            wb_table1_2_8I2e( false) ;
         }
      }

      protected void wb_table3_27_8I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_30_8I2( true) ;
         }
         else
         {
            wb_table4_30_8I2( false) ;
         }
         return  ;
      }

      protected void wb_table4_30_8I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_27_8I2e( true) ;
         }
         else
         {
            wb_table3_27_8I2e( false) ;
         }
      }

      protected void wb_table4_30_8I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"33\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtParametrosSistema_NomeSistema_Titleformat == 0 )
               {
                  context.SendWebValue( edtParametrosSistema_NomeSistema_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtParametrosSistema_NomeSistema_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbParametrosSistema_PadronizarStrings_Titleformat == 0 )
               {
                  context.SendWebValue( cmbParametrosSistema_PadronizarStrings.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbParametrosSistema_PadronizarStrings.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtParametrosSistema_FatorAjuste_Titleformat == 0 )
               {
                  context.SendWebValue( edtParametrosSistema_FatorAjuste_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtParametrosSistema_FatorAjuste_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbParametrosSistema_LicensiadoCadastrado_Titleformat == 0 )
               {
                  context.SendWebValue( cmbParametrosSistema_LicensiadoCadastrado.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbParametrosSistema_LicensiadoCadastrado.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A331ParametrosSistema_NomeSistema));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtParametrosSistema_NomeSistema_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtParametrosSistema_NomeSistema_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtParametrosSistema_NomeSistema_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbParametrosSistema_PadronizarStrings.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbParametrosSistema_PadronizarStrings_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A708ParametrosSistema_FatorAjuste, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtParametrosSistema_FatorAjuste_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtParametrosSistema_FatorAjuste_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbParametrosSistema_LicensiadoCadastrado.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbParametrosSistema_LicensiadoCadastrado_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 33 )
         {
            wbEnd = 0;
            nRC_GXsfl_33 = (short)(nGXsfl_33_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_30_8I2e( true) ;
         }
         else
         {
            wb_table4_30_8I2e( false) ;
         }
      }

      protected void wb_table2_8_8I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblParametrossistematitle_Internalname, "Par�metros do Sistema", "", "", lblParametrossistematitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_8I2( true) ;
         }
         else
         {
            wb_table5_13_8I2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_8I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_33_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWParametrosSistema.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_22_8I2( true) ;
         }
         else
         {
            wb_table6_22_8I2( false) ;
         }
         return  ;
      }

      protected void wb_table6_22_8I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8I2e( true) ;
         }
         else
         {
            wb_table2_8_8I2e( false) ;
         }
      }

      protected void wb_table6_22_8I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_8I2e( true) ;
         }
         else
         {
            wb_table6_22_8I2e( false) ;
         }
      }

      protected void wb_table5_13_8I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_8I2e( true) ;
         }
         else
         {
            wb_table5_13_8I2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8I2( ) ;
         WS8I2( ) ;
         WE8I2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117375639");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwparametrossistema.js", "?20203117375640");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_332( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_33_idx;
         edtParametrosSistema_Codigo_Internalname = "PARAMETROSSISTEMA_CODIGO_"+sGXsfl_33_idx;
         edtParametrosSistema_NomeSistema_Internalname = "PARAMETROSSISTEMA_NOMESISTEMA_"+sGXsfl_33_idx;
         cmbParametrosSistema_PadronizarStrings_Internalname = "PARAMETROSSISTEMA_PADRONIZARSTRINGS_"+sGXsfl_33_idx;
         edtParametrosSistema_FatorAjuste_Internalname = "PARAMETROSSISTEMA_FATORAJUSTE_"+sGXsfl_33_idx;
         cmbParametrosSistema_LicensiadoCadastrado_Internalname = "PARAMETROSSISTEMA_LICENSIADOCADASTRADO_"+sGXsfl_33_idx;
      }

      protected void SubsflControlProps_fel_332( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_33_fel_idx;
         edtParametrosSistema_Codigo_Internalname = "PARAMETROSSISTEMA_CODIGO_"+sGXsfl_33_fel_idx;
         edtParametrosSistema_NomeSistema_Internalname = "PARAMETROSSISTEMA_NOMESISTEMA_"+sGXsfl_33_fel_idx;
         cmbParametrosSistema_PadronizarStrings_Internalname = "PARAMETROSSISTEMA_PADRONIZARSTRINGS_"+sGXsfl_33_fel_idx;
         edtParametrosSistema_FatorAjuste_Internalname = "PARAMETROSSISTEMA_FATORAJUSTE_"+sGXsfl_33_fel_idx;
         cmbParametrosSistema_LicensiadoCadastrado_Internalname = "PARAMETROSSISTEMA_LICENSIADOCADASTRADO_"+sGXsfl_33_fel_idx;
      }

      protected void sendrow_332( )
      {
         SubsflControlProps_332( ) ;
         WB8I0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_33_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_33_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_33_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV86Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV86Update_GXI : context.PathToRelativeUrl( AV29Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosSistema_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A330ParametrosSistema_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtParametrosSistema_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosSistema_NomeSistema_Internalname,StringUtil.RTrim( A331ParametrosSistema_NomeSistema),StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtParametrosSistema_NomeSistema_Link,(String)"",(String)"",(String)"",(String)edtParametrosSistema_NomeSistema_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_33_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PARAMETROSSISTEMA_PADRONIZARSTRINGS_" + sGXsfl_33_idx;
               cmbParametrosSistema_PadronizarStrings.Name = GXCCtl;
               cmbParametrosSistema_PadronizarStrings.WebTags = "";
               cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               if ( cmbParametrosSistema_PadronizarStrings.ItemCount > 0 )
               {
                  A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cmbParametrosSistema_PadronizarStrings.getValidValue(StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbParametrosSistema_PadronizarStrings,(String)cmbParametrosSistema_PadronizarStrings_Internalname,StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings),(short)1,(String)cmbParametrosSistema_PadronizarStrings_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbParametrosSistema_PadronizarStrings.CurrentValue = StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_PadronizarStrings_Internalname, "Values", (String)(cmbParametrosSistema_PadronizarStrings.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosSistema_FatorAjuste_Internalname,StringUtil.LTrim( StringUtil.NToC( A708ParametrosSistema_FatorAjuste, 6, 2, ",", "")),context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtParametrosSistema_FatorAjuste_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_33_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PARAMETROSSISTEMA_LICENSIADOCADASTRADO_" + sGXsfl_33_idx;
               cmbParametrosSistema_LicensiadoCadastrado.Name = GXCCtl;
               cmbParametrosSistema_LicensiadoCadastrado.WebTags = "";
               cmbParametrosSistema_LicensiadoCadastrado.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               cmbParametrosSistema_LicensiadoCadastrado.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               if ( cmbParametrosSistema_LicensiadoCadastrado.ItemCount > 0 )
               {
                  A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cmbParametrosSistema_LicensiadoCadastrado.getValidValue(StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado)));
                  n399ParametrosSistema_LicensiadoCadastrado = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbParametrosSistema_LicensiadoCadastrado,(String)cmbParametrosSistema_LicensiadoCadastrado_Internalname,StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado),(short)1,(String)cmbParametrosSistema_LicensiadoCadastrado_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbParametrosSistema_LicensiadoCadastrado.CurrentValue = StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_LicensiadoCadastrado_Internalname, "Values", (String)(cmbParametrosSistema_LicensiadoCadastrado.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_CODIGO"+"_"+sGXsfl_33_idx, GetSecureSignedToken( sGXsfl_33_idx, context.localUtil.Format( (decimal)(A330ParametrosSistema_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_NOMESISTEMA"+"_"+sGXsfl_33_idx, GetSecureSignedToken( sGXsfl_33_idx, StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_PADRONIZARSTRINGS"+"_"+sGXsfl_33_idx, GetSecureSignedToken( sGXsfl_33_idx, A417ParametrosSistema_PadronizarStrings));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_FATORAJUSTE"+"_"+sGXsfl_33_idx, GetSecureSignedToken( sGXsfl_33_idx, context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_LICENSIADOCADASTRADO"+"_"+sGXsfl_33_idx, GetSecureSignedToken( sGXsfl_33_idx, A399ParametrosSistema_LicensiadoCadastrado));
            GridContainer.AddRow(GridRow);
            nGXsfl_33_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_33_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_33_idx+1));
            sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
            SubsflControlProps_332( ) ;
         }
         /* End function sendrow_332 */
      }

      protected void init_default_properties( )
      {
         lblParametrossistematitle_Internalname = "PARAMETROSSISTEMATITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtParametrosSistema_Codigo_Internalname = "PARAMETROSSISTEMA_CODIGO";
         edtParametrosSistema_NomeSistema_Internalname = "PARAMETROSSISTEMA_NOMESISTEMA";
         cmbParametrosSistema_PadronizarStrings_Internalname = "PARAMETROSSISTEMA_PADRONIZARSTRINGS";
         edtParametrosSistema_FatorAjuste_Internalname = "PARAMETROSSISTEMA_FATORAJUSTE";
         cmbParametrosSistema_LicensiadoCadastrado_Internalname = "PARAMETROSSISTEMA_LICENSIADOCADASTRADO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfparametrossistema_nomesistema_Internalname = "vTFPARAMETROSSISTEMA_NOMESISTEMA";
         edtavTfparametrossistema_nomesistema_sel_Internalname = "vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL";
         edtavTfparametrossistema_padronizarstrings_sel_Internalname = "vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL";
         edtavTfparametrossistema_fatorajuste_Internalname = "vTFPARAMETROSSISTEMA_FATORAJUSTE";
         edtavTfparametrossistema_fatorajuste_to_Internalname = "vTFPARAMETROSSISTEMA_FATORAJUSTE_TO";
         edtavTfparametrossistema_licensiadocadastrado_sel_Internalname = "vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL";
         Ddo_parametrossistema_nomesistema_Internalname = "DDO_PARAMETROSSISTEMA_NOMESISTEMA";
         edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE";
         Ddo_parametrossistema_padronizarstrings_Internalname = "DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS";
         edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE";
         Ddo_parametrossistema_fatorajuste_Internalname = "DDO_PARAMETROSSISTEMA_FATORAJUSTE";
         edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE";
         Ddo_parametrossistema_licensiadocadastrado_Internalname = "DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO";
         edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbParametrosSistema_LicensiadoCadastrado_Jsonclick = "";
         edtParametrosSistema_FatorAjuste_Jsonclick = "";
         cmbParametrosSistema_PadronizarStrings_Jsonclick = "";
         edtParametrosSistema_NomeSistema_Jsonclick = "";
         edtParametrosSistema_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtParametrosSistema_NomeSistema_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbParametrosSistema_LicensiadoCadastrado_Titleformat = 0;
         edtParametrosSistema_FatorAjuste_Titleformat = 0;
         cmbParametrosSistema_PadronizarStrings_Titleformat = 0;
         edtParametrosSistema_NomeSistema_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbParametrosSistema_LicensiadoCadastrado.Title.Text = "Licenciado cadastrado?";
         edtParametrosSistema_FatorAjuste_Title = "FA";
         cmbParametrosSistema_PadronizarStrings.Title.Text = "Padronizar Strings";
         edtParametrosSistema_NomeSistema_Title = "Sistema";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Visible = 1;
         edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Visible = 1;
         edtavTfparametrossistema_licensiadocadastrado_sel_Jsonclick = "";
         edtavTfparametrossistema_licensiadocadastrado_sel_Visible = 1;
         edtavTfparametrossistema_fatorajuste_to_Jsonclick = "";
         edtavTfparametrossistema_fatorajuste_to_Visible = 1;
         edtavTfparametrossistema_fatorajuste_Jsonclick = "";
         edtavTfparametrossistema_fatorajuste_Visible = 1;
         edtavTfparametrossistema_padronizarstrings_sel_Jsonclick = "";
         edtavTfparametrossistema_padronizarstrings_sel_Visible = 1;
         edtavTfparametrossistema_nomesistema_sel_Jsonclick = "";
         edtavTfparametrossistema_nomesistema_sel_Visible = 1;
         edtavTfparametrossistema_nomesistema_Jsonclick = "";
         edtavTfparametrossistema_nomesistema_Visible = 1;
         Ddo_parametrossistema_licensiadocadastrado_Searchbuttontext = "Pesquisar";
         Ddo_parametrossistema_licensiadocadastrado_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrossistema_licensiadocadastrado_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrossistema_licensiadocadastrado_Sortasc = "Ordenar de A � Z";
         Ddo_parametrossistema_licensiadocadastrado_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_parametrossistema_licensiadocadastrado_Datalisttype = "FixedValues";
         Ddo_parametrossistema_licensiadocadastrado_Includedatalist = Convert.ToBoolean( -1);
         Ddo_parametrossistema_licensiadocadastrado_Includefilter = Convert.ToBoolean( 0);
         Ddo_parametrossistema_licensiadocadastrado_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_licensiadocadastrado_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_licensiadocadastrado_Titlecontrolidtoreplace = "";
         Ddo_parametrossistema_licensiadocadastrado_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrossistema_licensiadocadastrado_Cls = "ColumnSettings";
         Ddo_parametrossistema_licensiadocadastrado_Tooltip = "Op��es";
         Ddo_parametrossistema_licensiadocadastrado_Caption = "";
         Ddo_parametrossistema_fatorajuste_Searchbuttontext = "Pesquisar";
         Ddo_parametrossistema_fatorajuste_Rangefilterto = "At�";
         Ddo_parametrossistema_fatorajuste_Rangefilterfrom = "Desde";
         Ddo_parametrossistema_fatorajuste_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrossistema_fatorajuste_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrossistema_fatorajuste_Sortasc = "Ordenar de A � Z";
         Ddo_parametrossistema_fatorajuste_Includedatalist = Convert.ToBoolean( 0);
         Ddo_parametrossistema_fatorajuste_Filterisrange = Convert.ToBoolean( -1);
         Ddo_parametrossistema_fatorajuste_Filtertype = "Numeric";
         Ddo_parametrossistema_fatorajuste_Includefilter = Convert.ToBoolean( -1);
         Ddo_parametrossistema_fatorajuste_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_fatorajuste_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_fatorajuste_Titlecontrolidtoreplace = "";
         Ddo_parametrossistema_fatorajuste_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrossistema_fatorajuste_Cls = "ColumnSettings";
         Ddo_parametrossistema_fatorajuste_Tooltip = "Op��es";
         Ddo_parametrossistema_fatorajuste_Caption = "";
         Ddo_parametrossistema_padronizarstrings_Searchbuttontext = "Pesquisar";
         Ddo_parametrossistema_padronizarstrings_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrossistema_padronizarstrings_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrossistema_padronizarstrings_Sortasc = "Ordenar de A � Z";
         Ddo_parametrossistema_padronizarstrings_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_parametrossistema_padronizarstrings_Datalisttype = "FixedValues";
         Ddo_parametrossistema_padronizarstrings_Includedatalist = Convert.ToBoolean( -1);
         Ddo_parametrossistema_padronizarstrings_Includefilter = Convert.ToBoolean( 0);
         Ddo_parametrossistema_padronizarstrings_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_padronizarstrings_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_padronizarstrings_Titlecontrolidtoreplace = "";
         Ddo_parametrossistema_padronizarstrings_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrossistema_padronizarstrings_Cls = "ColumnSettings";
         Ddo_parametrossistema_padronizarstrings_Tooltip = "Op��es";
         Ddo_parametrossistema_padronizarstrings_Caption = "";
         Ddo_parametrossistema_nomesistema_Searchbuttontext = "Pesquisar";
         Ddo_parametrossistema_nomesistema_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_parametrossistema_nomesistema_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrossistema_nomesistema_Loadingdata = "Carregando dados...";
         Ddo_parametrossistema_nomesistema_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrossistema_nomesistema_Sortasc = "Ordenar de A � Z";
         Ddo_parametrossistema_nomesistema_Datalistupdateminimumcharacters = 0;
         Ddo_parametrossistema_nomesistema_Datalistproc = "GetWWParametrosSistemaFilterData";
         Ddo_parametrossistema_nomesistema_Datalisttype = "Dynamic";
         Ddo_parametrossistema_nomesistema_Includedatalist = Convert.ToBoolean( -1);
         Ddo_parametrossistema_nomesistema_Filterisrange = Convert.ToBoolean( 0);
         Ddo_parametrossistema_nomesistema_Filtertype = "Character";
         Ddo_parametrossistema_nomesistema_Includefilter = Convert.ToBoolean( -1);
         Ddo_parametrossistema_nomesistema_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_nomesistema_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrossistema_nomesistema_Titlecontrolidtoreplace = "";
         Ddo_parametrossistema_nomesistema_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrossistema_nomesistema_Cls = "ColumnSettings";
         Ddo_parametrossistema_nomesistema_Tooltip = "Op��es";
         Ddo_parametrossistema_nomesistema_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Parametros Sistema";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV60ParametrosSistema_NomeSistemaTitleFilterData',fld:'vPARAMETROSSISTEMA_NOMESISTEMATITLEFILTERDATA',pic:'',nv:null},{av:'AV64ParametrosSistema_PadronizarStringsTitleFilterData',fld:'vPARAMETROSSISTEMA_PADRONIZARSTRINGSTITLEFILTERDATA',pic:'',nv:null},{av:'AV67ParametrosSistema_FatorAjusteTitleFilterData',fld:'vPARAMETROSSISTEMA_FATORAJUSTETITLEFILTERDATA',pic:'',nv:null},{av:'AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData',fld:'vPARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLEFILTERDATA',pic:'',nv:null},{av:'edtParametrosSistema_NomeSistema_Titleformat',ctrl:'PARAMETROSSISTEMA_NOMESISTEMA',prop:'Titleformat'},{av:'edtParametrosSistema_NomeSistema_Title',ctrl:'PARAMETROSSISTEMA_NOMESISTEMA',prop:'Title'},{av:'cmbParametrosSistema_PadronizarStrings'},{av:'edtParametrosSistema_FatorAjuste_Titleformat',ctrl:'PARAMETROSSISTEMA_FATORAJUSTE',prop:'Titleformat'},{av:'edtParametrosSistema_FatorAjuste_Title',ctrl:'PARAMETROSSISTEMA_FATORAJUSTE',prop:'Title'},{av:'cmbParametrosSistema_LicensiadoCadastrado'},{av:'AV76GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV77GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E118I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PARAMETROSSISTEMA_NOMESISTEMA.ONOPTIONCLICKED","{handler:'E128I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrossistema_nomesistema_Activeeventkey',ctrl:'DDO_PARAMETROSSISTEMA_NOMESISTEMA',prop:'ActiveEventKey'},{av:'Ddo_parametrossistema_nomesistema_Filteredtext_get',ctrl:'DDO_PARAMETROSSISTEMA_NOMESISTEMA',prop:'FilteredText_get'},{av:'Ddo_parametrossistema_nomesistema_Selectedvalue_get',ctrl:'DDO_PARAMETROSSISTEMA_NOMESISTEMA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrossistema_nomesistema_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_NOMESISTEMA',prop:'SortedStatus'},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'Ddo_parametrossistema_padronizarstrings_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS',prop:'SortedStatus'},{av:'Ddo_parametrossistema_fatorajuste_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_FATORAJUSTE',prop:'SortedStatus'},{av:'Ddo_parametrossistema_licensiadocadastrado_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS.ONOPTIONCLICKED","{handler:'E138I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrossistema_padronizarstrings_Activeeventkey',ctrl:'DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS',prop:'ActiveEventKey'},{av:'Ddo_parametrossistema_padronizarstrings_Selectedvalue_get',ctrl:'DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrossistema_padronizarstrings_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS',prop:'SortedStatus'},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'Ddo_parametrossistema_nomesistema_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_NOMESISTEMA',prop:'SortedStatus'},{av:'Ddo_parametrossistema_fatorajuste_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_FATORAJUSTE',prop:'SortedStatus'},{av:'Ddo_parametrossistema_licensiadocadastrado_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PARAMETROSSISTEMA_FATORAJUSTE.ONOPTIONCLICKED","{handler:'E148I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrossistema_fatorajuste_Activeeventkey',ctrl:'DDO_PARAMETROSSISTEMA_FATORAJUSTE',prop:'ActiveEventKey'},{av:'Ddo_parametrossistema_fatorajuste_Filteredtext_get',ctrl:'DDO_PARAMETROSSISTEMA_FATORAJUSTE',prop:'FilteredText_get'},{av:'Ddo_parametrossistema_fatorajuste_Filteredtextto_get',ctrl:'DDO_PARAMETROSSISTEMA_FATORAJUSTE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrossistema_fatorajuste_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_FATORAJUSTE',prop:'SortedStatus'},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_parametrossistema_nomesistema_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_NOMESISTEMA',prop:'SortedStatus'},{av:'Ddo_parametrossistema_padronizarstrings_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS',prop:'SortedStatus'},{av:'Ddo_parametrossistema_licensiadocadastrado_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO.ONOPTIONCLICKED","{handler:'E158I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrossistema_licensiadocadastrado_Activeeventkey',ctrl:'DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO',prop:'ActiveEventKey'},{av:'Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_get',ctrl:'DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrossistema_licensiadocadastrado_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADO',prop:'SortedStatus'},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'Ddo_parametrossistema_nomesistema_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_NOMESISTEMA',prop:'SortedStatus'},{av:'Ddo_parametrossistema_padronizarstrings_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_PADRONIZARSTRINGS',prop:'SortedStatus'},{av:'Ddo_parametrossistema_fatorajuste_Sortedstatus',ctrl:'DDO_PARAMETROSSISTEMA_FATORAJUSTE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E198I2',iparms:[{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'edtParametrosSistema_NomeSistema_Link',ctrl:'PARAMETROSSISTEMA_NOMESISTEMA',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E168I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFParametrosSistema_NomeSistema',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA',pic:'@!',nv:''},{av:'AV62TFParametrosSistema_NomeSistema_Sel',fld:'vTFPARAMETROSSISTEMA_NOMESISTEMA_SEL',pic:'@!',nv:''},{av:'AV65TFParametrosSistema_PadronizarStrings_Sel',fld:'vTFPARAMETROSSISTEMA_PADRONIZARSTRINGS_SEL',pic:'9',nv:0},{av:'AV68TFParametrosSistema_FatorAjuste',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV69TFParametrosSistema_FatorAjuste_To',fld:'vTFPARAMETROSSISTEMA_FATORAJUSTE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV72TFParametrosSistema_LicensiadoCadastrado_Sel',fld:'vTFPARAMETROSSISTEMA_LICENSIADOCADASTRADO_SEL',pic:'9',nv:0},{av:'AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_NOMESISTEMATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_PADRONIZARSTRINGSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace',fld:'vDDO_PARAMETROSSISTEMA_LICENSIADOCADASTRADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_parametrossistema_nomesistema_Activeeventkey = "";
         Ddo_parametrossistema_nomesistema_Filteredtext_get = "";
         Ddo_parametrossistema_nomesistema_Selectedvalue_get = "";
         Ddo_parametrossistema_padronizarstrings_Activeeventkey = "";
         Ddo_parametrossistema_padronizarstrings_Selectedvalue_get = "";
         Ddo_parametrossistema_fatorajuste_Activeeventkey = "";
         Ddo_parametrossistema_fatorajuste_Filteredtext_get = "";
         Ddo_parametrossistema_fatorajuste_Filteredtextto_get = "";
         Ddo_parametrossistema_licensiadocadastrado_Activeeventkey = "";
         Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV61TFParametrosSistema_NomeSistema = "";
         AV62TFParametrosSistema_NomeSistema_Sel = "";
         AV68TFParametrosSistema_FatorAjuste = (decimal)(1);
         AV69TFParametrosSistema_FatorAjuste_To = (decimal)(1);
         AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace = "";
         AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace = "";
         AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace = "";
         AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace = "";
         AV87Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV74DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV60ParametrosSistema_NomeSistemaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ParametrosSistema_PadronizarStringsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67ParametrosSistema_FatorAjusteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_parametrossistema_nomesistema_Filteredtext_set = "";
         Ddo_parametrossistema_nomesistema_Selectedvalue_set = "";
         Ddo_parametrossistema_nomesistema_Sortedstatus = "";
         Ddo_parametrossistema_padronizarstrings_Selectedvalue_set = "";
         Ddo_parametrossistema_padronizarstrings_Sortedstatus = "";
         Ddo_parametrossistema_fatorajuste_Filteredtext_set = "";
         Ddo_parametrossistema_fatorajuste_Filteredtextto_set = "";
         Ddo_parametrossistema_fatorajuste_Sortedstatus = "";
         Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_set = "";
         Ddo_parametrossistema_licensiadocadastrado_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Update = "";
         AV86Update_GXI = "";
         A331ParametrosSistema_NomeSistema = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = "";
         AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel = "";
         AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema = "";
         H008I2_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         H008I2_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         H008I2_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         H008I2_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         H008I2_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         H008I2_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         H008I2_A330ParametrosSistema_Codigo = new int[1] ;
         H008I3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV51Session = context.GetSession();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblParametrossistematitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwparametrossistema__default(),
            new Object[][] {
                new Object[] {
               H008I2_A399ParametrosSistema_LicensiadoCadastrado, H008I2_n399ParametrosSistema_LicensiadoCadastrado, H008I2_A708ParametrosSistema_FatorAjuste, H008I2_n708ParametrosSistema_FatorAjuste, H008I2_A417ParametrosSistema_PadronizarStrings, H008I2_A331ParametrosSistema_NomeSistema, H008I2_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               H008I3_AGRID_nRecordCount
               }
            }
         );
         AV87Pgmname = "WWParametrosSistema";
         /* GeneXus formulas. */
         AV87Pgmname = "WWParametrosSistema";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_33 ;
      private short nGXsfl_33_idx=1 ;
      private short AV13OrderedBy ;
      private short AV65TFParametrosSistema_PadronizarStrings_Sel ;
      private short AV72TFParametrosSistema_LicensiadoCadastrado_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_33_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel ;
      private short AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel ;
      private short edtParametrosSistema_NomeSistema_Titleformat ;
      private short cmbParametrosSistema_PadronizarStrings_Titleformat ;
      private short edtParametrosSistema_FatorAjuste_Titleformat ;
      private short cmbParametrosSistema_LicensiadoCadastrado_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A330ParametrosSistema_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_parametrossistema_nomesistema_Datalistupdateminimumcharacters ;
      private int edtavTfparametrossistema_nomesistema_Visible ;
      private int edtavTfparametrossistema_nomesistema_sel_Visible ;
      private int edtavTfparametrossistema_padronizarstrings_sel_Visible ;
      private int edtavTfparametrossistema_fatorajuste_Visible ;
      private int edtavTfparametrossistema_fatorajuste_to_Visible ;
      private int edtavTfparametrossistema_licensiadocadastrado_sel_Visible ;
      private int edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Visible ;
      private int edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Visible ;
      private int edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV75PageToGo ;
      private int AV88GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV76GridCurrentPage ;
      private long AV77GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV68TFParametrosSistema_FatorAjuste ;
      private decimal AV69TFParametrosSistema_FatorAjuste_To ;
      private decimal A708ParametrosSistema_FatorAjuste ;
      private decimal AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste ;
      private decimal AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_parametrossistema_nomesistema_Activeeventkey ;
      private String Ddo_parametrossistema_nomesistema_Filteredtext_get ;
      private String Ddo_parametrossistema_nomesistema_Selectedvalue_get ;
      private String Ddo_parametrossistema_padronizarstrings_Activeeventkey ;
      private String Ddo_parametrossistema_padronizarstrings_Selectedvalue_get ;
      private String Ddo_parametrossistema_fatorajuste_Activeeventkey ;
      private String Ddo_parametrossistema_fatorajuste_Filteredtext_get ;
      private String Ddo_parametrossistema_fatorajuste_Filteredtextto_get ;
      private String Ddo_parametrossistema_licensiadocadastrado_Activeeventkey ;
      private String Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_33_idx="0001" ;
      private String AV61TFParametrosSistema_NomeSistema ;
      private String AV62TFParametrosSistema_NomeSistema_Sel ;
      private String AV87Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_parametrossistema_nomesistema_Caption ;
      private String Ddo_parametrossistema_nomesistema_Tooltip ;
      private String Ddo_parametrossistema_nomesistema_Cls ;
      private String Ddo_parametrossistema_nomesistema_Filteredtext_set ;
      private String Ddo_parametrossistema_nomesistema_Selectedvalue_set ;
      private String Ddo_parametrossistema_nomesistema_Dropdownoptionstype ;
      private String Ddo_parametrossistema_nomesistema_Titlecontrolidtoreplace ;
      private String Ddo_parametrossistema_nomesistema_Sortedstatus ;
      private String Ddo_parametrossistema_nomesistema_Filtertype ;
      private String Ddo_parametrossistema_nomesistema_Datalisttype ;
      private String Ddo_parametrossistema_nomesistema_Datalistproc ;
      private String Ddo_parametrossistema_nomesistema_Sortasc ;
      private String Ddo_parametrossistema_nomesistema_Sortdsc ;
      private String Ddo_parametrossistema_nomesistema_Loadingdata ;
      private String Ddo_parametrossistema_nomesistema_Cleanfilter ;
      private String Ddo_parametrossistema_nomesistema_Noresultsfound ;
      private String Ddo_parametrossistema_nomesistema_Searchbuttontext ;
      private String Ddo_parametrossistema_padronizarstrings_Caption ;
      private String Ddo_parametrossistema_padronizarstrings_Tooltip ;
      private String Ddo_parametrossistema_padronizarstrings_Cls ;
      private String Ddo_parametrossistema_padronizarstrings_Selectedvalue_set ;
      private String Ddo_parametrossistema_padronizarstrings_Dropdownoptionstype ;
      private String Ddo_parametrossistema_padronizarstrings_Titlecontrolidtoreplace ;
      private String Ddo_parametrossistema_padronizarstrings_Sortedstatus ;
      private String Ddo_parametrossistema_padronizarstrings_Datalisttype ;
      private String Ddo_parametrossistema_padronizarstrings_Datalistfixedvalues ;
      private String Ddo_parametrossistema_padronizarstrings_Sortasc ;
      private String Ddo_parametrossistema_padronizarstrings_Sortdsc ;
      private String Ddo_parametrossistema_padronizarstrings_Cleanfilter ;
      private String Ddo_parametrossistema_padronizarstrings_Searchbuttontext ;
      private String Ddo_parametrossistema_fatorajuste_Caption ;
      private String Ddo_parametrossistema_fatorajuste_Tooltip ;
      private String Ddo_parametrossistema_fatorajuste_Cls ;
      private String Ddo_parametrossistema_fatorajuste_Filteredtext_set ;
      private String Ddo_parametrossistema_fatorajuste_Filteredtextto_set ;
      private String Ddo_parametrossistema_fatorajuste_Dropdownoptionstype ;
      private String Ddo_parametrossistema_fatorajuste_Titlecontrolidtoreplace ;
      private String Ddo_parametrossistema_fatorajuste_Sortedstatus ;
      private String Ddo_parametrossistema_fatorajuste_Filtertype ;
      private String Ddo_parametrossistema_fatorajuste_Sortasc ;
      private String Ddo_parametrossistema_fatorajuste_Sortdsc ;
      private String Ddo_parametrossistema_fatorajuste_Cleanfilter ;
      private String Ddo_parametrossistema_fatorajuste_Rangefilterfrom ;
      private String Ddo_parametrossistema_fatorajuste_Rangefilterto ;
      private String Ddo_parametrossistema_fatorajuste_Searchbuttontext ;
      private String Ddo_parametrossistema_licensiadocadastrado_Caption ;
      private String Ddo_parametrossistema_licensiadocadastrado_Tooltip ;
      private String Ddo_parametrossistema_licensiadocadastrado_Cls ;
      private String Ddo_parametrossistema_licensiadocadastrado_Selectedvalue_set ;
      private String Ddo_parametrossistema_licensiadocadastrado_Dropdownoptionstype ;
      private String Ddo_parametrossistema_licensiadocadastrado_Titlecontrolidtoreplace ;
      private String Ddo_parametrossistema_licensiadocadastrado_Sortedstatus ;
      private String Ddo_parametrossistema_licensiadocadastrado_Datalisttype ;
      private String Ddo_parametrossistema_licensiadocadastrado_Datalistfixedvalues ;
      private String Ddo_parametrossistema_licensiadocadastrado_Sortasc ;
      private String Ddo_parametrossistema_licensiadocadastrado_Sortdsc ;
      private String Ddo_parametrossistema_licensiadocadastrado_Cleanfilter ;
      private String Ddo_parametrossistema_licensiadocadastrado_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfparametrossistema_nomesistema_Internalname ;
      private String edtavTfparametrossistema_nomesistema_Jsonclick ;
      private String edtavTfparametrossistema_nomesistema_sel_Internalname ;
      private String edtavTfparametrossistema_nomesistema_sel_Jsonclick ;
      private String edtavTfparametrossistema_padronizarstrings_sel_Internalname ;
      private String edtavTfparametrossistema_padronizarstrings_sel_Jsonclick ;
      private String edtavTfparametrossistema_fatorajuste_Internalname ;
      private String edtavTfparametrossistema_fatorajuste_Jsonclick ;
      private String edtavTfparametrossistema_fatorajuste_to_Internalname ;
      private String edtavTfparametrossistema_fatorajuste_to_Jsonclick ;
      private String edtavTfparametrossistema_licensiadocadastrado_sel_Internalname ;
      private String edtavTfparametrossistema_licensiadocadastrado_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_parametrossistema_nomesistematitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_parametrossistema_padronizarstringstitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_parametrossistema_fatorajustetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_parametrossistema_licensiadocadastradotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtParametrosSistema_Codigo_Internalname ;
      private String A331ParametrosSistema_NomeSistema ;
      private String edtParametrosSistema_NomeSistema_Internalname ;
      private String cmbParametrosSistema_PadronizarStrings_Internalname ;
      private String edtParametrosSistema_FatorAjuste_Internalname ;
      private String cmbParametrosSistema_LicensiadoCadastrado_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema ;
      private String AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel ;
      private String AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_parametrossistema_nomesistema_Internalname ;
      private String Ddo_parametrossistema_padronizarstrings_Internalname ;
      private String Ddo_parametrossistema_fatorajuste_Internalname ;
      private String Ddo_parametrossistema_licensiadocadastrado_Internalname ;
      private String edtParametrosSistema_NomeSistema_Title ;
      private String edtParametrosSistema_FatorAjuste_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtParametrosSistema_NomeSistema_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblParametrossistematitle_Internalname ;
      private String lblParametrossistematitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_33_fel_idx="0001" ;
      private String ROClassString ;
      private String edtParametrosSistema_Codigo_Jsonclick ;
      private String edtParametrosSistema_NomeSistema_Jsonclick ;
      private String cmbParametrosSistema_PadronizarStrings_Jsonclick ;
      private String edtParametrosSistema_FatorAjuste_Jsonclick ;
      private String cmbParametrosSistema_LicensiadoCadastrado_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_parametrossistema_nomesistema_Includesortasc ;
      private bool Ddo_parametrossistema_nomesistema_Includesortdsc ;
      private bool Ddo_parametrossistema_nomesistema_Includefilter ;
      private bool Ddo_parametrossistema_nomesistema_Filterisrange ;
      private bool Ddo_parametrossistema_nomesistema_Includedatalist ;
      private bool Ddo_parametrossistema_padronizarstrings_Includesortasc ;
      private bool Ddo_parametrossistema_padronizarstrings_Includesortdsc ;
      private bool Ddo_parametrossistema_padronizarstrings_Includefilter ;
      private bool Ddo_parametrossistema_padronizarstrings_Includedatalist ;
      private bool Ddo_parametrossistema_fatorajuste_Includesortasc ;
      private bool Ddo_parametrossistema_fatorajuste_Includesortdsc ;
      private bool Ddo_parametrossistema_fatorajuste_Includefilter ;
      private bool Ddo_parametrossistema_fatorajuste_Filterisrange ;
      private bool Ddo_parametrossistema_fatorajuste_Includedatalist ;
      private bool Ddo_parametrossistema_licensiadocadastrado_Includesortasc ;
      private bool Ddo_parametrossistema_licensiadocadastrado_Includesortdsc ;
      private bool Ddo_parametrossistema_licensiadocadastrado_Includefilter ;
      private bool Ddo_parametrossistema_licensiadocadastrado_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A417ParametrosSistema_PadronizarStrings ;
      private bool n708ParametrosSistema_FatorAjuste ;
      private bool A399ParametrosSistema_LicensiadoCadastrado ;
      private bool n399ParametrosSistema_LicensiadoCadastrado ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Update_IsBlob ;
      private String AV63ddo_ParametrosSistema_NomeSistemaTitleControlIdToReplace ;
      private String AV66ddo_ParametrosSistema_PadronizarStringsTitleControlIdToReplace ;
      private String AV70ddo_ParametrosSistema_FatorAjusteTitleControlIdToReplace ;
      private String AV73ddo_ParametrosSistema_LicensiadoCadastradoTitleControlIdToReplace ;
      private String AV86Update_GXI ;
      private String AV29Update ;
      private IGxSession AV51Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbParametrosSistema_PadronizarStrings ;
      private GXCombobox cmbParametrosSistema_LicensiadoCadastrado ;
      private IDataStoreProvider pr_default ;
      private bool[] H008I2_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] H008I2_n399ParametrosSistema_LicensiadoCadastrado ;
      private decimal[] H008I2_A708ParametrosSistema_FatorAjuste ;
      private bool[] H008I2_n708ParametrosSistema_FatorAjuste ;
      private bool[] H008I2_A417ParametrosSistema_PadronizarStrings ;
      private String[] H008I2_A331ParametrosSistema_NomeSistema ;
      private int[] H008I2_A330ParametrosSistema_Codigo ;
      private long[] H008I3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60ParametrosSistema_NomeSistemaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64ParametrosSistema_PadronizarStringsTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV67ParametrosSistema_FatorAjusteTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV71ParametrosSistema_LicensiadoCadastradoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV74DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwparametrossistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H008I2( IGxContext context ,
                                             String AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel ,
                                             String AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema ,
                                             short AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel ,
                                             decimal AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste ,
                                             decimal AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to ,
                                             short AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel ,
                                             String A331ParametrosSistema_NomeSistema ,
                                             bool A417ParametrosSistema_PadronizarStrings ,
                                             decimal A708ParametrosSistema_FatorAjuste ,
                                             bool A399ParametrosSistema_LicensiadoCadastrado ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [9] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_FatorAjuste], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_NomeSistema], [ParametrosSistema_Codigo]";
         sFromString = " FROM [ParametrosSistema] WITH (NOLOCK)";
         sOrderString = "";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_NomeSistema] like @lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_NomeSistema] like @lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_NomeSistema] = @AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_NomeSistema] = @AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_PadronizarStrings] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_PadronizarStrings] = 1)";
            }
         }
         if ( AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_PadronizarStrings] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_PadronizarStrings] = 0)";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_FatorAjuste] >= @AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_FatorAjuste] >= @AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_FatorAjuste] <= @AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_FatorAjuste] <= @AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_LicensiadoCadastrado] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_LicensiadoCadastrado] = 1)";
            }
         }
         if ( AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_LicensiadoCadastrado] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_LicensiadoCadastrado] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_NomeSistema]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_NomeSistema] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_PadronizarStrings]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_PadronizarStrings] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_FatorAjuste]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_FatorAjuste] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_LicensiadoCadastrado]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_LicensiadoCadastrado] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ParametrosSistema_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H008I3( IGxContext context ,
                                             String AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel ,
                                             String AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema ,
                                             short AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel ,
                                             decimal AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste ,
                                             decimal AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to ,
                                             short AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel ,
                                             String A331ParametrosSistema_NomeSistema ,
                                             bool A417ParametrosSistema_PadronizarStrings ,
                                             decimal A708ParametrosSistema_FatorAjuste ,
                                             bool A399ParametrosSistema_LicensiadoCadastrado ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ParametrosSistema] WITH (NOLOCK)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_NomeSistema] like @lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_NomeSistema] like @lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_NomeSistema] = @AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_NomeSistema] = @AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_PadronizarStrings] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_PadronizarStrings] = 1)";
            }
         }
         if ( AV82WWParametrosSistemaDS_3_Tfparametrossistema_padronizarstrings_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_PadronizarStrings] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_PadronizarStrings] = 0)";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_FatorAjuste] >= @AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_FatorAjuste] >= @AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_FatorAjuste] <= @AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_FatorAjuste] <= @AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_LicensiadoCadastrado] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_LicensiadoCadastrado] = 1)";
            }
         }
         if ( AV85WWParametrosSistemaDS_6_Tfparametrossistema_licensiadocadastrado_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosSistema_LicensiadoCadastrado] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosSistema_LicensiadoCadastrado] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H008I2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (decimal)dynConstraints[8] , (bool)dynConstraints[9] , (short)dynConstraints[10] , (bool)dynConstraints[11] );
               case 1 :
                     return conditional_H008I3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (decimal)dynConstraints[8] , (bool)dynConstraints[9] , (short)dynConstraints[10] , (bool)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008I2 ;
          prmH008I2 = new Object[] {
          new Object[] {"@lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH008I3 ;
          prmH008I3 = new Object[] {
          new Object[] {"@lV80WWParametrosSistemaDS_1_Tfparametrossistema_nomesistema",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWParametrosSistemaDS_2_Tfparametrossistema_nomesistema_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWParametrosSistemaDS_4_Tfparametrossistema_fatorajuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV84WWParametrosSistemaDS_5_Tfparametrossistema_fatorajuste_to",SqlDbType.Decimal,6,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008I2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008I2,11,0,true,false )
             ,new CursorDef("H008I3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008I3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[7]);
                }
                return;
       }
    }

 }

}
