/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:19:23.36
*/
gx.evt.autoSkip = false;
gx.define('modulo', false, function () {
   this.ServerClass =  "modulo" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7Modulo_Codigo=gx.fn.getIntegerValue("vMODULO_CODIGO",'.') ;
      this.AV11Insert_Sistema_Codigo=gx.fn.getIntegerValue("vINSERT_SISTEMA_CODIGO",'.') ;
      this.AV15Sistema_Codigo=gx.fn.getIntegerValue("vSISTEMA_CODIGO",'.') ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.A127Sistema_Codigo=gx.fn.getIntegerValue("SISTEMA_CODIGO",'.') ;
      this.A416Sistema_Nome=gx.fn.getControlValue("SISTEMA_NOME") ;
      this.AV17Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
   };
   this.Valid_Modulo_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("MODULO_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Modulo_nome=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("MODULO_NOME");
         this.AnyError  = 0;
         if ( ((''==this.A143Modulo_Nome)) )
         {
            try {
               gxballoon.setError("Módulo é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Modulo_sigla=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("MODULO_SIGLA");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e120r2_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e130r2_client=function()
   {
      this.executeServerEvent("'DOCOPIARCOLARMODULOS'", false, null, false, false);
   };
   this.e140r28_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e150r28_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,13,16,18,20,22,25,27,30,40];
   this.GXLastCtrlId =40;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 11, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setDynProp("Title", "Title", "Módulo", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[13]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[16]={fld:"TEXTBLOCKMODULO_NOME", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Modulo_nome,isvalid:null,rgrid:[],fld:"MODULO_NOME",gxz:"Z143Modulo_Nome",gxold:"O143Modulo_Nome",gxvar:"A143Modulo_Nome",ucs:[],op:[18],ip:[18],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A143Modulo_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z143Modulo_Nome=Value},v2c:function(){gx.fn.setControlValue("MODULO_NOME",gx.O.A143Modulo_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A143Modulo_Nome=this.val()},val:function(){return gx.fn.getControlValue("MODULO_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 18 , function() {
   });
   GXValidFnc[20]={fld:"TEXTBLOCKMODULO_SIGLA", format:0,grid:0};
   GXValidFnc[22]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Modulo_sigla,isvalid:null,rgrid:[],fld:"MODULO_SIGLA",gxz:"Z145Modulo_Sigla",gxold:"O145Modulo_Sigla",gxvar:"A145Modulo_Sigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A145Modulo_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z145Modulo_Sigla=Value},v2c:function(){gx.fn.setControlValue("MODULO_SIGLA",gx.O.A145Modulo_Sigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A145Modulo_Sigla=this.val()},val:function(){return gx.fn.getControlValue("MODULO_SIGLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 22 , function() {
   });
   GXValidFnc[25]={fld:"TEXTBLOCKMODULO_DESCRICAO", format:0,grid:0};
   GXValidFnc[27]={lvl:0,type:"vchar",len:500,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"MODULO_DESCRICAO",gxz:"Z144Modulo_Descricao",gxold:"O144Modulo_Descricao",gxvar:"A144Modulo_Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A144Modulo_Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z144Modulo_Descricao=Value},v2c:function(){gx.fn.setControlValue("MODULO_DESCRICAO",gx.O.A144Modulo_Descricao,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A144Modulo_Descricao=this.val()},val:function(){return gx.fn.getControlValue("MODULO_DESCRICAO")},nac:gx.falseFn};
   this.declareDomainHdlr( 27 , function() {
   });
   GXValidFnc[30]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[40]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Modulo_codigo,isvalid:null,rgrid:[],fld:"MODULO_CODIGO",gxz:"Z146Modulo_Codigo",gxold:"O146Modulo_Codigo",gxvar:"A146Modulo_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A146Modulo_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z146Modulo_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("MODULO_CODIGO",gx.O.A146Modulo_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A146Modulo_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("MODULO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 40 , function() {
   });
   this.A143Modulo_Nome = "" ;
   this.Z143Modulo_Nome = "" ;
   this.O143Modulo_Nome = "" ;
   this.A145Modulo_Sigla = "" ;
   this.Z145Modulo_Sigla = "" ;
   this.O145Modulo_Sigla = "" ;
   this.A144Modulo_Descricao = "" ;
   this.Z144Modulo_Descricao = "" ;
   this.O144Modulo_Descricao = "" ;
   this.A146Modulo_Codigo = 0 ;
   this.Z146Modulo_Codigo = 0 ;
   this.O146Modulo_Codigo = 0 ;
   this.AV8WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV18GXV1 = 0 ;
   this.AV11Insert_Sistema_Codigo = 0 ;
   this.AV14Tabela_ModuloCod = 0 ;
   this.AV12TrnContextAtt = {} ;
   this.AV7Modulo_Codigo = 0 ;
   this.AV15Sistema_Codigo = 0 ;
   this.AV10WebSession = {} ;
   this.A146Modulo_Codigo = 0 ;
   this.A127Sistema_Codigo = 0 ;
   this.AV17Pgmname = "" ;
   this.Gx_BScreen = 0 ;
   this.A143Modulo_Nome = "" ;
   this.A145Modulo_Sigla = "" ;
   this.A144Modulo_Descricao = "" ;
   this.A416Sistema_Nome = "" ;
   this.Gx_mode = "" ;
   this.Events = {"e120r2_client": ["AFTER TRN", true] ,"e130r2_client": ["'DOCOPIARCOLARMODULOS'", true] ,"e140r28_client": ["ENTER", true] ,"e150r28_client": ["CANCEL", true]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[]];
   this.EvtParms["'DOCOPIARCOLARMODULOS'"] = [[],[]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("AV7Modulo_Codigo", "vMODULO_CODIGO", 0, "int");
   this.setVCMap("AV11Insert_Sistema_Codigo", "vINSERT_SISTEMA_CODIGO", 0, "int");
   this.setVCMap("AV15Sistema_Codigo", "vSISTEMA_CODIGO", 0, "int");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("A127Sistema_Codigo", "SISTEMA_CODIGO", 0, "int");
   this.setVCMap("A416Sistema_Nome", "SISTEMA_NOME", 0, "svchar");
   this.setVCMap("AV17Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.InitStandaloneVars( );
   this.LvlOlds[ 28 ]= ["O143Modulo_Nome"] ;
});
gx.createParentObj(modulo);
