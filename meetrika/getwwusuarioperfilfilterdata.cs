/*
               File: GetWWUsuarioPerfilFilterData
        Description: Get WWUsuario Perfil Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:57.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwusuarioperfilfilterdata : GXProcedure
   {
      public getwwusuarioperfilfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwusuarioperfilfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwusuarioperfilfilterdata objgetwwusuarioperfilfilterdata;
         objgetwwusuarioperfilfilterdata = new getwwusuarioperfilfilterdata();
         objgetwwusuarioperfilfilterdata.AV24DDOName = aP0_DDOName;
         objgetwwusuarioperfilfilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwwusuarioperfilfilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwwusuarioperfilfilterdata.AV28OptionsJson = "" ;
         objgetwwusuarioperfilfilterdata.AV31OptionsDescJson = "" ;
         objgetwwusuarioperfilfilterdata.AV33OptionIndexesJson = "" ;
         objgetwwusuarioperfilfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwusuarioperfilfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwusuarioperfilfilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwusuarioperfilfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_USUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_USUARIO_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_PESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_PERFIL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPERFIL_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WWUsuarioPerfilGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWUsuarioPerfilGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WWUsuarioPerfilGridState"), "");
         }
         AV49GXV1 = 1;
         while ( AV49GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV49GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV10TFUsuario_Nome = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV11TFUsuario_Nome_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM") == 0 )
            {
               AV12TFUsuario_PessoaNom = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM_SEL") == 0 )
            {
               AV13TFUsuario_PessoaNom_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME") == 0 )
            {
               AV14TFPerfil_Nome = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME_SEL") == 0 )
            {
               AV15TFPerfil_Nome_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFPERFIL_TIPO_SEL") == 0 )
            {
               AV16TFPerfil_Tipo_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV17TFPerfil_Tipo_Sels.FromJSonString(AV16TFPerfil_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_INSERT_SEL") == 0 )
            {
               AV18TFUsuarioPerfil_Insert_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_UPDATE_SEL") == 0 )
            {
               AV19TFUsuarioPerfil_Update_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_DELETE_SEL") == 0 )
            {
               AV20TFUsuarioPerfil_Delete_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_DISPLAY_SEL") == 0 )
            {
               AV21TFUsuarioPerfil_Display_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            AV49GXV1 = (int)(AV49GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
            {
               AV41Perfil_Nome1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
            {
               AV42Usuario_PessoaNom1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "PERFIL_NOME") == 0 )
               {
                  AV45Perfil_Nome2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
               {
                  AV46Usuario_PessoaNom2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUSUARIO_NOMEOPTIONS' Routine */
         AV10TFUsuario_Nome = AV22SearchTxt;
         AV11TFUsuario_Nome_Sel = "";
         AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV52WWUsuarioPerfilDS_2_Perfil_nome1 = AV41Perfil_Nome1;
         AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV42Usuario_PessoaNom1;
         AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV56WWUsuarioPerfilDS_6_Perfil_nome2 = AV45Perfil_Nome2;
         AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV46Usuario_PessoaNom2;
         AV58WWUsuarioPerfilDS_8_Tfusuario_nome = AV10TFUsuario_Nome;
         AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV11TFUsuario_Nome_Sel;
         AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV12TFUsuario_PessoaNom;
         AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV13TFUsuario_PessoaNom_Sel;
         AV62WWUsuarioPerfilDS_12_Tfperfil_nome = AV14TFPerfil_Nome;
         AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV15TFPerfil_Nome_Sel;
         AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV17TFPerfil_Tipo_Sels;
         AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV18TFUsuarioPerfil_Insert_Sel;
         AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV19TFUsuarioPerfil_Update_Sel;
         AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV20TFUsuarioPerfil_Delete_Sel;
         AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV21TFUsuarioPerfil_Display_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                              AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                              AV52WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                              AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                              AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                              AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                              AV56WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                              AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                              AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                              AV58WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                              AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                              AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                              AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                              AV62WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                              AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels.Count ,
                                              AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                              AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                              AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                              AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome ,
                                              A544UsuarioPerfil_Insert ,
                                              A659UsuarioPerfil_Update ,
                                              A546UsuarioPerfil_Delete ,
                                              A543UsuarioPerfil_Display },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV52WWUsuarioPerfilDS_2_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV52WWUsuarioPerfilDS_2_Perfil_nome1), 50, "%");
         lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1), 100, "%");
         lV56WWUsuarioPerfilDS_6_Perfil_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWUsuarioPerfilDS_6_Perfil_nome2), 50, "%");
         lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2), 100, "%");
         lV58WWUsuarioPerfilDS_8_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWUsuarioPerfilDS_8_Tfusuario_nome), 50, "%");
         lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom), 100, "%");
         lV62WWUsuarioPerfilDS_12_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV62WWUsuarioPerfilDS_12_Tfperfil_nome), 50, "%");
         /* Using cursor P00LF2 */
         pr_default.execute(0, new Object[] {lV52WWUsuarioPerfilDS_2_Perfil_nome1, lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1, lV56WWUsuarioPerfilDS_6_Perfil_nome2, lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2, lV58WWUsuarioPerfilDS_8_Tfusuario_nome, AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel, lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom, AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel, lV62WWUsuarioPerfilDS_12_Tfperfil_nome, AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLF2 = false;
            A1Usuario_Codigo = P00LF2_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00LF2_A57Usuario_PessoaCod[0];
            A3Perfil_Codigo = P00LF2_A3Perfil_Codigo[0];
            A2Usuario_Nome = P00LF2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LF2_n2Usuario_Nome[0];
            A543UsuarioPerfil_Display = P00LF2_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = P00LF2_n543UsuarioPerfil_Display[0];
            A546UsuarioPerfil_Delete = P00LF2_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = P00LF2_n546UsuarioPerfil_Delete[0];
            A659UsuarioPerfil_Update = P00LF2_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = P00LF2_n659UsuarioPerfil_Update[0];
            A544UsuarioPerfil_Insert = P00LF2_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = P00LF2_n544UsuarioPerfil_Insert[0];
            A275Perfil_Tipo = P00LF2_A275Perfil_Tipo[0];
            A58Usuario_PessoaNom = P00LF2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LF2_n58Usuario_PessoaNom[0];
            A4Perfil_Nome = P00LF2_A4Perfil_Nome[0];
            A57Usuario_PessoaCod = P00LF2_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00LF2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LF2_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LF2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LF2_n58Usuario_PessoaNom[0];
            A275Perfil_Tipo = P00LF2_A275Perfil_Tipo[0];
            A4Perfil_Nome = P00LF2_A4Perfil_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LF2_A2Usuario_Nome[0], A2Usuario_Nome) == 0 ) )
            {
               BRKLF2 = false;
               A1Usuario_Codigo = P00LF2_A1Usuario_Codigo[0];
               A3Perfil_Codigo = P00LF2_A3Perfil_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKLF2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) )
            {
               AV26Option = A2Usuario_Nome;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLF2 )
            {
               BRKLF2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADUSUARIO_PESSOANOMOPTIONS' Routine */
         AV12TFUsuario_PessoaNom = AV22SearchTxt;
         AV13TFUsuario_PessoaNom_Sel = "";
         AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV52WWUsuarioPerfilDS_2_Perfil_nome1 = AV41Perfil_Nome1;
         AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV42Usuario_PessoaNom1;
         AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV56WWUsuarioPerfilDS_6_Perfil_nome2 = AV45Perfil_Nome2;
         AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV46Usuario_PessoaNom2;
         AV58WWUsuarioPerfilDS_8_Tfusuario_nome = AV10TFUsuario_Nome;
         AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV11TFUsuario_Nome_Sel;
         AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV12TFUsuario_PessoaNom;
         AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV13TFUsuario_PessoaNom_Sel;
         AV62WWUsuarioPerfilDS_12_Tfperfil_nome = AV14TFPerfil_Nome;
         AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV15TFPerfil_Nome_Sel;
         AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV17TFPerfil_Tipo_Sels;
         AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV18TFUsuarioPerfil_Insert_Sel;
         AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV19TFUsuarioPerfil_Update_Sel;
         AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV20TFUsuarioPerfil_Delete_Sel;
         AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV21TFUsuarioPerfil_Display_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                              AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                              AV52WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                              AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                              AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                              AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                              AV56WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                              AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                              AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                              AV58WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                              AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                              AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                              AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                              AV62WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                              AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels.Count ,
                                              AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                              AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                              AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                              AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome ,
                                              A544UsuarioPerfil_Insert ,
                                              A659UsuarioPerfil_Update ,
                                              A546UsuarioPerfil_Delete ,
                                              A543UsuarioPerfil_Display },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV52WWUsuarioPerfilDS_2_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV52WWUsuarioPerfilDS_2_Perfil_nome1), 50, "%");
         lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1), 100, "%");
         lV56WWUsuarioPerfilDS_6_Perfil_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWUsuarioPerfilDS_6_Perfil_nome2), 50, "%");
         lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2), 100, "%");
         lV58WWUsuarioPerfilDS_8_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWUsuarioPerfilDS_8_Tfusuario_nome), 50, "%");
         lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom), 100, "%");
         lV62WWUsuarioPerfilDS_12_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV62WWUsuarioPerfilDS_12_Tfperfil_nome), 50, "%");
         /* Using cursor P00LF3 */
         pr_default.execute(1, new Object[] {lV52WWUsuarioPerfilDS_2_Perfil_nome1, lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1, lV56WWUsuarioPerfilDS_6_Perfil_nome2, lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2, lV58WWUsuarioPerfilDS_8_Tfusuario_nome, AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel, lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom, AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel, lV62WWUsuarioPerfilDS_12_Tfperfil_nome, AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKLF4 = false;
            A1Usuario_Codigo = P00LF3_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00LF3_A57Usuario_PessoaCod[0];
            A3Perfil_Codigo = P00LF3_A3Perfil_Codigo[0];
            A58Usuario_PessoaNom = P00LF3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LF3_n58Usuario_PessoaNom[0];
            A543UsuarioPerfil_Display = P00LF3_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = P00LF3_n543UsuarioPerfil_Display[0];
            A546UsuarioPerfil_Delete = P00LF3_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = P00LF3_n546UsuarioPerfil_Delete[0];
            A659UsuarioPerfil_Update = P00LF3_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = P00LF3_n659UsuarioPerfil_Update[0];
            A544UsuarioPerfil_Insert = P00LF3_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = P00LF3_n544UsuarioPerfil_Insert[0];
            A275Perfil_Tipo = P00LF3_A275Perfil_Tipo[0];
            A2Usuario_Nome = P00LF3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LF3_n2Usuario_Nome[0];
            A4Perfil_Nome = P00LF3_A4Perfil_Nome[0];
            A57Usuario_PessoaCod = P00LF3_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00LF3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LF3_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LF3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LF3_n58Usuario_PessoaNom[0];
            A275Perfil_Tipo = P00LF3_A275Perfil_Tipo[0];
            A4Perfil_Nome = P00LF3_A4Perfil_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00LF3_A58Usuario_PessoaNom[0], A58Usuario_PessoaNom) == 0 ) )
            {
               BRKLF4 = false;
               A1Usuario_Codigo = P00LF3_A1Usuario_Codigo[0];
               A57Usuario_PessoaCod = P00LF3_A57Usuario_PessoaCod[0];
               A3Perfil_Codigo = P00LF3_A3Perfil_Codigo[0];
               A57Usuario_PessoaCod = P00LF3_A57Usuario_PessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKLF4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A58Usuario_PessoaNom)) )
            {
               AV26Option = A58Usuario_PessoaNom;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLF4 )
            {
               BRKLF4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADPERFIL_NOMEOPTIONS' Routine */
         AV14TFPerfil_Nome = AV22SearchTxt;
         AV15TFPerfil_Nome_Sel = "";
         AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV52WWUsuarioPerfilDS_2_Perfil_nome1 = AV41Perfil_Nome1;
         AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV42Usuario_PessoaNom1;
         AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV56WWUsuarioPerfilDS_6_Perfil_nome2 = AV45Perfil_Nome2;
         AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV46Usuario_PessoaNom2;
         AV58WWUsuarioPerfilDS_8_Tfusuario_nome = AV10TFUsuario_Nome;
         AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV11TFUsuario_Nome_Sel;
         AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV12TFUsuario_PessoaNom;
         AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV13TFUsuario_PessoaNom_Sel;
         AV62WWUsuarioPerfilDS_12_Tfperfil_nome = AV14TFPerfil_Nome;
         AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV15TFPerfil_Nome_Sel;
         AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV17TFPerfil_Tipo_Sels;
         AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV18TFUsuarioPerfil_Insert_Sel;
         AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV19TFUsuarioPerfil_Update_Sel;
         AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV20TFUsuarioPerfil_Delete_Sel;
         AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV21TFUsuarioPerfil_Display_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                              AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                              AV52WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                              AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                              AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                              AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                              AV56WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                              AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                              AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                              AV58WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                              AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                              AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                              AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                              AV62WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                              AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels.Count ,
                                              AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                              AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                              AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                              AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome ,
                                              A544UsuarioPerfil_Insert ,
                                              A659UsuarioPerfil_Update ,
                                              A546UsuarioPerfil_Delete ,
                                              A543UsuarioPerfil_Display },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV52WWUsuarioPerfilDS_2_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV52WWUsuarioPerfilDS_2_Perfil_nome1), 50, "%");
         lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1), 100, "%");
         lV56WWUsuarioPerfilDS_6_Perfil_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWUsuarioPerfilDS_6_Perfil_nome2), 50, "%");
         lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2), 100, "%");
         lV58WWUsuarioPerfilDS_8_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWUsuarioPerfilDS_8_Tfusuario_nome), 50, "%");
         lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom), 100, "%");
         lV62WWUsuarioPerfilDS_12_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV62WWUsuarioPerfilDS_12_Tfperfil_nome), 50, "%");
         /* Using cursor P00LF4 */
         pr_default.execute(2, new Object[] {lV52WWUsuarioPerfilDS_2_Perfil_nome1, lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1, lV56WWUsuarioPerfilDS_6_Perfil_nome2, lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2, lV58WWUsuarioPerfilDS_8_Tfusuario_nome, AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel, lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom, AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel, lV62WWUsuarioPerfilDS_12_Tfperfil_nome, AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKLF6 = false;
            A1Usuario_Codigo = P00LF4_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00LF4_A57Usuario_PessoaCod[0];
            A3Perfil_Codigo = P00LF4_A3Perfil_Codigo[0];
            A543UsuarioPerfil_Display = P00LF4_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = P00LF4_n543UsuarioPerfil_Display[0];
            A546UsuarioPerfil_Delete = P00LF4_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = P00LF4_n546UsuarioPerfil_Delete[0];
            A659UsuarioPerfil_Update = P00LF4_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = P00LF4_n659UsuarioPerfil_Update[0];
            A544UsuarioPerfil_Insert = P00LF4_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = P00LF4_n544UsuarioPerfil_Insert[0];
            A275Perfil_Tipo = P00LF4_A275Perfil_Tipo[0];
            A2Usuario_Nome = P00LF4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LF4_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LF4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LF4_n58Usuario_PessoaNom[0];
            A4Perfil_Nome = P00LF4_A4Perfil_Nome[0];
            A57Usuario_PessoaCod = P00LF4_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00LF4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LF4_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LF4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LF4_n58Usuario_PessoaNom[0];
            A275Perfil_Tipo = P00LF4_A275Perfil_Tipo[0];
            A4Perfil_Nome = P00LF4_A4Perfil_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00LF4_A3Perfil_Codigo[0] == A3Perfil_Codigo ) )
            {
               BRKLF6 = false;
               A1Usuario_Codigo = P00LF4_A1Usuario_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKLF6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A4Perfil_Nome)) )
            {
               AV26Option = A4Perfil_Nome;
               AV25InsertIndex = 1;
               while ( ( AV25InsertIndex <= AV27Options.Count ) && ( StringUtil.StrCmp(((String)AV27Options.Item(AV25InsertIndex)), AV26Option) < 0 ) )
               {
                  AV25InsertIndex = (int)(AV25InsertIndex+1);
               }
               AV27Options.Add(AV26Option, AV25InsertIndex);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), AV25InsertIndex);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLF6 )
            {
               BRKLF6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUsuario_Nome = "";
         AV11TFUsuario_Nome_Sel = "";
         AV12TFUsuario_PessoaNom = "";
         AV13TFUsuario_PessoaNom_Sel = "";
         AV14TFPerfil_Nome = "";
         AV15TFPerfil_Nome_Sel = "";
         AV16TFPerfil_Tipo_SelsJson = "";
         AV17TFPerfil_Tipo_Sels = new GxSimpleCollection();
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV41Perfil_Nome1 = "";
         AV42Usuario_PessoaNom1 = "";
         AV44DynamicFiltersSelector2 = "";
         AV45Perfil_Nome2 = "";
         AV46Usuario_PessoaNom2 = "";
         AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = "";
         AV52WWUsuarioPerfilDS_2_Perfil_nome1 = "";
         AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = "";
         AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = "";
         AV56WWUsuarioPerfilDS_6_Perfil_nome2 = "";
         AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = "";
         AV58WWUsuarioPerfilDS_8_Tfusuario_nome = "";
         AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel = "";
         AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = "";
         AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = "";
         AV62WWUsuarioPerfilDS_12_Tfperfil_nome = "";
         AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel = "";
         AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV52WWUsuarioPerfilDS_2_Perfil_nome1 = "";
         lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 = "";
         lV56WWUsuarioPerfilDS_6_Perfil_nome2 = "";
         lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 = "";
         lV58WWUsuarioPerfilDS_8_Tfusuario_nome = "";
         lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom = "";
         lV62WWUsuarioPerfilDS_12_Tfperfil_nome = "";
         A4Perfil_Nome = "";
         A58Usuario_PessoaNom = "";
         A2Usuario_Nome = "";
         P00LF2_A1Usuario_Codigo = new int[1] ;
         P00LF2_A57Usuario_PessoaCod = new int[1] ;
         P00LF2_A3Perfil_Codigo = new int[1] ;
         P00LF2_A2Usuario_Nome = new String[] {""} ;
         P00LF2_n2Usuario_Nome = new bool[] {false} ;
         P00LF2_A543UsuarioPerfil_Display = new bool[] {false} ;
         P00LF2_n543UsuarioPerfil_Display = new bool[] {false} ;
         P00LF2_A546UsuarioPerfil_Delete = new bool[] {false} ;
         P00LF2_n546UsuarioPerfil_Delete = new bool[] {false} ;
         P00LF2_A659UsuarioPerfil_Update = new bool[] {false} ;
         P00LF2_n659UsuarioPerfil_Update = new bool[] {false} ;
         P00LF2_A544UsuarioPerfil_Insert = new bool[] {false} ;
         P00LF2_n544UsuarioPerfil_Insert = new bool[] {false} ;
         P00LF2_A275Perfil_Tipo = new short[1] ;
         P00LF2_A58Usuario_PessoaNom = new String[] {""} ;
         P00LF2_n58Usuario_PessoaNom = new bool[] {false} ;
         P00LF2_A4Perfil_Nome = new String[] {""} ;
         AV26Option = "";
         P00LF3_A1Usuario_Codigo = new int[1] ;
         P00LF3_A57Usuario_PessoaCod = new int[1] ;
         P00LF3_A3Perfil_Codigo = new int[1] ;
         P00LF3_A58Usuario_PessoaNom = new String[] {""} ;
         P00LF3_n58Usuario_PessoaNom = new bool[] {false} ;
         P00LF3_A543UsuarioPerfil_Display = new bool[] {false} ;
         P00LF3_n543UsuarioPerfil_Display = new bool[] {false} ;
         P00LF3_A546UsuarioPerfil_Delete = new bool[] {false} ;
         P00LF3_n546UsuarioPerfil_Delete = new bool[] {false} ;
         P00LF3_A659UsuarioPerfil_Update = new bool[] {false} ;
         P00LF3_n659UsuarioPerfil_Update = new bool[] {false} ;
         P00LF3_A544UsuarioPerfil_Insert = new bool[] {false} ;
         P00LF3_n544UsuarioPerfil_Insert = new bool[] {false} ;
         P00LF3_A275Perfil_Tipo = new short[1] ;
         P00LF3_A2Usuario_Nome = new String[] {""} ;
         P00LF3_n2Usuario_Nome = new bool[] {false} ;
         P00LF3_A4Perfil_Nome = new String[] {""} ;
         P00LF4_A1Usuario_Codigo = new int[1] ;
         P00LF4_A57Usuario_PessoaCod = new int[1] ;
         P00LF4_A3Perfil_Codigo = new int[1] ;
         P00LF4_A543UsuarioPerfil_Display = new bool[] {false} ;
         P00LF4_n543UsuarioPerfil_Display = new bool[] {false} ;
         P00LF4_A546UsuarioPerfil_Delete = new bool[] {false} ;
         P00LF4_n546UsuarioPerfil_Delete = new bool[] {false} ;
         P00LF4_A659UsuarioPerfil_Update = new bool[] {false} ;
         P00LF4_n659UsuarioPerfil_Update = new bool[] {false} ;
         P00LF4_A544UsuarioPerfil_Insert = new bool[] {false} ;
         P00LF4_n544UsuarioPerfil_Insert = new bool[] {false} ;
         P00LF4_A275Perfil_Tipo = new short[1] ;
         P00LF4_A2Usuario_Nome = new String[] {""} ;
         P00LF4_n2Usuario_Nome = new bool[] {false} ;
         P00LF4_A58Usuario_PessoaNom = new String[] {""} ;
         P00LF4_n58Usuario_PessoaNom = new bool[] {false} ;
         P00LF4_A4Perfil_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwusuarioperfilfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LF2_A1Usuario_Codigo, P00LF2_A57Usuario_PessoaCod, P00LF2_A3Perfil_Codigo, P00LF2_A2Usuario_Nome, P00LF2_n2Usuario_Nome, P00LF2_A543UsuarioPerfil_Display, P00LF2_n543UsuarioPerfil_Display, P00LF2_A546UsuarioPerfil_Delete, P00LF2_n546UsuarioPerfil_Delete, P00LF2_A659UsuarioPerfil_Update,
               P00LF2_n659UsuarioPerfil_Update, P00LF2_A544UsuarioPerfil_Insert, P00LF2_n544UsuarioPerfil_Insert, P00LF2_A275Perfil_Tipo, P00LF2_A58Usuario_PessoaNom, P00LF2_n58Usuario_PessoaNom, P00LF2_A4Perfil_Nome
               }
               , new Object[] {
               P00LF3_A1Usuario_Codigo, P00LF3_A57Usuario_PessoaCod, P00LF3_A3Perfil_Codigo, P00LF3_A58Usuario_PessoaNom, P00LF3_n58Usuario_PessoaNom, P00LF3_A543UsuarioPerfil_Display, P00LF3_n543UsuarioPerfil_Display, P00LF3_A546UsuarioPerfil_Delete, P00LF3_n546UsuarioPerfil_Delete, P00LF3_A659UsuarioPerfil_Update,
               P00LF3_n659UsuarioPerfil_Update, P00LF3_A544UsuarioPerfil_Insert, P00LF3_n544UsuarioPerfil_Insert, P00LF3_A275Perfil_Tipo, P00LF3_A2Usuario_Nome, P00LF3_n2Usuario_Nome, P00LF3_A4Perfil_Nome
               }
               , new Object[] {
               P00LF4_A1Usuario_Codigo, P00LF4_A57Usuario_PessoaCod, P00LF4_A3Perfil_Codigo, P00LF4_A543UsuarioPerfil_Display, P00LF4_n543UsuarioPerfil_Display, P00LF4_A546UsuarioPerfil_Delete, P00LF4_n546UsuarioPerfil_Delete, P00LF4_A659UsuarioPerfil_Update, P00LF4_n659UsuarioPerfil_Update, P00LF4_A544UsuarioPerfil_Insert,
               P00LF4_n544UsuarioPerfil_Insert, P00LF4_A275Perfil_Tipo, P00LF4_A2Usuario_Nome, P00LF4_n2Usuario_Nome, P00LF4_A58Usuario_PessoaNom, P00LF4_n58Usuario_PessoaNom, P00LF4_A4Perfil_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFUsuarioPerfil_Insert_Sel ;
      private short AV19TFUsuarioPerfil_Update_Sel ;
      private short AV20TFUsuarioPerfil_Delete_Sel ;
      private short AV21TFUsuarioPerfil_Display_Sel ;
      private short AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ;
      private short AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ;
      private short AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ;
      private short AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ;
      private short A275Perfil_Tipo ;
      private int AV49GXV1 ;
      private int AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A3Perfil_Codigo ;
      private int AV25InsertIndex ;
      private long AV34count ;
      private String AV10TFUsuario_Nome ;
      private String AV11TFUsuario_Nome_Sel ;
      private String AV12TFUsuario_PessoaNom ;
      private String AV13TFUsuario_PessoaNom_Sel ;
      private String AV14TFPerfil_Nome ;
      private String AV15TFPerfil_Nome_Sel ;
      private String AV41Perfil_Nome1 ;
      private String AV42Usuario_PessoaNom1 ;
      private String AV45Perfil_Nome2 ;
      private String AV46Usuario_PessoaNom2 ;
      private String AV52WWUsuarioPerfilDS_2_Perfil_nome1 ;
      private String AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ;
      private String AV56WWUsuarioPerfilDS_6_Perfil_nome2 ;
      private String AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ;
      private String AV58WWUsuarioPerfilDS_8_Tfusuario_nome ;
      private String AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel ;
      private String AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ;
      private String AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ;
      private String AV62WWUsuarioPerfilDS_12_Tfperfil_nome ;
      private String AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel ;
      private String scmdbuf ;
      private String lV52WWUsuarioPerfilDS_2_Perfil_nome1 ;
      private String lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ;
      private String lV56WWUsuarioPerfilDS_6_Perfil_nome2 ;
      private String lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ;
      private String lV58WWUsuarioPerfilDS_8_Tfusuario_nome ;
      private String lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ;
      private String lV62WWUsuarioPerfilDS_12_Tfperfil_nome ;
      private String A4Perfil_Nome ;
      private String A58Usuario_PessoaNom ;
      private String A2Usuario_Nome ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ;
      private bool A544UsuarioPerfil_Insert ;
      private bool A659UsuarioPerfil_Update ;
      private bool A546UsuarioPerfil_Delete ;
      private bool A543UsuarioPerfil_Display ;
      private bool BRKLF2 ;
      private bool n2Usuario_Nome ;
      private bool n543UsuarioPerfil_Display ;
      private bool n546UsuarioPerfil_Delete ;
      private bool n659UsuarioPerfil_Update ;
      private bool n544UsuarioPerfil_Insert ;
      private bool n58Usuario_PessoaNom ;
      private bool BRKLF4 ;
      private bool BRKLF6 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV16TFPerfil_Tipo_SelsJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ;
      private String AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LF2_A1Usuario_Codigo ;
      private int[] P00LF2_A57Usuario_PessoaCod ;
      private int[] P00LF2_A3Perfil_Codigo ;
      private String[] P00LF2_A2Usuario_Nome ;
      private bool[] P00LF2_n2Usuario_Nome ;
      private bool[] P00LF2_A543UsuarioPerfil_Display ;
      private bool[] P00LF2_n543UsuarioPerfil_Display ;
      private bool[] P00LF2_A546UsuarioPerfil_Delete ;
      private bool[] P00LF2_n546UsuarioPerfil_Delete ;
      private bool[] P00LF2_A659UsuarioPerfil_Update ;
      private bool[] P00LF2_n659UsuarioPerfil_Update ;
      private bool[] P00LF2_A544UsuarioPerfil_Insert ;
      private bool[] P00LF2_n544UsuarioPerfil_Insert ;
      private short[] P00LF2_A275Perfil_Tipo ;
      private String[] P00LF2_A58Usuario_PessoaNom ;
      private bool[] P00LF2_n58Usuario_PessoaNom ;
      private String[] P00LF2_A4Perfil_Nome ;
      private int[] P00LF3_A1Usuario_Codigo ;
      private int[] P00LF3_A57Usuario_PessoaCod ;
      private int[] P00LF3_A3Perfil_Codigo ;
      private String[] P00LF3_A58Usuario_PessoaNom ;
      private bool[] P00LF3_n58Usuario_PessoaNom ;
      private bool[] P00LF3_A543UsuarioPerfil_Display ;
      private bool[] P00LF3_n543UsuarioPerfil_Display ;
      private bool[] P00LF3_A546UsuarioPerfil_Delete ;
      private bool[] P00LF3_n546UsuarioPerfil_Delete ;
      private bool[] P00LF3_A659UsuarioPerfil_Update ;
      private bool[] P00LF3_n659UsuarioPerfil_Update ;
      private bool[] P00LF3_A544UsuarioPerfil_Insert ;
      private bool[] P00LF3_n544UsuarioPerfil_Insert ;
      private short[] P00LF3_A275Perfil_Tipo ;
      private String[] P00LF3_A2Usuario_Nome ;
      private bool[] P00LF3_n2Usuario_Nome ;
      private String[] P00LF3_A4Perfil_Nome ;
      private int[] P00LF4_A1Usuario_Codigo ;
      private int[] P00LF4_A57Usuario_PessoaCod ;
      private int[] P00LF4_A3Perfil_Codigo ;
      private bool[] P00LF4_A543UsuarioPerfil_Display ;
      private bool[] P00LF4_n543UsuarioPerfil_Display ;
      private bool[] P00LF4_A546UsuarioPerfil_Delete ;
      private bool[] P00LF4_n546UsuarioPerfil_Delete ;
      private bool[] P00LF4_A659UsuarioPerfil_Update ;
      private bool[] P00LF4_n659UsuarioPerfil_Update ;
      private bool[] P00LF4_A544UsuarioPerfil_Insert ;
      private bool[] P00LF4_n544UsuarioPerfil_Insert ;
      private short[] P00LF4_A275Perfil_Tipo ;
      private String[] P00LF4_A2Usuario_Nome ;
      private bool[] P00LF4_n2Usuario_Nome ;
      private String[] P00LF4_A58Usuario_PessoaNom ;
      private bool[] P00LF4_n58Usuario_PessoaNom ;
      private String[] P00LF4_A4Perfil_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17TFPerfil_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getwwusuarioperfilfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LF2( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                             String AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                             String AV52WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                             String AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                             bool AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                             String AV56WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                             String AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                             String AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                             String AV58WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                             String AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                             String AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                             String AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                             String AV62WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                             int AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count ,
                                             short AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                             short AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                             short AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                             short AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome ,
                                             bool A544UsuarioPerfil_Insert ,
                                             bool A659UsuarioPerfil_Update ,
                                             bool A546UsuarioPerfil_Delete ,
                                             bool A543UsuarioPerfil_Display )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T2.[Usuario_Nome], T1.[UsuarioPerfil_Display], T1.[UsuarioPerfil_Delete], T1.[UsuarioPerfil_Update], T1.[UsuarioPerfil_Insert], T4.[Perfil_Tipo], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Perfil_Nome] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         if ( ( StringUtil.StrCmp(AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWUsuarioPerfilDS_2_Perfil_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV52WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV52WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWUsuarioPerfilDS_6_Perfil_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV56WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV56WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWUsuarioPerfilDS_8_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV58WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV58WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWUsuarioPerfilDS_12_Tfperfil_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV62WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV62WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 1)";
            }
         }
         if ( AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 0)";
            }
         }
         if ( AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 1)";
            }
         }
         if ( AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 0)";
            }
         }
         if ( AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 1)";
            }
         }
         if ( AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 0)";
            }
         }
         if ( AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 1)";
            }
         }
         if ( AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Usuario_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00LF3( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                             String AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                             String AV52WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                             String AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                             bool AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                             String AV56WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                             String AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                             String AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                             String AV58WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                             String AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                             String AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                             String AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                             String AV62WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                             int AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count ,
                                             short AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                             short AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                             short AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                             short AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome ,
                                             bool A544UsuarioPerfil_Insert ,
                                             bool A659UsuarioPerfil_Update ,
                                             bool A546UsuarioPerfil_Delete ,
                                             bool A543UsuarioPerfil_Display )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[UsuarioPerfil_Display], T1.[UsuarioPerfil_Delete], T1.[UsuarioPerfil_Update], T1.[UsuarioPerfil_Insert], T4.[Perfil_Tipo], T2.[Usuario_Nome], T4.[Perfil_Nome] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         if ( ( StringUtil.StrCmp(AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWUsuarioPerfilDS_2_Perfil_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV52WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV52WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWUsuarioPerfilDS_6_Perfil_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV56WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV56WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWUsuarioPerfilDS_8_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV58WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV58WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWUsuarioPerfilDS_12_Tfperfil_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV62WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV62WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 1)";
            }
         }
         if ( AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 0)";
            }
         }
         if ( AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 1)";
            }
         }
         if ( AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 0)";
            }
         }
         if ( AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 1)";
            }
         }
         if ( AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 0)";
            }
         }
         if ( AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 1)";
            }
         }
         if ( AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00LF4( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                             String AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                             String AV52WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                             String AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                             bool AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                             String AV56WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                             String AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                             String AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                             String AV58WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                             String AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                             String AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                             String AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                             String AV62WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                             int AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count ,
                                             short AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                             short AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                             short AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                             short AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome ,
                                             bool A544UsuarioPerfil_Insert ,
                                             bool A659UsuarioPerfil_Update ,
                                             bool A546UsuarioPerfil_Delete ,
                                             bool A543UsuarioPerfil_Display )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [10] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T1.[UsuarioPerfil_Display], T1.[UsuarioPerfil_Delete], T1.[UsuarioPerfil_Update], T1.[UsuarioPerfil_Insert], T4.[Perfil_Tipo], T2.[Usuario_Nome], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Perfil_Nome] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         if ( ( StringUtil.StrCmp(AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWUsuarioPerfilDS_2_Perfil_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV52WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV52WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUsuarioPerfilDS_3_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWUsuarioPerfilDS_6_Perfil_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV56WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV56WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV54WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWUsuarioPerfilDS_7_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWUsuarioPerfilDS_8_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV58WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV58WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWUsuarioPerfilDS_12_Tfperfil_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV62WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV62WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 1)";
            }
         }
         if ( AV65WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 0)";
            }
         }
         if ( AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 1)";
            }
         }
         if ( AV66WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 0)";
            }
         }
         if ( AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 1)";
            }
         }
         if ( AV67WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 0)";
            }
         }
         if ( AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 1)";
            }
         }
         if ( AV68WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Perfil_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LF2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] );
               case 1 :
                     return conditional_P00LF3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] );
               case 2 :
                     return conditional_P00LF4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LF2 ;
          prmP00LF2 = new Object[] {
          new Object[] {"@lV52WWUsuarioPerfilDS_2_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWUsuarioPerfilDS_6_Perfil_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV58WWUsuarioPerfilDS_8_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62WWUsuarioPerfilDS_12_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00LF3 ;
          prmP00LF3 = new Object[] {
          new Object[] {"@lV52WWUsuarioPerfilDS_2_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWUsuarioPerfilDS_6_Perfil_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV58WWUsuarioPerfilDS_8_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62WWUsuarioPerfilDS_12_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00LF4 ;
          prmP00LF4 = new Object[] {
          new Object[] {"@lV52WWUsuarioPerfilDS_2_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWUsuarioPerfilDS_3_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWUsuarioPerfilDS_6_Perfil_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWUsuarioPerfilDS_7_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV58WWUsuarioPerfilDS_8_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWUsuarioPerfilDS_9_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWUsuarioPerfilDS_10_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV61WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62WWUsuarioPerfilDS_12_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWUsuarioPerfilDS_13_Tfperfil_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LF2,100,0,true,false )
             ,new CursorDef("P00LF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LF3,100,0,true,false )
             ,new CursorDef("P00LF4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LF4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwusuarioperfilfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwusuarioperfilfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwusuarioperfilfilterdata") )
          {
             return  ;
          }
          getwwusuarioperfilfilterdata worker = new getwwusuarioperfilfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
