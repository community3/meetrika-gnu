/*
               File: type_SdtSDT_ID_Valor
        Description: SDT_ID_Valor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 21:37:11.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_ID_Valor" )]
   [XmlType(TypeName =  "SDT_ID_Valor" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_ID_Valor : GxUserType
   {
      public SdtSDT_ID_Valor( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_ID_Valor_Id = "";
         gxTv_SdtSDT_ID_Valor_Valor = "";
      }

      public SdtSDT_ID_Valor( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_ID_Valor deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_ID_Valor)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_ID_Valor obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Valor = deserialized.gxTpr_Valor;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ID") )
               {
                  gxTv_SdtSDT_ID_Valor_Id = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Valor") )
               {
                  gxTv_SdtSDT_ID_Valor_Valor = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_ID_Valor";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ID", StringUtil.RTrim( gxTv_SdtSDT_ID_Valor_Id));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Valor", StringUtil.RTrim( gxTv_SdtSDT_ID_Valor_Valor));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ID", gxTv_SdtSDT_ID_Valor_Id, false);
         AddObjectProperty("Valor", gxTv_SdtSDT_ID_Valor_Valor, false);
         return  ;
      }

      [  SoapElement( ElementName = "ID" )]
      [  XmlElement( ElementName = "ID"   )]
      public String gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_ID_Valor_Id ;
         }

         set {
            gxTv_SdtSDT_ID_Valor_Id = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Valor" )]
      [  XmlElement( ElementName = "Valor"   )]
      public String gxTpr_Valor
      {
         get {
            return gxTv_SdtSDT_ID_Valor_Valor ;
         }

         set {
            gxTv_SdtSDT_ID_Valor_Valor = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_ID_Valor_Id = "";
         gxTv_SdtSDT_ID_Valor_Valor = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_ID_Valor_Id ;
      protected String gxTv_SdtSDT_ID_Valor_Valor ;
   }

   [DataContract(Name = @"SDT_ID_Valor", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_ID_Valor_RESTInterface : GxGenericCollectionItem<SdtSDT_ID_Valor>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_ID_Valor_RESTInterface( ) : base()
      {
      }

      public SdtSDT_ID_Valor_RESTInterface( SdtSDT_ID_Valor psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ID" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (String)(value);
         }

      }

      [DataMember( Name = "Valor" , Order = 1 )]
      public String gxTpr_Valor
      {
         get {
            return sdt.gxTpr_Valor ;
         }

         set {
            sdt.gxTpr_Valor = (String)(value);
         }

      }

      public SdtSDT_ID_Valor sdt
      {
         get {
            return (SdtSDT_ID_Valor)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_ID_Valor() ;
         }
      }

   }

}
