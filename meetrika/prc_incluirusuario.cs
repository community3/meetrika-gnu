/*
               File: PRC_IncluirUsuario
        Description: PRC_Incluir Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:16:15.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_incluirusuario : GXProcedure
   {
      public prc_incluirusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_incluirusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           String aP1_Pessoa_Nome ,
                           String aP2_Pessoa_Docto ,
                           out String aP3_vrMsg )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Pessoa_Nome = aP1_Pessoa_Nome;
         this.AV10Pessoa_Docto = aP2_Pessoa_Docto;
         this.AV16vrMsg = "" ;
         initialize();
         executePrivate();
         aP3_vrMsg=this.AV16vrMsg;
      }

      public String executeUdp( int aP0_AreaTrabalho_Codigo ,
                                String aP1_Pessoa_Nome ,
                                String aP2_Pessoa_Docto )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Pessoa_Nome = aP1_Pessoa_Nome;
         this.AV10Pessoa_Docto = aP2_Pessoa_Docto;
         this.AV16vrMsg = "" ;
         initialize();
         executePrivate();
         aP3_vrMsg=this.AV16vrMsg;
         return AV16vrMsg ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 String aP1_Pessoa_Nome ,
                                 String aP2_Pessoa_Docto ,
                                 out String aP3_vrMsg )
      {
         prc_incluirusuario objprc_incluirusuario;
         objprc_incluirusuario = new prc_incluirusuario();
         objprc_incluirusuario.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_incluirusuario.AV9Pessoa_Nome = aP1_Pessoa_Nome;
         objprc_incluirusuario.AV10Pessoa_Docto = aP2_Pessoa_Docto;
         objprc_incluirusuario.AV16vrMsg = "" ;
         objprc_incluirusuario.context.SetSubmitInitialConfig(context);
         objprc_incluirusuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_incluirusuario);
         aP3_vrMsg=this.AV16vrMsg;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_incluirusuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19GXLvl6 = 0;
         /* Using cursor P00142 */
         pr_default.execute(0, new Object[] {AV10Pessoa_Docto});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A38Pessoa_Ativo = P00142_A38Pessoa_Ativo[0];
            A37Pessoa_Docto = P00142_A37Pessoa_Docto[0];
            A34Pessoa_Codigo = P00142_A34Pessoa_Codigo[0];
            AV19GXLvl6 = 1;
            AV11Pessoa.Load(A34Pessoa_Codigo);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( AV19GXLvl6 == 0 )
         {
            AV11Pessoa = new SdtPessoa(context);
         }
         AV11Pessoa.gxTpr_Pessoa_nome = AV9Pessoa_Nome;
         AV11Pessoa.gxTpr_Pessoa_docto = AV10Pessoa_Docto;
         AV11Pessoa.gxTpr_Pessoa_tipopessoa = "F";
         AV11Pessoa.gxTpr_Pessoa_ativo = true;
         AV11Pessoa.Save();
         if ( ! AV11Pessoa.Fail() )
         {
            context.CommitDataStores( "PRC_IncluirUsuario");
            AV13Pessoa_Codigo = AV11Pessoa.gxTpr_Pessoa_codigo;
            AV12Usuario = new SdtUsuario(context);
            AV12Usuario.gxTpr_Usuario_pessoacod = AV13Pessoa_Codigo;
            AV12Usuario.gxTpr_Usuario_ativo = true;
            AV12Usuario.Save();
            if ( ! AV12Usuario.Fail() )
            {
               context.CommitDataStores( "PRC_IncluirUsuario");
            }
            else
            {
               /* Execute user subroutine: 'ERRO-USUARIO' */
               S121 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'ERRO-PESSOA' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ERRO-PESSOA' Routine */
         AV14Messages = AV11Pessoa.GetMessages();
         AV20GXV1 = 1;
         while ( AV20GXV1 <= AV14Messages.Count )
         {
            AV15Message = ((SdtMessages_Message)AV14Messages.Item(AV20GXV1));
            AV16vrMsg = "Erro ao incluir Pessoa! " + AV15Message.gxTpr_Description;
            GX_msglist.addItem(AV16vrMsg);
            AV20GXV1 = (int)(AV20GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'ERRO-USUARIO' Routine */
         AV14Messages = AV12Usuario.GetMessages();
         AV21GXV2 = 1;
         while ( AV21GXV2 <= AV14Messages.Count )
         {
            AV15Message = ((SdtMessages_Message)AV14Messages.Item(AV21GXV2));
            AV16vrMsg = "Erro ao incluir Usu�rio! " + AV15Message.gxTpr_Description;
            GX_msglist.addItem(AV16vrMsg);
            AV21GXV2 = (int)(AV21GXV2+1);
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00142_A38Pessoa_Ativo = new bool[] {false} ;
         P00142_A37Pessoa_Docto = new String[] {""} ;
         P00142_A34Pessoa_Codigo = new int[1] ;
         A37Pessoa_Docto = "";
         AV11Pessoa = new SdtPessoa(context);
         AV12Usuario = new SdtUsuario(context);
         AV14Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_incluirusuario__default(),
            new Object[][] {
                new Object[] {
               P00142_A38Pessoa_Ativo, P00142_A37Pessoa_Docto, P00142_A34Pessoa_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV19GXLvl6 ;
      private int AV8AreaTrabalho_Codigo ;
      private int A34Pessoa_Codigo ;
      private int AV13Pessoa_Codigo ;
      private int AV20GXV1 ;
      private int AV21GXV2 ;
      private String AV9Pessoa_Nome ;
      private String AV16vrMsg ;
      private String scmdbuf ;
      private bool A38Pessoa_Ativo ;
      private bool returnInSub ;
      private String AV10Pessoa_Docto ;
      private String A37Pessoa_Docto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00142_A38Pessoa_Ativo ;
      private String[] P00142_A37Pessoa_Docto ;
      private int[] P00142_A34Pessoa_Codigo ;
      private String aP3_vrMsg ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV14Messages ;
      private SdtPessoa AV11Pessoa ;
      private SdtMessages_Message AV15Message ;
      private SdtUsuario AV12Usuario ;
   }

   public class prc_incluirusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00142 ;
          prmP00142 = new Object[] {
          new Object[] {"@AV10Pessoa_Docto",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00142", "SELECT [Pessoa_Ativo], [Pessoa_Docto], [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE ([Pessoa_Docto] = @AV10Pessoa_Docto) AND ([Pessoa_Ativo] = 1) ORDER BY [Pessoa_Docto] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00142,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
