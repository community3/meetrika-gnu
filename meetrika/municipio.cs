/*
               File: Municipio
        Description: Municipio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:14:46.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class municipio : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ESTADO_UF") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAESTADO_UF0A10( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A23Estado_UF = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A23Estado_UF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A21Pais_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A21Pais_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Municipio_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMUNICIPIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Municipio_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynEstado_UF.Name = "ESTADO_UF";
         dynEstado_UF.WebTags = "";
         dynPais_Codigo.Name = "PAIS_CODIGO";
         dynPais_Codigo.WebTags = "";
         dynPais_Codigo.removeAllItems();
         /* Using cursor T000A6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            dynPais_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T000A6_A21Pais_Codigo[0]), 6, 0)), T000A6_A22Pais_Nome[0], 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( dynPais_Codigo.ItemCount > 0 )
         {
            A21Pais_Codigo = (int)(NumberUtil.Val( dynPais_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Municipio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtMunicipio_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public municipio( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public municipio( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Municipio_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Municipio_Codigo = aP1_Municipio_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynEstado_UF = new GXCombobox();
         dynPais_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynEstado_UF.ItemCount > 0 )
         {
            A23Estado_UF = dynEstado_UF.getValidValue(A23Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         }
         if ( dynPais_Codigo.ItemCount > 0 )
         {
            A21Pais_Codigo = (int)(NumberUtil.Val( dynPais_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0A10( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0A10e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")), ((edtMunicipio_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtMunicipio_Codigo_Visible, edtMunicipio_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Municipio.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0A10( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0A10( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0A10e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_0A10( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_0A10e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0A10e( true) ;
         }
         else
         {
            wb_table1_2_0A10e( false) ;
         }
      }

      protected void wb_table3_31_0A10( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Municipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Municipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Municipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_0A10e( true) ;
         }
         else
         {
            wb_table3_31_0A10e( false) ;
         }
      }

      protected void wb_table2_5_0A10( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_12_0A10( true) ;
         }
         return  ;
      }

      protected void wb_table4_12_0A10e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0A10e( true) ;
         }
         else
         {
            wb_table2_5_0A10e( false) ;
         }
      }

      protected void wb_table4_12_0A10( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_nome_Internalname, "Munic�pio", "", "", lblTextblockmunicipio_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Municipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Nome_Internalname, StringUtil.RTrim( A26Municipio_Nome), StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMunicipio_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Municipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockestado_uf_Internalname, "UF", "", "", lblTextblockestado_uf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Municipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynEstado_UF, dynEstado_UF_Internalname, StringUtil.RTrim( A23Estado_UF), 1, dynEstado_UF_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, dynEstado_UF.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_Municipio.htm");
            dynEstado_UF.CurrentValue = StringUtil.RTrim( A23Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Values", (String)(dynEstado_UF.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpais_codigo_Internalname, "Pa�s", "", "", lblTextblockpais_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Municipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynPais_Codigo, dynPais_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)), 1, dynPais_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynPais_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_Municipio.htm");
            dynPais_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPais_Codigo_Internalname, "Values", (String)(dynPais_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_12_0A10e( true) ;
         }
         else
         {
            wb_table4_12_0A10e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110A2 */
         E110A2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
               dynEstado_UF.CurrentValue = cgiGet( dynEstado_UF_Internalname);
               A23Estado_UF = cgiGet( dynEstado_UF_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
               dynPais_Codigo.CurrentValue = cgiGet( dynPais_Codigo_Internalname);
               A21Pais_Codigo = (int)(NumberUtil.Val( cgiGet( dynPais_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
               A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
               n25Municipio_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               /* Read saved values. */
               Z25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z25Municipio_Codigo"), ",", "."));
               Z26Municipio_Nome = cgiGet( "Z26Municipio_Nome");
               Z23Estado_UF = cgiGet( "Z23Estado_UF");
               Z21Pais_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z21Pais_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N23Estado_UF = cgiGet( "N23Estado_UF");
               N21Pais_Codigo = (int)(context.localUtil.CToN( cgiGet( "N21Pais_Codigo"), ",", "."));
               AV7Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "vMUNICIPIO_CODIGO"), ",", "."));
               AV11Insert_Estado_UF = cgiGet( "vINSERT_ESTADO_UF");
               AV12Insert_Pais_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PAIS_CODIGO"), ",", "."));
               A24Estado_Nome = cgiGet( "ESTADO_NOME");
               A22Pais_Nome = cgiGet( "PAIS_NOME");
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Municipio";
               A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
               n25Municipio_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A25Municipio_Codigo != Z25Municipio_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("municipio:[SecurityCheckFailed value for]"+"Municipio_Codigo:"+context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("municipio:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n25Municipio_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode10 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode10;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound10 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0A0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "MUNICIPIO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtMunicipio_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110A2 */
                           E110A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120A2 */
                           E120A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120A2 */
            E120A2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0A10( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0A10( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0A0( )
      {
         BeforeValidate0A10( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0A10( ) ;
            }
            else
            {
               CheckExtendedTable0A10( ) ;
               CloseExtendedTableCursors0A10( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0A0( )
      {
      }

      protected void E110A2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Estado_UF") == 0 )
               {
                  AV11Insert_Estado_UF = AV13TrnContextAtt.gxTpr_Attributevalue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Estado_UF", AV11Insert_Estado_UF);
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Pais_Codigo") == 0 )
               {
                  AV12Insert_Pais_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Pais_Codigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtMunicipio_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Visible), 5, 0)));
      }

      protected void E120A2( )
      {
         /* After Trn Routine */
         context.wjLoc = formatLink("viewestado.aspx") + "?" + UrlEncode(StringUtil.RTrim(A23Estado_UF)) + "," + UrlEncode(StringUtil.RTrim("Municipios"));
         context.wjLocDisableFrm = 1;
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwmunicipio.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0A10( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z26Municipio_Nome = T000A3_A26Municipio_Nome[0];
               Z23Estado_UF = T000A3_A23Estado_UF[0];
               Z21Pais_Codigo = T000A3_A21Pais_Codigo[0];
            }
            else
            {
               Z26Municipio_Nome = A26Municipio_Nome;
               Z23Estado_UF = A23Estado_UF;
               Z21Pais_Codigo = A21Pais_Codigo;
            }
         }
         if ( GX_JID == -14 )
         {
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z26Municipio_Nome = A26Municipio_Nome;
            Z23Estado_UF = A23Estado_UF;
            Z21Pais_Codigo = A21Pais_Codigo;
            Z24Estado_Nome = A24Estado_Nome;
            Z22Pais_Nome = A22Pais_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAESTADO_UF_html0A10( ) ;
         edtMunicipio_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "Municipio";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtMunicipio_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Municipio_Codigo) )
         {
            A25Municipio_Codigo = AV7Municipio_Codigo;
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Insert_Estado_UF)) )
         {
            dynEstado_UF.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynEstado_UF.Enabled), 5, 0)));
         }
         else
         {
            dynEstado_UF.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynEstado_UF.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Pais_Codigo) )
         {
            dynPais_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPais_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynPais_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynPais_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPais_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynPais_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Pais_Codigo) )
         {
            A21Pais_Codigo = AV12Insert_Pais_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Insert_Estado_UF)) )
         {
            A23Estado_UF = AV11Insert_Estado_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000A4 */
            pr_default.execute(2, new Object[] {A23Estado_UF});
            A24Estado_Nome = T000A4_A24Estado_Nome[0];
            pr_default.close(2);
            /* Using cursor T000A5 */
            pr_default.execute(3, new Object[] {A21Pais_Codigo});
            A22Pais_Nome = T000A5_A22Pais_Nome[0];
            pr_default.close(3);
         }
      }

      protected void Load0A10( )
      {
         /* Using cursor T000A7 */
         pr_default.execute(5, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound10 = 1;
            A26Municipio_Nome = T000A7_A26Municipio_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
            A24Estado_Nome = T000A7_A24Estado_Nome[0];
            A22Pais_Nome = T000A7_A22Pais_Nome[0];
            A23Estado_UF = T000A7_A23Estado_UF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
            A21Pais_Codigo = T000A7_A21Pais_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
            ZM0A10( -14) ;
         }
         pr_default.close(5);
         OnLoadActions0A10( ) ;
      }

      protected void OnLoadActions0A10( )
      {
      }

      protected void CheckExtendedTable0A10( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A26Municipio_Nome)) )
         {
            GX_msglist.addItem("Nome do Munic�pio � obrigat�rio.", 1, "MUNICIPIO_NOME");
            AnyError = 1;
            GX_FocusControl = edtMunicipio_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000A4 */
         pr_default.execute(2, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A24Estado_Nome = T000A4_A24Estado_Nome[0];
         pr_default.close(2);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
         {
            GX_msglist.addItem("UF � obrigat�rio.", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000A5 */
         pr_default.execute(3, new Object[] {A21Pais_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pais'.", "ForeignKeyNotFound", 1, "PAIS_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynPais_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A22Pais_Nome = T000A5_A22Pais_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors0A10( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_15( String A23Estado_UF )
      {
         /* Using cursor T000A8 */
         pr_default.execute(6, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A24Estado_Nome = T000A8_A24Estado_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A24Estado_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_16( int A21Pais_Codigo )
      {
         /* Using cursor T000A9 */
         pr_default.execute(7, new Object[] {A21Pais_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pais'.", "ForeignKeyNotFound", 1, "PAIS_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynPais_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A22Pais_Nome = T000A9_A22Pais_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A22Pais_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey0A10( )
      {
         /* Using cursor T000A10 */
         pr_default.execute(8, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound10 = 1;
         }
         else
         {
            RcdFound10 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000A3 */
         pr_default.execute(1, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0A10( 14) ;
            RcdFound10 = 1;
            A25Municipio_Codigo = T000A3_A25Municipio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            n25Municipio_Codigo = T000A3_n25Municipio_Codigo[0];
            A26Municipio_Nome = T000A3_A26Municipio_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
            A23Estado_UF = T000A3_A23Estado_UF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
            A21Pais_Codigo = T000A3_A21Pais_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
            Z25Municipio_Codigo = A25Municipio_Codigo;
            sMode10 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0A10( ) ;
            if ( AnyError == 1 )
            {
               RcdFound10 = 0;
               InitializeNonKey0A10( ) ;
            }
            Gx_mode = sMode10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound10 = 0;
            InitializeNonKey0A10( ) ;
            sMode10 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0A10( ) ;
         if ( RcdFound10 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound10 = 0;
         /* Using cursor T000A11 */
         pr_default.execute(9, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T000A11_A25Municipio_Codigo[0] < A25Municipio_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T000A11_A25Municipio_Codigo[0] > A25Municipio_Codigo ) ) )
            {
               A25Municipio_Codigo = T000A11_A25Municipio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               n25Municipio_Codigo = T000A11_n25Municipio_Codigo[0];
               RcdFound10 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound10 = 0;
         /* Using cursor T000A12 */
         pr_default.execute(10, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T000A12_A25Municipio_Codigo[0] > A25Municipio_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T000A12_A25Municipio_Codigo[0] < A25Municipio_Codigo ) ) )
            {
               A25Municipio_Codigo = T000A12_A25Municipio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               n25Municipio_Codigo = T000A12_n25Municipio_Codigo[0];
               RcdFound10 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0A10( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtMunicipio_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0A10( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound10 == 1 )
            {
               if ( A25Municipio_Codigo != Z25Municipio_Codigo )
               {
                  A25Municipio_Codigo = Z25Municipio_Codigo;
                  n25Municipio_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "MUNICIPIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtMunicipio_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtMunicipio_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0A10( ) ;
                  GX_FocusControl = edtMunicipio_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A25Municipio_Codigo != Z25Municipio_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtMunicipio_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0A10( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "MUNICIPIO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtMunicipio_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtMunicipio_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0A10( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A25Municipio_Codigo != Z25Municipio_Codigo )
         {
            A25Municipio_Codigo = Z25Municipio_Codigo;
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "MUNICIPIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMunicipio_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtMunicipio_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0A10( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000A2 */
            pr_default.execute(0, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Municipio"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z26Municipio_Nome, T000A2_A26Municipio_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z23Estado_UF, T000A2_A23Estado_UF[0]) != 0 ) || ( Z21Pais_Codigo != T000A2_A21Pais_Codigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z26Municipio_Nome, T000A2_A26Municipio_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("municipio:[seudo value changed for attri]"+"Municipio_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z26Municipio_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000A2_A26Municipio_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z23Estado_UF, T000A2_A23Estado_UF[0]) != 0 )
               {
                  GXUtil.WriteLog("municipio:[seudo value changed for attri]"+"Estado_UF");
                  GXUtil.WriteLogRaw("Old: ",Z23Estado_UF);
                  GXUtil.WriteLogRaw("Current: ",T000A2_A23Estado_UF[0]);
               }
               if ( Z21Pais_Codigo != T000A2_A21Pais_Codigo[0] )
               {
                  GXUtil.WriteLog("municipio:[seudo value changed for attri]"+"Pais_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z21Pais_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000A2_A21Pais_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Municipio"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0A10( )
      {
         BeforeValidate0A10( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0A10( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0A10( 0) ;
            CheckOptimisticConcurrency0A10( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0A10( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0A10( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000A13 */
                     pr_default.execute(11, new Object[] {A26Municipio_Nome, A23Estado_UF, A21Pais_Codigo});
                     A25Municipio_Codigo = T000A13_A25Municipio_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
                     n25Municipio_Codigo = T000A13_n25Municipio_Codigo[0];
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Municipio") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0A0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0A10( ) ;
            }
            EndLevel0A10( ) ;
         }
         CloseExtendedTableCursors0A10( ) ;
      }

      protected void Update0A10( )
      {
         BeforeValidate0A10( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0A10( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0A10( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0A10( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0A10( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000A14 */
                     pr_default.execute(12, new Object[] {A26Municipio_Nome, A23Estado_UF, A21Pais_Codigo, n25Municipio_Codigo, A25Municipio_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Municipio") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Municipio"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0A10( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0A10( ) ;
         }
         CloseExtendedTableCursors0A10( ) ;
      }

      protected void DeferredUpdate0A10( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0A10( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0A10( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0A10( ) ;
            AfterConfirm0A10( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0A10( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000A15 */
                  pr_default.execute(13, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("Municipio") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode10 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0A10( ) ;
         Gx_mode = sMode10;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0A10( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000A16 */
            pr_default.execute(14, new Object[] {A23Estado_UF});
            A24Estado_Nome = T000A16_A24Estado_Nome[0];
            pr_default.close(14);
            /* Using cursor T000A17 */
            pr_default.execute(15, new Object[] {A21Pais_Codigo});
            A22Pais_Nome = T000A17_A22Pais_Nome[0];
            pr_default.close(15);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000A18 */
            pr_default.execute(16, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Pessoa"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T000A19 */
            pr_default.execute(17, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor T000A20 */
            pr_default.execute(18, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratante"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor T000A21 */
            pr_default.execute(19, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Banco Agencia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
         }
      }

      protected void EndLevel0A10( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0A10( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.CommitDataStores( "Municipio");
            if ( AnyError == 0 )
            {
               ConfirmValues0A0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.RollbackDataStores( "Municipio");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0A10( )
      {
         /* Scan By routine */
         /* Using cursor T000A22 */
         pr_default.execute(20);
         RcdFound10 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound10 = 1;
            A25Municipio_Codigo = T000A22_A25Municipio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            n25Municipio_Codigo = T000A22_n25Municipio_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0A10( )
      {
         /* Scan next routine */
         pr_default.readNext(20);
         RcdFound10 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound10 = 1;
            A25Municipio_Codigo = T000A22_A25Municipio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            n25Municipio_Codigo = T000A22_n25Municipio_Codigo[0];
         }
      }

      protected void ScanEnd0A10( )
      {
         pr_default.close(20);
      }

      protected void AfterConfirm0A10( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0A10( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0A10( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0A10( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0A10( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0A10( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0A10( )
      {
         edtMunicipio_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Nome_Enabled), 5, 0)));
         dynEstado_UF.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynEstado_UF.Enabled), 5, 0)));
         dynPais_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPais_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynPais_Codigo.Enabled), 5, 0)));
         edtMunicipio_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0A0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117144742");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("municipio.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Municipio_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z25Municipio_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z26Municipio_Nome", StringUtil.RTrim( Z26Municipio_Nome));
         GxWebStd.gx_hidden_field( context, "Z23Estado_UF", StringUtil.RTrim( Z23Estado_UF));
         GxWebStd.gx_hidden_field( context, "Z21Pais_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z21Pais_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N23Estado_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "N21Pais_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A21Pais_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vMUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_ESTADO_UF", StringUtil.RTrim( AV11Insert_Estado_UF));
         GxWebStd.gx_hidden_field( context, "vINSERT_PAIS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Pais_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ESTADO_NOME", StringUtil.RTrim( A24Estado_Nome));
         GxWebStd.gx_hidden_field( context, "PAIS_NOME", StringUtil.RTrim( A22Pais_Nome));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vMUNICIPIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Municipio_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Municipio";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("municipio:[SendSecurityCheck value for]"+"Municipio_Codigo:"+context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("municipio:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("municipio.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Municipio_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Municipio" ;
      }

      public override String GetPgmdesc( )
      {
         return "Municipio" ;
      }

      protected void InitializeNonKey0A10( )
      {
         A23Estado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         A21Pais_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21Pais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A21Pais_Codigo), 6, 0)));
         A26Municipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
         A24Estado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24Estado_Nome", A24Estado_Nome);
         A22Pais_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22Pais_Nome", A22Pais_Nome);
         Z26Municipio_Nome = "";
         Z23Estado_UF = "";
         Z21Pais_Codigo = 0;
      }

      protected void InitAll0A10( )
      {
         A25Municipio_Codigo = 0;
         n25Municipio_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         InitializeNonKey0A10( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117144753");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("municipio.js", "?20203117144754");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockmunicipio_nome_Internalname = "TEXTBLOCKMUNICIPIO_NOME";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         lblTextblockestado_uf_Internalname = "TEXTBLOCKESTADO_UF";
         dynEstado_UF_Internalname = "ESTADO_UF";
         lblTextblockpais_codigo_Internalname = "TEXTBLOCKPAIS_CODIGO";
         dynPais_Codigo_Internalname = "PAIS_CODIGO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Municipio";
         dynPais_Codigo_Jsonclick = "";
         dynPais_Codigo.Enabled = 1;
         dynEstado_UF_Jsonclick = "";
         dynEstado_UF.Enabled = 1;
         edtMunicipio_Nome_Jsonclick = "";
         edtMunicipio_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtMunicipio_Codigo_Jsonclick = "";
         edtMunicipio_Codigo_Enabled = 0;
         edtMunicipio_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPAIS_CODIGO0A1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPAIS_CODIGO_data0A1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPAIS_CODIGO_html0A1( )
      {
         int gxdynajaxvalue ;
         GXDLAPAIS_CODIGO_data0A1( ) ;
         gxdynajaxindex = 1;
         dynPais_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynPais_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPAIS_CODIGO_data0A1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T000A23 */
         pr_default.execute(21);
         while ( (pr_default.getStatus(21) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000A23_A21Pais_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000A23_A22Pais_Nome[0]));
            pr_default.readNext(21);
         }
         pr_default.close(21);
      }

      protected void GXDLAESTADO_UF0A10( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAESTADO_UF_data0A10( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAESTADO_UF_html0A10( )
      {
         String gxdynajaxvalue ;
         GXDLAESTADO_UF_data0A10( ) ;
         gxdynajaxindex = 1;
         dynEstado_UF.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynEstado_UF.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAESTADO_UF_data0A10( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000A24 */
         pr_default.execute(22);
         while ( (pr_default.getStatus(22) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( T000A24_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000A24_A24Estado_Nome[0]));
            pr_default.readNext(22);
         }
         pr_default.close(22);
      }

      public void Valid_Estado_uf( GXCombobox dynGX_Parm1 ,
                                   String GX_Parm2 )
      {
         dynEstado_UF = dynGX_Parm1;
         A23Estado_UF = dynEstado_UF.CurrentValue;
         A24Estado_Nome = GX_Parm2;
         /* Using cursor T000A25 */
         pr_default.execute(23, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
         }
         A24Estado_Nome = T000A25_A24Estado_Nome[0];
         pr_default.close(23);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
         {
            GX_msglist.addItem("UF � obrigat�rio.", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A24Estado_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A24Estado_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Pais_codigo( GXCombobox dynGX_Parm1 ,
                                     String GX_Parm2 )
      {
         dynPais_Codigo = dynGX_Parm1;
         A21Pais_Codigo = (int)(NumberUtil.Val( dynPais_Codigo.CurrentValue, "."));
         A22Pais_Nome = GX_Parm2;
         /* Using cursor T000A26 */
         pr_default.execute(24, new Object[] {A21Pais_Codigo});
         if ( (pr_default.getStatus(24) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pais'.", "ForeignKeyNotFound", 1, "PAIS_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynPais_Codigo_Internalname;
         }
         A22Pais_Nome = T000A26_A22Pais_Nome[0];
         pr_default.close(24);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A22Pais_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A22Pais_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120A2',iparms:[{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(23);
         pr_default.close(14);
         pr_default.close(24);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z26Municipio_Nome = "";
         Z23Estado_UF = "";
         N23Estado_UF = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A23Estado_UF = "";
         GXKey = "";
         T000A6_A21Pais_Codigo = new int[1] ;
         T000A6_A22Pais_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockmunicipio_nome_Jsonclick = "";
         A26Municipio_Nome = "";
         lblTextblockestado_uf_Jsonclick = "";
         lblTextblockpais_codigo_Jsonclick = "";
         AV11Insert_Estado_UF = "";
         A24Estado_Nome = "";
         A22Pais_Nome = "";
         AV14Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode10 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z24Estado_Nome = "";
         Z22Pais_Nome = "";
         T000A4_A24Estado_Nome = new String[] {""} ;
         T000A5_A22Pais_Nome = new String[] {""} ;
         T000A7_A25Municipio_Codigo = new int[1] ;
         T000A7_n25Municipio_Codigo = new bool[] {false} ;
         T000A7_A26Municipio_Nome = new String[] {""} ;
         T000A7_A24Estado_Nome = new String[] {""} ;
         T000A7_A22Pais_Nome = new String[] {""} ;
         T000A7_A23Estado_UF = new String[] {""} ;
         T000A7_A21Pais_Codigo = new int[1] ;
         T000A8_A24Estado_Nome = new String[] {""} ;
         T000A9_A22Pais_Nome = new String[] {""} ;
         T000A10_A25Municipio_Codigo = new int[1] ;
         T000A10_n25Municipio_Codigo = new bool[] {false} ;
         T000A3_A25Municipio_Codigo = new int[1] ;
         T000A3_n25Municipio_Codigo = new bool[] {false} ;
         T000A3_A26Municipio_Nome = new String[] {""} ;
         T000A3_A23Estado_UF = new String[] {""} ;
         T000A3_A21Pais_Codigo = new int[1] ;
         T000A11_A25Municipio_Codigo = new int[1] ;
         T000A11_n25Municipio_Codigo = new bool[] {false} ;
         T000A12_A25Municipio_Codigo = new int[1] ;
         T000A12_n25Municipio_Codigo = new bool[] {false} ;
         T000A2_A25Municipio_Codigo = new int[1] ;
         T000A2_n25Municipio_Codigo = new bool[] {false} ;
         T000A2_A26Municipio_Nome = new String[] {""} ;
         T000A2_A23Estado_UF = new String[] {""} ;
         T000A2_A21Pais_Codigo = new int[1] ;
         T000A13_A25Municipio_Codigo = new int[1] ;
         T000A13_n25Municipio_Codigo = new bool[] {false} ;
         T000A16_A24Estado_Nome = new String[] {""} ;
         T000A17_A22Pais_Nome = new String[] {""} ;
         T000A18_A34Pessoa_Codigo = new int[1] ;
         T000A19_A39Contratada_Codigo = new int[1] ;
         T000A20_A29Contratante_Codigo = new int[1] ;
         T000A21_A17BancoAgencia_Codigo = new int[1] ;
         T000A22_A25Municipio_Codigo = new int[1] ;
         T000A22_n25Municipio_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000A23_A21Pais_Codigo = new int[1] ;
         T000A23_A22Pais_Nome = new String[] {""} ;
         T000A24_A23Estado_UF = new String[] {""} ;
         T000A24_A24Estado_Nome = new String[] {""} ;
         T000A25_A24Estado_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T000A26_A22Pais_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.municipio__default(),
            new Object[][] {
                new Object[] {
               T000A2_A25Municipio_Codigo, T000A2_A26Municipio_Nome, T000A2_A23Estado_UF, T000A2_A21Pais_Codigo
               }
               , new Object[] {
               T000A3_A25Municipio_Codigo, T000A3_A26Municipio_Nome, T000A3_A23Estado_UF, T000A3_A21Pais_Codigo
               }
               , new Object[] {
               T000A4_A24Estado_Nome
               }
               , new Object[] {
               T000A5_A22Pais_Nome
               }
               , new Object[] {
               T000A6_A21Pais_Codigo, T000A6_A22Pais_Nome
               }
               , new Object[] {
               T000A7_A25Municipio_Codigo, T000A7_A26Municipio_Nome, T000A7_A24Estado_Nome, T000A7_A22Pais_Nome, T000A7_A23Estado_UF, T000A7_A21Pais_Codigo
               }
               , new Object[] {
               T000A8_A24Estado_Nome
               }
               , new Object[] {
               T000A9_A22Pais_Nome
               }
               , new Object[] {
               T000A10_A25Municipio_Codigo
               }
               , new Object[] {
               T000A11_A25Municipio_Codigo
               }
               , new Object[] {
               T000A12_A25Municipio_Codigo
               }
               , new Object[] {
               T000A13_A25Municipio_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000A16_A24Estado_Nome
               }
               , new Object[] {
               T000A17_A22Pais_Nome
               }
               , new Object[] {
               T000A18_A34Pessoa_Codigo
               }
               , new Object[] {
               T000A19_A39Contratada_Codigo
               }
               , new Object[] {
               T000A20_A29Contratante_Codigo
               }
               , new Object[] {
               T000A21_A17BancoAgencia_Codigo
               }
               , new Object[] {
               T000A22_A25Municipio_Codigo
               }
               , new Object[] {
               T000A23_A21Pais_Codigo, T000A23_A22Pais_Nome
               }
               , new Object[] {
               T000A24_A23Estado_UF, T000A24_A24Estado_Nome
               }
               , new Object[] {
               T000A25_A24Estado_Nome
               }
               , new Object[] {
               T000A26_A22Pais_Nome
               }
            }
         );
         AV14Pgmname = "Municipio";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound10 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Municipio_Codigo ;
      private int Z25Municipio_Codigo ;
      private int Z21Pais_Codigo ;
      private int N21Pais_Codigo ;
      private int A21Pais_Codigo ;
      private int AV7Municipio_Codigo ;
      private int trnEnded ;
      private int A25Municipio_Codigo ;
      private int edtMunicipio_Codigo_Enabled ;
      private int edtMunicipio_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtMunicipio_Nome_Enabled ;
      private int AV12Insert_Pais_Codigo ;
      private int AV15GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z26Municipio_Nome ;
      private String Z23Estado_UF ;
      private String N23Estado_UF ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A23Estado_UF ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtMunicipio_Nome_Internalname ;
      private String edtMunicipio_Codigo_Internalname ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockmunicipio_nome_Internalname ;
      private String lblTextblockmunicipio_nome_Jsonclick ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String lblTextblockestado_uf_Internalname ;
      private String lblTextblockestado_uf_Jsonclick ;
      private String dynEstado_UF_Internalname ;
      private String dynEstado_UF_Jsonclick ;
      private String lblTextblockpais_codigo_Internalname ;
      private String lblTextblockpais_codigo_Jsonclick ;
      private String dynPais_Codigo_Internalname ;
      private String dynPais_Codigo_Jsonclick ;
      private String AV11Insert_Estado_UF ;
      private String A24Estado_Nome ;
      private String A22Pais_Nome ;
      private String AV14Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode10 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z24Estado_Nome ;
      private String Z22Pais_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n25Municipio_Codigo ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T000A6_A21Pais_Codigo ;
      private String[] T000A6_A22Pais_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynEstado_UF ;
      private GXCombobox dynPais_Codigo ;
      private String[] T000A4_A24Estado_Nome ;
      private String[] T000A5_A22Pais_Nome ;
      private int[] T000A7_A25Municipio_Codigo ;
      private bool[] T000A7_n25Municipio_Codigo ;
      private String[] T000A7_A26Municipio_Nome ;
      private String[] T000A7_A24Estado_Nome ;
      private String[] T000A7_A22Pais_Nome ;
      private String[] T000A7_A23Estado_UF ;
      private int[] T000A7_A21Pais_Codigo ;
      private String[] T000A8_A24Estado_Nome ;
      private String[] T000A9_A22Pais_Nome ;
      private int[] T000A10_A25Municipio_Codigo ;
      private bool[] T000A10_n25Municipio_Codigo ;
      private int[] T000A3_A25Municipio_Codigo ;
      private bool[] T000A3_n25Municipio_Codigo ;
      private String[] T000A3_A26Municipio_Nome ;
      private String[] T000A3_A23Estado_UF ;
      private int[] T000A3_A21Pais_Codigo ;
      private int[] T000A11_A25Municipio_Codigo ;
      private bool[] T000A11_n25Municipio_Codigo ;
      private int[] T000A12_A25Municipio_Codigo ;
      private bool[] T000A12_n25Municipio_Codigo ;
      private int[] T000A2_A25Municipio_Codigo ;
      private bool[] T000A2_n25Municipio_Codigo ;
      private String[] T000A2_A26Municipio_Nome ;
      private String[] T000A2_A23Estado_UF ;
      private int[] T000A2_A21Pais_Codigo ;
      private int[] T000A13_A25Municipio_Codigo ;
      private bool[] T000A13_n25Municipio_Codigo ;
      private String[] T000A16_A24Estado_Nome ;
      private String[] T000A17_A22Pais_Nome ;
      private int[] T000A18_A34Pessoa_Codigo ;
      private int[] T000A19_A39Contratada_Codigo ;
      private int[] T000A20_A29Contratante_Codigo ;
      private int[] T000A21_A17BancoAgencia_Codigo ;
      private int[] T000A22_A25Municipio_Codigo ;
      private bool[] T000A22_n25Municipio_Codigo ;
      private int[] T000A23_A21Pais_Codigo ;
      private String[] T000A23_A22Pais_Nome ;
      private String[] T000A24_A23Estado_UF ;
      private String[] T000A24_A24Estado_Nome ;
      private String[] T000A25_A24Estado_Nome ;
      private String[] T000A26_A22Pais_Nome ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class municipio__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000A6 ;
          prmT000A6 = new Object[] {
          } ;
          Object[] prmT000A7 ;
          prmT000A7 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A4 ;
          prmT000A4 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000A5 ;
          prmT000A5 = new Object[] {
          new Object[] {"@Pais_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A8 ;
          prmT000A8 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000A9 ;
          prmT000A9 = new Object[] {
          new Object[] {"@Pais_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A10 ;
          prmT000A10 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A3 ;
          prmT000A3 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A11 ;
          prmT000A11 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A12 ;
          prmT000A12 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A2 ;
          prmT000A2 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A13 ;
          prmT000A13 = new Object[] {
          new Object[] {"@Municipio_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@Pais_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A14 ;
          prmT000A14 = new Object[] {
          new Object[] {"@Municipio_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@Pais_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A15 ;
          prmT000A15 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A16 ;
          prmT000A16 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000A17 ;
          prmT000A17 = new Object[] {
          new Object[] {"@Pais_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A18 ;
          prmT000A18 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A19 ;
          prmT000A19 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A20 ;
          prmT000A20 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A21 ;
          prmT000A21 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A22 ;
          prmT000A22 = new Object[] {
          } ;
          Object[] prmT000A23 ;
          prmT000A23 = new Object[] {
          } ;
          Object[] prmT000A24 ;
          prmT000A24 = new Object[] {
          } ;
          Object[] prmT000A25 ;
          prmT000A25 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000A26 ;
          prmT000A26 = new Object[] {
          new Object[] {"@Pais_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000A2", "SELECT [Municipio_Codigo], [Municipio_Nome], [Estado_UF], [Pais_Codigo] FROM [Municipio] WITH (UPDLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A2,1,0,true,false )
             ,new CursorDef("T000A3", "SELECT [Municipio_Codigo], [Municipio_Nome], [Estado_UF], [Pais_Codigo] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A3,1,0,true,false )
             ,new CursorDef("T000A4", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A4,1,0,true,false )
             ,new CursorDef("T000A5", "SELECT [Pais_Nome] FROM [Pais] WITH (NOLOCK) WHERE [Pais_Codigo] = @Pais_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A5,1,0,true,false )
             ,new CursorDef("T000A6", "SELECT [Pais_Codigo], [Pais_Nome] FROM [Pais] WITH (NOLOCK) ORDER BY [Pais_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A6,0,0,true,false )
             ,new CursorDef("T000A7", "SELECT TM1.[Municipio_Codigo], TM1.[Municipio_Nome], T2.[Estado_Nome], T3.[Pais_Nome], TM1.[Estado_UF], TM1.[Pais_Codigo] FROM (([Municipio] TM1 WITH (NOLOCK) INNER JOIN [Estado] T2 WITH (NOLOCK) ON T2.[Estado_UF] = TM1.[Estado_UF]) INNER JOIN [Pais] T3 WITH (NOLOCK) ON T3.[Pais_Codigo] = TM1.[Pais_Codigo]) WHERE TM1.[Municipio_Codigo] = @Municipio_Codigo ORDER BY TM1.[Municipio_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A7,100,0,true,false )
             ,new CursorDef("T000A8", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A8,1,0,true,false )
             ,new CursorDef("T000A9", "SELECT [Pais_Nome] FROM [Pais] WITH (NOLOCK) WHERE [Pais_Codigo] = @Pais_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A9,1,0,true,false )
             ,new CursorDef("T000A10", "SELECT [Municipio_Codigo] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A10,1,0,true,false )
             ,new CursorDef("T000A11", "SELECT TOP 1 [Municipio_Codigo] FROM [Municipio] WITH (NOLOCK) WHERE ( [Municipio_Codigo] > @Municipio_Codigo) ORDER BY [Municipio_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A11,1,0,true,true )
             ,new CursorDef("T000A12", "SELECT TOP 1 [Municipio_Codigo] FROM [Municipio] WITH (NOLOCK) WHERE ( [Municipio_Codigo] < @Municipio_Codigo) ORDER BY [Municipio_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A12,1,0,true,true )
             ,new CursorDef("T000A13", "INSERT INTO [Municipio]([Municipio_Nome], [Estado_UF], [Pais_Codigo]) VALUES(@Municipio_Nome, @Estado_UF, @Pais_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000A13)
             ,new CursorDef("T000A14", "UPDATE [Municipio] SET [Municipio_Nome]=@Municipio_Nome, [Estado_UF]=@Estado_UF, [Pais_Codigo]=@Pais_Codigo  WHERE [Municipio_Codigo] = @Municipio_Codigo", GxErrorMask.GX_NOMASK,prmT000A14)
             ,new CursorDef("T000A15", "DELETE FROM [Municipio]  WHERE [Municipio_Codigo] = @Municipio_Codigo", GxErrorMask.GX_NOMASK,prmT000A15)
             ,new CursorDef("T000A16", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A16,1,0,true,false )
             ,new CursorDef("T000A17", "SELECT [Pais_Nome] FROM [Pais] WITH (NOLOCK) WHERE [Pais_Codigo] = @Pais_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A17,1,0,true,false )
             ,new CursorDef("T000A18", "SELECT TOP 1 [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_MunicipioCod] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A18,1,0,true,true )
             ,new CursorDef("T000A19", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_MunicipioCod] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A19,1,0,true,true )
             ,new CursorDef("T000A20", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A20,1,0,true,true )
             ,new CursorDef("T000A21", "SELECT TOP 1 [BancoAgencia_Codigo] FROM [BancoAgencia] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A21,1,0,true,true )
             ,new CursorDef("T000A22", "SELECT [Municipio_Codigo] FROM [Municipio] WITH (NOLOCK) ORDER BY [Municipio_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A22,100,0,true,false )
             ,new CursorDef("T000A23", "SELECT [Pais_Codigo], [Pais_Nome] FROM [Pais] WITH (NOLOCK) ORDER BY [Pais_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A23,0,0,true,false )
             ,new CursorDef("T000A24", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A24,0,0,true,false )
             ,new CursorDef("T000A25", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A25,1,0,true,false )
             ,new CursorDef("T000A26", "SELECT [Pais_Nome] FROM [Pais] WITH (NOLOCK) WHERE [Pais_Codigo] = @Pais_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A26,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
