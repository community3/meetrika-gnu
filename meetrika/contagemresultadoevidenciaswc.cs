/*
               File: ContagemResultadoEvidenciasWC
        Description: Contagem Resultado Evidencias WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:53:59.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoevidenciaswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadoevidenciaswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadoevidenciaswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV7ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContagemResultado_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
               {
                  nRC_GXsfl_13 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_13_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_13_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid1_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
               {
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  A592ContagemResultado_Evidencia = GetNextPar( );
                  n592ContagemResultado_Evidencia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A592ContagemResultado_Evidencia", A592ContagemResultado_Evidencia);
                  A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n52Contratada_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
                  A1389ContagemResultado_RdmnIssueId = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1389ContagemResultado_RdmnIssueId = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1389ContagemResultado_RdmnIssueId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0)));
                  A890ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n890ContagemResultado_Responsavel = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
                  A484ContagemResultado_StatusDmn = GetNextPar( );
                  n484ContagemResultado_StatusDmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A586ContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
                  A589ContagemResultadoEvidencia_NomeArq = GetNextPar( );
                  n589ContagemResultadoEvidencia_NomeArq = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
                  A590ContagemResultadoEvidencia_TipoArq = GetNextPar( );
                  n590ContagemResultadoEvidencia_TipoArq = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
                  A646TipoDocumento_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
                  A587ContagemResultadoEvidencia_Descricao = GetNextPar( );
                  n587ContagemResultadoEvidencia_Descricao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A587ContagemResultadoEvidencia_Descricao", A587ContagemResultadoEvidencia_Descricao);
                  A591ContagemResultadoEvidencia_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  n591ContagemResultadoEvidencia_Data = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A591ContagemResultadoEvidencia_Data", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
                  A1493ContagemResultadoEvidencia_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1493ContagemResultadoEvidencia_Owner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
                  A1494ContagemResultadoEvidencia_Entidade = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1494ContagemResultadoEvidencia_Entidade", A1494ContagemResultadoEvidencia_Entidade);
                  A1770ContagemResultadoArtefato_EvdCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1770ContagemResultadoArtefato_EvdCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1770ContagemResultadoArtefato_EvdCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1770ContagemResultadoArtefato_EvdCod), 6, 0)));
                  A1771ContagemResultadoArtefato_ArtefatoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1771ContagemResultadoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1771ContagemResultadoArtefato_ArtefatoCod), 6, 0)));
                  A1751Artefatos_Descricao = GetNextPar( );
                  n1751Artefatos_Descricao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
                  A1449ContagemResultadoEvidencia_Link = GetNextPar( );
                  n1449ContagemResultadoEvidencia_Link = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1449ContagemResultadoEvidencia_Link", A1449ContagemResultadoEvidencia_Link);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV17SDT_AnexosDemanda);
                  A1109AnexoDe_Id = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1109AnexoDe_Id", StringUtil.LTrim( StringUtil.Str( (decimal)(A1109AnexoDe_Id), 6, 0)));
                  A1110AnexoDe_Tabela = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1110AnexoDe_Tabela", StringUtil.LTrim( StringUtil.Str( (decimal)(A1110AnexoDe_Tabela), 6, 0)));
                  A1106Anexo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
                  A1107Anexo_NomeArq = GetNextPar( );
                  n1107Anexo_NomeArq = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1107Anexo_NomeArq", A1107Anexo_NomeArq);
                  A1108Anexo_TipoArq = GetNextPar( );
                  n1108Anexo_TipoArq = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1108Anexo_TipoArq", A1108Anexo_TipoArq);
                  A1123Anexo_Descricao = GetNextPar( );
                  n1123Anexo_Descricao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1123Anexo_Descricao", A1123Anexo_Descricao);
                  A1103Anexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
                  A1495Anexo_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1495Anexo_Owner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
                  A1497Anexo_Entidade = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                  A1773ContagemResultadoArtefato_AnxCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1773ContagemResultadoArtefato_AnxCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1773ContagemResultadoArtefato_AnxCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1773ContagemResultadoArtefato_AnxCod), 6, 0)));
                  A1450Anexo_Link = GetNextPar( );
                  n1450Anexo_Link = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1450Anexo_Link", A1450Anexo_Link);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid1_refresh( A456ContagemResultado_Codigo, AV7ContagemResultado_Codigo, A592ContagemResultado_Evidencia, A52Contratada_AreaTrabalhoCod, A1389ContagemResultado_RdmnIssueId, A890ContagemResultado_Responsavel, A484ContagemResultado_StatusDmn, AV6WWPContext, A586ContagemResultadoEvidencia_Codigo, A589ContagemResultadoEvidencia_NomeArq, A590ContagemResultadoEvidencia_TipoArq, A646TipoDocumento_Nome, A587ContagemResultadoEvidencia_Descricao, A591ContagemResultadoEvidencia_Data, A1493ContagemResultadoEvidencia_Owner, A1494ContagemResultadoEvidencia_Entidade, A1770ContagemResultadoArtefato_EvdCod, A1771ContagemResultadoArtefato_ArtefatoCod, A1751Artefatos_Descricao, A1449ContagemResultadoEvidencia_Link, AV17SDT_AnexosDemanda, A1109AnexoDe_Id, A1110AnexoDe_Tabela, A1106Anexo_Codigo, A1107Anexo_NomeArq, A1108Anexo_TipoArq, A1123Anexo_Descricao, A1103Anexo_Data, A1495Anexo_Owner, A1497Anexo_Entidade, A1773ContagemResultadoArtefato_AnxCod, A1450Anexo_Link, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAC02( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavContagemresultado_evidencia_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_evidencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_evidencia_Enabled), 5, 0)));
               edtavCodigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCodigo_Enabled), 5, 0)));
               edtavTipodemanda_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipodemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodemanda_Enabled), 5, 0)));
               edtavArtefatos_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavArtefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavArtefatos_codigo_Enabled), 5, 0)));
               edtavPath_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPath_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPath_Enabled), 5, 0)));
               edtavNomearquivo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNomearquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNomearquivo_Enabled), 5, 0)));
               edtavTipoarquivo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipoarquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoarquivo_Enabled), 5, 0)));
               edtavTipodocumento_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipodocumento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_Enabled), 5, 0)));
               edtavArtefatos_descricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavArtefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavArtefatos_descricao_Enabled), 5, 0)));
               edtavData_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavData_Enabled), 5, 0)));
               edtavEntidade_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEntidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_Enabled), 5, 0)));
               WSC02( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Evidencias WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203242354014");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoevidenciaswc.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_13", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_13), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_EVIDENCIA", A592ContagemResultado_Evidencia);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_RDMNISSUEID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_TIPOARQ", StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"TIPODOCUMENTO_NOME", StringUtil.RTrim( A646TipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DESCRICAO", A587ContagemResultadoEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DATA", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOARTEFATO_EVDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1770ContagemResultadoArtefato_EvdCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1771ContagemResultadoArtefato_ArtefatoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ARTEFATOS_DESCRICAO", A1751Artefatos_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_LINK", A1449ContagemResultadoEvidencia_Link);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_ANEXOSDEMANDA", AV17SDT_AnexosDemanda);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_ANEXOSDEMANDA", AV17SDT_AnexosDemanda);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXODE_ID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1109AnexoDe_Id), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXODE_TABELA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1110AnexoDe_Tabela), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1106Anexo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_NOMEARQ", StringUtil.RTrim( A1107Anexo_NomeArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_TIPOARQ", StringUtil.RTrim( A1108Anexo_TipoArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_DESCRICAO", A1123Anexo_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_DATA", context.localUtil.TToC( A1103Anexo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOARTEFATO_ANXCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1773ContagemResultadoArtefato_AnxCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_LINK", A1450Anexo_Link);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ARQUIVO", A588ContagemResultadoEvidencia_Arquivo);
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_ARQUIVO", A1101Anexo_Arquivo);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_ISSUE", AV35SDT_Issue);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_ISSUE", AV35SDT_Issue);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_ARQUIVOEVIDENCIA", AV41Sdt_ArquivoEvidencia);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_ARQUIVOEVIDENCIA", AV41Sdt_ArquivoEvidencia);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vTIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TipoDocumento_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV42Sdt_ContagemResultadoEvidencias);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV42Sdt_ContagemResultadoEvidencias);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1495Anexo_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ANEXO_ENTIDADE", StringUtil.RTrim( A1497Anexo_Entidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ENTIDADE", StringUtil.RTrim( A1494ContagemResultadoEvidencia_Entidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Title", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Icon", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Icon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Confirmtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonyestext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Buttonyestext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonnotext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Buttonnotext));
         GxWebStd.gx_hidden_field( context, sPrefix+"INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Result", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormC02( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadoevidenciaswc.js", "?20203242354030");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoEvidenciasWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Evidencias WC" ;
      }

      protected void WBC00( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadoevidenciaswc.aspx");
               context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
               context.AddJavascriptSource("DVelop/Shared/dom.js", "");
               context.AddJavascriptSource("DVelop/Shared/event.js", "");
               context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
               context.AddJavascriptSource("DVelop/Shared/container.js", "");
               context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
               context.AddJavascriptSource("Window/InNewWindowRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            context.WriteHtmlText( "<p>") ;
            wb_table1_3_C02( true) ;
         }
         else
         {
            wb_table1_3_C02( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_C02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            wb_table2_37_C02( true) ;
         }
         else
         {
            wb_table2_37_C02( false) ;
         }
         return  ;
      }

      protected void wb_table2_37_C02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"INNEWWINDOWContainer"+"\"></div>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_ContagemResultadoEvidenciasWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTC02( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Evidencias WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPC00( ) ;
            }
         }
      }

      protected void WSC02( )
      {
         STARTC02( ) ;
         EVTC02( ) ;
      }

      protected void EVTC02( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL_APAGAR.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11C02 */
                                    E11C02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12C02 */
                                    E12C02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'FILEDOWNLOAD'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13C02 */
                                    E13C02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'SAVEEVIDENCIAS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14C02 */
                                    E14C02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOARTEFATOS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15C02 */
                                    E15C02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavCodigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VDISPLAY.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VDISPLAY.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC00( ) ;
                              }
                              nGXsfl_13_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
                              SubsflControlProps_132( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavCodigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCodigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCODIGO");
                                 GX_FocusControl = edtavCodigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV21Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCodigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Codigo), 6, 0)));
                              }
                              else
                              {
                                 AV21Codigo = (int)(context.localUtil.CToN( cgiGet( edtavCodigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCodigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Codigo), 6, 0)));
                              }
                              AV52TipoDemanda = cgiGet( edtavTipodemanda_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTipodemanda_Internalname, AV52TipoDemanda);
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavArtefatos_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavArtefatos_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vARTEFATOS_CODIGO");
                                 GX_FocusControl = edtavArtefatos_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV64Artefatos_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavArtefatos_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV64Artefatos_Codigo), 6, 0)));
                              }
                              else
                              {
                                 AV64Artefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavArtefatos_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavArtefatos_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV64Artefatos_Codigo), 6, 0)));
                              }
                              AV15Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Display)) ? AV73Display_GXI : context.convertURL( context.PathToRelativeUrl( AV15Display))));
                              AV14Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV14Delete)) ? AV74Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV14Delete))));
                              AV60Path = cgiGet( edtavPath_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPath_Internalname, AV60Path);
                              AV16NomeArquivo = cgiGet( edtavNomearquivo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
                              AV59TipoArquivo = cgiGet( edtavTipoarquivo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTipoarquivo_Internalname, AV59TipoArquivo);
                              AV19TipoDocumento = StringUtil.Upper( cgiGet( edtavTipodocumento_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTipodocumento_Internalname, AV19TipoDocumento);
                              AV63Artefatos_Descricao = StringUtil.Upper( cgiGet( edtavArtefatos_descricao_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavArtefatos_descricao_Internalname, AV63Artefatos_Descricao);
                              if ( context.localUtil.VCDateTime( cgiGet( edtavData_Internalname), 0, 0) == 0 )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Upload"}), 1, "vDATA");
                                 GX_FocusControl = edtavData_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV13Data = (DateTime)(DateTime.MinValue);
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavData_Internalname, context.localUtil.TToC( AV13Data, 8, 5, 0, 3, "/", ":", " "));
                              }
                              else
                              {
                                 AV13Data = context.localUtil.CToT( cgiGet( edtavData_Internalname), 0);
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavData_Internalname, context.localUtil.TToC( AV13Data, 8, 5, 0, 3, "/", ":", " "));
                              }
                              AV54Entidade = StringUtil.Upper( cgiGet( edtavEntidade_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavEntidade_Internalname, AV54Entidade);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCodigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16C02 */
                                          E16C02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCodigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17C02 */
                                          E17C02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCodigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18C02 */
                                          E18C02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VDISPLAY.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCodigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19C02 */
                                          E19C02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCodigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPC00( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCodigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEC02( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormC02( ) ;
            }
         }
      }

      protected void PAC02( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultado_evidencia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_132( ) ;
         while ( nGXsfl_13_idx <= nRC_GXsfl_13 )
         {
            sendrow_132( ) ;
            nGXsfl_13_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_13_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
            sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
            SubsflControlProps_132( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int A456ContagemResultado_Codigo ,
                                        int AV7ContagemResultado_Codigo ,
                                        String A592ContagemResultado_Evidencia ,
                                        int A52Contratada_AreaTrabalhoCod ,
                                        int A1389ContagemResultado_RdmnIssueId ,
                                        int A890ContagemResultado_Responsavel ,
                                        String A484ContagemResultado_StatusDmn ,
                                        wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                        int A586ContagemResultadoEvidencia_Codigo ,
                                        String A589ContagemResultadoEvidencia_NomeArq ,
                                        String A590ContagemResultadoEvidencia_TipoArq ,
                                        String A646TipoDocumento_Nome ,
                                        String A587ContagemResultadoEvidencia_Descricao ,
                                        DateTime A591ContagemResultadoEvidencia_Data ,
                                        int A1493ContagemResultadoEvidencia_Owner ,
                                        String A1494ContagemResultadoEvidencia_Entidade ,
                                        int A1770ContagemResultadoArtefato_EvdCod ,
                                        int A1771ContagemResultadoArtefato_ArtefatoCod ,
                                        String A1751Artefatos_Descricao ,
                                        String A1449ContagemResultadoEvidencia_Link ,
                                        IGxCollection AV17SDT_AnexosDemanda ,
                                        int A1109AnexoDe_Id ,
                                        int A1110AnexoDe_Tabela ,
                                        int A1106Anexo_Codigo ,
                                        String A1107Anexo_NomeArq ,
                                        String A1108Anexo_TipoArq ,
                                        String A1123Anexo_Descricao ,
                                        DateTime A1103Anexo_Data ,
                                        int A1495Anexo_Owner ,
                                        String A1497Anexo_Entidade ,
                                        int A1773ContagemResultadoArtefato_AnxCod ,
                                        String A1450Anexo_Link ,
                                        String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFC02( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFC02( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_evidencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_evidencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_evidencia_Enabled), 5, 0)));
         edtavCodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCodigo_Enabled), 5, 0)));
         edtavTipodemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipodemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodemanda_Enabled), 5, 0)));
         edtavArtefatos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavArtefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavArtefatos_codigo_Enabled), 5, 0)));
         edtavPath_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPath_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPath_Enabled), 5, 0)));
         edtavNomearquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNomearquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNomearquivo_Enabled), 5, 0)));
         edtavTipoarquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipoarquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoarquivo_Enabled), 5, 0)));
         edtavTipodocumento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipodocumento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_Enabled), 5, 0)));
         edtavArtefatos_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavArtefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavArtefatos_descricao_Enabled), 5, 0)));
         edtavData_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavData_Enabled), 5, 0)));
         edtavEntidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEntidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_Enabled), 5, 0)));
      }

      protected void RFC02( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 13;
         /* Execute user event: E17C02 */
         E17C02 ();
         nGXsfl_13_idx = 1;
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
         nGXsfl_13_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", sPrefix);
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "WorkWith");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_132( ) ;
            /* Execute user event: E18C02 */
            E18C02 ();
            wbEnd = 13;
            WBC00( ) ;
         }
         nGXsfl_13_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPC00( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_evidencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_evidencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_evidencia_Enabled), 5, 0)));
         edtavCodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCodigo_Enabled), 5, 0)));
         edtavTipodemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipodemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodemanda_Enabled), 5, 0)));
         edtavArtefatos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavArtefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavArtefatos_codigo_Enabled), 5, 0)));
         edtavPath_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPath_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPath_Enabled), 5, 0)));
         edtavNomearquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNomearquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNomearquivo_Enabled), 5, 0)));
         edtavTipoarquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipoarquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoarquivo_Enabled), 5, 0)));
         edtavTipodocumento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTipodocumento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_Enabled), 5, 0)));
         edtavArtefatos_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavArtefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavArtefatos_descricao_Enabled), 5, 0)));
         edtavData_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavData_Enabled), 5, 0)));
         edtavEntidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEntidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16C02 */
         E16C02 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV12ContagemResultado_Evidencia = cgiGet( edtavContagemresultado_evidencia_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12ContagemResultado_Evidencia", AV12ContagemResultado_Evidencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
            /* Read saved values. */
            nRC_GXsfl_13 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_13"), ",", "."));
            wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
            Dvelop_confirmpanel_apagar_Title = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Title");
            Dvelop_confirmpanel_apagar_Icon = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Icon");
            Dvelop_confirmpanel_apagar_Confirmtext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtext");
            Dvelop_confirmpanel_apagar_Buttonyestext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonyestext");
            Dvelop_confirmpanel_apagar_Buttonnotext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonnotext");
            Innewwindow_Target = cgiGet( sPrefix+"INNEWWINDOW_Target");
            Dvelop_confirmpanel_apagar_Result = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16C02 */
         E16C02 ();
         if (returnInSub) return;
      }

      protected void E16C02( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         /* Execute user subroutine: 'ARTEFATOS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E17C02( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         bttBtnsaveevidencias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnsaveevidencias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnsaveevidencias_Visible), 5, 0)));
         bttBtn_importarredmine_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_importarredmine_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_importarredmine_Visible), 5, 0)));
         AV17SDT_AnexosDemanda.Clear();
         /* Using cursor H00C02 */
         pr_default.execute(0, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = H00C02_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00C02_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00C02_A456ContagemResultado_Codigo[0];
            A592ContagemResultado_Evidencia = H00C02_A592ContagemResultado_Evidencia[0];
            n592ContagemResultado_Evidencia = H00C02_n592ContagemResultado_Evidencia[0];
            A52Contratada_AreaTrabalhoCod = H00C02_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00C02_n52Contratada_AreaTrabalhoCod[0];
            A1389ContagemResultado_RdmnIssueId = H00C02_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = H00C02_n1389ContagemResultado_RdmnIssueId[0];
            A890ContagemResultado_Responsavel = H00C02_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00C02_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = H00C02_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00C02_n484ContagemResultado_StatusDmn[0];
            A52Contratada_AreaTrabalhoCod = H00C02_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00C02_n52Contratada_AreaTrabalhoCod[0];
            AV12ContagemResultado_Evidencia = A592ContagemResultado_Evidencia;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12ContagemResultado_Evidencia", AV12ContagemResultado_Evidencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_EVIDENCIA", GetSecureSignedToken( sPrefix, AV12ContagemResultado_Evidencia));
            AV30AreaTrabalho = A52Contratada_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30AreaTrabalho), 6, 0)));
            AV31RdmnIssueId = A1389ContagemResultado_RdmnIssueId;
            AV61Responsavel = A890ContagemResultado_Responsavel;
            AV62StatusDmn = A484ContagemResultado_StatusDmn;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( StringUtil.StrCmp(AV62StatusDmn, "S") == 0 )
         {
            edtavDisplay_Visible = ((AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Userehcontratante||(AV6WWPContext.gxTpr_Userid==AV61Responsavel)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         }
         AV51i = 0;
         GXt_char1 = "";
         new prc_criarpastadeevd(context ).execute( ref  AV7ContagemResultado_Codigo, ref  AV51i, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         AV56Directory.Source = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV17SDT_AnexosDemanda", AV17SDT_AnexosDemanda);
      }

      private void E18C02( )
      {
         /* Grid1_Load Routine */
         /* Using cursor H00C03 */
         pr_default.execute(1, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A490ContagemResultado_ContratadaCod = H00C03_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00C03_n490ContagemResultado_ContratadaCod[0];
            A645TipoDocumento_Codigo = H00C03_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = H00C03_n645TipoDocumento_Codigo[0];
            A586ContagemResultadoEvidencia_Codigo = H00C03_A586ContagemResultadoEvidencia_Codigo[0];
            A456ContagemResultado_Codigo = H00C03_A456ContagemResultado_Codigo[0];
            A590ContagemResultadoEvidencia_TipoArq = H00C03_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = H00C03_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A589ContagemResultadoEvidencia_NomeArq = H00C03_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = H00C03_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A646TipoDocumento_Nome = H00C03_A646TipoDocumento_Nome[0];
            A587ContagemResultadoEvidencia_Descricao = H00C03_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = H00C03_n587ContagemResultadoEvidencia_Descricao[0];
            A591ContagemResultadoEvidencia_Data = H00C03_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = H00C03_n591ContagemResultadoEvidencia_Data[0];
            A1449ContagemResultadoEvidencia_Link = H00C03_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = H00C03_n1449ContagemResultadoEvidencia_Link[0];
            A52Contratada_AreaTrabalhoCod = H00C03_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00C03_n52Contratada_AreaTrabalhoCod[0];
            A1493ContagemResultadoEvidencia_Owner = H00C03_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = H00C03_n1493ContagemResultadoEvidencia_Owner[0];
            A646TipoDocumento_Nome = H00C03_A646TipoDocumento_Nome[0];
            A490ContagemResultado_ContratadaCod = H00C03_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00C03_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00C03_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00C03_n52Contratada_AreaTrabalhoCod[0];
            GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
            new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
            A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1494ContagemResultadoEvidencia_Entidade", A1494ContagemResultadoEvidencia_Entidade);
            AV18SDT_AnexosDemandaItem = new SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem(context);
            AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda = "L";
            AV18SDT_AnexosDemandaItem.gxTpr_Codigo = A586ContagemResultadoEvidencia_Codigo;
            AV18SDT_AnexosDemandaItem.gxTpr_Nomearquivo = A589ContagemResultadoEvidencia_NomeArq+"."+A590ContagemResultadoEvidencia_TipoArq;
            AV18SDT_AnexosDemandaItem.gxTpr_Tipoarquivo = A590ContagemResultadoEvidencia_TipoArq;
            AV18SDT_AnexosDemandaItem.gxTpr_Tipodocumento = A646TipoDocumento_Nome;
            AV18SDT_AnexosDemandaItem.gxTpr_Descricao = A587ContagemResultadoEvidencia_Descricao;
            AV18SDT_AnexosDemandaItem.gxTpr_Dataupload = A591ContagemResultadoEvidencia_Data;
            AV18SDT_AnexosDemandaItem.gxTpr_Owner = A1493ContagemResultadoEvidencia_Owner;
            AV18SDT_AnexosDemandaItem.gxTpr_Entidade = A1494ContagemResultadoEvidencia_Entidade;
            /* Using cursor H00C04 */
            pr_default.execute(2, new Object[] {A586ContagemResultadoEvidencia_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1770ContagemResultadoArtefato_EvdCod = H00C04_A1770ContagemResultadoArtefato_EvdCod[0];
               n1770ContagemResultadoArtefato_EvdCod = H00C04_n1770ContagemResultadoArtefato_EvdCod[0];
               A1771ContagemResultadoArtefato_ArtefatoCod = H00C04_A1771ContagemResultadoArtefato_ArtefatoCod[0];
               A1751Artefatos_Descricao = H00C04_A1751Artefatos_Descricao[0];
               n1751Artefatos_Descricao = H00C04_n1751Artefatos_Descricao[0];
               A1751Artefatos_Descricao = H00C04_A1751Artefatos_Descricao[0];
               n1751Artefatos_Descricao = H00C04_n1751Artefatos_Descricao[0];
               AV18SDT_AnexosDemandaItem.gxTpr_Artefatos_codigo = A1771ContagemResultadoArtefato_ArtefatoCod;
               AV18SDT_AnexosDemandaItem.gxTpr_Artefatos_descricao = A1751Artefatos_Descricao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( StringUtil.Len( A1449ContagemResultadoEvidencia_Link) > 0 )
            {
               AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda = "U";
               AV18SDT_AnexosDemandaItem.gxTpr_Nomearquivo = A1449ContagemResultadoEvidencia_Link;
            }
            AV17SDT_AnexosDemanda.Add(AV18SDT_AnexosDemandaItem, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         /* Using cursor H00C09 */
         pr_default.execute(3, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A645TipoDocumento_Codigo = H00C09_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = H00C09_n645TipoDocumento_Codigo[0];
            A1106Anexo_Codigo = H00C09_A1106Anexo_Codigo[0];
            A1110AnexoDe_Tabela = H00C09_A1110AnexoDe_Tabela[0];
            A1109AnexoDe_Id = H00C09_A1109AnexoDe_Id[0];
            A1108Anexo_TipoArq = H00C09_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = H00C09_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = H00C09_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = H00C09_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A1123Anexo_Descricao = H00C09_A1123Anexo_Descricao[0];
            n1123Anexo_Descricao = H00C09_n1123Anexo_Descricao[0];
            A1103Anexo_Data = H00C09_A1103Anexo_Data[0];
            A646TipoDocumento_Nome = H00C09_A646TipoDocumento_Nome[0];
            A1450Anexo_Link = H00C09_A1450Anexo_Link[0];
            n1450Anexo_Link = H00C09_n1450Anexo_Link[0];
            A1496Anexo_AreaTrabalhoCod = H00C09_A1496Anexo_AreaTrabalhoCod[0];
            n1496Anexo_AreaTrabalhoCod = H00C09_n1496Anexo_AreaTrabalhoCod[0];
            A1495Anexo_Owner = H00C09_A1495Anexo_Owner[0];
            n1495Anexo_Owner = H00C09_n1495Anexo_Owner[0];
            A645TipoDocumento_Codigo = H00C09_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = H00C09_n645TipoDocumento_Codigo[0];
            A1108Anexo_TipoArq = H00C09_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = H00C09_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = H00C09_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = H00C09_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A1123Anexo_Descricao = H00C09_A1123Anexo_Descricao[0];
            n1123Anexo_Descricao = H00C09_n1123Anexo_Descricao[0];
            A1103Anexo_Data = H00C09_A1103Anexo_Data[0];
            A1450Anexo_Link = H00C09_A1450Anexo_Link[0];
            n1450Anexo_Link = H00C09_n1450Anexo_Link[0];
            A1495Anexo_Owner = H00C09_A1495Anexo_Owner[0];
            n1495Anexo_Owner = H00C09_n1495Anexo_Owner[0];
            A646TipoDocumento_Nome = H00C09_A646TipoDocumento_Nome[0];
            A1496Anexo_AreaTrabalhoCod = H00C09_A1496Anexo_AreaTrabalhoCod[0];
            n1496Anexo_AreaTrabalhoCod = H00C09_n1496Anexo_AreaTrabalhoCod[0];
            GXt_char1 = A1497Anexo_Entidade;
            new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            A1497Anexo_Entidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
            AV18SDT_AnexosDemandaItem = new SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem(context);
            AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda = "G";
            AV18SDT_AnexosDemandaItem.gxTpr_Codigo = A1106Anexo_Codigo;
            AV18SDT_AnexosDemandaItem.gxTpr_Nomearquivo = A1107Anexo_NomeArq+"."+A1108Anexo_TipoArq;
            AV18SDT_AnexosDemandaItem.gxTpr_Tipoarquivo = A1108Anexo_TipoArq;
            AV18SDT_AnexosDemandaItem.gxTpr_Tipodocumento = "";
            AV18SDT_AnexosDemandaItem.gxTpr_Descricao = A1123Anexo_Descricao;
            AV18SDT_AnexosDemandaItem.gxTpr_Dataupload = A1103Anexo_Data;
            AV18SDT_AnexosDemandaItem.gxTpr_Owner = A1495Anexo_Owner;
            AV18SDT_AnexosDemandaItem.gxTpr_Entidade = A1497Anexo_Entidade;
            AV18SDT_AnexosDemandaItem.gxTpr_Tipodocumento = A646TipoDocumento_Nome;
            /* Using cursor H00C010 */
            pr_default.execute(4, new Object[] {A1106Anexo_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1773ContagemResultadoArtefato_AnxCod = H00C010_A1773ContagemResultadoArtefato_AnxCod[0];
               n1773ContagemResultadoArtefato_AnxCod = H00C010_n1773ContagemResultadoArtefato_AnxCod[0];
               A1771ContagemResultadoArtefato_ArtefatoCod = H00C010_A1771ContagemResultadoArtefato_ArtefatoCod[0];
               A1751Artefatos_Descricao = H00C010_A1751Artefatos_Descricao[0];
               n1751Artefatos_Descricao = H00C010_n1751Artefatos_Descricao[0];
               A1751Artefatos_Descricao = H00C010_A1751Artefatos_Descricao[0];
               n1751Artefatos_Descricao = H00C010_n1751Artefatos_Descricao[0];
               AV18SDT_AnexosDemandaItem.gxTpr_Artefatos_codigo = A1771ContagemResultadoArtefato_ArtefatoCod;
               AV18SDT_AnexosDemandaItem.gxTpr_Artefatos_descricao = A1751Artefatos_Descricao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
            if ( StringUtil.Len( A1450Anexo_Link) > 0 )
            {
               AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda = "H";
               AV18SDT_AnexosDemandaItem.gxTpr_Nomearquivo = A1450Anexo_Link;
            }
            AV17SDT_AnexosDemanda.Add(AV18SDT_AnexosDemandaItem, 0);
            pr_default.readNext(3);
         }
         pr_default.close(3);
         /* Execute user subroutine: 'GETFILES' */
         S122 ();
         if (returnInSub) return;
         AV17SDT_AnexosDemanda.Sort("NomeArquivo");
         AV72GXV1 = 1;
         while ( AV72GXV1 <= AV17SDT_AnexosDemanda.Count )
         {
            AV18SDT_AnexosDemandaItem = ((SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem)AV17SDT_AnexosDemanda.Item(AV72GXV1));
            AV15Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV15Display);
            AV73Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Tooltiptext = "Mostrar";
            edtavDisplay_Linktarget = "_blank";
            if ( ( StringUtil.StrCmp(AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda, "L") == 0 ) || ( StringUtil.StrCmp(AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda, "U") == 0 ) )
            {
               AV14Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV14Delete);
               AV74Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
               edtavDelete_Enabled = 1;
               edtavDelete_Tooltiptext = "Eliminar";
               if ( StringUtil.StrCmp(AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda, "U") == 0 )
               {
                  AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda = "H";
               }
            }
            else
            {
               AV14Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV14Delete);
               AV74Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
               edtavDelete_Enabled = 0;
               edtavDelete_Tooltiptext = "Anexo compartilhado n�o permite exclus�o.";
            }
            AV21Codigo = AV18SDT_AnexosDemandaItem.gxTpr_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCodigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Codigo), 6, 0)));
            AV13Data = AV18SDT_AnexosDemandaItem.gxTpr_Dataupload;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavData_Internalname, context.localUtil.TToC( AV13Data, 8, 5, 0, 3, "/", ":", " "));
            AV52TipoDemanda = AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTipodemanda_Internalname, AV52TipoDemanda);
            AV16NomeArquivo = AV18SDT_AnexosDemandaItem.gxTpr_Nomearquivo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
            AV59TipoArquivo = AV18SDT_AnexosDemandaItem.gxTpr_Tipoarquivo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTipoarquivo_Internalname, AV59TipoArquivo);
            AV19TipoDocumento = AV18SDT_AnexosDemandaItem.gxTpr_Tipodocumento;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTipodocumento_Internalname, AV19TipoDocumento);
            AV54Entidade = AV18SDT_AnexosDemandaItem.gxTpr_Entidade;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavEntidade_Internalname, AV54Entidade);
            edtavNomearquivo_Linktarget = "";
            edtavNomearquivo_Link = "";
            edtavNomearquivo_Tooltiptext = AV18SDT_AnexosDemandaItem.gxTpr_Descricao;
            AV64Artefatos_Codigo = AV18SDT_AnexosDemandaItem.gxTpr_Artefatos_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavArtefatos_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV64Artefatos_Codigo), 6, 0)));
            AV63Artefatos_Descricao = AV18SDT_AnexosDemandaItem.gxTpr_Artefatos_descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavArtefatos_descricao_Internalname, AV63Artefatos_Descricao);
            if ( StringUtil.StrCmp(AV52TipoDemanda, "F") == 0 )
            {
               AV16NomeArquivo = StringUtil.Substring( AV16NomeArquivo, StringUtil.StringSearchRev( AV16NomeArquivo, "\\", -1)+1, StringUtil.Len( AV16NomeArquivo));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
               AV60Path = AV56Directory.GetAbsoluteName();
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPath_Internalname, AV60Path);
            }
            else if ( StringUtil.StrCmp(AV52TipoDemanda, "H") == 0 )
            {
               if ( StringUtil.StringSearch( AV16NomeArquivo, "http", 1) == 0 )
               {
                  AV16NomeArquivo = StringUtil.StringReplace( AV16NomeArquivo, "\\\\", "");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
                  AV16NomeArquivo = StringUtil.StringReplace( AV16NomeArquivo, "//", "");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
                  AV53Html = "http:\\\\" + AV16NomeArquivo;
                  edtavNomearquivo_Link = formatLink(AV53Html) ;
               }
               else
               {
                  edtavNomearquivo_Link = formatLink(AV16NomeArquivo) ;
               }
               edtavNomearquivo_Linktarget = "_blank";
               edtavNomearquivo_Tooltiptext = AV16NomeArquivo+StringUtil.NewLine( )+AV18SDT_AnexosDemandaItem.gxTpr_Descricao;
               AV16NomeArquivo = StringUtil.StringReplace( AV16NomeArquivo, "http:", "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
               AV16NomeArquivo = StringUtil.StringReplace( AV16NomeArquivo, "https:", "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
               AV16NomeArquivo = StringUtil.StringReplace( AV16NomeArquivo, "\\\\", "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
               AV16NomeArquivo = StringUtil.StringReplace( AV16NomeArquivo, "//", "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
               if ( StringUtil.Len( AV16NomeArquivo) > 63 )
               {
                  AV16NomeArquivo = StringUtil.Substring( AV16NomeArquivo, 1, 60) + "...";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNomearquivo_Internalname, AV16NomeArquivo);
               }
            }
            edtavDelete_Visible = (((AV18SDT_AnexosDemandaItem.gxTpr_Owner==AV6WWPContext.gxTpr_Userid)) ? 1 : 0);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 13;
            }
            sendrow_132( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_13_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(13, Grid1Row);
            }
            AV72GXV1 = (int)(AV72GXV1+1);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV17SDT_AnexosDemanda", AV17SDT_AnexosDemanda);
      }

      protected void E19C02( )
      {
         /* Display_Click Routine */
         if ( StringUtil.StrCmp(AV52TipoDemanda, "H") == 0 )
         {
            if ( StringUtil.StringSearch( AV16NomeArquivo, "http", 1) == 0 )
            {
               AV53Html = "http:\\\\" + StringUtil.Trim( AV16NomeArquivo);
               Innewwindow_Target = AV53Html;
               context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Innewwindow_Internalname, "Target", Innewwindow_Target);
            }
            else
            {
               Innewwindow_Target = StringUtil.Trim( AV16NomeArquivo);
               context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Innewwindow_Internalname, "Target", Innewwindow_Target);
            }
            this.executeUsercontrolMethod(sPrefix, false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         }
         else if ( StringUtil.StrCmp(AV52TipoDemanda, "L") == 0 )
         {
            /* Using cursor H00C011 */
            pr_default.execute(5, new Object[] {AV21Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A586ContagemResultadoEvidencia_Codigo = H00C011_A586ContagemResultadoEvidencia_Codigo[0];
               A589ContagemResultadoEvidencia_NomeArq = H00C011_A589ContagemResultadoEvidencia_NomeArq[0];
               n589ContagemResultadoEvidencia_NomeArq = H00C011_n589ContagemResultadoEvidencia_NomeArq[0];
               A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
               A590ContagemResultadoEvidencia_TipoArq = H00C011_A590ContagemResultadoEvidencia_TipoArq[0];
               n590ContagemResultadoEvidencia_TipoArq = H00C011_n590ContagemResultadoEvidencia_TipoArq[0];
               A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
               A588ContagemResultadoEvidencia_Arquivo = H00C011_A588ContagemResultadoEvidencia_Arquivo[0];
               n588ContagemResultadoEvidencia_Arquivo = H00C011_n588ContagemResultadoEvidencia_Arquivo[0];
               context.wjLoc = formatLink("aprc_downloadfile.aspx") + "?" + UrlEncode(StringUtil.RTrim(A588ContagemResultadoEvidencia_Arquivo)) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim(StringUtil.Trim( A589ContagemResultadoEvidencia_NomeArq)+"."+StringUtil.Trim( A590ContagemResultadoEvidencia_TipoArq)));
               context.wjLocDisableFrm = 0;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
         }
         else if ( StringUtil.StrCmp(AV52TipoDemanda, "G") == 0 )
         {
            /* Using cursor H00C012 */
            pr_default.execute(6, new Object[] {AV21Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1106Anexo_Codigo = H00C012_A1106Anexo_Codigo[0];
               A1107Anexo_NomeArq = H00C012_A1107Anexo_NomeArq[0];
               n1107Anexo_NomeArq = H00C012_n1107Anexo_NomeArq[0];
               A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
               A1108Anexo_TipoArq = H00C012_A1108Anexo_TipoArq[0];
               n1108Anexo_TipoArq = H00C012_n1108Anexo_TipoArq[0];
               A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
               A1101Anexo_Arquivo = H00C012_A1101Anexo_Arquivo[0];
               n1101Anexo_Arquivo = H00C012_n1101Anexo_Arquivo[0];
               context.wjLoc = formatLink("aprc_downloadfile.aspx") + "?" + UrlEncode(StringUtil.RTrim(A1101Anexo_Arquivo)) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim(StringUtil.Trim( A1107Anexo_NomeArq)+"."+StringUtil.Trim( A1108Anexo_TipoArq)));
               context.wjLocDisableFrm = 0;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
         }
         else if ( StringUtil.StrCmp(AV52TipoDemanda, "F") == 0 )
         {
            context.wjLoc = formatLink("aprc_downloadfile.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV60Path+"\\"+AV16NomeArquivo)) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim(AV16NomeArquivo));
            context.wjLocDisableFrm = 0;
         }
      }

      protected void E12C02( )
      {
         /* 'DoInsert' Routine */
         context.PopUp(formatLink("wp_contagemresultadoevidencia.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo), new Object[] {"AV7ContagemResultado_Codigo"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E11C02( )
      {
         /* Dvelop_confirmpanel_apagar_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_confirmpanel_apagar_Result, "Yes") == 0 )
         {
            if ( AV64Artefatos_Codigo > 0 )
            {
               new prc_dltevidenciadoartefatodaos(context ).execute( ref  AV7ContagemResultado_Codigo, ref  AV64Artefatos_Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavArtefatos_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV64Artefatos_Codigo), 6, 0)));
            }
            AV20ContagemResultadoEvidencia.Load(AV21Codigo);
            AV20ContagemResultadoEvidencia.Delete();
            if ( AV20ContagemResultadoEvidencia.Success() )
            {
               context.CommitDataStores( "ContagemResultadoEvidenciasWC");
               context.DoAjaxRefreshCmp(sPrefix);
            }
            else
            {
               context.RollbackDataStores( "ContagemResultadoEvidenciasWC");
            }
         }
      }

      protected void E13C02( )
      {
         /* 'FileDownload' Routine */
         /* Execute user subroutine: 'FILEDOWNLOAD' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV36Attachment", AV36Attachment);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV41Sdt_ArquivoEvidencia", AV41Sdt_ArquivoEvidencia);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42Sdt_ContagemResultadoEvidencias", AV42Sdt_ContagemResultadoEvidencias);
      }

      protected void E14C02( )
      {
         /* 'SaveEvidencias' Routine */
         /* Execute user subroutine: 'SAVEEVIDENCIAS' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'CHECKREDMINE' Routine */
         /* Using cursor H00C013 */
         pr_default.execute(7, new Object[] {AV30AreaTrabalho});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A29Contratante_Codigo = H00C013_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00C013_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00C013_A5AreaTrabalho_Codigo[0];
            /* Using cursor H00C014 */
            pr_default.execute(8, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A1384Redmine_ContratanteCod = H00C014_A1384Redmine_ContratanteCod[0];
               A1381Redmine_Host = H00C014_A1381Redmine_Host[0];
               A1382Redmine_Url = H00C014_A1382Redmine_Url[0];
               A1383Redmine_Key = H00C014_A1383Redmine_Key[0];
               AV32Host = A1381Redmine_Host;
               AV33Url = A1382Redmine_Url;
               AV34Key = A1383Redmine_Key;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(8);
            }
            pr_default.close(8);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
         AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Clear();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32Host)) )
         {
            GX_msglist.addItem("Sem configura��o do Redmine da Contratante para consultar os anexos!");
         }
         else
         {
            AV29httpclient.Host = StringUtil.Trim( AV32Host);
            AV29httpclient.BaseURL = StringUtil.Trim( AV33Url);
            AV44Execute = "issues/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV31RdmnIssueId), 6, 0)) + ".xml?&include=attachments";
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Key)) )
            {
               AV44Execute = AV44Execute + "&key=" + StringUtil.Trim( AV34Key);
            }
            AV29httpclient.Execute("GET", AV44Execute);
            AV35SDT_Issue.FromXml(AV29httpclient.ToString(), "");
         }
         AV51i = 0;
         AV79GXV2 = 1;
         while ( AV79GXV2 <= AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Count )
         {
            AV36Attachment = ((SdtSDT_Redmineissue_attachments_attachment)AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Item(AV79GXV2));
            AV38FileName = StringUtil.Trim( AV36Attachment.gxTpr_Filename);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FileName", AV38FileName);
            AV39Ext = StringUtil.Trim( StringUtil.Substring( AV38FileName, StringUtil.StringSearchRev( AV38FileName, ".", -1)+1, 4));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39Ext", AV39Ext);
            AV38FileName = StringUtil.Substring( AV38FileName, 1, StringUtil.StringSearchRev( AV38FileName, ".", -1)-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FileName", AV38FileName);
            AV51i = (short)(AV51i+1);
            /* Using cursor H00C015 */
            pr_default.execute(9, new Object[] {AV7ContagemResultado_Codigo, AV38FileName, AV39Ext});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1393ContagemResultadoEvidencia_RdmnCreated = H00C015_A1393ContagemResultadoEvidencia_RdmnCreated[0];
               n1393ContagemResultadoEvidencia_RdmnCreated = H00C015_n1393ContagemResultadoEvidencia_RdmnCreated[0];
               A590ContagemResultadoEvidencia_TipoArq = H00C015_A590ContagemResultadoEvidencia_TipoArq[0];
               n590ContagemResultadoEvidencia_TipoArq = H00C015_n590ContagemResultadoEvidencia_TipoArq[0];
               A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
               A589ContagemResultadoEvidencia_NomeArq = H00C015_A589ContagemResultadoEvidencia_NomeArq[0];
               n589ContagemResultadoEvidencia_NomeArq = H00C015_n589ContagemResultadoEvidencia_NomeArq[0];
               A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
               A456ContagemResultado_Codigo = H00C015_A456ContagemResultado_Codigo[0];
               if ( A1393ContagemResultadoEvidencia_RdmnCreated == context.localUtil.CToT( AV36Attachment.gxTpr_Created_on, 2) )
               {
                  AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.RemoveItem(AV51i);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(9);
            }
            pr_default.close(9);
            AV79GXV2 = (int)(AV79GXV2+1);
         }
         bttBtn_importarredmine_Caption = "Importar "+StringUtil.Trim( StringUtil.Str( (decimal)(AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Count), 9, 0))+" anexos do Redmine";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_importarredmine_Internalname, "Caption", bttBtn_importarredmine_Caption);
         bttBtn_importarredmine_Visible = ((AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Count>0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_importarredmine_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_importarredmine_Visible), 5, 0)));
      }

      protected void S132( )
      {
         /* 'FILEDOWNLOAD' Routine */
         AV81GXV3 = 1;
         while ( AV81GXV3 <= AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Count )
         {
            AV36Attachment = ((SdtSDT_Redmineissue_attachments_attachment)AV35SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Item(AV81GXV3));
            lblTbjava_Caption = "var $idown;"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"downloadURL : function(url) {"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"if ($idown) {"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"     $idown.attr('src',url);"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"} else {"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"     $idown = $('<iframe>', { id:'idown', src:url }).hide().appendTo('body');"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"}"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"},"+StringUtil.NewLine( );
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            lblTbjava_Caption = lblTbjava_Caption+"downloadURL('"+AV36Attachment.gxTpr_Content_url+"');";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            AV38FileName = StringUtil.Trim( AV36Attachment.gxTpr_Filename);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FileName", AV38FileName);
            AV45FileUrl = AV36Attachment.gxTpr_Content_url;
            AV46FileContentType = AV36Attachment.gxTpr_Content_type;
            context.wjLoc = formatLink("aprc_downloadfile.aspx") + "?" + UrlEncode(StringUtil.RTrim(StringUtil.StringReplace( AV45FileUrl, AV38FileName, ""))) + "," + UrlEncode(StringUtil.RTrim(AV46FileContentType)) + "," + UrlEncode(StringUtil.RTrim(AV38FileName));
            context.wjLocDisableFrm = 0;
            AV39Ext = StringUtil.Trim( StringUtil.Substring( AV38FileName, StringUtil.StringSearchRev( AV38FileName, ".", -1)+1, 4));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39Ext", AV39Ext);
            AV38FileName = StringUtil.Substring( AV38FileName, 1, StringUtil.StringSearchRev( AV38FileName, ".", -1)-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FileName", AV38FileName);
            AV41Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV41Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo = StringUtil.Trim( AV36Attachment.gxTpr_Filename);
            AV41Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo = AV47TipoDocumento_Codigo;
            AV41Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = AV38FileName;
            AV41Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq = AV39Ext;
            AV41Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao = AV36Attachment.gxTpr_Description;
            AV41Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_rdmncreated = context.localUtil.CToT( AV36Attachment.gxTpr_Created_on, 2);
            AV42Sdt_ContagemResultadoEvidencias.Add(AV41Sdt_ArquivoEvidencia, 0);
            AV41Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
            AV81GXV3 = (int)(AV81GXV3+1);
         }
         AV40WebSession.Set("ArquivosEvd", AV42Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
      }

      protected void S122( )
      {
         /* 'GETFILES' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Directory.GetAbsoluteName())) )
         {
            AV83GXV5 = 1;
            AV82GXV4 = AV56Directory.GetFiles("");
            while ( AV83GXV5 <= AV82GXV4.ItemCount )
            {
               AV58File = AV82GXV4.Item(AV83GXV5);
               AV18SDT_AnexosDemandaItem = new SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem(context);
               AV18SDT_AnexosDemandaItem.gxTpr_Tipodemanda = "F";
               AV18SDT_AnexosDemandaItem.gxTpr_Nomearquivo = AV58File.GetAbsoluteName();
               AV18SDT_AnexosDemandaItem.gxTpr_Tipoarquivo = StringUtil.Substring( AV58File.GetName(), StringUtil.StringSearch( AV58File.GetName(), ".", 1)+1, 5);
               AV18SDT_AnexosDemandaItem.gxTpr_Tipodocumento = "";
               AV18SDT_AnexosDemandaItem.gxTpr_Descricao = "";
               AV18SDT_AnexosDemandaItem.gxTpr_Dataupload = AV58File.GetLastModified();
               AV18SDT_AnexosDemandaItem.gxTpr_Owner = 0;
               AV18SDT_AnexosDemandaItem.gxTpr_Entidade = "";
               AV17SDT_AnexosDemanda.Add(AV18SDT_AnexosDemandaItem, 0);
               AV83GXV5 = (int)(AV83GXV5+1);
            }
         }
      }

      protected void S142( )
      {
         /* 'SAVEEVIDENCIAS' Routine */
         new prc_newevidenciademanda(context ).execute( ref  AV7ContagemResultado_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S112( )
      {
         /* 'ARTEFATOS' Routine */
         AV84GXLvl354 = 0;
         /* Using cursor H00C016 */
         pr_default.execute(10, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A1772ContagemResultadoArtefato_OSCod = H00C016_A1772ContagemResultadoArtefato_OSCod[0];
            AV84GXLvl354 = 1;
            imgArtefatos_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgArtefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgArtefatos_Visible), 5, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(10);
         }
         pr_default.close(10);
         if ( AV84GXLvl354 == 0 )
         {
            imgArtefatos_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgArtefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgArtefatos_Visible), 5, 0)));
         }
      }

      protected void E15C02( )
      {
         /* 'DoArtefatos' Routine */
         AV40WebSession.Set("Caller", "Evidencias");
         context.PopUp(formatLink("wp_linkartefatos.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode("" +AV6WWPContext.gxTpr_Userid), new Object[] {});
      }

      protected void wb_table2_37_C02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_APAGARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_37_C02e( true) ;
         }
         else
         {
            wb_table2_37_C02e( false) ;
         }
      }

      protected void wb_table1_3_C02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_evidencia_Internalname, "Descri��o", "", "", lblTextblockcontagemresultado_evidencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoEvidenciasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContagemresultado_evidencia_Internalname, AV12ContagemResultado_Evidencia, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", 0, 1, edtavContagemresultado_evidencia_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_ContagemResultadoEvidenciasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblanexo_Internalname, "Anexos", "", "", lblLblanexo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescriptionCell", 0, "", 1, 1, 0, "HLP_ContagemResultadoEvidenciasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"Grid1Container"+"DivS\" data-gxgridid=\"13\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Path") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Arquivo / Link") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo documento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Artefato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Upload") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Respons�vel") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "WorkWith");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", sPrefix);
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Codigo), 6, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCodigo_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV52TipoDemanda));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTipodemanda_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64Artefatos_Codigo), 6, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavArtefatos_codigo_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV15Display));
               Grid1Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavDisplay_Linktarget));
               Grid1Column.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV14Delete));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               Grid1Column.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", AV60Path);
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPath_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV16NomeArquivo));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNomearquivo_Enabled), 5, 0, ".", "")));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavNomearquivo_Link));
               Grid1Column.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavNomearquivo_Linktarget));
               Grid1Column.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavNomearquivo_Tooltiptext));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV59TipoArquivo));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTipoarquivo_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV19TipoDocumento));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTipodocumento_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", AV63Artefatos_Descricao);
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavArtefatos_descricao_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.localUtil.TToC( AV13Data, 10, 8, 0, 3, "/", ":", " "));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavData_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV54Entidade));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEntidade_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 13 )
         {
            wbEnd = 0;
            nRC_GXsfl_13 = (short)(nGXsfl_13_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid1", Grid1Container);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "&nbsp;") ;
            wb_table3_27_C02( true) ;
         }
         else
         {
            wb_table3_27_C02( false) ;
         }
         return  ;
      }

      protected void wb_table3_27_C02e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_importarredmine_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(13), 2, 0)+","+"null"+");", bttBtn_importarredmine_Caption, bttBtn_importarredmine_Jsonclick, 5, "Importar anexos do Redmine", "", StyleString, ClassString, bttBtn_importarredmine_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'FILEDOWNLOAD\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoEvidenciasWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnsaveevidencias_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(13), 2, 0)+","+"null"+");", "Anexar arquivos importados", bttBtnsaveevidencias_Jsonclick, 5, "Salvat arquivos importados", "", StyleString, ClassString, bttBtnsaveevidencias_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'SAVEEVIDENCIAS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoEvidenciasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_C02e( true) ;
         }
         else
         {
            wb_table1_3_C02e( false) ;
         }
      }

      protected void wb_table3_27_C02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "", 0, "", "", 1, 10, sStyleString, "none", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "35d9ce8e-0cd2-4075-b379-43e1e9bf02ad", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Anexar arquivo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoEvidenciasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgArtefatos_Internalname, context.GetImagePath( "b2566579-dad1-42f5-8fd2-1897d2dc25a5", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgArtefatos_Visible, 1, "", "Anexar artefatos", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgArtefatos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOARTEFATOS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoEvidenciasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_27_C02e( true) ;
         }
         else
         {
            wb_table3_27_C02e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAC02( ) ;
         WSC02( ) ;
         WEC02( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContagemResultado_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAC02( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadoevidenciaswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAC02( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContagemResultado_Codigo != wcpOAV7ContagemResultado_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContagemResultado_Codigo = AV7ContagemResultado_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContagemResultado_Codigo = cgiGet( sPrefix+"AV7ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContagemResultado_Codigo) > 0 )
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContagemResultado_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAC02( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSC02( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSC02( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEC02( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203242354242");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("contagemresultadoevidenciaswc.js", "?20203242354243");
            context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
            context.AddJavascriptSource("DVelop/Shared/dom.js", "");
            context.AddJavascriptSource("DVelop/Shared/event.js", "");
            context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
            context.AddJavascriptSource("DVelop/Shared/container.js", "");
            context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
            context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_132( )
      {
         edtavCodigo_Internalname = sPrefix+"vCODIGO_"+sGXsfl_13_idx;
         edtavTipodemanda_Internalname = sPrefix+"vTIPODEMANDA_"+sGXsfl_13_idx;
         edtavArtefatos_codigo_Internalname = sPrefix+"vARTEFATOS_CODIGO_"+sGXsfl_13_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_13_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_13_idx;
         edtavPath_Internalname = sPrefix+"vPATH_"+sGXsfl_13_idx;
         edtavNomearquivo_Internalname = sPrefix+"vNOMEARQUIVO_"+sGXsfl_13_idx;
         edtavTipoarquivo_Internalname = sPrefix+"vTIPOARQUIVO_"+sGXsfl_13_idx;
         edtavTipodocumento_Internalname = sPrefix+"vTIPODOCUMENTO_"+sGXsfl_13_idx;
         edtavArtefatos_descricao_Internalname = sPrefix+"vARTEFATOS_DESCRICAO_"+sGXsfl_13_idx;
         edtavData_Internalname = sPrefix+"vDATA_"+sGXsfl_13_idx;
         edtavEntidade_Internalname = sPrefix+"vENTIDADE_"+sGXsfl_13_idx;
      }

      protected void SubsflControlProps_fel_132( )
      {
         edtavCodigo_Internalname = sPrefix+"vCODIGO_"+sGXsfl_13_fel_idx;
         edtavTipodemanda_Internalname = sPrefix+"vTIPODEMANDA_"+sGXsfl_13_fel_idx;
         edtavArtefatos_codigo_Internalname = sPrefix+"vARTEFATOS_CODIGO_"+sGXsfl_13_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_13_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_13_fel_idx;
         edtavPath_Internalname = sPrefix+"vPATH_"+sGXsfl_13_fel_idx;
         edtavNomearquivo_Internalname = sPrefix+"vNOMEARQUIVO_"+sGXsfl_13_fel_idx;
         edtavTipoarquivo_Internalname = sPrefix+"vTIPOARQUIVO_"+sGXsfl_13_fel_idx;
         edtavTipodocumento_Internalname = sPrefix+"vTIPODOCUMENTO_"+sGXsfl_13_fel_idx;
         edtavArtefatos_descricao_Internalname = sPrefix+"vARTEFATOS_DESCRICAO_"+sGXsfl_13_fel_idx;
         edtavData_Internalname = sPrefix+"vDATA_"+sGXsfl_13_fel_idx;
         edtavEntidade_Internalname = sPrefix+"vENTIDADE_"+sGXsfl_13_fel_idx;
      }

      protected void sendrow_132( )
      {
         SubsflControlProps_132( ) ;
         WBC00( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_13_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_13_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCodigo_Enabled!=0)&&(edtavCodigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 14,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCodigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Codigo), 6, 0, ",", "")),((edtavCodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV21Codigo), "ZZZZZ9")),TempTags+((edtavCodigo_Enabled!=0)&&(edtavCodigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCodigo_Enabled!=0)&&(edtavCodigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,14);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavTipodemanda_Enabled!=0)&&(edtavTipodemanda_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTipodemanda_Internalname,StringUtil.RTrim( AV52TipoDemanda),(String)"",TempTags+((edtavTipodemanda_Enabled!=0)&&(edtavTipodemanda_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavTipodemanda_Enabled!=0)&&(edtavTipodemanda_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,15);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTipodemanda_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavTipodemanda_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavArtefatos_codigo_Enabled!=0)&&(edtavArtefatos_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavArtefatos_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64Artefatos_Codigo), 6, 0, ",", "")),((edtavArtefatos_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64Artefatos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV64Artefatos_Codigo), "ZZZZZ9")),TempTags+((edtavArtefatos_codigo_Enabled!=0)&&(edtavArtefatos_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavArtefatos_codigo_Enabled!=0)&&(edtavArtefatos_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,16);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavArtefatos_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavArtefatos_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavDisplay_Enabled!=0)&&(edtavDisplay_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'"+sPrefix+"',false,'',13)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV15Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Display)));
         Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Display)) ? AV73Display_GXI : context.PathToRelativeUrl( AV15Display)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDisplay_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDISPLAY.CLICK."+sGXsfl_13_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV15Display_IsBlob,(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 18,'"+sPrefix+"',false,'',13)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV14Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV74Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14Delete)));
         Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14Delete)) ? AV74Delete_GXI : context.PathToRelativeUrl( AV14Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavDelete_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e20c02_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV14Delete_IsBlob,(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPath_Enabled!=0)&&(edtavPath_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 19,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPath_Internalname,(String)AV60Path,(String)"",TempTags+((edtavPath_Enabled!=0)&&(edtavPath_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPath_Enabled!=0)&&(edtavPath_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,19);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPath_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavPath_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2000,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavNomearquivo_Enabled!=0)&&(edtavNomearquivo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNomearquivo_Internalname,StringUtil.RTrim( AV16NomeArquivo),(String)"",TempTags+((edtavNomearquivo_Enabled!=0)&&(edtavNomearquivo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavNomearquivo_Enabled!=0)&&(edtavNomearquivo_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,20);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtavNomearquivo_Link,(String)edtavNomearquivo_Linktarget,(String)edtavNomearquivo_Tooltiptext,(String)"",(String)edtavNomearquivo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavNomearquivo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)255,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavTipoarquivo_Enabled!=0)&&(edtavTipoarquivo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTipoarquivo_Internalname,StringUtil.RTrim( AV59TipoArquivo),(String)"",TempTags+((edtavTipoarquivo_Enabled!=0)&&(edtavTipoarquivo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavTipoarquivo_Enabled!=0)&&(edtavTipoarquivo_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,21);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTipoarquivo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavTipoarquivo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavTipodocumento_Enabled!=0)&&(edtavTipodocumento_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTipodocumento_Internalname,StringUtil.RTrim( AV19TipoDocumento),StringUtil.RTrim( context.localUtil.Format( AV19TipoDocumento, "@!")),TempTags+((edtavTipodocumento_Enabled!=0)&&(edtavTipodocumento_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavTipodocumento_Enabled!=0)&&(edtavTipodocumento_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTipodocumento_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTipodocumento_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavArtefatos_descricao_Enabled!=0)&&(edtavArtefatos_descricao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavArtefatos_descricao_Internalname,(String)AV63Artefatos_Descricao,StringUtil.RTrim( context.localUtil.Format( AV63Artefatos_Descricao, "@!")),TempTags+((edtavArtefatos_descricao_Enabled!=0)&&(edtavArtefatos_descricao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavArtefatos_descricao_Enabled!=0)&&(edtavArtefatos_descricao_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavArtefatos_descricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavArtefatos_descricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavData_Enabled!=0)&&(edtavData_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 24,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavData_Internalname,context.localUtil.TToC( AV13Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( AV13Data, "99/99/99 99:99"),TempTags+((edtavData_Enabled!=0)&&(edtavData_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavData_Enabled!=0)&&(edtavData_Visible!=0) ? " onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,24);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavData_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavData_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavEntidade_Enabled!=0)&&(edtavEntidade_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 25,'"+sPrefix+"',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEntidade_Internalname,StringUtil.RTrim( AV54Entidade),StringUtil.RTrim( context.localUtil.Format( AV54Entidade, "@!")),TempTags+((edtavEntidade_Enabled!=0)&&(edtavEntidade_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavEntidade_Enabled!=0)&&(edtavEntidade_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,25);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEntidade_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavEntidade_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_13_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_13_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
         /* End function sendrow_132 */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_evidencia_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_EVIDENCIA";
         edtavContagemresultado_evidencia_Internalname = sPrefix+"vCONTAGEMRESULTADO_EVIDENCIA";
         lblLblanexo_Internalname = sPrefix+"LBLANEXO";
         edtavCodigo_Internalname = sPrefix+"vCODIGO";
         edtavTipodemanda_Internalname = sPrefix+"vTIPODEMANDA";
         edtavArtefatos_codigo_Internalname = sPrefix+"vARTEFATOS_CODIGO";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavPath_Internalname = sPrefix+"vPATH";
         edtavNomearquivo_Internalname = sPrefix+"vNOMEARQUIVO";
         edtavTipoarquivo_Internalname = sPrefix+"vTIPOARQUIVO";
         edtavTipodocumento_Internalname = sPrefix+"vTIPODOCUMENTO";
         edtavArtefatos_descricao_Internalname = sPrefix+"vARTEFATOS_DESCRICAO";
         edtavData_Internalname = sPrefix+"vDATA";
         edtavEntidade_Internalname = sPrefix+"vENTIDADE";
         imgInsert_Internalname = sPrefix+"INSERT";
         imgArtefatos_Internalname = sPrefix+"ARTEFATOS";
         tblTable1_Internalname = sPrefix+"TABLE1";
         bttBtn_importarredmine_Internalname = sPrefix+"BTN_IMPORTARREDMINE";
         bttBtnsaveevidencias_Internalname = sPrefix+"BTNSAVEEVIDENCIAS";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Dvelop_confirmpanel_apagar_Internalname = sPrefix+"DVELOP_CONFIRMPANEL_APAGAR";
         tblTable2_Internalname = sPrefix+"TABLE2";
         Innewwindow_Internalname = sPrefix+"INNEWWINDOW";
         lblTbjava_Internalname = sPrefix+"TBJAVA";
         Form.Internalname = sPrefix+"FORM";
         subGrid1_Internalname = sPrefix+"GRID1";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavEntidade_Jsonclick = "";
         edtavEntidade_Visible = -1;
         edtavData_Jsonclick = "";
         edtavData_Visible = -1;
         edtavArtefatos_descricao_Jsonclick = "";
         edtavArtefatos_descricao_Visible = -1;
         edtavTipodocumento_Jsonclick = "";
         edtavTipodocumento_Visible = -1;
         edtavTipoarquivo_Jsonclick = "";
         edtavTipoarquivo_Visible = 0;
         edtavNomearquivo_Jsonclick = "";
         edtavNomearquivo_Visible = -1;
         edtavPath_Jsonclick = "";
         edtavPath_Visible = 0;
         edtavDelete_Jsonclick = "";
         edtavDisplay_Jsonclick = "";
         edtavDisplay_Enabled = 1;
         edtavArtefatos_codigo_Jsonclick = "";
         edtavArtefatos_codigo_Visible = 0;
         edtavTipodemanda_Jsonclick = "";
         edtavTipodemanda_Visible = 0;
         edtavCodigo_Jsonclick = "";
         edtavCodigo_Visible = 0;
         imgArtefatos_Visible = 1;
         bttBtnsaveevidencias_Visible = 1;
         bttBtn_importarredmine_Visible = 1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavEntidade_Enabled = 1;
         edtavData_Enabled = 1;
         edtavArtefatos_descricao_Enabled = 1;
         edtavTipodocumento_Enabled = 1;
         edtavTipoarquivo_Enabled = 1;
         edtavNomearquivo_Tooltiptext = "";
         edtavNomearquivo_Linktarget = "";
         edtavNomearquivo_Link = "";
         edtavNomearquivo_Enabled = 1;
         edtavPath_Enabled = 1;
         edtavDelete_Tooltiptext = "";
         edtavDelete_Enabled = 1;
         edtavDisplay_Tooltiptext = "";
         edtavDisplay_Linktarget = "";
         edtavArtefatos_codigo_Enabled = 1;
         edtavTipodemanda_Enabled = 1;
         edtavCodigo_Enabled = 1;
         edtavDelete_Visible = -1;
         subGrid1_Class = "WorkWith";
         edtavContagemresultado_evidencia_Enabled = 1;
         bttBtn_importarredmine_Caption = "Importar anexos do Redmine";
         edtavDisplay_Visible = -1;
         subGrid1_Backcolorstyle = 3;
         lblTbjava_Caption = "Java";
         lblTbjava_Visible = 1;
         Innewwindow_Target = "";
         Dvelop_confirmpanel_apagar_Buttonnotext = "N�o";
         Dvelop_confirmpanel_apagar_Buttonyestext = "Sim";
         Dvelop_confirmpanel_apagar_Confirmtext = "Confirma apagar o arquivo/linnk anexado?";
         Dvelop_confirmpanel_apagar_Icon = "2";
         Dvelop_confirmpanel_apagar_Title = "Aten��o";
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A646TipoDocumento_Nome',fld:'TIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A591ContagemResultadoEvidencia_Data',fld:'CONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1494ContagemResultadoEvidencia_Entidade',fld:'CONTAGEMRESULTADOEVIDENCIA_ENTIDADE',pic:'@!',nv:''},{av:'A1770ContagemResultadoArtefato_EvdCod',fld:'CONTAGEMRESULTADOARTEFATO_EVDCOD',pic:'ZZZZZ9',nv:0},{av:'A1771ContagemResultadoArtefato_ArtefatoCod',fld:'CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'AV17SDT_AnexosDemanda',fld:'vSDT_ANEXOSDEMANDA',pic:'',nv:null},{av:'A1109AnexoDe_Id',fld:'ANEXODE_ID',pic:'ZZZZZ9',nv:0},{av:'A1110AnexoDe_Tabela',fld:'ANEXODE_TABELA',pic:'ZZZZZ9',nv:0},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'A1123Anexo_Descricao',fld:'ANEXO_DESCRICAO',pic:'@!',nv:''},{av:'A1103Anexo_Data',fld:'ANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1495Anexo_Owner',fld:'ANEXO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1497Anexo_Entidade',fld:'ANEXO_ENTIDADE',pic:'@!',nv:''},{av:'A1773ContagemResultadoArtefato_AnxCod',fld:'CONTAGEMRESULTADOARTEFATO_ANXCOD',pic:'ZZZZZ9',nv:0},{av:'A1450Anexo_Link',fld:'ANEXO_LINK',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A592ContagemResultado_Evidencia',fld:'CONTAGEMRESULTADO_EVIDENCIA',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1389ContagemResultado_RdmnIssueId',fld:'CONTAGEMRESULTADO_RDMNISSUEID',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{ctrl:'BTNSAVEEVIDENCIAS',prop:'Visible'},{ctrl:'BTN_IMPORTARREDMINE',prop:'Visible'},{av:'AV17SDT_AnexosDemanda',fld:'vSDT_ANEXOSDEMANDA',pic:'',nv:null},{av:'AV12ContagemResultado_Evidencia',fld:'vCONTAGEMRESULTADO_EVIDENCIA',pic:'',hsh:true,nv:''},{av:'AV30AreaTrabalho',fld:'vAREATRABALHO',pic:'ZZZZZ9',nv:0},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'}]}");
         setEventMetadata("GRID1.LOAD","{handler:'E18C02',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A646TipoDocumento_Nome',fld:'TIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A591ContagemResultadoEvidencia_Data',fld:'CONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1494ContagemResultadoEvidencia_Entidade',fld:'CONTAGEMRESULTADOEVIDENCIA_ENTIDADE',pic:'@!',nv:''},{av:'A1770ContagemResultadoArtefato_EvdCod',fld:'CONTAGEMRESULTADOARTEFATO_EVDCOD',pic:'ZZZZZ9',nv:0},{av:'A1771ContagemResultadoArtefato_ArtefatoCod',fld:'CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'AV17SDT_AnexosDemanda',fld:'vSDT_ANEXOSDEMANDA',pic:'',nv:null},{av:'A1109AnexoDe_Id',fld:'ANEXODE_ID',pic:'ZZZZZ9',nv:0},{av:'A1110AnexoDe_Tabela',fld:'ANEXODE_TABELA',pic:'ZZZZZ9',nv:0},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'A1123Anexo_Descricao',fld:'ANEXO_DESCRICAO',pic:'@!',nv:''},{av:'A1103Anexo_Data',fld:'ANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1495Anexo_Owner',fld:'ANEXO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1497Anexo_Entidade',fld:'ANEXO_ENTIDADE',pic:'@!',nv:''},{av:'A1773ContagemResultadoArtefato_AnxCod',fld:'CONTAGEMRESULTADOARTEFATO_ANXCOD',pic:'ZZZZZ9',nv:0},{av:'A1450Anexo_Link',fld:'ANEXO_LINK',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV17SDT_AnexosDemanda',fld:'vSDT_ANEXOSDEMANDA',pic:'',nv:null},{av:'AV15Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Linktarget',ctrl:'vDISPLAY',prop:'Linktarget'},{av:'AV14Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'AV21Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Data',fld:'vDATA',pic:'99/99/99 99:99',nv:''},{av:'AV52TipoDemanda',fld:'vTIPODEMANDA',pic:'',nv:''},{av:'AV16NomeArquivo',fld:'vNOMEARQUIVO',pic:'',nv:''},{av:'AV59TipoArquivo',fld:'vTIPOARQUIVO',pic:'',nv:''},{av:'AV19TipoDocumento',fld:'vTIPODOCUMENTO',pic:'@!',nv:''},{av:'AV54Entidade',fld:'vENTIDADE',pic:'@!',nv:''},{av:'edtavNomearquivo_Linktarget',ctrl:'vNOMEARQUIVO',prop:'Linktarget'},{av:'edtavNomearquivo_Link',ctrl:'vNOMEARQUIVO',prop:'Link'},{av:'edtavNomearquivo_Tooltiptext',ctrl:'vNOMEARQUIVO',prop:'Tooltiptext'},{av:'AV64Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV63Artefatos_Descricao',fld:'vARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV60Path',fld:'vPATH',pic:'',nv:''},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'}]}");
         setEventMetadata("VDISPLAY.CLICK","{handler:'E19C02',iparms:[{av:'AV52TipoDemanda',fld:'vTIPODEMANDA',pic:'',nv:''},{av:'AV16NomeArquivo',fld:'vNOMEARQUIVO',pic:'',nv:''},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1101Anexo_Arquivo',fld:'ANEXO_ARQUIVO',pic:'',nv:''},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'AV60Path',fld:'vPATH',pic:'',nv:''}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E12C02',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A592ContagemResultado_Evidencia',fld:'CONTAGEMRESULTADO_EVIDENCIA',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1389ContagemResultado_RdmnIssueId',fld:'CONTAGEMRESULTADO_RDMNISSUEID',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A646TipoDocumento_Nome',fld:'TIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A591ContagemResultadoEvidencia_Data',fld:'CONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1494ContagemResultadoEvidencia_Entidade',fld:'CONTAGEMRESULTADOEVIDENCIA_ENTIDADE',pic:'@!',nv:''},{av:'A1770ContagemResultadoArtefato_EvdCod',fld:'CONTAGEMRESULTADOARTEFATO_EVDCOD',pic:'ZZZZZ9',nv:0},{av:'A1771ContagemResultadoArtefato_ArtefatoCod',fld:'CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'AV17SDT_AnexosDemanda',fld:'vSDT_ANEXOSDEMANDA',pic:'',nv:null},{av:'A1109AnexoDe_Id',fld:'ANEXODE_ID',pic:'ZZZZZ9',nv:0},{av:'A1110AnexoDe_Tabela',fld:'ANEXODE_TABELA',pic:'ZZZZZ9',nv:0},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'A1123Anexo_Descricao',fld:'ANEXO_DESCRICAO',pic:'@!',nv:''},{av:'A1103Anexo_Data',fld:'ANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1495Anexo_Owner',fld:'ANEXO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1497Anexo_Entidade',fld:'ANEXO_ENTIDADE',pic:'@!',nv:''},{av:'A1773ContagemResultadoArtefato_AnxCod',fld:'CONTAGEMRESULTADOARTEFATO_ANXCOD',pic:'ZZZZZ9',nv:0},{av:'A1450Anexo_Link',fld:'ANEXO_LINK',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VDELETE.CLICK","{handler:'E20C02',iparms:[],oparms:[]}");
         setEventMetadata("DVELOP_CONFIRMPANEL_APAGAR.CLOSE","{handler:'E11C02',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A592ContagemResultado_Evidencia',fld:'CONTAGEMRESULTADO_EVIDENCIA',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1389ContagemResultado_RdmnIssueId',fld:'CONTAGEMRESULTADO_RDMNISSUEID',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A646TipoDocumento_Nome',fld:'TIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A591ContagemResultadoEvidencia_Data',fld:'CONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1494ContagemResultadoEvidencia_Entidade',fld:'CONTAGEMRESULTADOEVIDENCIA_ENTIDADE',pic:'@!',nv:''},{av:'A1770ContagemResultadoArtefato_EvdCod',fld:'CONTAGEMRESULTADOARTEFATO_EVDCOD',pic:'ZZZZZ9',nv:0},{av:'A1771ContagemResultadoArtefato_ArtefatoCod',fld:'CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'AV17SDT_AnexosDemanda',fld:'vSDT_ANEXOSDEMANDA',pic:'',nv:null},{av:'A1109AnexoDe_Id',fld:'ANEXODE_ID',pic:'ZZZZZ9',nv:0},{av:'A1110AnexoDe_Tabela',fld:'ANEXODE_TABELA',pic:'ZZZZZ9',nv:0},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'A1123Anexo_Descricao',fld:'ANEXO_DESCRICAO',pic:'@!',nv:''},{av:'A1103Anexo_Data',fld:'ANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1495Anexo_Owner',fld:'ANEXO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1497Anexo_Entidade',fld:'ANEXO_ENTIDADE',pic:'@!',nv:''},{av:'A1773ContagemResultadoArtefato_AnxCod',fld:'CONTAGEMRESULTADOARTEFATO_ANXCOD',pic:'ZZZZZ9',nv:0},{av:'A1450Anexo_Link',fld:'ANEXO_LINK',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Dvelop_confirmpanel_apagar_Result',ctrl:'DVELOP_CONFIRMPANEL_APAGAR',prop:'Result'},{av:'AV64Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV64Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'FILEDOWNLOAD'","{handler:'E13C02',iparms:[{av:'AV35SDT_Issue',fld:'vSDT_ISSUE',pic:'',nv:null},{av:'AV41Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV47TipoDocumento_Codigo',fld:'vTIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}],oparms:[{av:'AV36Attachment',fld:'vATTACHMENT',pic:'',nv:null},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV38FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV39Ext',fld:'vEXT',pic:'',nv:''},{av:'AV41Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV42Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}]}");
         setEventMetadata("'SAVEEVIDENCIAS'","{handler:'E14C02',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A592ContagemResultado_Evidencia',fld:'CONTAGEMRESULTADO_EVIDENCIA',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1389ContagemResultado_RdmnIssueId',fld:'CONTAGEMRESULTADO_RDMNISSUEID',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A646TipoDocumento_Nome',fld:'TIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A591ContagemResultadoEvidencia_Data',fld:'CONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1494ContagemResultadoEvidencia_Entidade',fld:'CONTAGEMRESULTADOEVIDENCIA_ENTIDADE',pic:'@!',nv:''},{av:'A1770ContagemResultadoArtefato_EvdCod',fld:'CONTAGEMRESULTADOARTEFATO_EVDCOD',pic:'ZZZZZ9',nv:0},{av:'A1771ContagemResultadoArtefato_ArtefatoCod',fld:'CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'AV17SDT_AnexosDemanda',fld:'vSDT_ANEXOSDEMANDA',pic:'',nv:null},{av:'A1109AnexoDe_Id',fld:'ANEXODE_ID',pic:'ZZZZZ9',nv:0},{av:'A1110AnexoDe_Tabela',fld:'ANEXODE_TABELA',pic:'ZZZZZ9',nv:0},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'A1123Anexo_Descricao',fld:'ANEXO_DESCRICAO',pic:'@!',nv:''},{av:'A1103Anexo_Data',fld:'ANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1495Anexo_Owner',fld:'ANEXO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1497Anexo_Entidade',fld:'ANEXO_ENTIDADE',pic:'@!',nv:''},{av:'A1773ContagemResultadoArtefato_AnxCod',fld:'CONTAGEMRESULTADOARTEFATO_ANXCOD',pic:'ZZZZZ9',nv:0},{av:'A1450Anexo_Link',fld:'ANEXO_LINK',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOARTEFATOS'","{handler:'E15C02',iparms:[{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Dvelop_confirmpanel_apagar_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A592ContagemResultado_Evidencia = "";
         A484ContagemResultado_StatusDmn = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A646TipoDocumento_Nome = "";
         A587ContagemResultadoEvidencia_Descricao = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         A1494ContagemResultadoEvidencia_Entidade = "";
         A1751Artefatos_Descricao = "";
         A1449ContagemResultadoEvidencia_Link = "";
         AV17SDT_AnexosDemanda = new GxObjectCollection( context, "SDT_AnexosDemanda.SDT_AnexosDemandaItem", "GxEv3Up14_Meetrika", "SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem", "GeneXus.Programs");
         A1107Anexo_NomeArq = "";
         A1108Anexo_TipoArq = "";
         A1123Anexo_Descricao = "";
         A1103Anexo_Data = (DateTime)(DateTime.MinValue);
         A1497Anexo_Entidade = "";
         A1450Anexo_Link = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         A1101Anexo_Arquivo = "";
         AV35SDT_Issue = new SdtSDT_Redmineissue(context);
         AV41Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV42Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV12ContagemResultado_Evidencia = "";
         GX_FocusControl = "";
         lblTbjava_Jsonclick = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV52TipoDemanda = "";
         AV15Display = "";
         AV73Display_GXI = "";
         AV14Delete = "";
         AV74Delete_GXI = "";
         AV60Path = "";
         AV16NomeArquivo = "";
         AV59TipoArquivo = "";
         AV19TipoDocumento = "";
         AV63Artefatos_Descricao = "";
         AV13Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV54Entidade = "";
         Grid1Container = new GXWebGrid( context);
         scmdbuf = "";
         H00C02_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00C02_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00C02_A456ContagemResultado_Codigo = new int[1] ;
         H00C02_A592ContagemResultado_Evidencia = new String[] {""} ;
         H00C02_n592ContagemResultado_Evidencia = new bool[] {false} ;
         H00C02_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00C02_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00C02_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         H00C02_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         H00C02_A890ContagemResultado_Responsavel = new int[1] ;
         H00C02_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00C02_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00C02_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         AV62StatusDmn = "";
         AV56Directory = new GxDirectory(context.GetPhysicalPath());
         H00C03_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00C03_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00C03_A645TipoDocumento_Codigo = new int[1] ;
         H00C03_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00C03_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00C03_A456ContagemResultado_Codigo = new int[1] ;
         H00C03_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00C03_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00C03_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00C03_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00C03_A646TipoDocumento_Nome = new String[] {""} ;
         H00C03_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         H00C03_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         H00C03_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00C03_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         H00C03_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         H00C03_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         H00C03_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00C03_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00C03_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         H00C03_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         A588ContagemResultadoEvidencia_Arquivo_Filetype = "";
         A588ContagemResultadoEvidencia_Arquivo_Filename = "";
         AV18SDT_AnexosDemandaItem = new SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem(context);
         H00C04_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         H00C04_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         H00C04_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         H00C04_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         H00C04_A1751Artefatos_Descricao = new String[] {""} ;
         H00C04_n1751Artefatos_Descricao = new bool[] {false} ;
         H00C09_A645TipoDocumento_Codigo = new int[1] ;
         H00C09_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00C09_A1106Anexo_Codigo = new int[1] ;
         H00C09_A1110AnexoDe_Tabela = new int[1] ;
         H00C09_A1109AnexoDe_Id = new int[1] ;
         H00C09_A1108Anexo_TipoArq = new String[] {""} ;
         H00C09_n1108Anexo_TipoArq = new bool[] {false} ;
         H00C09_A1107Anexo_NomeArq = new String[] {""} ;
         H00C09_n1107Anexo_NomeArq = new bool[] {false} ;
         H00C09_A1123Anexo_Descricao = new String[] {""} ;
         H00C09_n1123Anexo_Descricao = new bool[] {false} ;
         H00C09_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00C09_A646TipoDocumento_Nome = new String[] {""} ;
         H00C09_A1450Anexo_Link = new String[] {""} ;
         H00C09_n1450Anexo_Link = new bool[] {false} ;
         H00C09_A1496Anexo_AreaTrabalhoCod = new int[1] ;
         H00C09_n1496Anexo_AreaTrabalhoCod = new bool[] {false} ;
         H00C09_A1495Anexo_Owner = new int[1] ;
         H00C09_n1495Anexo_Owner = new bool[] {false} ;
         A1101Anexo_Arquivo_Filetype = "";
         A1101Anexo_Arquivo_Filename = "";
         GXt_char1 = "";
         H00C010_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         H00C010_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         H00C010_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         H00C010_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         H00C010_A1751Artefatos_Descricao = new String[] {""} ;
         H00C010_n1751Artefatos_Descricao = new bool[] {false} ;
         AV53Html = "";
         Grid1Row = new GXWebRow();
         H00C011_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00C011_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00C011_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00C011_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00C011_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00C011_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         H00C011_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         H00C012_A1106Anexo_Codigo = new int[1] ;
         H00C012_A1107Anexo_NomeArq = new String[] {""} ;
         H00C012_n1107Anexo_NomeArq = new bool[] {false} ;
         H00C012_A1108Anexo_TipoArq = new String[] {""} ;
         H00C012_n1108Anexo_TipoArq = new bool[] {false} ;
         H00C012_A1101Anexo_Arquivo = new String[] {""} ;
         H00C012_n1101Anexo_Arquivo = new bool[] {false} ;
         AV20ContagemResultadoEvidencia = new SdtContagemResultadoEvidencia(context);
         AV36Attachment = new SdtSDT_Redmineissue_attachments_attachment(context);
         H00C013_A29Contratante_Codigo = new int[1] ;
         H00C013_n29Contratante_Codigo = new bool[] {false} ;
         H00C013_A5AreaTrabalho_Codigo = new int[1] ;
         H00C014_A1380Redmine_Codigo = new int[1] ;
         H00C014_A1384Redmine_ContratanteCod = new int[1] ;
         H00C014_A1381Redmine_Host = new String[] {""} ;
         H00C014_A1382Redmine_Url = new String[] {""} ;
         H00C014_A1383Redmine_Key = new String[] {""} ;
         A1381Redmine_Host = "";
         A1382Redmine_Url = "";
         A1383Redmine_Key = "";
         AV32Host = "";
         AV33Url = "";
         AV34Key = "";
         AV29httpclient = new GxHttpClient( context);
         AV44Execute = "";
         AV38FileName = "";
         AV39Ext = "";
         H00C015_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00C015_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         H00C015_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         H00C015_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00C015_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00C015_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00C015_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00C015_A456ContagemResultado_Codigo = new int[1] ;
         A1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         AV45FileUrl = "";
         AV46FileContentType = "";
         AV40WebSession = context.GetSession();
         AV82GXV4 = new GxFileCollection();
         AV58File = new GxFile(context.GetPhysicalPath());
         H00C016_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         H00C016_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         sStyleString = "";
         lblTextblockcontagemresultado_evidencia_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         lblLblanexo_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         bttBtn_importarredmine_Jsonclick = "";
         bttBtnsaveevidencias_Jsonclick = "";
         imgInsert_Jsonclick = "";
         imgArtefatos_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContagemResultado_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoevidenciaswc__default(),
            new Object[][] {
                new Object[] {
               H00C02_A490ContagemResultado_ContratadaCod, H00C02_n490ContagemResultado_ContratadaCod, H00C02_A456ContagemResultado_Codigo, H00C02_A592ContagemResultado_Evidencia, H00C02_n592ContagemResultado_Evidencia, H00C02_A52Contratada_AreaTrabalhoCod, H00C02_n52Contratada_AreaTrabalhoCod, H00C02_A1389ContagemResultado_RdmnIssueId, H00C02_n1389ContagemResultado_RdmnIssueId, H00C02_A890ContagemResultado_Responsavel,
               H00C02_n890ContagemResultado_Responsavel, H00C02_A484ContagemResultado_StatusDmn, H00C02_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               H00C03_A490ContagemResultado_ContratadaCod, H00C03_n490ContagemResultado_ContratadaCod, H00C03_A645TipoDocumento_Codigo, H00C03_n645TipoDocumento_Codigo, H00C03_A586ContagemResultadoEvidencia_Codigo, H00C03_A456ContagemResultado_Codigo, H00C03_A590ContagemResultadoEvidencia_TipoArq, H00C03_n590ContagemResultadoEvidencia_TipoArq, H00C03_A589ContagemResultadoEvidencia_NomeArq, H00C03_n589ContagemResultadoEvidencia_NomeArq,
               H00C03_A646TipoDocumento_Nome, H00C03_A587ContagemResultadoEvidencia_Descricao, H00C03_n587ContagemResultadoEvidencia_Descricao, H00C03_A591ContagemResultadoEvidencia_Data, H00C03_n591ContagemResultadoEvidencia_Data, H00C03_A1449ContagemResultadoEvidencia_Link, H00C03_n1449ContagemResultadoEvidencia_Link, H00C03_A52Contratada_AreaTrabalhoCod, H00C03_n52Contratada_AreaTrabalhoCod, H00C03_A1493ContagemResultadoEvidencia_Owner,
               H00C03_n1493ContagemResultadoEvidencia_Owner
               }
               , new Object[] {
               H00C04_A1769ContagemResultadoArtefato_Codigo, H00C04_A1770ContagemResultadoArtefato_EvdCod, H00C04_n1770ContagemResultadoArtefato_EvdCod, H00C04_A1771ContagemResultadoArtefato_ArtefatoCod, H00C04_A1751Artefatos_Descricao, H00C04_n1751Artefatos_Descricao
               }
               , new Object[] {
               H00C09_A645TipoDocumento_Codigo, H00C09_n645TipoDocumento_Codigo, H00C09_A1106Anexo_Codigo, H00C09_A1110AnexoDe_Tabela, H00C09_A1109AnexoDe_Id, H00C09_A1108Anexo_TipoArq, H00C09_n1108Anexo_TipoArq, H00C09_A1107Anexo_NomeArq, H00C09_n1107Anexo_NomeArq, H00C09_A1123Anexo_Descricao,
               H00C09_n1123Anexo_Descricao, H00C09_A1103Anexo_Data, H00C09_A646TipoDocumento_Nome, H00C09_A1450Anexo_Link, H00C09_n1450Anexo_Link, H00C09_A1496Anexo_AreaTrabalhoCod, H00C09_n1496Anexo_AreaTrabalhoCod, H00C09_A1495Anexo_Owner, H00C09_n1495Anexo_Owner
               }
               , new Object[] {
               H00C010_A1769ContagemResultadoArtefato_Codigo, H00C010_A1773ContagemResultadoArtefato_AnxCod, H00C010_n1773ContagemResultadoArtefato_AnxCod, H00C010_A1771ContagemResultadoArtefato_ArtefatoCod, H00C010_A1751Artefatos_Descricao, H00C010_n1751Artefatos_Descricao
               }
               , new Object[] {
               H00C011_A586ContagemResultadoEvidencia_Codigo, H00C011_A589ContagemResultadoEvidencia_NomeArq, H00C011_n589ContagemResultadoEvidencia_NomeArq, H00C011_A590ContagemResultadoEvidencia_TipoArq, H00C011_n590ContagemResultadoEvidencia_TipoArq, H00C011_A588ContagemResultadoEvidencia_Arquivo, H00C011_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               H00C012_A1106Anexo_Codigo, H00C012_A1107Anexo_NomeArq, H00C012_n1107Anexo_NomeArq, H00C012_A1108Anexo_TipoArq, H00C012_n1108Anexo_TipoArq, H00C012_A1101Anexo_Arquivo, H00C012_n1101Anexo_Arquivo
               }
               , new Object[] {
               H00C013_A29Contratante_Codigo, H00C013_n29Contratante_Codigo, H00C013_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00C014_A1380Redmine_Codigo, H00C014_A1384Redmine_ContratanteCod, H00C014_A1381Redmine_Host, H00C014_A1382Redmine_Url, H00C014_A1383Redmine_Key
               }
               , new Object[] {
               H00C015_A586ContagemResultadoEvidencia_Codigo, H00C015_A1393ContagemResultadoEvidencia_RdmnCreated, H00C015_n1393ContagemResultadoEvidencia_RdmnCreated, H00C015_A590ContagemResultadoEvidencia_TipoArq, H00C015_n590ContagemResultadoEvidencia_TipoArq, H00C015_A589ContagemResultadoEvidencia_NomeArq, H00C015_n589ContagemResultadoEvidencia_NomeArq, H00C015_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00C016_A1769ContagemResultadoArtefato_Codigo, H00C016_A1772ContagemResultadoArtefato_OSCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_evidencia_Enabled = 0;
         edtavCodigo_Enabled = 0;
         edtavTipodemanda_Enabled = 0;
         edtavArtefatos_codigo_Enabled = 0;
         edtavPath_Enabled = 0;
         edtavNomearquivo_Enabled = 0;
         edtavTipoarquivo_Enabled = 0;
         edtavTipodocumento_Enabled = 0;
         edtavArtefatos_descricao_Enabled = 0;
         edtavData_Enabled = 0;
         edtavEntidade_Enabled = 0;
      }

      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_13 ;
      private short nGXsfl_13_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_13_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short AV51i ;
      private short GRID1_nEOF ;
      private short AV84GXLvl354 ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short subGrid1_Backstyle ;
      private int AV7ContagemResultado_Codigo ;
      private int wcpOAV7ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int A890ContagemResultado_Responsavel ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A1493ContagemResultadoEvidencia_Owner ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A1109AnexoDe_Id ;
      private int A1110AnexoDe_Tabela ;
      private int A1106Anexo_Codigo ;
      private int A1495Anexo_Owner ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private int edtavContagemresultado_evidencia_Enabled ;
      private int edtavCodigo_Enabled ;
      private int edtavTipodemanda_Enabled ;
      private int edtavArtefatos_codigo_Enabled ;
      private int edtavPath_Enabled ;
      private int edtavNomearquivo_Enabled ;
      private int edtavTipoarquivo_Enabled ;
      private int edtavTipodocumento_Enabled ;
      private int edtavArtefatos_descricao_Enabled ;
      private int edtavData_Enabled ;
      private int edtavEntidade_Enabled ;
      private int AV47TipoDocumento_Codigo ;
      private int A1496Anexo_AreaTrabalhoCod ;
      private int lblTbjava_Visible ;
      private int AV21Codigo ;
      private int AV64Artefatos_Codigo ;
      private int subGrid1_Islastpage ;
      private int bttBtnsaveevidencias_Visible ;
      private int bttBtn_importarredmine_Visible ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV30AreaTrabalho ;
      private int AV31RdmnIssueId ;
      private int AV61Responsavel ;
      private int edtavDisplay_Visible ;
      private int A645TipoDocumento_Codigo ;
      private int AV72GXV1 ;
      private int edtavDelete_Enabled ;
      private int edtavDelete_Visible ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A1384Redmine_ContratanteCod ;
      private int AV79GXV2 ;
      private int AV81GXV3 ;
      private int AV83GXV5 ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int imgArtefatos_Visible ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int edtavCodigo_Visible ;
      private int edtavTipodemanda_Visible ;
      private int edtavArtefatos_codigo_Visible ;
      private int edtavDisplay_Enabled ;
      private int edtavPath_Visible ;
      private int edtavNomearquivo_Visible ;
      private int edtavTipoarquivo_Visible ;
      private int edtavTipodocumento_Visible ;
      private int edtavArtefatos_descricao_Visible ;
      private int edtavData_Visible ;
      private int edtavEntidade_Visible ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private String Dvelop_confirmpanel_apagar_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_13_idx="0001" ;
      private String A484ContagemResultado_StatusDmn ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String A646TipoDocumento_Nome ;
      private String A1494ContagemResultadoEvidencia_Entidade ;
      private String A1107Anexo_NomeArq ;
      private String A1108Anexo_TipoArq ;
      private String A1497Anexo_Entidade ;
      private String GXKey ;
      private String edtavContagemresultado_evidencia_Internalname ;
      private String edtavCodigo_Internalname ;
      private String edtavTipodemanda_Internalname ;
      private String edtavArtefatos_codigo_Internalname ;
      private String edtavPath_Internalname ;
      private String edtavNomearquivo_Internalname ;
      private String edtavTipoarquivo_Internalname ;
      private String edtavTipodocumento_Internalname ;
      private String edtavArtefatos_descricao_Internalname ;
      private String edtavData_Internalname ;
      private String edtavEntidade_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvelop_confirmpanel_apagar_Title ;
      private String Dvelop_confirmpanel_apagar_Icon ;
      private String Dvelop_confirmpanel_apagar_Confirmtext ;
      private String Dvelop_confirmpanel_apagar_Buttonyestext ;
      private String Dvelop_confirmpanel_apagar_Buttonnotext ;
      private String Innewwindow_Target ;
      private String GX_FocusControl ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV52TipoDemanda ;
      private String edtavDisplay_Internalname ;
      private String edtavDelete_Internalname ;
      private String AV16NomeArquivo ;
      private String AV59TipoArquivo ;
      private String AV19TipoDocumento ;
      private String AV54Entidade ;
      private String bttBtnsaveevidencias_Internalname ;
      private String bttBtn_importarredmine_Internalname ;
      private String scmdbuf ;
      private String AV62StatusDmn ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filetype ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filename ;
      private String A1101Anexo_Arquivo_Filetype ;
      private String A1101Anexo_Arquivo_Filename ;
      private String GXt_char1 ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Linktarget ;
      private String edtavDelete_Tooltiptext ;
      private String edtavNomearquivo_Linktarget ;
      private String edtavNomearquivo_Link ;
      private String edtavNomearquivo_Tooltiptext ;
      private String AV53Html ;
      private String Innewwindow_Internalname ;
      private String A1381Redmine_Host ;
      private String A1382Redmine_Url ;
      private String A1383Redmine_Key ;
      private String AV32Host ;
      private String AV34Key ;
      private String AV44Execute ;
      private String AV38FileName ;
      private String AV39Ext ;
      private String bttBtn_importarredmine_Caption ;
      private String AV45FileUrl ;
      private String AV46FileContentType ;
      private String imgArtefatos_Internalname ;
      private String sStyleString ;
      private String tblTable2_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontagemresultado_evidencia_Internalname ;
      private String lblTextblockcontagemresultado_evidencia_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String lblLblanexo_Internalname ;
      private String lblLblanexo_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String bttBtn_importarredmine_Jsonclick ;
      private String bttBtnsaveevidencias_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String imgArtefatos_Jsonclick ;
      private String sCtrlAV7ContagemResultado_Codigo ;
      private String sGXsfl_13_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavCodigo_Jsonclick ;
      private String edtavTipodemanda_Jsonclick ;
      private String edtavArtefatos_codigo_Jsonclick ;
      private String edtavDisplay_Jsonclick ;
      private String edtavDelete_Jsonclick ;
      private String edtavPath_Jsonclick ;
      private String edtavNomearquivo_Jsonclick ;
      private String edtavTipoarquivo_Jsonclick ;
      private String edtavTipodocumento_Jsonclick ;
      private String edtavArtefatos_descricao_Jsonclick ;
      private String edtavData_Jsonclick ;
      private String edtavEntidade_Jsonclick ;
      private String Dvelop_confirmpanel_apagar_Internalname ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private DateTime A1103Anexo_Data ;
      private DateTime AV13Data ;
      private DateTime A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool entryPointCalled ;
      private bool n592ContagemResultado_Evidencia ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n587ContagemResultadoEvidencia_Descricao ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n1493ContagemResultadoEvidencia_Owner ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private bool n1751Artefatos_Descricao ;
      private bool n1449ContagemResultadoEvidencia_Link ;
      private bool n1107Anexo_NomeArq ;
      private bool n1108Anexo_TipoArq ;
      private bool n1123Anexo_Descricao ;
      private bool n1495Anexo_Owner ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private bool n1450Anexo_Link ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n645TipoDocumento_Codigo ;
      private bool n1496Anexo_AreaTrabalhoCod ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool n1101Anexo_Arquivo ;
      private bool n29Contratante_Codigo ;
      private bool n1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool AV15Display_IsBlob ;
      private bool AV14Delete_IsBlob ;
      private String A592ContagemResultado_Evidencia ;
      private String A587ContagemResultadoEvidencia_Descricao ;
      private String A1449ContagemResultadoEvidencia_Link ;
      private String A1450Anexo_Link ;
      private String AV12ContagemResultado_Evidencia ;
      private String AV33Url ;
      private String A1751Artefatos_Descricao ;
      private String A1123Anexo_Descricao ;
      private String AV73Display_GXI ;
      private String AV74Delete_GXI ;
      private String AV60Path ;
      private String AV63Artefatos_Descricao ;
      private String AV15Display ;
      private String AV14Delete ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private String A1101Anexo_Arquivo ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00C02_A490ContagemResultado_ContratadaCod ;
      private bool[] H00C02_n490ContagemResultado_ContratadaCod ;
      private int[] H00C02_A456ContagemResultado_Codigo ;
      private String[] H00C02_A592ContagemResultado_Evidencia ;
      private bool[] H00C02_n592ContagemResultado_Evidencia ;
      private int[] H00C02_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00C02_n52Contratada_AreaTrabalhoCod ;
      private int[] H00C02_A1389ContagemResultado_RdmnIssueId ;
      private bool[] H00C02_n1389ContagemResultado_RdmnIssueId ;
      private int[] H00C02_A890ContagemResultado_Responsavel ;
      private bool[] H00C02_n890ContagemResultado_Responsavel ;
      private String[] H00C02_A484ContagemResultado_StatusDmn ;
      private bool[] H00C02_n484ContagemResultado_StatusDmn ;
      private int[] H00C03_A490ContagemResultado_ContratadaCod ;
      private bool[] H00C03_n490ContagemResultado_ContratadaCod ;
      private int[] H00C03_A645TipoDocumento_Codigo ;
      private bool[] H00C03_n645TipoDocumento_Codigo ;
      private int[] H00C03_A586ContagemResultadoEvidencia_Codigo ;
      private int[] H00C03_A456ContagemResultado_Codigo ;
      private String[] H00C03_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00C03_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] H00C03_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00C03_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] H00C03_A646TipoDocumento_Nome ;
      private String[] H00C03_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] H00C03_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] H00C03_A591ContagemResultadoEvidencia_Data ;
      private bool[] H00C03_n591ContagemResultadoEvidencia_Data ;
      private String[] H00C03_A1449ContagemResultadoEvidencia_Link ;
      private bool[] H00C03_n1449ContagemResultadoEvidencia_Link ;
      private int[] H00C03_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00C03_n52Contratada_AreaTrabalhoCod ;
      private int[] H00C03_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] H00C03_n1493ContagemResultadoEvidencia_Owner ;
      private int[] H00C04_A1769ContagemResultadoArtefato_Codigo ;
      private int[] H00C04_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] H00C04_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] H00C04_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private String[] H00C04_A1751Artefatos_Descricao ;
      private bool[] H00C04_n1751Artefatos_Descricao ;
      private int[] H00C09_A645TipoDocumento_Codigo ;
      private bool[] H00C09_n645TipoDocumento_Codigo ;
      private int[] H00C09_A1106Anexo_Codigo ;
      private int[] H00C09_A1110AnexoDe_Tabela ;
      private int[] H00C09_A1109AnexoDe_Id ;
      private String[] H00C09_A1108Anexo_TipoArq ;
      private bool[] H00C09_n1108Anexo_TipoArq ;
      private String[] H00C09_A1107Anexo_NomeArq ;
      private bool[] H00C09_n1107Anexo_NomeArq ;
      private String[] H00C09_A1123Anexo_Descricao ;
      private bool[] H00C09_n1123Anexo_Descricao ;
      private DateTime[] H00C09_A1103Anexo_Data ;
      private String[] H00C09_A646TipoDocumento_Nome ;
      private String[] H00C09_A1450Anexo_Link ;
      private bool[] H00C09_n1450Anexo_Link ;
      private int[] H00C09_A1496Anexo_AreaTrabalhoCod ;
      private bool[] H00C09_n1496Anexo_AreaTrabalhoCod ;
      private int[] H00C09_A1495Anexo_Owner ;
      private bool[] H00C09_n1495Anexo_Owner ;
      private int[] H00C010_A1769ContagemResultadoArtefato_Codigo ;
      private int[] H00C010_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] H00C010_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] H00C010_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private String[] H00C010_A1751Artefatos_Descricao ;
      private bool[] H00C010_n1751Artefatos_Descricao ;
      private int[] H00C011_A586ContagemResultadoEvidencia_Codigo ;
      private String[] H00C011_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00C011_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] H00C011_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00C011_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] H00C011_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] H00C011_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] H00C012_A1106Anexo_Codigo ;
      private String[] H00C012_A1107Anexo_NomeArq ;
      private bool[] H00C012_n1107Anexo_NomeArq ;
      private String[] H00C012_A1108Anexo_TipoArq ;
      private bool[] H00C012_n1108Anexo_TipoArq ;
      private String[] H00C012_A1101Anexo_Arquivo ;
      private bool[] H00C012_n1101Anexo_Arquivo ;
      private int[] H00C013_A29Contratante_Codigo ;
      private bool[] H00C013_n29Contratante_Codigo ;
      private int[] H00C013_A5AreaTrabalho_Codigo ;
      private int[] H00C014_A1380Redmine_Codigo ;
      private int[] H00C014_A1384Redmine_ContratanteCod ;
      private String[] H00C014_A1381Redmine_Host ;
      private String[] H00C014_A1382Redmine_Url ;
      private String[] H00C014_A1383Redmine_Key ;
      private int[] H00C015_A586ContagemResultadoEvidencia_Codigo ;
      private DateTime[] H00C015_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] H00C015_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private String[] H00C015_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00C015_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] H00C015_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00C015_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] H00C015_A456ContagemResultado_Codigo ;
      private int[] H00C016_A1769ContagemResultadoArtefato_Codigo ;
      private int[] H00C016_A1772ContagemResultadoArtefato_OSCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpClient AV29httpclient ;
      private IGxSession AV40WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem ))]
      private IGxCollection AV17SDT_AnexosDemanda ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV42Sdt_ContagemResultadoEvidencias ;
      private GxFile AV58File ;
      private GxDirectory AV56Directory ;
      private GxFileCollection AV82GXV4 ;
      private SdtSDT_Redmineissue AV35SDT_Issue ;
      private SdtSDT_Redmineissue_attachments_attachment AV36Attachment ;
      private SdtContagemResultadoEvidencia AV20ContagemResultadoEvidencia ;
      private SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem AV18SDT_AnexosDemandaItem ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV41Sdt_ArquivoEvidencia ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemresultadoevidenciaswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00C02 ;
          prmH00C02 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C03 ;
          prmH00C03 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C04 ;
          prmH00C04 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C09 ;
          prmH00C09 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C010 ;
          prmH00C010 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C011 ;
          prmH00C011 = new Object[] {
          new Object[] {"@AV21Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C012 ;
          prmH00C012 = new Object[] {
          new Object[] {"@AV21Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C013 ;
          prmH00C013 = new Object[] {
          new Object[] {"@AV30AreaTrabalho",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C014 ;
          prmH00C014 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C015 ;
          prmH00C015 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38FileName",SqlDbType.Char,255,0} ,
          new Object[] {"@AV39Ext",SqlDbType.Char,5,0}
          } ;
          Object[] prmH00C016 ;
          prmH00C016 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00C02", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Evidencia], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_RdmnIssueId], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_StatusDmn] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C02,1,0,false,true )
             ,new CursorDef("H00C03", "SELECT T3.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[TipoDocumento_Codigo], T1.[ContagemResultadoEvidencia_Codigo], T1.[ContagemResultado_Codigo], T1.[ContagemResultadoEvidencia_TipoArq], T1.[ContagemResultadoEvidencia_NomeArq], T2.[TipoDocumento_Nome], T1.[ContagemResultadoEvidencia_Descricao], T1.[ContagemResultadoEvidencia_Data], T1.[ContagemResultadoEvidencia_Link], T4.[Contratada_AreaTrabalhoCod], T1.[ContagemResultadoEvidencia_Owner] FROM ((([ContagemResultadoEvidencia] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C03,100,0,true,false )
             ,new CursorDef("H00C04", "SELECT TOP 1 T1.[ContagemResultadoArtefato_Codigo], T1.[ContagemResultadoArtefato_EvdCod], T1.[ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, T2.[Artefatos_Descricao] FROM ([ContagemResultadoArtefato] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[ContagemResultadoArtefato_ArtefatoCod]) WHERE T1.[ContagemResultadoArtefato_EvdCod] = @ContagemResultadoEvidencia_Codigo ORDER BY T1.[ContagemResultadoArtefato_EvdCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C04,1,0,false,true )
             ,new CursorDef("H00C09", "SELECT T2.[TipoDocumento_Codigo], T1.[Anexo_Codigo], T1.[AnexoDe_Tabela], T1.[AnexoDe_Id], T2.[Anexo_TipoArq], T2.[Anexo_NomeArq], T2.[Anexo_Descricao], T2.[Anexo_Data], T3.[TipoDocumento_Nome], T2.[Anexo_Link], COALESCE( T4.[Anexo_AreaTrabalhoCod], 0) AS Anexo_AreaTrabalhoCod, T2.[Anexo_Owner] FROM ((([AnexosDe] T1 WITH (NOLOCK) INNER JOIN [Anexos] T2 WITH (NOLOCK) ON T2.[Anexo_Codigo] = T1.[Anexo_Codigo]) LEFT JOIN [TipoDocumento] T3 WITH (NOLOCK) ON T3.[TipoDocumento_Codigo] = T2.[TipoDocumento_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T6.[AnexoDe_Tabela], 0) = 1 THEN COALESCE( T7.[Contratada_AreaTrabalhoCod], 0) END AS Anexo_AreaTrabalhoCod, T5.[Anexo_Codigo] FROM (([Anexos] T5 WITH (NOLOCK) LEFT JOIN (SELECT MIN([AnexoDe_Tabela]) AS AnexoDe_Tabela, [Anexo_Codigo] FROM [AnexosDe] WITH (NOLOCK) GROUP BY [Anexo_Codigo] ) T6 ON T6.[Anexo_Codigo] = T5.[Anexo_Codigo]) LEFT JOIN (SELECT MIN(T9.[Contratada_AreaTrabalhoCod]) AS Contratada_AreaTrabalhoCod, COALESCE( T11.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod, T10.[Anexo_Codigo] FROM ([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN [Contratada] T9 WITH (NOLOCK) ON T9.[Contratada_Codigo] = T8.[ContagemResultado_ContratadaCod]),  ([Anexos] T10 WITH (NOLOCK) LEFT JOIN (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (NOLOCK) GROUP BY [Anexo_Codigo] ) T11 ON T11.[Anexo_Codigo] = T10.[Anexo_Codigo]) WHERE (T11.[Anexo_Codigo] = T10.[Anexo_Codigo]) AND (T8.[ContagemResultado_Codigo] = COALESCE( T11.[Anexo_DemandaCod], 0)) GROUP BY T11.[Anexo_DemandaCod], T10.[Anexo_Codigo] ) T7 ON T7.[Anexo_Codigo] = T5.[Anexo_Codigo]) ) T4 ON T4.[Anexo_Codigo] = T1.[Anexo_Codigo]) WHERE (T1.[AnexoDe_Id] = @AV7ContagemResultado_Codigo) AND (T1.[AnexoDe_Tabela] = 1) ORDER BY T1.[AnexoDe_Id] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C09,100,0,true,false )
             ,new CursorDef("H00C010", "SELECT TOP 1 T1.[ContagemResultadoArtefato_Codigo], T1.[ContagemResultadoArtefato_AnxCod], T1.[ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, T2.[Artefatos_Descricao] FROM ([ContagemResultadoArtefato] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[ContagemResultadoArtefato_ArtefatoCod]) WHERE T1.[ContagemResultadoArtefato_AnxCod] = @Anexo_Codigo ORDER BY T1.[ContagemResultadoArtefato_AnxCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C010,1,0,false,true )
             ,new CursorDef("H00C011", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @AV21Codigo ORDER BY [ContagemResultadoEvidencia_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C011,1,0,false,true )
             ,new CursorDef("H00C012", "SELECT TOP 1 [Anexo_Codigo], [Anexo_NomeArq], [Anexo_TipoArq], [Anexo_Arquivo] FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @AV21Codigo ORDER BY [Anexo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C012,1,0,false,true )
             ,new CursorDef("H00C013", "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV30AreaTrabalho ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C013,1,0,true,true )
             ,new CursorDef("H00C014", "SELECT TOP 1 [Redmine_Codigo], [Redmine_ContratanteCod], [Redmine_Host], [Redmine_Url], [Redmine_Key] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Contratante_Codigo ORDER BY [Redmine_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C014,1,0,false,true )
             ,new CursorDef("H00C015", "SELECT [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_NomeArq], [ContagemResultado_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo) AND ([ContagemResultadoEvidencia_NomeArq] = @AV38FileName) AND ([ContagemResultadoEvidencia_TipoArq] = @AV39Ext) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C015,100,0,false,false )
             ,new CursorDef("H00C016", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo], [ContagemResultadoArtefato_OSCod] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_OSCod] = @AV7ContagemResultado_Codigo ORDER BY [ContagemResultadoArtefato_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C016,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(4, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(4, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
