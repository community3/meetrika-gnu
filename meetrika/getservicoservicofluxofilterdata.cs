/*
               File: GetServicoServicoFluxoFilterData
        Description: Get Servico Servico Fluxo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:40.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getservicoservicofluxofilterdata : GXProcedure
   {
      public getservicoservicofluxofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getservicoservicofluxofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getservicoservicofluxofilterdata objgetservicoservicofluxofilterdata;
         objgetservicoservicofluxofilterdata = new getservicoservicofluxofilterdata();
         objgetservicoservicofluxofilterdata.AV22DDOName = aP0_DDOName;
         objgetservicoservicofluxofilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetservicoservicofluxofilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetservicoservicofluxofilterdata.AV26OptionsJson = "" ;
         objgetservicoservicofluxofilterdata.AV29OptionsDescJson = "" ;
         objgetservicoservicofluxofilterdata.AV31OptionIndexesJson = "" ;
         objgetservicoservicofluxofilterdata.context.SetSubmitInitialConfig(context);
         objgetservicoservicofluxofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetservicoservicofluxofilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getservicoservicofluxofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_SERVICOFLUXO_SRVPOSSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOFLUXO_SRVPOSSIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("ServicoServicoFluxoGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ServicoServicoFluxoGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("ServicoServicoFluxoGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_ORDEM") == 0 )
            {
               AV10TFServicoFluxo_Ordem = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA") == 0 )
            {
               AV12TFServicoFluxo_SrvPosSigla = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA_SEL") == 0 )
            {
               AV13TFServicoFluxo_SrvPosSigla_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSPRCTMP") == 0 )
            {
               AV14TFServicoFluxo_SrvPosPrcTmp = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV15TFServicoFluxo_SrvPosPrcTmp_To = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSPRCPGM") == 0 )
            {
               AV16TFServicoFluxo_SrvPosPrcPgm = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV17TFServicoFluxo_SrvPosPrcPgm_To = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSPRCCNC") == 0 )
            {
               AV18TFServicoFluxo_SrvPosPrcCnc = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV19TFServicoFluxo_SrvPosPrcCnc_To = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "PARM_&SERVICOFLUXO_SERVICOCOD") == 0 )
            {
               AV38ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICOFLUXO_SRVPOSSIGLAOPTIONS' Routine */
         AV12TFServicoFluxo_SrvPosSigla = AV20SearchTxt;
         AV13TFServicoFluxo_SrvPosSigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10TFServicoFluxo_Ordem ,
                                              AV11TFServicoFluxo_Ordem_To ,
                                              AV13TFServicoFluxo_SrvPosSigla_Sel ,
                                              AV12TFServicoFluxo_SrvPosSigla ,
                                              AV14TFServicoFluxo_SrvPosPrcTmp ,
                                              AV15TFServicoFluxo_SrvPosPrcTmp_To ,
                                              AV16TFServicoFluxo_SrvPosPrcPgm ,
                                              AV17TFServicoFluxo_SrvPosPrcPgm_To ,
                                              AV18TFServicoFluxo_SrvPosPrcCnc ,
                                              AV19TFServicoFluxo_SrvPosPrcCnc_To ,
                                              A1532ServicoFluxo_Ordem ,
                                              A1527ServicoFluxo_SrvPosSigla ,
                                              A1554ServicoFluxo_SrvPosPrcTmp ,
                                              A1555ServicoFluxo_SrvPosPrcPgm ,
                                              A1556ServicoFluxo_SrvPosPrcCnc ,
                                              AV38ServicoFluxo_ServicoCod ,
                                              A1522ServicoFluxo_ServicoCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV12TFServicoFluxo_SrvPosSigla = StringUtil.PadR( StringUtil.RTrim( AV12TFServicoFluxo_SrvPosSigla), 15, "%");
         /* Using cursor P00JQ2 */
         pr_default.execute(0, new Object[] {AV38ServicoFluxo_ServicoCod, AV10TFServicoFluxo_Ordem, AV11TFServicoFluxo_Ordem_To, lV12TFServicoFluxo_SrvPosSigla, AV13TFServicoFluxo_SrvPosSigla_Sel, AV14TFServicoFluxo_SrvPosPrcTmp, AV15TFServicoFluxo_SrvPosPrcTmp_To, AV16TFServicoFluxo_SrvPosPrcPgm, AV17TFServicoFluxo_SrvPosPrcPgm_To, AV18TFServicoFluxo_SrvPosPrcCnc, AV19TFServicoFluxo_SrvPosPrcCnc_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJQ2 = false;
            A1526ServicoFluxo_ServicoPos = P00JQ2_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = P00JQ2_n1526ServicoFluxo_ServicoPos[0];
            A1522ServicoFluxo_ServicoCod = P00JQ2_A1522ServicoFluxo_ServicoCod[0];
            A1527ServicoFluxo_SrvPosSigla = P00JQ2_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = P00JQ2_n1527ServicoFluxo_SrvPosSigla[0];
            A1556ServicoFluxo_SrvPosPrcCnc = P00JQ2_A1556ServicoFluxo_SrvPosPrcCnc[0];
            n1556ServicoFluxo_SrvPosPrcCnc = P00JQ2_n1556ServicoFluxo_SrvPosPrcCnc[0];
            A1555ServicoFluxo_SrvPosPrcPgm = P00JQ2_A1555ServicoFluxo_SrvPosPrcPgm[0];
            n1555ServicoFluxo_SrvPosPrcPgm = P00JQ2_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1554ServicoFluxo_SrvPosPrcTmp = P00JQ2_A1554ServicoFluxo_SrvPosPrcTmp[0];
            n1554ServicoFluxo_SrvPosPrcTmp = P00JQ2_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1532ServicoFluxo_Ordem = P00JQ2_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = P00JQ2_n1532ServicoFluxo_Ordem[0];
            A1528ServicoFluxo_Codigo = P00JQ2_A1528ServicoFluxo_Codigo[0];
            A1527ServicoFluxo_SrvPosSigla = P00JQ2_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = P00JQ2_n1527ServicoFluxo_SrvPosSigla[0];
            A1556ServicoFluxo_SrvPosPrcCnc = P00JQ2_A1556ServicoFluxo_SrvPosPrcCnc[0];
            n1556ServicoFluxo_SrvPosPrcCnc = P00JQ2_n1556ServicoFluxo_SrvPosPrcCnc[0];
            A1555ServicoFluxo_SrvPosPrcPgm = P00JQ2_A1555ServicoFluxo_SrvPosPrcPgm[0];
            n1555ServicoFluxo_SrvPosPrcPgm = P00JQ2_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1554ServicoFluxo_SrvPosPrcTmp = P00JQ2_A1554ServicoFluxo_SrvPosPrcTmp[0];
            n1554ServicoFluxo_SrvPosPrcTmp = P00JQ2_n1554ServicoFluxo_SrvPosPrcTmp[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00JQ2_A1522ServicoFluxo_ServicoCod[0] == A1522ServicoFluxo_ServicoCod ) && ( StringUtil.StrCmp(P00JQ2_A1527ServicoFluxo_SrvPosSigla[0], A1527ServicoFluxo_SrvPosSigla) == 0 ) )
            {
               BRKJQ2 = false;
               A1526ServicoFluxo_ServicoPos = P00JQ2_A1526ServicoFluxo_ServicoPos[0];
               n1526ServicoFluxo_ServicoPos = P00JQ2_n1526ServicoFluxo_ServicoPos[0];
               A1528ServicoFluxo_Codigo = P00JQ2_A1528ServicoFluxo_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKJQ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla)) )
            {
               AV24Option = A1527ServicoFluxo_SrvPosSigla;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJQ2 )
            {
               BRKJQ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFServicoFluxo_SrvPosSigla = "";
         AV13TFServicoFluxo_SrvPosSigla_Sel = "";
         scmdbuf = "";
         lV12TFServicoFluxo_SrvPosSigla = "";
         A1527ServicoFluxo_SrvPosSigla = "";
         P00JQ2_A1526ServicoFluxo_ServicoPos = new int[1] ;
         P00JQ2_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         P00JQ2_A1522ServicoFluxo_ServicoCod = new int[1] ;
         P00JQ2_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         P00JQ2_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         P00JQ2_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         P00JQ2_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         P00JQ2_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         P00JQ2_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         P00JQ2_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         P00JQ2_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         P00JQ2_A1532ServicoFluxo_Ordem = new short[1] ;
         P00JQ2_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         P00JQ2_A1528ServicoFluxo_Codigo = new int[1] ;
         AV24Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getservicoservicofluxofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JQ2_A1526ServicoFluxo_ServicoPos, P00JQ2_n1526ServicoFluxo_ServicoPos, P00JQ2_A1522ServicoFluxo_ServicoCod, P00JQ2_A1527ServicoFluxo_SrvPosSigla, P00JQ2_n1527ServicoFluxo_SrvPosSigla, P00JQ2_A1556ServicoFluxo_SrvPosPrcCnc, P00JQ2_n1556ServicoFluxo_SrvPosPrcCnc, P00JQ2_A1555ServicoFluxo_SrvPosPrcPgm, P00JQ2_n1555ServicoFluxo_SrvPosPrcPgm, P00JQ2_A1554ServicoFluxo_SrvPosPrcTmp,
               P00JQ2_n1554ServicoFluxo_SrvPosPrcTmp, P00JQ2_A1532ServicoFluxo_Ordem, P00JQ2_n1532ServicoFluxo_Ordem, P00JQ2_A1528ServicoFluxo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10TFServicoFluxo_Ordem ;
      private short AV11TFServicoFluxo_Ordem_To ;
      private short AV14TFServicoFluxo_SrvPosPrcTmp ;
      private short AV15TFServicoFluxo_SrvPosPrcTmp_To ;
      private short AV16TFServicoFluxo_SrvPosPrcPgm ;
      private short AV17TFServicoFluxo_SrvPosPrcPgm_To ;
      private short AV18TFServicoFluxo_SrvPosPrcCnc ;
      private short AV19TFServicoFluxo_SrvPosPrcCnc_To ;
      private short A1532ServicoFluxo_Ordem ;
      private short A1554ServicoFluxo_SrvPosPrcTmp ;
      private short A1555ServicoFluxo_SrvPosPrcPgm ;
      private short A1556ServicoFluxo_SrvPosPrcCnc ;
      private int AV41GXV1 ;
      private int AV38ServicoFluxo_ServicoCod ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int A1526ServicoFluxo_ServicoPos ;
      private int A1528ServicoFluxo_Codigo ;
      private long AV32count ;
      private String AV12TFServicoFluxo_SrvPosSigla ;
      private String AV13TFServicoFluxo_SrvPosSigla_Sel ;
      private String scmdbuf ;
      private String lV12TFServicoFluxo_SrvPosSigla ;
      private String A1527ServicoFluxo_SrvPosSigla ;
      private bool returnInSub ;
      private bool BRKJQ2 ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private bool n1527ServicoFluxo_SrvPosSigla ;
      private bool n1556ServicoFluxo_SrvPosPrcCnc ;
      private bool n1555ServicoFluxo_SrvPosPrcPgm ;
      private bool n1554ServicoFluxo_SrvPosPrcTmp ;
      private bool n1532ServicoFluxo_Ordem ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JQ2_A1526ServicoFluxo_ServicoPos ;
      private bool[] P00JQ2_n1526ServicoFluxo_ServicoPos ;
      private int[] P00JQ2_A1522ServicoFluxo_ServicoCod ;
      private String[] P00JQ2_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] P00JQ2_n1527ServicoFluxo_SrvPosSigla ;
      private short[] P00JQ2_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] P00JQ2_n1556ServicoFluxo_SrvPosPrcCnc ;
      private short[] P00JQ2_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] P00JQ2_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] P00JQ2_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] P00JQ2_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] P00JQ2_A1532ServicoFluxo_Ordem ;
      private bool[] P00JQ2_n1532ServicoFluxo_Ordem ;
      private int[] P00JQ2_A1528ServicoFluxo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
   }

   public class getservicoservicofluxofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JQ2( IGxContext context ,
                                             short AV10TFServicoFluxo_Ordem ,
                                             short AV11TFServicoFluxo_Ordem_To ,
                                             String AV13TFServicoFluxo_SrvPosSigla_Sel ,
                                             String AV12TFServicoFluxo_SrvPosSigla ,
                                             short AV14TFServicoFluxo_SrvPosPrcTmp ,
                                             short AV15TFServicoFluxo_SrvPosPrcTmp_To ,
                                             short AV16TFServicoFluxo_SrvPosPrcPgm ,
                                             short AV17TFServicoFluxo_SrvPosPrcPgm_To ,
                                             short AV18TFServicoFluxo_SrvPosPrcCnc ,
                                             short AV19TFServicoFluxo_SrvPosPrcCnc_To ,
                                             short A1532ServicoFluxo_Ordem ,
                                             String A1527ServicoFluxo_SrvPosSigla ,
                                             short A1554ServicoFluxo_SrvPosPrcTmp ,
                                             short A1555ServicoFluxo_SrvPosPrcPgm ,
                                             short A1556ServicoFluxo_SrvPosPrcCnc ,
                                             int AV38ServicoFluxo_ServicoCod ,
                                             int A1522ServicoFluxo_ServicoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos, T1.[ServicoFluxo_ServicoCod], T2.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T2.[Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc, T2.[Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, T2.[Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, T1.[ServicoFluxo_Ordem], T1.[ServicoFluxo_Codigo] FROM ([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos])";
         scmdbuf = scmdbuf + " WHERE (T1.[ServicoFluxo_ServicoCod] = @AV38ServicoFluxo_ServicoCod)";
         if ( ! (0==AV10TFServicoFluxo_Ordem) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] >= @AV10TFServicoFluxo_Ordem)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV11TFServicoFluxo_Ordem_To) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] <= @AV11TFServicoFluxo_Ordem_To)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoFluxo_SrvPosSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServicoFluxo_SrvPosSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV12TFServicoFluxo_SrvPosSigla)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoFluxo_SrvPosSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV13TFServicoFluxo_SrvPosSigla_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV14TFServicoFluxo_SrvPosPrcTmp) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercTmp] >= @AV14TFServicoFluxo_SrvPosPrcTmp)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV15TFServicoFluxo_SrvPosPrcTmp_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercTmp] <= @AV15TFServicoFluxo_SrvPosPrcTmp_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV16TFServicoFluxo_SrvPosPrcPgm) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercPgm] >= @AV16TFServicoFluxo_SrvPosPrcPgm)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV17TFServicoFluxo_SrvPosPrcPgm_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercPgm] <= @AV17TFServicoFluxo_SrvPosPrcPgm_To)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV18TFServicoFluxo_SrvPosPrcCnc) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercCnc] >= @AV18TFServicoFluxo_SrvPosPrcCnc)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV19TFServicoFluxo_SrvPosPrcCnc_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercCnc] <= @AV19TFServicoFluxo_SrvPosPrcCnc_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ServicoFluxo_ServicoCod], T2.[Servico_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JQ2(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JQ2 ;
          prmP00JQ2 = new Object[] {
          new Object[] {"@AV38ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV11TFServicoFluxo_Ordem_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV12TFServicoFluxo_SrvPosSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFServicoFluxo_SrvPosSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV14TFServicoFluxo_SrvPosPrcTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFServicoFluxo_SrvPosPrcTmp_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV16TFServicoFluxo_SrvPosPrcPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV17TFServicoFluxo_SrvPosPrcPgm_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV18TFServicoFluxo_SrvPosPrcCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFServicoFluxo_SrvPosPrcCnc_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JQ2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getservicoservicofluxofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getservicoservicofluxofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getservicoservicofluxofilterdata") )
          {
             return  ;
          }
          getservicoservicofluxofilterdata worker = new getservicoservicofluxofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
