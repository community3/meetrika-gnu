/*
               File: PRC_PontualidadeLoteReduz
        Description: Pontualidade Lote Reduz
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:1.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_pontualidadelotereduz : GXProcedure
   {
      public prc_pontualidadelotereduz( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_pontualidadelotereduz( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                           short aP1_Dias ,
                           short aP2_IndP ,
                           out decimal aP3_Reduz )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV9Dias = aP1_Dias;
         this.AV11IndP = aP2_IndP;
         this.AV10Reduz = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP3_Reduz=this.AV10Reduz;
      }

      public decimal executeUdp( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                                 short aP1_Dias ,
                                 short aP2_IndP )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV9Dias = aP1_Dias;
         this.AV11IndP = aP2_IndP;
         this.AV10Reduz = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP3_Reduz=this.AV10Reduz;
         return AV10Reduz ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                                 short aP1_Dias ,
                                 short aP2_IndP ,
                                 out decimal aP3_Reduz )
      {
         prc_pontualidadelotereduz objprc_pontualidadelotereduz;
         objprc_pontualidadelotereduz = new prc_pontualidadelotereduz();
         objprc_pontualidadelotereduz.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         objprc_pontualidadelotereduz.AV9Dias = aP1_Dias;
         objprc_pontualidadelotereduz.AV11IndP = aP2_IndP;
         objprc_pontualidadelotereduz.AV10Reduz = 0 ;
         objprc_pontualidadelotereduz.context.SetSubmitInitialConfig(context);
         objprc_pontualidadelotereduz.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_pontualidadelotereduz);
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP3_Reduz=this.AV10Reduz;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_pontualidadelotereduz)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008Y2 */
         pr_default.execute(0, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1269ContratoServicosIndicador_Codigo = P008Y2_A1269ContratoServicosIndicador_Codigo[0];
            A1345ContratoServicosIndicador_CalculoSob = P008Y2_A1345ContratoServicosIndicador_CalculoSob[0];
            n1345ContratoServicosIndicador_CalculoSob = P008Y2_n1345ContratoServicosIndicador_CalculoSob[0];
            A1308ContratoServicosIndicador_Tipo = P008Y2_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P008Y2_n1308ContratoServicosIndicador_Tipo[0];
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A1345ContratoServicosIndicador_CalculoSob ,
                                                 A1301ContratoServicosIndicadorFaixa_Desde ,
                                                 AV9Dias ,
                                                 A1302ContratoServicosIndicadorFaixa_Ate ,
                                                 AV11IndP ,
                                                 A1269ContratoServicosIndicador_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT
                                                 }
            });
            /* Using cursor P008Y3 */
            pr_default.execute(1, new Object[] {A1269ContratoServicosIndicador_Codigo, AV9Dias, AV9Dias, AV9Dias, AV11IndP, AV11IndP, AV11IndP});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1302ContratoServicosIndicadorFaixa_Ate = P008Y3_A1302ContratoServicosIndicadorFaixa_Ate[0];
               A1301ContratoServicosIndicadorFaixa_Desde = P008Y3_A1301ContratoServicosIndicadorFaixa_Desde[0];
               A1303ContratoServicosIndicadorFaixa_Reduz = P008Y3_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               A1299ContratoServicosIndicadorFaixa_Codigo = P008Y3_A1299ContratoServicosIndicadorFaixa_Codigo[0];
               AV10Reduz = A1303ContratoServicosIndicadorFaixa_Reduz;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008Y2_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P008Y2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P008Y2_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         P008Y2_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         P008Y2_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P008Y2_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         A1345ContratoServicosIndicador_CalculoSob = "";
         A1308ContratoServicosIndicador_Tipo = "";
         P008Y3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P008Y3_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P008Y3_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P008Y3_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P008Y3_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_pontualidadelotereduz__default(),
            new Object[][] {
                new Object[] {
               P008Y2_A1270ContratoServicosIndicador_CntSrvCod, P008Y2_A1269ContratoServicosIndicador_Codigo, P008Y2_A1345ContratoServicosIndicador_CalculoSob, P008Y2_n1345ContratoServicosIndicador_CalculoSob, P008Y2_A1308ContratoServicosIndicador_Tipo, P008Y2_n1308ContratoServicosIndicador_Tipo
               }
               , new Object[] {
               P008Y3_A1269ContratoServicosIndicador_Codigo, P008Y3_A1302ContratoServicosIndicadorFaixa_Ate, P008Y3_A1301ContratoServicosIndicadorFaixa_Desde, P008Y3_A1303ContratoServicosIndicadorFaixa_Reduz, P008Y3_A1299ContratoServicosIndicadorFaixa_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Dias ;
      private short AV11IndP ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private decimal AV10Reduz ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private String scmdbuf ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosIndicador_CntSrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P008Y2_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P008Y2_A1269ContratoServicosIndicador_Codigo ;
      private String[] P008Y2_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] P008Y2_n1345ContratoServicosIndicador_CalculoSob ;
      private String[] P008Y2_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P008Y2_n1308ContratoServicosIndicador_Tipo ;
      private int[] P008Y3_A1269ContratoServicosIndicador_Codigo ;
      private decimal[] P008Y3_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P008Y3_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P008Y3_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private int[] P008Y3_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private decimal aP3_Reduz ;
   }

   public class prc_pontualidadelotereduz__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P008Y3( IGxContext context ,
                                             String A1345ContratoServicosIndicador_CalculoSob ,
                                             decimal A1301ContratoServicosIndicadorFaixa_Desde ,
                                             short AV9Dias ,
                                             decimal A1302ContratoServicosIndicadorFaixa_Ate ,
                                             short AV11IndP ,
                                             int A1269ContratoServicosIndicador_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo)";
         if ( StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "D") == 0 )
         {
            sWhereString = sWhereString + " and (( [ContratoServicosIndicadorFaixa_Desde] <= @AV9Dias and [ContratoServicosIndicadorFaixa_Ate] >= @AV9Dias) or ( [ContratoServicosIndicadorFaixa_Desde] <= @AV9Dias and ([ContratoServicosIndicadorFaixa_Ate] = convert(int, 0))))";
         }
         else
         {
            GXv_int1[1] = 1;
            GXv_int1[2] = 1;
            GXv_int1[3] = 1;
         }
         if ( StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "P") == 0 )
         {
            sWhereString = sWhereString + " and (( [ContratoServicosIndicadorFaixa_Desde] <= @AV11IndP and [ContratoServicosIndicadorFaixa_Ate] >= @AV11IndP) or ( [ContratoServicosIndicadorFaixa_Desde] <= @AV11IndP and ([ContratoServicosIndicadorFaixa_Ate] = convert(int, 0))))";
         }
         else
         {
            GXv_int1[4] = 1;
            GXv_int1[5] = 1;
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContratoServicosIndicador_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P008Y3(context, (String)dynConstraints[0] , (decimal)dynConstraints[1] , (short)dynConstraints[2] , (decimal)dynConstraints[3] , (short)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008Y2 ;
          prmP008Y2 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008Y3 ;
          prmP008Y3 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV11IndP",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV11IndP",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV11IndP",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008Y2", "SELECT [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Tipo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE ([ContratoServicosIndicador_CntSrvCod] = @ContratoServicosIndicador_CntSrvCod) AND ([ContratoServicosIndicador_Tipo] = 'PL') ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008Y2,100,0,true,false )
             ,new CursorDef("P008Y3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008Y3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 2) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                return;
       }
    }

 }

}
