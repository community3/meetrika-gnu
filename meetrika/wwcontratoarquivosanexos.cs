/*
               File: WWContratoArquivosAnexos
        Description:  Contrato Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:35:27.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoarquivosanexos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoarquivosanexos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoarquivosanexos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_87 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_87_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_87_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoArquivosAnexos_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
               AV37ContratoArquivosAnexos_Data1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoArquivosAnexos_Data1", context.localUtil.TToC( AV37ContratoArquivosAnexos_Data1, 8, 5, 0, 3, "/", ":", " "));
               AV38ContratoArquivosAnexos_Data_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoArquivosAnexos_Data_To1", context.localUtil.TToC( AV38ContratoArquivosAnexos_Data_To1, 8, 5, 0, 3, "/", ":", " "));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ContratoArquivosAnexos_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
               AV39ContratoArquivosAnexos_Data2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoArquivosAnexos_Data2", context.localUtil.TToC( AV39ContratoArquivosAnexos_Data2, 8, 5, 0, 3, "/", ":", " "));
               AV40ContratoArquivosAnexos_Data_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoArquivosAnexos_Data_To2", context.localUtil.TToC( AV40ContratoArquivosAnexos_Data_To2, 8, 5, 0, 3, "/", ":", " "));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV46TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
               AV47TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
               AV50TFContratada_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratada_PessoaNom", AV50TFContratada_PessoaNom);
               AV51TFContratada_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom_Sel", AV51TFContratada_PessoaNom_Sel);
               AV54TFContratada_PessoaCNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratada_PessoaCNPJ", AV54TFContratada_PessoaCNPJ);
               AV55TFContratada_PessoaCNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ_Sel", AV55TFContratada_PessoaCNPJ_Sel);
               AV58TFContratoArquivosAnexos_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContratoArquivosAnexos_Descricao", AV58TFContratoArquivosAnexos_Descricao);
               AV59TFContratoArquivosAnexos_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao_Sel", AV59TFContratoArquivosAnexos_Descricao_Sel);
               AV62TFContratoArquivosAnexos_NomeArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratoArquivosAnexos_NomeArq", AV62TFContratoArquivosAnexos_NomeArq);
               AV63TFContratoArquivosAnexos_NomeArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_NomeArq_Sel", AV63TFContratoArquivosAnexos_NomeArq_Sel);
               AV66TFContratoArquivosAnexos_TipoArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoArquivosAnexos_TipoArq", AV66TFContratoArquivosAnexos_TipoArq);
               AV67TFContratoArquivosAnexos_TipoArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoArquivosAnexos_TipoArq_Sel", AV67TFContratoArquivosAnexos_TipoArq_Sel);
               AV70TFContratoArquivosAnexos_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
               AV71TFContratoArquivosAnexos_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV48ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Contrato_NumeroTitleControlIdToReplace", AV48ddo_Contrato_NumeroTitleControlIdToReplace);
               AV52ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Contratada_PessoaNomTitleControlIdToReplace", AV52ddo_Contratada_PessoaNomTitleControlIdToReplace);
               AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
               AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace", AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace);
               AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace", AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace);
               AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace", AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace);
               AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace", AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace);
               AV119Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A108ContratoArquivosAnexos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA6L2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START6L2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117352848");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoarquivosanexos.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO1", AV17ContratoArquivosAnexos_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DATA1", context.localUtil.TToC( AV37ContratoArquivosAnexos_Data1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DATA_TO1", context.localUtil.TToC( AV38ContratoArquivosAnexos_Data_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO2", AV21ContratoArquivosAnexos_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DATA2", context.localUtil.TToC( AV39ContratoArquivosAnexos_Data2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DATA_TO2", context.localUtil.TToC( AV40ContratoArquivosAnexos_Data_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV46TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV47TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV50TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV51TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ", AV54TFContratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ_SEL", AV55TFContratada_PessoaCNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO", AV58TFContratoArquivosAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL", AV59TFContratoArquivosAnexos_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_NOMEARQ", StringUtil.RTrim( AV62TFContratoArquivosAnexos_NomeArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL", StringUtil.RTrim( AV63TFContratoArquivosAnexos_NomeArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_TIPOARQ", StringUtil.RTrim( AV66TFContratoArquivosAnexos_TipoArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL", StringUtil.RTrim( AV67TFContratoArquivosAnexos_TipoArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA", context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA_TO", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_87", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_87), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV75DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV75DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV45Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV45Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV49Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV49Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV53Contratada_PessoaCNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV53Contratada_PessoaCNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA", AV57ContratoArquivosAnexos_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA", AV57ContratoArquivosAnexos_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOARQUIVOSANEXOS_NOMEARQTITLEFILTERDATA", AV61ContratoArquivosAnexos_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOARQUIVOSANEXOS_NOMEARQTITLEFILTERDATA", AV61ContratoArquivosAnexos_NomeArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOARQUIVOSANEXOS_TIPOARQTITLEFILTERDATA", AV65ContratoArquivosAnexos_TipoArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOARQUIVOSANEXOS_TIPOARQTITLEFILTERDATA", AV65ContratoArquivosAnexos_TipoArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA", AV69ContratoArquivosAnexos_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA", AV69ContratoArquivosAnexos_DataTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV119Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Caption", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cls", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Caption", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Cls", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Sortedstatus", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoarquivosanexos_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Sortasc", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Sortdsc", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Caption", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Tooltip", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Cls", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filteredtext_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includesortasc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_tipoarq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_tipoarq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Sortedstatus", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includefilter", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_tipoarq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filtertype", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filterisrange", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_tipoarq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includedatalist", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_tipoarq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Datalisttype", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Datalistproc", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoarquivosanexos_tipoarq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Sortasc", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Sortdsc", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Loadingdata", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Cleanfilter", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Noresultsfound", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Searchbuttontext", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Caption", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Tooltip", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cls", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filtertype", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortasc", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_nomearq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Activeeventkey", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filteredtext_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE6L2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT6L2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoarquivosanexos.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoArquivosAnexos" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contrato Arquivos Anexos" ;
      }

      protected void WB6L0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_6L2( true) ;
         }
         else
         {
            wb_table1_2_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV46TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV46TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV47TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV47TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV50TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV50TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV51TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV51TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_Internalname, AV54TFContratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( AV54TFContratada_PessoaCNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_sel_Internalname, AV55TFContratada_PessoaCNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV55TFContratada_PessoaCNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoarquivosanexos_descricao_Internalname, AV58TFContratoArquivosAnexos_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavTfcontratoarquivosanexos_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoarquivosanexos_descricao_sel_Internalname, AV59TFContratoArquivosAnexos_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", 0, edtavTfcontratoarquivosanexos_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_nomearq_Internalname, StringUtil.RTrim( AV62TFContratoArquivosAnexos_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV62TFContratoArquivosAnexos_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_nomearq_sel_Internalname, StringUtil.RTrim( AV63TFContratoArquivosAnexos_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV63TFContratoArquivosAnexos_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_tipoarq_Internalname, StringUtil.RTrim( AV66TFContratoArquivosAnexos_TipoArq), StringUtil.RTrim( context.localUtil.Format( AV66TFContratoArquivosAnexos_TipoArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_tipoarq_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_tipoarq_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_tipoarq_sel_Internalname, StringUtil.RTrim( AV67TFContratoArquivosAnexos_TipoArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV67TFContratoArquivosAnexos_TipoArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_tipoarq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_tipoarq_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoarquivosanexos_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_data_Internalname, context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV70TFContratoArquivosAnexos_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoarquivosanexos_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoarquivosanexos_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoarquivosanexos_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_data_to_Internalname, context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV71TFContratoArquivosAnexos_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoarquivosanexos_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoarquivosanexos_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoarquivosanexos_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoarquivosanexos_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoarquivosanexos_dataauxdate_Internalname, context.localUtil.Format(AV72DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"), context.localUtil.Format( AV72DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoarquivosanexos_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoarquivosanexos_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname, context.localUtil.Format(AV73DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV73DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoarquivosanexos_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV48ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOARQUIVOSANEXOS_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOARQUIVOSANEXOS_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Internalname, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOARQUIVOSANEXOS_TIPOARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Internalname, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOARQUIVOSANEXOS_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
         }
         wbLoad = true;
      }

      protected void START6L2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contrato Arquivos Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6L0( ) ;
      }

      protected void WS6L2( )
      {
         START6L2( ) ;
         EVT6L2( ) ;
      }

      protected void EVT6L2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E116L2 */
                              E116L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E126L2 */
                              E126L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E136L2 */
                              E136L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E146L2 */
                              E146L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E156L2 */
                              E156L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E166L2 */
                              E166L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E176L2 */
                              E176L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOARQUIVOSANEXOS_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E186L2 */
                              E186L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E196L2 */
                              E196L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E206L2 */
                              E206L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E216L2 */
                              E216L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E226L2 */
                              E226L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E236L2 */
                              E236L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E246L2 */
                              E246L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E256L2 */
                              E256L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E266L2 */
                              E266L2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_87_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
                              SubsflControlProps_872( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV117Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV118Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                              n41Contratada_PessoaNom = false;
                              A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                              n42Contratada_PessoaCNPJ = false;
                              A110ContratoArquivosAnexos_Descricao = cgiGet( edtContratoArquivosAnexos_Descricao_Internalname);
                              A112ContratoArquivosAnexos_NomeArq = cgiGet( edtContratoArquivosAnexos_NomeArq_Internalname);
                              A109ContratoArquivosAnexos_TipoArq = cgiGet( edtContratoArquivosAnexos_TipoArq_Internalname);
                              A113ContratoArquivosAnexos_Data = context.localUtil.CToT( cgiGet( edtContratoArquivosAnexos_Data_Internalname), 0);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E276L2 */
                                    E276L2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E286L2 */
                                    E286L2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E296L2 */
                                    E296L2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoarquivosanexos_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO1"), AV17ContratoArquivosAnexos_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoarquivosanexos_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA1"), 0) != AV37ContratoArquivosAnexos_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoarquivosanexos_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA_TO1"), 0) != AV38ContratoArquivosAnexos_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoarquivosanexos_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO2"), AV21ContratoArquivosAnexos_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoarquivosanexos_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA2"), 0) != AV39ContratoArquivosAnexos_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoarquivosanexos_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA_TO2"), 0) != AV40ContratoArquivosAnexos_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV46TFContrato_Numero) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV47TFContrato_Numero_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV50TFContratada_PessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV51TFContratada_PessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoacnpj Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV54TFContratada_PessoaCNPJ) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoacnpj_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV55TFContratada_PessoaCNPJ_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO"), AV58TFContratoArquivosAnexos_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL"), AV59TFContratoArquivosAnexos_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_nomearq Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_NOMEARQ"), AV62TFContratoArquivosAnexos_NomeArq) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_nomearq_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL"), AV63TFContratoArquivosAnexos_NomeArq_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_tipoarq Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_TIPOARQ"), AV66TFContratoArquivosAnexos_TipoArq) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_tipoarq_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL"), AV67TFContratoArquivosAnexos_TipoArq_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA"), 0) != AV70TFContratoArquivosAnexos_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoarquivosanexos_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA_TO"), 0) != AV71TFContratoArquivosAnexos_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE6L2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA6L2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOARQUIVOSANEXOS_DESCRICAO", "Descri��o do Arquvo", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOARQUIVOSANEXOS_DATA", "Data/Hora Upload", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOARQUIVOSANEXOS_DESCRICAO", "Descri��o do Arquvo", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOARQUIVOSANEXOS_DATA", "Data/Hora Upload", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_872( ) ;
         while ( nGXsfl_87_idx <= nRC_GXsfl_87 )
         {
            sendrow_872( ) ;
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17ContratoArquivosAnexos_Descricao1 ,
                                       DateTime AV37ContratoArquivosAnexos_Data1 ,
                                       DateTime AV38ContratoArquivosAnexos_Data_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21ContratoArquivosAnexos_Descricao2 ,
                                       DateTime AV39ContratoArquivosAnexos_Data2 ,
                                       DateTime AV40ContratoArquivosAnexos_Data_To2 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       String AV46TFContrato_Numero ,
                                       String AV47TFContrato_Numero_Sel ,
                                       String AV50TFContratada_PessoaNom ,
                                       String AV51TFContratada_PessoaNom_Sel ,
                                       String AV54TFContratada_PessoaCNPJ ,
                                       String AV55TFContratada_PessoaCNPJ_Sel ,
                                       String AV58TFContratoArquivosAnexos_Descricao ,
                                       String AV59TFContratoArquivosAnexos_Descricao_Sel ,
                                       String AV62TFContratoArquivosAnexos_NomeArq ,
                                       String AV63TFContratoArquivosAnexos_NomeArq_Sel ,
                                       String AV66TFContratoArquivosAnexos_TipoArq ,
                                       String AV67TFContratoArquivosAnexos_TipoArq_Sel ,
                                       DateTime AV70TFContratoArquivosAnexos_Data ,
                                       DateTime AV71TFContratoArquivosAnexos_Data_To ,
                                       String AV48ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV52ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace ,
                                       String AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace ,
                                       String AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace ,
                                       String AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace ,
                                       String AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace ,
                                       String AV119Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A108ContratoArquivosAnexos_Codigo ,
                                       int A74Contrato_Codigo ,
                                       int A39Contratada_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6L2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DESCRICAO", GetSecureSignedToken( "", A110ContratoArquivosAnexos_Descricao));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_DESCRICAO", A110ContratoArquivosAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A112ContratoArquivosAnexos_NomeArq, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_NOMEARQ", StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_TIPOARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A109ContratoArquivosAnexos_TipoArq, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_TIPOARQ", StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DATA", GetSecureSignedToken( "", context.localUtil.Format( A113ContratoArquivosAnexos_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_DATA", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6L2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV119Pgmname = "WWContratoArquivosAnexos";
         context.Gx_err = 0;
      }

      protected void RF6L2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 87;
         /* Execute user event: E286L2 */
         E286L2 ();
         nGXsfl_87_idx = 1;
         sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
         SubsflControlProps_872( ) ;
         nGXsfl_87_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_872( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                                 AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                                 AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                                 AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                                 AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                                 AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                                 AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                                 AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                                 AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                                 AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                                 AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                                 AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                                 AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                                 AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                                 AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                                 AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                                 AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                                 AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                                 AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                                 AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                                 AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                                 AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                                 AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                                 AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                                 AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                                 A110ContratoArquivosAnexos_Descricao ,
                                                 A113ContratoArquivosAnexos_Data ,
                                                 A77Contrato_Numero ,
                                                 A41Contratada_PessoaNom ,
                                                 A42Contratada_PessoaCNPJ ,
                                                 A112ContratoArquivosAnexos_NomeArq ,
                                                 A109ContratoArquivosAnexos_TipoArq ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
            lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
            lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
            lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
            lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
            lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
            lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
            lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
            lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
            lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
            /* Using cursor H006L2 */
            pr_default.execute(0, new Object[] {lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_87_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A40Contratada_PessoaCod = H006L2_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = H006L2_A74Contrato_Codigo[0];
               A108ContratoArquivosAnexos_Codigo = H006L2_A108ContratoArquivosAnexos_Codigo[0];
               A39Contratada_Codigo = H006L2_A39Contratada_Codigo[0];
               A113ContratoArquivosAnexos_Data = H006L2_A113ContratoArquivosAnexos_Data[0];
               A109ContratoArquivosAnexos_TipoArq = H006L2_A109ContratoArquivosAnexos_TipoArq[0];
               A112ContratoArquivosAnexos_NomeArq = H006L2_A112ContratoArquivosAnexos_NomeArq[0];
               A110ContratoArquivosAnexos_Descricao = H006L2_A110ContratoArquivosAnexos_Descricao[0];
               A42Contratada_PessoaCNPJ = H006L2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006L2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006L2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006L2_n41Contratada_PessoaNom[0];
               A77Contrato_Numero = H006L2_A77Contrato_Numero[0];
               A39Contratada_Codigo = H006L2_A39Contratada_Codigo[0];
               A77Contrato_Numero = H006L2_A77Contrato_Numero[0];
               A40Contratada_PessoaCod = H006L2_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H006L2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006L2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006L2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006L2_n41Contratada_PessoaNom[0];
               /* Execute user event: E296L2 */
               E296L2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 87;
            WB6L0( ) ;
         }
         nGXsfl_87_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV17ContratoArquivosAnexos_Descricao1;
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV37ContratoArquivosAnexos_Data1;
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV38ContratoArquivosAnexos_Data_To1;
         AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV21ContratoArquivosAnexos_Descricao2;
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV39ContratoArquivosAnexos_Data2;
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV40ContratoArquivosAnexos_Data_To2;
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV50TFContratada_PessoaNom;
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV51TFContratada_PessoaNom_Sel;
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV54TFContratada_PessoaCNPJ;
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV55TFContratada_PessoaCNPJ_Sel;
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV58TFContratoArquivosAnexos_Descricao;
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV59TFContratoArquivosAnexos_Descricao_Sel;
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV62TFContratoArquivosAnexos_NomeArq;
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV63TFContratoArquivosAnexos_NomeArq_Sel;
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV66TFContratoArquivosAnexos_TipoArq;
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV67TFContratoArquivosAnexos_TipoArq_Sel;
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV70TFContratoArquivosAnexos_Data;
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV71TFContratoArquivosAnexos_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                              AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                              AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                              AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                              AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                              AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                              AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                              AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                              AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                              AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                              AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                              AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                              AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                              AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                              AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                              AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                              AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                              AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                              AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                              AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                              AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                              AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                              AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                              AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                              AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A113ContratoArquivosAnexos_Data ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
         lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
         lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
         lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
         lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
         lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
         /* Using cursor H006L3 */
         pr_default.execute(1, new Object[] {lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to});
         GRID_nRecordCount = H006L3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV17ContratoArquivosAnexos_Descricao1;
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV37ContratoArquivosAnexos_Data1;
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV38ContratoArquivosAnexos_Data_To1;
         AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV21ContratoArquivosAnexos_Descricao2;
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV39ContratoArquivosAnexos_Data2;
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV40ContratoArquivosAnexos_Data_To2;
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV50TFContratada_PessoaNom;
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV51TFContratada_PessoaNom_Sel;
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV54TFContratada_PessoaCNPJ;
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV55TFContratada_PessoaCNPJ_Sel;
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV58TFContratoArquivosAnexos_Descricao;
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV59TFContratoArquivosAnexos_Descricao_Sel;
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV62TFContratoArquivosAnexos_NomeArq;
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV63TFContratoArquivosAnexos_NomeArq_Sel;
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV66TFContratoArquivosAnexos_TipoArq;
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV67TFContratoArquivosAnexos_TipoArq_Sel;
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV70TFContratoArquivosAnexos_Data;
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV71TFContratoArquivosAnexos_Data_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV17ContratoArquivosAnexos_Descricao1;
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV37ContratoArquivosAnexos_Data1;
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV38ContratoArquivosAnexos_Data_To1;
         AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV21ContratoArquivosAnexos_Descricao2;
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV39ContratoArquivosAnexos_Data2;
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV40ContratoArquivosAnexos_Data_To2;
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV50TFContratada_PessoaNom;
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV51TFContratada_PessoaNom_Sel;
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV54TFContratada_PessoaCNPJ;
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV55TFContratada_PessoaCNPJ_Sel;
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV58TFContratoArquivosAnexos_Descricao;
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV59TFContratoArquivosAnexos_Descricao_Sel;
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV62TFContratoArquivosAnexos_NomeArq;
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV63TFContratoArquivosAnexos_NomeArq_Sel;
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV66TFContratoArquivosAnexos_TipoArq;
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV67TFContratoArquivosAnexos_TipoArq_Sel;
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV70TFContratoArquivosAnexos_Data;
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV71TFContratoArquivosAnexos_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV17ContratoArquivosAnexos_Descricao1;
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV37ContratoArquivosAnexos_Data1;
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV38ContratoArquivosAnexos_Data_To1;
         AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV21ContratoArquivosAnexos_Descricao2;
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV39ContratoArquivosAnexos_Data2;
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV40ContratoArquivosAnexos_Data_To2;
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV50TFContratada_PessoaNom;
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV51TFContratada_PessoaNom_Sel;
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV54TFContratada_PessoaCNPJ;
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV55TFContratada_PessoaCNPJ_Sel;
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV58TFContratoArquivosAnexos_Descricao;
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV59TFContratoArquivosAnexos_Descricao_Sel;
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV62TFContratoArquivosAnexos_NomeArq;
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV63TFContratoArquivosAnexos_NomeArq_Sel;
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV66TFContratoArquivosAnexos_TipoArq;
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV67TFContratoArquivosAnexos_TipoArq_Sel;
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV70TFContratoArquivosAnexos_Data;
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV71TFContratoArquivosAnexos_Data_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV17ContratoArquivosAnexos_Descricao1;
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV37ContratoArquivosAnexos_Data1;
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV38ContratoArquivosAnexos_Data_To1;
         AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV21ContratoArquivosAnexos_Descricao2;
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV39ContratoArquivosAnexos_Data2;
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV40ContratoArquivosAnexos_Data_To2;
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV50TFContratada_PessoaNom;
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV51TFContratada_PessoaNom_Sel;
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV54TFContratada_PessoaCNPJ;
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV55TFContratada_PessoaCNPJ_Sel;
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV58TFContratoArquivosAnexos_Descricao;
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV59TFContratoArquivosAnexos_Descricao_Sel;
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV62TFContratoArquivosAnexos_NomeArq;
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV63TFContratoArquivosAnexos_NomeArq_Sel;
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV66TFContratoArquivosAnexos_TipoArq;
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV67TFContratoArquivosAnexos_TipoArq_Sel;
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV70TFContratoArquivosAnexos_Data;
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV71TFContratoArquivosAnexos_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV17ContratoArquivosAnexos_Descricao1;
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV37ContratoArquivosAnexos_Data1;
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV38ContratoArquivosAnexos_Data_To1;
         AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV21ContratoArquivosAnexos_Descricao2;
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV39ContratoArquivosAnexos_Data2;
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV40ContratoArquivosAnexos_Data_To2;
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV50TFContratada_PessoaNom;
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV51TFContratada_PessoaNom_Sel;
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV54TFContratada_PessoaCNPJ;
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV55TFContratada_PessoaCNPJ_Sel;
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV58TFContratoArquivosAnexos_Descricao;
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV59TFContratoArquivosAnexos_Descricao_Sel;
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV62TFContratoArquivosAnexos_NomeArq;
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV63TFContratoArquivosAnexos_NomeArq_Sel;
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV66TFContratoArquivosAnexos_TipoArq;
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV67TFContratoArquivosAnexos_TipoArq_Sel;
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV70TFContratoArquivosAnexos_Data;
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV71TFContratoArquivosAnexos_Data_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6L0( )
      {
         /* Before Start, stand alone formulas. */
         AV119Pgmname = "WWContratoArquivosAnexos";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E276L2 */
         E276L2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV75DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV45Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV49Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACNPJTITLEFILTERDATA"), AV53Contratada_PessoaCNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA"), AV57ContratoArquivosAnexos_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOARQUIVOSANEXOS_NOMEARQTITLEFILTERDATA"), AV61ContratoArquivosAnexos_NomeArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOARQUIVOSANEXOS_TIPOARQTITLEFILTERDATA"), AV65ContratoArquivosAnexos_TipoArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA"), AV69ContratoArquivosAnexos_DataTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17ContratoArquivosAnexos_Descricao1 = cgiGet( edtavContratoarquivosanexos_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoarquivosanexos_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contrato Arquivos Anexos_Data1"}), 1, "vCONTRATOARQUIVOSANEXOS_DATA1");
               GX_FocusControl = edtavContratoarquivosanexos_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37ContratoArquivosAnexos_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoArquivosAnexos_Data1", context.localUtil.TToC( AV37ContratoArquivosAnexos_Data1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV37ContratoArquivosAnexos_Data1 = context.localUtil.CToT( cgiGet( edtavContratoarquivosanexos_data1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoArquivosAnexos_Data1", context.localUtil.TToC( AV37ContratoArquivosAnexos_Data1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoarquivosanexos_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contrato Arquivos Anexos_Data_To1"}), 1, "vCONTRATOARQUIVOSANEXOS_DATA_TO1");
               GX_FocusControl = edtavContratoarquivosanexos_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38ContratoArquivosAnexos_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoArquivosAnexos_Data_To1", context.localUtil.TToC( AV38ContratoArquivosAnexos_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV38ContratoArquivosAnexos_Data_To1 = context.localUtil.CToT( cgiGet( edtavContratoarquivosanexos_data_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoArquivosAnexos_Data_To1", context.localUtil.TToC( AV38ContratoArquivosAnexos_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21ContratoArquivosAnexos_Descricao2 = cgiGet( edtavContratoarquivosanexos_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoarquivosanexos_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contrato Arquivos Anexos_Data2"}), 1, "vCONTRATOARQUIVOSANEXOS_DATA2");
               GX_FocusControl = edtavContratoarquivosanexos_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39ContratoArquivosAnexos_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoArquivosAnexos_Data2", context.localUtil.TToC( AV39ContratoArquivosAnexos_Data2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV39ContratoArquivosAnexos_Data2 = context.localUtil.CToT( cgiGet( edtavContratoarquivosanexos_data2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoArquivosAnexos_Data2", context.localUtil.TToC( AV39ContratoArquivosAnexos_Data2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoarquivosanexos_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contrato Arquivos Anexos_Data_To2"}), 1, "vCONTRATOARQUIVOSANEXOS_DATA_TO2");
               GX_FocusControl = edtavContratoarquivosanexos_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40ContratoArquivosAnexos_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoArquivosAnexos_Data_To2", context.localUtil.TToC( AV40ContratoArquivosAnexos_Data_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV40ContratoArquivosAnexos_Data_To2 = context.localUtil.CToT( cgiGet( edtavContratoarquivosanexos_data_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoArquivosAnexos_Data_To2", context.localUtil.TToC( AV40ContratoArquivosAnexos_Data_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV46TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
            AV47TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
            AV50TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratada_PessoaNom", AV50TFContratada_PessoaNom);
            AV51TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom_Sel", AV51TFContratada_PessoaNom_Sel);
            AV54TFContratada_PessoaCNPJ = cgiGet( edtavTfcontratada_pessoacnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratada_PessoaCNPJ", AV54TFContratada_PessoaCNPJ);
            AV55TFContratada_PessoaCNPJ_Sel = cgiGet( edtavTfcontratada_pessoacnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ_Sel", AV55TFContratada_PessoaCNPJ_Sel);
            AV58TFContratoArquivosAnexos_Descricao = cgiGet( edtavTfcontratoarquivosanexos_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContratoArquivosAnexos_Descricao", AV58TFContratoArquivosAnexos_Descricao);
            AV59TFContratoArquivosAnexos_Descricao_Sel = cgiGet( edtavTfcontratoarquivosanexos_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao_Sel", AV59TFContratoArquivosAnexos_Descricao_Sel);
            AV62TFContratoArquivosAnexos_NomeArq = cgiGet( edtavTfcontratoarquivosanexos_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratoArquivosAnexos_NomeArq", AV62TFContratoArquivosAnexos_NomeArq);
            AV63TFContratoArquivosAnexos_NomeArq_Sel = cgiGet( edtavTfcontratoarquivosanexos_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_NomeArq_Sel", AV63TFContratoArquivosAnexos_NomeArq_Sel);
            AV66TFContratoArquivosAnexos_TipoArq = cgiGet( edtavTfcontratoarquivosanexos_tipoarq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoArquivosAnexos_TipoArq", AV66TFContratoArquivosAnexos_TipoArq);
            AV67TFContratoArquivosAnexos_TipoArq_Sel = cgiGet( edtavTfcontratoarquivosanexos_tipoarq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoArquivosAnexos_TipoArq_Sel", AV67TFContratoArquivosAnexos_TipoArq_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoarquivosanexos_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContrato Arquivos Anexos_Data"}), 1, "vTFCONTRATOARQUIVOSANEXOS_DATA");
               GX_FocusControl = edtavTfcontratoarquivosanexos_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV70TFContratoArquivosAnexos_Data = context.localUtil.CToT( cgiGet( edtavTfcontratoarquivosanexos_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoarquivosanexos_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContrato Arquivos Anexos_Data_To"}), 1, "vTFCONTRATOARQUIVOSANEXOS_DATA_TO");
               GX_FocusControl = edtavTfcontratoarquivosanexos_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV71TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( cgiGet( edtavTfcontratoarquivosanexos_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Arquivos Anexos_Data Aux Date"}), 1, "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratoarquivosanexos_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72DDO_ContratoArquivosAnexos_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContratoArquivosAnexos_DataAuxDate", context.localUtil.Format(AV72DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV72DDO_ContratoArquivosAnexos_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContratoArquivosAnexos_DataAuxDate", context.localUtil.Format(AV72DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Arquivos Anexos_Data Aux Date To"}), 1, "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73DDO_ContratoArquivosAnexos_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContratoArquivosAnexos_DataAuxDateTo", context.localUtil.Format(AV73DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV73DDO_ContratoArquivosAnexos_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContratoArquivosAnexos_DataAuxDateTo", context.localUtil.Format(AV73DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"));
            }
            AV48ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Contrato_NumeroTitleControlIdToReplace", AV48ddo_Contrato_NumeroTitleControlIdToReplace);
            AV52ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Contratada_PessoaNomTitleControlIdToReplace", AV52ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
            AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace", AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace);
            AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace", AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace);
            AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace = cgiGet( edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace", AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace);
            AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace", AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_87 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_87"), ",", "."));
            AV77GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV78GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratada_pessoanom_Caption = cgiGet( "DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratada_pessoacnpj_Caption = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Caption");
            Ddo_contratada_pessoacnpj_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Tooltip");
            Ddo_contratada_pessoacnpj_Cls = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cls");
            Ddo_contratada_pessoacnpj_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set");
            Ddo_contratada_pessoacnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set");
            Ddo_contratada_pessoacnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype");
            Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortasc"));
            Ddo_contratada_pessoacnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc"));
            Ddo_contratada_pessoacnpj_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus");
            Ddo_contratada_pessoacnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includefilter"));
            Ddo_contratada_pessoacnpj_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filtertype");
            Ddo_contratada_pessoacnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filterisrange"));
            Ddo_contratada_pessoacnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includedatalist"));
            Ddo_contratada_pessoacnpj_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalisttype");
            Ddo_contratada_pessoacnpj_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistproc");
            Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoacnpj_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortasc");
            Ddo_contratada_pessoacnpj_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortdsc");
            Ddo_contratada_pessoacnpj_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Loadingdata");
            Ddo_contratada_pessoacnpj_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter");
            Ddo_contratada_pessoacnpj_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound");
            Ddo_contratada_pessoacnpj_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext");
            Ddo_contratoarquivosanexos_descricao_Caption = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Caption");
            Ddo_contratoarquivosanexos_descricao_Tooltip = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Tooltip");
            Ddo_contratoarquivosanexos_descricao_Cls = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cls");
            Ddo_contratoarquivosanexos_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_set");
            Ddo_contratoarquivosanexos_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_set");
            Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoarquivosanexos_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortasc"));
            Ddo_contratoarquivosanexos_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortdsc"));
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortedstatus");
            Ddo_contratoarquivosanexos_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includefilter"));
            Ddo_contratoarquivosanexos_descricao_Filtertype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filtertype");
            Ddo_contratoarquivosanexos_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filterisrange"));
            Ddo_contratoarquivosanexos_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includedatalist"));
            Ddo_contratoarquivosanexos_descricao_Datalisttype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalisttype");
            Ddo_contratoarquivosanexos_descricao_Datalistproc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistproc");
            Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoarquivosanexos_descricao_Sortasc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortasc");
            Ddo_contratoarquivosanexos_descricao_Sortdsc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortdsc");
            Ddo_contratoarquivosanexos_descricao_Loadingdata = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Loadingdata");
            Ddo_contratoarquivosanexos_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cleanfilter");
            Ddo_contratoarquivosanexos_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Noresultsfound");
            Ddo_contratoarquivosanexos_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Searchbuttontext");
            Ddo_contratoarquivosanexos_nomearq_Caption = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Caption");
            Ddo_contratoarquivosanexos_nomearq_Tooltip = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Tooltip");
            Ddo_contratoarquivosanexos_nomearq_Cls = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Cls");
            Ddo_contratoarquivosanexos_nomearq_Filteredtext_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filteredtext_set");
            Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Selectedvalue_set");
            Ddo_contratoarquivosanexos_nomearq_Dropdownoptionstype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Dropdownoptionstype");
            Ddo_contratoarquivosanexos_nomearq_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_contratoarquivosanexos_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includesortasc"));
            Ddo_contratoarquivosanexos_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includesortdsc"));
            Ddo_contratoarquivosanexos_nomearq_Sortedstatus = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Sortedstatus");
            Ddo_contratoarquivosanexos_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includefilter"));
            Ddo_contratoarquivosanexos_nomearq_Filtertype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filtertype");
            Ddo_contratoarquivosanexos_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filterisrange"));
            Ddo_contratoarquivosanexos_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Includedatalist"));
            Ddo_contratoarquivosanexos_nomearq_Datalisttype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Datalisttype");
            Ddo_contratoarquivosanexos_nomearq_Datalistproc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Datalistproc");
            Ddo_contratoarquivosanexos_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoarquivosanexos_nomearq_Sortasc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Sortasc");
            Ddo_contratoarquivosanexos_nomearq_Sortdsc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Sortdsc");
            Ddo_contratoarquivosanexos_nomearq_Loadingdata = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Loadingdata");
            Ddo_contratoarquivosanexos_nomearq_Cleanfilter = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Cleanfilter");
            Ddo_contratoarquivosanexos_nomearq_Noresultsfound = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Noresultsfound");
            Ddo_contratoarquivosanexos_nomearq_Searchbuttontext = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Searchbuttontext");
            Ddo_contratoarquivosanexos_tipoarq_Caption = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Caption");
            Ddo_contratoarquivosanexos_tipoarq_Tooltip = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Tooltip");
            Ddo_contratoarquivosanexos_tipoarq_Cls = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Cls");
            Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filteredtext_set");
            Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Selectedvalue_set");
            Ddo_contratoarquivosanexos_tipoarq_Dropdownoptionstype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Dropdownoptionstype");
            Ddo_contratoarquivosanexos_tipoarq_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Titlecontrolidtoreplace");
            Ddo_contratoarquivosanexos_tipoarq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includesortasc"));
            Ddo_contratoarquivosanexos_tipoarq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includesortdsc"));
            Ddo_contratoarquivosanexos_tipoarq_Sortedstatus = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Sortedstatus");
            Ddo_contratoarquivosanexos_tipoarq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includefilter"));
            Ddo_contratoarquivosanexos_tipoarq_Filtertype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filtertype");
            Ddo_contratoarquivosanexos_tipoarq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filterisrange"));
            Ddo_contratoarquivosanexos_tipoarq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Includedatalist"));
            Ddo_contratoarquivosanexos_tipoarq_Datalisttype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Datalisttype");
            Ddo_contratoarquivosanexos_tipoarq_Datalistproc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Datalistproc");
            Ddo_contratoarquivosanexos_tipoarq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoarquivosanexos_tipoarq_Sortasc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Sortasc");
            Ddo_contratoarquivosanexos_tipoarq_Sortdsc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Sortdsc");
            Ddo_contratoarquivosanexos_tipoarq_Loadingdata = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Loadingdata");
            Ddo_contratoarquivosanexos_tipoarq_Cleanfilter = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Cleanfilter");
            Ddo_contratoarquivosanexos_tipoarq_Noresultsfound = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Noresultsfound");
            Ddo_contratoarquivosanexos_tipoarq_Searchbuttontext = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Searchbuttontext");
            Ddo_contratoarquivosanexos_data_Caption = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Caption");
            Ddo_contratoarquivosanexos_data_Tooltip = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Tooltip");
            Ddo_contratoarquivosanexos_data_Cls = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cls");
            Ddo_contratoarquivosanexos_data_Filteredtext_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_set");
            Ddo_contratoarquivosanexos_data_Filteredtextto_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_set");
            Ddo_contratoarquivosanexos_data_Dropdownoptionstype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Dropdownoptionstype");
            Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Titlecontrolidtoreplace");
            Ddo_contratoarquivosanexos_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortasc"));
            Ddo_contratoarquivosanexos_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortdsc"));
            Ddo_contratoarquivosanexos_data_Sortedstatus = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortedstatus");
            Ddo_contratoarquivosanexos_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includefilter"));
            Ddo_contratoarquivosanexos_data_Filtertype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filtertype");
            Ddo_contratoarquivosanexos_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filterisrange"));
            Ddo_contratoarquivosanexos_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includedatalist"));
            Ddo_contratoarquivosanexos_data_Sortasc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortasc");
            Ddo_contratoarquivosanexos_data_Sortdsc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortdsc");
            Ddo_contratoarquivosanexos_data_Cleanfilter = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cleanfilter");
            Ddo_contratoarquivosanexos_data_Rangefilterfrom = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterfrom");
            Ddo_contratoarquivosanexos_data_Rangefilterto = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterto");
            Ddo_contratoarquivosanexos_data_Searchbuttontext = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratada_pessoacnpj_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey");
            Ddo_contratada_pessoacnpj_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get");
            Ddo_contratada_pessoacnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get");
            Ddo_contratoarquivosanexos_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Activeeventkey");
            Ddo_contratoarquivosanexos_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_get");
            Ddo_contratoarquivosanexos_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_get");
            Ddo_contratoarquivosanexos_nomearq_Activeeventkey = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Activeeventkey");
            Ddo_contratoarquivosanexos_nomearq_Filteredtext_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Filteredtext_get");
            Ddo_contratoarquivosanexos_nomearq_Selectedvalue_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ_Selectedvalue_get");
            Ddo_contratoarquivosanexos_tipoarq_Activeeventkey = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Activeeventkey");
            Ddo_contratoarquivosanexos_tipoarq_Filteredtext_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Filteredtext_get");
            Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ_Selectedvalue_get");
            Ddo_contratoarquivosanexos_data_Activeeventkey = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Activeeventkey");
            Ddo_contratoarquivosanexos_data_Filteredtext_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_get");
            Ddo_contratoarquivosanexos_data_Filteredtextto_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO1"), AV17ContratoArquivosAnexos_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA1"), 0) != AV37ContratoArquivosAnexos_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA_TO1"), 0) != AV38ContratoArquivosAnexos_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO2"), AV21ContratoArquivosAnexos_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA2"), 0) != AV39ContratoArquivosAnexos_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DATA_TO2"), 0) != AV40ContratoArquivosAnexos_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV46TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV47TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV50TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV51TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV54TFContratada_PessoaCNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV55TFContratada_PessoaCNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO"), AV58TFContratoArquivosAnexos_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL"), AV59TFContratoArquivosAnexos_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_NOMEARQ"), AV62TFContratoArquivosAnexos_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL"), AV63TFContratoArquivosAnexos_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_TIPOARQ"), AV66TFContratoArquivosAnexos_TipoArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL"), AV67TFContratoArquivosAnexos_TipoArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA"), 0) != AV70TFContratoArquivosAnexos_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA_TO"), 0) != AV71TFContratoArquivosAnexos_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E276L2 */
         E276L2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E276L2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_sel_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_descricao_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_descricao_sel_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_nomearq_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_nomearq_sel_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_tipoarq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_tipoarq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_tipoarq_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_tipoarq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_tipoarq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_tipoarq_sel_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_data_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_data_to_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV48ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Contrato_NumeroTitleControlIdToReplace", AV48ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV52ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Contratada_PessoaNomTitleControlIdToReplace", AV52ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace);
         AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace = Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoArquivosAnexos_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace);
         AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace", AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoarquivosanexos_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoArquivosAnexos_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "TitleControlIdToReplace", Ddo_contratoarquivosanexos_nomearq_Titlecontrolidtoreplace);
         AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace = Ddo_contratoarquivosanexos_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace", AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace);
         edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoarquivosanexos_tipoarq_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoArquivosAnexos_TipoArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "TitleControlIdToReplace", Ddo_contratoarquivosanexos_tipoarq_Titlecontrolidtoreplace);
         AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace = Ddo_contratoarquivosanexos_tipoarq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace", AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace);
         edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoArquivosAnexos_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "TitleControlIdToReplace", Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace);
         AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace", AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace);
         edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contrato Arquivos Anexos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o", 0);
         cmbavOrderedby.addItem("2", "N� Contrato", 0);
         cmbavOrderedby.addItem("3", "Contratada", 0);
         cmbavOrderedby.addItem("4", "CNPJ", 0);
         cmbavOrderedby.addItem("5", "Nome", 0);
         cmbavOrderedby.addItem("6", "Tipo", 0);
         cmbavOrderedby.addItem("7", "Data", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV75DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV75DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E286L2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV45Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57ContratoArquivosAnexos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61ContratoArquivosAnexos_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65ContratoArquivosAnexos_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoArquivosAnexos_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV48ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CNPJ", AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         edtContratoArquivosAnexos_Descricao_Titleformat = 2;
         edtContratoArquivosAnexos_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Descricao_Internalname, "Title", edtContratoArquivosAnexos_Descricao_Title);
         edtContratoArquivosAnexos_NomeArq_Titleformat = 2;
         edtContratoArquivosAnexos_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_NomeArq_Internalname, "Title", edtContratoArquivosAnexos_NomeArq_Title);
         edtContratoArquivosAnexos_TipoArq_Titleformat = 2;
         edtContratoArquivosAnexos_TipoArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_TipoArq_Internalname, "Title", edtContratoArquivosAnexos_TipoArq_Title);
         edtContratoArquivosAnexos_Data_Titleformat = 2;
         edtContratoArquivosAnexos_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Data_Internalname, "Title", edtContratoArquivosAnexos_Data_Title);
         AV77GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77GridCurrentPage), 10, 0)));
         AV78GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78GridPageCount), 10, 0)));
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV17ContratoArquivosAnexos_Descricao1;
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV37ContratoArquivosAnexos_Data1;
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV38ContratoArquivosAnexos_Data_To1;
         AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV21ContratoArquivosAnexos_Descricao2;
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV39ContratoArquivosAnexos_Data2;
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV40ContratoArquivosAnexos_Data_To2;
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV50TFContratada_PessoaNom;
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV51TFContratada_PessoaNom_Sel;
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV54TFContratada_PessoaCNPJ;
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV55TFContratada_PessoaCNPJ_Sel;
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV58TFContratoArquivosAnexos_Descricao;
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV59TFContratoArquivosAnexos_Descricao_Sel;
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV62TFContratoArquivosAnexos_NomeArq;
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV63TFContratoArquivosAnexos_NomeArq_Sel;
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV66TFContratoArquivosAnexos_TipoArq;
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV67TFContratoArquivosAnexos_TipoArq_Sel;
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV70TFContratoArquivosAnexos_Data;
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV71TFContratoArquivosAnexos_Data_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45Contrato_NumeroTitleFilterData", AV45Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49Contratada_PessoaNomTitleFilterData", AV49Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53Contratada_PessoaCNPJTitleFilterData", AV53Contratada_PessoaCNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57ContratoArquivosAnexos_DescricaoTitleFilterData", AV57ContratoArquivosAnexos_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV61ContratoArquivosAnexos_NomeArqTitleFilterData", AV61ContratoArquivosAnexos_NomeArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65ContratoArquivosAnexos_TipoArqTitleFilterData", AV65ContratoArquivosAnexos_TipoArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69ContratoArquivosAnexos_DataTitleFilterData", AV69ContratoArquivosAnexos_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E116L2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV76PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV76PageToGo) ;
         }
      }

      protected void E126L2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
            AV47TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136L2( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratada_PessoaNom", AV50TFContratada_PessoaNom);
            AV51TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom_Sel", AV51TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146L2( )
      {
         /* Ddo_contratada_pessoacnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFContratada_PessoaCNPJ = Ddo_contratada_pessoacnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratada_PessoaCNPJ", AV54TFContratada_PessoaCNPJ);
            AV55TFContratada_PessoaCNPJ_Sel = Ddo_contratada_pessoacnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ_Sel", AV55TFContratada_PessoaCNPJ_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E156L2( )
      {
         /* Ddo_contratoarquivosanexos_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFContratoArquivosAnexos_Descricao = Ddo_contratoarquivosanexos_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContratoArquivosAnexos_Descricao", AV58TFContratoArquivosAnexos_Descricao);
            AV59TFContratoArquivosAnexos_Descricao_Sel = Ddo_contratoarquivosanexos_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao_Sel", AV59TFContratoArquivosAnexos_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E166L2( )
      {
         /* Ddo_contratoarquivosanexos_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_nomearq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_nomearq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_nomearq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_nomearq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFContratoArquivosAnexos_NomeArq = Ddo_contratoarquivosanexos_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratoArquivosAnexos_NomeArq", AV62TFContratoArquivosAnexos_NomeArq);
            AV63TFContratoArquivosAnexos_NomeArq_Sel = Ddo_contratoarquivosanexos_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_NomeArq_Sel", AV63TFContratoArquivosAnexos_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E176L2( )
      {
         /* Ddo_contratoarquivosanexos_tipoarq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_tipoarq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_tipoarq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_tipoarq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_tipoarq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_tipoarq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFContratoArquivosAnexos_TipoArq = Ddo_contratoarquivosanexos_tipoarq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoArquivosAnexos_TipoArq", AV66TFContratoArquivosAnexos_TipoArq);
            AV67TFContratoArquivosAnexos_TipoArq_Sel = Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoArquivosAnexos_TipoArq_Sel", AV67TFContratoArquivosAnexos_TipoArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E186L2( )
      {
         /* Ddo_contratoarquivosanexos_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFContratoArquivosAnexos_Data = context.localUtil.CToT( Ddo_contratoarquivosanexos_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            AV71TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( Ddo_contratoarquivosanexos_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV71TFContratoArquivosAnexos_Data_To) )
            {
               AV71TFContratoArquivosAnexos_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV71TFContratoArquivosAnexos_Data_To)), (short)(DateTimeUtil.Month( AV71TFContratoArquivosAnexos_Data_To)), (short)(DateTimeUtil.Day( AV71TFContratoArquivosAnexos_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E296L2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV117Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoarquivosanexos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A108ContratoArquivosAnexos_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV118Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoarquivosanexos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A108ContratoArquivosAnexos_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         edtContratada_PessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratoArquivosAnexos_Descricao_Link = formatLink("viewcontratoarquivosanexos.aspx") + "?" + UrlEncode("" +A108ContratoArquivosAnexos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 87;
         }
         sendrow_872( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_87_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(87, GridRow);
         }
      }

      protected void E196L2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E246L2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E206L2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E256L2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E216L2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV37ContratoArquivosAnexos_Data1, AV38ContratoArquivosAnexos_Data_To1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV39ContratoArquivosAnexos_Data2, AV40ContratoArquivosAnexos_Data_To2, AV18DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratada_PessoaNom, AV51TFContratada_PessoaNom_Sel, AV54TFContratada_PessoaCNPJ, AV55TFContratada_PessoaCNPJ_Sel, AV58TFContratoArquivosAnexos_Descricao, AV59TFContratoArquivosAnexos_Descricao_Sel, AV62TFContratoArquivosAnexos_NomeArq, AV63TFContratoArquivosAnexos_NomeArq_Sel, AV66TFContratoArquivosAnexos_TipoArq, AV67TFContratoArquivosAnexos_TipoArq_Sel, AV70TFContratoArquivosAnexos_Data, AV71TFContratoArquivosAnexos_Data_To, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV52ddo_Contratada_PessoaNomTitleControlIdToReplace, AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace, AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace, AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV119Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E266L2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E226L2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E236L2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoarquivosanexos.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         Ddo_contratoarquivosanexos_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
         Ddo_contratoarquivosanexos_nomearq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_nomearq_Sortedstatus);
         Ddo_contratoarquivosanexos_tipoarq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_tipoarq_Sortedstatus);
         Ddo_contratoarquivosanexos_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratada_pessoacnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratoarquivosanexos_nomearq_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_nomearq_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratoarquivosanexos_tipoarq_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_tipoarq_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratoarquivosanexos_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoarquivosanexos_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
         {
            edtavContratoarquivosanexos_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoarquivosanexos_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
         {
            edtavContratoarquivosanexos_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ContratoArquivosAnexos_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV46TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV47TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV50TFContratada_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratada_PessoaNom", AV50TFContratada_PessoaNom);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
         AV51TFContratada_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom_Sel", AV51TFContratada_PessoaNom_Sel);
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
         AV54TFContratada_PessoaCNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratada_PessoaCNPJ", AV54TFContratada_PessoaCNPJ);
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
         AV55TFContratada_PessoaCNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ_Sel", AV55TFContratada_PessoaCNPJ_Sel);
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
         AV58TFContratoArquivosAnexos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContratoArquivosAnexos_Descricao", AV58TFContratoArquivosAnexos_Descricao);
         Ddo_contratoarquivosanexos_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_descricao_Filteredtext_set);
         AV59TFContratoArquivosAnexos_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao_Sel", AV59TFContratoArquivosAnexos_Descricao_Sel);
         Ddo_contratoarquivosanexos_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SelectedValue_set", Ddo_contratoarquivosanexos_descricao_Selectedvalue_set);
         AV62TFContratoArquivosAnexos_NomeArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratoArquivosAnexos_NomeArq", AV62TFContratoArquivosAnexos_NomeArq);
         Ddo_contratoarquivosanexos_nomearq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_nomearq_Filteredtext_set);
         AV63TFContratoArquivosAnexos_NomeArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_NomeArq_Sel", AV63TFContratoArquivosAnexos_NomeArq_Sel);
         Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "SelectedValue_set", Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set);
         AV66TFContratoArquivosAnexos_TipoArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoArquivosAnexos_TipoArq", AV66TFContratoArquivosAnexos_TipoArq);
         Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set);
         AV67TFContratoArquivosAnexos_TipoArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoArquivosAnexos_TipoArq_Sel", AV67TFContratoArquivosAnexos_TipoArq_Sel);
         Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "SelectedValue_set", Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set);
         AV70TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contratoarquivosanexos_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_data_Filteredtext_set);
         AV71TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contratoarquivosanexos_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "FilteredTextTo_set", Ddo_contratoarquivosanexos_data_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ContratoArquivosAnexos_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV119Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV119Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV119Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV120GXV1 = 1;
         while ( AV120GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV120GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV46TFContrato_Numero = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContrato_Numero)) )
               {
                  Ddo_contrato_numero_Filteredtext_set = AV46TFContrato_Numero;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV47TFContrato_Numero_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFContrato_Numero_Sel)) )
               {
                  Ddo_contrato_numero_Selectedvalue_set = AV47TFContrato_Numero_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV50TFContratada_PessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratada_PessoaNom", AV50TFContratada_PessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratada_PessoaNom)) )
               {
                  Ddo_contratada_pessoanom_Filteredtext_set = AV50TFContratada_PessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV51TFContratada_PessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom_Sel", AV51TFContratada_PessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom_Sel)) )
               {
                  Ddo_contratada_pessoanom_Selectedvalue_set = AV51TFContratada_PessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV54TFContratada_PessoaCNPJ = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratada_PessoaCNPJ", AV54TFContratada_PessoaCNPJ);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFContratada_PessoaCNPJ)) )
               {
                  Ddo_contratada_pessoacnpj_Filteredtext_set = AV54TFContratada_PessoaCNPJ;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV55TFContratada_PessoaCNPJ_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ_Sel", AV55TFContratada_PessoaCNPJ_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ_Sel)) )
               {
                  Ddo_contratada_pessoacnpj_Selectedvalue_set = AV55TFContratada_PessoaCNPJ_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
            {
               AV58TFContratoArquivosAnexos_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFContratoArquivosAnexos_Descricao", AV58TFContratoArquivosAnexos_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFContratoArquivosAnexos_Descricao)) )
               {
                  Ddo_contratoarquivosanexos_descricao_Filteredtext_set = AV58TFContratoArquivosAnexos_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL") == 0 )
            {
               AV59TFContratoArquivosAnexos_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao_Sel", AV59TFContratoArquivosAnexos_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoArquivosAnexos_Descricao_Sel)) )
               {
                  Ddo_contratoarquivosanexos_descricao_Selectedvalue_set = AV59TFContratoArquivosAnexos_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SelectedValue_set", Ddo_contratoarquivosanexos_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_NOMEARQ") == 0 )
            {
               AV62TFContratoArquivosAnexos_NomeArq = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratoArquivosAnexos_NomeArq", AV62TFContratoArquivosAnexos_NomeArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFContratoArquivosAnexos_NomeArq)) )
               {
                  Ddo_contratoarquivosanexos_nomearq_Filteredtext_set = AV62TFContratoArquivosAnexos_NomeArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_nomearq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL") == 0 )
            {
               AV63TFContratoArquivosAnexos_NomeArq_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_NomeArq_Sel", AV63TFContratoArquivosAnexos_NomeArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFContratoArquivosAnexos_NomeArq_Sel)) )
               {
                  Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set = AV63TFContratoArquivosAnexos_NomeArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_nomearq_Internalname, "SelectedValue_set", Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_TIPOARQ") == 0 )
            {
               AV66TFContratoArquivosAnexos_TipoArq = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoArquivosAnexos_TipoArq", AV66TFContratoArquivosAnexos_TipoArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratoArquivosAnexos_TipoArq)) )
               {
                  Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set = AV66TFContratoArquivosAnexos_TipoArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL") == 0 )
            {
               AV67TFContratoArquivosAnexos_TipoArq_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoArquivosAnexos_TipoArq_Sel", AV67TFContratoArquivosAnexos_TipoArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoArquivosAnexos_TipoArq_Sel)) )
               {
                  Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set = AV67TFContratoArquivosAnexos_TipoArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_tipoarq_Internalname, "SelectedValue_set", Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DATA") == 0 )
            {
               AV70TFContratoArquivosAnexos_Data = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
               AV71TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV70TFContratoArquivosAnexos_Data) )
               {
                  AV72DDO_ContratoArquivosAnexos_DataAuxDate = DateTimeUtil.ResetTime(AV70TFContratoArquivosAnexos_Data);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContratoArquivosAnexos_DataAuxDate", context.localUtil.Format(AV72DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"));
                  Ddo_contratoarquivosanexos_data_Filteredtext_set = context.localUtil.DToC( AV72DDO_ContratoArquivosAnexos_DataAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV71TFContratoArquivosAnexos_Data_To) )
               {
                  AV73DDO_ContratoArquivosAnexos_DataAuxDateTo = DateTimeUtil.ResetTime(AV71TFContratoArquivosAnexos_Data_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContratoArquivosAnexos_DataAuxDateTo", context.localUtil.Format(AV73DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"));
                  Ddo_contratoarquivosanexos_data_Filteredtextto_set = context.localUtil.DToC( AV73DDO_ContratoArquivosAnexos_DataAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "FilteredTextTo_set", Ddo_contratoarquivosanexos_data_Filteredtextto_set);
               }
            }
            AV120GXV1 = (int)(AV120GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoArquivosAnexos_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 )
            {
               AV37ContratoArquivosAnexos_Data1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoArquivosAnexos_Data1", context.localUtil.TToC( AV37ContratoArquivosAnexos_Data1, 8, 5, 0, 3, "/", ":", " "));
               AV38ContratoArquivosAnexos_Data_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoArquivosAnexos_Data_To1", context.localUtil.TToC( AV38ContratoArquivosAnexos_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ContratoArquivosAnexos_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 )
               {
                  AV39ContratoArquivosAnexos_Data2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoArquivosAnexos_Data2", context.localUtil.TToC( AV39ContratoArquivosAnexos_Data2, 8, 5, 0, 3, "/", ":", " "));
                  AV40ContratoArquivosAnexos_Data_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoArquivosAnexos_Data_To2", context.localUtil.TToC( AV40ContratoArquivosAnexos_Data_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV119Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratada_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFContratada_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFContratada_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFContratada_PessoaCNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV54TFContratada_PessoaCNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFContratada_PessoaCNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFContratoArquivosAnexos_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV58TFContratoArquivosAnexos_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV59TFContratoArquivosAnexos_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFContratoArquivosAnexos_NomeArq)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_NOMEARQ";
            AV11GridStateFilterValue.gxTpr_Value = AV62TFContratoArquivosAnexos_NomeArq;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFContratoArquivosAnexos_NomeArq_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFContratoArquivosAnexos_NomeArq_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratoArquivosAnexos_TipoArq)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_TIPOARQ";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFContratoArquivosAnexos_TipoArq;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoArquivosAnexos_TipoArq_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV67TFContratoArquivosAnexos_TipoArq_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV70TFContratoArquivosAnexos_Data) && (DateTime.MinValue==AV71TFContratoArquivosAnexos_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV70TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV71TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV119Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ContratoArquivosAnexos_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ! ( (DateTime.MinValue==AV37ContratoArquivosAnexos_Data1) && (DateTime.MinValue==AV38ContratoArquivosAnexos_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV37ContratoArquivosAnexos_Data1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV38ContratoArquivosAnexos_Data_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ContratoArquivosAnexos_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ! ( (DateTime.MinValue==AV39ContratoArquivosAnexos_Data2) && (DateTime.MinValue==AV40ContratoArquivosAnexos_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV39ContratoArquivosAnexos_Data2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV40ContratoArquivosAnexos_Data_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV119Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoArquivosAnexos";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_6L2( true) ;
         }
         else
         {
            wb_table2_8_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_81_6L2( true) ;
         }
         else
         {
            wb_table3_81_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table3_81_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6L2e( true) ;
         }
         else
         {
            wb_table1_2_6L2e( false) ;
         }
      }

      protected void wb_table3_81_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_84_6L2( true) ;
         }
         else
         {
            wb_table4_84_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table4_84_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_81_6L2e( true) ;
         }
         else
         {
            wb_table3_81_6L2e( false) ;
         }
      }

      protected void wb_table4_84_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"87\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoArquivosAnexos_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoArquivosAnexos_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoArquivosAnexos_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoArquivosAnexos_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoArquivosAnexos_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoArquivosAnexos_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoArquivosAnexos_TipoArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoArquivosAnexos_TipoArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoArquivosAnexos_TipoArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoArquivosAnexos_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoArquivosAnexos_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoArquivosAnexos_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratada_PessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A110ContratoArquivosAnexos_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoArquivosAnexos_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoArquivosAnexos_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoArquivosAnexos_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoArquivosAnexos_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoArquivosAnexos_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoArquivosAnexos_TipoArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoArquivosAnexos_TipoArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoArquivosAnexos_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoArquivosAnexos_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 87 )
         {
            wbEnd = 0;
            nRC_GXsfl_87 = (short)(nGXsfl_87_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_84_6L2e( true) ;
         }
         else
         {
            wb_table4_84_6L2e( false) ;
         }
      }

      protected void wb_table2_8_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoarquivosanexostitle_Internalname, "Arquivos Anexos do Contrato ", "", "", lblContratoarquivosanexostitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_6L2( true) ;
         }
         else
         {
            wb_table5_13_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoArquivosAnexos.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_6L2( true) ;
         }
         else
         {
            wb_table6_23_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_6L2e( true) ;
         }
         else
         {
            wb_table2_8_6L2e( false) ;
         }
      }

      protected void wb_table6_23_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_6L2( true) ;
         }
         else
         {
            wb_table7_28_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_6L2e( true) ;
         }
         else
         {
            wb_table6_23_6L2e( false) ;
         }
      }

      protected void wb_table7_28_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratoArquivosAnexos.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_6L2( true) ;
         }
         else
         {
            wb_table8_37_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWContratoArquivosAnexos.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_6L2( true) ;
         }
         else
         {
            wb_table9_62_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_6L2e( true) ;
         }
         else
         {
            wb_table7_28_6L2e( false) ;
         }
      }

      protected void wb_table9_62_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_WWContratoArquivosAnexos.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoarquivosanexos_descricao2_Internalname, AV21ContratoArquivosAnexos_Descricao2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", 0, edtavContratoarquivosanexos_descricao2_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            wb_table10_68_6L2( true) ;
         }
         else
         {
            wb_table10_68_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table10_68_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_6L2e( true) ;
         }
         else
         {
            wb_table9_62_6L2e( false) ;
         }
      }

      protected void wb_table10_68_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Internalname, tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoarquivosanexos_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoarquivosanexos_data2_Internalname, context.localUtil.TToC( AV39ContratoArquivosAnexos_Data2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV39ContratoArquivosAnexos_Data2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoarquivosanexos_data2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavContratoarquivosanexos_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoarquivosanexos_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoarquivosanexos_data_to2_Internalname, context.localUtil.TToC( AV40ContratoArquivosAnexos_Data_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV40ContratoArquivosAnexos_Data_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoarquivosanexos_data_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavContratoarquivosanexos_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_68_6L2e( true) ;
         }
         else
         {
            wb_table10_68_6L2e( false) ;
         }
      }

      protected void wb_table8_37_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContratoArquivosAnexos.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoarquivosanexos_descricao1_Internalname, AV17ContratoArquivosAnexos_Descricao1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavContratoarquivosanexos_descricao1_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_WWContratoArquivosAnexos.htm");
            wb_table11_43_6L2( true) ;
         }
         else
         {
            wb_table11_43_6L2( false) ;
         }
         return  ;
      }

      protected void wb_table11_43_6L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_6L2e( true) ;
         }
         else
         {
            wb_table8_37_6L2e( false) ;
         }
      }

      protected void wb_table11_43_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Internalname, tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoarquivosanexos_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoarquivosanexos_data1_Internalname, context.localUtil.TToC( AV37ContratoArquivosAnexos_Data1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV37ContratoArquivosAnexos_Data1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoarquivosanexos_data1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavContratoarquivosanexos_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoarquivosanexos_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoarquivosanexos_data_to1_Internalname, context.localUtil.TToC( AV38ContratoArquivosAnexos_Data_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV38ContratoArquivosAnexos_Data_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoarquivosanexos_data_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavContratoarquivosanexos_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_43_6L2e( true) ;
         }
         else
         {
            wb_table11_43_6L2e( false) ;
         }
      }

      protected void wb_table5_13_6L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_6L2e( true) ;
         }
         else
         {
            wb_table5_13_6L2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6L2( ) ;
         WS6L2( ) ;
         WE6L2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117354141");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratoarquivosanexos.js", "?20203117354142");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_872( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_87_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_87_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_87_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_87_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_87_idx;
         edtContratoArquivosAnexos_Descricao_Internalname = "CONTRATOARQUIVOSANEXOS_DESCRICAO_"+sGXsfl_87_idx;
         edtContratoArquivosAnexos_NomeArq_Internalname = "CONTRATOARQUIVOSANEXOS_NOMEARQ_"+sGXsfl_87_idx;
         edtContratoArquivosAnexos_TipoArq_Internalname = "CONTRATOARQUIVOSANEXOS_TIPOARQ_"+sGXsfl_87_idx;
         edtContratoArquivosAnexos_Data_Internalname = "CONTRATOARQUIVOSANEXOS_DATA_"+sGXsfl_87_idx;
      }

      protected void SubsflControlProps_fel_872( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_87_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_87_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_87_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_87_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_87_fel_idx;
         edtContratoArquivosAnexos_Descricao_Internalname = "CONTRATOARQUIVOSANEXOS_DESCRICAO_"+sGXsfl_87_fel_idx;
         edtContratoArquivosAnexos_NomeArq_Internalname = "CONTRATOARQUIVOSANEXOS_NOMEARQ_"+sGXsfl_87_fel_idx;
         edtContratoArquivosAnexos_TipoArq_Internalname = "CONTRATOARQUIVOSANEXOS_TIPOARQ_"+sGXsfl_87_fel_idx;
         edtContratoArquivosAnexos_Data_Internalname = "CONTRATOARQUIVOSANEXOS_DATA_"+sGXsfl_87_fel_idx;
      }

      protected void sendrow_872( )
      {
         SubsflControlProps_872( ) ;
         WB6L0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_87_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_87_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_87_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV117Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV117Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV118Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV118Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratada_PessoaNom_Link,(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_Descricao_Internalname,(String)A110ContratoArquivosAnexos_Descricao,(String)A110ContratoArquivosAnexos_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoArquivosAnexos_Descricao_Link,(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)87,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_NomeArq_Internalname,StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_TipoArq_Internalname,StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_TipoArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_Data_Internalname,context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A113ContratoArquivosAnexos_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DESCRICAO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, A110ContratoArquivosAnexos_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_NOMEARQ"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, StringUtil.RTrim( context.localUtil.Format( A112ContratoArquivosAnexos_NomeArq, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_TIPOARQ"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, StringUtil.RTrim( context.localUtil.Format( A109ContratoArquivosAnexos_TipoArq, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DATA"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, context.localUtil.Format( A113ContratoArquivosAnexos_Data, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         /* End function sendrow_872 */
      }

      protected void init_default_properties( )
      {
         lblContratoarquivosanexostitle_Internalname = "CONTRATOARQUIVOSANEXOSTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoarquivosanexos_descricao1_Internalname = "vCONTRATOARQUIVOSANEXOS_DESCRICAO1";
         edtavContratoarquivosanexos_data1_Internalname = "vCONTRATOARQUIVOSANEXOS_DATA1";
         lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA_RANGEMIDDLETEXT1";
         edtavContratoarquivosanexos_data_to1_Internalname = "vCONTRATOARQUIVOSANEXOS_DATA_TO1";
         tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoarquivosanexos_descricao2_Internalname = "vCONTRATOARQUIVOSANEXOS_DESCRICAO2";
         edtavContratoarquivosanexos_data2_Internalname = "vCONTRATOARQUIVOSANEXOS_DATA2";
         lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA_RANGEMIDDLETEXT2";
         edtavContratoarquivosanexos_data_to2_Internalname = "vCONTRATOARQUIVOSANEXOS_DATA_TO2";
         tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         edtContratoArquivosAnexos_Descricao_Internalname = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         edtContratoArquivosAnexos_NomeArq_Internalname = "CONTRATOARQUIVOSANEXOS_NOMEARQ";
         edtContratoArquivosAnexos_TipoArq_Internalname = "CONTRATOARQUIVOSANEXOS_TIPOARQ";
         edtContratoArquivosAnexos_Data_Internalname = "CONTRATOARQUIVOSANEXOS_DATA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratada_pessoanom_Internalname = "vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = "vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratada_pessoacnpj_Internalname = "vTFCONTRATADA_PESSOACNPJ";
         edtavTfcontratada_pessoacnpj_sel_Internalname = "vTFCONTRATADA_PESSOACNPJ_SEL";
         edtavTfcontratoarquivosanexos_descricao_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DESCRICAO";
         edtavTfcontratoarquivosanexos_descricao_sel_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL";
         edtavTfcontratoarquivosanexos_nomearq_Internalname = "vTFCONTRATOARQUIVOSANEXOS_NOMEARQ";
         edtavTfcontratoarquivosanexos_nomearq_sel_Internalname = "vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL";
         edtavTfcontratoarquivosanexos_tipoarq_Internalname = "vTFCONTRATOARQUIVOSANEXOS_TIPOARQ";
         edtavTfcontratoarquivosanexos_tipoarq_sel_Internalname = "vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL";
         edtavTfcontratoarquivosanexos_data_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DATA";
         edtavTfcontratoarquivosanexos_data_to_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DATA_TO";
         edtavDdo_contratoarquivosanexos_dataauxdate_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATE";
         edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATETO";
         divDdo_contratoarquivosanexos_dataauxdates_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATES";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoanom_Internalname = "DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacnpj_Internalname = "DDO_CONTRATADA_PESSOACNPJ";
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE";
         Ddo_contratoarquivosanexos_descricao_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO";
         edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratoarquivosanexos_nomearq_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ";
         edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE";
         Ddo_contratoarquivosanexos_tipoarq_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ";
         edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE";
         Ddo_contratoarquivosanexos_data_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_DATA";
         edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoArquivosAnexos_Data_Jsonclick = "";
         edtContratoArquivosAnexos_TipoArq_Jsonclick = "";
         edtContratoArquivosAnexos_NomeArq_Jsonclick = "";
         edtContratoArquivosAnexos_Descricao_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtavContratoarquivosanexos_data_to1_Jsonclick = "";
         edtavContratoarquivosanexos_data1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoarquivosanexos_data_to2_Jsonclick = "";
         edtavContratoarquivosanexos_data2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoArquivosAnexos_Descricao_Link = "";
         edtContratada_PessoaNom_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoArquivosAnexos_Data_Titleformat = 0;
         edtContratoArquivosAnexos_TipoArq_Titleformat = 0;
         edtContratoArquivosAnexos_NomeArq_Titleformat = 0;
         edtContratoArquivosAnexos_Descricao_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible = 1;
         edtavContratoarquivosanexos_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible = 1;
         edtavContratoarquivosanexos_descricao1_Visible = 1;
         edtContratoArquivosAnexos_Data_Title = "Data";
         edtContratoArquivosAnexos_TipoArq_Title = "Tipo";
         edtContratoArquivosAnexos_NomeArq_Title = "Nome";
         edtContratoArquivosAnexos_Descricao_Title = "Descri��o";
         edtContratada_PessoaCNPJ_Title = "CNPJ";
         edtContratada_PessoaNom_Title = "Contratada";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoarquivosanexos_dataauxdateto_Jsonclick = "";
         edtavDdo_contratoarquivosanexos_dataauxdate_Jsonclick = "";
         edtavTfcontratoarquivosanexos_data_to_Jsonclick = "";
         edtavTfcontratoarquivosanexos_data_to_Visible = 1;
         edtavTfcontratoarquivosanexos_data_Jsonclick = "";
         edtavTfcontratoarquivosanexos_data_Visible = 1;
         edtavTfcontratoarquivosanexos_tipoarq_sel_Jsonclick = "";
         edtavTfcontratoarquivosanexos_tipoarq_sel_Visible = 1;
         edtavTfcontratoarquivosanexos_tipoarq_Jsonclick = "";
         edtavTfcontratoarquivosanexos_tipoarq_Visible = 1;
         edtavTfcontratoarquivosanexos_nomearq_sel_Jsonclick = "";
         edtavTfcontratoarquivosanexos_nomearq_sel_Visible = 1;
         edtavTfcontratoarquivosanexos_nomearq_Jsonclick = "";
         edtavTfcontratoarquivosanexos_nomearq_Visible = 1;
         edtavTfcontratoarquivosanexos_descricao_sel_Visible = 1;
         edtavTfcontratoarquivosanexos_descricao_Visible = 1;
         edtavTfcontratada_pessoacnpj_sel_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_sel_Visible = 1;
         edtavTfcontratada_pessoacnpj_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoarquivosanexos_data_Searchbuttontext = "Pesquisar";
         Ddo_contratoarquivosanexos_data_Rangefilterto = "At�";
         Ddo_contratoarquivosanexos_data_Rangefilterfrom = "Desde";
         Ddo_contratoarquivosanexos_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoarquivosanexos_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoarquivosanexos_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratoarquivosanexos_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoarquivosanexos_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Filtertype = "Date";
         Ddo_contratoarquivosanexos_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace = "";
         Ddo_contratoarquivosanexos_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoarquivosanexos_data_Cls = "ColumnSettings";
         Ddo_contratoarquivosanexos_data_Tooltip = "Op��es";
         Ddo_contratoarquivosanexos_data_Caption = "";
         Ddo_contratoarquivosanexos_tipoarq_Searchbuttontext = "Pesquisar";
         Ddo_contratoarquivosanexos_tipoarq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoarquivosanexos_tipoarq_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoarquivosanexos_tipoarq_Loadingdata = "Carregando dados...";
         Ddo_contratoarquivosanexos_tipoarq_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoarquivosanexos_tipoarq_Sortasc = "Ordenar de A � Z";
         Ddo_contratoarquivosanexos_tipoarq_Datalistupdateminimumcharacters = 0;
         Ddo_contratoarquivosanexos_tipoarq_Datalistproc = "GetWWContratoArquivosAnexosFilterData";
         Ddo_contratoarquivosanexos_tipoarq_Datalisttype = "Dynamic";
         Ddo_contratoarquivosanexos_tipoarq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_tipoarq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoarquivosanexos_tipoarq_Filtertype = "Character";
         Ddo_contratoarquivosanexos_tipoarq_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_tipoarq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_tipoarq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_tipoarq_Titlecontrolidtoreplace = "";
         Ddo_contratoarquivosanexos_tipoarq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoarquivosanexos_tipoarq_Cls = "ColumnSettings";
         Ddo_contratoarquivosanexos_tipoarq_Tooltip = "Op��es";
         Ddo_contratoarquivosanexos_tipoarq_Caption = "";
         Ddo_contratoarquivosanexos_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_contratoarquivosanexos_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoarquivosanexos_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoarquivosanexos_nomearq_Loadingdata = "Carregando dados...";
         Ddo_contratoarquivosanexos_nomearq_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoarquivosanexos_nomearq_Sortasc = "Ordenar de A � Z";
         Ddo_contratoarquivosanexos_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_contratoarquivosanexos_nomearq_Datalistproc = "GetWWContratoArquivosAnexosFilterData";
         Ddo_contratoarquivosanexos_nomearq_Datalisttype = "Dynamic";
         Ddo_contratoarquivosanexos_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoarquivosanexos_nomearq_Filtertype = "Character";
         Ddo_contratoarquivosanexos_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_nomearq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_nomearq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_nomearq_Titlecontrolidtoreplace = "";
         Ddo_contratoarquivosanexos_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoarquivosanexos_nomearq_Cls = "ColumnSettings";
         Ddo_contratoarquivosanexos_nomearq_Tooltip = "Op��es";
         Ddo_contratoarquivosanexos_nomearq_Caption = "";
         Ddo_contratoarquivosanexos_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoarquivosanexos_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoarquivosanexos_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoarquivosanexos_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoarquivosanexos_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoarquivosanexos_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoarquivosanexos_descricao_Datalistproc = "GetWWContratoArquivosAnexosFilterData";
         Ddo_contratoarquivosanexos_descricao_Datalisttype = "Dynamic";
         Ddo_contratoarquivosanexos_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoarquivosanexos_descricao_Filtertype = "Character";
         Ddo_contratoarquivosanexos_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoarquivosanexos_descricao_Cls = "ColumnSettings";
         Ddo_contratoarquivosanexos_descricao_Tooltip = "Op��es";
         Ddo_contratoarquivosanexos_descricao_Caption = "";
         Ddo_contratada_pessoacnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoacnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacnpj_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoacnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoacnpj_Datalistproc = "GetWWContratoArquivosAnexosFilterData";
         Ddo_contratada_pessoacnpj_Datalisttype = "Dynamic";
         Ddo_contratada_pessoacnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacnpj_Filtertype = "Character";
         Ddo_contratada_pessoacnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacnpj_Cls = "ColumnSettings";
         Ddo_contratada_pessoacnpj_Tooltip = "Op��es";
         Ddo_contratada_pessoacnpj_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetWWContratoArquivosAnexosFilterData";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetWWContratoArquivosAnexosFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contrato Arquivos Anexos";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV45Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV49Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV53Contratada_PessoaCNPJTitleFilterData',fld:'vCONTRATADA_PESSOACNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV57ContratoArquivosAnexos_DescricaoTitleFilterData',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV61ContratoArquivosAnexos_NomeArqTitleFilterData',fld:'vCONTRATOARQUIVOSANEXOS_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV65ContratoArquivosAnexos_TipoArqTitleFilterData',fld:'vCONTRATOARQUIVOSANEXOS_TIPOARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContratoArquivosAnexos_DataTitleFilterData',fld:'vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'edtContratoArquivosAnexos_Descricao_Titleformat',ctrl:'CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'Titleformat'},{av:'edtContratoArquivosAnexos_Descricao_Title',ctrl:'CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'Title'},{av:'edtContratoArquivosAnexos_NomeArq_Titleformat',ctrl:'CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'Titleformat'},{av:'edtContratoArquivosAnexos_NomeArq_Title',ctrl:'CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'Title'},{av:'edtContratoArquivosAnexos_TipoArq_Titleformat',ctrl:'CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'Titleformat'},{av:'edtContratoArquivosAnexos_TipoArq_Title',ctrl:'CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'Title'},{av:'edtContratoArquivosAnexos_Data_Titleformat',ctrl:'CONTRATOARQUIVOSANEXOS_DATA',prop:'Titleformat'},{av:'edtContratoArquivosAnexos_Data_Title',ctrl:'CONTRATOARQUIVOSANEXOS_DATA',prop:'Title'},{av:'AV77GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV78GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E126L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_nomearq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_tipoarq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E136L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_nomearq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_tipoarq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED","{handler:'E146L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoacnpj_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacnpj_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_nomearq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_tipoarq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO.ONOPTIONCLICKED","{handler:'E156L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_descricao_Activeeventkey',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoarquivosanexos_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoarquivosanexos_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_nomearq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_tipoarq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ.ONOPTIONCLICKED","{handler:'E166L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_nomearq_Activeeventkey',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_contratoarquivosanexos_nomearq_Filteredtext_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_contratoarquivosanexos_nomearq_Selectedvalue_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_nomearq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SortedStatus'},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_tipoarq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ.ONOPTIONCLICKED","{handler:'E176L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_tipoarq_Activeeventkey',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'ActiveEventKey'},{av:'Ddo_contratoarquivosanexos_tipoarq_Filteredtext_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'FilteredText_get'},{av:'Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_tipoarq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SortedStatus'},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_nomearq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOARQUIVOSANEXOS_DATA.ONOPTIONCLICKED","{handler:'E186L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_data_Activeeventkey',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratoarquivosanexos_data_Filteredtext_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredText_get'},{av:'Ddo_contratoarquivosanexos_data_Filteredtextto_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_nomearq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_tipoarq_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E296L2',iparms:[{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratada_PessoaNom_Link',ctrl:'CONTRATADA_PESSOANOM',prop:'Link'},{av:'edtContratoArquivosAnexos_Descricao_Link',ctrl:'CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E196L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E246L2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E206L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E256L2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E216L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E266L2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E226L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV119Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV50TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_set'},{av:'AV51TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_set'},{av:'AV54TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_set'},{av:'AV55TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_set'},{av:'AV58TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_descricao_Filteredtext_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'FilteredText_set'},{av:'AV59TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_descricao_Selectedvalue_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SelectedValue_set'},{av:'AV62TFContratoArquivosAnexos_NomeArq',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_nomearq_Filteredtext_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'FilteredText_set'},{av:'AV63TFContratoArquivosAnexos_NomeArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ',prop:'SelectedValue_set'},{av:'AV66TFContratoArquivosAnexos_TipoArq',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'FilteredText_set'},{av:'AV67TFContratoArquivosAnexos_TipoArq_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ',prop:'SelectedValue_set'},{av:'AV70TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contratoarquivosanexos_data_Filteredtext_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredText_set'},{av:'AV71TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contratoarquivosanexos_data_Filteredtextto_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV37ContratoArquivosAnexos_Data1',fld:'vCONTRATOARQUIVOSANEXOS_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV38ContratoArquivosAnexos_Data_To1',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV39ContratoArquivosAnexos_Data2',fld:'vCONTRATOARQUIVOSANEXOS_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV40ContratoArquivosAnexos_Data_To2',fld:'vCONTRATOARQUIVOSANEXOS_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOARQUIVOSANEXOS_DATA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E236L2',iparms:[{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratada_pessoacnpj_Activeeventkey = "";
         Ddo_contratada_pessoacnpj_Filteredtext_get = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_get = "";
         Ddo_contratoarquivosanexos_descricao_Activeeventkey = "";
         Ddo_contratoarquivosanexos_descricao_Filteredtext_get = "";
         Ddo_contratoarquivosanexos_descricao_Selectedvalue_get = "";
         Ddo_contratoarquivosanexos_nomearq_Activeeventkey = "";
         Ddo_contratoarquivosanexos_nomearq_Filteredtext_get = "";
         Ddo_contratoarquivosanexos_nomearq_Selectedvalue_get = "";
         Ddo_contratoarquivosanexos_tipoarq_Activeeventkey = "";
         Ddo_contratoarquivosanexos_tipoarq_Filteredtext_get = "";
         Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_get = "";
         Ddo_contratoarquivosanexos_data_Activeeventkey = "";
         Ddo_contratoarquivosanexos_data_Filteredtext_get = "";
         Ddo_contratoarquivosanexos_data_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratoArquivosAnexos_Descricao1 = "";
         AV37ContratoArquivosAnexos_Data1 = (DateTime)(DateTime.MinValue);
         AV38ContratoArquivosAnexos_Data_To1 = (DateTime)(DateTime.MinValue);
         AV19DynamicFiltersSelector2 = "";
         AV21ContratoArquivosAnexos_Descricao2 = "";
         AV39ContratoArquivosAnexos_Data2 = (DateTime)(DateTime.MinValue);
         AV40ContratoArquivosAnexos_Data_To2 = (DateTime)(DateTime.MinValue);
         AV46TFContrato_Numero = "";
         AV47TFContrato_Numero_Sel = "";
         AV50TFContratada_PessoaNom = "";
         AV51TFContratada_PessoaNom_Sel = "";
         AV54TFContratada_PessoaCNPJ = "";
         AV55TFContratada_PessoaCNPJ_Sel = "";
         AV58TFContratoArquivosAnexos_Descricao = "";
         AV59TFContratoArquivosAnexos_Descricao_Sel = "";
         AV62TFContratoArquivosAnexos_NomeArq = "";
         AV63TFContratoArquivosAnexos_NomeArq_Sel = "";
         AV66TFContratoArquivosAnexos_TipoArq = "";
         AV67TFContratoArquivosAnexos_TipoArq_Sel = "";
         AV70TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         AV71TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
         AV48ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV52ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace = "";
         AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = "";
         AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace = "";
         AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace = "";
         AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = "";
         AV119Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV75DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV45Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57ContratoArquivosAnexos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61ContratoArquivosAnexos_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65ContratoArquivosAnexos_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoArquivosAnexos_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         Ddo_contratoarquivosanexos_descricao_Filteredtext_set = "";
         Ddo_contratoarquivosanexos_descricao_Selectedvalue_set = "";
         Ddo_contratoarquivosanexos_descricao_Sortedstatus = "";
         Ddo_contratoarquivosanexos_nomearq_Filteredtext_set = "";
         Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set = "";
         Ddo_contratoarquivosanexos_nomearq_Sortedstatus = "";
         Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set = "";
         Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set = "";
         Ddo_contratoarquivosanexos_tipoarq_Sortedstatus = "";
         Ddo_contratoarquivosanexos_data_Filteredtext_set = "";
         Ddo_contratoarquivosanexos_data_Filteredtextto_set = "";
         Ddo_contratoarquivosanexos_data_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV72DDO_ContratoArquivosAnexos_DataAuxDate = DateTime.MinValue;
         AV73DDO_ContratoArquivosAnexos_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV117Update_GXI = "";
         AV29Delete = "";
         AV118Delete_GXI = "";
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A110ContratoArquivosAnexos_Descricao = "";
         A112ContratoArquivosAnexos_NomeArq = "";
         A109ContratoArquivosAnexos_TipoArq = "";
         A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = "";
         lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = "";
         lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = "";
         lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = "";
         lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = "";
         lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = "";
         lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = "";
         lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = "";
         AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = "";
         AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = "";
         AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = (DateTime)(DateTime.MinValue);
         AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = (DateTime)(DateTime.MinValue);
         AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = "";
         AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = "";
         AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = (DateTime)(DateTime.MinValue);
         AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = (DateTime)(DateTime.MinValue);
         AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = "";
         AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero = "";
         AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = "";
         AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = "";
         AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = "";
         AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = "";
         AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = "";
         AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = "";
         AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = "";
         AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = "";
         AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = "";
         AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = "";
         AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = (DateTime)(DateTime.MinValue);
         AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = (DateTime)(DateTime.MinValue);
         H006L2_A40Contratada_PessoaCod = new int[1] ;
         H006L2_A74Contrato_Codigo = new int[1] ;
         H006L2_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         H006L2_A39Contratada_Codigo = new int[1] ;
         H006L2_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         H006L2_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         H006L2_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         H006L2_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         H006L2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006L2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006L2_A41Contratada_PessoaNom = new String[] {""} ;
         H006L2_n41Contratada_PessoaNom = new bool[] {false} ;
         H006L2_A77Contrato_Numero = new String[] {""} ;
         H006L3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoarquivosanexostitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoarquivosanexos__default(),
            new Object[][] {
                new Object[] {
               H006L2_A40Contratada_PessoaCod, H006L2_A74Contrato_Codigo, H006L2_A108ContratoArquivosAnexos_Codigo, H006L2_A39Contratada_Codigo, H006L2_A113ContratoArquivosAnexos_Data, H006L2_A109ContratoArquivosAnexos_TipoArq, H006L2_A112ContratoArquivosAnexos_NomeArq, H006L2_A110ContratoArquivosAnexos_Descricao, H006L2_A42Contratada_PessoaCNPJ, H006L2_n42Contratada_PessoaCNPJ,
               H006L2_A41Contratada_PessoaNom, H006L2_n41Contratada_PessoaNom, H006L2_A77Contrato_Numero
               }
               , new Object[] {
               H006L3_AGRID_nRecordCount
               }
            }
         );
         AV119Pgmname = "WWContratoArquivosAnexos";
         /* GeneXus formulas. */
         AV119Pgmname = "WWContratoArquivosAnexos";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_87 ;
      private short nGXsfl_87_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_87_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ;
      private short AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short edtContratoArquivosAnexos_Descricao_Titleformat ;
      private short edtContratoArquivosAnexos_NomeArq_Titleformat ;
      private short edtContratoArquivosAnexos_TipoArq_Titleformat ;
      private short edtContratoArquivosAnexos_Data_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A108ContratoArquivosAnexos_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters ;
      private int Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratoarquivosanexos_nomearq_Datalistupdateminimumcharacters ;
      private int Ddo_contratoarquivosanexos_tipoarq_Datalistupdateminimumcharacters ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratada_pessoacnpj_Visible ;
      private int edtavTfcontratada_pessoacnpj_sel_Visible ;
      private int edtavTfcontratoarquivosanexos_descricao_Visible ;
      private int edtavTfcontratoarquivosanexos_descricao_sel_Visible ;
      private int edtavTfcontratoarquivosanexos_nomearq_Visible ;
      private int edtavTfcontratoarquivosanexos_nomearq_sel_Visible ;
      private int edtavTfcontratoarquivosanexos_tipoarq_Visible ;
      private int edtavTfcontratoarquivosanexos_tipoarq_sel_Visible ;
      private int edtavTfcontratoarquivosanexos_data_Visible ;
      private int edtavTfcontratoarquivosanexos_data_to_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A40Contratada_PessoaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV76PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavContratoarquivosanexos_descricao1_Visible ;
      private int tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Visible ;
      private int edtavContratoarquivosanexos_descricao2_Visible ;
      private int tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Visible ;
      private int AV120GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV77GridCurrentPage ;
      private long AV78GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratada_pessoacnpj_Activeeventkey ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_get ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_get ;
      private String Ddo_contratoarquivosanexos_descricao_Activeeventkey ;
      private String Ddo_contratoarquivosanexos_descricao_Filteredtext_get ;
      private String Ddo_contratoarquivosanexos_descricao_Selectedvalue_get ;
      private String Ddo_contratoarquivosanexos_nomearq_Activeeventkey ;
      private String Ddo_contratoarquivosanexos_nomearq_Filteredtext_get ;
      private String Ddo_contratoarquivosanexos_nomearq_Selectedvalue_get ;
      private String Ddo_contratoarquivosanexos_tipoarq_Activeeventkey ;
      private String Ddo_contratoarquivosanexos_tipoarq_Filteredtext_get ;
      private String Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_get ;
      private String Ddo_contratoarquivosanexos_data_Activeeventkey ;
      private String Ddo_contratoarquivosanexos_data_Filteredtext_get ;
      private String Ddo_contratoarquivosanexos_data_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_87_idx="0001" ;
      private String AV46TFContrato_Numero ;
      private String AV47TFContrato_Numero_Sel ;
      private String AV50TFContratada_PessoaNom ;
      private String AV51TFContratada_PessoaNom_Sel ;
      private String AV62TFContratoArquivosAnexos_NomeArq ;
      private String AV63TFContratoArquivosAnexos_NomeArq_Sel ;
      private String AV66TFContratoArquivosAnexos_TipoArq ;
      private String AV67TFContratoArquivosAnexos_TipoArq_Sel ;
      private String AV119Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratada_pessoacnpj_Caption ;
      private String Ddo_contratada_pessoacnpj_Tooltip ;
      private String Ddo_contratada_pessoacnpj_Cls ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_set ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_set ;
      private String Ddo_contratada_pessoacnpj_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacnpj_Sortedstatus ;
      private String Ddo_contratada_pessoacnpj_Filtertype ;
      private String Ddo_contratada_pessoacnpj_Datalisttype ;
      private String Ddo_contratada_pessoacnpj_Datalistproc ;
      private String Ddo_contratada_pessoacnpj_Sortasc ;
      private String Ddo_contratada_pessoacnpj_Sortdsc ;
      private String Ddo_contratada_pessoacnpj_Loadingdata ;
      private String Ddo_contratada_pessoacnpj_Cleanfilter ;
      private String Ddo_contratada_pessoacnpj_Noresultsfound ;
      private String Ddo_contratada_pessoacnpj_Searchbuttontext ;
      private String Ddo_contratoarquivosanexos_descricao_Caption ;
      private String Ddo_contratoarquivosanexos_descricao_Tooltip ;
      private String Ddo_contratoarquivosanexos_descricao_Cls ;
      private String Ddo_contratoarquivosanexos_descricao_Filteredtext_set ;
      private String Ddo_contratoarquivosanexos_descricao_Selectedvalue_set ;
      private String Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype ;
      private String Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoarquivosanexos_descricao_Sortedstatus ;
      private String Ddo_contratoarquivosanexos_descricao_Filtertype ;
      private String Ddo_contratoarquivosanexos_descricao_Datalisttype ;
      private String Ddo_contratoarquivosanexos_descricao_Datalistproc ;
      private String Ddo_contratoarquivosanexos_descricao_Sortasc ;
      private String Ddo_contratoarquivosanexos_descricao_Sortdsc ;
      private String Ddo_contratoarquivosanexos_descricao_Loadingdata ;
      private String Ddo_contratoarquivosanexos_descricao_Cleanfilter ;
      private String Ddo_contratoarquivosanexos_descricao_Noresultsfound ;
      private String Ddo_contratoarquivosanexos_descricao_Searchbuttontext ;
      private String Ddo_contratoarquivosanexos_nomearq_Caption ;
      private String Ddo_contratoarquivosanexos_nomearq_Tooltip ;
      private String Ddo_contratoarquivosanexos_nomearq_Cls ;
      private String Ddo_contratoarquivosanexos_nomearq_Filteredtext_set ;
      private String Ddo_contratoarquivosanexos_nomearq_Selectedvalue_set ;
      private String Ddo_contratoarquivosanexos_nomearq_Dropdownoptionstype ;
      private String Ddo_contratoarquivosanexos_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_contratoarquivosanexos_nomearq_Sortedstatus ;
      private String Ddo_contratoarquivosanexos_nomearq_Filtertype ;
      private String Ddo_contratoarquivosanexos_nomearq_Datalisttype ;
      private String Ddo_contratoarquivosanexos_nomearq_Datalistproc ;
      private String Ddo_contratoarquivosanexos_nomearq_Sortasc ;
      private String Ddo_contratoarquivosanexos_nomearq_Sortdsc ;
      private String Ddo_contratoarquivosanexos_nomearq_Loadingdata ;
      private String Ddo_contratoarquivosanexos_nomearq_Cleanfilter ;
      private String Ddo_contratoarquivosanexos_nomearq_Noresultsfound ;
      private String Ddo_contratoarquivosanexos_nomearq_Searchbuttontext ;
      private String Ddo_contratoarquivosanexos_tipoarq_Caption ;
      private String Ddo_contratoarquivosanexos_tipoarq_Tooltip ;
      private String Ddo_contratoarquivosanexos_tipoarq_Cls ;
      private String Ddo_contratoarquivosanexos_tipoarq_Filteredtext_set ;
      private String Ddo_contratoarquivosanexos_tipoarq_Selectedvalue_set ;
      private String Ddo_contratoarquivosanexos_tipoarq_Dropdownoptionstype ;
      private String Ddo_contratoarquivosanexos_tipoarq_Titlecontrolidtoreplace ;
      private String Ddo_contratoarquivosanexos_tipoarq_Sortedstatus ;
      private String Ddo_contratoarquivosanexos_tipoarq_Filtertype ;
      private String Ddo_contratoarquivosanexos_tipoarq_Datalisttype ;
      private String Ddo_contratoarquivosanexos_tipoarq_Datalistproc ;
      private String Ddo_contratoarquivosanexos_tipoarq_Sortasc ;
      private String Ddo_contratoarquivosanexos_tipoarq_Sortdsc ;
      private String Ddo_contratoarquivosanexos_tipoarq_Loadingdata ;
      private String Ddo_contratoarquivosanexos_tipoarq_Cleanfilter ;
      private String Ddo_contratoarquivosanexos_tipoarq_Noresultsfound ;
      private String Ddo_contratoarquivosanexos_tipoarq_Searchbuttontext ;
      private String Ddo_contratoarquivosanexos_data_Caption ;
      private String Ddo_contratoarquivosanexos_data_Tooltip ;
      private String Ddo_contratoarquivosanexos_data_Cls ;
      private String Ddo_contratoarquivosanexos_data_Filteredtext_set ;
      private String Ddo_contratoarquivosanexos_data_Filteredtextto_set ;
      private String Ddo_contratoarquivosanexos_data_Dropdownoptionstype ;
      private String Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace ;
      private String Ddo_contratoarquivosanexos_data_Sortedstatus ;
      private String Ddo_contratoarquivosanexos_data_Filtertype ;
      private String Ddo_contratoarquivosanexos_data_Sortasc ;
      private String Ddo_contratoarquivosanexos_data_Sortdsc ;
      private String Ddo_contratoarquivosanexos_data_Cleanfilter ;
      private String Ddo_contratoarquivosanexos_data_Rangefilterfrom ;
      private String Ddo_contratoarquivosanexos_data_Rangefilterto ;
      private String Ddo_contratoarquivosanexos_data_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_Internalname ;
      private String edtavTfcontratada_pessoacnpj_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_sel_Internalname ;
      private String edtavTfcontratada_pessoacnpj_sel_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_descricao_Internalname ;
      private String edtavTfcontratoarquivosanexos_descricao_sel_Internalname ;
      private String edtavTfcontratoarquivosanexos_nomearq_Internalname ;
      private String edtavTfcontratoarquivosanexos_nomearq_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_nomearq_sel_Internalname ;
      private String edtavTfcontratoarquivosanexos_nomearq_sel_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_tipoarq_Internalname ;
      private String edtavTfcontratoarquivosanexos_tipoarq_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_tipoarq_sel_Internalname ;
      private String edtavTfcontratoarquivosanexos_tipoarq_sel_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_data_Internalname ;
      private String edtavTfcontratoarquivosanexos_data_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_data_to_Internalname ;
      private String edtavTfcontratoarquivosanexos_data_to_Jsonclick ;
      private String divDdo_contratoarquivosanexos_dataauxdates_Internalname ;
      private String edtavDdo_contratoarquivosanexos_dataauxdate_Internalname ;
      private String edtavDdo_contratoarquivosanexos_dataauxdate_Jsonclick ;
      private String edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname ;
      private String edtavDdo_contratoarquivosanexos_dataauxdateto_Jsonclick ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoarquivosanexos_nomearqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoarquivosanexos_tipoarqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratoArquivosAnexos_Descricao_Internalname ;
      private String A112ContratoArquivosAnexos_NomeArq ;
      private String edtContratoArquivosAnexos_NomeArq_Internalname ;
      private String A109ContratoArquivosAnexos_TipoArq ;
      private String edtContratoArquivosAnexos_TipoArq_Internalname ;
      private String edtContratoArquivosAnexos_Data_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero ;
      private String lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ;
      private String lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ;
      private String lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ;
      private String AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ;
      private String AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero ;
      private String AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ;
      private String AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ;
      private String AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ;
      private String AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ;
      private String AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ;
      private String AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoarquivosanexos_descricao1_Internalname ;
      private String edtavContratoarquivosanexos_data1_Internalname ;
      private String edtavContratoarquivosanexos_data_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoarquivosanexos_descricao2_Internalname ;
      private String edtavContratoarquivosanexos_data2_Internalname ;
      private String edtavContratoarquivosanexos_data_to2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratada_pessoacnpj_Internalname ;
      private String Ddo_contratoarquivosanexos_descricao_Internalname ;
      private String Ddo_contratoarquivosanexos_nomearq_Internalname ;
      private String Ddo_contratoarquivosanexos_tipoarq_Internalname ;
      private String Ddo_contratoarquivosanexos_data_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtContratoArquivosAnexos_Descricao_Title ;
      private String edtContratoArquivosAnexos_NomeArq_Title ;
      private String edtContratoArquivosAnexos_TipoArq_Title ;
      private String edtContratoArquivosAnexos_Data_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratada_PessoaNom_Link ;
      private String edtContratoArquivosAnexos_Descricao_Link ;
      private String tblTablemergeddynamicfilterscontratoarquivosanexos_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontratoarquivosanexos_data2_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoarquivosanexostitle_Internalname ;
      private String lblContratoarquivosanexostitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoarquivosanexos_data2_Jsonclick ;
      private String lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext2_Jsonclick ;
      private String edtavContratoarquivosanexos_data_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoarquivosanexos_data1_Jsonclick ;
      private String lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratoarquivosanexos_data_rangemiddletext1_Jsonclick ;
      private String edtavContratoarquivosanexos_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_87_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtContratoArquivosAnexos_Descricao_Jsonclick ;
      private String edtContratoArquivosAnexos_NomeArq_Jsonclick ;
      private String edtContratoArquivosAnexos_TipoArq_Jsonclick ;
      private String edtContratoArquivosAnexos_Data_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV37ContratoArquivosAnexos_Data1 ;
      private DateTime AV38ContratoArquivosAnexos_Data_To1 ;
      private DateTime AV39ContratoArquivosAnexos_Data2 ;
      private DateTime AV40ContratoArquivosAnexos_Data_To2 ;
      private DateTime AV70TFContratoArquivosAnexos_Data ;
      private DateTime AV71TFContratoArquivosAnexos_Data_To ;
      private DateTime A113ContratoArquivosAnexos_Data ;
      private DateTime AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ;
      private DateTime AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ;
      private DateTime AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ;
      private DateTime AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ;
      private DateTime AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ;
      private DateTime AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ;
      private DateTime AV72DDO_ContratoArquivosAnexos_DataAuxDate ;
      private DateTime AV73DDO_ContratoArquivosAnexos_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratada_pessoacnpj_Includesortasc ;
      private bool Ddo_contratada_pessoacnpj_Includesortdsc ;
      private bool Ddo_contratada_pessoacnpj_Includefilter ;
      private bool Ddo_contratada_pessoacnpj_Filterisrange ;
      private bool Ddo_contratada_pessoacnpj_Includedatalist ;
      private bool Ddo_contratoarquivosanexos_descricao_Includesortasc ;
      private bool Ddo_contratoarquivosanexos_descricao_Includesortdsc ;
      private bool Ddo_contratoarquivosanexos_descricao_Includefilter ;
      private bool Ddo_contratoarquivosanexos_descricao_Filterisrange ;
      private bool Ddo_contratoarquivosanexos_descricao_Includedatalist ;
      private bool Ddo_contratoarquivosanexos_nomearq_Includesortasc ;
      private bool Ddo_contratoarquivosanexos_nomearq_Includesortdsc ;
      private bool Ddo_contratoarquivosanexos_nomearq_Includefilter ;
      private bool Ddo_contratoarquivosanexos_nomearq_Filterisrange ;
      private bool Ddo_contratoarquivosanexos_nomearq_Includedatalist ;
      private bool Ddo_contratoarquivosanexos_tipoarq_Includesortasc ;
      private bool Ddo_contratoarquivosanexos_tipoarq_Includesortdsc ;
      private bool Ddo_contratoarquivosanexos_tipoarq_Includefilter ;
      private bool Ddo_contratoarquivosanexos_tipoarq_Filterisrange ;
      private bool Ddo_contratoarquivosanexos_tipoarq_Includedatalist ;
      private bool Ddo_contratoarquivosanexos_data_Includesortasc ;
      private bool Ddo_contratoarquivosanexos_data_Includesortdsc ;
      private bool Ddo_contratoarquivosanexos_data_Includefilter ;
      private bool Ddo_contratoarquivosanexos_data_Filterisrange ;
      private bool Ddo_contratoarquivosanexos_data_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV17ContratoArquivosAnexos_Descricao1 ;
      private String AV21ContratoArquivosAnexos_Descricao2 ;
      private String A110ContratoArquivosAnexos_Descricao ;
      private String lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ;
      private String lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ;
      private String AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ;
      private String AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV54TFContratada_PessoaCNPJ ;
      private String AV55TFContratada_PessoaCNPJ_Sel ;
      private String AV58TFContratoArquivosAnexos_Descricao ;
      private String AV59TFContratoArquivosAnexos_Descricao_Sel ;
      private String AV48ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV52ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV56ddo_Contratada_PessoaCNPJTitleControlIdToReplace ;
      private String AV60ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace ;
      private String AV64ddo_ContratoArquivosAnexos_NomeArqTitleControlIdToReplace ;
      private String AV68ddo_ContratoArquivosAnexos_TipoArqTitleControlIdToReplace ;
      private String AV74ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace ;
      private String AV117Update_GXI ;
      private String AV118Delete_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ;
      private String lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ;
      private String AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ;
      private String AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ;
      private String AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ;
      private String AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ;
      private String AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ;
      private String AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H006L2_A40Contratada_PessoaCod ;
      private int[] H006L2_A74Contrato_Codigo ;
      private int[] H006L2_A108ContratoArquivosAnexos_Codigo ;
      private int[] H006L2_A39Contratada_Codigo ;
      private DateTime[] H006L2_A113ContratoArquivosAnexos_Data ;
      private String[] H006L2_A109ContratoArquivosAnexos_TipoArq ;
      private String[] H006L2_A112ContratoArquivosAnexos_NomeArq ;
      private String[] H006L2_A110ContratoArquivosAnexos_Descricao ;
      private String[] H006L2_A42Contratada_PessoaCNPJ ;
      private bool[] H006L2_n42Contratada_PessoaCNPJ ;
      private String[] H006L2_A41Contratada_PessoaNom ;
      private bool[] H006L2_n41Contratada_PessoaNom ;
      private String[] H006L2_A77Contrato_Numero ;
      private long[] H006L3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53Contratada_PessoaCNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57ContratoArquivosAnexos_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61ContratoArquivosAnexos_NomeArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65ContratoArquivosAnexos_TipoArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69ContratoArquivosAnexos_DataTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV75DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratoarquivosanexos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006L2( IGxContext context ,
                                             String AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [27] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T1.[ContratoArquivosAnexos_Codigo], T2.[Contratada_Codigo], T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_TipoArq], T1.[ContratoArquivosAnexos_NomeArq], T1.[ContratoArquivosAnexos_Descricao], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero]";
         sFromString = " FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_NomeArq]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_NomeArq] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_TipoArq]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_TipoArq] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Data]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006L3( IGxContext context ,
                                             String AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [22] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV93WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV99WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV97WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H006L2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 1 :
                     return conditional_H006L3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006L2 ;
          prmH006L2 = new Object[] {
          new Object[] {"@lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006L3 ;
          prmH006L3 = new Object[] {
          new Object[] {"@lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV94WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV95WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV96WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV100WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV101WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV102WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV103WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV104WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV105WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV106WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV108WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV109WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV110WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV111WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV112WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV113WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV114WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV115WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV116WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006L2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006L2,11,0,true,false )
             ,new CursorDef("H006L3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006L3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 10) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(8) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                ((String[]) buf[10])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getString(11, 20) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                return;
       }
    }

 }

}
