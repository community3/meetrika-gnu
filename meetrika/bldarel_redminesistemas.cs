using System;
using GeneXus.Builder;
using System.IO;
public class bldarel_redminesistemas : GxBaseBuilder
{
   string cs_path = "." ;
   public bldarel_redminesistemas( ) : base()
   {
   }

   public override int BeforeCompile( )
   {
      return 0 ;
   }

   public override int AfterCompile( )
   {
      int ErrCode ;
      ErrCode = 0;
      return ErrCode ;
   }

   static public int Main( string[] args )
   {
      bldarel_redminesistemas x = new bldarel_redminesistemas() ;
      x.SetMainSourceFile( "arel_redminesistemas.cs");
      x.LoadVariables( args);
      return x.CompileAll( );
   }

   public override ItemCollection GetSortedBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\GeneXus.Programs.Common.dll", cs_path + @"\genexus.programs.common.rsp");
      return sc ;
   }

   public override TargetCollection GetRuntimeBuildList( )
   {
      TargetCollection sc = new TargetCollection() ;
      sc.Add( @"arel_redminesistemas", "dll");
      return sc ;
   }

   public override ItemCollection GetResBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\messages.por.dll", cs_path + @"\messages.por.txt");
      return sc ;
   }

   public override bool ToBuild( String obj )
   {
      if (checkTime(obj, cs_path + @"\bin\GxClasses.dll" ))
         return true;
      if ( obj == @"bin\GeneXus.Programs.Common.dll" )
      {
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_ConsultarAutorizacaoUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WsFiltros.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_Demandas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_Demandas_Demanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WSAutenticacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Requisitos_SDT_RequisitosItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGeoRegions_GeoRegionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGeoMarkers_GeoMarkersItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtjqSelectData_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Parametros.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtDVB_SidebarMenuOptionsData_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Periodos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_UsuarioSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ContratoServicoUnidadeMedicao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Artefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AreaTrabalho_Sistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ID_Valor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_SaldoContratoCB.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSchedulerEvents.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSchedulerEvents_Day.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSchedulerEvents_event.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership_roles.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership_roles_role.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_CustomFields_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Associar.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AutenticacaoIN.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT_Error.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT_Usuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT_Usuario_Perfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssuePut.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssuePut_Issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineStatus_issue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_assigned_to.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_attachments.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_attachments_attachment.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_attachments_attachment_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_changesets.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_changesets_changeset.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_children.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_children_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_children_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal_details.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal_details_detail.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal_user.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_priority.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_assigned_to.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_priority.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineTrackers.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineTrackers_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_assigned_to.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_fixed_version.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_priority.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers_user.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers_user_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers_user_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineprojects.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineprojects_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineprojects_project_parent.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUploadifyOutput.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroInfoSDT.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Baselines_Funcao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_MonitorDmn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWizardSteps_WizardStepsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWizardAuxiliarData_WizardAuxiliarDataItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtDVB_SDTDropDownOptionsTitleSettingsIcons.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_VariaveisCocomo_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTreeNodeCollection_TreeNode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ConsultaSistemas_Sistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Grafico_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_CheckList_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_CloneAtributo_Atributo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ItensConagem_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Codigos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Demandas_Demanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ContagemResultadoEvidencias_Arquivo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_FiltroConsContadorFM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxChart.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxChart_Serie.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerParameters_Parameter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemExpandData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemDoubleClickData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemDoubleClickData_Element.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemDoubleClickData_Filter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemCollapseData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemClickData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemClickData_Element.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemClickData_Filter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerFilterChangedData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerDragAndDropData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_AxisOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_ExpandCollapse.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Filter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Format.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Format_ConditionalStyle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Format_ValueStyle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Grouping.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAggregationChangedData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTargets_Target.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxEventFK.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxKeyValue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroEventSDT_GxSynchroEventSDTItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTileSDT.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPurchaseReceiptInformation.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ManterDadosREC.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtInNewWindowTargets_InNewWindowTargetsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Usuarios_Usuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDTSolicitacao_SDTSolicitacaoItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFckEditorMenu_FckEditorMenuItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabOptions_TabOptionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtContext.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSectionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMExampleSDTApplicationData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtGoogleDocsResult.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtGoogleDocsResult_Doc.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtGridStateCollection_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtDVB_SDTDropDownOptionsData_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPTransactionContext.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPTransactionContext_Attribute.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPTabOptions_TabOptionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPGridState.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPGridState_DynamicFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPGridState_FilterValue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPColumnsSelector.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPColumnsSelector_InvisibleColumn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPColumnsSelector_VisibleColumn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabsMenuData_TabsMenuDataItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabsMenuData_TabsMenuDataItem_SectionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxMap.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxMap_Point.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPContext.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtProgramNames_ProgramName.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtAuditingObject.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtAuditingObject_RecordItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtAuditingObject_RecordItem_AttributeItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtNotificationInfo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtLinkList_LinkItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtMessages_Message.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxObjectCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxSilentTrnGridCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\SoapParm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxWebStd.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxFullTextSearchReindexer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxModelInfoProvider.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTmp_File.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUsuarioPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratante.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPessoa.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratanteUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratadaUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratada.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContrato.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAreaTrabalho_Guias.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtModulo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSistema_Tecnologia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServico.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFuncoesAPFAtributos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAtributos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtModuloFuncoes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFuncoesUsuarioFuncoesAPF.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemItemAtributosFSoftware.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemItemAtributosFMetrica.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtMenuPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtMenu.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtParametrosSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAmbienteTecnologicoTecnologias.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFuncaoDadosTabela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAreaTrabalho.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacoesItens.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacoes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoContagens.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoEvidencia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtProjeto.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUsuarioServicos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtLoteArquivoAnexo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoChckLstLog.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicos_Rmn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRegrasContagemAnexos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultado.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosSistemas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoGestor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFeriados.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAnexos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAnexos_De.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosPrioridade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAudit.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSistemaDePara.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosDePara.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotificacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotificacao_Demanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotificacao_Destinatario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoFluxo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoResponsavel.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtHelpSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtEmail_Instancia_Destinatarios.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtLogErroBC.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoSistemas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtEmail_Instancia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratanteUsuarioSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtCatalogoSutentacaoArtefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosArtefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacaoServico.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoArtefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoArtefato.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoAuxiliar.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGpoObjCtrlResponsavel.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtProposta.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoCheck.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacaoServicoReqNegocio.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtReqNegReqTec.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoQA.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtWebNotification.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMDescription.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMProperty.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMError.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeSimple.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMCountry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMCountryLanguages.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepository.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUpdateRepositoryConfiguration.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUpdateRepositoryConfigurationApplicationsToImport.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMLoginAdditionalParameters.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSecurityPolicy.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserAttribute.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserAttributeMultiValues.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRole.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRoleFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplication.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationEnvironment.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationDelegateAuthorization.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationPermissionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationPermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationToken.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationTokenElement.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMPermissionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMPermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLogFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLog.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLogLoginRetry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSecurityPolicyFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionAddressList.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSession.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLoginRetry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionRole.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationFacebook.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeFacebook.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeLocal.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeTrusted.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeWebService.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationWebService.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationWebServiceServer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTwitter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeTwitter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryCreate.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMImportRepositoryConfiguration.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionFileFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionInfo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionInfoProperties.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionProperties.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationGoogle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeGoogle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationCustom.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeCustom.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationGAMRemote.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeGAMRemote.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxCbb.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServerSideSign.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtCrcStream.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPDFTools.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtIgImageEditor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCryptoSignAlgorithm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMExternalAuthentication.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMVersion.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserListOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAllowMultipleConcurrentSessions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionAccessTypeDefault.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionTypeFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMBooleanFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMExternalAuthorizationVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionLogListOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMTracing.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoPessoa.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINUnidadeContratacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINUnidadeGuiaRef.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoNaoConformidade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoDados.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoUnidade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTecnicaContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoMenu.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINMessageTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFckEditorObjectInterface.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFckEditorModes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoFuncaoAPF.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoFuncaoDados.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINComplexidade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusDaFuncao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINProgressIndicatorType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINDVelopConfirmPanelType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusDemanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoReferencia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINDMStatusSolicitacoes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoFabrica.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAPIAuthorizationStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEventExecution.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINSmartDeviceType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINRecentLinksOptions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCameraAPIQuality.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAudioAPISessionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNetworkAPIConnectionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEventAction.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEventStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINApplicationState.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINSynchronizationReceiveResult.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINRegionState.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINIMEMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINBeaconProximity.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCobranca.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerChartType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerOutputType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerXAxisLabels.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerAggregationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerAxisOrderType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerAxisType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerConditionOperator.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerExpandCollapse.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCallTargetSize.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerFilterType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerObjectType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerSubtotals.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerShowDataLabelsIn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoErroContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusErroContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINLocalExecucao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusProjeto.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoINM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipodePrazo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemArrowAlign.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemIconAlign.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemScale.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINPdfCertificationLevel.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINServicoAtende.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTabela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMConstant.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINWWPDomains.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNotificacaoMidia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINObrigaValores.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINObjetoControle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoDias.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoManutencao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINOrigem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEvento.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoHierarquia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINContagemResultado_TipoRegistro.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoStandBy.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINHistoricoCodigo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINSaldoContratoOperacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNivelContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAutorizacaoConsumoStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCheckMomento.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAlertaToGroup.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINReqPrioridade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINHttpMethod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINReqSituacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINRequisitoStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNecessidadeStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINPDFpermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINBarcodeType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINPage.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINLogFile.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoGestor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTecnicaMoSCoW.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINIgImageEditorFormat.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroStatusDemanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroTipoData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTrnMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroPeriodo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroStatusDemandaCaixaEntrada.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINExportType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEncoding.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserActivationMethod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserGender.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryRememberUserTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTimezones.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAuthenticationFunctions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAuthenticationTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMBrowser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEffect.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMErrorMessages.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMGenerateAuditory.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMGenerateSessionStatistics.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCallType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMIdentificatorKey.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMMenuOptionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionAccessType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCryptoEncryptAlgorithm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRecoveryPasswordTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRememberUserTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryConnectionTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCryptoHashAlgorithm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAPiMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryUserIdentifications.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMApplicationAuthorizarionRequestType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMApplicationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtOAuthVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtOpenIdVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtWebServiceVersions.cs" ))
            return true;
      }
      if ( obj == @"bin\messages.por.dll" )
      {
         if (checkTime(obj, cs_path + @"\messages.por.txt" ))
            return true;
      }
      return false ;
   }

}

