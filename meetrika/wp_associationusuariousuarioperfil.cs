/*
               File: WP_AssociationUsuarioUsuarioPerfil
        Description: Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:37.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associationusuariousuarioperfil : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associationusuariousuarioperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associationusuariousuarioperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo )
      {
         this.AV7Usuario_Codigo = aP0_Usuario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7Usuario_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA0W2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS0W2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE0W2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117303773");
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associationusuariousuarioperfil.aspx") + "?" + UrlEncode("" +AV7Usuario_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuario_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm0W2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "WP_AssociationUsuarioUsuarioPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar" ;
      }

      protected void WB0W0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_0W2( true) ;
         }
         else
         {
            wb_table1_2_0W2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV16AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV17NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV18AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV19NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_gamguid_Internalname, StringUtil.RTrim( AV31Usuario_GamGuid), StringUtil.RTrim( context.localUtil.Format( AV31Usuario_GamGuid, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_gamguid_Jsonclick, 0, "Attribute", "", "", "", edtavUsuario_gamguid_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</p>") ;
         }
         wbLoad = true;
      }

      protected void START0W2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0W0( ) ;
      }

      protected void WS0W2( )
      {
         START0W2( ) ;
         EVT0W2( ) ;
      }

      protected void EVT0W2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110W2 */
                           E110W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120W2 */
                           E120W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E130W2 */
                                 E130W2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E140W2 */
                           E140W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E150W2 */
                           E150W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E160W2 */
                           E160W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E170W2 */
                           E170W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E180W2 */
                           E180W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E190W2 */
                           E190W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E200W2 */
                           E200W2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E180W2 */
                           E180W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E190W2 */
                           E190W2 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE0W2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0W2( ) ;
            }
         }
      }

      protected void PA0W2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0W2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0W2( )
      {
         /* Execute user event: E120W2 */
         E120W2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H000W2 */
            pr_default.execute(0, new Object[] {AV7Usuario_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A57Usuario_PessoaCod = H000W2_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = H000W2_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = H000W2_A58Usuario_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               n58Usuario_PessoaNom = H000W2_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = H000W2_A58Usuario_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               n58Usuario_PessoaNom = H000W2_n58Usuario_PessoaNom[0];
               /* Execute user event: E200W2 */
               E200W2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB0W0( ) ;
         }
      }

      protected void STRUP0W0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110W2 */
         E110W2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
            n58Usuario_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV25AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            AV16AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV17NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV18AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
            AV19NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
            AV31Usuario_GamGuid = cgiGet( edtavUsuario_gamguid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_GamGuid", AV31Usuario_GamGuid);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E110W2 */
         E110W2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E110W2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         AV38AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV37WebSession.Get("Area"), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38AreaTrabalho_Codigo), 6, 0)));
         AV37WebSession.Remove("Area");
         if ( (0==AV38AreaTrabalho_Codigo) )
         {
            AV38AreaTrabalho_Codigo = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38AreaTrabalho_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV41GXLvl13 = 0;
            /* Using cursor H000W3 */
            pr_default.execute(1, new Object[] {AV7Usuario_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1Usuario_Codigo = H000W3_A1Usuario_Codigo[0];
               A341Usuario_UserGamGuid = H000W3_A341Usuario_UserGamGuid[0];
               AV41GXLvl13 = 1;
               AV31Usuario_GamGuid = A341Usuario_UserGamGuid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_GamGuid", AV31Usuario_GamGuid);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( AV41GXLvl13 == 0 )
            {
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            edtavUsuario_gamguid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_gamguid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_gamguid_Visible), 5, 0)));
            /* Using cursor H000W4 */
            pr_default.execute(2, new Object[] {AV38AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A3Perfil_Codigo = H000W4_A3Perfil_Codigo[0];
               A7Perfil_AreaTrabalhoCod = H000W4_A7Perfil_AreaTrabalhoCod[0];
               A4Perfil_Nome = H000W4_A4Perfil_Nome[0];
               AV10Exist = false;
               /* Using cursor H000W5 */
               pr_default.execute(3, new Object[] {AV7Usuario_Codigo, A3Perfil_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A1Usuario_Codigo = H000W5_A1Usuario_Codigo[0];
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               AV13Description = A4Perfil_Nome;
               if ( AV10Exist )
               {
                  AV20AddedKeyList.Add(A3Perfil_Codigo, 0);
                  AV22AddedDscList.Add(AV13Description, 0);
               }
               else
               {
                  AV21NotAddedKeyList.Add(A3Perfil_Codigo, 0);
                  AV23NotAddedDscList.Add(AV13Description, 0);
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
      }

      protected void E120W2( )
      {
         /* Refresh Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV14i = 1;
         AV44GXV1 = 1;
         while ( AV44GXV1 <= AV20AddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV44GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV44GXV1 = (int)(AV44GXV1+1);
         }
         AV14i = 1;
         AV45GXV2 = 1;
         while ( AV45GXV2 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV45GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV45GXV2 = (int)(AV45GXV2+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E130W2 */
         E130W2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E130W2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV12Success = true;
         AV46GXV3 = 1;
         while ( AV46GXV3 <= AV20AddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV46GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            /* Using cursor H000W6 */
            pr_default.execute(4, new Object[] {AV8Perfil_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A3Perfil_Codigo = H000W6_A3Perfil_Codigo[0];
               A329Perfil_GamId = H000W6_A329Perfil_GamId[0];
               AV27Perfil_GamId = A329Perfil_GamId;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            if ( AV12Success )
            {
               AV10Exist = false;
               /* Using cursor H000W7 */
               pr_default.execute(5, new Object[] {AV7Usuario_Codigo, AV8Perfil_Codigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A3Perfil_Codigo = H000W7_A3Perfil_Codigo[0];
                  A1Usuario_Codigo = H000W7_A1Usuario_Codigo[0];
                  A329Perfil_GamId = H000W7_A329Perfil_GamId[0];
                  A329Perfil_GamId = H000W7_A329Perfil_GamId[0];
                  AV27Perfil_GamId = A329Perfil_GamId;
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(5);
               if ( ! AV10Exist )
               {
                  AV11UsuarioPerfil = new SdtUsuarioPerfil(context);
                  AV11UsuarioPerfil.gxTpr_Usuario_codigo = AV7Usuario_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11UsuarioPerfil", AV11UsuarioPerfil);
                  AV11UsuarioPerfil.gxTpr_Perfil_codigo = AV8Perfil_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11UsuarioPerfil", AV11UsuarioPerfil);
                  AV11UsuarioPerfil.Save();
                  if ( ! AV11UsuarioPerfil.Success() )
                  {
                     AV12Success = false;
                  }
                  else
                  {
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Usuario_GamGuid)) )
                     {
                        AV29Gamuser.load( AV31Usuario_GamGuid);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_GamGuid", AV31Usuario_GamGuid);
                        AV30isOk = AV29Gamuser.addrolebyid(AV27Perfil_GamId, out  AV28Errors);
                        if ( ! AV30isOk )
                        {
                           /* Execute user subroutine: 'DISPLAYERRORS' */
                           S132 ();
                           if ( returnInSub )
                           {
                              returnInSub = true;
                              if (true) return;
                           }
                        }
                     }
                     else
                     {
                        AV12Success = false;
                        GX_msglist.addItem("Usu�rio n�o identificado no GAM!  "+AV31Usuario_GamGuid);
                     }
                  }
               }
            }
            AV14i = (int)(AV14i+1);
            AV46GXV3 = (int)(AV46GXV3+1);
         }
         AV14i = 1;
         AV49GXV4 = 1;
         while ( AV49GXV4 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV49GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            /* Using cursor H000W8 */
            pr_default.execute(6, new Object[] {AV8Perfil_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A3Perfil_Codigo = H000W8_A3Perfil_Codigo[0];
               A329Perfil_GamId = H000W8_A329Perfil_GamId[0];
               AV27Perfil_GamId = A329Perfil_GamId;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
            if ( AV12Success )
            {
               AV10Exist = false;
               /* Using cursor H000W9 */
               pr_default.execute(7, new Object[] {AV7Usuario_Codigo, AV8Perfil_Codigo});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  A3Perfil_Codigo = H000W9_A3Perfil_Codigo[0];
                  A1Usuario_Codigo = H000W9_A1Usuario_Codigo[0];
                  A329Perfil_GamId = H000W9_A329Perfil_GamId[0];
                  A329Perfil_GamId = H000W9_A329Perfil_GamId[0];
                  AV27Perfil_GamId = A329Perfil_GamId;
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(7);
               if ( AV10Exist )
               {
                  AV11UsuarioPerfil = new SdtUsuarioPerfil(context);
                  AV11UsuarioPerfil.Load(AV7Usuario_Codigo, AV8Perfil_Codigo);
                  if ( AV11UsuarioPerfil.Success() )
                  {
                     AV29Gamuser.load( AV31Usuario_GamGuid);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_GamGuid", AV31Usuario_GamGuid);
                     AV30isOk = AV29Gamuser.deleterolebyid(AV27Perfil_GamId, out  AV28Errors);
                     if ( ! AV30isOk )
                     {
                        /* Execute user subroutine: 'DISPLAYERRORS' */
                        S132 ();
                        if ( returnInSub )
                        {
                           returnInSub = true;
                           if (true) return;
                        }
                     }
                     AV11UsuarioPerfil.Delete();
                  }
                  if ( ! AV11UsuarioPerfil.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV14i = (int)(AV14i+1);
            AV49GXV4 = (int)(AV49GXV4+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociationUsuarioUsuarioPerfil");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E140W2( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E150W2( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E160W2( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E170W2( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21NotAddedKeyList = (IGxCollection)(AV20AddedKeyList.Clone());
         AV23NotAddedDscList = (IGxCollection)(AV22AddedDscList.Clone());
         AV22AddedDscList.Clear();
         AV20AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E180W2( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E190W2( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV53GXV6 = 1;
         AV52GXV5 = AV11UsuarioPerfil.GetMessages();
         while ( AV53GXV6 <= AV52GXV5.Count )
         {
            AV15Message = ((SdtMessages_Message)AV52GXV5.Item(AV53GXV6));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV53GXV6 = (int)(AV53GXV6+1);
         }
      }

      protected void S122( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16AddedKeyListXml)) )
         {
            AV22AddedDscList.FromXml(AV18AddedDscListXml, "Collection");
            AV20AddedKeyList.FromXml(AV16AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NotAddedKeyListXml)) )
         {
            AV21NotAddedKeyList.FromXml(AV17NotAddedKeyListXml, "Collection");
            AV23NotAddedDscList.FromXml(AV19NotAddedDscListXml, "Collection");
         }
      }

      protected void S112( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV20AddedKeyList.Count > 0 )
         {
            AV16AddedKeyListXml = AV20AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = AV22AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         else
         {
            AV16AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         if ( AV21NotAddedKeyList.Count > 0 )
         {
            AV17NotAddedKeyListXml = AV21NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = AV23NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
         else
         {
            AV17NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
      }

      protected void S172( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV26InsertIndex = 1;
         AV54GXV7 = 1;
         while ( AV54GXV7 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV54GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV8Perfil_Codigo, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV14i = (int)(AV14i+1);
            AV54GXV7 = (int)(AV54GXV7+1);
         }
         AV21NotAddedKeyList.Clear();
         AV23NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV55GXV8 = 1;
         while ( AV55GXV8 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV55GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            if ( AV8Perfil_Codigo == AV24NotAssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV55GXV8 = (int)(AV55GXV8+1);
         }
         if ( AV14i <= AV21NotAddedKeyList.Count )
         {
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV24NotAssociatedRecords, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV21NotAddedKeyList.RemoveItem(AV14i);
            AV23NotAddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S152( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV56GXV9 = 1;
         while ( AV56GXV9 <= AV20AddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV56GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            if ( AV8Perfil_Codigo == AV25AssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV56GXV9 = (int)(AV56GXV9+1);
         }
         if ( AV14i <= AV20AddedKeyList.Count )
         {
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV23NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV23NotAddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV21NotAddedKeyList.Add(AV25AssociatedRecords, AV26InsertIndex);
            AV23NotAddedDscList.Add(AV13Description, AV26InsertIndex);
            AV20AddedKeyList.RemoveItem(AV14i);
            AV22AddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S132( )
      {
         /* 'DISPLAYERRORS' Routine */
         if ( AV28Errors.Count > 0 )
         {
            AV57GXV10 = 1;
            while ( AV57GXV10 <= AV28Errors.Count )
            {
               AV32Error = ((SdtGAMError)AV28Errors.Item(AV57GXV10));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV32Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV57GXV10 = (int)(AV57GXV10+1);
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E200W2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_0W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_0W2( true) ;
         }
         else
         {
            wb_table2_8_0W2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_0W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table3_16_0W2( true) ;
         }
         else
         {
            wb_table3_16_0W2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_0W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table4_48_0W2( true) ;
         }
         else
         {
            wb_table4_48_0W2( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_0W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0W2e( true) ;
         }
         else
         {
            wb_table1_2_0W2e( false) ;
         }
      }

      protected void wb_table4_48_0W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_0W2e( true) ;
         }
         else
         {
            wb_table4_48_0W2e( false) ;
         }
      }

      protected void wb_table3_16_0W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_19_0W2( true) ;
         }
         else
         {
            wb_table5_19_0W2( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_0W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_0W2e( true) ;
         }
         else
         {
            wb_table3_16_0W2e( false) ;
         }
      }

      protected void wb_table5_19_0W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Perfis N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Perfis Associados", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            wb_table6_31_0W2( true) ;
         }
         else
         {
            wb_table6_31_0W2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_0W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_0W2e( true) ;
         }
         else
         {
            wb_table5_19_0W2e( false) ;
         }
      }

      protected void wb_table6_31_0W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_0W2e( true) ;
         }
         else
         {
            wb_table6_31_0W2e( false) ;
         }
      }

      protected void wb_table2_8_0W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:190px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Perfis ao Usu�rio :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left;width:584px")+"\" class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaNom_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_WP_AssociationUsuarioUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0W2e( true) ;
         }
         else
         {
            wb_table2_8_0W2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0W2( ) ;
         WS0W2( ) ;
         WE0W2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311730393");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associationusuariousuarioperfil.js", "?2020311730394");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         lblTbjava_Internalname = "TBJAVA";
         edtavUsuario_gamguid_Internalname = "vUSUARIO_GAMGUID";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtUsuario_PessoaNom_Jsonclick = "";
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavUsuario_gamguid_Jsonclick = "";
         edtavUsuario_gamguid_Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV16AddedKeyListXml = "";
         AV17NotAddedKeyListXml = "";
         AV18AddedDscListXml = "";
         AV19NotAddedDscListXml = "";
         lblTbjava_Jsonclick = "";
         AV31Usuario_GamGuid = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H000W2_A57Usuario_PessoaCod = new int[1] ;
         H000W2_A1Usuario_Codigo = new int[1] ;
         H000W2_A58Usuario_PessoaNom = new String[] {""} ;
         H000W2_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37WebSession = context.GetSession();
         AV9HTTPRequest = new GxHttpRequest( context);
         H000W3_A1Usuario_Codigo = new int[1] ;
         H000W3_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         H000W4_A3Perfil_Codigo = new int[1] ;
         H000W4_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H000W4_A4Perfil_Nome = new String[] {""} ;
         A4Perfil_Nome = "";
         H000W5_A3Perfil_Codigo = new int[1] ;
         H000W5_A1Usuario_Codigo = new int[1] ;
         AV13Description = "";
         AV20AddedKeyList = new GxSimpleCollection();
         AV22AddedDscList = new GxSimpleCollection();
         AV21NotAddedKeyList = new GxSimpleCollection();
         AV23NotAddedDscList = new GxSimpleCollection();
         H000W6_A3Perfil_Codigo = new int[1] ;
         H000W6_A329Perfil_GamId = new long[1] ;
         H000W7_A3Perfil_Codigo = new int[1] ;
         H000W7_A1Usuario_Codigo = new int[1] ;
         H000W7_A329Perfil_GamId = new long[1] ;
         AV11UsuarioPerfil = new SdtUsuarioPerfil(context);
         AV29Gamuser = new SdtGAMUser(context);
         AV28Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         H000W8_A3Perfil_Codigo = new int[1] ;
         H000W8_A329Perfil_GamId = new long[1] ;
         H000W9_A3Perfil_Codigo = new int[1] ;
         H000W9_A1Usuario_Codigo = new int[1] ;
         H000W9_A329Perfil_GamId = new long[1] ;
         AV52GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         AV32Error = new SdtGAMError(context);
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associationusuariousuarioperfil__default(),
            new Object[][] {
                new Object[] {
               H000W2_A57Usuario_PessoaCod, H000W2_A1Usuario_Codigo, H000W2_A58Usuario_PessoaNom, H000W2_n58Usuario_PessoaNom
               }
               , new Object[] {
               H000W3_A1Usuario_Codigo, H000W3_A341Usuario_UserGamGuid
               }
               , new Object[] {
               H000W4_A3Perfil_Codigo, H000W4_A7Perfil_AreaTrabalhoCod, H000W4_A4Perfil_Nome
               }
               , new Object[] {
               H000W5_A3Perfil_Codigo, H000W5_A1Usuario_Codigo
               }
               , new Object[] {
               H000W6_A3Perfil_Codigo, H000W6_A329Perfil_GamId
               }
               , new Object[] {
               H000W7_A3Perfil_Codigo, H000W7_A1Usuario_Codigo, H000W7_A329Perfil_GamId
               }
               , new Object[] {
               H000W8_A3Perfil_Codigo, H000W8_A329Perfil_GamId
               }
               , new Object[] {
               H000W9_A3Perfil_Codigo, H000W9_A1Usuario_Codigo, H000W9_A329Perfil_GamId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV41GXLvl13 ;
      private short nGXWrapped ;
      private int AV7Usuario_Codigo ;
      private int wcpOAV7Usuario_Codigo ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int lblTbjava_Visible ;
      private int edtavUsuario_gamguid_Visible ;
      private int AV24NotAssociatedRecords ;
      private int AV25AssociatedRecords ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int AV38AreaTrabalho_Codigo ;
      private int A3Perfil_Codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV14i ;
      private int AV44GXV1 ;
      private int AV8Perfil_Codigo ;
      private int AV45GXV2 ;
      private int AV46GXV3 ;
      private int AV49GXV4 ;
      private int AV53GXV6 ;
      private int AV26InsertIndex ;
      private int AV54GXV7 ;
      private int AV55GXV8 ;
      private int AV56GXV9 ;
      private int AV57GXV10 ;
      private int idxLst ;
      private long A329Perfil_GamId ;
      private long AV27Perfil_GamId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String edtavUsuario_gamguid_Internalname ;
      private String AV31Usuario_GamGuid ;
      private String edtavUsuario_gamguid_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String scmdbuf ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String A341Usuario_UserGamGuid ;
      private String A4Perfil_Nome ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n58Usuario_PessoaNom ;
      private bool returnInSub ;
      private bool AV10Exist ;
      private bool AV12Success ;
      private bool AV30isOk ;
      private String AV16AddedKeyListXml ;
      private String AV17NotAddedKeyListXml ;
      private String AV18AddedDscListXml ;
      private String AV19NotAddedDscListXml ;
      private String AV13Description ;
      private IGxSession AV37WebSession ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H000W2_A57Usuario_PessoaCod ;
      private int[] H000W2_A1Usuario_Codigo ;
      private String[] H000W2_A58Usuario_PessoaNom ;
      private bool[] H000W2_n58Usuario_PessoaNom ;
      private int[] H000W3_A1Usuario_Codigo ;
      private String[] H000W3_A341Usuario_UserGamGuid ;
      private int[] H000W4_A3Perfil_Codigo ;
      private int[] H000W4_A7Perfil_AreaTrabalhoCod ;
      private String[] H000W4_A4Perfil_Nome ;
      private int[] H000W5_A3Perfil_Codigo ;
      private int[] H000W5_A1Usuario_Codigo ;
      private int[] H000W6_A3Perfil_Codigo ;
      private long[] H000W6_A329Perfil_GamId ;
      private int[] H000W7_A3Perfil_Codigo ;
      private int[] H000W7_A1Usuario_Codigo ;
      private long[] H000W7_A329Perfil_GamId ;
      private int[] H000W8_A3Perfil_Codigo ;
      private long[] H000W8_A329Perfil_GamId ;
      private int[] H000W9_A3Perfil_Codigo ;
      private int[] H000W9_A1Usuario_Codigo ;
      private long[] H000W9_A329Perfil_GamId ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV21NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV28Errors ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV52GXV5 ;
      private SdtGAMError AV32Error ;
      private SdtGAMUser AV29Gamuser ;
      private SdtMessages_Message AV15Message ;
      private SdtUsuarioPerfil AV11UsuarioPerfil ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wp_associationusuariousuarioperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000W2 ;
          prmH000W2 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000W3 ;
          prmH000W3 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000W4 ;
          prmH000W4 = new Object[] {
          new Object[] {"@AV38AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000W5 ;
          prmH000W5 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000W6 ;
          prmH000W6 = new Object[] {
          new Object[] {"@AV8Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000W7 ;
          prmH000W7 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000W8 ;
          prmH000W8 = new Object[] {
          new Object[] {"@AV8Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000W9 ;
          prmH000W9 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000W2", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV7Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W2,1,0,true,true )
             ,new CursorDef("H000W3", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV7Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W3,1,0,false,true )
             ,new CursorDef("H000W4", "SELECT [Perfil_Codigo], [Perfil_AreaTrabalhoCod], [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_AreaTrabalhoCod] = @AV38AreaTrabalho_Codigo ORDER BY [Perfil_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W4,100,0,true,false )
             ,new CursorDef("H000W5", "SELECT [Perfil_Codigo], [Usuario_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV7Usuario_Codigo and [Perfil_Codigo] = @Perfil_Codigo ORDER BY [Usuario_Codigo], [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W5,1,0,false,true )
             ,new CursorDef("H000W6", "SELECT [Perfil_Codigo], [Perfil_GamId] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @AV8Perfil_Codigo ORDER BY [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W6,1,0,false,true )
             ,new CursorDef("H000W7", "SELECT T1.[Perfil_Codigo], T1.[Usuario_Codigo], T2.[Perfil_GamId] FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE T1.[Usuario_Codigo] = @AV7Usuario_Codigo and T1.[Perfil_Codigo] = @AV8Perfil_Codigo ORDER BY T1.[Usuario_Codigo], T1.[Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W7,1,0,false,true )
             ,new CursorDef("H000W8", "SELECT [Perfil_Codigo], [Perfil_GamId] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @AV8Perfil_Codigo ORDER BY [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W8,1,0,false,true )
             ,new CursorDef("H000W9", "SELECT T1.[Perfil_Codigo], T1.[Usuario_Codigo], T2.[Perfil_GamId] FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE T1.[Usuario_Codigo] = @AV7Usuario_Codigo and T1.[Perfil_Codigo] = @AV8Perfil_Codigo ORDER BY T1.[Usuario_Codigo], T1.[Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W9,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
