/*
               File: type_SdtContratoSistemas
        Description: Sistemas atendidos pelo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:41.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoSistemas" )]
   [XmlType(TypeName =  "ContratoSistemas" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratoSistemas : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoSistemas( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoSistemas_Mode = "";
      }

      public SdtContratoSistemas( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1725ContratoSistemas_CntCod ,
                        int AV1726ContratoSistemas_SistemaCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1725ContratoSistemas_CntCod,(int)AV1726ContratoSistemas_SistemaCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoSistemas_CntCod", typeof(int)}, new Object[]{"ContratoSistemas_SistemaCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoSistemas");
         metadata.Set("BT", "ContratoSistemas");
         metadata.Set("PK", "[ \"ContratoSistemas_CntCod\",\"ContratoSistemas_SistemaCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContratoSistemas_CntCod\",\"ContratoSistemas_SistemaCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contrato_Codigo\" ],\"FKMap\":[ \"ContratoSistemas_CntCod-Contrato_Codigo\" ] },{ \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[ \"ContratoSistemas_SistemaCod-Sistema_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratosistemas_cntcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratosistemas_sistemacod_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoSistemas deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoSistemas)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoSistemas obj ;
         obj = this;
         obj.gxTpr_Contratosistemas_cntcod = deserialized.gxTpr_Contratosistemas_cntcod;
         obj.gxTpr_Contratosistemas_sistemacod = deserialized.gxTpr_Contratosistemas_sistemacod;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratosistemas_cntcod_Z = deserialized.gxTpr_Contratosistemas_cntcod_Z;
         obj.gxTpr_Contratosistemas_sistemacod_Z = deserialized.gxTpr_Contratosistemas_sistemacod_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoSistemas_CntCod") )
               {
                  gxTv_SdtContratoSistemas_Contratosistemas_cntcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoSistemas_SistemaCod") )
               {
                  gxTv_SdtContratoSistemas_Contratosistemas_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoSistemas_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoSistemas_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoSistemas_CntCod_Z") )
               {
                  gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoSistemas_SistemaCod_Z") )
               {
                  gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoSistemas";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoSistemas_CntCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoSistemas_Contratosistemas_cntcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoSistemas_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoSistemas_Contratosistemas_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoSistemas_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoSistemas_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoSistemas_CntCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoSistemas_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoSistemas_CntCod", gxTv_SdtContratoSistemas_Contratosistemas_cntcod, false);
         AddObjectProperty("ContratoSistemas_SistemaCod", gxTv_SdtContratoSistemas_Contratosistemas_sistemacod, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoSistemas_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoSistemas_Initialized, false);
            AddObjectProperty("ContratoSistemas_CntCod_Z", gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z, false);
            AddObjectProperty("ContratoSistemas_SistemaCod_Z", gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoSistemas_CntCod" )]
      [  XmlElement( ElementName = "ContratoSistemas_CntCod"   )]
      public int gxTpr_Contratosistemas_cntcod
      {
         get {
            return gxTv_SdtContratoSistemas_Contratosistemas_cntcod ;
         }

         set {
            if ( gxTv_SdtContratoSistemas_Contratosistemas_cntcod != value )
            {
               gxTv_SdtContratoSistemas_Mode = "INS";
               this.gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z_SetNull( );
               this.gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z_SetNull( );
            }
            gxTv_SdtContratoSistemas_Contratosistemas_cntcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoSistemas_SistemaCod" )]
      [  XmlElement( ElementName = "ContratoSistemas_SistemaCod"   )]
      public int gxTpr_Contratosistemas_sistemacod
      {
         get {
            return gxTv_SdtContratoSistemas_Contratosistemas_sistemacod ;
         }

         set {
            if ( gxTv_SdtContratoSistemas_Contratosistemas_sistemacod != value )
            {
               gxTv_SdtContratoSistemas_Mode = "INS";
               this.gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z_SetNull( );
               this.gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z_SetNull( );
            }
            gxTv_SdtContratoSistemas_Contratosistemas_sistemacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoSistemas_Mode ;
         }

         set {
            gxTv_SdtContratoSistemas_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoSistemas_Mode_SetNull( )
      {
         gxTv_SdtContratoSistemas_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoSistemas_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoSistemas_Initialized ;
         }

         set {
            gxTv_SdtContratoSistemas_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoSistemas_Initialized_SetNull( )
      {
         gxTv_SdtContratoSistemas_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoSistemas_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoSistemas_CntCod_Z" )]
      [  XmlElement( ElementName = "ContratoSistemas_CntCod_Z"   )]
      public int gxTpr_Contratosistemas_cntcod_Z
      {
         get {
            return gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z ;
         }

         set {
            gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z_SetNull( )
      {
         gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoSistemas_SistemaCod_Z" )]
      [  XmlElement( ElementName = "ContratoSistemas_SistemaCod_Z"   )]
      public int gxTpr_Contratosistemas_sistemacod_Z
      {
         get {
            return gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z ;
         }

         set {
            gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z_SetNull( )
      {
         gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoSistemas_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratosistemas", "GeneXus.Programs.contratosistemas_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoSistemas_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoSistemas_Contratosistemas_cntcod ;
      private int gxTv_SdtContratoSistemas_Contratosistemas_sistemacod ;
      private int gxTv_SdtContratoSistemas_Contratosistemas_cntcod_Z ;
      private int gxTv_SdtContratoSistemas_Contratosistemas_sistemacod_Z ;
      private String gxTv_SdtContratoSistemas_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoSistemas", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratoSistemas_RESTInterface : GxGenericCollectionItem<SdtContratoSistemas>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoSistemas_RESTInterface( ) : base()
      {
      }

      public SdtContratoSistemas_RESTInterface( SdtContratoSistemas psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoSistemas_CntCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratosistemas_cntcod
      {
         get {
            return sdt.gxTpr_Contratosistemas_cntcod ;
         }

         set {
            sdt.gxTpr_Contratosistemas_cntcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoSistemas_SistemaCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratosistemas_sistemacod
      {
         get {
            return sdt.gxTpr_Contratosistemas_sistemacod ;
         }

         set {
            sdt.gxTpr_Contratosistemas_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContratoSistemas sdt
      {
         get {
            return (SdtContratoSistemas)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoSistemas() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 6 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
