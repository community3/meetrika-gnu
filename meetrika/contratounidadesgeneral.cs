/*
               File: ContratoUnidadesGeneral
        Description: Contrato Unidades General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:29:55.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratounidadesgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratounidadesgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratounidadesgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoUnidades_ContratoCod )
      {
         this.A1207ContratoUnidades_ContratoCod = aP0_ContratoUnidades_ContratoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1207ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1207ContratoUnidades_ContratoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAIO2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContratoUnidadesGeneral";
               context.Gx_err = 0;
               WSIO2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Unidades General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120295560");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratounidadesgeneral.aspx") + "?" + UrlEncode("" +A1207ContratoUnidades_ContratoCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1207ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOUNIDADES_PRODUTIVIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoUnidadesGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratounidadesgeneral:[SendSecurityCheck value for]"+"ContratoUnidades_UndMedCod:"+context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormIO2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratounidadesgeneral.js", "?20203120295561");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoUnidadesGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Unidades General" ;
      }

      protected void WBIO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratounidadesgeneral.aspx");
            }
            wb_table1_2_IO2( true) ;
         }
         else
         {
            wb_table1_2_IO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IO2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoUnidades_ContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoUnidades_ContratoCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoUnidades_ContratoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoUnidadesGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoUnidades_UndMedCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoUnidades_UndMedCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoUnidades_UndMedCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoUnidadesGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTIO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Unidades General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPIO0( ) ;
            }
         }
      }

      protected void WSIO2( )
      {
         STARTIO2( ) ;
         EVTIO2( ) ;
      }

      protected void EVTIO2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11IO2 */
                                    E11IO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12IO2 */
                                    E12IO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13IO2 */
                                    E13IO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14IO2 */
                                    E14IO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEIO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIO2( ) ;
            }
         }
      }

      protected void PAIO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoUnidadesGeneral";
         context.Gx_err = 0;
      }

      protected void RFIO2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00IO2 */
            pr_default.execute(0, new Object[] {A1207ContratoUnidades_ContratoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1204ContratoUnidades_UndMedCod = H00IO2_A1204ContratoUnidades_UndMedCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
               A1206ContratoUnidades_UndMedSigla = H00IO2_A1206ContratoUnidades_UndMedSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
               n1206ContratoUnidades_UndMedSigla = H00IO2_n1206ContratoUnidades_UndMedSigla[0];
               A1208ContratoUnidades_Produtividade = H00IO2_A1208ContratoUnidades_Produtividade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( A1208ContratoUnidades_Produtividade, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOUNIDADES_PRODUTIVIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
               n1208ContratoUnidades_Produtividade = H00IO2_n1208ContratoUnidades_Produtividade[0];
               A1205ContratoUnidades_UndMedNom = H00IO2_A1205ContratoUnidades_UndMedNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1205ContratoUnidades_UndMedNom", A1205ContratoUnidades_UndMedNom);
               n1205ContratoUnidades_UndMedNom = H00IO2_n1205ContratoUnidades_UndMedNom[0];
               A1206ContratoUnidades_UndMedSigla = H00IO2_A1206ContratoUnidades_UndMedSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
               n1206ContratoUnidades_UndMedSigla = H00IO2_n1206ContratoUnidades_UndMedSigla[0];
               A1205ContratoUnidades_UndMedNom = H00IO2_A1205ContratoUnidades_UndMedNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1205ContratoUnidades_UndMedNom", A1205ContratoUnidades_UndMedNom);
               n1205ContratoUnidades_UndMedNom = H00IO2_n1205ContratoUnidades_UndMedNom[0];
               /* Execute user event: E12IO2 */
               E12IO2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            WBIO0( ) ;
         }
      }

      protected void STRUPIO0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContratoUnidadesGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11IO2 */
         E11IO2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1205ContratoUnidades_UndMedNom = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedNom_Internalname));
            n1205ContratoUnidades_UndMedNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1205ContratoUnidades_UndMedNom", A1205ContratoUnidades_UndMedNom);
            A1208ContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtContratoUnidades_Produtividade_Internalname), ",", ".");
            n1208ContratoUnidades_Produtividade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( A1208ContratoUnidades_Produtividade, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOUNIDADES_PRODUTIVIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
            A1206ContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedSigla_Internalname));
            n1206ContratoUnidades_UndMedSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
            A1204ContratoUnidades_UndMedCod = (int)(context.localUtil.CToN( cgiGet( edtContratoUnidades_UndMedCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1207ContratoUnidades_ContratoCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoUnidadesGeneral";
            A1204ContratoUnidades_UndMedCod = (int)(context.localUtil.CToN( cgiGet( edtContratoUnidades_UndMedCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratounidadesgeneral:[SecurityCheckFailed value for]"+"ContratoUnidades_UndMedCod:"+context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11IO2 */
         E11IO2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11IO2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12IO2( )
      {
         /* Load Routine */
         edtContratoUnidades_UndMedNom_Link = formatLink("viewunidademedicao.aspx") + "?" + UrlEncode("" +A1204ContratoUnidades_UndMedCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoUnidades_UndMedNom_Internalname, "Link", edtContratoUnidades_UndMedNom_Link);
         edtContratoUnidades_ContratoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoUnidades_ContratoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoUnidades_ContratoCod_Visible), 5, 0)));
         edtContratoUnidades_UndMedCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoUnidades_UndMedCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoUnidades_UndMedCod_Visible), 5, 0)));
      }

      protected void E13IO2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode("" +A1204ContratoUnidades_UndMedCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E14IO2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode("" +A1204ContratoUnidades_UndMedCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoUnidades";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoUnidades_ContratoCod";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_IO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_IO2( true) ;
         }
         else
         {
            wb_table2_8_IO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_IO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_IO2( true) ;
         }
         else
         {
            wb_table3_26_IO2( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_IO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IO2e( true) ;
         }
         else
         {
            wb_table1_2_IO2e( false) ;
         }
      }

      protected void wb_table3_26_IO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoUnidadesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoUnidadesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_IO2e( true) ;
         }
         else
         {
            wb_table3_26_IO2e( false) ;
         }
      }

      protected void wb_table2_8_IO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratounidades_undmednom_Internalname, "Unidade de Contratação", "", "", lblTextblockcontratounidades_undmednom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoUnidadesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoUnidades_UndMedNom_Internalname, StringUtil.RTrim( A1205ContratoUnidades_UndMedNom), StringUtil.RTrim( context.localUtil.Format( A1205ContratoUnidades_UndMedNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratoUnidades_UndMedNom_Link, "", "", "", edtContratoUnidades_UndMedNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoUnidadesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratounidades_produtividade_Internalname, "Produtividade diaria", "", "", lblTextblockcontratounidades_produtividade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoUnidadesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table4_18_IO2( true) ;
         }
         else
         {
            wb_table4_18_IO2( false) ;
         }
         return  ;
      }

      protected void wb_table4_18_IO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_IO2e( true) ;
         }
         else
         {
            wb_table2_8_IO2e( false) ;
         }
      }

      protected void wb_table4_18_IO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratounidades_produtividade_Internalname, tblTablemergedcontratounidades_produtividade_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoUnidades_Produtividade_Internalname, StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ",", "")), context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoUnidades_Produtividade_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoUnidadesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoUnidades_UndMedSigla_Internalname, StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla), StringUtil.RTrim( context.localUtil.Format( A1206ContratoUnidades_UndMedSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoUnidades_UndMedSigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoUnidadesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_18_IO2e( true) ;
         }
         else
         {
            wb_table4_18_IO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1207ContratoUnidades_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIO2( ) ;
         WSIO2( ) ;
         WEIO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1207ContratoUnidades_ContratoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAIO2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratounidadesgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAIO2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1207ContratoUnidades_ContratoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
         }
         wcpOA1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1207ContratoUnidades_ContratoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1207ContratoUnidades_ContratoCod != wcpOA1207ContratoUnidades_ContratoCod ) ) )
         {
            setjustcreated();
         }
         wcpOA1207ContratoUnidades_ContratoCod = A1207ContratoUnidades_ContratoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1207ContratoUnidades_ContratoCod = cgiGet( sPrefix+"A1207ContratoUnidades_ContratoCod_CTRL");
         if ( StringUtil.Len( sCtrlA1207ContratoUnidades_ContratoCod) > 0 )
         {
            A1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA1207ContratoUnidades_ContratoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
         }
         else
         {
            A1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1207ContratoUnidades_ContratoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAIO2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSIO2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSIO2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1207ContratoUnidades_ContratoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1207ContratoUnidades_ContratoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1207ContratoUnidades_ContratoCod_CTRL", StringUtil.RTrim( sCtrlA1207ContratoUnidades_ContratoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEIO2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120295581");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratounidadesgeneral.js", "?20203120295581");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratounidades_undmednom_Internalname = sPrefix+"TEXTBLOCKCONTRATOUNIDADES_UNDMEDNOM";
         edtContratoUnidades_UndMedNom_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDNOM";
         lblTextblockcontratounidades_produtividade_Internalname = sPrefix+"TEXTBLOCKCONTRATOUNIDADES_PRODUTIVIDADE";
         edtContratoUnidades_Produtividade_Internalname = sPrefix+"CONTRATOUNIDADES_PRODUTIVIDADE";
         edtContratoUnidades_UndMedSigla_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDSIGLA";
         tblTablemergedcontratounidades_produtividade_Internalname = sPrefix+"TABLEMERGEDCONTRATOUNIDADES_PRODUTIVIDADE";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratoUnidades_ContratoCod_Internalname = sPrefix+"CONTRATOUNIDADES_CONTRATOCOD";
         edtContratoUnidades_UndMedCod_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoUnidades_UndMedSigla_Jsonclick = "";
         edtContratoUnidades_Produtividade_Jsonclick = "";
         edtContratoUnidades_UndMedNom_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratoUnidades_UndMedNom_Link = "";
         edtContratoUnidades_UndMedCod_Jsonclick = "";
         edtContratoUnidades_UndMedCod_Visible = 1;
         edtContratoUnidades_ContratoCod_Jsonclick = "";
         edtContratoUnidades_ContratoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13IO2',iparms:[{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14IO2',iparms:[{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00IO2_A1207ContratoUnidades_ContratoCod = new int[1] ;
         H00IO2_A1204ContratoUnidades_UndMedCod = new int[1] ;
         H00IO2_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         H00IO2_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         H00IO2_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         H00IO2_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         H00IO2_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         H00IO2_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         A1206ContratoUnidades_UndMedSigla = "";
         A1205ContratoUnidades_UndMedNom = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratounidades_undmednom_Jsonclick = "";
         lblTextblockcontratounidades_produtividade_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1207ContratoUnidades_ContratoCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratounidadesgeneral__default(),
            new Object[][] {
                new Object[] {
               H00IO2_A1207ContratoUnidades_ContratoCod, H00IO2_A1204ContratoUnidades_UndMedCod, H00IO2_A1206ContratoUnidades_UndMedSigla, H00IO2_n1206ContratoUnidades_UndMedSigla, H00IO2_A1208ContratoUnidades_Produtividade, H00IO2_n1208ContratoUnidades_Produtividade, H00IO2_A1205ContratoUnidades_UndMedNom, H00IO2_n1205ContratoUnidades_UndMedNom
               }
            }
         );
         AV14Pgmname = "ContratoUnidadesGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoUnidadesGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int wcpOA1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int edtContratoUnidades_ContratoCod_Visible ;
      private int edtContratoUnidades_UndMedCod_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoUnidades_ContratoCod ;
      private int idxLst ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContratoUnidades_ContratoCod_Internalname ;
      private String edtContratoUnidades_ContratoCod_Jsonclick ;
      private String edtContratoUnidades_UndMedCod_Internalname ;
      private String edtContratoUnidades_UndMedCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String edtContratoUnidades_UndMedNom_Internalname ;
      private String edtContratoUnidades_Produtividade_Internalname ;
      private String edtContratoUnidades_UndMedSigla_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContratoUnidades_UndMedNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratounidades_undmednom_Internalname ;
      private String lblTextblockcontratounidades_undmednom_Jsonclick ;
      private String edtContratoUnidades_UndMedNom_Jsonclick ;
      private String lblTextblockcontratounidades_produtividade_Internalname ;
      private String lblTextblockcontratounidades_produtividade_Jsonclick ;
      private String tblTablemergedcontratounidades_produtividade_Internalname ;
      private String edtContratoUnidades_Produtividade_Jsonclick ;
      private String edtContratoUnidades_UndMedSigla_Jsonclick ;
      private String sCtrlA1207ContratoUnidades_ContratoCod ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00IO2_A1207ContratoUnidades_ContratoCod ;
      private int[] H00IO2_A1204ContratoUnidades_UndMedCod ;
      private String[] H00IO2_A1206ContratoUnidades_UndMedSigla ;
      private bool[] H00IO2_n1206ContratoUnidades_UndMedSigla ;
      private decimal[] H00IO2_A1208ContratoUnidades_Produtividade ;
      private bool[] H00IO2_n1208ContratoUnidades_Produtividade ;
      private String[] H00IO2_A1205ContratoUnidades_UndMedNom ;
      private bool[] H00IO2_n1205ContratoUnidades_UndMedNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratounidadesgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IO2 ;
          prmH00IO2 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IO2", "SELECT T1.[ContratoUnidades_ContratoCod], T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod]) WHERE T1.[ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod ORDER BY T1.[ContratoUnidades_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IO2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
