/*
               File: CaixaGeneral
        Description: Caixa General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:55.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class caixageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public caixageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public caixageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Caixa_Codigo )
      {
         this.A874Caixa_Codigo = aP0_Caixa_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A874Caixa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A874Caixa_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"CAIXA_TIPODECONTACOD") == 0 )
               {
                  A870TipodeConta_Codigo = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXSGACAIXA_TIPODECONTACODFR0( A870TipodeConta_Codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAFR2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "CaixaGeneral";
               context.Gx_err = 0;
               WSFR2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Caixa General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117235558");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("caixageneral.aspx") + "?" + UrlEncode("" +A874Caixa_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA874Caixa_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA874Caixa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CAIXA_DOCUMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CAIXA_EMISSAO", GetSecureSignedToken( sPrefix, A876Caixa_Emissao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CAIXA_VENCIMENTO", GetSecureSignedToken( sPrefix, A877Caixa_Vencimento));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CAIXA_TIPODECONTACOD", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A881Caixa_TipoDeContaCod, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CAIXA_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CAIXA_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormFR2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("caixageneral.js", "?20203117235560");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "CaixaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Caixa General" ;
      }

      protected void WBFR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "caixageneral.aspx");
            }
            wb_table1_2_FR2( true) ;
         }
         else
         {
            wb_table1_2_FR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCaixa_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A874Caixa_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A874Caixa_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtCaixa_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_CaixaGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTFR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Caixa General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPFR0( ) ;
            }
         }
      }

      protected void WSFR2( )
      {
         STARTFR2( ) ;
         EVTFR2( ) ;
      }

      protected void EVTFR2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11FR2 */
                                    E11FR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12FR2 */
                                    E12FR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13FR2 */
                                    E13FR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14FR2 */
                                    E14FR2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFR0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormFR2( ) ;
            }
         }
      }

      protected void PAFR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXSGACAIXA_TIPODECONTACODFR0( String A870TipodeConta_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGACAIXA_TIPODECONTACOD_dataFR0( A870TipodeConta_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGACAIXA_TIPODECONTACOD_dataFR0( String A870TipodeConta_Codigo )
      {
         l870TipodeConta_Codigo = StringUtil.PadR( StringUtil.RTrim( A870TipodeConta_Codigo), 20, "%");
         /* Using cursor H00FR2 */
         pr_default.execute(0, new Object[] {l870TipodeConta_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FR2_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FR2_A870TipodeConta_Codigo[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "CaixaGeneral";
         context.Gx_err = 0;
      }

      protected void RFFR2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00FR3 */
            pr_default.execute(1, new Object[] {A874Caixa_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A880Caixa_Valor = H00FR3_A880Caixa_Valor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A880Caixa_Valor", StringUtil.LTrim( StringUtil.Str( A880Caixa_Valor, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A879Caixa_Descricao = H00FR3_A879Caixa_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A879Caixa_Descricao", A879Caixa_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!"))));
               n879Caixa_Descricao = H00FR3_n879Caixa_Descricao[0];
               A881Caixa_TipoDeContaCod = H00FR3_A881Caixa_TipoDeContaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A881Caixa_TipoDeContaCod", A881Caixa_TipoDeContaCod);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_TIPODECONTACOD", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A881Caixa_TipoDeContaCod, ""))));
               A877Caixa_Vencimento = H00FR3_A877Caixa_Vencimento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A877Caixa_Vencimento", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_VENCIMENTO", GetSecureSignedToken( sPrefix, A877Caixa_Vencimento));
               n877Caixa_Vencimento = H00FR3_n877Caixa_Vencimento[0];
               A876Caixa_Emissao = H00FR3_A876Caixa_Emissao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A876Caixa_Emissao", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_EMISSAO", GetSecureSignedToken( sPrefix, A876Caixa_Emissao));
               n876Caixa_Emissao = H00FR3_n876Caixa_Emissao[0];
               A875Caixa_Documento = H00FR3_A875Caixa_Documento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A875Caixa_Documento", A875Caixa_Documento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_DOCUMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!"))));
               /* Execute user event: E12FR2 */
               E12FR2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBFR0( ) ;
         }
      }

      protected void STRUPFR0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "CaixaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11FR2 */
         E11FR2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A875Caixa_Documento = StringUtil.Upper( cgiGet( edtCaixa_Documento_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A875Caixa_Documento", A875Caixa_Documento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_DOCUMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!"))));
            A876Caixa_Emissao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtCaixa_Emissao_Internalname), 0));
            n876Caixa_Emissao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A876Caixa_Emissao", context.localUtil.Format(A876Caixa_Emissao, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_EMISSAO", GetSecureSignedToken( sPrefix, A876Caixa_Emissao));
            A877Caixa_Vencimento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtCaixa_Vencimento_Internalname), 0));
            n877Caixa_Vencimento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A877Caixa_Vencimento", context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_VENCIMENTO", GetSecureSignedToken( sPrefix, A877Caixa_Vencimento));
            A881Caixa_TipoDeContaCod = cgiGet( edtCaixa_TipoDeContaCod_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A881Caixa_TipoDeContaCod", A881Caixa_TipoDeContaCod);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_TIPODECONTACOD", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A881Caixa_TipoDeContaCod, ""))));
            A879Caixa_Descricao = StringUtil.Upper( cgiGet( edtCaixa_Descricao_Internalname));
            n879Caixa_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A879Caixa_Descricao", A879Caixa_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!"))));
            A880Caixa_Valor = context.localUtil.CToN( cgiGet( edtCaixa_Valor_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A880Caixa_Valor", StringUtil.LTrim( StringUtil.Str( A880Caixa_Valor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CAIXA_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            /* Read saved values. */
            wcpOA874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA874Caixa_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11FR2 */
         E11FR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11FR2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12FR2( )
      {
         /* Load Routine */
         edtCaixa_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtCaixa_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCaixa_Codigo_Visible), 5, 0)));
      }

      protected void E13FR2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("caixa.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A874Caixa_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14FR2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("caixa.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A874Caixa_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Caixa";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Caixa_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Caixa_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_FR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_FR2( true) ;
         }
         else
         {
            wb_table2_8_FR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_FR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_43_FR2( true) ;
         }
         else
         {
            wb_table3_43_FR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_43_FR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FR2e( true) ;
         }
         else
         {
            wb_table1_2_FR2e( false) ;
         }
      }

      protected void wb_table3_43_FR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_43_FR2e( true) ;
         }
         else
         {
            wb_table3_43_FR2e( false) ;
         }
      }

      protected void wb_table2_8_FR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_documento_Internalname, "Documento", "", "", lblTextblockcaixa_documento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCaixa_Documento_Internalname, A875Caixa_Documento, StringUtil.RTrim( context.localUtil.Format( A875Caixa_Documento, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Documento_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_emissao_Internalname, "Data de Emiss�o", "", "", lblTextblockcaixa_emissao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table4_18_FR2( true) ;
         }
         else
         {
            wb_table4_18_FR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_18_FR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_tipodecontacod_Internalname, "Conta", "", "", lblTextblockcaixa_tipodecontacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCaixa_TipoDeContaCod_Internalname, StringUtil.RTrim( A881Caixa_TipoDeContaCod), StringUtil.RTrim( context.localUtil.Format( A881Caixa_TipoDeContaCod, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_TipoDeContaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_descricao_Internalname, "Descri��o", "", "", lblTextblockcaixa_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCaixa_Descricao_Internalname, A879Caixa_Descricao, StringUtil.RTrim( context.localUtil.Format( A879Caixa_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_valor_Internalname, "Valor", "", "", lblTextblockcaixa_valor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCaixa_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A880Caixa_Valor, 18, 5, ",", "")), context.localUtil.Format( A880Caixa_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Valor_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_FR2e( true) ;
         }
         else
         {
            wb_table2_8_FR2e( false) ;
         }
      }

      protected void wb_table4_18_FR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcaixa_emissao_Internalname, tblTablemergedcaixa_emissao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtCaixa_Emissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtCaixa_Emissao_Internalname, context.localUtil.Format(A876Caixa_Emissao, "99/99/99"), context.localUtil.Format( A876Caixa_Emissao, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Emissao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CaixaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtCaixa_Emissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CaixaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcaixa_vencimento_Internalname, "Data de Vencimento", "", "", lblTextblockcaixa_vencimento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CaixaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtCaixa_Vencimento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtCaixa_Vencimento_Internalname, context.localUtil.Format(A877Caixa_Vencimento, "99/99/99"), context.localUtil.Format( A877Caixa_Vencimento, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCaixa_Vencimento_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CaixaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtCaixa_Vencimento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CaixaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_18_FR2e( true) ;
         }
         else
         {
            wb_table4_18_FR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A874Caixa_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFR2( ) ;
         WSFR2( ) ;
         WEFR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA874Caixa_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAFR2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "caixageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAFR2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A874Caixa_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
         }
         wcpOA874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA874Caixa_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A874Caixa_Codigo != wcpOA874Caixa_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA874Caixa_Codigo = A874Caixa_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA874Caixa_Codigo = cgiGet( sPrefix+"A874Caixa_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA874Caixa_Codigo) > 0 )
         {
            A874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA874Caixa_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A874Caixa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A874Caixa_Codigo), 6, 0)));
         }
         else
         {
            A874Caixa_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A874Caixa_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAFR2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSFR2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSFR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A874Caixa_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A874Caixa_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA874Caixa_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A874Caixa_Codigo_CTRL", StringUtil.RTrim( sCtrlA874Caixa_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEFR2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311723563");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("caixageneral.js", "?2020311723563");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcaixa_documento_Internalname = sPrefix+"TEXTBLOCKCAIXA_DOCUMENTO";
         edtCaixa_Documento_Internalname = sPrefix+"CAIXA_DOCUMENTO";
         lblTextblockcaixa_emissao_Internalname = sPrefix+"TEXTBLOCKCAIXA_EMISSAO";
         edtCaixa_Emissao_Internalname = sPrefix+"CAIXA_EMISSAO";
         lblTextblockcaixa_vencimento_Internalname = sPrefix+"TEXTBLOCKCAIXA_VENCIMENTO";
         edtCaixa_Vencimento_Internalname = sPrefix+"CAIXA_VENCIMENTO";
         tblTablemergedcaixa_emissao_Internalname = sPrefix+"TABLEMERGEDCAIXA_EMISSAO";
         lblTextblockcaixa_tipodecontacod_Internalname = sPrefix+"TEXTBLOCKCAIXA_TIPODECONTACOD";
         edtCaixa_TipoDeContaCod_Internalname = sPrefix+"CAIXA_TIPODECONTACOD";
         lblTextblockcaixa_descricao_Internalname = sPrefix+"TEXTBLOCKCAIXA_DESCRICAO";
         edtCaixa_Descricao_Internalname = sPrefix+"CAIXA_DESCRICAO";
         lblTextblockcaixa_valor_Internalname = sPrefix+"TEXTBLOCKCAIXA_VALOR";
         edtCaixa_Valor_Internalname = sPrefix+"CAIXA_VALOR";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtCaixa_Codigo_Internalname = sPrefix+"CAIXA_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtCaixa_Vencimento_Jsonclick = "";
         edtCaixa_Emissao_Jsonclick = "";
         edtCaixa_Valor_Jsonclick = "";
         edtCaixa_Descricao_Jsonclick = "";
         edtCaixa_TipoDeContaCod_Jsonclick = "";
         edtCaixa_Documento_Jsonclick = "";
         edtCaixa_Codigo_Jsonclick = "";
         edtCaixa_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13FR2',iparms:[{av:'A874Caixa_Codigo',fld:'CAIXA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14FR2',iparms:[{av:'A874Caixa_Codigo',fld:'CAIXA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A870TipodeConta_Codigo = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A875Caixa_Documento = "";
         A876Caixa_Emissao = DateTime.MinValue;
         A877Caixa_Vencimento = DateTime.MinValue;
         A881Caixa_TipoDeContaCod = "";
         A879Caixa_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         l870TipodeConta_Codigo = "";
         H00FR2_A870TipodeConta_Codigo = new String[] {""} ;
         H00FR3_A874Caixa_Codigo = new int[1] ;
         H00FR3_A880Caixa_Valor = new decimal[1] ;
         H00FR3_A879Caixa_Descricao = new String[] {""} ;
         H00FR3_n879Caixa_Descricao = new bool[] {false} ;
         H00FR3_A881Caixa_TipoDeContaCod = new String[] {""} ;
         H00FR3_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         H00FR3_n877Caixa_Vencimento = new bool[] {false} ;
         H00FR3_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         H00FR3_n876Caixa_Emissao = new bool[] {false} ;
         H00FR3_A875Caixa_Documento = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcaixa_documento_Jsonclick = "";
         lblTextblockcaixa_emissao_Jsonclick = "";
         lblTextblockcaixa_tipodecontacod_Jsonclick = "";
         lblTextblockcaixa_descricao_Jsonclick = "";
         lblTextblockcaixa_valor_Jsonclick = "";
         lblTextblockcaixa_vencimento_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA874Caixa_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.caixageneral__default(),
            new Object[][] {
                new Object[] {
               H00FR2_A870TipodeConta_Codigo
               }
               , new Object[] {
               H00FR3_A874Caixa_Codigo, H00FR3_A880Caixa_Valor, H00FR3_A879Caixa_Descricao, H00FR3_n879Caixa_Descricao, H00FR3_A881Caixa_TipoDeContaCod, H00FR3_A877Caixa_Vencimento, H00FR3_n877Caixa_Vencimento, H00FR3_A876Caixa_Emissao, H00FR3_n876Caixa_Emissao, H00FR3_A875Caixa_Documento
               }
            }
         );
         AV14Pgmname = "CaixaGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "CaixaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A874Caixa_Codigo ;
      private int wcpOA874Caixa_Codigo ;
      private int edtCaixa_Codigo_Visible ;
      private int gxdynajaxindex ;
      private int AV7Caixa_Codigo ;
      private int idxLst ;
      private decimal A880Caixa_Valor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String A870TipodeConta_Codigo ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A881Caixa_TipoDeContaCod ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtCaixa_Codigo_Internalname ;
      private String edtCaixa_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String l870TipodeConta_Codigo ;
      private String edtCaixa_Documento_Internalname ;
      private String edtCaixa_Emissao_Internalname ;
      private String edtCaixa_Vencimento_Internalname ;
      private String edtCaixa_TipoDeContaCod_Internalname ;
      private String edtCaixa_Descricao_Internalname ;
      private String edtCaixa_Valor_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcaixa_documento_Internalname ;
      private String lblTextblockcaixa_documento_Jsonclick ;
      private String edtCaixa_Documento_Jsonclick ;
      private String lblTextblockcaixa_emissao_Internalname ;
      private String lblTextblockcaixa_emissao_Jsonclick ;
      private String lblTextblockcaixa_tipodecontacod_Internalname ;
      private String lblTextblockcaixa_tipodecontacod_Jsonclick ;
      private String edtCaixa_TipoDeContaCod_Jsonclick ;
      private String lblTextblockcaixa_descricao_Internalname ;
      private String lblTextblockcaixa_descricao_Jsonclick ;
      private String edtCaixa_Descricao_Jsonclick ;
      private String lblTextblockcaixa_valor_Internalname ;
      private String lblTextblockcaixa_valor_Jsonclick ;
      private String edtCaixa_Valor_Jsonclick ;
      private String tblTablemergedcaixa_emissao_Internalname ;
      private String edtCaixa_Emissao_Jsonclick ;
      private String lblTextblockcaixa_vencimento_Internalname ;
      private String lblTextblockcaixa_vencimento_Jsonclick ;
      private String edtCaixa_Vencimento_Jsonclick ;
      private String sCtrlA874Caixa_Codigo ;
      private DateTime A876Caixa_Emissao ;
      private DateTime A877Caixa_Vencimento ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n879Caixa_Descricao ;
      private bool n877Caixa_Vencimento ;
      private bool n876Caixa_Emissao ;
      private bool returnInSub ;
      private String A875Caixa_Documento ;
      private String A879Caixa_Descricao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00FR2_A870TipodeConta_Codigo ;
      private int[] H00FR3_A874Caixa_Codigo ;
      private decimal[] H00FR3_A880Caixa_Valor ;
      private String[] H00FR3_A879Caixa_Descricao ;
      private bool[] H00FR3_n879Caixa_Descricao ;
      private String[] H00FR3_A881Caixa_TipoDeContaCod ;
      private DateTime[] H00FR3_A877Caixa_Vencimento ;
      private bool[] H00FR3_n877Caixa_Vencimento ;
      private DateTime[] H00FR3_A876Caixa_Emissao ;
      private bool[] H00FR3_n876Caixa_Emissao ;
      private String[] H00FR3_A875Caixa_Documento ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class caixageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FR2 ;
          prmH00FR2 = new Object[] {
          new Object[] {"@l870TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmH00FR3 ;
          prmH00FR3 = new Object[] {
          new Object[] {"@Caixa_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FR2", "SELECT TOP 5 [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE UPPER([TipodeConta_Codigo]) like UPPER(@l870TipodeConta_Codigo) ORDER BY [TipodeConta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FR2,0,0,true,false )
             ,new CursorDef("H00FR3", "SELECT [Caixa_Codigo], [Caixa_Valor], [Caixa_Descricao], [Caixa_TipoDeContaCod], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Documento] FROM [Caixa] WITH (NOLOCK) WHERE [Caixa_Codigo] = @Caixa_Codigo ORDER BY [Caixa_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FR3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
