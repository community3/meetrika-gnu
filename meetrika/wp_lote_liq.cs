/*
               File: WP_Lote_Liq
        Description: Liquida��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 22:8:7.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_lote_liq : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_lote_liq( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_lote_liq( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Lote_Codigo ,
                           out bool aP1_Confirmado )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         this.AV10Confirmado = false ;
         executePrivate();
         aP0_Lote_Codigo=this.A596Lote_Codigo;
         aP1_Confirmado=this.AV10Confirmado;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A596Lote_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10Confirmado = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Confirmado", AV10Confirmado);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADW2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDW2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020318228773");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_lote_liq.aspx") + "?" + UrlEncode("" +A596Lote_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV10Confirmado))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "LOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_LOTEACEITECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV15Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV15Codigos);
         }
         GxWebStd.gx_hidden_field( context, "vLOTE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Lote_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vLOTE_NOME", StringUtil.RTrim( AV13Lote_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV10Confirmado);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDW2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDW2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_lote_liq.aspx") + "?" + UrlEncode("" +A596Lote_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV10Confirmado)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Lote_Liq" ;
      }

      public override String GetPgmdesc( )
      {
         return "Liquida��o" ;
      }

      protected void WBDW0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DW2( true) ;
         }
         else
         {
            wb_table1_2_DW2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DW2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDW2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Liquida��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDW0( ) ;
      }

      protected void WSDW2( )
      {
         STARTDW2( ) ;
         EVTDW2( ) ;
      }

      protected void EVTDW2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DW2 */
                              E11DW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CONFIRMAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DW2 */
                              E12DW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CANCELAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DW2 */
                              E13DW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14DW2 */
                              E14DW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDW2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADW2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavLote_liqbanco_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDW2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFDW2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00DW2 */
            pr_default.execute(0, new Object[] {A596Lote_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               /* Execute user event: E14DW2 */
               E14DW2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBDW0( ) ;
         }
      }

      protected void STRUPDW0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DW2 */
         E11DW2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV7Lote_LiqBanco = StringUtil.Upper( cgiGet( edtavLote_liqbanco_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Lote_LiqBanco", AV7Lote_LiqBanco);
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_liqdata_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data liquidado"}), 1, "vLOTE_LIQDATA");
               GX_FocusControl = edtavLote_liqdata_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Lote_LiqData = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Lote_LiqData", context.localUtil.Format(AV8Lote_LiqData, "99/99/99"));
            }
            else
            {
               AV8Lote_LiqData = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_liqdata_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Lote_LiqData", context.localUtil.Format(AV8Lote_LiqData, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_liqvalor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_liqvalor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_LIQVALOR");
               GX_FocusControl = edtavLote_liqvalor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9Lote_LiqValor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( AV9Lote_LiqValor, 18, 5)));
            }
            else
            {
               AV9Lote_LiqValor = context.localUtil.CToN( cgiGet( edtavLote_liqvalor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( AV9Lote_LiqValor, 18, 5)));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DW2 */
         E11DW2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11DW2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 18/03/2020 22:07", 0) ;
         AV6Lote_DataNFe = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         /* Using cursor H00DW3 */
         pr_default.execute(1, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A595Lote_AreaTrabalhoCod = H00DW3_A595Lote_AreaTrabalhoCod[0];
            A563Lote_Nome = H00DW3_A563Lote_Nome[0];
            AV12Lote_AreaTrabalhoCod = A595Lote_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Lote_AreaTrabalhoCod), 6, 0)));
            AV13Lote_Nome = A563Lote_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Lote_Nome", AV13Lote_Nome);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void E12DW2( )
      {
         /* 'Confirmar' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV7Lote_LiqBanco)) )
         {
            GX_msglist.addItem("Banco � obrigat�rio!");
         }
         else if ( (DateTime.MinValue==AV8Lote_LiqData) )
         {
            GX_msglist.addItem("Data de liquidado � obrigat�ria!");
         }
         else if ( (Convert.ToDecimal(0)==AV9Lote_LiqValor) )
         {
            GX_msglist.addItem("Valor liquidado � obrigat�ria!");
         }
         else
         {
            AV10Confirmado = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Confirmado", AV10Confirmado);
            /* Execute user subroutine: 'UPDATE' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.setWebReturnParms(new Object[] {(int)A596Lote_Codigo,(bool)AV10Confirmado});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15Codigos", AV15Codigos);
      }

      protected void E13DW2( )
      {
         /* 'Cancelar' Routine */
         context.setWebReturnParms(new Object[] {(int)A596Lote_Codigo,(bool)AV10Confirmado});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'UPDATE' Routine */
         new prc_updloteliq(context ).execute( ref  A596Lote_Codigo, ref  AV7Lote_LiqBanco,  AV8Lote_LiqData,  AV9Lote_LiqValor) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Lote_LiqBanco", AV7Lote_LiqBanco);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Lote_LiqData", context.localUtil.Format(AV8Lote_LiqData, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( AV9Lote_LiqValor, 18, 5)));
         /* Execute user subroutine: 'STATUS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S122( )
      {
         /* 'STATUS' Routine */
         /* Using cursor H00DW4 */
         pr_default.execute(2, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = H00DW4_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = H00DW4_n597ContagemResultado_LoteAceiteCod[0];
            A456ContagemResultado_Codigo = H00DW4_A456ContagemResultado_Codigo[0];
            AV15Codigos.Add(A456ContagemResultado_Codigo, 0);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         AV14WebSession.Set("SdtCodigos", AV15Codigos.ToXml(false, true, "Collection", ""));
         new prc_agruparparafaturamento(context ).execute(  AV12Lote_AreaTrabalhoCod,  5,  "",  "L",  AV13Lote_Nome) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Lote_AreaTrabalhoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Lote_Nome", AV13Lote_Nome);
      }

      protected void nextLoad( )
      {
      }

      protected void E14DW2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_DW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(450), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Banco:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Liq.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_liqbanco_Internalname, StringUtil.RTrim( AV7Lote_LiqBanco), StringUtil.RTrim( context.localUtil.Format( AV7Lote_LiqBanco, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,10);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqbanco_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLote_liqbanco_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_Lote_Liq.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Data:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Liq.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_liqdata_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_liqdata_Internalname, context.localUtil.Format(AV8Lote_LiqData, "99/99/99"), context.localUtil.Format( AV8Lote_LiqData, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqdata_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLote_liqdata_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_Lote_Liq.htm");
            GxWebStd.gx_bitmap( context, edtavLote_liqdata_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavLote_liqdata_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Lote_Liq.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Valor R$:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Liq.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_liqvalor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV9Lote_LiqValor, 18, 5, ",", "")), ((edtavLote_liqvalor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV9Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV9Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_liqvalor_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLote_liqvalor_Enabled, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_Lote_Liq.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:bottom;height:100px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CONFIRMAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Lote_Liq.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Cancelar", bttButton2_Jsonclick, 5, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CANCELAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Lote_Liq.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DW2e( true) ;
         }
         else
         {
            wb_table1_2_DW2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A596Lote_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         AV10Confirmado = (bool)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Confirmado", AV10Confirmado);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADW2( ) ;
         WSDW2( ) ;
         WEDW2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020318228794");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_lote_liq.js", "?2020318228794");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavLote_liqbanco_Internalname = "vLOTE_LIQBANCO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavLote_liqdata_Internalname = "vLOTE_LIQDATA";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavLote_liqvalor_Internalname = "vLOTE_LIQVALOR";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavLote_liqvalor_Jsonclick = "";
         edtavLote_liqvalor_Enabled = 1;
         edtavLote_liqdata_Jsonclick = "";
         edtavLote_liqdata_Enabled = 1;
         edtavLote_liqbanco_Jsonclick = "";
         edtavLote_liqbanco_Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Liquida��o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'CONFIRMAR'","{handler:'E12DW2',iparms:[{av:'AV7Lote_LiqBanco',fld:'vLOTE_LIQBANCO',pic:'@!',nv:''},{av:'AV8Lote_LiqData',fld:'vLOTE_LIQDATA',pic:'',nv:''},{av:'AV9Lote_LiqValor',fld:'vLOTE_LIQVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A597ContagemResultado_LoteAceiteCod',fld:'CONTAGEMRESULTADO_LOTEACEITECOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV12Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV13Lote_Nome',fld:'vLOTE_NOME',pic:'@!',nv:''}],oparms:[{av:'AV10Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'AV7Lote_LiqBanco',fld:'vLOTE_LIQBANCO',pic:'@!',nv:''},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Codigos',fld:'vCODIGOS',pic:'',nv:null}]}");
         setEventMetadata("'CANCELAR'","{handler:'E13DW2',iparms:[{av:'AV10Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV15Codigos = new GxSimpleCollection();
         AV13Lote_Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00DW2_A596Lote_Codigo = new int[1] ;
         AV7Lote_LiqBanco = "";
         AV8Lote_LiqData = DateTime.MinValue;
         AV6Lote_DataNFe = DateTime.MinValue;
         H00DW3_A596Lote_Codigo = new int[1] ;
         H00DW3_A595Lote_AreaTrabalhoCod = new int[1] ;
         H00DW3_A563Lote_Nome = new String[] {""} ;
         A563Lote_Nome = "";
         H00DW4_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00DW4_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00DW4_A456ContagemResultado_Codigo = new int[1] ;
         AV14WebSession = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock2_Jsonclick = "";
         TempTags = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_lote_liq__default(),
            new Object[][] {
                new Object[] {
               H00DW2_A596Lote_Codigo
               }
               , new Object[] {
               H00DW3_A596Lote_Codigo, H00DW3_A595Lote_AreaTrabalhoCod, H00DW3_A563Lote_Nome
               }
               , new Object[] {
               H00DW4_A597ContagemResultado_LoteAceiteCod, H00DW4_n597ContagemResultado_LoteAceiteCod, H00DW4_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A596Lote_Codigo ;
      private int wcpOA596Lote_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV12Lote_AreaTrabalhoCod ;
      private int A595Lote_AreaTrabalhoCod ;
      private int edtavLote_liqbanco_Enabled ;
      private int edtavLote_liqdata_Enabled ;
      private int edtavLote_liqvalor_Enabled ;
      private int idxLst ;
      private decimal AV9Lote_LiqValor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV13Lote_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLote_liqbanco_Internalname ;
      private String scmdbuf ;
      private String AV7Lote_LiqBanco ;
      private String edtavLote_liqdata_Internalname ;
      private String edtavLote_liqvalor_Internalname ;
      private String A563Lote_Nome ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String TempTags ;
      private String edtavLote_liqbanco_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavLote_liqdata_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavLote_liqvalor_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private DateTime AV8Lote_LiqData ;
      private DateTime AV6Lote_DataNFe ;
      private bool entryPointCalled ;
      private bool AV10Confirmado ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Lote_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00DW2_A596Lote_Codigo ;
      private int[] H00DW3_A596Lote_Codigo ;
      private int[] H00DW3_A595Lote_AreaTrabalhoCod ;
      private String[] H00DW3_A563Lote_Nome ;
      private int[] H00DW4_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00DW4_n597ContagemResultado_LoteAceiteCod ;
      private int[] H00DW4_A456ContagemResultado_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private bool aP1_Confirmado ;
      private IGxSession AV14WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV15Codigos ;
      private GXWebForm Form ;
   }

   public class wp_lote_liq__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DW2 ;
          prmH00DW2 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DW3 ;
          prmH00DW3 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DW4 ;
          prmH00DW4 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DW2", "SELECT [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @Lote_Codigo ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DW2,1,0,true,true )
             ,new CursorDef("H00DW3", "SELECT TOP 1 [Lote_Codigo], [Lote_AreaTrabalhoCod], [Lote_Nome] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @Lote_Codigo ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DW3,1,0,false,true )
             ,new CursorDef("H00DW4", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DW4,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
