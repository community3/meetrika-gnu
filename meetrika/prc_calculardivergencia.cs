/*
               File: PRC_CalcularDivergencia
        Description: Calcular Divergencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_calculardivergencia : GXProcedure
   {
      public prc_calculardivergencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_calculardivergencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_CalculoDivergencia ,
                           decimal aP1_Bruto2 ,
                           decimal aP2_Bruto1 ,
                           decimal aP3_Liquido2 ,
                           decimal aP4_Liquido1 ,
                           out decimal aP5_Divergencia )
      {
         this.AV14CalculoDivergencia = aP0_CalculoDivergencia;
         this.AV11Bruto2 = aP1_Bruto2;
         this.AV10Bruto1 = aP2_Bruto1;
         this.AV13Liquido2 = aP3_Liquido2;
         this.AV12Liquido1 = aP4_Liquido1;
         this.AV9Divergencia = 0 ;
         initialize();
         executePrivate();
         aP5_Divergencia=this.AV9Divergencia;
      }

      public decimal executeUdp( String aP0_CalculoDivergencia ,
                                 decimal aP1_Bruto2 ,
                                 decimal aP2_Bruto1 ,
                                 decimal aP3_Liquido2 ,
                                 decimal aP4_Liquido1 )
      {
         this.AV14CalculoDivergencia = aP0_CalculoDivergencia;
         this.AV11Bruto2 = aP1_Bruto2;
         this.AV10Bruto1 = aP2_Bruto1;
         this.AV13Liquido2 = aP3_Liquido2;
         this.AV12Liquido1 = aP4_Liquido1;
         this.AV9Divergencia = 0 ;
         initialize();
         executePrivate();
         aP5_Divergencia=this.AV9Divergencia;
         return AV9Divergencia ;
      }

      public void executeSubmit( String aP0_CalculoDivergencia ,
                                 decimal aP1_Bruto2 ,
                                 decimal aP2_Bruto1 ,
                                 decimal aP3_Liquido2 ,
                                 decimal aP4_Liquido1 ,
                                 out decimal aP5_Divergencia )
      {
         prc_calculardivergencia objprc_calculardivergencia;
         objprc_calculardivergencia = new prc_calculardivergencia();
         objprc_calculardivergencia.AV14CalculoDivergencia = aP0_CalculoDivergencia;
         objprc_calculardivergencia.AV11Bruto2 = aP1_Bruto2;
         objprc_calculardivergencia.AV10Bruto1 = aP2_Bruto1;
         objprc_calculardivergencia.AV13Liquido2 = aP3_Liquido2;
         objprc_calculardivergencia.AV12Liquido1 = aP4_Liquido1;
         objprc_calculardivergencia.AV9Divergencia = 0 ;
         objprc_calculardivergencia.context.SetSubmitInitialConfig(context);
         objprc_calculardivergencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_calculardivergencia);
         aP5_Divergencia=this.AV9Divergencia;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_calculardivergencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( ( StringUtil.StrCmp(AV14CalculoDivergencia, "B") == 0 ) && ( ( ( AV11Bruto2 > Convert.ToDecimal( 0 )) && ( AV10Bruto1 == Convert.ToDecimal( 0 )) ) || ( ( AV10Bruto1 > Convert.ToDecimal( 0 )) && ( AV11Bruto2 == Convert.ToDecimal( 0 )) ) ) )
         {
            AV9Divergencia = (decimal)(100);
         }
         else if ( ( StringUtil.StrCmp(AV14CalculoDivergencia, "L") == 0 ) && ( ( ( AV13Liquido2 > Convert.ToDecimal( 0 )) && ( AV12Liquido1 == Convert.ToDecimal( 0 )) ) || ( ( AV12Liquido1 > Convert.ToDecimal( 0 )) && ( AV13Liquido2 == Convert.ToDecimal( 0 )) ) ) )
         {
            AV9Divergencia = (decimal)(100);
         }
         else if ( ( AV11Bruto2 + AV13Liquido2 > Convert.ToDecimal( 0 )) && ( ( AV10Bruto1 == Convert.ToDecimal( 0 )) || ( AV12Liquido1 == Convert.ToDecimal( 0 )) ) )
         {
            AV9Divergencia = (decimal)(100);
         }
         else if ( ( ( AV11Bruto2 == Convert.ToDecimal( 0 )) || ( AV13Liquido2 == Convert.ToDecimal( 0 )) ) && ( AV10Bruto1 + AV12Liquido1 > Convert.ToDecimal( 0 )) )
         {
            AV9Divergencia = (decimal)(100);
         }
         else if ( ( AV11Bruto2 + AV13Liquido2 > Convert.ToDecimal( 0 )) && ( AV10Bruto1 + AV12Liquido1 > Convert.ToDecimal( 0 )) )
         {
            AV15Contagem = AV10Bruto1;
            AV16Validacao = AV11Bruto2;
            if ( ( AV16Validacao > Convert.ToDecimal( 0 )) )
            {
               AV8Calculo = (decimal)(AV15Contagem-AV16Validacao);
               if ( ( AV8Calculo < Convert.ToDecimal( 0 )) )
               {
                  AV8Calculo = (decimal)(AV8Calculo*-1);
               }
               if ( AV8Calculo > AV15Contagem )
               {
                  AV20DivergenciaB = (decimal)(100);
               }
               else
               {
                  AV20DivergenciaB = NumberUtil.Round( AV8Calculo/ (decimal)(AV15Contagem)*100, 2);
               }
            }
            AV15Contagem = AV12Liquido1;
            AV16Validacao = AV13Liquido2;
            if ( ( AV16Validacao > Convert.ToDecimal( 0 )) )
            {
               AV8Calculo = (decimal)(AV15Contagem-AV16Validacao);
               if ( ( AV8Calculo < Convert.ToDecimal( 0 )) )
               {
                  AV8Calculo = (decimal)(AV8Calculo*-1);
               }
               if ( AV8Calculo > AV15Contagem )
               {
                  AV19DivergenciaL = (decimal)(100);
               }
               else
               {
                  AV19DivergenciaL = NumberUtil.Round( AV8Calculo/ (decimal)(AV15Contagem)*100, 2);
               }
            }
            if ( StringUtil.StrCmp(AV14CalculoDivergencia, "B") == 0 )
            {
               AV9Divergencia = AV20DivergenciaB;
            }
            else if ( StringUtil.StrCmp(AV14CalculoDivergencia, "L") == 0 )
            {
               AV9Divergencia = AV19DivergenciaL;
            }
            else
            {
               AV9Divergencia = ((AV20DivergenciaB>AV19DivergenciaL) ? AV20DivergenciaB : AV19DivergenciaL);
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private decimal AV11Bruto2 ;
      private decimal AV10Bruto1 ;
      private decimal AV13Liquido2 ;
      private decimal AV12Liquido1 ;
      private decimal AV9Divergencia ;
      private decimal AV15Contagem ;
      private decimal AV16Validacao ;
      private decimal AV8Calculo ;
      private decimal AV20DivergenciaB ;
      private decimal AV19DivergenciaL ;
      private String AV14CalculoDivergencia ;
      private decimal aP5_Divergencia ;
   }

}
