/*
               File: PRC_SetDataPrvPgm
        Description: Data prevista de pagamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:22.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_setdataprvpgm : GXProcedure
   {
      public prc_setdataprvpgm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_setdataprvpgm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           String aP1_StatusDmn )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11StatusDmn = aP1_StatusDmn;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 String aP1_StatusDmn )
      {
         prc_setdataprvpgm objprc_setdataprvpgm;
         objprc_setdataprvpgm = new prc_setdataprvpgm();
         objprc_setdataprvpgm.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_setdataprvpgm.AV11StatusDmn = aP1_StatusDmn;
         objprc_setdataprvpgm.context.SetSubmitInitialConfig(context);
         objprc_setdataprvpgm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_setdataprvpgm);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_setdataprvpgm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00TY3 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P00TY3_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00TY3_n597ContagemResultado_LoteAceiteCod[0];
            A1553ContagemResultado_CntSrvCod = P00TY3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00TY3_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P00TY3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00TY3_n1603ContagemResultado_CntCod[0];
            A91Contrato_DiasPagto = P00TY3_A91Contrato_DiasPagto[0];
            n91Contrato_DiasPagto = P00TY3_n91Contrato_DiasPagto[0];
            A1348ContagemResultado_DataHomologacao = P00TY3_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P00TY3_n1348ContagemResultado_DataHomologacao[0];
            A529ContagemResultado_DataAceite = P00TY3_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00TY3_n529ContagemResultado_DataAceite[0];
            A674Lote_DataNfe = P00TY3_A674Lote_DataNfe[0];
            n674Lote_DataNfe = P00TY3_n674Lote_DataNfe[0];
            A1351ContagemResultado_DataPrevista = P00TY3_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00TY3_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00TY3_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00TY3_n472ContagemResultado_DataEntrega[0];
            A1903ContagemResultado_DataPrvPgm = P00TY3_A1903ContagemResultado_DataPrvPgm[0];
            n1903ContagemResultado_DataPrvPgm = P00TY3_n1903ContagemResultado_DataPrvPgm[0];
            A566ContagemResultado_DataUltCnt = P00TY3_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00TY3_n566ContagemResultado_DataUltCnt[0];
            A529ContagemResultado_DataAceite = P00TY3_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00TY3_n529ContagemResultado_DataAceite[0];
            A674Lote_DataNfe = P00TY3_A674Lote_DataNfe[0];
            n674Lote_DataNfe = P00TY3_n674Lote_DataNfe[0];
            A1603ContagemResultado_CntCod = P00TY3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00TY3_n1603ContagemResultado_CntCod[0];
            A91Contrato_DiasPagto = P00TY3_A91Contrato_DiasPagto[0];
            n91Contrato_DiasPagto = P00TY3_n91Contrato_DiasPagto[0];
            A566ContagemResultado_DataUltCnt = P00TY3_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00TY3_n566ContagemResultado_DataUltCnt[0];
            AV10Dias = (short)(((0==A91Contrato_DiasPagto) ? 30 : A91Contrato_DiasPagto));
            if ( StringUtil.StrCmp(AV11StatusDmn, "R") == 0 )
            {
               AV14Data = DateTimeUtil.ResetTime( A566ContagemResultado_DataUltCnt ) ;
            }
            else if ( StringUtil.StrCmp(AV11StatusDmn, "H") == 0 )
            {
               AV14Data = A1348ContagemResultado_DataHomologacao;
            }
            else if ( StringUtil.StrCmp(AV11StatusDmn, "O") == 0 )
            {
               AV14Data = A529ContagemResultado_DataAceite;
            }
            else if ( StringUtil.StrCmp(AV11StatusDmn, "P") == 0 )
            {
               AV14Data = DateTimeUtil.ResetTime( A674Lote_DataNfe ) ;
            }
            else
            {
               if ( P00TY3_n1351ContagemResultado_DataPrevista[0] )
               {
                  AV14Data = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
               }
               else
               {
                  AV14Data = A1351ContagemResultado_DataPrevista;
               }
            }
            AV13DataPrvPgm = DateTimeUtil.TAdd( AV14Data, 86400*(AV10Dias));
            if ( DateTimeUtil.Dow( AV13DataPrvPgm) > 0 )
            {
               while ( ( DateTimeUtil.Dow( AV13DataPrvPgm) == 1 ) || ( DateTimeUtil.Dow( AV13DataPrvPgm) == 7 ) || new prc_ehferiado(context).executeUdp( ref  AV13DataPrvPgm) )
               {
                  AV13DataPrvPgm = DateTimeUtil.TAdd( AV13DataPrvPgm, 86400*(1));
               }
            }
            A1903ContagemResultado_DataPrvPgm = DateTimeUtil.ResetTime(AV13DataPrvPgm);
            n1903ContagemResultado_DataPrvPgm = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00TY4 */
            pr_default.execute(1, new Object[] {n1903ContagemResultado_DataPrvPgm, A1903ContagemResultado_DataPrvPgm, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00TY5 */
            pr_default.execute(2, new Object[] {n1903ContagemResultado_DataPrvPgm, A1903ContagemResultado_DataPrvPgm, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TY3_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00TY3_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00TY3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00TY3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00TY3_A1603ContagemResultado_CntCod = new int[1] ;
         P00TY3_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00TY3_A456ContagemResultado_Codigo = new int[1] ;
         P00TY3_A91Contrato_DiasPagto = new short[1] ;
         P00TY3_n91Contrato_DiasPagto = new bool[] {false} ;
         P00TY3_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00TY3_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P00TY3_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         P00TY3_n529ContagemResultado_DataAceite = new bool[] {false} ;
         P00TY3_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         P00TY3_n674Lote_DataNfe = new bool[] {false} ;
         P00TY3_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00TY3_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00TY3_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00TY3_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00TY3_A1903ContagemResultado_DataPrvPgm = new DateTime[] {DateTime.MinValue} ;
         P00TY3_n1903ContagemResultado_DataPrvPgm = new bool[] {false} ;
         P00TY3_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00TY3_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         A529ContagemResultado_DataAceite = (DateTime)(DateTime.MinValue);
         A674Lote_DataNfe = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1903ContagemResultado_DataPrvPgm = DateTime.MinValue;
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV14Data = (DateTime)(DateTime.MinValue);
         AV13DataPrvPgm = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_setdataprvpgm__default(),
            new Object[][] {
                new Object[] {
               P00TY3_A597ContagemResultado_LoteAceiteCod, P00TY3_n597ContagemResultado_LoteAceiteCod, P00TY3_A1553ContagemResultado_CntSrvCod, P00TY3_n1553ContagemResultado_CntSrvCod, P00TY3_A1603ContagemResultado_CntCod, P00TY3_n1603ContagemResultado_CntCod, P00TY3_A456ContagemResultado_Codigo, P00TY3_A91Contrato_DiasPagto, P00TY3_n91Contrato_DiasPagto, P00TY3_A1348ContagemResultado_DataHomologacao,
               P00TY3_n1348ContagemResultado_DataHomologacao, P00TY3_A529ContagemResultado_DataAceite, P00TY3_n529ContagemResultado_DataAceite, P00TY3_A674Lote_DataNfe, P00TY3_n674Lote_DataNfe, P00TY3_A1351ContagemResultado_DataPrevista, P00TY3_n1351ContagemResultado_DataPrevista, P00TY3_A472ContagemResultado_DataEntrega, P00TY3_n472ContagemResultado_DataEntrega, P00TY3_A1903ContagemResultado_DataPrvPgm,
               P00TY3_n1903ContagemResultado_DataPrvPgm, P00TY3_A566ContagemResultado_DataUltCnt, P00TY3_n566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A91Contrato_DiasPagto ;
      private short AV10Dias ;
      private int A456ContagemResultado_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private String AV11StatusDmn ;
      private String scmdbuf ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime A529ContagemResultado_DataAceite ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV14Data ;
      private DateTime AV13DataPrvPgm ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A1903ContagemResultado_DataPrvPgm ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n91Contrato_DiasPagto ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool n529ContagemResultado_DataAceite ;
      private bool n674Lote_DataNfe ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1903ContagemResultado_DataPrvPgm ;
      private bool n566ContagemResultado_DataUltCnt ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00TY3_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00TY3_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00TY3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00TY3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00TY3_A1603ContagemResultado_CntCod ;
      private bool[] P00TY3_n1603ContagemResultado_CntCod ;
      private int[] P00TY3_A456ContagemResultado_Codigo ;
      private short[] P00TY3_A91Contrato_DiasPagto ;
      private bool[] P00TY3_n91Contrato_DiasPagto ;
      private DateTime[] P00TY3_A1348ContagemResultado_DataHomologacao ;
      private bool[] P00TY3_n1348ContagemResultado_DataHomologacao ;
      private DateTime[] P00TY3_A529ContagemResultado_DataAceite ;
      private bool[] P00TY3_n529ContagemResultado_DataAceite ;
      private DateTime[] P00TY3_A674Lote_DataNfe ;
      private bool[] P00TY3_n674Lote_DataNfe ;
      private DateTime[] P00TY3_A1351ContagemResultado_DataPrevista ;
      private bool[] P00TY3_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00TY3_A472ContagemResultado_DataEntrega ;
      private bool[] P00TY3_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00TY3_A1903ContagemResultado_DataPrvPgm ;
      private bool[] P00TY3_n1903ContagemResultado_DataPrvPgm ;
      private DateTime[] P00TY3_A566ContagemResultado_DataUltCnt ;
      private bool[] P00TY3_n566ContagemResultado_DataUltCnt ;
   }

   public class prc_setdataprvpgm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TY3 ;
          prmP00TY3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TY4 ;
          prmP00TY4 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrvPgm",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TY5 ;
          prmP00TY5 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrvPgm",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TY3", "SELECT TOP 1 T1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T4.[Contrato_DiasPagto], T1.[ContagemResultado_DataHomologacao], T2.[Lote_Data] AS ContagemResultado_DataAceite, T2.[Lote_DataNfe], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_DataPrvPgm], COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Lote] T2 WITH (NOLOCK) ON T2.[Lote_Codigo] = T1.[ContagemResultado_LoteAceiteCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TY3,1,0,true,true )
             ,new CursorDef("P00TY4", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrvPgm]=@ContagemResultado_DataPrvPgm  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00TY4)
             ,new CursorDef("P00TY5", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrvPgm]=@ContagemResultado_DataPrvPgm  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00TY5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
