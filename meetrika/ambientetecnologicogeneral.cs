/*
               File: AmbienteTecnologicoGeneral
        Description: Ambiente Tecnologico General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:42.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ambientetecnologicogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public ambientetecnologicogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public ambientetecnologicogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AmbienteTecnologico_Codigo )
      {
         this.A351AmbienteTecnologico_Codigo = aP0_AmbienteTecnologico_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkAmbienteTecnologico_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A351AmbienteTecnologico_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA9R2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "AmbienteTecnologicoGeneral";
               context.Gx_err = 0;
               WS9R2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Ambiente Tecnologico General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117204265");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("ambientetecnologicogeneral.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICO_TAXAENTREGADSNV", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICO_LOCPF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICO_ATIVO", GetSecureSignedToken( sPrefix, A353AmbienteTecnologico_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9R2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("ambientetecnologicogeneral.js", "?20203117204267");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AmbienteTecnologicoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Ambiente Tecnologico General" ;
      }

      protected void WB9R0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "ambientetecnologicogeneral.aspx");
            }
            wb_table1_2_9R2( true) ;
         }
         else
         {
            wb_table1_2_9R2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9R2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAmbienteTecnologico_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AmbienteTecnologicoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START9R2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Ambiente Tecnologico General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP9R0( ) ;
            }
         }
      }

      protected void WS9R2( )
      {
         START9R2( ) ;
         EVT9R2( ) ;
      }

      protected void EVT9R2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E119R2 */
                                    E119R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E129R2 */
                                    E129R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E139R2 */
                                    E139R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E149R2 */
                                    E149R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E159R2 */
                                    E159R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9R2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9R2( ) ;
            }
         }
      }

      protected void PA9R2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            chkAmbienteTecnologico_Ativo.Name = "AMBIENTETECNOLOGICO_ATIVO";
            chkAmbienteTecnologico_Ativo.WebTags = "";
            chkAmbienteTecnologico_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAmbienteTecnologico_Ativo_Internalname, "TitleCaption", chkAmbienteTecnologico_Ativo.Caption);
            chkAmbienteTecnologico_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9R2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "AmbienteTecnologicoGeneral";
         context.Gx_err = 0;
      }

      protected void RF9R2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H009R2 */
            pr_default.execute(0, new Object[] {A351AmbienteTecnologico_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A353AmbienteTecnologico_Ativo = H009R2_A353AmbienteTecnologico_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_ATIVO", GetSecureSignedToken( sPrefix, A353AmbienteTecnologico_Ativo));
               A978AmbienteTecnologico_LocPF = H009R2_A978AmbienteTecnologico_LocPF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_LOCPF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9")));
               n978AmbienteTecnologico_LocPF = H009R2_n978AmbienteTecnologico_LocPF[0];
               A977AmbienteTecnologico_TaxaEntregaMlhr = H009R2_A977AmbienteTecnologico_TaxaEntregaMlhr[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")));
               n977AmbienteTecnologico_TaxaEntregaMlhr = H009R2_n977AmbienteTecnologico_TaxaEntregaMlhr[0];
               A976AmbienteTecnologico_TaxaEntregaDsnv = H009R2_A976AmbienteTecnologico_TaxaEntregaDsnv[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGADSNV", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")));
               n976AmbienteTecnologico_TaxaEntregaDsnv = H009R2_n976AmbienteTecnologico_TaxaEntregaDsnv[0];
               A352AmbienteTecnologico_Descricao = H009R2_A352AmbienteTecnologico_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
               /* Execute user event: E129R2 */
               E129R2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB9R0( ) ;
         }
      }

      protected void STRUP9R0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "AmbienteTecnologicoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E119R2 */
         E119R2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
            A976AmbienteTecnologico_TaxaEntregaDsnv = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname), ",", "."));
            n976AmbienteTecnologico_TaxaEntregaDsnv = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGADSNV", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")));
            A977AmbienteTecnologico_TaxaEntregaMlhr = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname), ",", "."));
            n977AmbienteTecnologico_TaxaEntregaMlhr = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")));
            A978AmbienteTecnologico_LocPF = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_LocPF_Internalname), ",", "."));
            n978AmbienteTecnologico_LocPF = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_LOCPF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9")));
            A353AmbienteTecnologico_Ativo = StringUtil.StrToBool( cgiGet( chkAmbienteTecnologico_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AMBIENTETECNOLOGICO_ATIVO", GetSecureSignedToken( sPrefix, A353AmbienteTecnologico_Ativo));
            /* Read saved values. */
            wcpOA351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA351AmbienteTecnologico_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E119R2 */
         E119R2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E119R2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E129R2( )
      {
         /* Load Routine */
         edtAmbienteTecnologico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAmbienteTecnologico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_Codigo_Visible), 5, 0)));
      }

      protected void E139R2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("ambientetecnologico.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A351AmbienteTecnologico_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E149R2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("ambientetecnologico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A351AmbienteTecnologico_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E159R2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwambientetecnologico.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "AmbienteTecnologico";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "AmbienteTecnologico_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_9R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_9R2( true) ;
         }
         else
         {
            wb_table2_8_9R2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_9R2( true) ;
         }
         else
         {
            wb_table3_36_9R2( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_9R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9R2e( true) ;
         }
         else
         {
            wb_table1_2_9R2e( false) ;
         }
      }

      protected void wb_table3_36_9R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_9R2e( true) ;
         }
         else
         {
            wb_table3_36_9R2e( false) ;
         }
      }

      protected void wb_table2_8_9R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_descricao_Internalname, "Nome", "", "", lblTextblockambientetecnologico_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_Descricao_Internalname, A352AmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_taxaentregadsnv_Internalname, "Taxa de Entrega (Desenvolvimento)", "", "", lblTextblockambientetecnologico_taxaentregadsnv_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_taxaentregamlhr_Internalname, "Taxa de Entrega (Melhoria)", "", "", lblTextblockambientetecnologico_taxaentregamlhr_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_locpf_Internalname, "LOC/PF", "", "", lblTextblockambientetecnologico_locpf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_LocPF_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_LocPF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_ativo_Internalname, "Ativo", "", "", lblTextblockambientetecnologico_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAmbienteTecnologico_Ativo_Internalname, StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9R2e( true) ;
         }
         else
         {
            wb_table2_8_9R2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A351AmbienteTecnologico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9R2( ) ;
         WS9R2( ) ;
         WE9R2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA351AmbienteTecnologico_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA9R2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "ambientetecnologicogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA9R2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A351AmbienteTecnologico_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         }
         wcpOA351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA351AmbienteTecnologico_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A351AmbienteTecnologico_Codigo != wcpOA351AmbienteTecnologico_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA351AmbienteTecnologico_Codigo = cgiGet( sPrefix+"A351AmbienteTecnologico_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA351AmbienteTecnologico_Codigo) > 0 )
         {
            A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA351AmbienteTecnologico_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         }
         else
         {
            A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A351AmbienteTecnologico_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA9R2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS9R2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS9R2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A351AmbienteTecnologico_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA351AmbienteTecnologico_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A351AmbienteTecnologico_Codigo_CTRL", StringUtil.RTrim( sCtrlA351AmbienteTecnologico_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE9R2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311720436");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("ambientetecnologicogeneral.js", "?2020311720436");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockambientetecnologico_descricao_Internalname = sPrefix+"TEXTBLOCKAMBIENTETECNOLOGICO_DESCRICAO";
         edtAmbienteTecnologico_Descricao_Internalname = sPrefix+"AMBIENTETECNOLOGICO_DESCRICAO";
         lblTextblockambientetecnologico_taxaentregadsnv_Internalname = sPrefix+"TEXTBLOCKAMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname = sPrefix+"AMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         lblTextblockambientetecnologico_taxaentregamlhr_Internalname = sPrefix+"TEXTBLOCKAMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname = sPrefix+"AMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         lblTextblockambientetecnologico_locpf_Internalname = sPrefix+"TEXTBLOCKAMBIENTETECNOLOGICO_LOCPF";
         edtAmbienteTecnologico_LocPF_Internalname = sPrefix+"AMBIENTETECNOLOGICO_LOCPF";
         lblTextblockambientetecnologico_ativo_Internalname = sPrefix+"TEXTBLOCKAMBIENTETECNOLOGICO_ATIVO";
         chkAmbienteTecnologico_Ativo_Internalname = sPrefix+"AMBIENTETECNOLOGICO_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtAmbienteTecnologico_Codigo_Internalname = sPrefix+"AMBIENTETECNOLOGICO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAmbienteTecnologico_LocPF_Jsonclick = "";
         edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick = "";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick = "";
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         chkAmbienteTecnologico_Ativo.Caption = "";
         edtAmbienteTecnologico_Codigo_Jsonclick = "";
         edtAmbienteTecnologico_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E139R2',iparms:[{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E149R2',iparms:[{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E159R2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A352AmbienteTecnologico_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H009R2_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009R2_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         H009R2_A978AmbienteTecnologico_LocPF = new short[1] ;
         H009R2_n978AmbienteTecnologico_LocPF = new bool[] {false} ;
         H009R2_A977AmbienteTecnologico_TaxaEntregaMlhr = new short[1] ;
         H009R2_n977AmbienteTecnologico_TaxaEntregaMlhr = new bool[] {false} ;
         H009R2_A976AmbienteTecnologico_TaxaEntregaDsnv = new short[1] ;
         H009R2_n976AmbienteTecnologico_TaxaEntregaDsnv = new bool[] {false} ;
         H009R2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockambientetecnologico_descricao_Jsonclick = "";
         lblTextblockambientetecnologico_taxaentregadsnv_Jsonclick = "";
         lblTextblockambientetecnologico_taxaentregamlhr_Jsonclick = "";
         lblTextblockambientetecnologico_locpf_Jsonclick = "";
         lblTextblockambientetecnologico_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA351AmbienteTecnologico_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.ambientetecnologicogeneral__default(),
            new Object[][] {
                new Object[] {
               H009R2_A351AmbienteTecnologico_Codigo, H009R2_A353AmbienteTecnologico_Ativo, H009R2_A978AmbienteTecnologico_LocPF, H009R2_n978AmbienteTecnologico_LocPF, H009R2_A977AmbienteTecnologico_TaxaEntregaMlhr, H009R2_n977AmbienteTecnologico_TaxaEntregaMlhr, H009R2_A976AmbienteTecnologico_TaxaEntregaDsnv, H009R2_n976AmbienteTecnologico_TaxaEntregaDsnv, H009R2_A352AmbienteTecnologico_Descricao
               }
            }
         );
         AV14Pgmname = "AmbienteTecnologicoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "AmbienteTecnologicoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short A978AmbienteTecnologico_LocPF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A351AmbienteTecnologico_Codigo ;
      private int wcpOA351AmbienteTecnologico_Codigo ;
      private int edtAmbienteTecnologico_Codigo_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7AmbienteTecnologico_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtAmbienteTecnologico_Codigo_Internalname ;
      private String edtAmbienteTecnologico_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkAmbienteTecnologico_Ativo_Internalname ;
      private String scmdbuf ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname ;
      private String edtAmbienteTecnologico_LocPF_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockambientetecnologico_descricao_Internalname ;
      private String lblTextblockambientetecnologico_descricao_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String lblTextblockambientetecnologico_taxaentregadsnv_Internalname ;
      private String lblTextblockambientetecnologico_taxaentregadsnv_Jsonclick ;
      private String edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick ;
      private String lblTextblockambientetecnologico_taxaentregamlhr_Internalname ;
      private String lblTextblockambientetecnologico_taxaentregamlhr_Jsonclick ;
      private String edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick ;
      private String lblTextblockambientetecnologico_locpf_Internalname ;
      private String lblTextblockambientetecnologico_locpf_Jsonclick ;
      private String edtAmbienteTecnologico_LocPF_Jsonclick ;
      private String lblTextblockambientetecnologico_ativo_Internalname ;
      private String lblTextblockambientetecnologico_ativo_Jsonclick ;
      private String sCtrlA351AmbienteTecnologico_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A353AmbienteTecnologico_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n978AmbienteTecnologico_LocPF ;
      private bool n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool returnInSub ;
      private String A352AmbienteTecnologico_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkAmbienteTecnologico_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H009R2_A351AmbienteTecnologico_Codigo ;
      private bool[] H009R2_A353AmbienteTecnologico_Ativo ;
      private short[] H009R2_A978AmbienteTecnologico_LocPF ;
      private bool[] H009R2_n978AmbienteTecnologico_LocPF ;
      private short[] H009R2_A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool[] H009R2_n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short[] H009R2_A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool[] H009R2_n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private String[] H009R2_A352AmbienteTecnologico_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class ambientetecnologicogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009R2 ;
          prmH009R2 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009R2", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Ativo], [AmbienteTecnologico_LocPF], [AmbienteTecnologico_TaxaEntregaMlhr], [AmbienteTecnologico_TaxaEntregaDsnv], [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ORDER BY [AmbienteTecnologico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009R2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
