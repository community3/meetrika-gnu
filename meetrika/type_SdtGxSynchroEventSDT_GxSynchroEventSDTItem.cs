/*
               File: type_SdtGxSynchroEventSDT_GxSynchroEventSDTItem
        Description: GxSynchroEventSDT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:56.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxSynchroEventSDT.GxSynchroEventSDTItem" )]
   [XmlType(TypeName =  "GxSynchroEventSDT.GxSynchroEventSDTItem" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtGxSynchroEventSDT_GxSynchroEventSDTItem : GxUserType
   {
      public SdtGxSynchroEventSDT_GxSynchroEventSDTItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp = (DateTime)(DateTime.MinValue);
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc = "";
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata = "";
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors = "";
      }

      public SdtGxSynchroEventSDT_GxSynchroEventSDTItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxSynchroEventSDT_GxSynchroEventSDTItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxSynchroEventSDT_GxSynchroEventSDTItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxSynchroEventSDT_GxSynchroEventSDTItem obj ;
         obj = this;
         obj.gxTpr_Eventid = (Guid)(deserialized.gxTpr_Eventid);
         obj.gxTpr_Eventtimestamp = deserialized.gxTpr_Eventtimestamp;
         obj.gxTpr_Eventbc = deserialized.gxTpr_Eventbc;
         obj.gxTpr_Eventaction = deserialized.gxTpr_Eventaction;
         obj.gxTpr_Eventdata = deserialized.gxTpr_Eventdata;
         obj.gxTpr_Eventstatus = deserialized.gxTpr_Eventstatus;
         obj.gxTpr_Eventerrors = deserialized.gxTpr_Eventerrors;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventId") )
               {
                  gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventid = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventTimestamp") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventBC") )
               {
                  gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventAction") )
               {
                  gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventaction = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventData") )
               {
                  gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventStatus") )
               {
                  gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventstatus = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventErrors") )
               {
                  gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxSynchroEventSDT.GxSynchroEventSDTItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("EventId", StringUtil.RTrim( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventid.ToString()));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp) )
         {
            oWriter.WriteStartElement("EventTimestamp");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("EventTimestamp", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("EventBC", StringUtil.RTrim( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("EventAction", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventaction), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("EventData", StringUtil.RTrim( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("EventStatus", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventstatus), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("EventErrors", StringUtil.RTrim( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("EventId", gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventid, false);
         datetime_STZ = gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("EventTimestamp", sDateCnv, false);
         AddObjectProperty("EventBC", gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc, false);
         AddObjectProperty("EventAction", gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventaction, false);
         AddObjectProperty("EventData", gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata, false);
         AddObjectProperty("EventStatus", gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventstatus, false);
         AddObjectProperty("EventErrors", gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors, false);
         return  ;
      }

      [  SoapElement( ElementName = "EventId" )]
      [  XmlElement( ElementName = "EventId"   )]
      public Guid gxTpr_Eventid
      {
         get {
            return gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventid ;
         }

         set {
            gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventid = (Guid)(value);
         }

      }

      [  SoapElement( ElementName = "EventTimestamp" )]
      [  XmlElement( ElementName = "EventTimestamp"  , IsNullable=true )]
      public string gxTpr_Eventtimestamp_Nullable
      {
         get {
            if ( gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp = DateTime.MinValue;
            else
               gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Eventtimestamp
      {
         get {
            return gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp ;
         }

         set {
            gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "EventBC" )]
      [  XmlElement( ElementName = "EventBC"   )]
      public String gxTpr_Eventbc
      {
         get {
            return gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc ;
         }

         set {
            gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "EventAction" )]
      [  XmlElement( ElementName = "EventAction"   )]
      public short gxTpr_Eventaction
      {
         get {
            return gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventaction ;
         }

         set {
            gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventaction = (short)(value);
         }

      }

      [  SoapElement( ElementName = "EventData" )]
      [  XmlElement( ElementName = "EventData"   )]
      public String gxTpr_Eventdata
      {
         get {
            return gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata ;
         }

         set {
            gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata = (String)(value);
         }

      }

      [  SoapElement( ElementName = "EventStatus" )]
      [  XmlElement( ElementName = "EventStatus"   )]
      public short gxTpr_Eventstatus
      {
         get {
            return gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventstatus ;
         }

         set {
            gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventstatus = (short)(value);
         }

      }

      [  SoapElement( ElementName = "EventErrors" )]
      [  XmlElement( ElementName = "EventErrors"   )]
      public String gxTpr_Eventerrors
      {
         get {
            return gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors ;
         }

         set {
            gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp = (DateTime)(DateTime.MinValue);
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc = "";
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata = "";
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors = "";
         gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventid = (Guid)(System.Guid.Empty);
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventaction ;
      protected short gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventstatus ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventtimestamp ;
      protected DateTime datetime_STZ ;
      protected String gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventdata ;
      protected String gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventerrors ;
      protected String gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventbc ;
      protected Guid gxTv_SdtGxSynchroEventSDT_GxSynchroEventSDTItem_Eventid ;
   }

   [DataContract(Name = @"GxSynchroEventSDT.GxSynchroEventSDTItem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtGxSynchroEventSDT_GxSynchroEventSDTItem_RESTInterface : GxGenericCollectionItem<SdtGxSynchroEventSDT_GxSynchroEventSDTItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxSynchroEventSDT_GxSynchroEventSDTItem_RESTInterface( ) : base()
      {
      }

      public SdtGxSynchroEventSDT_GxSynchroEventSDTItem_RESTInterface( SdtGxSynchroEventSDT_GxSynchroEventSDTItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "EventId" , Order = 0 )]
      public Guid gxTpr_Eventid
      {
         get {
            return sdt.gxTpr_Eventid ;
         }

         set {
            sdt.gxTpr_Eventid = (Guid)((Guid)(value));
         }

      }

      [DataMember( Name = "EventTimestamp" , Order = 1 )]
      public String gxTpr_Eventtimestamp
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Eventtimestamp) ;
         }

         set {
            sdt.gxTpr_Eventtimestamp = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "EventBC" , Order = 2 )]
      public String gxTpr_Eventbc
      {
         get {
            return sdt.gxTpr_Eventbc ;
         }

         set {
            sdt.gxTpr_Eventbc = (String)(value);
         }

      }

      [DataMember( Name = "EventAction" , Order = 3 )]
      public Nullable<short> gxTpr_Eventaction
      {
         get {
            return sdt.gxTpr_Eventaction ;
         }

         set {
            sdt.gxTpr_Eventaction = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "EventData" , Order = 4 )]
      public String gxTpr_Eventdata
      {
         get {
            return sdt.gxTpr_Eventdata ;
         }

         set {
            sdt.gxTpr_Eventdata = (String)(value);
         }

      }

      [DataMember( Name = "EventStatus" , Order = 5 )]
      public Nullable<short> gxTpr_Eventstatus
      {
         get {
            return sdt.gxTpr_Eventstatus ;
         }

         set {
            sdt.gxTpr_Eventstatus = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "EventErrors" , Order = 6 )]
      public String gxTpr_Eventerrors
      {
         get {
            return sdt.gxTpr_Eventerrors ;
         }

         set {
            sdt.gxTpr_Eventerrors = (String)(value);
         }

      }

      public SdtGxSynchroEventSDT_GxSynchroEventSDTItem sdt
      {
         get {
            return (SdtGxSynchroEventSDT_GxSynchroEventSDTItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxSynchroEventSDT_GxSynchroEventSDTItem() ;
         }
      }

   }

}
